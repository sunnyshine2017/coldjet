(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: EventHandling
 * File: G_EventHandling.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Global  data types of package EventHandling
 ********************************************************************)

TYPE
	EventHandlingCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bDummy : BOOL;
	END_STRUCT;
	EventHandlingDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bDummy : BOOL;
	END_STRUCT;
	EventHandlingPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		nDummy : UINT;
	END_STRUCT;
	EventHandlingState_TYP : 	STRUCT  (*Rückmeldungen*)
		nErrorCount : UINT;
		nWarningCount : UINT;
		nInfoCount : UINT;
	END_STRUCT;
	EventHandling_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : EventHandlingCallerBox_TYP;
		DirectBox : EventHandlingDirectBox_TYP;
		Par : EventHandlingPar_TYP;
		State : EventHandlingState_TYP;
	END_STRUCT;
	EventId_ENUM : 
		(
		eEVT_ERR_TEST := 0,
		eEVT_ERR_EMERGENCY_STOP,
		eEVT_ERR_LOAD_PARAMETER,
		eEVT_ERR_SAVE_PARAMETER,
		eEVT_ERR_DEL_PARAMETER,
		eEVT_ERR_GET_IP_SETTINGS,
		eEVT_ERR_SET_IP_SETTINGS,
		eEVT_ERR_BONDING_DEVICE,
		eEVT_ERR_SENSOR_DISTANCE,
		eEVT_ERR_NEG_PRODUCT_DISTANCE,
		eEVT_ERR_IMAGE_PROCESSING, (*10*)
		eEVT_ERR_FIFO_OVERFLOW,
		eEVT_ERR_ENCODER_MONITORING,
		eEVT_ERR_INVALID_PRINT_MARK_LEN,
		eEVT_ERR_CREATING_DIR,
		eEVT_ERR_READ_PARAMETER,
		eEVT_ERR_CLEANING,
		eEVT_ERR_AXIS,
		eEVT_ERR_AXIS_CONTROLLER,
		eEVT_ERR_AXIS_LAG_ERROR,
		eEVT_ERR_AXIS_VELOCITY_LIMIT, (*20*)
		eEVT_ERR_AXIS_ACCELERATION_LIMIT,
		eEVT_ERR_AXIS_PLCOPEN_ERROR,
		eEVT_ERR_AXIS_DISCRETE_MOTION,
		eEVT_ERR_AXIS_INVALID_FB_PARAM,
		eEVT_ERR_PRESSURE_CONTROL,
		eEVT_ERR_CLEANING_PERIOD,
		eEVT_ERR_HMI_PANEL_NOT_CONNECTED,
		eEVT_ERR_GLUE_TANK_EMPTY,
		eEVT_ERR_SOFTING_TANK_EMPTY,
		eEVT_ERR_CLEANING_TANK_EMPTY, (*30*)
		eEVT_ERR_TANK_PID_PAR_INVALID,
		eEVT_ERR_TANK_TEMP_SENS_BRK_WIRE,
		eEVT_ERR_TANK_TEMP_SENS_MIN_VAL,
		eEVT_ERR_TANK_TEMP_SENS_INVALID,
		eEVT_ERR_HEAD_PID_PAR_INVALID,
		eEVT_ERR_HEAD_TEMP_SENS_BRK_WIRE,
		eEVT_ERR_HEAD_TEMP_SENS_MIN_VAL,
		eEVT_ERR_HEAD_TEMP_SENS_INVALID,
		eEVT_ERR_TANK_EXT_HEATING_SIGNAL,
		eEVT_ERR_HEAD_EXT_HEATING_SIGNAL, (*40*)
		eEVT_ERR_TEMP_TANK_NOT_IN_TOL,
		eEVT_ERR_TEMP_BON_DEV_NOT_IN_TOL,
		eEVT_ERR_TANK_OVER_TEMP,
		eEVT_ERR_BON_DEV_OVER_TEMP,
		eEVT_ERR_TANK_HEATING_SHORTED,
		eEVT_ERR_BON_DEV_HEATING_SHORTED,
		eEVT_ERR_VAL_CLOSE_COMP_TO_LARGE, (*47*)
		eEVT_WAR_TEST := 100,
		eEVT_WAR_NEG_PRODUCT_DISTANCE,
		eEVT_WAR_TANK_EMPTY_STOP_DELAY_0,
		eEVT_WAR_TANK_EMPTY_STOP_DELAY_1,
		eEVT_WAR_TANK_EMPTY_STOP_DELAY_2,
		eEVT_WAR_STITCH_WRITE_ERR_PVAR,
		eEVT_WAR_SYSTEM_CLEAN_CANCELED,
		eEVT_WAR_GLUE_TANK_EMPTY,
		eEVT_WAR_SOFTING_TANK_EMPTY,
		eEVT_WAR_CLEANING_TANK_EMPTY,
		eEVT_WAR_TANK_PID_PAR_INVALID, (*110*)
		eEVT_WAR_TANK_UPDATE_PID_ABORTED,
		eEVT_WAR_TANK_PID_INTERNAL,
		eEVT_WAR_HEAD_PID_PAR_INVALID,
		eEVT_WAR_HEAD_UPDATE_PID_ABORTED,
		eEVT_WAR_HEAD_PID_INTERNAL,
		eEVT_WAR_HEAT_SYS_CRITICAL_TEMP,
		eEVT_WAR_DISABL_HEAT_SYS_IN_PROD,
		eEVT_WAR_DISABLED_BONDEV_IN_PROD,
		eEVT_WAR_CLEAN_BONDEV_NOT_POSSIB, (*119*)
		eEVT_INF_TEST := 200,
		eEVT_INF_PARAMETER_LOADED,
		eEVT_INF_PARAMETER_SAVED,
		eEVT_INF_PARAMETER_DELETED,
		eEVT_INF_SERVICE_LOGGED_IN,
		eEVT_INF_SERVICE_LOGGED_OUT,
		eEVT_INF_AXIS_INITIALIZED,
		eEVT_INF_HEAT_SYS_WARM_UP,
		eEVT_INF_HEAT_SYS_AUTO_NOT_POS,
		eEVT_INF_HEAT_SYS_IN_STAND_BY,
		eEVT_INF_NO_ENABL_BONDEV_TO_COLD, (*210*)
		eEVT_INF_TANK_SET_TEMP_TO_LOW,
		eEVT_INF_HEAD_SET_TEMP_TO_LOW (*212*)
		);
END_TYPE
