/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: EventHandling
 * File: EventHandling.c
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Implementation of program EventHandling
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <EventHandlingFunc.h>

void _INIT EventHandlingINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gEventHandling, 0, sizeof(gEventHandling));
	gEventHandling.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	brsmemset((UDINT)&gEventManagement, 0, sizeof(gEventManagement));
	EventTest.nCount = 10;
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC EventHandlingCYCLIC(void)
{
	// Direct-Kommandos
	if(Step.bInitDone == 1)
	{
		if(gEventHandling.DirectBox.bDummy == 1)
		{
			BrbClearDirectBox((UDINT)&gEventHandling.DirectBox, sizeof(gEventHandling.DirectBox));
		}
	}
	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			//if(gParHandling.State.bParValid == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gEventHandling.CallerBox, sizeof(gEventHandling.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			if(gEventHandling.CallerBox.bDummy == 1)
			{
				Step.eStepNr = eSTEP_CMD1;
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_CMD1:
			strcpy(Step.sStepText, "eSTEP_CMD1");
			Step.eStepNr = eSTEP_CMD1_FINISHED;
			break;

		case eSTEP_CMD1_FINISHED:
			strcpy(Step.sStepText, "eSTEP_CMD1_FINISHED");
			BrbClearCallerBox((UDINT)&gEventHandling.CallerBox, sizeof(gEventHandling.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

	}

	// Error-Test
	if (1 == 1)
	{
		if(EventTest.Errors.bTestError == 1)
		{
			EventTest.Errors.bTestError = 0;
			AppSetEvent(eEVT_ERR_TEST, EventTest.nCount, "Only test error", EventTest.nCount, &gEventManagement);
			AppResetEvent(eEVT_ERR_TEST, &gEventManagement);
			EventTest.nCount += 1;
		}
		if(AppIsEventAcknowledged(eEVT_ERR_TEST, &gEventManagement) == 1)
		{
			nTestCount++;
		}
		
		if(EventTest.Errors.bEmergencyStop == 1)
		{
			AppSetEvent(eEVT_ERR_EMERGENCY_STOP, 0, "Front", 1, &gEventManagement);
		}
		else
		{
			AppResetEvent(eEVT_ERR_EMERGENCY_STOP, &gEventManagement);
		}
		if(AppIsEventAcknowledged(eEVT_ERR_EMERGENCY_STOP, &gEventManagement) == 1)
		{
		}

		if(EventTest.Errors.bLoadingParameter == 1)
		{
			EventTest.Errors.bLoadingParameter = 0;
			AppSetEvent(eEVT_ERR_LOAD_PARAMETER, 20700, "", 0, &gEventManagement);
			AppResetEvent(eEVT_ERR_LOAD_PARAMETER, &gEventManagement);
		}
		if(AppIsEventAcknowledged(eEVT_ERR_LOAD_PARAMETER, &gEventManagement) == 1)
		{
		}
	}
	// Warnings-Test
	if (1 == 1)
	{
		if(EventTest.Warnings.bTestWarning == 1)
		{
			EventTest.Warnings.bTestWarning = 0;
			AppSetEvent(eEVT_WAR_TEST, EventTest.nCount, "Only test warning", EventTest.nCount, &gEventManagement);
			AppResetEvent(eEVT_WAR_TEST, &gEventManagement);
			EventTest.nCount += 1;
		}
		if(AppIsEventAcknowledged(eEVT_WAR_TEST, &gEventManagement) == 1)
		{
		}
	}
	// Infos-Test
	if (1 == 1)
	{
		if(EventTest.Infos.bTestInfo == 1)
		{
			EventTest.Infos.bTestInfo = 0;
			AppSetEvent(eEVT_INF_TEST, EventTest.nCount, "Only test info", EventTest.nCount, &gEventManagement);
			AppResetEvent(eEVT_INF_TEST, &gEventManagement);
			EventTest.nCount += 1;
		}
		if(AppIsEventAcknowledged(eEVT_INF_TEST, &gEventManagement) == 1)
		{
		}

		if(EventTest.Infos.bParameterLoaded == 1)
		{
			EventTest.Infos.bParameterLoaded = 0;
			AppSetEvent(eEVT_INF_PARAMETER_LOADED, 0, "", 0, &gEventManagement);
			AppResetEvent(eEVT_INF_PARAMETER_LOADED, &gEventManagement);
		}
		if(AppIsEventAcknowledged(eEVT_INF_PARAMETER_LOADED, &gEventManagement) == 1)
		{
		}

		if(EventTest.Infos.bParameterSaved == 1)
		{
			EventTest.Infos.bParameterSaved = 0;
			AppSetEvent(eEVT_INF_PARAMETER_SAVED, 0, "", 0, &gEventManagement);
			AppResetEvent(eEVT_INF_PARAMETER_SAVED, &gEventManagement);
		}
		if(AppIsEventAcknowledged(eEVT_INF_PARAMETER_SAVED, &gEventManagement) == 1)
		{
		}

		if(EventTest.Infos.bServiceLoggedIn == 1)
		{
			EventTest.Infos.bServiceLoggedIn = 0;
			AppSetEvent(eEVT_INF_SERVICE_LOGGED_IN, 0, "", 0, &gEventManagement);
			AppResetEvent(eEVT_INF_SERVICE_LOGGED_IN, &gEventManagement);
		}
		if(AppIsEventAcknowledged(eEVT_INF_SERVICE_LOGGED_IN, &gEventManagement) == 1)
		{
		}

		if(EventTest.Infos.bServiceLoggedOut == 1)
		{
			EventTest.Infos.bServiceLoggedOut = 0;
			AppSetEvent(eEVT_INF_SERVICE_LOGGED_OUT, 0, "", 0, &gEventManagement);
			AppResetEvent(eEVT_INF_SERVICE_LOGGED_OUT, &gEventManagement);
		}
		if(AppIsEventAcknowledged(eEVT_INF_SERVICE_LOGGED_OUT, &gEventManagement) == 1)
		{
		}

	}

	// Ereignis-Behandlung
	tClock = clock_ms();
	nErrorCount = 0;
	nWarningCount = 0;
	nInfoCount = 0;
	for(nEventId=0; nEventId<=nAPP_EVENT_INDEX_MAX; nEventId++)
	{
		if(gEventManagement.Events[nEventId].bSet == 1 && gEventManagement.AlarmImage[nEventId] == 0 && tClock > gEventManagement.Events[nEventId].tClockSet + tSTATE_DELAY_TIME)
		{
			// Ereignis ist (immer noch) aktiv (Setzen wird verzögert, damit VC4 genug Zeit hat, um die Detail-Daten zu erkennen)
			gEventManagement.Events[nEventId].bSet = 0;
			gEventManagement.AlarmImage[nEventId] = 1;// Setzen an VC4
		}
		else if(gEventManagement.Events[nEventId].bReset == 1 && gEventManagement.AlarmImage[nEventId] == 1 && tClock > gEventManagement.Events[nEventId].tClockSet + 2*tSTATE_DELAY_TIME)
		{
			// Ereignis ist nicht mehr aktiv (Rücksetzen wird verzögert, damit VC4 genug Zeit hat, um das gesetzte Bit zu erkennen)
			gEventManagement.Events[nEventId].bSet = 0;
			gEventManagement.Events[nEventId].bReset = 0;
			gEventManagement.AlarmImage[nEventId] = 0;// Rücksetzen an VC4
			gEventManagement.Events[nEventId].bWaitForAcknowledge = 1;
		}
		else if(gEventManagement.Events[nEventId].bWaitForAcknowledge == 1 && gEventManagement.AlarmImage[nEventId] == 0 && gEventManagement.AcknowledgeImage[nEventId] == 0)
		{
			// Ereignis wurde an Visu quittiert
			gEventManagement.Events[nEventId].bWaitForAcknowledge = 0;
			gEventManagement.Events[nEventId].bAcknowledged = 1;
		}
		if(gEventManagement.Events[nEventId].bSet == 1 || gEventManagement.AlarmImage[nEventId] == 1 || gEventManagement.Events[nEventId].bWaitForAcknowledge == 1)
		{
			if(nEventId >= 200)
			{
				nInfoCount += 1;
			}
			else if(nEventId >= 100)
			{
				nWarningCount += 1;
			}
			else
			{
				nErrorCount += 1;
			}
		}
	}
	gEventHandling.State.nErrorCount = nErrorCount;
	gEventHandling.State.nWarningCount = nWarningCount;
	gEventHandling.State.nInfoCount = nInfoCount;
}

void _EXIT EventHandlingEXIT(void)
{
}
