(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: EventHandling
 * File: EventHandling.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Local data types of program EventHandling
 ********************************************************************)

TYPE
	EventHandlingStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_CMD1 := 200,
		eSTEP_CMD1_FINISHED
		);
	EventTestErrors_TYP : 	STRUCT 
		bTestError : BOOL;
		bEmergencyStop : BOOL;
		bLoadingParameter : BOOL;
	END_STRUCT;
	EventTestWarnings_TYP : 	STRUCT 
		bTestWarning : BOOL;
	END_STRUCT;
	EventTestInfos_TYP : 	STRUCT 
		bTestInfo : BOOL;
		bParameterLoaded : BOOL;
		bParameterSaved : BOOL;
		bServiceLoggedIn : BOOL;
		bServiceLoggedOut : BOOL;
	END_STRUCT;
	EventTest_TYP : 	STRUCT 
		nCount : DINT;
		Errors : EventTestErrors_TYP;
		Warnings : EventTestWarnings_TYP;
		Infos : EventTestInfos_TYP;
	END_STRUCT;
END_TYPE
