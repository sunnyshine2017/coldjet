/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Encoder
 * File: EncoderFunc.c
 * Author: kusculart
 * Created: Oktober 02, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <EncoderFunc.h>

// Referenzsignal des �bergebenen Geber-Indexes ermitteln und die Positionen in den jeweiligen Fifo schreiben
void LatchPositionOfSimulatetedReferenzSignal( SINT nEncIndex )
{
	EncConfig[nEncIndex].nDiffIncrements = SimulatedEncoderPar[nEncIndex].nEncoderIncrements - EncLatch[nEncIndex].nOldIncrements;
	
	if( EncConfig[nEncIndex].nDiffIncrements < 0 )
	{
		EncConfig[nEncIndex].nDiffIncrements = EncConfig[nEncIndex].nDiffIncrements * (-1);
	}
	
	// Der simulierte Geber ist gleich oder mehr als eine Umdrehung gefahren -> Referenzsignal muss erzeugt werden
	if( EncConfig[nEncIndex].nDiffIncrements >= SimulatedEncoderPar[nEncIndex].nIncPerRev )
	{
		EncLatch[nEncIndex].nDiffIncrements =  EncConfig[nEncIndex].nDiffIncrements - SimulatedEncoderPar[nEncIndex].nIncPerRev;
		// Inkrement berechnen, an dem das Referenzsignal h�tte kommen sollte
		EncLatch[nEncIndex].nOldIncrements = SimulatedEncoderPar[nEncIndex].nEncoderIncrements - EncLatch[nEncIndex].nDiffIncrements;
		
		// Position berechnen f�r den Zeitpunkt an dem das Referenzsignal anlag
		CurrentFifoPos[nEncIndex].nPosToBond	= 	gEncoder.State.nAxisPos[nEncIndex] - (DINT)((REAL)EncLatch[nEncIndex].nDiffIncrements * (rFACTOR_MM_TO_AXIS_UNITS
													* (gPar.MachinePar.EncoderPar[nEncIndex].rDistancePerRev / (LREAL)SimulatedEncoderPar[nEncIndex].nIncPerRev)));
		CurrentFifoPos[nEncIndex].nDiffPos		= CurrentFifoPos[nEncIndex].nPosToBond - CurrentFifoPos[nEncIndex].nDiffPos;		// Strecke zwischen zwei Referenzimpulsen berechnen
		CurrentFifoPos[nEncIndex].bEditProdukt	= 0;
		BrbFifoIn((BrbMemListManagement_Typ*)&gFifoManEnc[nEncIndex], (UDINT)&CurrentFifoPos[nEncIndex]);		// Gelatchte Pos. f�r Klebeauftrag in Schieberegister speichern
		CurrentFifoPos[nEncIndex].nDiffPos		= CurrentFifoPos[nEncIndex].nPosToBond;
	}
}

// Wenn Position im Fifo die maximalen Abstand von Sensor bis zum Auftragekopf passiert hat, den Positionswert aus Fifo l�schen
void DeletePositionWhenPastMaxSensorDistance( SINT nEncIndex )
{
	if (gFifoManEnc[nEncIndex].nEntryCount > 0)
	{
		BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManEnc[nEncIndex], 0, (UDINT)&FirstPosToBond[nEncIndex].nPosToBond);
		if ((gEncoder.State.nAxisPos[nEncIndex] - FirstPosToBond[nEncIndex].nPosToBond) > nMAX_SENSOR_DISTANCE)
		{
			BrbFifoOut((BrbMemListManagement_Typ*)&gFifoManEnc[nEncIndex], (UDINT)&FirstPosToBond[nEncIndex].nPosToBond);
			// Alle Auftragek�pfe durchsuchen, um die diesem Geber zugeordneten zu finden
			for (nForIdx = 0; nForIdx <= gPar.MachinePar.nBonDevIndex; nForIdx++)
			{
				if ((gPar.ProductPar.BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_ENCODER) &&
					(gPar.ProductPar.BonDevPar[nForIdx].nIdxEncoder == nEncIndex) &&
					(gPar.ProductPar.BonDevPar[nForIdx].bBonDevEn == 1))
				{
					if (gBondingDev.State.nEntryCnt[nForIdx] > 0)
					{
						gBondingDev.State.nEntryCnt[nForIdx] -= 1;
					}
					if (gBondingDev.State.nCurrentFifoCnt[nForIdx] > 0)
					{
						gBondingDev.State.nCurrentFifoCnt[nForIdx] -= 1;
					}
					if (gBondingDev.State.nCurrentFifoCntFlag[nForIdx] > 0)
					{
						gBondingDev.State.nCurrentFifoCntFlag[nForIdx] -= 1;
					}
					if ((gPar.ProductPar.BonDevPar[nForIdx].ProductDetection.bProductDetExist == 1) &&
					(gBondingDev.State.nProDetEntryCnt[nForIdx] > 0))
					{
						gBondingDev.State.nProDetEntryCnt[nForIdx] -= 1;
					}
				}
			}
		}
	}	
}

// Berechnung des simulierten Gebers. Ausgabewert, ist die Positions�nderung in der Einheit 0.01 mm.
DINT CalcSimulatedEncoder( SimEncod_TYP *pSimulatedEncoderPar, EncLatch_TYP *pEncLatch, Encod_TYP *pEncodPar, REAL rSimulatedEncoderSpeed, UDINT nIncPerRev, REAL rDistancePerRev )
{
	// Um ein ruhigeres Geschwindigkeitssignal zu erreichen, werden die in den Parametern angegebenen
	// Inkremente pro Umdrehungen mit 1000 multipliziert.
	pSimulatedEncoderPar->nIncPerRev = nIncPerRev * 1000;
	pSimulatedEncoderPar->rDistancePerRev = rDistancePerRev;
	
	// Inkremente pro Zyklus (1ms) f�r angegebene Sim-Geschwindigkeit ausrechnen
	pSimulatedEncoderPar->rSimInc = rSimulatedEncoderSpeed * 1000.0/* m/s->mm/s */ / 1000/* ms */ * /*rFACTOR_MM_TO_AXIS_UNITS * */((LREAL)pSimulatedEncoderPar->nIncPerRev / pSimulatedEncoderPar->rDistancePerRev);
	// Virtueller Geberwert
	pSimulatedEncoderPar->rSimEncoderIn += pSimulatedEncoderPar->rSimInc + 0.00000001; 								// Der Zahlenwert kompensiert die Rechen-Ungenauigkeit
	pSimulatedEncoderPar->rReset = pSimulatedEncoderPar->rSimInc * 10000;
	if(pSimulatedEncoderPar->rSimEncoderIn >= pSimulatedEncoderPar->rReset)
	{
		// F�r die Berechnung des simulierten Referenzimpulses
		pEncLatch->nOldIncrements -= pSimulatedEncoderPar->nEncoderIncrements;
		pEncLatch->nOldIncrements -= (DINT)pSimulatedEncoderPar->rSimInc;
		
		pSimulatedEncoderPar->rSimEncoderIn = 0;
		pEncodPar->nEncoderIncrementsOld = (DINT)(-pSimulatedEncoderPar->rSimInc);
	}
	pSimulatedEncoderPar->nEncoderIncrements = (DINT)pSimulatedEncoderPar->rSimEncoderIn;			// Simulierten Geberwert �bergeben
	
	// �berlauf des Dints muss ber�cksichtigt werden, deswegen mit Differenz rechnen
	pEncodPar->nDiff = pSimulatedEncoderPar->nEncoderIncrements - pEncodPar->nEncoderIncrementsOld;
	// Umrechnung auf AXIS_UNITS
	pEncodPar->rDiffScaled = (LREAL)pEncodPar->nDiff * rFACTOR_MM_TO_AXIS_UNITS * pSimulatedEncoderPar->rDistancePerRev / (LREAL)pSimulatedEncoderPar->nIncPerRev;	
	// Nachkommastelle aufaddieren und merken
	pEncodPar->nBeforeComma = (DINT)pEncodPar->rDiffScaled;
	pEncodPar->rAfterComma = pEncodPar->rDiffScaled - (LREAL)pEncodPar->nBeforeComma;
	pEncodPar->rAfterCommaStore += pEncodPar->rAfterComma;
	if(pEncodPar->rAfterCommaStore >= 1.0)
	{
		// Nachkommastellen-�berlauf
		pEncodPar->nBeforeComma += 1;
		pEncodPar->rAfterCommaStore = pEncodPar->rAfterCommaStore - 1.0;
	}
	
	// Inkremente merken
	pEncodPar->nEncoderIncrementsOld = pSimulatedEncoderPar->nEncoderIncrements;
	
	// Achspositions�nderung zur�ckgeben; DINT in [AXIS_UNITS] (0,01 mm)
	return pEncodPar->nBeforeComma;
}

