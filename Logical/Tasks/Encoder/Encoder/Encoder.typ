(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Encoder
 * File: Encoder.typ
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Local data types of program Encoder
 ********************************************************************)

TYPE
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_ENC_RESET, (*Geberwerte resetten (Simulation)*)
		eSTEP_ENC_INIT,
		eSTEP_GET_BONDEV_PAR
		);
	EncoderStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	IO_TYP : 	STRUCT 
		Input : IoInput_TYP;
		Output : IoOutput_TYP;
	END_STRUCT;
	IoBrokenWire_TYP : 	STRUCT 
		bBrokenWireA : BOOL;
		bBrokenWireB : BOOL;
		bBrokenWireR : BOOL;
	END_STRUCT;
	IoInput_TYP : 	STRUCT 
		nEncoderIncrements : ARRAY[0..nIDX_ENCODER_MAX]OF DINT; (*Geber Inkremente*)
		nEncLatchInc : ARRAY[0..nIDX_ENCODER_MAX]OF DINT; (*Gelatchte Position bei Referenzimpuls*)
		nEncLatchCount : ARRAY[0..nIDX_ENCODER_MAX]OF SINT; (*Latch Count*)
		bEncLatchEn : ARRAY[0..nIDX_ENCODER_MAX]OF BOOL;
		BrokenWire : ARRAY[0..nIDX_ENCODER_MAX]OF IoBrokenWire_TYP;
	END_STRUCT;
	IoOutput_TYP : 	STRUCT 
		bAckBrokenWire : BOOL;
	END_STRUCT;
	Encod_TYP : 	STRUCT 
		rDiffScaled : LREAL;
		rAfterComma : LREAL;
		rAfterCommaStore : REAL;
		nEncoderIncrementsOld : DINT;
		nDiff : DINT;
		nBeforeComma : DINT;
	END_STRUCT;
	SimEncod_TYP : 	STRUCT 
		EncodPar : Encod_TYP;
		rSimInc : LREAL; (*Inkremente pro Zyklus (1ms) [1 inc/ms]*)
		rSimEncoderIn : LREAL;
		rReset : REAL;
		nIncPerRev : UDINT;
		rDistancePerRev : REAL;
		nEncoderIncrements : DINT;
	END_STRUCT;
	EncLatch_TYP : 	STRUCT 
		nDiffIncrements : DINT;
		nOldIncrements : DINT;
		bEncEnable : BOOL;
	END_STRUCT;
	EncConfig_TYP : 	STRUCT 
		nOldIncrements : DINT; (*Inkremente Alt*)
		nDiffIncrements : DINT;
		nAdaptedInc : DINT; (*Angepasste Inkremente*)
		nAdaptedLatchInc : DINT; (*Angepasste gelatchte Inkremente*)
		bEncReady : BOOL;
		bEncDir : BOOL;
	END_STRUCT;
	CalcedVar_TYP : 	STRUCT 
		nTimeDifferenceTotal : UDINT; (*Summe aller im Fifo befindlichen Zeitdifferenzen zweier Triggersignale*)
		nEntryCount : UINT; (*Anzahl der Eintr�ge im Fifo, mit denen der rAverrageTimeDifference errechnet wird*)
		rAverrageTimeDifference : LREAL; (*Errechneter Mittelwert der Zeitdifferenz zweier Triggersignale*)
	END_STRUCT;
	TriggerEncoderPar_TYP : 	STRUCT 
		nSensorIdx : SINT := -1; (*Index des Triggersensors der dieser Gebernummer zugeordnet ist (Parameter aus gPar->EncoderPar)*)
		nRisingCnt_old : SINT; (*gemerkter Z�hlwert der zuletzt erkannten positiven Flanke des verwendeten Trigger-Sensors (Z�hlwert von DS-Modul)*)
		rDistance : REAL; (*Distanz zwischen zwei Trigger-Signalen in [mm] (Parameter aus gPar->EncoderPar)*)
		rEncoderSpeed : REAL; (*�ber den Trigger-Sensor errechnete Geschwindigkeit des Gebers in [m/s]*)
		CalcedVar : CalcedVar_TYP; (*Struktur mit Varialben zur Berechnung der Geschwindigkeit dieses Gebers*)
		TON_TriggerSignalTimeOut : TON; (*Funktionsblock zur Pr�fung ob die Trigger-Signal TimeOut-Zeit �berschritten wurde*)
	END_STRUCT;
END_TYPE
