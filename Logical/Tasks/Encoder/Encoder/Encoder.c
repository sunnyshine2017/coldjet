/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Encoder
 * File: Encoder.c
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Implementation of program Encoder
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <EncoderFunc.h>

void _INIT EncoderINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gEncoder, 0, sizeof(gEncoder));
	gEncoder.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	brsmemset((UDINT)&nOldLatchCount[0], 0, sizeof(nOldLatchCount));
	brsmemset((UDINT)&rEncInc[0], 0, sizeof(rEncInc));
	brsmemset((UDINT)&EncConfig[0], 0, sizeof(EncConfig));
	SimEncodPar.nIncPerRev = 1024;
	SimEncodPar.rDistancePerRev = 100;
	
	// Letzte gew�hlte Geschwindigkeit des simulierten Gebers setzen
	gEncoder.Par.rSimulatedEncoderSpeed = gParPermanent.rSimulatedEncoderSpeed;
	// Default-Wert setzen
	if( gEncoder.Par.rSimulatedEncoderSpeed == 0.0 )
	{
		gEncoder.Par.rSimulatedEncoderSpeed = 2.0;
	}
	
	rVelocityFactor = 1.0;
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC EncoderCYCLIC(void)
{	
	
	// Automatik wurde eingeschaltet
	fbEdgePosAuto.CLK = gMainLogic.State.bAutomatic;
	R_TRIG(&fbEdgePosAuto);
	if (fbEdgePosAuto.Q == 1)
	{
		bEdgePos = 1;	
	}
	if (bEdgePos == 1)
	{
		if (BrbSetCaller(&gEncoder.CallerBox.Caller, eCALLERID_ENCODER) == eBRB_CALLER_STATE_OK)
		{
			gEncoder.CallerBox.bInitEncoder = 1;									// Initialisierung starten
			bEdgePos = 0;
		}
	}
	
	// ---------- StepHandling ----------
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	
	// ---------- Schrittkette ----------
	switch(Step.eStepNr)
	{
		//=======================================================================================================================
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			
			for(nEncIndex = 0; nEncIndex <= nIDX_ENCODER_MAX; nEncIndex++)											// Schieberegister f�r Position initialisieren
			{
				gFifoManEnc[nEncIndex].pList		= (UDINT)&gEncoder.Par.EncFifo[nEncIndex].FifoPos[0];			// Adressen der Schieberegister �bergeben
				gFifoManEnc[nEncIndex].nEntryLength	= sizeof(gEncoder.Par.EncFifo[nEncIndex].FifoPos[0]);			// Gr��e eines Eintrages
				gFifoManEnc[nEncIndex].nIndexMax	= nFIFO_SIZE_INDEX;												// Max. Index des Schieberegisters
				gFifoManEnc[nEncIndex].nEntryCount	= 0;															// Momentane Anzahl an g�ltigen Eintr�gen
			
				gFifoManTriggerEncoder[nEncIndex].pList = (UDINT)&FifoTriggerSignal[nEncIndex][0];					// Adressen der Schieberegister �bergeben
				gFifoManTriggerEncoder[nEncIndex].nEntryLength = sizeof(FifoTriggerSignal[nEncIndex][0]);			// Gr��e eines Eintrages
				gFifoManTriggerEncoder[nEncIndex].nIndexMax = nIDX_FIFO_ENCODER_TRIGGER_MAX;						// Max. Index des Schieberegisters
				gFifoManTriggerEncoder[nEncIndex].nEntryCount = 0;													// Momentane Anzahl an g�ltigen Eintr�gen
				
				// DCS Fub f�r Geschw. initialisieren
				fbPrecDCSVelocity[nEncIndex].EnableTrack = 0;
				fbPrecDCSVelocity[nEncIndex].Switches.StartPosition = 0;
				fbPrecDCSVelocity[nEncIndex].Switches.Period = 1000;
				fbPrecDCSVelocity[nEncIndex].Switches.FirstOnPosition[0] = 100;
				fbPrecDCSVelocity[nEncIndex].Switches.LastOnPosition[0] = 200;
				// fbPrecDCSVelocity[nEncIndex].FilterOptions.Mode = asMCDCS_FILTER_OFF;
				fbPrecDCSVelocity[nEncIndex].FilterOptions.Mode = asMCDCS_FILTER_LOW_PASS;
				fbPrecDCSVelocity[nEncIndex].FilterOptions.Parameter1 = 0.003;
				fbPrecDCSVelocity[nEncIndex].FilterOptions.Parameter2 = 0.003;
				fbPrecDCSVelocity[nEncIndex].PositionConfig.DataType = asMCDCS_DATATYPE_DINT;
				fbPrecDCSVelocity[nEncIndex].PositionConfig.Period = 0;
			}
			
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			
			if(gParHandling.State.bInitDone == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gEncoder.CallerBox, sizeof(gEncoder.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//=======================================================================================================================
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			
			// Geberwerte resetten
			if (gEncoder.CallerBox.bSimEncReset == 1)
			{
				Step.eStepNr = eSTEP_ENC_RESET;
			}
			
			// Geber initialisieren
			if (gEncoder.CallerBox.bInitEncoder == 1)
			{
				Step.eStepNr = eSTEP_ENC_INIT;
			}
			
			// Bondig Device Parameter ermitteln
			if (gEncoder.CallerBox.bGetBonDevPar == 1)
			{
				Step.eStepNr = eSTEP_GET_BONDEV_PAR;
			}

			break;
		
		//-----------------------------------------------------------------------------------------------------------------------
		// Geberwerte zur�cksetzen
		case eSTEP_ENC_RESET:
			strcpy(Step.sStepText, "eSTEP_ENC_RESET");
			
			brsmemset((UDINT)&SimEncodPar.EncodPar, 0, sizeof(SimEncodPar.EncodPar));
			SimEncodPar.rSimEncoderIn		= 0.0;
			SimEncodPar.nEncoderIncrements	= 0;
			SimEncodPar.nEncoderIncrements	= 0;
			SimEncodPar.rReset				= 0;
			SimEncodPar.rSimInc				= 0;			
			gEncoder.State.nSimAxisPos		= 0;
			BrbClearCallerBox((UDINT)&gEncoder.CallerBox, sizeof(gEncoder.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Geber initialisieren
		case eSTEP_ENC_INIT:
			strcpy(Step.sStepText, "eSTEP_ENC_INIT");
			
			brsmemset((UDINT)&EncLatch, 0, sizeof(EncLatch));
			// Bonding Device Parameter ermitteln
			for (nEncIndex = 0; nEncIndex <= nIDX_ENCODER_MAX; nEncIndex++)	
			{
				EncLatch[nEncIndex].bEncEnable = 0;
				// Fifo abl�schen Trigger Sensor
				BrbMemListClear((BrbMemListManagement_Typ*)&gFifoManEnc[nEncIndex]);
			}
			
			Step.eStepNr = eSTEP_GET_BONDEV_PAR;
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Bonding Device Parameter ermitteln
		case eSTEP_GET_BONDEV_PAR:
			strcpy(Step.sStepText, "eSTEP_GET_BONDEV_PAR");
			
			for (nEncIndex = 0; nEncIndex <= gPar.MachinePar.nEncoderIndex; nEncIndex++)
			{
				for (nForIdx = 0; nForIdx <= gPar.MachinePar.nBonDevIndex; nForIdx++)	
				{
					if ((gPar.ProductPar.BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_ENCODER) &&
						(gPar.ProductPar.BonDevPar[nForIdx].nIdxEncoder == nEncIndex) &&
						(gPar.ProductPar.BonDevPar[nForIdx].bBonDevEn == 1) &&
						(EncLatch[nEncIndex].bEncEnable == 0))
					{
						IO.Input.bEncLatchEn[nEncIndex] = 1;
						EncLatch[nEncIndex].bEncEnable = 1;
						CurrentFifoPos[nEncIndex].nDiffPos = 0;
						// Flankenz�hler synchronisieren
						nOldLatchCount[nEncIndex] = IO.Input.nEncLatchCount[nEncIndex];
					}
				}
			}

			gEncoder.State.bEncReady = 1;
			BrbClearCallerBox((UDINT)&gEncoder.CallerBox, sizeof(gEncoder.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;						
			break;
	}
	
	// ------------- DirektBox -----------------
	if( gEncoder.DirectBox.bSimulatedEncoderStart == TRUE )
	{
		gEncoder.DirectBox.bSimulatedEncoderStart = FALSE;
		gEncoder.State.bSimulatedEncoderStarted = TRUE;
	}
	if( gEncoder.DirectBox.bSimulatedEncoderStop == TRUE )
	{
		gEncoder.DirectBox.bSimulatedEncoderStop = FALSE;
		gEncoder.State.bSimulatedEncoderStarted = FALSE;
		
		memset( &SimulatedEncoderPar, 0x00, sizeof( SimulatedEncoderPar ) );
		
//		fbPrecDCSVelocity[0].Enable = TRUE;
	}
	
	// ---------- Systemzeit auslesen ----------
	// Der Status "gSystem.State.nSystemTime" ist am Datenpunkt "SystemTime" der SPS gemappt.
	// In der ArSim gibt es diesen Datenpunkt nicht. Er muss deshalb hier in TC#1 ermittelt werden.
	#ifdef HW_ARSIM
		gSystem.State.nSystemTime = AsIOTimeStamp();
	#endif

	
	// ---------- Gebersimulation Ein (ist nicht der simulierte Geber!!!) ----------
	if ((gEncoder.DirectBox.bSimEncOn == 1) ||
		(gMainLogic.State.bCleaningActive == 1))
	{
		gEncoder.State.bSimEnc = 1;	
		
		if (gMainLogic.State.bCleaningActive == 1)							// Reinigung aktiv
		{
			gEncoder.Par.rSimSpeed = nCLEANING_VELOCITY;					// Bei Reinigung Geschwindigkeit simulieren
		}
		
		// Inkremente pro Zyklus (1ms) f�r angegebene Sim-Geschwindigkeit ausrechnen
		SimEncodPar.rSimInc = gEncoder.Par.rSimSpeed * rFACTOR_MM_TO_AXIS_UNITS * ((LREAL)SimEncodPar.nIncPerRev / SimEncodPar.rDistancePerRev);
		// Virtueller Geberwert
		SimEncodPar.rSimEncoderIn += SimEncodPar.rSimInc + 0.00000001; 								// Der Zahlenwert kompensiert die Rechen-Ungenauigkeit
		SimEncodPar.rReset = SimEncodPar.rSimInc * 100;
		if(SimEncodPar.rSimEncoderIn >= SimEncodPar.rReset)
		{
			SimEncodPar.rSimEncoderIn = 0;
			SimEncodPar.EncodPar.nEncoderIncrementsOld = (DINT)(-SimEncodPar.rSimInc);
		}
		SimEncodPar.nEncoderIncrements = (DINT)SimEncodPar.rSimEncoderIn;			// Simulierten Geberwert �bergeben
		
		// �berlauf des Dints muss ber�cksichtigt werden, deswegen mit Differenz rechnen
		SimEncodPar.EncodPar.nDiff = SimEncodPar.nEncoderIncrements - SimEncodPar.EncodPar.nEncoderIncrementsOld;
		// Umrechnung auf AXIS_UNITS
		SimEncodPar.EncodPar.rDiffScaled = (LREAL)SimEncodPar.EncodPar.nDiff * SimEncodPar.rDistancePerRev / (LREAL)SimEncodPar.nIncPerRev;	
		// Nachkommastelle aufaddieren und merken
		SimEncodPar.EncodPar.nBeforeComma = (DINT)SimEncodPar.EncodPar.rDiffScaled;
		SimEncodPar.EncodPar.rAfterComma = SimEncodPar.EncodPar.rDiffScaled - (LREAL)SimEncodPar.EncodPar.nBeforeComma;
		SimEncodPar.EncodPar.rAfterCommaStore += SimEncodPar.EncodPar.rAfterComma;
		if(SimEncodPar.EncodPar.rAfterCommaStore >= 1.0)
		{
			// Nachkommastellen-�berlauf
			SimEncodPar.EncodPar.nBeforeComma += 1;
			SimEncodPar.EncodPar.rAfterCommaStore = SimEncodPar.EncodPar.rAfterCommaStore - 1.0;
		}
		// Achsposition ist DINT in  [AXIS_UNITS]
		gEncoder.State.nSimAxisPos += SimEncodPar.EncodPar.nBeforeComma;
		if (gEncoder.State.bSimEnc == 1)
		{
			gEncoder.State.nSimAxisPos = gEncoder.State.nSimAxisPos;
		}
		SimEncodPar.EncodPar.nEncoderIncrementsOld = SimEncodPar.nEncoderIncrements;
	}
	else
	{
		gEncoder.State.bSimEnc = 0;
	}
	
	
	// ---------- Reale Hardware-Geberauswertung starten ----------
//	if (gParHandling.State.bMParValid == 1)
//	{
	// Schleife von Index 1 (erster als Hardware vorhandener Geber) bis Anzahl konfigurierter Geber
	for (nEncIndex = 1; nEncIndex <= gPar.MachinePar.nEncoderIndex; nEncIndex++)
	{
		// Der Geber mit diesem Index ist KEIN virtueller Geber und ist als Hardware vorhanden
		if( gPar.MachinePar.EncoderPar[nEncIndex].eEncoderType == eENCODER_TYPE_ENCODER)
		{
			// ---------- Geschwindigkeiten ermitteln ----------
			if (nEncIndex > gPar.MachinePar.nEncoderIndex)
			{
				fbPrecDCSVelocity[nEncIndex].Enable = 0;
			}
			else
			{
				fbPrecDCSVelocity[nEncIndex].Enable = 1;
				fbPrecDCSVelocity[nEncIndex].Position.Integer = gEncoder.State.nAxisPos[nEncIndex];
				ASMcDcsPrecisionDigitalCamSwitch(&fbPrecDCSVelocity[nEncIndex]);
				gEncoder.State.rVelocity[nEncIndex] = fbPrecDCSVelocity[nEncIndex].AdditionalInfo.Velocity;
			}
			
			// ---------- Z�hlrichtung vom Geber ermitteln ----------
//			if (IO.Input.nEncoderIncrements[nEncIndex] < EncConfig[nEncIndex].nOldIncrements)
//			{
//				EncConfig[nEncIndex].bEncDir = 0;
//				EncConfig[nEncIndex].bEncReady = 1;
//			}
//			else if (IO.Input.nEncoderIncrements[nEncIndex] > EncConfig[nEncIndex].nOldIncrements)
//			{
//				EncConfig[nEncIndex].bEncDir = 1;
//				EncConfig[nEncIndex].bEncReady = 1;
//			}
//			EncConfig[nEncIndex].nDiffIncrements = ( IO.Input.nEncoderIncrements[nEncIndex] - EncConfig[nEncIndex].nOldIncrements);
			
			EncConfig[nEncIndex].nDiffIncrements = ( IO.Input.nEncoderIncrements[nEncIndex] - EncConfig[nEncIndex].nOldIncrements);

			EncConfig[nEncIndex].nDiffIncrements = EncConfig[nEncIndex].nDiffIncrements * gPar.MachinePar.EncoderPar[nEncIndex].rVelocityFactor;
			
			if (EncConfig[nEncIndex].nDiffIncrements < 0)
			{
				EncConfig[nEncIndex].nDiffIncrements = EncConfig[nEncIndex].nDiffIncrements * (-1);
			}
			// ---------- Angepasste Inkremente vom Geber ----------
			EncConfig[nEncIndex].nAdaptedInc 	= EncConfig[nEncIndex].nAdaptedInc + EncConfig[nEncIndex].nDiffIncrements;
			EncConfig[nEncIndex].nOldIncrements = IO.Input.nEncoderIncrements[nEncIndex];
			
//			if (IO.Input.nEncoderIncrements[nEncIndex] < EncConfig[nEncIndex].nOldIncrements)
//			{
//				EncConfig[nEncIndex].bEncDir = 0;
//				EncConfig[nEncIndex].bEncReady = 1;
//			}
//			else if (IO.Input.nEncoderIncrements[nEncIndex] > EncConfig[nEncIndex].nOldIncrements)
//			{
//				EncConfig[nEncIndex].bEncDir = 1;
//				EncConfig[nEncIndex].bEncReady = 1;
//			}
//			
//			if (EncodPar[nEncIndex].nDiff == 0)
//			{
//				EncConfig[nEncIndex].bEncReady = 0;
//			}
			
			
//			// ---------- Z�hlrichtung vom Geber ----------
//			if (EncConfig[nEncIndex].bEncDir == 0)
//			{
//				EncConfig[nEncIndex].nAdaptedInc = IO.Input.nEncoderIncrements[nEncIndex] * (- 1);
//				EncConfig[nEncIndex].nAdaptedLatchInc = IO.Input.nEncLatchInc[nEncIndex] * (- 1);
//			}
//			else
//			{
//				EncConfig[nEncIndex].nAdaptedInc = IO.Input.nEncoderIncrements[nEncIndex];
//				EncConfig[nEncIndex].nAdaptedLatchInc = IO.Input.nEncLatchInc[nEncIndex];
//			}
//			EncConfig[nEncIndex].nOldIncrements = IO.Input.nEncoderIncrements[nEncIndex];
		
			// ---------- Achsposition berechnen ----------
			
			// �berlauf des Dints muss ber�cksichtigt werden, deswegen mit Differenz rechnen
			EncodPar[nEncIndex].nDiff = EncConfig[nEncIndex].nAdaptedInc - EncodPar[nEncIndex].nEncoderIncrementsOld;
			// Umrechnung auf AXIS_UNITS
			if (gPar.MachinePar.EncoderPar[nEncIndex].nIncPerRev <= 0)
			{
				gPar.MachinePar.EncoderPar[nEncIndex].nIncPerRev = 1024;	// Falls Wert = 0, dann mit Default Wert beschreiben
			}

			rEncInc[nEncIndex] = gPar.MachinePar.EncoderPar[nEncIndex].nIncPerRev * 4;		// Faktor 4 kommt daher, dass zwei Spure (A,B) sowohl positiv als auch negativ ausgewertet werden
			
			EncodPar[nEncIndex].rDiffScaled = (LREAL)EncodPar[nEncIndex].nDiff * rFACTOR_MM_TO_AXIS_UNITS * (gPar.MachinePar.EncoderPar[nEncIndex].rDistancePerRev / (LREAL)rEncInc[nEncIndex]);
			// Nachkommastelle aufaddieren und merken
			EncodPar[nEncIndex].nBeforeComma = (DINT)EncodPar[nEncIndex].rDiffScaled;
			EncodPar[nEncIndex].rAfterComma = EncodPar[nEncIndex].rDiffScaled - (LREAL)EncodPar[nEncIndex].nBeforeComma;
			EncodPar[nEncIndex].rAfterCommaStore += EncodPar[nEncIndex].rAfterComma;
			if (EncodPar[nEncIndex].rAfterCommaStore >= 1.0)
			{
				// Nachkommastellen-�berlauf
				EncodPar[nEncIndex].nBeforeComma += 1;
				EncodPar[nEncIndex].rAfterCommaStore = EncodPar[nEncIndex].rAfterCommaStore - 1.0;
			}
			// Achsposition ist DINT in  [AXIS_UNITS] (0,01 mm)
			gEncoder.State.nAxisPos[nEncIndex] += EncodPar[nEncIndex].nBeforeComma;
			EncodPar[nEncIndex].nEncoderIncrementsOld = EncConfig[nEncIndex].nAdaptedInc;
			
			
			// ---------- Achsposition bei Referenzsignal ermitteln ----------
			if ((gMainLogic.State.bAutomatic == 1) &&
				(EncLatch[nEncIndex].bEncEnable == 1) &&
				(gEncoder.State.bEncReady == 1))
			{
				if (IO.Input.nEncLatchCount[nEncIndex] != nOldLatchCount[nEncIndex])										// Referenzimpuls gekommen
				{
					EncLatch[nEncIndex].nDiffIncrements 	= IO.Input.nEncoderIncrements[nEncIndex] - IO.Input.nEncLatchInc[nEncIndex];
					if (EncLatch[nEncIndex].nDiffIncrements < 0)
					{
						EncLatch[nEncIndex].nDiffIncrements = EncLatch[nEncIndex].nDiffIncrements * (-1);
					}
					//EncLatch[nEncIndex].nDiffIncrements 	= EncConfig[nEncIndex].nAdaptedInc - EncConfig[nEncIndex].nAdaptedLatchInc;
//// ToDo MZ: Ist da ein Fehler beim Casten, wenn die Division einen Wert kleiner Null hat???
//					CurrentFifoPos[nEncIndex].nPosToBond	= gEncoder.State.nAxisPos[nEncIndex] - (EncLatch[nEncIndex].nDiffIncrements * (DINT)(rFACTOR_MM_TO_AXIS_UNITS * (gPar.MachinePar.EncoderPar[nEncIndex].rDistancePerRev / (LREAL)rEncInc[nEncIndex])));		// Position berechnen
					CurrentFifoPos[nEncIndex].nPosToBond	= gEncoder.State.nAxisPos[nEncIndex] - (REAL)(EncLatch[nEncIndex].nDiffIncrements * (rFACTOR_MM_TO_AXIS_UNITS * (gPar.MachinePar.EncoderPar[nEncIndex].rDistancePerRev / (LREAL)rEncInc[nEncIndex])));		// Position berechnen
					CurrentFifoPos[nEncIndex].nDiffPos		= CurrentFifoPos[nEncIndex].nPosToBond - CurrentFifoPos[nEncIndex].nDiffPos;		// Strecke zwischen zwei Referenzimpulsen berechnen
					CurrentFifoPos[nEncIndex].bEditProdukt	= 0;
					BrbFifoIn((BrbMemListManagement_Typ*)&gFifoManEnc[nEncIndex], (UDINT)&CurrentFifoPos[nEncIndex]);		// Gelatchte Pos. f�r Klebeauftrag in Schieberegister speichern
					CurrentFifoPos[nEncIndex].nDiffPos		= CurrentFifoPos[nEncIndex].nPosToBond;
					nOldLatchCount[nEncIndex]				= IO.Input.nEncLatchCount[nEncIndex];
				}
			}
			else
			{
				EncLatch[nEncIndex].nOldIncrements = IO.Input.nEncLatchInc[nEncIndex];
				IO.Input.bEncLatchEn[nEncIndex] = 0;
//				gEncoder.State.bEncReady = 0;
			}
			
			// ---------- Wenn Position im Fifo den Auftragekopf passiert, den Positionswert aus Fifo l�schen ----------
			DeletePositionWhenPastMaxSensorDistance( nEncIndex );
			
			// Geber�berwachung hat ausgel�st
			if (IO.Output.bAckBrokenWire == 1)
			{
				fbTimer.IN = 1;
				fbTimer.PT = 150;
				TON_10ms(&fbTimer);
				if (fbTimer.Q == 1)
				{
					bMsgDone[nEncIndex] = 0;
					IO.Output.bAckBrokenWire = 0;
					fbTimer.IN = 0;
					TON_10ms(&fbTimer);					
				}
			}
			if (((IO.Input.BrokenWire[nEncIndex].bBrokenWireA == 1) ||
				(IO.Input.BrokenWire[nEncIndex].bBrokenWireB == 1) ||
				(IO.Input.BrokenWire[nEncIndex].bBrokenWireR == 1)) &&
				(bMsgDone[nEncIndex] == 0))
			{
				if (IO.Input.BrokenWire[nEncIndex].bBrokenWireA == 1)
				{
					AppSetEvent(eEVT_ERR_ENCODER_MONITORING, 0, "A", (nEncIndex), &gEventManagement);					// Meldung absetzen 
					AppResetEvent(eEVT_ERR_ENCODER_MONITORING, &gEventManagement);
				}
				if (IO.Input.BrokenWire[nEncIndex].bBrokenWireB == 1)
				{
					AppSetEvent(eEVT_ERR_ENCODER_MONITORING, 0, "B", (nEncIndex), &gEventManagement);					// Meldung absetzen 
					AppResetEvent(eEVT_ERR_ENCODER_MONITORING, &gEventManagement);
				}
				if (IO.Input.BrokenWire[nEncIndex].bBrokenWireR == 1)
				{
					AppSetEvent(eEVT_ERR_ENCODER_MONITORING, 0, "R", (nEncIndex), &gEventManagement);					// Meldung absetzen 
					AppResetEvent(eEVT_ERR_ENCODER_MONITORING, &gEventManagement);
				}
				bMsgDone[nEncIndex] = 1;			
			}
			// Fehler wurde quittiert
			if (AppIsEventAcknowledged(eEVT_ERR_ENCODER_MONITORING, &gEventManagement) == 1)
			{
				IO.Output.bAckBrokenWire = 1;
			}
			
		}
	}

	// ---------- Berechnung des Simulierten-Gebers starten ----------
	
	// Der simulierte Geber ist aktiv
	if( gPar.MachinePar.EncoderPar[0].bSimulationEnable == TRUE )
	{
		if( gEncoder.State.bSimulatedEncoderStarted == TRUE )
		{
			// Status global sichtbar setzen, dass der simulierte Geber l�uft
			gEncoder.State.bSimulatedEncoderStarted = TRUE;
			
			gEncoder.State.nAxisPos[0] += CalcSimulatedEncoder(	&SimulatedEncoderPar[0],
																&EncLatch[0],
																&EncodPar[0],
																gEncoder.Par.rSimulatedEncoderSpeed,
																gPar.MachinePar.EncoderPar[0].nIncPerRev,
																gPar.MachinePar.EncoderPar[0].rDistancePerRev );
			
			// ---------- Achsposition bei Referenzsignal ermitteln ----------
			if ((gMainLogic.State.bAutomatic == 1) &&
				(EncLatch[0].bEncEnable == 1) &&
				(gEncoder.State.bEncReady == 1))
			{
				// Referenzsignal des simulierten Gebers ermitteln und die Positionen in Fifo schreiben
				LatchPositionOfSimulatetedReferenzSignal( 0 );
				
			}
			
			// ---------- Wenn Position im Fifo den Auftragekopf passiert, den Positionswert aus Fifo l�schen ----------
			DeletePositionWhenPastMaxSensorDistance( 0 );
			
			// Die Parameter von EncodPar[0] auf die EncodPar-Struktur von SimulatedEncoderPar kopieren. Dies dient nur zur leichteren
			// Betrachtung der Werte des simulierten Gebers im Watch. Dies hat keine Auswirkung auf die Funktionalit�t.
			memcpy( &SimulatedEncoderPar[0].EncodPar, &EncodPar[0], sizeof( SimulatedEncoderPar[0].EncodPar ) );
		
			fbPrecDCSVelocity[0].Enable = TRUE;
			fbPrecDCSVelocity[0].Position.Integer = gEncoder.State.nAxisPos[0];
			ASMcDcsPrecisionDigitalCamSwitch(&fbPrecDCSVelocity[0]);
			gEncoder.State.rVelocity[0] = fbPrecDCSVelocity[0].AdditionalInfo.Velocity;
		
		}
		else
		{
			gEncoder.State.rVelocity[0] = 0.0;
		}
		
		// Ge�nderte simulierte Gebergeschwindigkeit in remanente Struktur speichern
		if( gEncoder.Par.rSimulatedEncoderSpeed != gParPermanent.rSimulatedEncoderSpeed )
		{	
			gParPermanent.rSimulatedEncoderSpeed = gEncoder.Par.rSimulatedEncoderSpeed;
		}
		
	}
	
	// ---------- Auswertung der virtuellen Geber starten ----------
	// Schleife von Index 1 bis Anzahl konfigurierter Geber
	for (nEncIndex = 1; nEncIndex <= gPar.MachinePar.nEncoderIndex; nEncIndex++)
	{
		// Der Geber mit diesem Index ist ein virtueller Geber und nicht als Hardware vorhanden
		if( gPar.MachinePar.EncoderPar[nEncIndex].eEncoderType == eENCODER_TYPE_VIRTUAL )
		{
			// Index des Quell-Gebers dieses virtuellen Gebers ermittlen		
			nSourceEncoderIdx = gPar.MachinePar.EncoderPar[nEncIndex].nSourceEncoderIdx;
			
			// Differenz der aktuellen und der Geberposition des letzten Zykluses ermitteln
			nSourceEncoderDifference = gEncoder.State.nAxisPos[nSourceEncoderIdx] - nSourceEncoderAxisPos_old[nEncIndex];
			// Die Differenz muss immer positiv sein
			if( nSourceEncoderDifference < 0 )
			{
				nSourceEncoderDifference = nSourceEncoderDifference * (-1);
			}
						
			// Alle Auftragek�pfe durchsuchen, um die diesem Geber zugeordneten zu finden
			for( nForIdx = 0; nForIdx <= gPar.MachinePar.nBonDevIndex; nForIdx++ )
			{
				if( gPar.ProductPar.BonDevPar[nForIdx].nIdxEncoder == nEncIndex )
				{
					// Division durch Null vermeiden
					if( gPar.ProductPar.BonDevPar[nForIdx].rProductLen > 0 )
					{
						// Diese hier implementierte Formel ergibt sich aus der Spezifikation "2015_06_036_Klebung f�r digitale Falzwerke_.pdf" von manroland Seite 2 Punkt 3.1.
						gEncoder.State.nAxisPos[nEncIndex] += (DINT)(nSourceEncoderDifference * (gPar.MachinePar.EncoderPar[nEncIndex].rFactor / gPar.ProductPar.BonDevPar[nForIdx].rProductLen ) );
						// Schleife bei dem zuerst gefundenen Kopf mit Geber-Typ "virtueller Geber" beenden, da sonst ein vielfaches des Faktors "gPar.MachinePar.EncoderPar[nEncIndex].rFactor / gPar.ProductPar.BonDevPar[nForIdx].rProductLen" hinzugerechnet wird
						break;
					}
				}
			}
			
			// Geschwindigkeit errechnen
			fbPrecDCSVelocity[nEncIndex].Enable = 1;
			fbPrecDCSVelocity[nEncIndex].Position.Integer = gEncoder.State.nAxisPos[nEncIndex];
			ASMcDcsPrecisionDigitalCamSwitch(&fbPrecDCSVelocity[nEncIndex]);
			gEncoder.State.rVelocity[nEncIndex] = fbPrecDCSVelocity[nEncIndex].AdditionalInfo.Velocity;
			
			// Position des Quellgebers f�r den jeweiligen virtuellen Geber merken. Wird f�r die Differenzbildung im n�chsten Zyklus ben�tigt.
			nSourceEncoderAxisPos_old[nEncIndex] = gEncoder.State.nAxisPos[nSourceEncoderIdx];
		
		}
	}
	
	// ---------- Auswertung des Trigger-Gebers starten ----------
	// Schleife von Index 1 bis Anzahl konfigurierter Geber
	for (nEncIndex = 1; nEncIndex <= gPar.MachinePar.nEncoderIndex; nEncIndex++)
	{
		// Der Geber mit diesem Index ist ein Trigger-Geber
		if( gPar.MachinePar.EncoderPar[nEncIndex].eEncoderType == eENCODER_TYPE_TRIGGER )
		{	
			// Diesem Trigger-Geber ist ein g�ltiger Trigger-Sensor zugewiesen (nTriggerSensorIdx muss gr��er gleich Null sein)
			if( (gPar.MachinePar.EncoderPar[nEncIndex].nTriggerSensorIdx >= 0) &&
				(gPar.MachinePar.EncoderPar[nEncIndex].nTriggerSensorIdx <= gPar.MachinePar.nTriggerSenIndex) )
			{
				// Parameter aus Parameterstruktur heraussuchen und in TriggerEncoderPar-Struktur eintragen
				TriggerEncoderPar[nEncIndex].nSensorIdx = gPar.MachinePar.EncoderPar[nEncIndex].nTriggerSensorIdx;
				TriggerEncoderPar[nEncIndex].rDistance = gPar.MachinePar.EncoderPar[nEncIndex].rTriggerSignalDistance;
				
				// Es wurde eine neue positive Flanke des Triggersensors erkannt
				if( gTriggerSensor.Par.SensorTS[TriggerEncoderPar[nEncIndex].nSensorIdx].nRisingCnt != TriggerEncoderPar[nEncIndex].nRisingCnt_old  )
				{
					
					// Ist der Fifo mit der maximalen Anzahl an Eintr�gen gef�llt?
					if( gFifoManTriggerEncoder[nEncIndex].nEntryCount == gFifoManTriggerEncoder[nEncIndex].nIndexMax + 1 )
					{
						// -> �ltesten Eintrag aus Fifo l�schen um Platz f�r den n�chsten zu schaffen
						BrbFifoOut( &gFifoManTriggerEncoder[nEncIndex], (UDINT)&TriggerSignal );
					}
					
					// CurrentTriggerSignal-Struktur mit wergen bef�llen, um diese anschlie�end in den Fifo zu schreiben
					CurrentTriggerSignal.nCount = gTriggerSensor.Par.SensorTS[TriggerEncoderPar[nEncIndex].nSensorIdx].nRisingCnt;
					nTempTimeDifference = (INT)gSystem.State.nSystemTime - gTriggerSensor.Par.SensorTS[TriggerEncoderPar[nEncIndex].nSensorIdx].nTsRising;
					CurrentTriggerSignal.nTimeStamp = gSystem.State.nSystemTime - nTempTimeDifference;
				
					// Im Fifo ist bereits mindestens ein Eintrag enthalten
					if( gFifoManTriggerEncoder[nEncIndex].nEntryCount > 0 )
					{
						// Den vorherigen Eintrag auslesen und auf die Struktur LastTriggerSignal schreiben
						BrbMemListGetEntry( &gFifoManTriggerEncoder[nEncIndex], gFifoManTriggerEncoder[nEncIndex].nEntryCount - 1, (UDINT)&LastTriggerSignal );
						
						// Zeitdifferenz der Zeitstempel des letzten und des aktuellen Eintrages ermitteln und in den aktuellen Eintrag �bernehmen
						CurrentTriggerSignal.nTimeDifference = CurrentTriggerSignal.nTimeStamp - LastTriggerSignal.nTimeStamp;
					}
					// Dieser wird der erste Eintrag in den Fifo
					else
					{
						// Zeitdifferenz ist Null, da es kein vorheriges Triggersignal gibt
						CurrentTriggerSignal.nTimeDifference = 0;
					}
					
					// Die ermittelten Werte der CurrentTriggerSignal-Struktur in Fifo eintragen
					BrbFifoIn((BrbMemListManagement_Typ*)&gFifoManTriggerEncoder[nEncIndex], (UDINT)&CurrentTriggerSignal);
					
					
					// ----- Berechnung der Geschwindigkeit aus den Triggersignalen
					// Es sind mindestens zwei Eintr�ge im Fifo vorhanden
					if( gFifoManTriggerEncoder[nEncIndex].nEntryCount > 1 )
					{
						// Hilfsvariable zur�cksetzen
						nTimeDifferenceTotal = 0;
						
						// Alle im Fifo befindlichen Eintr�ge der Variable nTimeDifference addieren
						for( i=0; i<gFifoManTriggerEncoder[nEncIndex].nEntryCount; i++ )
						{
							
							BrbMemListGetEntry( &gFifoManTriggerEncoder[nEncIndex], i, (UDINT)&TriggerSignal );
							
							// Eintrag nTimeDifference in Hilfsvariable addieren
							nTimeDifferenceTotal += TriggerSignal.nTimeDifference;
						}
						
						
						// Beim ersten Triggersignal nach einem Timeout/nach dem einschalten ist der Wert nTimeDifference des
						// ersten Fifo-Eintrages leer (Null). Damit dies die Mittelwertbildung der Zeitdifferenz der Trigger-Signale
						// nicht verf�lscht, wird gepr�ft ob der erste Eintrag des Fifos hier eine Null enth�lt, um daraufhin die die
						// Anzahl der eingetragenen Werte anzupassen.
						if( FifoTriggerSignal[nEncIndex][0].nTimeDifference == 0 )
						{
							// -> ung�ltiger Wert im ersten Eintrag vorhanden, daher die Anzahl der Eintr�ge um 1 verringern
							nEntryCount = gFifoManTriggerEncoder[nEncIndex].nEntryCount - 1;
						}
						// Alle vorhandenen Fifo-Eintr�ge sind mit g�ltigen Werten f�r nTimeDiefference bef�llt
						else
						{
							nEntryCount = gFifoManTriggerEncoder[nEncIndex].nEntryCount;
						}
						
						// Mittelwert der Zeitdifferenz der Triggersignale ermitteln
						rAverrageTimeDifference = (LREAL)nTimeDifferenceTotal / nEntryCount;
						
						// Der Mittelwert ist nicht Null und auch nicht negativ?
						if( rAverrageTimeDifference > 0.0 )
						{
							// Ja:
							// Berechnung der derzeitigen Geschwindigkeit in [m/s]
							TriggerEncoderPar[nEncIndex].rEncoderSpeed = (LREAL)TriggerEncoderPar[nEncIndex].rDistance / 1000 /*mm -> m*/ / ( rAverrageTimeDifference / 1000.0 / 1000.0);
						}
						else
						{
							// Nein: Der Mittelwert ist Null bzw. negativ
							// Die Geschwindigkeit betr�gt 0 m/s
							TriggerEncoderPar[nEncIndex].rEncoderSpeed = 0.0;
						}
						
						// Hilfsvariablen f�r Fehlersuche etc. in Struktur schreiben
						TriggerEncoderPar[nEncIndex].CalcedVar.rAverrageTimeDifference = rAverrageTimeDifference;
						TriggerEncoderPar[nEncIndex].CalcedVar.nTimeDifferenceTotal = nTimeDifferenceTotal;
						TriggerEncoderPar[nEncIndex].CalcedVar.nEntryCount = nEntryCount;
					}
					
					// Da ein neues Triggersignal erfasst wurde, den TimeOut zur�cksetzen
					TriggerEncoderPar[nEncIndex].TON_TriggerSignalTimeOut.IN = FALSE;
					
					// Aktuellen nRisingCnt der Eingangskarte merken, um eine �nderung ermitteln zu k�nnen
					TriggerEncoderPar[nEncIndex].nRisingCnt_old = gTriggerSensor.Par.SensorTS[TriggerEncoderPar[nEncIndex].nSensorIdx].nRisingCnt;
				}
				// Keine neues Triggersingal in diesem Zyklus
				else
				{
					// Trigger-TimeOut starten bzw. weiterlaufen lassen
					TriggerEncoderPar[nEncIndex].TON_TriggerSignalTimeOut.IN = TRUE;
					TriggerEncoderPar[nEncIndex].TON_TriggerSignalTimeOut.PT = gPar.MachinePar.EncoderPar[nEncIndex].rTriggerSignalTimeOut * 1000;
				}
				
				// Aufruf der Funktion TON() um den Trigger-TimeOut feststelen zu k�nnen
				TON( &TriggerEncoderPar[nEncIndex].TON_TriggerSignalTimeOut );
				// Innerhalb der konfigurierten Trigger-TimeOut-Zeit ist kein neue Triggersignal erfasst worden
				if( TriggerEncoderPar[nEncIndex].TON_TriggerSignalTimeOut.Q == TRUE )
				{
					// Alle eintr�ge im Fifo werden gel�scht
					BrbMemListClear( (BrbMemListManagement_Typ*)&gFifoManTriggerEncoder[nEncIndex] );
					// Die Geschwindigkeit wird auf Null gesetzt
					TriggerEncoderPar[nEncIndex].rEncoderSpeed = 0.0;
				}
				
				if( TriggerEncoderPar[nEncIndex].rEncoderSpeed != 0.0 )
				{
					// Ermittelte Geschwindigkeit an den simulierten Geber �bergeben und mit diesem die Positionen berechnen
					gEncoder.State.nAxisPos[nEncIndex] += CalcSimulatedEncoder(	&SimulatedEncoderPar[nEncIndex],
																				&EncLatch[nEncIndex],
																				&EncodPar[nEncIndex],
																				TriggerEncoderPar[nEncIndex].rEncoderSpeed,
																				1024,
																				1000.0 );
					
					// Die Parameter von EncodPar[nEncIndex] auf die EncodPar-Struktur von SimulatedEncoderPar kopieren. Dies dient nur zur leichteren
					// Betrachtung der Werte des simulierten Gebers im Watch. Dies hat keine Auswirkung auf die Funktionalit�t.
					memcpy( &SimulatedEncoderPar[nEncIndex].EncodPar, &EncodPar[nEncIndex], sizeof( SimulatedEncoderPar[nEncIndex].EncodPar ) );
																				
					// Geschwindigkeit ermitteln, aus den vom simulierten Geber errechneten Positionen
					fbPrecDCSVelocity[nEncIndex].Enable = TRUE;
					fbPrecDCSVelocity[nEncIndex].Position.Integer = gEncoder.State.nAxisPos[nEncIndex];
					ASMcDcsPrecisionDigitalCamSwitch(&fbPrecDCSVelocity[nEncIndex]);
					gEncoder.State.rVelocity[nEncIndex] = fbPrecDCSVelocity[nEncIndex].AdditionalInfo.Velocity;
				}
				else
				{
					gEncoder.State.rVelocity[nEncIndex] = 0.0;
				}
			}
			// Diesem Trigger-Geber ist kein g�ltiger Trigger-Sensor zugewiesen (nTriggerSensorIdx < 0) bzw. die Anzahl der Triggersensoren
			// wurde verringert und der verkn�pfte Trigger ist nicht mehr konfiguriert (nTriggerSensorIdx > gPar.MachinePar.nTriggerSenIndex)
			else
			{
				// Die Konfiguration wurde gerade so ge�ndert, dass der verkn�pfte Triggersensor ung�ltig wurde
				if( (TriggerEncoderPar[nEncIndex].nSensorIdx != gPar.MachinePar.EncoderPar[nEncIndex].nTriggerSensorIdx) ||
					(gPar.MachinePar.EncoderPar[nEncIndex].nTriggerSensorIdx > gPar.MachinePar.nTriggerSenIndex) )
				{
					// Den verkn�pften Triggersensor auf ung�ltig setzen
					gPar.MachinePar.EncoderPar[nEncIndex].nTriggerSensorIdx = -1;
					// Errechnete Geschwindigkeit und Postionen auf ung�ltig setzen
					gEncoder.State.rVelocity[nEncIndex] = 0.0;
					gEncoder.State.nAxisPos[nEncIndex] = 0;
					// 
					TriggerEncoderPar[nEncIndex].rEncoderSpeed = 0.0;
					TriggerEncoderPar[nEncIndex].rDistance = 0.0;
					TriggerEncoderPar[nEncIndex].nRisingCnt_old = 0;
					// Alle eintr�ge im Fifo werden gel�scht
					BrbMemListClear( (BrbMemListManagement_Typ*)&gFifoManTriggerEncoder[nEncIndex] );
					// nTriggerSensorIdx merken um �nderungen feststellen zu k�nnen
					TriggerEncoderPar[nEncIndex].nSensorIdx = gPar.MachinePar.EncoderPar[nEncIndex].nTriggerSensorIdx;
				}
			}
		}
		else
		{
			// Dieser Geber war im letzten Zyklus noch als Trigger-Geber konfiguriert
			if( TriggerEncoderPar[nEncIndex].rDistance != 0.0 )
			{
				// Interne Trigger-Parameter zur�cksetzen
				memset( &TriggerEncoderPar[nEncIndex], 0x00, sizeof( TriggerEncoderPar[nEncIndex] ) );
				// Alle eintr�ge im Fifo werden gel�scht
				BrbMemListClear( (BrbMemListManagement_Typ*)&gFifoManTriggerEncoder[nEncIndex] );
			}
		}
	}
	
	// Variablen und Parameter der nicht aktivierten Geber zur�cksetzen (n�tig wenn von 2 auf 1 Geber umgestellt wird)
	if( gPar.MachinePar.nEncoderIndex != nEncoderIndex_old )
	{
		// Schleife vom letzten aktiven Geber-Index + 1 bis maximale Anzahl m�glicher Geber
		for (nEncIndex=gPar.MachinePar.nEncoderIndex + 1; nEncIndex<=nIDX_ENCODER_MAX; nEncIndex++)
		{
			gEncoder.State.rVelocity[nEncIndex] = 0.0;
			gEncoder.State.nAxisPos[nEncIndex] = 0;
			memset( &TriggerEncoderPar[nEncIndex], 0x00, sizeof( TriggerEncoderPar[nEncIndex] ) );
			// Alle eintr�ge im Fifo werden gel�scht
			BrbMemListClear( (BrbMemListManagement_Typ*)&gFifoManTriggerEncoder[nEncIndex] );
			BrbMemListClear( (BrbMemListManagement_Typ*)&gFifoManEnc[nEncIndex] );
		}
		
		// Merken wie viele Geber konfiguriert sind, um �nderungen dieser Konfiguration festzustellen
		nEncoderIndex_old = gPar.MachinePar.nEncoderIndex;
	}
}

void _EXIT EncoderEXIT(void)
{
}
