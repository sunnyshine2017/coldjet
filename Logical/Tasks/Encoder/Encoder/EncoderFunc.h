/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Encoder
 * File: EncoderFunc.h
 * Author: kusculart
 * Created: Oktober 02, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#include <string.h>
#include <Global.h>

void LatchPositionOfSimulatetedReferenzSignal( SINT nEncIndex );
void DeletePositionWhenPastMaxSensorDistance( SINT nEncIndex );
DINT CalcSimulatedEncoder( SimEncod_TYP *pSimulatedEncoderPar, EncLatch_TYP *pEncLatch, Encod_TYP *pEncodPar, REAL rSimulatedEncoderSpeed, UDINT nIncPerRev, REAL rDistancePerRev );
