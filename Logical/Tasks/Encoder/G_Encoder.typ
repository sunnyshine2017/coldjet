(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: Encoder
 * File: G_Encoder.typ
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Global  data types of package Encoder
 ********************************************************************)

TYPE
	EncoderCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bSimEncReset : BOOL; (*Kommando Encoder Werte resetten (Simulation)*)
		bInitEncoder : BOOL; (*Geber initialisieren f�r rotativen Klebeauftrag*)
		bGetBonDevPar : BOOL;
	END_STRUCT;
	EncoderDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bSimEncOn : BOOL; (*Geberwerte simulieren*)
		bSimulatedEncoderStart : BOOL; (*Starten des simulierten Gebers*)
		bSimulatedEncoderStop : BOOL; (*Stoppen des simulierten Gebers*)
	END_STRUCT;
	EncoderPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		EncFifo : ARRAY[0..nIDX_ENCODER_MAX]OF EncFifoPos_TYP; (*Encoder Fifo*)
		rSimSpeed : REAL; (*Simulierte Geschwindigkeit in 0,01 m/s*)
		rSimulatedEncoderSpeed : REAL; (*Geschwindigkeit des simulierten Encoders in 0.01 m/s*)
	END_STRUCT;
	EncoderState_TYP : 	STRUCT  (*R�ckmeldungen*)
		rVelocity : ARRAY[0..nIDX_ENCODER_MAX]OF REAL; (*Errechnete Geschwindigkeit vom FUB in 0,01 mm/s*)
		nAxisPos : ARRAY[0..nIDX_ENCODER_MAX]OF DINT; (*Vorkomma Achsposition in 1/100 mm*)
		nSimAxisPos : DINT; (*Simulierte Achsposition in 1/100 mm*)
		bSimEnc : BOOL; (*Status Geber Simulation*)
		bEncReady : BOOL; (*Geber  bereit*)
		bSimulatedEncoderStarted : BOOL; (*Simulierter Geber ist gestartet*)
	END_STRUCT;
	Encoder_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : EncoderCallerBox_TYP;
		DirectBox : EncoderDirectBox_TYP;
		Par : EncoderPar_TYP;
		State : EncoderState_TYP;
	END_STRUCT;
	EncFifoPos_TYP : 	STRUCT 
		FifoPos : ARRAY[0..nFIFO_SIZE_INDEX]OF PosToBond_TYP;
	END_STRUCT;
	FifoTriggerSignal_TYP : 	STRUCT 
		nTimeStamp : DINT;
		nTimeDifference : DINT;
		nCount : SINT;
	END_STRUCT;
END_TYPE
