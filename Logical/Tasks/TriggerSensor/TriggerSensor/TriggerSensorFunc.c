/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: TriggerSensor
 * File: TriggerSensorFunc.c
 * Author: kusculart
 * Created: Oktober 02, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <TriggerSensorFunc.h>

// Diese Funktion ermittelt, je nach Betriebsart des Trigger-Sensors, die gelatchte Position und schreibt diese in den entsprechenden Fifo
void GetTriggeredPositionAndWriteIntoFifo( DINT nTriggerPosition )
{
	if( BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR )
	{
		FifoPos[nForIdx].nPosToBond = nTriggerPosition;
		FifoPos[nForIdx].nDiffPos	= FifoPos[nForIdx].nPosToBond - FifoPos[nForIdx].nDiffPos;										// Strecke zw. zwei Produkten bilden
		FifoPos[nForIdx].bEditProdukt = 0;
		FifoPos[nForIdx].bProductDetected = 0;
		
		// ----- Triggersensor ist hinter dem Klebekopf angebracht -----
		// d.h. das Produkt welches erkannt wird, ist schon unter dem Klebekopf hindurchgefahren und dieses kann nicht mehr geklebt werden. Ist das Produkt ein Endlos-Papier, dann kann
		// anhand dieses Triggersignals �ber die Produktl�nge auf die Position des folgenden x-ten Produktes geschlossen werden und dieses kann geklebt werden.
		
		// Der Abstand zwischen Druckmarkensensor und Klebekopf ist negativ
		// -> Der Druckmarkensensor ist nicht wie �blich vor dem Klebekopf, sondern sitzt hinter diesem
		if( BonDevPar[nForIdx].nSensorOffset < 0 )
		{
			// Den negativen Sensorabstand in einen Absolutwert wandeln
			NegativeSensorOffset.nNegativeSensorOffset = -BonDevPar[nForIdx].nSensorOffset;
			// Anhand der Produktl�nge errechnen, wie viele ganzzahlige sich zwischen Klebekopf und Druckmarkensensor befinden
			NegativeSensorOffset.nProductsBetweenBonDev_Sensor = NegativeSensorOffset.nNegativeSensorOffset / BonDevPar[nForIdx].nProductLen;
			// Die L�nge des Teiles des Produktes in [1/100 mm] welcher nicht mehr ganz zwischen Klebekopf und Sensor passt
			NegativeSensorOffset.nProductRestBetweenBonDev_Sensor = NegativeSensorOffset.nNegativeSensorOffset % BonDevPar[nForIdx].nProductLen;
			// Die Produkte passen nicht ganzzahlig in den Abstand zwischen Klebekopf und Sensor -> Es bleibt ein Rest �brig
			if( NegativeSensorOffset.nProductRestBetweenBonDev_Sensor > 0 )
			{
				// Produktl�nge in [1/100 mm] welche nicht mehr in den Abstand zwischen Klebekopf und Sensor passt -> Steht �ber den Klebekopf hinaus
				NegativeSensorOffset.nProductRest = BonDevPar[nForIdx].nProductLen - NegativeSensorOffset.nProductRestBetweenBonDev_Sensor;
			}
			// Die Produkte passen ganzzahlig in den Abstand zwischen Klebekopf und Sensor
			else
			{
				// Das letzte Produkt schlie�t b�ndig mit dem Klebekopf ab
				NegativeSensorOffset.nProductRest = 0;
			}
			
			NegativeSensorOffset.nNegativeOffsetToBonDev = NegativeSensorOffset.nProductRest;
			
			for( nProductCnt = 0; nProductCnt < 10; nProductCnt++ )
			{	
				if( NegativeSensorOffset.nNegativeOffsetToBonDev < (100/*mm*/ * rFACTOR_MM_TO_AXIS_UNITS) )
				{
					// Die Produktl�nge so lange aufaddieren, bis der nNegativeOffsetToBonDev mindestens 100 mm gro� ist
					NegativeSensorOffset.nNegativeOffsetToBonDev += BonDevPar[nForIdx].nProductLen;
				}
				else
				{
					// Schleife hier verlassen
					break;
				}
			}
			// Offset errechnen, der auf die gelatchte Position addiert wird um an der richtigen Stelle des x-ten nachfolgenden Produktes kleben zu k�nnen	
			NegativeSensorOffset.nNegativePrintMarkPositionOffset = NegativeSensorOffset.nNegativeSensorOffset + NegativeSensorOffset.nNegativeOffsetToBonDev;
			// Position des x-ten zu klebenden Produktes durch addieren des Offsets ermitteln
			FifoPos[nForIdx].nPosToBond += NegativeSensorOffset.nNegativePrintMarkPositionOffset;
		}
		// Berechnete Pos. f�r Klebeauftrag in Schieberegister speichern
		BrbFifoIn((BrbMemListManagement_Typ*)&gFifoManTrSen[nForIdx], (UDINT)&FifoPos[nForIdx]);
		IgnoreWrongTrigger[nForIdx].nStartPosPro = FifoPos[nForIdx].nPosToBond;
		FifoPos[nForIdx].nDiffPos	= FifoPos[nForIdx].nPosToBond;
		IgnoreWrongTrigger[nForIdx].bInit = 1;
		
	}
	else if( BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
	{
		// 
		if( gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource].nEntryCount > 0 )
		{									
			// Schleife �ber die vom Druckmarkensensor erkannten Druckmarken (Fifo-Eintr�ge)
			for( i = 0; i < gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource].nEntryCount; i++ )
			{
				// Auslesen des jeweiligen Eintrag des Fifos
				BrbMemListGetEntry( (BrbMemListManagement_Typ*)&gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource], (UDINT)i, (UDINT)&FifoPrintMarkTriggerPos[nForIdx] );
				
				// Dieser Eintrag wurde noch keinem Produkt zugeordnet, welches der Trigger-Sensor erkannt hat									
				if( FifoPrintMarkTriggerPos[nForIdx].bProductPositionSetByTrigger == FALSE )
				{
					// Hilfvariablen bef�llen
					nPositionProductBegins = FifoPrintMarkTriggerPos[nForIdx].nPositionProductBegins;
					nDistanceProductMoved = nTriggerPosition/*[nForIdx]*/ - nPositionProductBegins;
					nDistanceProductHasToMove = BonDevPar[nForIdx].nDistancePrintMarkTriggerSensor;
					
					// Die vom Druckmarkensensor geltachte Position in diesem Fifo-Eintrag hat das untere Toleranzfeld erreicht -> Dieser Eintrag kann zu dem vom Triggersensor erfassten Produkt geh�ren
					if( nDistanceProductMoved >= nDistanceProductHasToMove - BonDevPar[nForIdx].nProductLen / 2 )
					{
						// Die vom Druckmarkensensor geltachte Position in diesem Fifo-Eintrag liegt innerhalb der Toleranz -> Dieser Eintrag geh�rt zu dem vom Trigger-Sensor erfassten Produkt
						if( nDistanceProductMoved <= nDistanceProductHasToMove + BonDevPar[nForIdx].nProductLen / 2 )
						{
							// Triggerz�hler nur einmal f�r diesen Eintrag erh�hen
							if( FifoPrintMarkTriggerPos[nForIdx].bProductPositionSetByTrigger == FALSE )
							{
								// Die ermittelte Geber-Position w�hrend des Trigger-Signals in Fifo eintragen und damit die Position die der Druckmarkensensors eingetragen hat �berschreiben.
								FifoPrintMarkTriggerPos[nForIdx].nPositionProductBegins = nTriggerPosition/*[nForIdx]*/ ;
								// Flag setzen, dass dieser Eintrag einen g�ltigen, vom Trigger-Sensor gelatchten, Positionswert besitzt. Anhand dieses Flags arbeitet der Task "BondingDev" diesen Eintrag ab.
								FifoPrintMarkTriggerPos[nForIdx].bProductPositionSetByTrigger = TRUE;
								// Eintrag in Fifo mit den neuen Werten �berschreiben.
//														BrbMemListIn( (BrbMemListManagement_Typ*)&gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource], (UDINT)i, (UDINT)&FifoPrintMarkTriggerPos[nForIdx] );
								memcpy( ((PmPos_TYP*)gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource].pList) + i, &FifoPrintMarkTriggerPos[nForIdx] , gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource].nEntryLength );
								
								// Triggerz�hler nur einmal f�r diesen Eintrag erh�hen
								nTriggerSignalEntryCount[nForIdx]++;
							}
							// Schleife abbrechen
							break;
						}
					}
				}
				else
				{
					// n�chsten Fifo-Eintrag �berpr�fen
					continue;
				}
			}									
		}
		else
		{
			// -> erstmal nix
		}
	}
}

// Diese Funktion dekrementiert die EntryCnts der globalen Struktur gBondingDev.State
void DeleteEntryCntsInBondingDevState( USINT nBonDevIndex )
{
	
	if (gBondingDev.State.nEntryCnt[nBonDevIndex] > 0)
	{
		gBondingDev.State.nEntryCnt[nBonDevIndex] -= 1;
	}
	if (gBondingDev.State.nCurrentFifoCnt[nBonDevIndex] > 0)
	{
		gBondingDev.State.nCurrentFifoCnt[nBonDevIndex] -= 1;
	}
	if (gBondingDev.State.nCurrentFifoCntFlag[nBonDevIndex] > 0)
	{
		gBondingDev.State.nCurrentFifoCntFlag[nBonDevIndex] -= 1;
	}
	// Produkterkennung aktiv
	if( (gPar.ProductPar.BonDevPar[nBonDevIndex].ProductDetection.bProductDetExist == TRUE) &&
		(gBondingDev.State.nProDetEntryCnt[nBonDevIndex] > 0) )
	{
		gBondingDev.State.nProDetEntryCnt[nBonDevIndex] -= 1;
	}
}
