/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: TriggerSensor
 * File: TriggerSensor.c
 * Author: kusculart
 * Created: September 02, 2014
 ********************************************************************
 * Implementation of program TriggerSensor
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <TriggerSensorFunc.h>

void _INIT TriggerSensorINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gTriggerSensor, 0, sizeof(gTriggerSensor));
	gTriggerSensor.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	brsmemset((UDINT)&nOldEdgeRisingCnt,0,sizeof(nOldEdgeRisingCnt));
	brsmemset((UDINT)&nOldEdgeFallingCnt,0,sizeof(nOldEdgeFallingCnt));
	bRequestInit = 0;
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC TriggerSensorCYCLIC(void)
{

	// Automatik wurde eingeschaltet
	fbEdgePosAuto.CLK = gMainLogic.State.bAutomatic;
	R_TRIG(&fbEdgePosAuto);
	if (fbEdgePosAuto.Q == 1)
	{
		//gTriggerSensor.CallerBox.bInit = 1;
		bRequestInit = 1;
	}
	if (bRequestInit == 1)
	{
		if (BrbSetCaller(&gTriggerSensor.CallerBox.Caller, eCALLERID_TRIGGER_SENSOR) == eBRB_CALLER_STATE_OK)
		{
			gTriggerSensor.CallerBox.bInit = 1;									// Initialisierung starten
			bRequestInit = 0;
		}	
	}

	
	// --------- Direkt-Kommandos ---------
	// Die Initialisierungs-Schrittkette erfolgreich durchgelaufen -> Direkt-Kommandos sind m�glich
	if(Step.bInitDone == TRUE)
	{
		// Trigger-Parameter der Klebek�pfe updaten, da sich diese in der Visu ge�ndert haben (�bernahmen w�hrend Automatik aktiv)
		if( gTriggerSensor.DirectBox.bUpdateBonDevPar == TRUE )
		{
			// Direkt-Kommando zur�cksetzen
			gTriggerSensor.DirectBox.bUpdateBonDevPar = FALSE;
			
			// Parameter werden nur direkt �bernommen, wenn sich das Klebesystem im Automatik-Betrieb befindet
			if( gMainLogic.State.bAutomatic == TRUE )
			{
				// Bonding Device Parameter ermitteln
				for (nForIdx = 0; nForIdx <= gPar.MachinePar.nBonDevIndex; nForIdx++)	
				{	
					nIdxSenSource = gPar.ProductPar.BonDevPar[nForIdx].nIdxSenSource;
					
					if ((gPar.ProductPar.BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR) &&
						(gPar.ProductPar.BonDevPar[nForIdx].bBonDevEn == TRUE) )
					{	
						// Diese Parameter k�nnen w�hrend der Automatik-Betrieb aktiv ist ge�ndert werden und werden daraufhin sofort �bernommen.
						BonDevPar[nIdxSenSource].nSensorOffset = (DINT)(gPar.ProductPar.BonDevPar[nForIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS);
					}
					else if( (gPar.ProductPar.BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) &&
							 (gPar.ProductPar.BonDevPar[nForIdx].bBonDevEn == TRUE) )
					{
						// Diese Parameter k�nnen w�hrend der Automatik-Betrieb aktiv ist ge�ndert werden und werden daraufhin sofort �bernommen.
						BonDevPar[nIdxSenSource].nSensorOffset = (DINT)(gPar.ProductPar.BonDevPar[nForIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS);
						BonDevPar[nIdxSenSource].nDistancePrintMarkTriggerSensor = (DINT)(gPar.ProductPar.BonDevPar[nForIdx].rDistancePrintMarkTriggerSensor * rFACTOR_MM_TO_AXIS_UNITS);
					}
				}		
			}
		}
	}
	// Initialisierungs-Schrittkette ist noch nicht abgelaufen -> keine Direkt-Kommandos m�glich
	else
	{
		// Direkt-Kommandos sofort zur�cksetzen
		BrbClearDirectBox( (UDINT)&gPrintMarkSensor.DirectBox, sizeof(gPrintMarkSensor.DirectBox) );
	}
	
		
	// ---------- StepHandling ----------
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	
	// ---------- Schrittkette ----------
	switch(Step.eStepNr)
	{
		//=======================================================================================================================
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
	
			for(nForIdx = 0; nForIdx <= nDEV_INDEX_MAX; nForIdx++)															// Schieberegister f�r Position initialisieren
			{
				gFifoManTrSen[nForIdx].pList			= (UDINT)&gTriggerSensor.Par.PosBondDev[nForIdx].FifoPos[0];		// Adressen der Schieberegister �bergeben
				gFifoManTrSen[nForIdx].nEntryLength		= sizeof(gTriggerSensor.Par.PosBondDev[nForIdx].FifoPos[0]);		// Gr��e eines Eintrages
				gFifoManTrSen[nForIdx].nIndexMax		= nFIFO_SIZE_INDEX;													// Max. Index des Schieberegisters
				gFifoManTrSen[nForIdx].nEntryCount		= 0;																// Momentane Anzahl an g�ltigen Eintr�gen
			}
			
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			
			if(gParHandling.State.bInitDone == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gTriggerSensor.CallerBox, sizeof(gTriggerSensor.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//=======================================================================================================================
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			
			// Initialisierung starten
			if (gTriggerSensor.CallerBox.bInit == 1)
			{
				Step.eStepNr = eSTEP_INIT_TRIGGER;
			}
			
			// Bonding Device Parameter ermitteln
			if (gTriggerSensor.CallerBox.bGetBonDevPar == 1)
			{
				Step.eStepNr = eSTEP_GET_BONDEV_PAR;
			}
			
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Initialisierung
		case eSTEP_INIT_TRIGGER:
			strcpy(Step.sStepText, "eSTEP_INIT_TRIGGER");
			
			brsmemset( (UDINT)&BonDevPar, 0x00, sizeof( BonDevPar ) );
			brsmemset( (UDINT)&IgnoreWrongTrigger, 0, sizeof(IgnoreWrongTrigger) );
			
			for (nForIdx = 0; nForIdx <= gPar.MachinePar.nTriggerSenIndex; nForIdx++)	
			{	
				// Fifo abl�schen Trigger Sensor
				BrbMemListClear((BrbMemListManagement_Typ*)&gFifoManTrSen[nForIdx]);
			}
			
			Step.eStepNr = eSTEP_GET_BONDEV_PAR;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		// Bonding Device Parameter ermitteln
		case eSTEP_GET_BONDEV_PAR:
			strcpy(Step.sStepText, "eSTEP_GET_BONDEV_PAR");		
			
			// Bonding Device Parameter ermitteln
			for (nForIdx = 0; nForIdx <= gPar.MachinePar.nBonDevIndex; nForIdx++)	
			{	
				nIdxSenSource = gPar.ProductPar.BonDevPar[nForIdx].nIdxSenSource;
				
				if( (gPar.ProductPar.BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR) &&
					(gPar.ProductPar.BonDevPar[nForIdx].bBonDevEn == TRUE) &&
					(BonDevPar[nIdxSenSource].bEnTrSen == FALSE) )
				{
					BonDevPar[nIdxSenSource].nEncoderIdx = gPar.ProductPar.BonDevPar[nForIdx].nIdxEncoder;
					BonDevPar[nIdxSenSource].bEnTrSen = TRUE;
					BonDevPar[nIdxSenSource].nProductLen = (DINT)(gPar.ProductPar.BonDevPar[nForIdx].rProductLen * rFACTOR_MM_TO_AXIS_UNITS); 	// Produktl�nge
					BonDevPar[nIdxSenSource].nBonDevIdx = nForIdx;
					BonDevPar[nIdxSenSource].eIdxSenType = gPar.ProductPar.BonDevPar[nForIdx].eIdxSenType;
					BonDevPar[nIdxSenSource].nSensorOffset = (DINT)(gPar.ProductPar.BonDevPar[nForIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS);
					// Flankenz�hler synchronisieren
					nOldEdgeRisingCnt[nIdxSenSource] = gTriggerSensor.Par.SensorTS[nIdxSenSource].nRisingCnt;
					nOldEdgeFallingCnt[nIdxSenSource] = gTriggerSensor.Par.SensorTS[nIdxSenSource].nFallingCnt;
				}
				else if( (gPar.ProductPar.BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) &&
						 (gPar.ProductPar.BonDevPar[nForIdx].bBonDevEn == TRUE) &&
						 (BonDevPar[nIdxSenSource].bEnTrSen == FALSE ) )
				{
					BonDevPar[nIdxSenSource].nEncoderIdx = gPar.ProductPar.BonDevPar[nForIdx].nIdxEncoder;
					BonDevPar[nIdxSenSource].bEnTrSen = TRUE;
					BonDevPar[nIdxSenSource].nBonDevIdx = nForIdx;
					BonDevPar[nIdxSenSource].nIdxPrintMarkSensorSource = gPar.ProductPar.BonDevPar[nForIdx].nIdxPrintMarkSensorSource;
					BonDevPar[nIdxSenSource].nProductLen = (DINT)(gPar.ProductPar.BonDevPar[nForIdx].rProductLen * rFACTOR_MM_TO_AXIS_UNITS); 	// Produktl�nge
					BonDevPar[nIdxSenSource].eIdxSenType = gPar.ProductPar.BonDevPar[nForIdx].eIdxSenType;
					BonDevPar[nIdxSenSource].nSensorOffset = (DINT)(gPar.ProductPar.BonDevPar[nForIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS);
					BonDevPar[nIdxSenSource].nDistancePrintMarkTriggerSensor = (DINT)(gPar.ProductPar.BonDevPar[nForIdx].rDistancePrintMarkTriggerSensor * rFACTOR_MM_TO_AXIS_UNITS);
					// Flankenz�hler synchronisieren
					nOldEdgeRisingCnt[nIdxSenSource]	= gTriggerSensor.Par.SensorTS[nIdxSenSource].nRisingCnt;
					nOldEdgeFallingCnt[nIdxSenSource] = gTriggerSensor.Par.SensorTS[nIdxSenSource].nFallingCnt;
				}
			}
			
			gTriggerSensor.State.bTriggerSenReady = 1;
			BrbClearCallerBox((UDINT)&gTriggerSensor.CallerBox, sizeof(gTriggerSensor.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;
	}
	
	// ---------- Zeitstempel Aufzeichnung aktiv ----------
	// Schleife �ber alle konfigurierten Triggersensoren
	for (nForIdx = 0; nForIdx <= gPar.MachinePar.nTriggerSenIndex; nForIdx++)	
	{
		// Parameter sind g�ltig UND Automatikbetrieb ist aktiv
		if ((gParHandling.State.bMParValid == 1) &&
			(gMainLogic.State.bAutomatic == 1))
		{
			if (gTriggerSensor.State.bTriggerSenReady == 1)
			{
				// Nur Ausf�hren, wenn Sensor benutzt wird
				if( BonDevPar[nForIdx].bEnTrSen == TRUE )
				{
					// Auswertung bei positiver Flanke
					if( gPar.ProductPar.TriggerSenPar.bPosNegDetection[nForIdx] == FALSE )
					{
						// Eine neue positive Flanke eines Trigger-Signals wurde erkannt
						if( gTriggerSensor.Par.SensorTS[nForIdx].nRisingCnt != nOldEdgeRisingCnt[nForIdx] )
						{
							// Anzahl der positiven Flanken merken
							nOldEdgeRisingCnt[nForIdx] 	= gTriggerSensor.Par.SensorTS[nForIdx].nRisingCnt;
							// Zeitdifferenz bilden, die seit dem Erfassen des Trigger-Singals vergangen ist
							nDiffTime[nForIdx] = (INT)gSystem.State.nSystemTime - gTriggerSensor.Par.SensorTS[nForIdx].nTsRising;
							// Geber-Position ermitteln, die im Moment des Triggersignals aktuelle war
							nTriggerPosition[nForIdx] = gEncoder.State.nAxisPos[BonDevPar[nForIdx].nEncoderIdx] - (DINT)((gEncoder.State.rVelocity[BonDevPar[nForIdx].nEncoderIdx] * (REAL)nDiffTime[nForIdx]) / nFACTOR_VELOCITY);
							
							// Je nach Betriebsart des Trigger-Sensors, die gelatchte Position ermitteln und in den entsprechenden Fifo schreiben						
							GetTriggeredPositionAndWriteIntoFifo( nTriggerPosition[nForIdx] );
						}
					}
					// Auswertung bei negativer Flanke
					else
					{
						// Eine neue negative Flanke eines Trigger-Signals wurde erkannt
						if (gTriggerSensor.Par.SensorTS[nForIdx].nFallingCnt != nOldEdgeFallingCnt[nForIdx])
						{
							// Anzahl der negativen Flanken merken
							nOldEdgeFallingCnt[nForIdx] = gTriggerSensor.Par.SensorTS[nForIdx].nFallingCnt;
							// Zeitdifferenz bilden, die seit dem Erfassen des Trigger-Singals vergangen ist
							nDiffTime[nForIdx] = (INT)gSystem.State.nSystemTime - gTriggerSensor.Par.SensorTS[nForIdx].nTsFalling;
							// Geber-Position ermitteln, die im Moment des Triggersignals aktuelle war
							nTriggerPosition[nForIdx] = gEncoder.State.nAxisPos[BonDevPar[nForIdx].nEncoderIdx] - (DINT)((gEncoder.State.rVelocity[BonDevPar[nForIdx].nEncoderIdx] * (REAL)nDiffTime[nForIdx]) / nFACTOR_VELOCITY);
							
							// Je nach Betriebsart des Trigger-Sensors, die gelatchte Position ermitteln und in den entsprechenden Fifo schreiben
							GetTriggeredPositionAndWriteIntoFifo( nTriggerPosition[nForIdx] );
						}			
					}
				}
				else
				{
					nOldEdgeRisingCnt[nForIdx] 	= gTriggerSensor.Par.SensorTS[nForIdx].nRisingCnt;
					nOldEdgeFallingCnt[nForIdx] = gTriggerSensor.Par.SensorTS[nForIdx].nFallingCnt;				
				}	
			}
			else
			{
				nOldEdgeRisingCnt[nForIdx] 	= gTriggerSensor.Par.SensorTS[nForIdx].nRisingCnt;
				nOldEdgeFallingCnt[nForIdx] = gTriggerSensor.Par.SensorTS[nForIdx].nFallingCnt;		
			}
		}
		else
		{
			nOldEdgeRisingCnt[nForIdx] 	= gTriggerSensor.Par.SensorTS[nForIdx].nRisingCnt;
			nOldEdgeFallingCnt[nForIdx] = gTriggerSensor.Par.SensorTS[nForIdx].nFallingCnt;
			gTriggerSensor.State.bTriggerSenReady = 0;
		}
		
		// Bei Betriebsart "PrintMark + Trigger" den Fifo nach ung�ltigen Eintr�gen durchsuchen
		if( BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
		{
			// Schleife �ber alle Eintr�ge im Druckmarken-Fifo
			for( i = 0; i < gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource].nEntryCount; i++ )
			{
				// Auslesen des jeweiligen Eintrag des Druckmarken-Fifos
				BrbMemListGetEntry( (BrbMemListManagement_Typ*)&gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource], (UDINT)i, (UDINT)&FifoPrintMarkTriggerPos[nForIdx] );
				
				// Dieser Eintrag des Fifos wurde noch keinem Produkt, welches der Trigger-Sensor erkannt hat, zugeordnet							
				if( FifoPrintMarkTriggerPos[nForIdx].bProductPositionSetByTrigger == FALSE )
				{
					// Hilfvariablen bef�llen (Zur INFO: Durch die Geschwindigkeitskompensation kann nDistanceProductMoved auch negativ sein)
					nPositionProductBegins = FifoPrintMarkTriggerPos[nForIdx].nPositionProductBegins;
					nDistanceProductMoved = gEncoder.State.nAxisPos[BonDevPar[nForIdx].nEncoderIdx] - nPositionProductBegins;
					nDistanceProductHasToMove = BonDevPar[nForIdx].nDistancePrintMarkTriggerSensor;
			
					// Das Produkt muss am Triggersensor vorbei sein, der Trigger-Sensor hat dieses aber nicht erkannt (verz�gert durch h�ngen bleiben bzw. entnehmen vom Laufband)
					if( nDistanceProductMoved > nDistanceProductHasToMove + BonDevPar[nForIdx].nProductLen / 2 )
					{
						// Da dieses Produkt vom Trigger-Sensor nicht erkannt bzw. nicht zugeordnet werden konnte, muss es aus dem Fifo entfernt werden.
						// Ansonsten w�rde der Task "BondingDev" sich an diesem Eintrag "aufh�ngen" und keine weiteren Klebeauftr�ge bearbeiten.
						BrbMemListOut( (BrbMemListManagement_Typ*)&gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource], i, (UDINT)&FifoPrintMarkTriggerPos[nForIdx]);
												
						
						if( i < gBondingDev.State.nEntryCnt[BonDevPar[nForIdx].nBonDevIdx] - 1 )
						{
							// Diese Funktion dekrementiert die EntryCnts der globalen Struktur gBondingDev.State
							DeleteEntryCntsInBondingDevState( BonDevPar[nForIdx].nBonDevIdx );
						}
					}
				}	
			}
		}
		
		// ---------- Wenn Position im Fifo den Auftragekopf passiert, den Positionswert aus Fifo l�schen ----------
		if( ((BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR) &&
			 (gFifoManTrSen[nForIdx].nEntryCount > 0)) ||
			
			((BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) &&
			 (gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource].nEntryCount > 0) &&
			 (nTriggerSignalEntryCount[nForIdx] > 0)) )
		{
			// ------------------------------------------------------------
			// Positionen des ersten Fifo-Eintrages (des �ltesten) auslesen
			// ------------------------------------------------------------
			if( BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR )
			{
				BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManTrSen[nForIdx], 0, (UDINT)&FirstPosToBond[nForIdx]);
				nFirstTriggerPosition[nForIdx] = FirstPosToBond[nForIdx].nPosToBond;
			}
			else if( BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
			{
				BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource], 0, (UDINT)&FirstPrintMarkTriggerPos[nForIdx]);
				nFirstTriggerPosition[nForIdx] = FirstPrintMarkTriggerPos[nForIdx].nPositionProductBegins;
			}
			
			// Die Position des �ltesten Trigger-Eintrags im Fifo ist von der aktuellen Position des Gebers mindestens nMAX_SENSOR_DISTANCE (maximale Sensordistanz) entfernt.
			if( (gEncoder.State.nAxisPos[BonDevPar[nForIdx].nEncoderIdx] - nFirstTriggerPosition[nForIdx]) > ((DINT)nMAX_SENSOR_DISTANCE) )
			{
				if( BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR )	
				{
					BrbFifoOut((BrbMemListManagement_Typ*)&gFifoManTrSen[nForIdx], (UDINT)&FirstPosToBond[nForIdx]);
				}
				else if( BonDevPar[nForIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
				{
					BrbFifoOut((BrbMemListManagement_Typ*)&gFifoManPmSen[BonDevPar[nForIdx].nIdxPrintMarkSensorSource], (UDINT)&FirstPosToBond[nForIdx]);
					nTriggerSignalEntryCount[nForIdx] -= 1;
				}
				
				// ---------------------------------------------------------------------------------------------------------------------
				// Die EntryCnts der globalen Struktur gBondingDev.State der Auftragek�pfe dekrementieren, die auf diesen Sensor schauen
				// ---------------------------------------------------------------------------------------------------------------------
				// Schleife �ber alle konfigurierten Auftragek�pfe
				for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
				{
					if ((gPar.ProductPar.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR) ||
						(gPar.ProductPar.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR))
					{
						if ((gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource == nForIdx) &&
							(gPar.ProductPar.BonDevPar[nDevIndex].bBonDevEn == TRUE))
						{
							// Diese Funktion dekrementiert die EntryCnts der globalen Struktur gBondingDev.State
							DeleteEntryCntsInBondingDevState( nDevIndex );
						}
					}
				}
			}
		}	
	}
}

void _EXIT TriggerSensorEXIT(void)
{
}
