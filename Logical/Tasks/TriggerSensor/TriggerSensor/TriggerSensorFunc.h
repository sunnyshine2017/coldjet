/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: TriggerSensor
 * File: TriggerSensorFunc.h
 * Author: kusculart
 * Created: Oktober 02, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#include <string.h>
#include <Global.h>

void GetTriggeredPositionAndWriteIntoFifo( DINT nTriggerPosition );
void DeleteEntryCntsInBondingDevState( USINT nBonDevIndex );
