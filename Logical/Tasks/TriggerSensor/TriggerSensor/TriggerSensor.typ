(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: TriggerSensor
 * File: TriggerSensor.typ
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Local data types of program TriggerSensor
 ********************************************************************)

TYPE
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_INIT_TRIGGER,
		eSTEP_GET_BONDEV_PAR
		);
	TriggerSensorStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	BonDevParForTriggerSen_TYP : 	STRUCT 
		bEnTrSen : BOOL; (*Trigger Sensor Freigabe*)
		nBonDevIdx : SINT; (*Index f�r Bonding Device *)
		nEncoderIdx : SINT; (*Geberindex *)
		nIdxPrintMarkSensorSource : SINT; (*Bei PrintMark_Trigger: Index des verkn�pften Druckmarkensonsors*)
		eIdxSenType : SensorType_ENUM; (*Betriebsart des Triggersensors*)
		nProductLen : DINT; (*Produktl�nge aus der Par-Struktur in [1/100 mm] (Achseinheiten)*)
		nDistancePrintMarkTriggerSensor : REAL; (*Bei PrintMark_Trigger: Abstand in [1/100 mm] vom Druckmarken-Sensor bis zum Triggersensor*)
		nSensorOffset : DINT;
	END_STRUCT;
	IgnoreWrongTrigger_type : 	STRUCT 
		nDiffLen : DINT; (*Strecke zwische Produktbeginn und akt. Achsposition*)
		nStartPosPro : DINT; (*Startposition vom Produkt*)
		bInit : BOOL;
	END_STRUCT;
END_TYPE
