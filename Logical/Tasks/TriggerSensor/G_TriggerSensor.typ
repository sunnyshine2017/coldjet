(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: TriggerSensor
 * File: G_TriggerSensor.typ
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Global  data types of package TriggerSensor
 ********************************************************************)

TYPE
	TriggerSensorCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bInit : BOOL; (*Initialisierung*)
		bGetBonDevPar : BOOL; (*Bonding Device Parameter ermitteln*)
	END_STRUCT;
	TriggerSensorDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bUpdateBonDevPar : BOOL; (*Trigger-Parameter der Klebek�pfe updaten, da sich diese in der Visu ge�ndert haben (�bernahmen w�hrend Automatik aktiv)*)
	END_STRUCT;
	TriggerSensorPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		PosBondDev : ARRAY[0..nDEV_INDEX_MAX]OF TriggerFifo_TYP; (*Positionen f�r den Klebeauftrag*)
		SensorTS : ARRAY[0..nDEV_INDEX_MAX]OF Timestamp_TYP; (*Zeitstempel Sensor*)
	END_STRUCT;
	TriggerSensorState_TYP : 	STRUCT  (*R�ckmeldungen*)
		bTriggerSenReady : BOOL; (*Triggersensoren bereit*)
	END_STRUCT;
	TriggerSensor_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : TriggerSensorCallerBox_TYP;
		DirectBox : TriggerSensorDirectBox_TYP;
		Par : TriggerSensorPar_TYP;
		State : TriggerSensorState_TYP;
	END_STRUCT;
	TriggerFifo_TYP : 	STRUCT 
		FifoPos : ARRAY[0..nFIFO_SIZE_INDEX]OF PosToBond_TYP;
	END_STRUCT;
	Timestamp_TYP : 	STRUCT 
		nTsRising : INT; (*Zeitstempel steigende Flanke*)
		nRisingCnt : SINT; (*Z�hler steigende Flanke*)
		nTsFalling : INT; (*Zeitstempel fallende Flanke*)
		nFallingCnt : SINT; (*Z�hler fallende Flanke*)
	END_STRUCT;
END_TYPE
