(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: System
 * File: System.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Local data types of program System
 ********************************************************************)

TYPE
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_GET_ETH_SETTINGS := 200,
		eSTEP_GES_NEXT_INTERFACE,
		eSTEP_GES_SUBNET_MASK,
		eSTEP_GES_IP_ADRESS,
		eSTEP_GES_FINISHED,
		eSTEP_SET_ETH_SETTINGS := 300,
		eSTEP_SES_CLEAR_NV,
		eSTEP_SES_NEXT_INTERFACE,
		eSTEP_SES_IP_ADRESS,
		eSTEP_SES_SUBNET_MASK,
		eSTEP_SES_FINISHED
		);
	SystemStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
END_TYPE
