/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: System
 * File: System.c
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Implementation of program System
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <SystemFunc.h>

void _INIT SystemINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gSystem, 0, sizeof(gSystem));
	gSystem.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;
	#ifdef HW_ARSIM
		// Onboard
		gSystem.State.EthInterface[0].bExists = 1;
		strcpy(gSystem.State.EthInterface[0].sDevice, "IF3");
		gSystem.State.EthInterface[0].nVisDescriptionIndex = VIS_ETH_DESC_INDEX_ONBOARD;
		gSystem.State.EthInterface[0].bChangeable = 0;
	#endif
	#ifdef HW_C70
		// Onboard
		gSystem.State.EthInterface[0].bExists = 1;
		strcpy(gSystem.State.EthInterface[0].sDevice, "IF2");
		gSystem.State.EthInterface[0].nVisDescriptionIndex = VIS_ETH_DESC_INDEX_ONBOARD;
		gSystem.State.EthInterface[0].bChangeable = 1;
	#endif
	#ifdef HW_CP1585
		// Onboard (Panel)
		gSystem.State.EthInterface[0].bExists = 1;
		strcpy(gSystem.State.EthInterface[0].sDevice, "IF2");
		gSystem.State.EthInterface[0].nVisDescriptionIndex = VIS_ETH_DESC_INDEX_PANEL;
		gSystem.State.EthInterface[0].bChangeable = 0;
		// Powerlink-Einsteckmodul (Maschinen-Kommunikation)
		gSystem.State.EthInterface[1].bExists = 1;
		strcpy(gSystem.State.EthInterface[1].sDevice, "SS1.IF1.ETH");
		gSystem.State.EthInterface[1].nVisDescriptionIndex = VIS_ETH_DESC_INDEX_COMMUNICATION;
		gSystem.State.EthInterface[1].bChangeable = 1;
		// Powerlink-Gateway (Remote-Zugang)
		gSystem.State.EthInterface[2].bExists = 1;
		strcpy(gSystem.State.EthInterface[2].sDevice, "IF3.ETH");
		gSystem.State.EthInterface[2].nVisDescriptionIndex = VIS_ETH_DESC_INDEX_REMOTE_ACCESS;
		gSystem.State.EthInterface[2].bChangeable = 1;
	#endif

	
	// ---------------------- Vorbereitungen zur Pr�fung ob T30-Bedienpanel vorhanden ist ----------------------
	// Ist die X20CP1585 mit einem T30-Bedienpanel als CPU gew�hlt, muss �berwacht werden ob das T30-Bedienpanel verbunden ist.
	// Hierzu wird die RfbExt-Funktionen genutzt und hier initialisisert.
	#ifdef HW_T30_PLUGGED
		// Initialisieren der RfbExt-Funktionen
	    nStatusRfbExtInit = RfbExtInit( (UDINT)&RfbExtLib, 500 );
	#else
		// "Warning 6424: Variable xxx is declared but not used in the current configuration" vermeiden
		nStatusRfbExtInit = 0;
		RfbExtLib.pClient = 0;
		nStatusRfbExtConnect = 0;
		nStatusRfbExtInit = 0;
		nDataAge = 0;
		nStatusRfbExtDataAge = 0;
		bEventSetHmiPanelNotConnected = 0;
	#endif
	
	// Initialisierung Task
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
	
	gCalcPos = gCalcPos;
}

void _CYCLIC SystemCYCLIC(void)
{
	// Direct-Kommandos
	if(Step.bInitDone == 1)
	{
		if(gSystem.DirectBox.bGetVersions == 1)
		{
			// Die Version des Projekts wird aus dem Task "System" ausgelesen.
			nGetVersionStatus =	MO_ver((STRING*)&"System", 0, &ProjectInfo);
			if(nGetVersionStatus == 0)
			{
				strcpy((STRING*)&gSystem.State.sProjectVersion, (STRING*)&ProjectInfo.version);
				RTCtime_typ Timestamp;
				brsmemset((UDINT)&Timestamp, 0, sizeof(Timestamp));
				Timestamp.year = ProjectInfo.year;
				Timestamp.month = ProjectInfo.month;
				Timestamp.day = ProjectInfo.day;
				Timestamp.hour = ProjectInfo.hour;
				Timestamp.minute = ProjectInfo.minute;
				Timestamp.second = ProjectInfo.second;
				brsmemset((UDINT)&gSystem.State.sProjectTimestamp, 0, sizeof(gSystem.State.sProjectTimestamp));
				BrbGetTimeText(&Timestamp, (STRING*)&gSystem.State.sProjectTimestamp, sizeof(gSystem.State.sProjectTimestamp), "dd.mm.yyyy   hh:MM:ss");
			}
			fbTargetInfo.enable			= 1;
			fbTargetInfo.pOSVersion	= (UDINT)&gSystem.State.sArVersion;
			TARGETInfo(&fbTargetInfo);
			// Die Vc-Version wird aus dem Modul "vccbtn" ausgelesen.
			nGetVersionStatus =	MO_ver((STRING*)&"vccbtn", 0, &VcInfo);
			if(nGetVersionStatus == 0)
			{
				strcpy((STRING*)&gSystem.State.sVcVersion, (STRING*)&VcInfo.version);
			}
			// Die Mc-Version wird aus dem Modul "acp10man" ausgelesen.
			nGetVersionStatus =	MO_ver((STRING*)&"acp10man", 0, &McInfo);
			if(nGetVersionStatus == 0)
			{
				strcpy((STRING*)&gSystem.State.sMcVersion, (STRING*)&McInfo.version);
			}
			BrbClearDirectBox((UDINT)&gSystem.DirectBox, sizeof(gSystem.DirectBox));
		}
		if(gSystem.DirectBox.bSetSystemTime == 1)
		{
			fbDTStructureSetTime.enable = 1;
			fbDTStructureSetTime.pDTStructure = (UDINT)&gSystem.Par.dtSystemTime;
			DTStructureSetTime(&fbDTStructureSetTime);
			if(fbDTStructureSetTime.status != 65535)
			{
				BrbClearDirectBox((UDINT)&gSystem.DirectBox, sizeof(gSystem.DirectBox));
			}
		}
	}
	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			//if(gParHandling.State.bParValid == 1)
			{					
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;
		
		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			Step.bInitDone = 1;
			// Holen der Versionen
			gSystem.DirectBox.bGetVersions = 1;
			Step.eStepNr = eSTEP_GET_ETH_SETTINGS;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			gSystem.State.bGettingOrSettingEth = 0;
			if(gSystem.CallerBox.bGetEthSettings == 1)
			{
				gSystem.State.bGettingOrSettingEth = 1;
				Step.eStepNr = eSTEP_GET_ETH_SETTINGS;
			}
			if(gSystem.CallerBox.bSetEthSettings == 1)
			{
				gSystem.State.bGettingOrSettingEth = 1;
				Step.eStepNr = eSTEP_SET_ETH_SETTINGS;
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_GET_ETH_SETTINGS:
			strcpy(Step.sStepText, "eSTEP_GET_ETH_SETTINGS");
			brsmemset((UDINT)&fbCfgGetIPAddr, 0, sizeof(fbCfgGetIPAddr));
			brsmemset((UDINT)&fbCfgGetSubnetMask, 0, sizeof(fbCfgGetSubnetMask));
			brsmemset((UDINT)&gSystem.Par.EthInterface, 0, sizeof(gSystem.Par.EthInterface));
			nEthInterfaceIndex = -1;
			Step.eStepNr = eSTEP_GES_NEXT_INTERFACE;
			break;

		case eSTEP_GES_NEXT_INTERFACE:
			strcpy(Step.sStepText, "eSTEP_GES_NEXT_INTERFACE");
			if(nEthInterfaceIndex < nSYSTEM_ETH_INTERFACE_INDEX_MAX)
			{
				nEthInterfaceIndex += 1;
				if(gSystem.State.EthInterface[nEthInterfaceIndex].bExists == 1)
				{
					Step.eStepNr = eSTEP_GES_SUBNET_MASK;
				}
			}
			else
			{
				Step.eStepNr = eSTEP_GES_FINISHED;
			}
			break;

		case eSTEP_GES_SUBNET_MASK:
			strcpy(Step.sStepText, "eSTEP_GES_SUBNET_MASK");
			fbCfgGetSubnetMask.enable = 1;
			fbCfgGetSubnetMask.pDevice = (UDINT)&gSystem.State.EthInterface[nEthInterfaceIndex].sDevice;
			fbCfgGetSubnetMask.pSubnetMask = (UDINT)&gSystem.Par.EthInterface[nEthInterfaceIndex].sSubnetMask;
			fbCfgGetSubnetMask.Len = sizeof(gSystem.Par.EthInterface[nEthInterfaceIndex].sSubnetMask);
			CfgGetSubnetMask(&fbCfgGetSubnetMask);
			if(fbCfgGetSubnetMask.status == 0)
			{
				Step.eStepNr = eSTEP_GES_IP_ADRESS;
			}
			else if(fbCfgGetSubnetMask.status != 65535)
			{
				STRING sDetail[nAPP_EVENT_DETAIL_CHAR_MAX];
				memset(sDetail, 0, sizeof(sDetail));
				strcat(sDetail, "SubnetMask ");
				strcat(sDetail, gSystem.State.EthInterface[nEthInterfaceIndex].sDevice);
				AppSetEvent(eEVT_ERR_GET_IP_SETTINGS, fbCfgGetSubnetMask.status, sDetail, 0, &gEventManagement);
				AppResetEvent(eEVT_ERR_GET_IP_SETTINGS, &gEventManagement);
				Step.eStepNr = eSTEP_GES_IP_ADRESS;
			}
			break;

		case eSTEP_GES_IP_ADRESS:
			strcpy(Step.sStepText, "eSTEP_GES_IP_ADRESS");
			fbCfgGetIPAddr.enable = 1;
			fbCfgGetIPAddr.pDevice = (UDINT)&gSystem.State.EthInterface[nEthInterfaceIndex].sDevice;
			fbCfgGetIPAddr.pIPAddr = (UDINT)&gSystem.Par.EthInterface[nEthInterfaceIndex].sIpAdress;
			fbCfgGetIPAddr.Len = sizeof(gSystem.Par.EthInterface[nEthInterfaceIndex].sIpAdress);
			CfgGetIPAddr(&fbCfgGetIPAddr);
			if(fbCfgGetIPAddr.status == 0)
			{
				Step.eStepNr = eSTEP_GES_NEXT_INTERFACE;
			}
			else if(fbCfgGetIPAddr.status != 65535)
			{
				STRING sDetail[nAPP_EVENT_DETAIL_CHAR_MAX];
				memset(sDetail, 0, sizeof(sDetail));
				strcat(sDetail, "IpAddress ");
				strcat(sDetail, gSystem.State.EthInterface[nEthInterfaceIndex].sDevice);
				AppSetEvent(eEVT_ERR_GET_IP_SETTINGS, fbCfgGetIPAddr.status, sDetail, 0, &gEventManagement);
				AppResetEvent(eEVT_ERR_GET_IP_SETTINGS, &gEventManagement);
				Step.eStepNr = eSTEP_GES_NEXT_INTERFACE;
			}
			break;

		case eSTEP_GES_FINISHED:
			strcpy(Step.sStepText, "eSTEP_GES_FINISHED");
			BrbClearCallerBox((UDINT)&gSystem.CallerBox, sizeof(gSystem.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_SET_ETH_SETTINGS:
			strcpy(Step.sStepText, "eSTEP_SET_ETH_SETTINGS");
			brsmemset((UDINT)&fbCfgSetIPAddr, 0, sizeof(fbCfgSetIPAddr));
			brsmemset((UDINT)&fbCfgSetSubnetMask, 0, sizeof(fbCfgSetSubnetMask));
			for(nEthInterfaceIndex=0; nEthInterfaceIndex<=nSYSTEM_ETH_INTERFACE_INDEX_MAX; nEthInterfaceIndex++)
			{
				BrbCheckIpAddress(gSystem.Par.EthInterface[nEthInterfaceIndex].sIpAdress);
				BrbCheckIpAddress(gSystem.Par.EthInterface[nEthInterfaceIndex].sSubnetMask);
			}
			nEthInterfaceIndex = -1;
			Step.eStepNr = eSTEP_SES_CLEAR_NV;
			break;

		case eSTEP_SES_CLEAR_NV:
			strcpy(Step.sStepText, "eSTEP_SES_CLEAR_NV");
			// L�schen der nichtfl�chtigen Einstellungen
			nStatusCfgClearNV = CfgClearNV();
			if(nStatusCfgClearNV == 0)
			{
				Step.eStepNr = eSTEP_SES_NEXT_INTERFACE;
			}
			else
			{
				AppSetEvent(eEVT_ERR_SET_IP_SETTINGS, nStatusCfgClearNV, "NonVolatile", 0, &gEventManagement);
				AppResetEvent(eEVT_ERR_SET_IP_SETTINGS, &gEventManagement);
				Step.eStepNr = eSTEP_GET_ETH_SETTINGS;
			}
		break;

		case eSTEP_SES_NEXT_INTERFACE:
			strcpy(Step.sStepText, "eSTEP_SES_NEXT_INTERFACE");
			if(nEthInterfaceIndex < nSYSTEM_ETH_INTERFACE_INDEX_MAX)
			{
				nEthInterfaceIndex += 1;
				if(gSystem.State.EthInterface[nEthInterfaceIndex].bExists == 1 && gSystem.State.EthInterface[nEthInterfaceIndex].bChangeable == 1)
				{
					Step.eStepNr = eSTEP_SES_SUBNET_MASK;
				}
			}
			else
			{
				Step.eStepNr = eSTEP_SES_FINISHED;
			}
			break;

		case eSTEP_SES_SUBNET_MASK:
			strcpy(Step.sStepText, "eSTEP_SES_SUBNET_MASK");
			fbCfgSetSubnetMask.enable = 1;
			fbCfgSetSubnetMask.pDevice = (UDINT)&gSystem.State.EthInterface[nEthInterfaceIndex].sDevice;
			fbCfgSetSubnetMask.pSubnetMask = (UDINT)&gSystem.Par.EthInterface[nEthInterfaceIndex].sSubnetMask;
			fbCfgSetSubnetMask.Option = cfgOPTION_NON_VOLATILE;
			CfgSetSubnetMask(&fbCfgSetSubnetMask);
			if(fbCfgSetSubnetMask.status == 0)
			{
				Step.eStepNr = eSTEP_SES_IP_ADRESS;
			}
			else if(fbCfgSetSubnetMask.status != ERR_FUB_BUSY)
			{
				STRING sDetail[nAPP_EVENT_DETAIL_CHAR_MAX];
				memset(sDetail, 0, sizeof(sDetail));
				strcat(sDetail, "SubnetMask ");
				strcat(sDetail, gSystem.State.EthInterface[nEthInterfaceIndex].sDevice);
				AppSetEvent(eEVT_ERR_SET_IP_SETTINGS, fbCfgSetSubnetMask.status, sDetail, 0, &gEventManagement);
				AppResetEvent(eEVT_ERR_SET_IP_SETTINGS, &gEventManagement);
				Step.eStepNr = eSTEP_SES_IP_ADRESS;
			}
			break;

		case eSTEP_SES_IP_ADRESS:
			strcpy(Step.sStepText, "eSTEP_SES_IP_ADRESS");
			fbCfgSetIPAddr.enable = 1;
			fbCfgSetIPAddr.pDevice = (UDINT)&gSystem.State.EthInterface[nEthInterfaceIndex].sDevice;
			fbCfgSetIPAddr.pIPAddr = (UDINT)&gSystem.Par.EthInterface[nEthInterfaceIndex].sIpAdress;
			fbCfgSetIPAddr.Option = cfgOPTION_NON_VOLATILE;
			CfgSetIPAddr(&fbCfgSetIPAddr);
			if(fbCfgSetIPAddr.status == 0)
			{
				Step.eStepNr = eSTEP_SES_NEXT_INTERFACE;
			}
			else if(fbCfgSetIPAddr.status != ERR_FUB_BUSY)
			{
				STRING sDetail[nAPP_EVENT_DETAIL_CHAR_MAX];
				memset(sDetail, 0, sizeof(sDetail));
				strcat(sDetail, "IpAddress ");
				strcat(sDetail, gSystem.State.EthInterface[nEthInterfaceIndex].sDevice);
				AppSetEvent(eEVT_ERR_SET_IP_SETTINGS, fbCfgSetIPAddr.status, sDetail, 0, &gEventManagement);
				AppResetEvent(eEVT_ERR_SET_IP_SETTINGS, &gEventManagement);
				Step.eStepNr = eSTEP_SES_NEXT_INTERFACE;
			}
			break;

		case eSTEP_SES_FINISHED:
			strcpy(Step.sStepText, "eSTEP_SES_FINISHED");
			Step.eStepNr = eSTEP_GET_ETH_SETTINGS;
			break;

	}

	// ---------- Systemzeit auslesen ----------
	// Der Status "gSystem.State.nSystemTime" ist am Datenpunkt "SystemTime" der SPS gemappt.
	// In der ArSim gibt es diesen Datenpunkt nicht. Er wird deshalb im Task "Encoder" in TC#1 ermittelt.
	
	// Auslesen der totalen Systemzeit
	fbDTStructureGetTime.enable = 1;
	fbDTStructureGetTime.pDTStructure = (UDINT)&gSystem.State.dtSystemTime;
	DTStructureGetTime(&fbDTStructureGetTime);
	
	
	// -------------- Pr�fen ob T30-Bedienpanel angesteckt ist -----------
	#ifdef HW_T30_PLUGGED
		// Eine Verbindung zu der Visualisierung welche auf dem VNC-Server l�uft wurde noch nicht aufgebaut
		if( RfbExtLib.pClient == 0x00 )
		{
			// Verbindung zur Visualisierung "VisWvga" aufbauen
			nStatusRfbExtConnect = RfbExtConnect( (UDINT)&RfbExtLib,  (UDINT)"VisWvg");	// Die Namen der Visualisierungen k�nnen auf der CPU nur 6 Zeichen lang sein.
																						// Daher wird VisWvga auf VisWvg abgeschnitten!!!
			// Verbindung wurde erfolgreich aufgebaut
			if( nStatusRfbExtConnect == ERR_OK )
			{
			
			}
			else
			{
				
			}
		}
		// Eine Verbindung zum VNC-Server wurde erfolgreich aufgebaut
		else
		{
			// Pr�fen ob die VNC-Daten des T30-Bedienpanels �lter als 5 Sekunden sind
			nStatusRfbExtDataAge = RfbExtDataAge( &RfbExtLib, (UDINT)&nDataAge );
				
			// Funktion hat erfolgreich das alter der Daten ermittelt
			if( nStatusRfbExtDataAge == ERR_OK )
			{
				// Die RfbExt-Daten sind �lter als 5 Sekunden (5000 ms)
				if( nDataAge > 5000 )
				{
					// Befindet sich das Klebesystem im Betriebsmodus "Automatik", wird dieses gestoppt und eine
					// Fehlermeldung abgesetzt welche das Fehlen des T30-Bedienpanels anzeigt.
					if( gMainLogic.State.bAutomatic == 1 )
					{
						// Absetzen des Events "T30-Panel nicht mehr gesteckt"
						AppSetEvent( eEVT_ERR_HMI_PANEL_NOT_CONNECTED, 0, "", 0, &gEventManagement );
						// Flag setzen: Event "T30-Panel nicht mehr gesteckt" ist abgesetzt
						bEventSetHmiPanelNotConnected = 1;
						// Klebesystem stoppen
						gMainLogic.DirectBox.bStop = 1;
					}
				}
				// Das alter der RfbExt-Daten ist unter 5 Sekunden
				else
				{
					// Das Event "T30-Panel nicht mehr gesteckt" wurde abgesetzt
					if( bEventSetHmiPanelNotConnected == 1 )
					{
						// Event "T30-Panel nicht mehr gesteckt" zur�cksetzen
						AppResetEvent( eEVT_ERR_HMI_PANEL_NOT_CONNECTED, &gEventManagement );
						// Flag zur�cksetzen
						bEventSetHmiPanelNotConnected = 0;
					}
				}
			}
		}
	#endif
}

void _EXIT SystemEXIT(void)
{
}
