(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: System
 * File: G_System.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Global  data types of package System
 ********************************************************************)

TYPE
	SystemCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bGetEthSettings : BOOL;
		bSetEthSettings : BOOL;
	END_STRUCT;
	SystemDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bGetVersions : BOOL;
		bSetSystemTime : BOOL;
	END_STRUCT;
	SystemParEthInterface_TYP : 	STRUCT  (*Rückmeldungen*)
		sIpAdress : STRING[nBRB_IP_ADR_CHAR_MAX];
		sSubnetMask : STRING[nBRB_IP_ADR_CHAR_MAX];
	END_STRUCT;
	SystemPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		dtSystemTime : DTStructure;
		EthInterface : ARRAY[0..nSYSTEM_ETH_INTERFACE_INDEX_MAX]OF SystemParEthInterface_TYP;
	END_STRUCT;
	SystemStateEthInterface_TYP : 	STRUCT  (*Rückmeldungen*)
		bExists : BOOL;
		sDevice : STRING[64];
		nVisDescriptionIndex : UINT;
		bChangeable : BOOL;
	END_STRUCT;
	SystemState_TYP : 	STRUCT  (*Rückmeldungen*)
		nSystemTime : DINT; (*Systemzeit*)
		dtSystemTime : DTStructure;
		sProjectVersion : STRING[nSYSTEM_VERSION_CHAR_MAX];
		sProjectTimestamp : STRING[nBRB_TIME_TEXT_CHAR_MAX];
		sArVersion : STRING[nSYSTEM_VERSION_CHAR_MAX];
		sVcVersion : STRING[nSYSTEM_VERSION_CHAR_MAX];
		sMcVersion : STRING[nSYSTEM_VERSION_CHAR_MAX];
		EthInterface : ARRAY[0..nSYSTEM_ETH_INTERFACE_INDEX_MAX]OF SystemStateEthInterface_TYP;
		bGettingOrSettingEth : BOOL;
	END_STRUCT;
	System_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : SystemCallerBox_TYP;
		DirectBox : SystemDirectBox_TYP;
		Par : SystemPar_TYP;
		State : SystemState_TYP;
	END_STRUCT;
END_TYPE
