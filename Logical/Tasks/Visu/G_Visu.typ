(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: Visu
 * File: G_Visu.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Global  data types of package Visu
 ********************************************************************)

TYPE
	GlobalDataMsgPopup : 	STRUCT 
		ChangePage : INT;
		CurrentPage : INT;
	END_STRUCT;
	VisuCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bDummy : BOOL;
	END_STRUCT;
	VisuDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bShowDialogMoveAxes : BOOL;
		bShowDialogStartHeating : BOOL;
	END_STRUCT;
	VisuPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		bDummy : BOOL;
	END_STRUCT;
	VisuState_TYP : 	STRUCT  (*Rückmeldungen*)
		bDialogMoveAxesActive : BOOL; (*Dialog "MoveAxes" wird angezeigt*)
		bDialogStartHeatingActive : BOOL; (*Dialog "StartHeating" wird angezeigt*)
	END_STRUCT;
	Visu_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : VisuCallerBox_TYP;
		DirectBox : VisuDirectBox_TYP;
		Par : VisuPar_TYP;
		State : VisuState_TYP;
	END_STRUCT;
	VisuEthDescriptionIndices_ENUM : 
		(
		VIS_ETH_DESC_INDEX_ONBOARD := 0,
		VIS_ETH_DESC_INDEX_PANEL := 1,
		VIS_ETH_DESC_INDEX_COMMUNICATION := 2,
		VIS_ETH_DESC_INDEX_REMOTE_ACCESS := 3
		);
END_TYPE
