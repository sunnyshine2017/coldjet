(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Visu
 * File: Visu.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Local data types of program Visu
 ********************************************************************)

TYPE
	Pages_ENUM : 
		(
		ePAGE_TEMPLATE := 0,
		ePAGE_SCREENSAVER := 10,
		ePAGE_MAIN := 1000,
		ePAGE_HEAD := 1100,
		ePAGE_HEAD_COPY := 1101,
		ePAGE_HEAD_PAR := 1200,
		ePAGE_PRODUCTION := 2000,
		ePAGE_PRODUCTION_SAVE := 2010,
		ePAGE_PRODUCTION_DELETE := 2020,
		ePAGE_CONFIGURATION1 := 2101,
		ePAGE_CONFIGURATION2 := 2102,
		ePAGE_CONFIGURATION3 := 2103,
		ePAGE_CONFIGURATION4 := 2104,
		ePAGE_CONFIGURATION5 := 2105,
		ePAGE_CONFIGURATION6 := 2106,
		ePAGE_CONFIGURATION7 := 2107,
		ePAGE_CONFIGURATION8 := 2108,
		ePAGE_CONFIGURATION9 := 2109,
		ePAGE_CONFIGURATION10 := 2110,
		ePAGE_CONFIGURATION11 := 2111,
		ePAGE_CONFIGURATION12 := 2112,
		ePAGE_CONFIGURATION13 := 2113,
		ePAGE_CONFIGURATION14 := 2114,
		ePAGE_CONFIGURATION15 := 2115,
		ePAGE_CONFIGURATION16 := 2116,
		ePAGE_CONFIGURATION17 := 2117,
		ePAGE_CONFIG_PAR_TMPCTRL_ZONE := 2150,
		ePAGE_SERVICE1 := 2201,
		ePAGE_SERVICE2 := 2202,
		ePAGE_SERVICE3 := 2203,
		ePAGE_USER := 2300,
		ePAGE_MODULE_CONFIG := 2310,
		ePAGE_EVENTS := 2400,
		ePAGE_CONTACT := 2500,
		ePAGE_OVERVIEW := 3000,
		ePAGE_CLEANING := 3100,
		ePAGE_SYSTEM_CLEANING := 3101,
		ePAGE_PRESSURE_CONTROL := 3200,
		ePAGE_MOTORIZED_ADJUSTMENT0 := 3300,
		ePAGE_MOTORIZED_ADJUSTMENT1 := 3301,
		ePAGE_CAMERA := 3400,
		ePAGE_TEMPCTRL := 3500,
		ePAGE_ANALOGFUELLSTAND := 4000,
		ePAGE_DLG_MOVE_AXES := 10000,
		ePAGE_DLG_START_HEATING := 10010,
		ePAGE_MEDIENVERBRAUCH := 10011
		);
	Colors_ENUM : 
		(
		eBACKCOLOR_BLACK := 0,
		eBACKCOLOR_BLUE := 1,
		eBACKCOLOR_GREEN := 2,
		eBACKCOLOR_LIGHT_GREY := 7,
		eBACKCOLOR_LIGHT_BLUE := 9,
		eBACKCOLOR_LIGHT_GREEN := 10,
		eBACKCOLOR_WHITE := 15,
		eBACKCOLOR_RED := 45,
		eBACKCOLOR_YELLOW := 46,
		eBACKCOLOR_SILVER := 59,
		eBACKCOLOR_DARK_GREY := 248,
		eBACKCOLOR_MINT_GREEN := 106,
		eBACKCOLOR_PAGE := 249,
		eBACKCOLOR_INPUT := 96,
		eBACKCOLOR_EDIT := 100,
		eBACKCOLOR_PLANATOL_BLUE := 240,
		eBACKCOLOR_FOCUS := 133,
		eFORECOLOR_BLACK := 0, (*256*0*)
		eFORECOLOR_BLUE := 256, (*256*1*)
		eFORECOLOR_GREEN := 512, (*256*2*)
		eFORECOLOR_LIGHT_GREY := 1792, (*256*7*)
		eFORECOLOR_LIGHT_BLUE := 2304, (*256*9*)
		eFORECOLOR_LIGHT_GREEN := 2560, (*256*10*)
		eFORECOLOR_WHITE := 3840, (*256*15*)
		eFORECOLOR_RED := 11520, (*256*45*)
		eFORECOLOR_YELLOW := 11776, (*256*46*)
		eFORECOLOR_SILVER := 15104, (*256*59*)
		eFORECOLOR_DARK_GREY := 63488, (*256*248*)
		eFORECOLOR_MINT_GREEN := 27136, (*256*106*)
		eFORECOLOR_PAGE := 64512, (*256*252*)
		eFORECOLOR_INPUT := 24576, (*256*96*)
		eFORECOLOR_EDIT := 25600, (*256*100*)
		eFORECOLOR_FOCUS := 25856, (*256*101*)
		eFORECOLOR_PLANATOL_BLUE := 61440, (*256*240*)
		eCOLOR_SELECTED := 96, (*96+0*248*)
		eCOLOR_ENABLED := 15, (*15+256*0*)
		eCOLOR_DISABLED := 63737, (*249+256*248*)
		eCOLOR_MENU_UNSELECTED := 252, (*252+256*0*)
		eCOLOR_MENU_SELECTED := 7, (*7+256*0*)
		eCOLOR_INPUT := 15, (*15+256*0*)
		eCOLOR_OUTPUT := 249 (*249+256*0*)
		);
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_CMD1 := 200,
		eSTEP_CMD1_FINISHED
		);
	UserLevel_ENUM : 
		(
		eUSERLEVEL_OPERATOR,
		eUSERLEVEL_SERVICE,
		eUSERLEVEL_CONFIGURATOR
		);
	DropsBmpIndices_ENUM : 
		(
		eDROPS_BMP_INDEX_BORDER,
		eDROPS_BMP_INDEX_BLUE,
		eDROPS_BMP_INDEX_GREEN,
		eDROPS_BMP_INDEX_YELLOW,
		eDROPS_BMP_INDEX_RED,
		eDROPS_BMP_INDEX_BORDER_BIG,
		eDROPS_BMP_INDEX_BLUE_BIG,
		eDROPS_BMP_INDEX_GREEN_BIG,
		eDROPS_BMP_INDEX_YELLOW_BIG,
		eDROPS_BMP_INDEX_RED_BIG
		);
	VisuStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	VisGeneralOwn_TYP : 	STRUCT 
		nScreenWidth : UINT;
		nScreenHeight : UINT;
		nUserLevel : UINT;
		nUserLevelOld : UINT;
		bBlink400 : BOOL;
	END_STRUCT;
	VisHeader_TYP : 	STRUCT 
		bmpLogo : BrbVc4Bitmap_TYP;
		txtStepperSimulated : BrbVc4Text_TYP;
		sDateTime : STRING[nBRB_TIME_TEXT_CHAR_MAX];
		btnMenu : BrbVc4Button_TYP;
		btnHelp : BrbVc4Button_TYP;
		btnHome : BrbVc4Button_TYP;
	END_STRUCT;
	VisMenu_TYP : 	STRUCT 
		eLastMenuPage : Pages_ENUM;
		btnProduction : BrbVc4Button_TYP;
		btnConfiguration : BrbVc4Button_TYP;
		btnService : BrbVc4Button_TYP;
		btnUser : BrbVc4Button_TYP;
		btnEvents : BrbVc4Button_TYP;
		btnContact : BrbVc4Button_TYP;
		nPageCount : UINT;
		nPageCountTotal : UINT;
	END_STRUCT;
	HeadBmpIndices_ENUM : 
		(
		eHEAD_BMP_INDEX_SMALL_GREY,
		eHEAD_BMP_INDEX_SMALL_RED,
		eHEAD_BMP_INDEX_SMALL_BLUE,
		eHEAD_BMP_INDEX_SMALL_GREEN,
		eHEAD_BMP_INDEX_BIG_GREY,
		eHEAD_BMP_INDEX_BIG_RED,
		eHEAD_BMP_INDEX_BIG_BLUE,
		eHEAD_BMP_INDEX_BIG_GREEN
		);
	VisHead_TYP : 	STRUCT 
		btnHead : BrbVc4Button_TYP;
		btnHeadJog : BrbVc4JogButton_TYP;
		numHead : BrbVc4Numeric_TYP;
		chkHead : BrbVc4Checkbox_TYP;
		hsHead : BrbVc4Hotspot_TYP;
		numEncoderVelocity : BrbVc4Numeric_TYP;
	END_STRUCT;
	VisHeads_TYP : 	STRUCT 
		nHeadOffset : INT;
		Head : ARRAY[0..nDEV_INDEX_MAX]OF VisHead_TYP;
	END_STRUCT;
	TankBmpIndices_ENUM : 
		(
		eTANK_BMP_INDEX_SMALL_GREY,
		eTANK_BMP_INDEX_SMALL_RED,
		eTANK_BMP_INDEX_SMALL_BLUE,
		eTANK_BMP_INDEX_SMALL_GREEN
		);
	VisTank_TYP : 	STRUCT 
		btnTank : BrbVc4Button_TYP;
		numTank : BrbVc4Numeric_TYP;
	END_STRUCT;
	VisTanks_TYP : 	STRUCT 
		nTankOffset : INT;
		Tank : ARRAY[0..nVIS_TANK_SHOW_INDEX_MAX]OF VisTank_TYP;
	END_STRUCT;
	VisNavigation_TYP : 	STRUCT 
		btnLeft : BrbVc4Button_TYP;
		btnRight : BrbVc4Button_TYP;
		btnUp : BrbVc4Button_TYP;
		btnDown : BrbVc4Button_TYP;
	END_STRUCT;
	ModuleBmpIndices_ENUM : 
		(
		eMODUL_BMP_INDEX_EMPTY,
		eMODUL_BMP_INDEX_OVERVIEW,
		eMODUL_BMP_INDEX_CLEANING,
		eMODUL_BMP_INDEX_PRESSURE_CTRL,
		eMODUL_BMP_INDEX_MOT_ADJUSTMENT,
		eMODUL_BMP_INDEX_CAMERA,
		eMODUL_BMP_INDEX_TEMPCTRL,
		eMODUL_BMP_INDEX_ANALOGFUELL
		);
	VisToolbarModuleSource_TYP : 	STRUCT 
		eModuleBmpIndex : ModuleBmpIndices_ENUM;
	END_STRUCT;
	VisToolbarModuleShow_TYP : 	STRUCT 
		btnModule : BrbVc4Button_TYP;
	END_STRUCT;
	VisToolbar_TYP : 	STRUCT 
		ModulesSource : ARRAY[0..nVIS_TOOLBAR_SOURCE_INDEX_MAX]OF VisToolbarModuleSource_TYP;
		btnLeft : BrbVc4Button_TYP;
		btnRight : BrbVc4Button_TYP;
		nScrollOffsetMax : INT;
		nScrollOffset : INT;
		nEnabledModuleCount : UINT;
		ModulesShow : ARRAY[0..nVIS_TOOLBAR_SHOW_INDEX_MAX]OF VisToolbarModuleShow_TYP;
	END_STRUCT;
	VisDropRunning_TYP : 	STRUCT 
		bRun : BOOL;
		nIndex : UINT;
		nStatus : UINT;
	END_STRUCT;
	VisFooter_TYP : 	STRUCT 
		bmpDrop : BrbVc4Bitmap_TYP;
		DropRunningGreen : VisDropRunning_TYP;
		DropRunningYellow : VisDropRunning_TYP;
		DropRunningRed : VisDropRunning_TYP;
		hsDrop : BrbVc4Hotspot_TYP;
		txtParInvalidWarning : BrbVc4Text_TYP;
		txtParSaveWarning : BrbVc4Text_TYP;
		strCurrentProductName : BrbVc4String_TYP;
		bShowStopStart : BOOL;
		btnStart : BrbVc4Button_TYP;
		btnStop : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageScreensaver_TYP : 	STRUCT 
		bDummy : BOOL;
	END_STRUCT;
	VisPageDialogMoveAxes_TYP : 	STRUCT 
		bAllAxesHomingOk : BOOL;
		txtHoming : BrbVc4Text_TYP;
		btnNo : BrbVc4Button_TYP;
		btnYes : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageDialogStartHeating_TYP : 	STRUCT 
		chkTank : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF BrbVc4Checkbox_TYP;
		btnOk : BrbVc4Button_TYP;
		nStep : UINT;
		nTankIndexStart : UINT;
	END_STRUCT;
	VisPageMain_TYP : 	STRUCT 
		bgGlue : BrbVc4Bargraph_TYP;
		bgSoft : BrbVc4Bargraph_TYP;
	END_STRUCT;
	VisPageHeadSwitch_TYP : 	STRUCT 
		nNumber : UINT;
		rPause : REAL;
		rLine : REAL;
		bInputCompleted : BOOL;
		nColor : UINT;
		nStatus : UINT;
	END_STRUCT;
	VisPageHead_TYP : 	STRUCT 
		nSelectedHeadIndex : UINT;
		txtSensorType : BrbVc4Text_TYP;
		nSelectedPrintmarkStatus : UINT;
		nSelectedPrintMarkIndex : UINT;
		btnPrintmarkMinus : BrbVc4Button_TYP;
		btnPrintmarkPlus : BrbVc4Button_TYP;
		nSwitchOffset : INT;
		btnSwitchMinus : BrbVc4Button_TYP;
		numSwitchIndexMax : BrbVc4Numeric_TYP;
		btnSwitchPlus : BrbVc4Button_TYP;
		numProductLen : BrbVc4Numeric_TYP;
		numCylinderPerimeter : BrbVc4Numeric_TYP;
		chkStichModeOn : BrbVc4Checkbox_TYP;
		numTrackLenTotal : BrbVc4Numeric_TYP;
		SwitchesShow : ARRAY[0..nVIS_HEAD_SWITCH_SHOW_INDEX_MAX]OF VisPageHeadSwitch_TYP;
		numStitchCloseDistance : BrbVc4Numeric_TYP;
		ddStitchSize : BrbVc4Dropdown_TYP;
		btnSwitchesRight : BrbVc4Button_TYP;
		btnSwitchesLeft : BrbVc4Button_TYP;
		btnCopy : BrbVc4Button_TYP;
		btnApply : BrbVc4Button_TYP;
		btnHeadPar : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageHeadCopy_TYP : 	STRUCT 
		numSourceHeadIndex : BrbVc4Numeric_TYP;
		numDestHeadIndex : BrbVc4Numeric_TYP;
		btnCancel : BrbVc4Button_TYP;
		btnCopy : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageHeadPar_TYP : 	STRUCT 
		nSelectedHeadIndex : UINT;
		txtSensorType : BrbVc4Text_TYP;
		numHeadProductOffset : BrbVc4Numeric_TYP;
		numHopperCleaningPeriod : BrbVc4Numeric_TYP;
		numHopperFillingPeriod : BrbVc4Numeric_TYP;
		numCollectorCleaningPeriod : BrbVc4Numeric_TYP;
		numCollectorFillingPeriod : BrbVc4Numeric_TYP;
		btnBack : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageProdFileListShowEntry_TYP : 	STRUCT 
		sProductName : STRING[nBRB_FILE_NAME_CHAR_MAX];
		nColor : UINT;
	END_STRUCT;
	VisPageProductionFileList_TYP : 	STRUCT 
		bGetListSource : BOOL;
		fbBrbReadDir : BrbReadDir;
		ListSource : ARRAY[0..nVIS_FILE_LIST_SOURCE_INDEX_MAX]OF BrbReadDirListEntry_TYP;
		bSelectCurrentProduct : BOOL;
		nCurrentProcuctIndex : DINT;
		sSelectedProductName : STRING[nBRB_FILE_NAME_CHAR_MAX];
		ScrollList : BrbVc4ScrollList_TYP;
		ListShow : ARRAY[0..nVIS_FILE_LIST_SHOW_INDEX_MAX]OF VisPageProdFileListShowEntry_TYP;
		tgList : BrbVc4Touchgrid_TYP;
	END_STRUCT;
	VisPageProduction_TYP : 	STRUCT 
		FileList : VisPageProductionFileList_TYP;
		btnLoad : BrbVc4Button_TYP;
		btnSave : BrbVc4Button_TYP;
		btnDelete : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageProductionSave_TYP : 	STRUCT 
		strOldName : BrbVc4String_TYP;
		strNewName : BrbVc4String_TYP;
		btnCancel : BrbVc4Button_TYP;
		btnSave : BrbVc4Button_TYP;
		bWaitForSave : BOOL;
	END_STRUCT;
	VisPageProductionDelete_TYP : 	STRUCT 
		strName : BrbVc4String_TYP;
		btnCancel : BrbVc4Button_TYP;
		btnDelete : BrbVc4Button_TYP;
		bWaitForDelete : BOOL;
	END_STRUCT;
	VisPageConfiguration_TYP : 	STRUCT 
		bInputAllowed : BOOL;
		nInputStatus : UINT;
		bInputAllowedInAutomatic : BOOL;
		eLastConfigurationPageIndex : Pages_ENUM;
		hsLanguageGerman : BrbVc4Hotspot_TYP;
		shLanguageGerman : BrbVc4Shape_TYP;
		hsLanguageEnglish : BrbVc4Hotspot_TYP;
		shLanguageEnglish : BrbVc4Shape_TYP;
		chkTankGlueEnable : BrbVc4Checkbox_TYP;
		chkTankCleanEnable : BrbVc4Checkbox_TYP;
		chkTankSoftEnable : BrbVc4Checkbox_TYP;
		nSelectedHeadIndex : INT;
		btnHeadRight : BrbVc4Button_TYP;
		btnHeadLeft : BrbVc4Button_TYP;
		ddHeadSensorType : BrbVc4Dropdown_TYP;
		layHeadTrigger : BrbVc4Layer_TYP;
		layHeadEncoder : BrbVc4Layer_TYP;
		layHeadPrintmark : BrbVc4Layer_TYP;
		layHeadPrintmarkTrigger : BrbVc4Layer_TYP;
		numHeadTriggerSensorNumber : BrbVc4Numeric_TYP;
		numHeadPrintMarkSensorNumber : BrbVc4Numeric_TYP;
		numHeadDistanceHeadSensor : BrbVc4Numeric_TYP;
		numHeadEncoderNumber : BrbVc4Numeric_TYP;
		numHeadCylinderPerimeter : BrbVc4Numeric_TYP;
		numHeadReduceProductLenPercent : BrbVc4Numeric_TYP;
		numHeadPrintMarkEncoder : BrbVc4Numeric_TYP;
		numHeadPrintMarkSource : BrbVc4Numeric_TYP;
		numHeadDistancePrintTrigSensor : BrbVc4Numeric_TYP;
		numHeadDistancePrintVelocityJump : BrbVc4Numeric_TYP;
		numHeadValveCompOpenTime : BrbVc4Numeric_TYP;
		numHeadValveCompCloseTime : BrbVc4Numeric_TYP;
		chkHeadProductDetectionOn : BrbVc4Checkbox_TYP;
		numHeadProductDetectSensorNumber : BrbVc4Numeric_TYP;
		numHeadProductDetectSensOffset : BrbVc4Numeric_TYP;
		numSelectedTriggerSensorIndex : BrbVc4Numeric_TYP;
		btnTriggerSensorRight : BrbVc4Button_TYP;
		btnTriggerSensorLeft : BrbVc4Button_TYP;
		numSelectedEncoderIndex : BrbVc4Numeric_TYP;
		optTriggerSensorEdgeDetectPos : BrbVc4Optionbox_TYP;
		optTriggerSensorEdgeDetectNeg : BrbVc4Optionbox_TYP;
		btnEncoderRight : BrbVc4Button_TYP;
		btnEncoderLeft : BrbVc4Button_TYP;
		layEncoderSim : BrbVc4Layer_TYP;
		layEncoderReal : BrbVc4Layer_TYP;
		layEncoderVirtual : BrbVc4Layer_TYP;
		layEncoderTrigger : BrbVc4Layer_TYP;
		ddEncoderType : BrbVc4Dropdown_TYP;
		chkEncoderSimEnable : BrbVc4Checkbox_TYP;
		numEncoderIncPerRev : BrbVc4Numeric_TYP;
		numEncoderVelocityFactor : BrbVc4Numeric_TYP;
		numEncoderDistPerRev : BrbVc4Numeric_TYP;
		ddEncoderSource : BrbVc4Dropdown_TYP;
		numEncoderFactor : BrbVc4Numeric_TYP;
		ddEncoderTriggerSource : BrbVc4Dropdown_TYP;
		numEncoderTriggerSignalDistance : BrbVc4Numeric_TYP;
		numEncoderTriggerTimeOut : BrbVc4Numeric_TYP;
		numSelectedPrintmarkSensorIndex : BrbVc4Numeric_TYP;
		btnPrintmarkSensorRight : BrbVc4Button_TYP;
		btnPrintmarkSensorLeft : BrbVc4Button_TYP;
		optPrintmarkSensorEdgeDetectPos : BrbVc4Optionbox_TYP;
		optPrintmarkSensorEdgeDetectNeg : BrbVc4Optionbox_TYP;
		numSelectedPrintmarkIndex : BrbVc4Numeric_TYP;
		numPrintmarkTolerance : BrbVc4Numeric_TYP;
		btnPrintmarkRight : BrbVc4Button_TYP;
		btnPrintmarkLeft : BrbVc4Button_TYP;
		numPrintmarkLength : BrbVc4Numeric_TYP;
		nStitchingInputStatus : UINT;
		nCleaningInputStatus : UINT;
		txtNoCleaningTank : BrbVc4Text_TYP;
		chkCleaningAutoValveControl : BrbVc4Checkbox_TYP;
		chkSystemCleaningEnable : BrbVc4Checkbox_TYP;
		nPressureControlInputStatus : UINT;
		nMotorizedAdjustmentInputStatus : UINT;
		chkMotorizedAdjustmentVertical : BrbVc4Checkbox_TYP;
		numMotAdjustRefOffsetVertical : BrbVc4Numeric_TYP;
		numMotAdjustNegLimitVertical : BrbVc4Numeric_TYP;
		numMotAdjustPosLimitVertical : BrbVc4Numeric_TYP;
		chkMotorizedAdjustmentHorizontal : BrbVc4Checkbox_TYP;
		numMotAdjustRefOffsetHorizontal : BrbVc4Numeric_TYP;
		numMotAdjustNegLimitHorizontal : BrbVc4Numeric_TYP;
		numMotAdjustPosLimitHorizontal : BrbVc4Numeric_TYP;
		nImageProcessingInputStatus : UINT;
		btnTankRight : BrbVc4Button_TYP;
		btnTankLeft : BrbVc4Button_TYP;
		nSelectedTankIndex : INT;
		nTempCtrlInputStatus : UINT;
		chkTempCtrlTankExists : BrbVc4Checkbox_TYP;
		btnParTempCtrlZoneTank : BrbVc4Button_TYP;
		numTempCtrlToleranceMin : BrbVc4Numeric_TYP;
		numTempCtrlToleranceMax : BrbVc4Numeric_TYP;
		numTempCtrlTempWarning : BrbVc4Numeric_TYP;
		ParTempCtrlZone : VisPageConfigParTempCtrlZone_TYP;
		chkTempCtrlHeadZonesExist : BrbVc4Checkbox_TYP;
		chkTempCtrlHeadZoneHeadExist : BrbVc4Checkbox_TYP;
		chkTempCtrlHeadZoneTubeExist : BrbVc4Checkbox_TYP;
		chkTempCtrlHeadZoneReserveExist : BrbVc4Checkbox_TYP;
		btnParTempCtrlZoneTube : BrbVc4Button_TYP;
		btnParTempCtrlZoneHead : BrbVc4Button_TYP;
		btnParTempCtrlZoneReserve : BrbVc4Button_TYP;
		ddTempCtrlHeadTankNumber : BrbVc4Dropdown_TYP;
		numTempCtrlStandbyTemp : BrbVc4Numeric_TYP;
		btnStrainGaugeProdReset : BrbVc4Button_TYP;
		btnStrainGaugeProdSumReset : BrbVc4Button_TYP;
		btnStrainGaugeSoftSumReset : BrbVc4Button_TYP;
		btnStrainGaugeSoftReset : BrbVc4Button_TYP;
		btnTabOfConsum : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageConfigParTempCtrlZone_TYP : 	STRUCT 
		eParType : Vis_TempCtrlParType_ENUM;
		pParSource : REFERENCE TO TempCtrlTempZoneCfgPar_TYP;
		numSetTempMax : BrbVc4Numeric_TYP;
		numGain : BrbVc4Numeric_TYP;
		numIntegrationTime : BrbVc4Numeric_TYP;
		numDerivativeTime : BrbVc4Numeric_TYP;
		numFilterTime : BrbVc4Numeric_TYP;
		numMinOut : BrbVc4Numeric_TYP;
		numMaxOut : BrbVc4Numeric_TYP;
	END_STRUCT;
	Vis_TempCtrlParType_ENUM : 
		(
		eVIS_TEMPCTRL_PAR_UNKNOWN,
		eVIS_TEMPCTRL_PAR_TANK,
		eVIS_TEMPCTRL_PAR_TUBE,
		eVIS_TEMPCTRL_PAR_HEAD,
		eVIS_TEMPCTRL_PAR_RESERVE
		);
	VisPageServiceEthInterface_TYP : 	STRUCT 
		bInputCompleted : BOOL;
		nStatus : UINT;
	END_STRUCT;
	VisPageService_TYP : 	STRUCT 
		numOperatingHoursTotal : BrbVc4Numeric_TYP;
		numOperatingHoursService : BrbVc4Numeric_TYP;
		btnReset : BrbVc4Button_TYP;
		btnCalibrateTouch : BrbVc4Button_TYP;
		numVelocityEncoder : ARRAY[0..nIDX_ENCODER_MAX]OF BrbVc4Numeric_TYP;
		dtSystemTime : DTStructure;
		nSystemTimeStatus : UINT;
		bSystemTimeInputCompleted : BOOL;
		numSelectedPrintmarkSensorIndex : BrbVc4Numeric_TYP;
		btnPrintmarkSensorRight : BrbVc4Button_TYP;
		btnPrintmarkSensorLeft : BrbVc4Button_TYP;
		numPrintmarkLen : BrbVc4Numeric_TYP;
		btnSimEncoderVelocityMinus : BrbVc4Button_TYP;
		numSimEncoderVelocity : BrbVc4Numeric_TYP;
		btnSimEncoderVelocityPlus : BrbVc4Button_TYP;
		btnSimEncoderStart : BrbVc4Button_TYP;
		btnSimEncoderStop : BrbVc4Button_TYP;
		EthInterface : ARRAY[0..nSYSTEM_ETH_INTERFACE_INDEX_MAX]OF VisPageServiceEthInterface_TYP;
	END_STRUCT;
	VisPageUser_TYP : 	STRUCT 
		btnPassword : BrbVc4Button_TYP;
		pwPassword : BrbVc4Password_TYP;
		btnLogout : BrbVc4Button_TYP;
		btnModConfig : BrbVc4Button_TYP;
		btnMsgPopupOk : BrbVc4Button_TYP;
		bmpMsgPopupBell : BrbVc4Bitmap_TYP;
		txtMsgPopup : BrbVc4Text_TYP;
	END_STRUCT;
	VisPageModuleConfig_TYP : 	STRUCT 
		chkOverviewEnable : BrbVc4Checkbox_TYP;
		chkCleaningEnable : BrbVc4Checkbox_TYP;
		chkStitchingEnable : BrbVc4Checkbox_TYP;
		chkMotorizedAdjustmentEnable : BrbVc4Checkbox_TYP;
		chkPressureControlEnable : BrbVc4Checkbox_TYP;
		chkImageProcessingEnable : BrbVc4Checkbox_TYP;
		chkTempCtrlEnable : BrbVc4Checkbox_TYP;
		chkAnalogFuellstand : BrbVc4Checkbox_TYP;
		btnOk : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageEvents_TYP : 	STRUCT 
		btnDummy : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageContact_TYP : 	STRUCT 
		nDummy : UINT;
	END_STRUCT;
	VisPageOverview_TYP : 	STRUCT 
		btnCorrectionMinus : BrbVc4Button_TYP;
		numCorrection : BrbVc4Numeric_TYP;
		btnCorrectionPlus : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageCleaning_TYP : 	STRUCT 
		nHeadsEnabledCount : UINT;
		bgClean : BrbVc4Bargraph_TYP;
		optCleanInterval : BrbVc4Optionbox_TYP;
		optCleanStandard : BrbVc4Optionbox_TYP;
		optBondInterval : BrbVc4Optionbox_TYP;
		optBondStandard : BrbVc4Optionbox_TYP;
		btnStop : BrbVc4Button_TYP;
		btnStart : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageSystemCleaning_TYP : 	STRUCT 
		strProgress : BrbVc4String_TYP;
		numGlueTankCleaningPeriod : BrbVc4Numeric_TYP;
		numDetergentTankCleaningPeriod : BrbVc4Numeric_TYP;
		btnCancel : BrbVc4Button_TYP;
		btnStart : BrbVc4Button_TYP;
	END_STRUCT;
	VisPagePressureControl_TYP : 	STRUCT 
		btnCorrectionMinus : BrbVc4Button_TYP;
		numCorrection : BrbVc4Numeric_TYP;
		btnCorrectionPlus : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageMotorizedAdjustment_TYP : 	STRUCT 
		nSelectedHeadIndex : UINT;
		Vertical : VisPageMotorizedAdjustmentAx_TYP;
		Horizontal : VisPageMotorizedAdjustmentAx_TYP;
		btnHomeAllAxes : BrbVc4Button_TYP;
		btnLeft : BrbVc4Button_TYP;
		btnRight : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageMotorizedAdjustmentAx_TYP : 	STRUCT 
		numAxPos : BrbVc4Numeric_TYP;
		btnAxJogNeg : BrbVc4JogButton_TYP;
		btnAxJogPos : BrbVc4JogButton_TYP;
		btnStop : BrbVc4Button_TYP;
		btnHome : BrbVc4Button_TYP;
		numAxLimitNeg : BrbVc4Numeric_TYP;
		numAxLimitPos : BrbVc4Numeric_TYP;
		numParkPos : BrbVc4Numeric_TYP;
		btnMoveToParkPos : BrbVc4Button_TYP;
		numWorkPos : BrbVc4Numeric_TYP;
		btnMoveToWorkPos : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageCamera_TYP : 	STRUCT 
		btnReset : BrbVc4Button_TYP;
	END_STRUCT;
	VisPageMedienVerbrauch_TYP : 	STRUCT 
		btnSave : BrbVc4Button_TYP;
		sProductNameMedien : VisPageProductionFileList_TYP;
		rSoftConcWeight : ARRAY[0..nVIS_FILE_LIST_SHOW_INDEX_MAX]OF REAL;
		rGlueWeight : ARRAY[0..nVIS_FILE_LIST_SHOW_INDEX_MAX]OF REAL;
		dtDateTime : ARRAY[0..nVIS_FILE_LIST_SHOW_INDEX_MAX]OF DATE_AND_TIME;
	END_STRUCT;
	VisPageAnalogFuellstand_TYP : 	STRUCT 
	END_STRUCT;
	VisPageTempCtrlZone_TYP : 	STRUCT 
		numActTemp : BrbVc4Numeric_TYP;
		numSetTemp : BrbVc4Numeric_TYP;
	END_STRUCT;
	VisPageTempCtrl_TYP : 	STRUCT 
		nSelectedTankIndex : UINT;
		btnHeatingStartStop : BrbVc4Button_TYP;
		numActTempTank : BrbVc4Numeric_TYP;
		numSetTempTank : BrbVc4Numeric_TYP;
		btnStandbyStartStop : BrbVc4Button_TYP;
		numStandbyTemp : BrbVc4Numeric_TYP;
		nSelectedHeadIndex : UINT;
		chkHeatingZonesEnable : BrbVc4Checkbox_TYP;
		Zone : ARRAY[0..nIDX_TEMP_CTRL_ZONE_MAX]OF VisPageTempCtrlZone_TYP;
		numRelatedTank : BrbVc4Numeric_TYP;
	END_STRUCT;
	Vis_TYP : 	STRUCT 
		General : BrbVc4General_TYP;
		GeneralOwn : VisGeneralOwn_TYP;
		PageHandling : BrbVc4PageHandling_TYP;
		ScreenSaver : BrbVc4ScreenSaver_TYP;
		Scrollbar : BrbVc4Scrollbar_TYP;
		tcTabCtrl : BrbVc4TabCtrl_TYP;
		Heads : VisHeads_TYP;
		Tanks : VisTanks_TYP;
		Header : VisHeader_TYP;
		Navigation : VisNavigation_TYP;
		Toolbar : VisToolbar_TYP;
		Footer : VisFooter_TYP;
		Menu : VisMenu_TYP;
		PageScreensaver : VisPageScreensaver_TYP;
		PageDialogMoveAxes : VisPageDialogMoveAxes_TYP;
		PageDialogStartHeating : VisPageDialogStartHeating_TYP;
		PageMain : VisPageMain_TYP;
		PageHead : VisPageHead_TYP;
		PageHeadCopy : VisPageHeadCopy_TYP;
		PageHeadPar : VisPageHeadPar_TYP;
		PageProduction : VisPageProduction_TYP;
		PageProductionSave : VisPageProductionSave_TYP;
		PageProductionDelete : VisPageProductionDelete_TYP;
		PageConfiguration : VisPageConfiguration_TYP;
		PageService : VisPageService_TYP;
		PageUser : VisPageUser_TYP;
		PageModuleConfig : VisPageModuleConfig_TYP;
		PageEvents : VisPageEvents_TYP;
		PageContact : VisPageContact_TYP;
		PageOverview : VisPageOverview_TYP;
		PageCleaning : VisPageCleaning_TYP;
		PageSystemCleaning : VisPageSystemCleaning_TYP;
		PagePressureControl : VisPagePressureControl_TYP;
		PageMotorizedAdjustment : VisPageMotorizedAdjustment_TYP;
		PageCamera : VisPageCamera_TYP;
		PageTempCtrl : VisPageTempCtrl_TYP;
		PageMedienVerbrauch : VisPageMedienVerbrauch_TYP;
		PageAnalogFuellstand : VisPageAnalogFuellstand_TYP;
	END_STRUCT;
END_TYPE
