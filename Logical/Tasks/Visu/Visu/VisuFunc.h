/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Visu
 * File: VisuFunc.h
 * Author: niedermeierr
 * Created: September 14, 2012
 *******************************************************************/

#include <bur/plctypes.h>
#include <string.h>
#include <math.h>

void HandleGeneralOwn();
void HandleHeader();
void HandleToolbar();
void HandleMenu();
void ResetAllMenuButtons();
void SetNavigationButtons(BOOL bLeftVisible, BOOL bRightVisible, BOOL bUpVisible, BOOL bDownVisible);
void HandleFooter();
void SetScrollbarVerticalButtonColors(BrbVc4ScrollbarVer_TYP* pScrollbar);
void HandleReadProductionFileList();
