﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.0.19.69 SP?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Task">Visu.c</File>
    <File Description="Lokale Datentypen des Tasks" Private="true">Visu.typ</File>
    <File Description="Lokale Variablen des Tasks" Private="true">Visu.var</File>
    <File Description="Hilfsfunktionen des Tasks">VisuFunc.c</File>
    <File Description="Prototypen der Hilfsfuntkionen">VisuFunc.h</File>
  </Files>
</Program>