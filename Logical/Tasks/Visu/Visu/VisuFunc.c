/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Visu
 * File: VisuFunc.c
 * Author: niedermeierr
 * Created: September 14, 2012
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include "VisuFunc.h"

void HandleGeneralOwn()
{
	// UserLogIn
	if(Vis.GeneralOwn.nUserLevel > 0 && Vis.GeneralOwn.nUserLevelOld == 0)
	{
		AppSetEvent(eEVT_INF_SERVICE_LOGGED_IN, 0, "", 0, &gEventManagement);
		AppResetEvent(eEVT_INF_SERVICE_LOGGED_IN, &gEventManagement);
	}
	else if(Vis.GeneralOwn.nUserLevel == 0 && Vis.GeneralOwn.nUserLevelOld > 0)
	{
		AppSetEvent(eEVT_INF_SERVICE_LOGGED_OUT, 0, "", 0, &gEventManagement);
		AppResetEvent(eEVT_INF_SERVICE_LOGGED_OUT, &gEventManagement);
	}
	Vis.GeneralOwn.nUserLevelOld = Vis.GeneralOwn.nUserLevel;
	// Blinken
	fbBlink400.IN = 1;
	fbBlink400.PT = 400;
	TON(&fbBlink400);
	if(fbBlink400.Q == 1)
	{
		fbBlink400.IN = 0;
		TON(&fbBlink400);
		Vis.GeneralOwn.bBlink400 = !Vis.GeneralOwn.bBlink400;
	}
}

void HandleHeader()
{
	BrbVc4SetControlVisibility(&Vis.Header.bmpLogo.nStatus, !gAxisControl.State.bStepperSimulated);
	BrbVc4SetControlVisibility(&Vis.Header.txtStepperSimulated.nStatus, gAxisControl.State.bStepperSimulated);
	BrbGetCurrentTimeText(Vis.Header.sDateTime, sizeof(Vis.Header.sDateTime), "dd.mm.yyyy   hh:MM:ss");
	if(BrbVc4HandleButton(&Vis.Header.btnMenu) == 1)
	{
		Vis.Header.btnMenu.bClicked = 0;
		ResetAllMenuButtons();
		if(gEventHandling.State.nErrorCount == 0 && gEventHandling.State.nWarningCount == 0)
		{
			if(Vis.Menu.eLastMenuPage == ePAGE_PRODUCTION)
			{
				Vis.Menu.btnProduction.bClicked = 1;
			}
			else if(Vis.Menu.eLastMenuPage == ePAGE_CONFIGURATION1)
			{
				Vis.Menu.btnConfiguration.bClicked = 1;
			}
			else if(Vis.Menu.eLastMenuPage == ePAGE_SERVICE1)
			{
				Vis.Menu.btnService.bClicked = 1;
			}
			else if(Vis.Menu.eLastMenuPage == ePAGE_SERVICE2)
			{
				Vis.Menu.btnService.bClicked = 1;
			}
			else if(Vis.Menu.eLastMenuPage == ePAGE_USER)
			{
				Vis.Menu.btnUser.bClicked = 1;
			}
			else if(Vis.Menu.eLastMenuPage == ePAGE_EVENTS)
			{
				Vis.Menu.btnEvents.bClicked = 1;
			}
			else if(Vis.Menu.eLastMenuPage == ePAGE_CONTACT)
			{
				Vis.Menu.btnContact.bClicked = 1;
			}
			if(Vis.Menu.eLastMenuPage != ePAGE_CONFIGURATION1)
			{
				BrbVc4ChangePage(&Vis.PageHandling, Vis.Menu.eLastMenuPage, 0);
			}
			else
			{
				if(Vis.GeneralOwn.nUserLevel == eUSERLEVEL_OPERATOR)
				{
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION1, 0);
				}
				else
				{
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageConfiguration.eLastConfigurationPageIndex, 0);
				}
			}
		}
		else
		{
			Vis.Menu.btnEvents.bClicked = 1;
			BrbVc4ChangePage(&Vis.PageHandling, ePAGE_EVENTS, 0);
		}
	}
	if(BrbVc4HandleButton(&Vis.Header.btnHelp) == 1)
	{
		Vis.Header.btnHelp.bClicked = 0;
	}
	if(BrbVc4HandleButton(&Vis.Header.btnHome) == 1)
	{
		Vis.Header.btnHome.bClicked = 0;
		BrbVc4ChangePage(&Vis.PageHandling, ePAGE_MAIN, 0);
	}
}

void HandleToolbar()
{
	// Eingeschaltet Module ermitteln
	INT nModSourceIndex = 0;
	memset(Vis.Toolbar.ModulesSource, 0, sizeof(Vis.Toolbar.ModulesSource));
	Vis.Toolbar.ModulesSource[nModSourceIndex++].eModuleBmpIndex = eMODUL_BMP_INDEX_EMPTY;
	Vis.Toolbar.ModulesSource[nModSourceIndex++].eModuleBmpIndex = eMODUL_BMP_INDEX_EMPTY;
	if(gPar.MachinePar.MachineModule.bOverviewEn == 1)
	{
		Vis.Toolbar.ModulesSource[nModSourceIndex].eModuleBmpIndex = eMODUL_BMP_INDEX_OVERVIEW;
		nModSourceIndex++;
	}
	if(gPar.MachinePar.MachineModule.bCleaningEn == 1)
	{
		Vis.Toolbar.ModulesSource[nModSourceIndex].eModuleBmpIndex = eMODUL_BMP_INDEX_CLEANING;
		nModSourceIndex++;
	}
	if(gPar.MachinePar.MachineModule.bPressureCtrlEn == 1)
	{
		Vis.Toolbar.ModulesSource[nModSourceIndex].eModuleBmpIndex = eMODUL_BMP_INDEX_PRESSURE_CTRL;
		nModSourceIndex++;
	}
	if(gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn == 1)
	{
		Vis.Toolbar.ModulesSource[nModSourceIndex].eModuleBmpIndex = eMODUL_BMP_INDEX_MOT_ADJUSTMENT;
		nModSourceIndex++;
	}
	if(gPar.MachinePar.MachineModule.bImgProcessingEn == 1)
	{
		Vis.Toolbar.ModulesSource[nModSourceIndex].eModuleBmpIndex = eMODUL_BMP_INDEX_CAMERA;
		nModSourceIndex++;
	}
	if(gPar.MachinePar.MachineModule.bTempCtrlEn == 1)
	{
		Vis.Toolbar.ModulesSource[nModSourceIndex].eModuleBmpIndex = eMODUL_BMP_INDEX_TEMPCTRL;
		nModSourceIndex++;
	}
	if(gPar.MachinePar.MachineModule.bAnalogFuellStandEn == 1)
	{
		Vis.Toolbar.ModulesSource[nModSourceIndex].eModuleBmpIndex = eMODUL_BMP_INDEX_ANALOGFUELL;
		nModSourceIndex++;
	}
	
	Vis.Toolbar.ModulesSource[nModSourceIndex++].eModuleBmpIndex = eMODUL_BMP_INDEX_EMPTY;
	Vis.Toolbar.ModulesSource[nModSourceIndex++].eModuleBmpIndex = eMODUL_BMP_INDEX_EMPTY;
	Vis.Toolbar.nEnabledModuleCount = nModSourceIndex - 2*nVIS_TOOLBAR_SHOW_EMPTY_OFFSET;
	// Scrolling
	Vis.Toolbar.nScrollOffsetMax = Vis.Toolbar.nEnabledModuleCount + nVIS_TOOLBAR_SHOW_EMPTY_OFFSET + 1 - nVIS_TOOLBAR_SHOW_INDEX_MAX;
	BrbVc4SetControlVisibility(&Vis.Toolbar.btnLeft.nStatus, Vis.Toolbar.nScrollOffset < Vis.Toolbar.nScrollOffsetMax); // Scrolling mit Platzhalter
	if(BrbVc4HandleButton(&Vis.Toolbar.btnLeft) == 1)
	{
		Vis.Toolbar.btnLeft.bClicked = 0;
		if(Vis.Toolbar.nScrollOffset < Vis.Toolbar.nScrollOffsetMax)
		{
			Vis.Toolbar.nScrollOffset += 1;
		}
	}
	BrbVc4SetControlVisibility(&Vis.Toolbar.btnRight.nStatus, Vis.Toolbar.nScrollOffset > 0); // Scrolling mit Platzhalter
	if(BrbVc4HandleButton(&Vis.Toolbar.btnRight) == 1)
	{
		Vis.Toolbar.btnRight.bClicked = 0;
		if(Vis.Toolbar.nScrollOffset > 0)
		{
			Vis.Toolbar.nScrollOffset -= 1;
		}
	}
	if(Vis.Toolbar.nScrollOffset < 0 && Vis.Toolbar.nScrollOffsetMax > -1)
	{
		Vis.Toolbar.nScrollOffset = 0;
	}
	else if(Vis.Toolbar.nScrollOffset > Vis.Toolbar.nScrollOffsetMax)
	{
		Vis.Toolbar.nScrollOffset = Vis.Toolbar.nScrollOffsetMax;
	}
	// Anzeige + Klicks
	INT nModShowIndex = 0;
	nModSourceIndex = 0;
	for(nModShowIndex=0; nModShowIndex<=nVIS_TOOLBAR_SHOW_INDEX_MAX; nModShowIndex++)
	{
		if(nModSourceIndex+Vis.Toolbar.nScrollOffset <= nVIS_TOOLBAR_SOURCE_INDEX_MAX)
		{
			Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.nBmpIndex = Vis.Toolbar.ModulesSource[nModSourceIndex+Vis.Toolbar.nScrollOffset].eModuleBmpIndex;
		}
		else
		{
			Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.nBmpIndex = eMODUL_BMP_INDEX_EMPTY;
		}
		if(Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.bClicked == 1)
		{
			Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.bClicked = 0;
			if(Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.nBmpIndex == eMODUL_BMP_INDEX_OVERVIEW)
			{
				Vis.Toolbar.nScrollOffset = nModSourceIndex+Vis.Toolbar.nScrollOffset - 2;
				BrbVc4ChangePage(&Vis.PageHandling, ePAGE_OVERVIEW, 0);
			}
			else if(Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.nBmpIndex == eMODUL_BMP_INDEX_CLEANING)
			{
				Vis.Toolbar.nScrollOffset = nModSourceIndex+Vis.Toolbar.nScrollOffset - 2;
				BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CLEANING, 0);
			}
			else if(Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.nBmpIndex == eMODUL_BMP_INDEX_PRESSURE_CTRL)
			{
				Vis.Toolbar.nScrollOffset = nModSourceIndex+Vis.Toolbar.nScrollOffset - 2;
				BrbVc4ChangePage(&Vis.PageHandling, ePAGE_PRESSURE_CONTROL, 0);
			}
			else if(Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.nBmpIndex == eMODUL_BMP_INDEX_MOT_ADJUSTMENT)
			{
				Vis.Toolbar.nScrollOffset = nModSourceIndex+Vis.Toolbar.nScrollOffset - 2;
				BrbVc4ChangePage(&Vis.PageHandling, ePAGE_MOTORIZED_ADJUSTMENT0, 0);
			}
			else if(Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.nBmpIndex == eMODUL_BMP_INDEX_CAMERA)
			{
				Vis.Toolbar.nScrollOffset = nModSourceIndex+Vis.Toolbar.nScrollOffset - 2;
				BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CAMERA, 0);
			}
			else if(Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.nBmpIndex == eMODUL_BMP_INDEX_TEMPCTRL)
			{
				Vis.Toolbar.nScrollOffset = nModSourceIndex+Vis.Toolbar.nScrollOffset - 2;
				BrbVc4ChangePage(&Vis.PageHandling,ePAGE_TEMPCTRL , 0);
			}
			else if(Vis.Toolbar.ModulesShow[nModShowIndex].btnModule.nBmpIndex == eMODUL_BMP_INDEX_ANALOGFUELL)
			{
				Vis.Toolbar.nScrollOffset = nModSourceIndex+Vis.Toolbar.nScrollOffset - 2;
				BrbVc4ChangePage(&Vis.PageHandling,ePAGE_ANALOGFUELLSTAND, 0);
			}
			
		}		
		nModSourceIndex++;
	}	
	
	
}

void HandleMenu()
{
	Vis.Menu.nPageCountTotal = 15;
	if(BrbVc4HandleButton(&Vis.Menu.btnProduction) == 1)
	{
		Vis.Menu.btnProduction.bClicked = 0;
		ResetAllMenuButtons();
		Vis.Menu.btnProduction.nColor = eCOLOR_MENU_SELECTED;
		BrbVc4ChangePage(&Vis.PageHandling, ePAGE_PRODUCTION, 0);
	}
	else if(BrbVc4HandleButton(&Vis.Menu.btnConfiguration) == 1)
	{
		Vis.Menu.btnConfiguration.bClicked = 0;
		ResetAllMenuButtons();
		Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
		if(Vis.GeneralOwn.nUserLevel == eUSERLEVEL_OPERATOR)
		{
			BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION1, 0);
		}
		else
		{
			BrbVc4ChangePage(&Vis.PageHandling, Vis.PageConfiguration.eLastConfigurationPageIndex, 0);
		}
	}
	else if(BrbVc4HandleButton(&Vis.Menu.btnService) == 1)
	{
		Vis.Menu.btnService.bClicked = 0;
		ResetAllMenuButtons();
		Vis.Menu.btnService.nColor = eCOLOR_MENU_SELECTED;
		BrbVc4ChangePage(&Vis.PageHandling, ePAGE_SERVICE1, 0);
	}
	else if(BrbVc4HandleButton(&Vis.Menu.btnUser) == 1)
	{
		Vis.Menu.btnUser.bClicked = 0;
		ResetAllMenuButtons();
		Vis.Menu.btnUser.nColor = eCOLOR_MENU_SELECTED;
		BrbVc4ChangePage(&Vis.PageHandling, ePAGE_USER, 0);
	}
	else if(BrbVc4HandleButton(&Vis.Menu.btnEvents) == 1)
	{
		Vis.Menu.btnEvents.bClicked = 0;
		ResetAllMenuButtons();
		Vis.Menu.btnEvents.nColor = eCOLOR_MENU_SELECTED;
		BrbVc4ChangePage(&Vis.PageHandling, ePAGE_EVENTS, 0);
	}
	else if(BrbVc4HandleButton(&Vis.Menu.btnContact) == 1)
	{
		Vis.Menu.btnContact.bClicked = 0;
		ResetAllMenuButtons();
		Vis.Menu.btnContact.nColor = eCOLOR_MENU_SELECTED;
		BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONTACT, 0);
	}
}

void ResetAllMenuButtons()
{
	Vis.Menu.btnProduction.nColor = eCOLOR_MENU_UNSELECTED;
	Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_UNSELECTED;
	Vis.Menu.btnService.nColor = eCOLOR_MENU_UNSELECTED;
	Vis.Menu.btnUser.nColor = eCOLOR_MENU_UNSELECTED;
	Vis.Menu.btnEvents.nColor = eCOLOR_MENU_UNSELECTED;
	Vis.Menu.btnContact.nColor = eCOLOR_MENU_UNSELECTED;
}

void SetNavigationButtons(BOOL bLeftVisible, BOOL bRightVisible, BOOL bUpVisible, BOOL bDownVisible)
{
	BrbVc4SetControlVisibility(&Vis.Navigation.btnLeft.nStatus, bLeftVisible);
	BrbVc4SetControlEnability(&Vis.Navigation.btnLeft.nStatus, bLeftVisible);
	BrbVc4SetControlVisibility(&Vis.Navigation.btnRight.nStatus, bRightVisible);
	BrbVc4SetControlEnability(&Vis.Navigation.btnRight.nStatus, bRightVisible);
	BrbVc4SetControlVisibility(&Vis.Navigation.btnUp.nStatus, bUpVisible);
	BrbVc4SetControlEnability(&Vis.Navigation.btnUp.nStatus, bUpVisible);
	BrbVc4SetControlVisibility(&Vis.Navigation.btnDown.nStatus, bDownVisible);
	BrbVc4SetControlEnability(&Vis.Navigation.btnDown.nStatus, bDownVisible);
}

void HandleFooter()
{
	BrbVc4SetControlVisibility(&Vis.Footer.DropRunningRed.nStatus, gEventHandling.State.nErrorCount > 0);
	BrbVc4SetControlVisibility(&Vis.Footer.DropRunningYellow.nStatus, gEventHandling.State.nWarningCount > 0);
	BrbVc4SetControlVisibility(&Vis.Footer.DropRunningGreen.nStatus, Vis.Footer.DropRunningGreen.bRun && gEventHandling.State.nWarningCount == 0 && gEventHandling.State.nErrorCount == 0);
	Vis.Footer.DropRunningGreen.bRun = gMainLogic.State.bAutomatic;
	if(Vis.Footer.DropRunningGreen.bRun == 1)
	{
		Vis.Footer.DropRunningGreen.nIndex += 3;
		if(Vis.Footer.DropRunningGreen.nIndex >= 360)
		{
			Vis.Footer.DropRunningGreen.nIndex = 0;
		}
	}
	if(gEventHandling.State.nErrorCount > 0)
	{
		Vis.Footer.bmpDrop.nIndex = eDROPS_BMP_INDEX_RED;
	}
	else if(gEventHandling.State.nWarningCount > 0)
	{
		Vis.Footer.bmpDrop.nIndex = eDROPS_BMP_INDEX_YELLOW;
	}
	else if(gMainLogic.State.bAutomatic == 1)
	{
		Vis.Footer.bmpDrop.nIndex = eDROPS_BMP_INDEX_GREEN;
	}
	else
	{
		Vis.Footer.bmpDrop.nIndex = eDROPS_BMP_INDEX_BLUE;
	}
	if(Vis.Footer.hsDrop.bClicked == 1)
	{
		Vis.Footer.hsDrop.bClicked = 0;
		if(gEventHandling.State.nErrorCount > 0 || gEventHandling.State.nWarningCount > 0)
		{
			Vis.Menu.btnEvents.bClicked = 1;
			BrbVc4ChangePage(&Vis.PageHandling, ePAGE_EVENTS, 0);
		}
		else
		{
			BrbVc4ChangePage(&Vis.PageHandling, ePAGE_MAIN, 0);
		}
	}
	BrbVc4SetControlVisibility(&Vis.Footer.txtParInvalidWarning.nStatus, gCalcPos.State.bInvalidParEntry && gParHandling.State.bAction == 0);
	BrbVc4SetControlVisibility(&Vis.Footer.txtParSaveWarning.nStatus, gParHandling.State.bParChanged);
	strcpy(Vis.Footer.strCurrentProductName.sValue, gParPermanent.sCurrentProductName);
	// Start/Stop
	BrbVc4SetControlVisibility(&Vis.Footer.btnStart.nStatus, gMainLogic.State.bAutomatic == 0 && Vis.Footer.bShowStopStart == 1);
	// Beim Setzen des Disable-Bits w�rde der Hintergrund des Buttons vom Betriebssytem automatisch grau gezeichnet.
	// Es soll aber nur die Schrift ausgegraut werden, der Hintergrund soll blau bleiben. Deswegen werden hier die Farben explizit gesetzt.
	BrbVc4SetControlColor(&Vis.Footer.btnStart.nColor, gMainLogic.State.bAutomaticPossible == 1, eBACKCOLOR_PLANATOL_BLUE+eFORECOLOR_WHITE, eBACKCOLOR_PLANATOL_BLUE+eFORECOLOR_DARK_GREY);
	if(BrbVc4HandleButton(&Vis.Footer.btnStart) == 1)
	{
		Vis.Footer.btnStart.bClicked = 0;
		if(gMainLogic.State.bAutomaticPossible == 1)
		{
			if(BrbSetCaller(&gMainLogic.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
			{
				gMainLogic.CallerBox.bAutoOn = 1;
			}
		}
	}
	BrbVc4SetControlVisibility(&Vis.Footer.btnStop.nStatus, gMainLogic.State.bAutomatic == 1 && Vis.Footer.bShowStopStart == 1);
	if(BrbVc4HandleButton(&Vis.Footer.btnStop) == 1)
	{
		Vis.Footer.btnStop.bClicked = 0;
		gMainLogic.DirectBox.bStop = 1;
	}
}

void SetScrollbarVerticalButtonColors(BrbVc4ScrollbarVer_TYP* pScrollbar)
{
	BrbVc4SetControlColor(&pScrollbar->btnTop.nColor, BrbVc4IsControlEnabled(pScrollbar->btnTop.nStatus), eCOLOR_ENABLED, eCOLOR_DISABLED);
	BrbVc4SetControlColor(&pScrollbar->btnPageUp.nColor, BrbVc4IsControlEnabled(pScrollbar->btnPageUp.nStatus), eCOLOR_ENABLED, eCOLOR_DISABLED);
	BrbVc4SetControlColor(&pScrollbar->btnLineUp.nColor, BrbVc4IsControlEnabled(pScrollbar->btnLineUp.nStatus), eCOLOR_ENABLED, eCOLOR_DISABLED);
	BrbVc4SetControlColor(&pScrollbar->btnLineDown.nColor, BrbVc4IsControlEnabled(pScrollbar->btnLineDown.nStatus), eCOLOR_ENABLED, eCOLOR_DISABLED);
	BrbVc4SetControlColor(&pScrollbar->btnPageDown.nColor, BrbVc4IsControlEnabled(pScrollbar->btnPageDown.nStatus), eCOLOR_ENABLED, eCOLOR_DISABLED);
	BrbVc4SetControlColor(&pScrollbar->btnBottom.nColor, BrbVc4IsControlEnabled(pScrollbar->btnBottom.nStatus), eCOLOR_ENABLED, eCOLOR_DISABLED);
}

void HandleReadProductionFileList()
{
	// Quell-Datei-Liste einlesen
	if(Vis.PageProduction.FileList.bGetListSource == 1)
	{
		Vis.PageProduction.FileList.fbBrbReadDir.pDevice = (STRING*)&sDEV_PRODUCT_PAR;
		Vis.PageProduction.FileList.fbBrbReadDir.pPath = 0;
		Vis.PageProduction.FileList.fbBrbReadDir.eFilter = eBRB_DIR_INFO_ONLY_FILES;
		Vis.PageProduction.FileList.fbBrbReadDir.bWithParentDir = 0;
		Vis.PageProduction.FileList.fbBrbReadDir.pFileFilter = "txt";
		Vis.PageProduction.FileList.fbBrbReadDir.eSorting = eBRB_FILE_SORTING_ALPH_UP;
		Vis.PageProduction.FileList.fbBrbReadDir.pList = (UDINT)&Vis.PageProduction.FileList.ListSource;
		Vis.PageProduction.FileList.fbBrbReadDir.nListIndexMax = nVIS_FILE_LIST_SOURCE_INDEX_MAX;
		BrbReadDir(&Vis.PageProduction.FileList.fbBrbReadDir);
		if(Vis.PageProduction.FileList.fbBrbReadDir.nStatus == 0)
		{
			Vis.PageProduction.FileList.bGetListSource = 0;
			Vis.PageProduction.FileList.bSelectCurrentProduct = 1;
		}
		else if(Vis.PageProduction.FileList.fbBrbReadDir.nStatus != 65535)
		{
			Vis.PageProduction.FileList.bGetListSource = 0;
		}
	}
	if(Vis.PageProduction.FileList.bSelectCurrentProduct == 1)
	{
		Vis.PageProduction.FileList.bSelectCurrentProduct = 0;
		STRING sFileName[nBRB_FILE_NAME_CHAR_MAX];
		strcpy(sFileName, gParPermanent.sCurrentProductName);
		strcat(sFileName, ".txt");
		for(nIndex=0;nIndex<=nVIS_FILE_LIST_SOURCE_INDEX_MAX; nIndex++)
		{
			BrbReadDirListEntry_TYP* pSourceEntry = &Vis.PageProduction.FileList.ListSource[nIndex];
			if(strcmp(pSourceEntry->sName, sFileName) == 0)
			{
				Vis.PageProduction.FileList.ScrollList.nSelectedIndex = nIndex;
				Vis.PageProduction.FileList.ScrollList.nScrollOffset = nIndex - 3;
			}
		}
	}

}

/*void HandleModuleConfig()
	
{
	if (gPar.MachinePar.MachineModule.bStitchmodeEn != 1)
	{
		BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
	}
	else
	{
		BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent, 0);
	}


	
}*/
