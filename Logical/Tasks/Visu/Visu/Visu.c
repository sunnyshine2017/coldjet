/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Visu
 * File: Visu.c
 * Author: niedermeier
 * Created: September 14, 2012
 * Edited : Murali Juni 26, 2017
 ********************************************************************
 * Implementation of program Visu
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <VisuFunc.h>

void _INIT VisuINIT(void)
{
	// Initalisierung der Variablen

	
	// Initalisierung der Vis
	brsmemset((UDINT)&Vis, 0, sizeof(Vis));
	strcpy(Vis.General.sVisName, "VisWvg");
	Vis.General.nRedrawCycles = 10;
	Vis.General.bDisableRedraw = 1;
	Vis.PageHandling.nPageNext = ePAGE_MAIN;
	Vis.PageHandling.nPageCurrent = Vis.PageHandling.nPageNext;
	Vis.PageHandling.bPageInit = 1;
	Vis.PageHandling.nPageDefault = ePAGE_MAIN;
	Vis.PageHandling.PagesPreviousLifo.pList = (UDINT)&Vis.PageHandling.nPagesPrevious;
	Vis.PageHandling.PagesPreviousLifo.nEntryLength = sizeof(Vis.PageHandling.nPagesPrevious[0]);
	Vis.PageHandling.PagesPreviousLifo.nIndexMax = nBRB_PAGE_LAST_INDEX_MAX;
	BrbMemListClear(&Vis.PageHandling.PagesPreviousLifo);
	Vis.ScreenSaver.nScreenSaverPage = ePAGE_SCREENSAVER;
	Vis.ScreenSaver.bEnable = 1;
	Vis.General.nLanguageChange = gParPermanent.nVisLanguage;
	Vis.GeneralOwn.nUserLevelOld = 0;
	Vis.GeneralOwn.nScreenWidth = 480;
	Vis.GeneralOwn.nScreenHeight = 800;

	// Initialisierung der Seiten
	Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
	Vis.PageConfiguration.eLastConfigurationPageIndex = ePAGE_CONFIGURATION1;
	ResetAllMenuButtons();
	Vis.Toolbar.nScrollOffset = 1;
	Vis.PageProduction.FileList.bGetListSource = 1;

	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gVisu, 0, sizeof(gVisu));
	gVisu.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
	
	// Automatisches Einloggen
#ifdef AUTO_LOG_IN_SERVICE
	Vis.GeneralOwn.nUserLevel = eUSERLEVEL_SERVICE;
#endif

#ifdef AUTO_LOG_IN_CONFIGURATOR
	Vis.GeneralOwn.nUserLevel = eUSERLEVEL_CONFIGURATOR;
#endif
	

	

}

void _CYCLIC VisuCYCLIC(void)
{
	// Allgemein
	BrbVc4HandleGeneral(&Vis.General);
	HandleGeneralOwn();
	Vis.ScreenSaver.fbScreenSaver.PT = gPar.MachinePar.nScreensaverTime;
	fbOperatingSeconds.IN = 1;
	fbOperatingSeconds.PT = 1000;
	TON(&fbOperatingSeconds);
	if(fbOperatingSeconds.Q == 1)
	{
		fbOperatingSeconds.IN = 0;
		TON(&fbOperatingSeconds);
		fbOperatingSeconds.IN = 1;
		TON(&fbOperatingSeconds);
		gParPermanent.nOperatingSecondsTotal += 1;
		gParPermanent.nOperatingSecondsService += 1;
	}

	
//Ausloggen nach zwei Stunden, wenn eingeloggt als Service
	
if(Vis.GeneralOwn.nUserLevel == eUSERLEVEL_SERVICE)
	{
		
		fbServiceLogout2Hours.IN = 1;
		fbServiceLogout2Hours.PT =100000 ; 
		TON(&fbServiceLogout2Hours);
		if(fbServiceLogout2Hours.Q == 1)
		{
			fbServiceLogout2Hours.IN = 0;
			TON(&fbServiceLogout2Hours);
			Vis.GeneralOwn.nUserLevel = eUSERLEVEL_OPERATOR;
		}
	
		if((BrbVc4HandleButton(&Vis.PageUser.btnLogout) == 1) )
		{
		
			fbServiceLogout2Hours.IN = 0;
			TON(&fbServiceLogout2Hours);
			
		}
	}
	
	if(Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR)
	{
	
		fbServiceLogout2Hours.IN = 0;
		TON(&fbServiceLogout2Hours);
	}

	
	
	// Header
	HandleHeader();
	
	// Footer
	HandleFooter();
	
	// Sonstiges
	HandleReadProductionFileList(); // Liste muss auch eingelesen werden, wenn die Seite nicht angezeigt wird
	Vis.PageConfiguration.bInputAllowed = (gMainLogic.State.bAutomatic == 0);
	Vis.PageConfiguration.bInputAllowedInAutomatic = (Vis.GeneralOwn.nUserLevel >= eUSERLEVEL_SERVICE);
	BrbVc4SetControlEnability(&Vis.PageConfiguration.nInputStatus, Vis.PageConfiguration.bInputAllowed == 1);
	

	
	// Seiten --------------------------------------------------------------------------------------------------------------------------------------------------
	switch(Vis.PageHandling.nPageCurrent)
	{
		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_TEMPLATE:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_SCREENSAVER:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_MAIN:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				HandleToolbar();
				//K�pfe
				for(nHeadIndex=0; nHeadIndex<=nVIS_HEAD_MAIN_SHOW_INDEX_MAX; nHeadIndex++)
				{
					if(nHeadIndex+Vis.Heads.nHeadOffset <= gPar.MachinePar.nBonDevIndex)
					{
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 1);
						if(Vis.Heads.Head[nHeadIndex].btnHead.bClicked == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.bClicked = 0;
							Vis.PageHead.nSelectedHeadIndex = nHeadIndex+Vis.Heads.nHeadOffset;
							BrbVc4ChangePage(&Vis.PageHandling, ePAGE_HEAD, 0);
						}
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].chkHead.nStatus, 1);
						if(BrbVc4HandleCheckbox(&Vis.Heads.Head[nHeadIndex].chkHead) == 1)
						{
							gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].bBonDevEn = Vis.Heads.Head[nHeadIndex].chkHead.bChecked;
						}
						else
						{
							Vis.Heads.Head[nHeadIndex].chkHead.bChecked = gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].bBonDevEn;
						}
						if(gCalcPos.State.HeadState[nHeadIndex+Vis.Heads.nHeadOffset].bHeadError == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_BIG_RED;
						}	
						else if(Vis.Heads.Head[nHeadIndex].chkHead.bChecked == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_BIG_BLUE;
						}
						else
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_BIG_GREY;
						}
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 1);
						Vis.Heads.Head[nHeadIndex].numHead.nValue = nHeadIndex+Vis.Heads.nHeadOffset + 1;
					}
					else
					{
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].chkHead.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 0);
					}
				}
				// Navigation
				if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex <= nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
				{
					SetNavigationButtons(0, 0, 0, 0);
				}
				else if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex > nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
				{
					SetNavigationButtons(0, 1, 0, 0);
				}
				else if(Vis.Heads.nHeadOffset >= gPar.MachinePar.nBonDevIndex - nVIS_HEAD_MAIN_SHOW_INDEX_MAX )
				{
					SetNavigationButtons(1, 0, 0, 0);
				}
				else
				{
					SetNavigationButtons(1, 1, 0, 0);
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					Vis.Heads.nHeadOffset -= nVIS_HEAD_MAIN_SHOW_INDEX_MAX+1;
					if(Vis.Heads.nHeadOffset < 0)
					{
						Vis.Heads.nHeadOffset = 0;
					}
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					Vis.Heads.nHeadOffset += nVIS_HEAD_MAIN_SHOW_INDEX_MAX+1;
					if(Vis.Heads.nHeadOffset > nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
					{
						Vis.Heads.nHeadOffset = nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX;
					}
				}
				// Tanks
				BrbVc4SetControlVisibility(&Vis.PageMain.bgGlue.nStatus, gPar.MachinePar.TankType.bTankGlueEn == 1);
				Vis.PageMain.bgGlue.nValue = gMachine.State.TankLvl.nFillLvlGlue;
				BrbVc4SetControlVisibility(&Vis.PageMain.bgSoft.nStatus, gPar.MachinePar.TankType.bTankSoftEn == 1);
				Vis.PageMain.bgSoft.nValue = gMachine.State.TankLvl.nFillLvlSoft;
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_HEAD:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Kopf-Parameter
				Vis.PageHead.txtSensorType.nIndex = gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType;
				BrbVc4SetControlVisibility(&Vis.PageHead.btnPrintmarkMinus.nStatus, Vis.PageHead.nSelectedPrintMarkIndex > 0 && 
					(gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR ||
					gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR));
				if(BrbVc4HandleButton(&Vis.PageHead.btnPrintmarkMinus) == 1)
				{
					Vis.PageHead.btnPrintmarkMinus.bClicked = 0;
					if(Vis.PageHead.nSelectedPrintMarkIndex > 0)
					{
						Vis.PageHead.nSelectedPrintMarkIndex -= 1;
					}
				}
				BrbVc4SetControlVisibility(&Vis.PageHead.btnPrintmarkPlus.nStatus, Vis.PageHead.nSelectedPrintMarkIndex < nIDX_PRINT_MARK_MAX && 
					(gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR || 
					gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR));
				if(BrbVc4HandleButton(&Vis.PageHead.btnPrintmarkPlus) == 1)
				{
					Vis.PageHead.btnPrintmarkPlus.bClicked = 0;
					if(Vis.PageHead.nSelectedPrintMarkIndex < nIDX_PRINT_MARK_MAX)
					{
						Vis.PageHead.nSelectedPrintMarkIndex += 1;
					}
				}
				if(gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType != eSEN_TYPE_PRINT_MARK_SENSOR && 
					gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType != eSEN_TYPE_PRINT_MARK_TRIG_SENSOR)
				{
					Vis.PageHead.nSelectedPrintMarkIndex = 0;
				}
				BrbVc4SetControlVisibility(&Vis.PageHead.nSelectedPrintmarkStatus, gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType
					== eSEN_TYPE_PRINT_MARK_SENSOR || gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR);
				BrbVc4SetControlVisibility(&Vis.PageHead.btnSwitchMinus.nStatus, gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax > 0);
				if(BrbVc4HandleButton(&Vis.PageHead.btnSwitchMinus) == 1)
				{
					Vis.PageHead.btnSwitchMinus.bClicked = 0;
					if(gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex] > 0)
					{
						gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex] -= 1;
					}
				}
				BrbVc4SetControlVisibility(&Vis.PageHead.btnSwitchPlus.nStatus, gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex]
					.nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex] < nIDX_SWITCHES_MAX);
				if(BrbVc4HandleButton(&Vis.PageHead.btnSwitchPlus) == 1)
				{
					Vis.PageHead.btnSwitchPlus.bClicked = 0;
					if(gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex] < nIDX_SWITCHES_MAX)
					{
						gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex] += 1;
					}
				}
				if(Vis.PageHead.numSwitchIndexMax.bInputCompleted == 1)
				{
					Vis.PageHead.numSwitchIndexMax.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex] = Vis.PageHead.numSwitchIndexMax.nValue;
				}
				else
				{
					Vis.PageHead.numSwitchIndexMax.nValue = gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex];
				}
				if(gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_ENCODER)
				{
					BrbVc4SetControlVisibility(&Vis.PageHead.numProductLen.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageHead.numCylinderPerimeter.nStatus, 1);
					BrbVc4SetControlColor(&Vis.PageHead.numCylinderPerimeter.nColor, gCalcPos.State.HeadState[Vis.PageHead.nSelectedHeadIndex].bInvalidProductLen == 
						0, eCOLOR_OUTPUT, eBACKCOLOR_RED + eFORECOLOR_WHITE);
					Vis.PageHead.numCylinderPerimeter.rValue = gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].rCylinderPerimeter;
				}
				else
				{
					BrbVc4SetControlVisibility(&Vis.PageHead.numProductLen.nStatus, 1);
					BrbVc4SetControlVisibility(&Vis.PageHead.numCylinderPerimeter.nStatus, 0);
					BrbVc4SetControlColor(&Vis.PageHead.numProductLen.nColor, gCalcPos.State.HeadState[Vis.PageHead.nSelectedHeadIndex].bInvalidProductLen == 
						0, eCOLOR_INPUT, eBACKCOLOR_RED + eFORECOLOR_WHITE);
					if(Vis.PageHead.numProductLen.bInputCompleted == 1)
					{
						Vis.PageHead.numProductLen.bInputCompleted = 0;
						gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].rProductLen = Vis.PageHead.numProductLen.rValue;
					}
					else
					{
						Vis.PageHead.numProductLen.rValue = gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].rProductLen;
					}
				}
				BrbVc4SetControlVisibility(&Vis.PageHead.chkStichModeOn.nStatus, gPar.MachinePar.MachineModule.bStitchmodeEn == 1);
				if(BrbVc4HandleCheckbox(&Vis.PageHead.chkStichModeOn) == 1)
				{
					gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].bStitchmode = Vis.PageHead.chkStichModeOn.bChecked;
				}
				else
				{
					Vis.PageHead.chkStichModeOn.bChecked = gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].bStitchmode;
				}
				BrbVc4SetControlColor(&Vis.PageHead.numTrackLenTotal.nColor, gCalcPos.State.HeadState[Vis.PageHead.nSelectedHeadIndex].
					TrackState[Vis.PageHead.nSelectedPrintMarkIndex].bInvalidTrackLen == 0, eCOLOR_OUTPUT, eBACKCOLOR_RED + eFORECOLOR_WHITE);
				Vis.PageHead.numTrackLenTotal.rValue = gCalcPos.State.HeadState[Vis.PageHead.nSelectedHeadIndex].TrackState[Vis.PageHead.nSelectedPrintMarkIndex].rCalcTrackLen;
				// Zeichnen
				if(Vis.General.nRedrawCounter == 0)
				{
					UINT nFunc = VA_Saccess(1, Vis.General.nVcHandle);
					if(nFunc == 0)
					{
						nFunc = VA_Attach(1, Vis.General.nVcHandle, 0, (UDINT)&"P01100_Head/Default/dbLines");
						if(nFunc == 0)
						{
							// Zeichnen
							DINT nDrawboxWidth = 460;
							DINT nDrawboxHeight = 40;
							// L�schen
							BrbVc4Rectangle_TYP Background;
							Background.nLeft = Background.nTop = 0;
							Background.nWidth = nDrawboxWidth;
							Background.nHeight = nDrawboxHeight-3;
							Background.nBorderColor = 59;
							Background.nFillColor = 251;
							Background.nDashWidth = 0;
							BrbVc4DrawRectangle(&Background, &Vis.General);
							REAL rScaleFactor = 1.0;
							if(gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_ENCODER)
							{
								rScaleFactor = (REAL)(nDrawboxWidth - 4) / gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].rCylinderPerimeter;
							}
							else
							{
								rScaleFactor = (REAL)(nDrawboxWidth - 4) / gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].rProductLen;
							}
							BrbVc4Rectangle_TYP Rect;
							Rect.nTop = (DINT)((REAL)nDrawboxHeight / 2.0) - 2;
							Rect.nHeight = 4;
							Rect.nDashWidth = 0;
							DINT rLastPos = 0.0;
							for(nSwitchIndex=0; nSwitchIndex<=gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex];
								nSwitchIndex++)
							{
								if(gCalcPos.State.HeadState[Vis.PageHead.nSelectedHeadIndex].nInvalidParIdx[Vis.PageHead.nSelectedPrintMarkIndex] == nSwitchIndex)
								{
									Rect.nFillColor = Rect.nBorderColor = eBACKCOLOR_RED;
								}
								else if(nSwitchIndex >= Vis.PageHead.nSwitchOffset && nSwitchIndex <= Vis.PageHead.nSwitchOffset + nVIS_HEAD_SWITCH_SHOW_INDEX_MAX)
								{
									Rect.nFillColor = Rect.nBorderColor = eBACKCOLOR_GREEN;
								}
								else
								{
									Rect.nFillColor = Rect.nBorderColor = eBACKCOLOR_BLACK;
								}
								Rect.nLeft = (DINT)((gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].Switches.BondingParIdx[Vis.PageHead.nSelectedPrintMarkIndex].
									rPause[nSwitchIndex] + rLastPos) * rScaleFactor);
								rLastPos += gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].Switches.BondingParIdx[Vis.PageHead.nSelectedPrintMarkIndex].
									rPause[nSwitchIndex];
								Rect.nWidth = (DINT)((gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].Switches.
									BondingParIdx[Vis.PageHead.nSelectedPrintMarkIndex].rStitchLen[nSwitchIndex] + rLastPos) * rScaleFactor) - Rect.nLeft;
								rLastPos += gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].Switches.BondingParIdx[Vis.PageHead.nSelectedPrintMarkIndex].
									rStitchLen[nSwitchIndex];
								BrbVc4DrawRectangle(&Rect, &Vis.General);
							}
							VA_Detach(1, Vis.General.nVcHandle);
						}
						// Zugriff freigeben
						nFunc = VA_Srelease(1, Vis.General.nVcHandle);
					}
				}
				// Pause + Strich
				for(nSwitchIndex=0; nSwitchIndex<=nVIS_HEAD_SWITCH_SHOW_INDEX_MAX; nSwitchIndex++)
				{
					Vis.PageHead.SwitchesShow[nSwitchIndex].nNumber = Vis.PageHead.nSwitchOffset+nSwitchIndex+1;
					if(nSwitchIndex+Vis.PageHead.nSwitchOffset <= gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex])
					{
						BrbVc4SetControlVisibility(&Vis.PageHead.SwitchesShow[nSwitchIndex].nStatus, 1);
						BrbVc4SetControlColor(&Vis.PageHead.SwitchesShow[nSwitchIndex].nColor, gCalcPos.State.HeadState[Vis.PageHead.nSelectedHeadIndex]
							.nInvalidParIdx[Vis.PageHead.nSelectedPrintMarkIndex] != nSwitchIndex+Vis.PageHead.nSwitchOffset, eCOLOR_INPUT, eBACKCOLOR_RED + eFORECOLOR_WHITE);
						
						if(Vis.PageHead.SwitchesShow[nSwitchIndex].bInputCompleted == 1)
						{
							Vis.PageHead.SwitchesShow[nSwitchIndex].bInputCompleted = 0;
							gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].Switches.BondingParIdx[Vis.PageHead.nSelectedPrintMarkIndex]
								.rPause[Vis.PageHead.nSwitchOffset+nSwitchIndex] = Vis.PageHead.SwitchesShow[nSwitchIndex].rPause;
							gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].Switches.BondingParIdx[Vis.PageHead.nSelectedPrintMarkIndex]
								.rStitchLen[Vis.PageHead.nSwitchOffset+nSwitchIndex] = Vis.PageHead.SwitchesShow[nSwitchIndex].rLine;
							
						}
						else
						{
							Vis.PageHead.SwitchesShow[nSwitchIndex].rPause = gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex]
								.Switches.BondingParIdx[Vis.PageHead.nSelectedPrintMarkIndex].rPause[Vis.PageHead.nSwitchOffset+nSwitchIndex];
							Vis.PageHead.SwitchesShow[nSwitchIndex].rLine = gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex]
								.Switches.BondingParIdx[Vis.PageHead.nSelectedPrintMarkIndex].rStitchLen[Vis.PageHead.nSwitchOffset+nSwitchIndex];
						}
					}
					else
					{
						Vis.PageHead.SwitchesShow[nSwitchIndex].nColor = eCOLOR_INPUT;
						BrbVc4SetControlVisibility(&Vis.PageHead.SwitchesShow[nSwitchIndex].nStatus, 0);
					}
					
				}
			
				// Blinken des Buttons `Werte �bernehmen'
				BrbVc4SetControlVisibility(&Vis.PageHead.btnApply.nStatus, Vis.GeneralOwn.bBlink400);
				// Stitch-Size
				BrbVc4SetControlVisibility(&Vis.PageHead.numStitchCloseDistance.nStatus, gPar.MachinePar.MachineModule.bStitchmodeEn == 1 && 
					gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].bStitchmode == 1);
				if(Vis.PageHead.numStitchCloseDistance.bInputCompleted == 1)
				{
					
					Vis.PageHead.numStitchCloseDistance.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].rStitchCloseDistance = Vis.PageHead.numStitchCloseDistance.rValue;
				}
				else
				{
					Vis.PageHead.numStitchCloseDistance.rValue = gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].rStitchCloseDistance;
				}
				BrbVc4SetControlVisibility(&Vis.PageHead.ddStitchSize.nStatus, gPar.MachinePar.MachineModule.bStitchmodeEn == 1 && 
					gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].bStitchmode == 1);
				if(BrbVc4HandleDropdown(&Vis.PageHead.ddStitchSize) == 1)
				{
					gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nStitchSize = Vis.PageHead.ddStitchSize.nIndex;
				}
				else
				{
					Vis.PageHead.ddStitchSize.nIndex = gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nStitchSize;
				}
				// Strich-Navigation
			
				BrbVc4SetControlVisibility(&Vis.PageHead.btnSwitchesLeft.nStatus, Vis.PageHead.nSwitchOffset > 0);
				if(BrbVc4HandleButton(&Vis.PageHead.btnSwitchesLeft) == 1)
				{
					Vis.PageHead.btnSwitchesLeft.bClicked = 0;
					Vis.PageHead.nSwitchOffset -= 1;
				}
				BrbVc4SetControlVisibility(&Vis.PageHead.btnSwitchesRight.nStatus, Vis.PageHead.nSwitchOffset 
					< gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex] - nVIS_HEAD_SWITCH_SHOW_INDEX_MAX);
				if(BrbVc4HandleButton(&Vis.PageHead.btnSwitchesRight) == 1)
				{
					Vis.PageHead.btnSwitchesRight.bClicked = 0;
					Vis.PageHead.nSwitchOffset += 1;
				}
				if(Vis.PageHead.nSwitchOffset <= 0)
				{
					Vis.PageHead.nSwitchOffset = 0;
				}
				else if(Vis.PageHead.nSwitchOffset > gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex]
					.nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex] - nVIS_HEAD_SWITCH_SHOW_INDEX_MAX)
				{
					Vis.PageHead.nSwitchOffset = gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].nSwitchIndexMax[Vis.PageHead.nSelectedPrintMarkIndex] 
						- nVIS_HEAD_SWITCH_SHOW_INDEX_MAX;
				}
				BrbVc4SetControlEnability(&Vis.PageHead.btnApply.nStatus, gCalcPos.State.bInvalidParEntry == 0);
				if(BrbVc4HandleButton(&Vis.PageHead.btnApply) == 1)
				{
					Vis.PageHead.btnApply.bClicked = 0;
					if(BrbSetCaller(&gBondingDev.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gBondingDev.CallerBox.bApplyPar = 1;
					}
				}
				BrbVc4SetControlEnability(&Vis.PageHead.btnCopy.nStatus, gPar.MachinePar.nBonDevIndex > 0);
				if(BrbVc4HandleButton(&Vis.PageHead.btnCopy) == 1)
				{
					Vis.PageHead.btnCopy.bClicked = 0;
					Vis.PageHeadCopy.numSourceHeadIndex.nValue = Vis.PageHead.nSelectedHeadIndex;
					if(gPar.MachinePar.nBonDevIndex > Vis.PageHead.nSelectedHeadIndex)
					{
						Vis.PageHeadCopy.numDestHeadIndex.nValue = Vis.PageHead.nSelectedHeadIndex + 1;
					}
					else if(Vis.PageHead.nSelectedHeadIndex > 0)
					{
						Vis.PageHeadCopy.numDestHeadIndex.nValue = Vis.PageHead.nSelectedHeadIndex - 1;
					}
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_HEAD_COPY, 0);
				}
				//K�pfe
				for(nHeadIndex=0; nHeadIndex<=nVIS_HEAD_MAIN_SHOW_INDEX_MAX; nHeadIndex++)
				{
					if(nHeadIndex+Vis.Heads.nHeadOffset <= gPar.MachinePar.nBonDevIndex)
					{
						if(nHeadIndex+Vis.Heads.nHeadOffset == Vis.PageHead.nSelectedHeadIndex)
						{
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, Vis.GeneralOwn.bBlink400);
						}
						else
						{
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 1);
						}
						if(gCalcPos.State.HeadState[nHeadIndex+Vis.Heads.nHeadOffset].bHeadError == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_RED;
						}	
						else if(gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].bBonDevEn == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_BLUE;
						}
						else
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_GREY;
						}
						if(Vis.Heads.Head[nHeadIndex].btnHead.bClicked == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.bClicked = 0;
							Vis.PageHead.nSelectedHeadIndex = nHeadIndex+Vis.Heads.nHeadOffset;
						}
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 1);
						Vis.Heads.Head[nHeadIndex].numHead.nValue = nHeadIndex+Vis.Heads.nHeadOffset + 1;
					}
					else
					{
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 0);
					}
				}
					
			
				// Navigation
				if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex <= nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
				{
					SetNavigationButtons(0, 0, 0, 0);
				}
				else if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex > nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
				{
					SetNavigationButtons(0, 1, 0, 0);
				}
				else if(Vis.Heads.nHeadOffset >= gPar.MachinePar.nBonDevIndex - nVIS_HEAD_MAIN_SHOW_INDEX_MAX )
				{
					SetNavigationButtons(1, 0, 0, 0);
				}
				else
				{
					SetNavigationButtons(1, 1, 0, 0);
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					Vis.Heads.nHeadOffset -= nVIS_HEAD_MAIN_SHOW_INDEX_MAX+1;
					if(Vis.Heads.nHeadOffset < 0)
					{
						Vis.Heads.nHeadOffset = 0;
					}
					Vis.PageHead.nSwitchOffset = 0;
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					Vis.Heads.nHeadOffset += nVIS_HEAD_MAIN_SHOW_INDEX_MAX+1;
					if(Vis.Heads.nHeadOffset > nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
					{
						Vis.Heads.nHeadOffset = nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX;
					}
					Vis.PageHead.nSwitchOffset = 0;
				}
				BrbVc4SetControlVisibility(&Vis.PageHead.btnHeadPar.nStatus, gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType 
					== eSEN_TYPE_PRINT_MARK_SENSOR || gPar.ProductPar.BonDevPar[Vis.PageHead.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR);
				if(BrbVc4HandleButton(&Vis.PageHead.btnHeadPar) == 1)
				{
					Vis.PageHead.btnHeadPar.bClicked = 0;
					Vis.PageHeadPar.nSelectedHeadIndex = Vis.PageHead.nSelectedHeadIndex;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_HEAD_PAR, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_HEAD_COPY:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 0;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				if(BrbVc4HandleButton(&Vis.PageHeadCopy.btnCancel) == 1)
				{
					Vis.PageHeadCopy.btnCancel.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_HEAD, 0);
				}
				if(BrbVc4HandleButton(&Vis.PageHeadCopy.btnCopy) == 1)
				{
					Vis.PageHeadCopy.btnCopy.bClicked = 0;
					for(nPrintmarkIndex=0; nPrintmarkIndex<=nIDX_PRINT_MARK_MAX; nPrintmarkIndex++)
					{
						gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numDestHeadIndex.nValue].nSwitchIndexMax[nPrintmarkIndex] = 
							gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numSourceHeadIndex.nValue].nSwitchIndexMax[nPrintmarkIndex];
						for(nSwitchIndex=0; nSwitchIndex<=gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numSourceHeadIndex.nValue].nSwitchIndexMax[nPrintmarkIndex]; nSwitchIndex++)
						{
							gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numDestHeadIndex.nValue].Switches.BondingParIdx[nPrintmarkIndex].rPause[nSwitchIndex] = 
								gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numSourceHeadIndex.nValue].Switches.BondingParIdx[nPrintmarkIndex].rPause[nSwitchIndex];
							gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numDestHeadIndex.nValue].Switches.BondingParIdx[nPrintmarkIndex].rStitchLen[nSwitchIndex] = 
								gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numSourceHeadIndex.nValue].Switches.BondingParIdx[nPrintmarkIndex].rStitchLen[nSwitchIndex];
						}
					}
					gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numDestHeadIndex.nValue].rProductLen = gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numSourceHeadIndex.nValue].rProductLen;
					gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numDestHeadIndex.nValue].bStitchmode = gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numSourceHeadIndex.nValue].bStitchmode;
					gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numDestHeadIndex.nValue].nStitchSize = gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numSourceHeadIndex.nValue].nStitchSize;
					gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numDestHeadIndex.nValue].rStitchCloseDistance = gPar.ProductPar.BonDevPar[Vis.PageHeadCopy.numSourceHeadIndex.nValue].rStitchCloseDistance;
					Vis.PageHead.nSelectedHeadIndex = Vis.PageHeadCopy.numDestHeadIndex.nValue;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_HEAD, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_HEAD_PAR:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Kopf-Parameter
				Vis.PageHeadPar.txtSensorType.nIndex = gPar.ProductPar.BonDevPar[Vis.PageHeadPar.nSelectedHeadIndex].eIdxSenType;
				if(Vis.PageHeadPar.numHeadProductOffset.bInputCompleted == 1)
				{
					Vis.PageHeadPar.numHeadProductOffset.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageHeadPar.nSelectedHeadIndex].rPmProductOffset = Vis.PageHeadPar.numHeadProductOffset.rValue;
					gPrintMarkSensor.DirectBox.bUpdateBonDevPar = 1;
				}
				else
				{
					Vis.PageHeadPar.numHeadProductOffset.rValue = gPar.ProductPar.BonDevPar[Vis.PageHeadPar.nSelectedHeadIndex].rPmProductOffset;
				}
				BrbVc4SetControlVisibility(&Vis.PageHeadPar.numHopperCleaningPeriod.nStatus, Vis.PageHeadPar.nSelectedHeadIndex == 0 && (gPar.ProductPar.BonDevPar[Vis.PageHeadPar.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR || gPar.ProductPar.BonDevPar[Vis.PageHeadPar.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR));
				if(Vis.PageHeadPar.numHopperCleaningPeriod.bInputCompleted == 1)
				{
					Vis.PageHeadPar.numHeadProductOffset.bInputCompleted = 0;
					gPar.MachinePar.nHopperCleaningPeriod = Vis.PageHeadPar.numHopperCleaningPeriod.nValue;
				}
				else
				{
					Vis.PageHeadPar.numHopperCleaningPeriod.nValue = gPar.MachinePar.nHopperCleaningPeriod;
				}
				BrbVc4SetControlVisibility(&Vis.PageHeadPar.numHopperFillingPeriod.nStatus, Vis.PageHeadPar.nSelectedHeadIndex == 0 && (gPar.ProductPar.BonDevPar[Vis.PageHeadPar.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR || gPar.ProductPar.BonDevPar[Vis.PageHeadPar.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR));
				if(Vis.PageHeadPar.numHopperFillingPeriod.bInputCompleted == 1)
				{
					Vis.PageHeadPar.numHopperFillingPeriod.bInputCompleted = 0;
					gPar.MachinePar.nHopperFillingPeriod = Vis.PageHeadPar.numHopperFillingPeriod.nValue;
				}
				else
				{
					Vis.PageHeadPar.numHopperFillingPeriod.nValue = gPar.MachinePar.nHopperFillingPeriod;
				}
				BrbVc4SetControlVisibility(&Vis.PageHeadPar.numCollectorCleaningPeriod.nStatus, Vis.PageHeadPar.nSelectedHeadIndex == 1 && (gPar.ProductPar.BonDevPar
					[Vis.PageHeadPar.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR || gPar.ProductPar.BonDevPar[Vis.PageHeadPar.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR));
				if(Vis.PageHeadPar.numCollectorCleaningPeriod.bInputCompleted == 1)
				{
					Vis.PageHeadPar.numCollectorCleaningPeriod.bInputCompleted = 0;
					gPar.MachinePar.nCollectorCleaningPeriod = Vis.PageHeadPar.numCollectorCleaningPeriod.nValue;
				}
				else
				{
					Vis.PageHeadPar.numCollectorCleaningPeriod.nValue = gPar.MachinePar.nCollectorCleaningPeriod;
				}
				BrbVc4SetControlVisibility(&Vis.PageHeadPar.numCollectorFillingPeriod.nStatus, Vis.PageHeadPar.nSelectedHeadIndex == 1 && (gPar.ProductPar.BonDevPar
					[Vis.PageHeadPar.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR || gPar.ProductPar.BonDevPar[Vis.PageHeadPar.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR));
				if(Vis.PageHeadPar.numCollectorFillingPeriod.bInputCompleted == 1)
				{
					Vis.PageHeadPar.numCollectorFillingPeriod.bInputCompleted = 0;
					gPar.MachinePar.nCollectorFillingPeriod = Vis.PageHeadPar.numCollectorFillingPeriod.nValue;
				}
				else
				{
					Vis.PageHeadPar.numCollectorFillingPeriod.nValue = gPar.MachinePar.nCollectorFillingPeriod;
				}
				// Navigation		
				if(BrbVc4HandleButton(&Vis.PageHeadPar.btnBack) == 1)
				{
					Vis.PageHeadPar.btnBack.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_HEAD, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_PRODUCTION:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnProduction.nColor = eCOLOR_MENU_SELECTED;
				Vis.Menu.eLastMenuPage = ePAGE_PRODUCTION;
				Vis.Footer.bShowStopStart = 1;
				brsmemset((UDINT)&Vis.Scrollbar, 0, sizeof(Vis.Scrollbar));
				Vis.Scrollbar.Vertical.nTotalIndexMin = 0;
				Vis.Scrollbar.Vertical.nTotalIndexMax = nVIS_FILE_LIST_SOURCE_INDEX_MAX;
				Vis.Scrollbar.Vertical.nCountShow = nVIS_FILE_LIST_SHOW_INDEX_MAX + 1;
				Vis.PageProduction.FileList.bSelectCurrentProduct = 1;
				// Das Touchgrid ist so parametriert, da� es genau �ber der Liste liegt
				Vis.PageProduction.FileList.tgList.bClickEnabled = 1;
				Vis.PageProduction.FileList.tgList.bDrawGrid = 0;
				Vis.PageProduction.FileList.tgList.bDrawMarker = 0;
				Vis.PageProduction.FileList.tgList.nGridLeft = 12;
				Vis.PageProduction.FileList.tgList.nGridTop = 192;
				Vis.PageProduction.FileList.tgList.nCellWidth = 396;
				Vis.PageProduction.FileList.tgList.nIndexMaxX = 0;
				Vis.PageProduction.FileList.tgList.nCellHeight = 41;
				Vis.PageProduction.FileList.tgList.nIndexMaxY = nVIS_FILE_LIST_SHOW_INDEX_MAX;
				Vis.PageProduction.FileList.tgList.nGridColor = 0;
				Vis.PageProduction.FileList.tgList.eMarkerFigure = 0;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Liste anzeigen
				Vis.Scrollbar.Vertical.bDisabled = Vis.PageProduction.FileList.bGetListSource;
				Vis.Scrollbar.Vertical.nTotalIndexMax = Vis.PageProduction.FileList.fbBrbReadDir.nFileCount-1;
				BrbVc4HandleScrollbarVertical(&Vis.Scrollbar.Vertical, &Vis.PageProduction.FileList.ScrollList.nScrollOffset);
				SetScrollbarVerticalButtonColors(&Vis.Scrollbar.Vertical);
				Vis.PageProduction.FileList.nCurrentProcuctIndex = -1;
				brsmemset((UDINT)Vis.PageProduction.FileList.sSelectedProductName, 0, sizeof(Vis.PageProduction.FileList.sSelectedProductName));
				for(nIndex=0;nIndex<=nVIS_FILE_LIST_SHOW_INDEX_MAX; nIndex++)
				{
					BrbReadDirListEntry_TYP* pSourceEntry = &Vis.PageProduction.FileList.ListSource[Vis.PageProduction.FileList.ScrollList.nScrollOffset + nIndex];
					VisPageProdFileListShowEntry_TYP* pShowEntry = &Vis.PageProduction.FileList.ListShow[nIndex];
					brsmemset((UDINT)&pShowEntry->sProductName, 0, sizeof(pShowEntry->sProductName));
					if(pSourceEntry->nSize > 0)
					{
						strncpy(pShowEntry->sProductName, pSourceEntry->sName, strlen(pSourceEntry->sName) - 4);
						if(Vis.PageProduction.FileList.ScrollList.nScrollOffset + nIndex == Vis.PageProduction.FileList.ScrollList.nSelectedIndex)
						{
							pShowEntry->nColor = eCOLOR_SELECTED;
							strcpy(Vis.PageProduction.FileList.sSelectedProductName, pShowEntry->sProductName);
						}
						else
						{
							pShowEntry->nColor = eCOLOR_ENABLED;
						}
					}
					else
					{
						pShowEntry->nColor = eCOLOR_DISABLED;
					}
					if(strcmp(pShowEntry->sProductName, gParPermanent.sCurrentProductName) == 0)
					{
						Vis.PageProduction.FileList.nCurrentProcuctIndex = Vis.PageProduction.FileList.ScrollList.nScrollOffset + nIndex;
					}
				}
				// Klick auf Touchgrid auswerten
				BrbVc4HandleTouchGrid(&Vis.PageProduction.FileList.tgList, &Vis.General);
				if(Vis.PageProduction.FileList.tgList.eState == eBRB_TOUCH_STATE_PUSHED_EDGE)
				{
					VisPageProdFileListShowEntry_TYP* pShowEntry = &Vis.PageProduction.FileList.ListShow[Vis.PageProduction.FileList.tgList.nSelectedIndex];
					if(brsstrlen((UDINT)&pShowEntry->sProductName) > 0)
					{
						Vis.PageProduction.FileList.ScrollList.nSelectedIndex = Vis.PageProduction.FileList.ScrollList.nScrollOffset + Vis.PageProduction.FileList.tgList.nSelectedIndex;
					}
					else
					{
						Vis.PageProduction.FileList.ScrollList.nSelectedIndex = -1;
					}
				}
				if(Vis.PageProduction.FileList.ScrollList.nSelectedIndex > Vis.PageProduction.FileList.fbBrbReadDir.nFileCount-1)
				{
					Vis.PageProduction.FileList.ScrollList.nSelectedIndex = Vis.PageProduction.FileList.fbBrbReadDir.nFileCount-1;
				}
				// Aktionen
				BrbVc4SetControlEnability(&Vis.PageProduction.btnLoad.nStatus, gParHandling.State.bAction == 0 && Vis.PageProduction.FileList.bGetListSource == 0 && 
					gMainLogic.State.bAutomatic == 0);
				if(BrbVc4HandleButton(&Vis.PageProduction.btnLoad) == 1)
				{
					Vis.PageProduction.btnLoad.bClicked = 0;
					if(Vis.PageProduction.FileList.ScrollList.nSelectedIndex > -1)
					{
						if(BrbSetCaller(&gParHandling.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
						{
							strcpy(gParHandling.Par.sFileName, Vis.PageProduction.FileList.ListSource[Vis.PageProduction.FileList.ScrollList.nSelectedIndex].sName);
							gParHandling.CallerBox.bLoadProductPar = 1;
						}
					}
					else
					{
						Vis.PageProduction.btnLoad.bClicked = 0;
					}
				}
				BrbVc4SetControlEnability(&Vis.PageProduction.btnSave.nStatus, gCalcPos.State.bInvalidParEntry == 0 && gParHandling.State.bAction == 0 && Vis.PageProduction.
					FileList.bGetListSource == 0);
				if(BrbVc4HandleButton(&Vis.PageProduction.btnSave) == 1)
				{
					Vis.PageProduction.btnSave.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_PRODUCTION_SAVE, 0);
				}
				BrbVc4SetControlEnability(&Vis.PageProduction.btnDelete.nStatus, gParHandling.State.bAction == 0 && Vis.PageProduction.FileList.bGetListSource == 0 && 
					Vis.PageProduction.FileList.nCurrentProcuctIndex != Vis.PageProduction.FileList.ScrollList.nSelectedIndex);
				if(BrbVc4HandleButton(&Vis.PageProduction.btnDelete) == 1)
				{
					Vis.PageProduction.btnDelete.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_PRODUCTION_DELETE, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_PRODUCTION_SAVE:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 0;
				brsmemset((UDINT)&Vis.PageProductionSave.strOldName.sValue, 0, sizeof(Vis.PageProductionSave.strOldName.sValue));
				strncpy(Vis.PageProductionSave.strOldName.sValue, gParPermanent.sCurrentProductName, sizeof(Vis.PageProductionSave.strOldName.sValue));
				brsmemset((UDINT)&Vis.PageProductionSave.strNewName.sValue, 0, sizeof(Vis.PageProductionSave.strNewName.sValue));
				strncpy(Vis.PageProductionSave.strNewName.sValue, Vis.PageProduction.FileList.sSelectedProductName, sizeof(Vis.PageProductionSave.strNewName.sValue));
				BrbVc4SetControlEnability(&Vis.PageProductionSave.btnCancel.nStatus, 1);
				BrbVc4SetControlEnability(&Vis.PageProductionSave.btnSave.nStatus, 1);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				if(Vis.PageProductionSave.strNewName.bInputCompleted == 1)
				{
					Vis.PageProductionSave.strNewName.bInputCompleted = 0;
					BrbCheckFileName(Vis.PageProductionSave.strNewName.sValue); // Sonderzeichen ersetzen
				}
				if(BrbVc4HandleButton(&Vis.PageProductionSave.btnCancel) == 1)
				{
					Vis.PageProductionSave.btnCancel.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_PRODUCTION, 0);
				}
				if(BrbVc4HandleButton(&Vis.PageProductionSave.btnSave) == 1)
				{
					Vis.PageProductionSave.btnSave.bClicked = 0;
					if(BrbSetCaller(&gParHandling.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						strcpy(gParHandling.Par.sFileName, Vis.PageProductionSave.strNewName.sValue);
						gParHandling.CallerBox.bSaveProductPar = 1;
						BrbVc4SetControlEnability(&Vis.PageProductionSave.btnCancel.nStatus, 0);
						BrbVc4SetControlEnability(&Vis.PageProductionSave.btnSave.nStatus, 0);
						Vis.PageProductionSave.bWaitForSave = 1;
					}
				}
				if(Vis.PageProductionSave.bWaitForSave == 1)
				{
					// Der Sprung auf die Listen-Seite darf erst gemacht werden, wenn die Aktion beendet ist
					if(gParHandling.CallerBox.bSaveProductPar == 0)
					{
						Vis.PageProductionSave.bWaitForSave = 0;
						BrbVc4SetControlEnability(&Vis.PageProductionSave.btnCancel.nStatus, 1);
						BrbVc4SetControlEnability(&Vis.PageProductionSave.btnSave.nStatus, 1);
						if(strcmp(Vis.PageProductionSave.strOldName.sValue, Vis.PageProductionSave.strNewName.sValue) != 0)
						{
							Vis.PageProduction.FileList.bGetListSource = 1;
						}
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_PRODUCTION, 0);
					}
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_PRODUCTION_DELETE:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 0;
				brsmemset((UDINT)&Vis.PageProductionDelete.strName.sValue, 0, sizeof(Vis.PageProductionDelete.strName.sValue));
				strncpy(Vis.PageProductionDelete.strName.sValue, Vis.PageProduction.FileList.sSelectedProductName, sizeof(Vis.PageProductionDelete.strName.sValue));
				BrbVc4SetControlEnability(&Vis.PageProductionDelete.btnCancel.nStatus, 1);
				BrbVc4SetControlEnability(&Vis.PageProductionDelete.btnDelete.nStatus, 1);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				if(BrbVc4HandleButton(&Vis.PageProductionDelete.btnCancel) == 1)
				{
					Vis.PageProductionDelete.btnCancel.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_PRODUCTION, 0);
				}
				if(BrbVc4HandleButton(&Vis.PageProductionDelete.btnDelete) == 1)
				{
					Vis.PageProductionDelete.btnDelete.bClicked = 0;
					if(BrbSetCaller(&gParHandling.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						strcpy(gParHandling.Par.sFileName, Vis.PageProductionDelete.strName.sValue);
						gParHandling.CallerBox.bDeleteProductPar = 1;
						BrbVc4SetControlEnability(&Vis.PageProductionDelete.btnCancel.nStatus, 0);
						BrbVc4SetControlEnability(&Vis.PageProductionDelete.btnDelete.nStatus, 0);
						Vis.PageProductionDelete.bWaitForDelete = 1;
					}
				}
				if(Vis.PageProductionDelete.bWaitForDelete == 1)
				{
					// Der Sprung auf die Listen-Seite darf erst gemacht werden, wenn die Aktion beendet ist
					if(gParHandling.CallerBox.bDeleteProductPar == 0)
					{
						Vis.PageProductionDelete.bWaitForDelete = 0;
						BrbVc4SetControlEnability(&Vis.PageProductionDelete.btnCancel.nStatus, 1);
						BrbVc4SetControlEnability(&Vis.PageProductionDelete.btnDelete.nStatus, 1);
						Vis.PageProduction.FileList.bGetListSource = 1;
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_PRODUCTION, 0);
					}
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CONFIGURATION1:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
				Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
				Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
				Vis.Footer.bShowStopStart = 1;
				Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
				SetNavigationButtons(0, Vis.GeneralOwn.nUserLevel >= eUSERLEVEL_SERVICE, 0, 0);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Sprache
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.shLanguageGerman.nStatus, Vis.General.nLanguageCurrent == 1);
				if(Vis.PageConfiguration.hsLanguageGerman.bClicked == 1)
				{
					Vis.PageConfiguration.hsLanguageGerman.bClicked = 0;
					Vis.General.nLanguageChange = 1;
				}
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.shLanguageEnglish.nStatus, Vis.General.nLanguageCurrent == 0);
				if(Vis.PageConfiguration.hsLanguageEnglish.bClicked == 1)
				{
					Vis.PageConfiguration.hsLanguageEnglish.bClicked = 0;
					Vis.General.nLanguageChange = 0;
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CONFIGURATION2:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
				Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
				Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
				Vis.Footer.bShowStopStart = 1;
				Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
				SetNavigationButtons(1, 1, 0, 0);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Tanks
				BrbVc4SetControlEnability(&Vis.PageConfiguration.chkTankGlueEnable.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkTankGlueEnable) == 1)
				{
					gPar.MachinePar.TankType.bTankGlueEn = Vis.PageConfiguration.chkTankGlueEnable.bChecked;
				}
				else
				{
					Vis.PageConfiguration.chkTankGlueEnable.bChecked = gPar.MachinePar.TankType.bTankGlueEn;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.chkTankCleanEnable.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkTankCleanEnable) == 1)
				{
					gPar.MachinePar.TankType.bTankCleanEn = Vis.PageConfiguration.chkTankCleanEnable.bChecked;
				}
				else
				{
					Vis.PageConfiguration.chkTankCleanEnable.bChecked = gPar.MachinePar.TankType.bTankCleanEn;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.chkTankSoftEnable.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkTankSoftEnable) == 1)
				{
					gPar.MachinePar.TankType.bTankSoftEn = Vis.PageConfiguration.chkTankSoftEnable.bChecked;
				}
				else
				{
					Vis.PageConfiguration.chkTankSoftEnable.bChecked = gPar.MachinePar.TankType.bTankSoftEn;
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CONFIGURATION3:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
				Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
				Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
				Vis.Footer.bShowStopStart = 1;
				Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
				SetNavigationButtons(1, 1, 0, 0);
				if(Vis.PageConfiguration.nSelectedHeadIndex > gPar.MachinePar.nBonDevIndex)
				{
					Vis.PageConfiguration.nSelectedHeadIndex = gPar.MachinePar.nBonDevIndex;
				}
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// K�pfe
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnHeadLeft.nStatus, Vis.PageConfiguration.nSelectedHeadIndex > 0);
				if(BrbVc4HandleButton(&Vis.PageConfiguration.btnHeadLeft) == 1)
				{
					Vis.PageConfiguration.btnHeadLeft.bClicked = 0;
					Vis.PageConfiguration.nSelectedHeadIndex -= 1;
				}
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnHeadRight.nStatus, Vis.PageConfiguration.nSelectedHeadIndex < gPar.MachinePar.nBonDevIndex);
				if(BrbVc4HandleButton(&Vis.PageConfiguration.btnHeadRight) == 1)
				{
					Vis.PageConfiguration.btnHeadRight.bClicked = 0;
					Vis.PageConfiguration.nSelectedHeadIndex += 1;
				}
				if(Vis.PageConfiguration.nSelectedHeadIndex < 0)
				{
					Vis.PageConfiguration.nSelectedHeadIndex = 0;
				}
				else if(Vis.PageConfiguration.nSelectedHeadIndex > gPar.MachinePar.nBonDevIndex)
				{
					Vis.PageConfiguration.nSelectedHeadIndex = gPar.MachinePar.nBonDevIndex;
				}
				if(gPar.MachinePar.nTriggerSenIndex > -1)
				{
					Vis.PageConfiguration.ddHeadSensorType.nOptions[eSEN_TYPE_TRIGGER_SENSOR] = eBRB_DD_OPTION_NORMAL;
				}
				else
				{
					Vis.PageConfiguration.ddHeadSensorType.nOptions[eSEN_TYPE_TRIGGER_SENSOR] = eBRB_DD_OPTION_DISABLED;
				}
				if(gPar.MachinePar.nEncoderIndex > -1)
				{
					Vis.PageConfiguration.ddHeadSensorType.nOptions[eSEN_TYPE_ENCODER] = eBRB_DD_OPTION_NORMAL;
				}
				else
				{
					Vis.PageConfiguration.ddHeadSensorType.nOptions[eSEN_TYPE_ENCODER] = eBRB_DD_OPTION_DISABLED;
				}
				if(gPar.MachinePar.nPrintMarkSensorIndex > -1)
				{
					Vis.PageConfiguration.ddHeadSensorType.nOptions[eSEN_TYPE_PRINT_MARK_SENSOR] = eBRB_DD_OPTION_NORMAL;
				}
				else
				{
					Vis.PageConfiguration.ddHeadSensorType.nOptions[eSEN_TYPE_PRINT_MARK_SENSOR] = eBRB_DD_OPTION_DISABLED;
				}
				if(gPar.MachinePar.nTriggerSenIndex > -1 && gPar.MachinePar.nPrintMarkSensorIndex > -1)
				{
					Vis.PageConfiguration.ddHeadSensorType.nOptions[eSEN_TYPE_PRINT_MARK_TRIG_SENSOR] = eBRB_DD_OPTION_NORMAL;
				}
				else
				{
					Vis.PageConfiguration.ddHeadSensorType.nOptions[eSEN_TYPE_PRINT_MARK_TRIG_SENSOR] = eBRB_DD_OPTION_DISABLED;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.ddHeadSensorType.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(BrbVc4HandleDropdown(&Vis.PageConfiguration.ddHeadSensorType) == 1)
				{
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType = Vis.PageConfiguration.ddHeadSensorType.nIndex;
				}
				else
				{
					Vis.PageConfiguration.ddHeadSensorType.nIndex = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType;
				}
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.layHeadTrigger.nStatus, gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR);
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.layHeadEncoder.nStatus, gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_ENCODER);
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.layHeadPrintmark.nStatus, gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR);
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.layHeadPrintmarkTrigger.nStatus, gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR);
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadDistanceHeadSensor.nStatus, Vis.PageConfiguration.bInputAllowed == 1 || Vis.PageConfiguration.bInputAllowedInAutomatic);
				if(Vis.PageConfiguration.numHeadDistanceHeadSensor.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadDistanceHeadSensor.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rSenOffset = Vis.PageConfiguration.numHeadDistanceHeadSensor.rValue;
					if(gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR)
					{
						gTriggerSensor.DirectBox.bUpdateBonDevPar = 1;
					}
					else if(gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR)
					{
						gPrintMarkSensor.DirectBox.bUpdateBonDevPar = 1;
					}
					else if(gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR)
					{
						gTriggerSensor.DirectBox.bUpdateBonDevPar = 1;
						gPrintMarkSensor.DirectBox.bUpdateBonDevPar = 1;
					}
				}
				else
				{
					Vis.PageConfiguration.numHeadDistanceHeadSensor.rValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rSenOffset;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadTriggerSensorNumber.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(Vis.PageConfiguration.numHeadTriggerSensorNumber.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadTriggerSensorNumber.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].nIdxSenSource = Vis.PageConfiguration.numHeadTriggerSensorNumber.nValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadTriggerSensorNumber.nValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].nIdxSenSource;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadPrintMarkSensorNumber.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(Vis.PageConfiguration.numHeadPrintMarkSensorNumber.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadPrintMarkSensorNumber.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].nIdxSenSource = Vis.PageConfiguration.numHeadPrintMarkSensorNumber.nValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadPrintMarkSensorNumber.nValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].nIdxSenSource;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadEncoderNumber.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(Vis.PageConfiguration.numHeadEncoderNumber.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadEncoderNumber.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].nIdxEncoder = Vis.PageConfiguration.numHeadEncoderNumber.nValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadEncoderNumber.nValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].nIdxEncoder;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadCylinderPerimeter.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(Vis.PageConfiguration.numHeadCylinderPerimeter.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadCylinderPerimeter.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rCylinderPerimeter = Vis.PageConfiguration.numHeadCylinderPerimeter.rValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadCylinderPerimeter.rValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rCylinderPerimeter;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadPrintMarkEncoder.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(Vis.PageConfiguration.numHeadPrintMarkEncoder.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadPrintMarkEncoder.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].nIdxPrintMarkEncoder = Vis.PageConfiguration.numHeadPrintMarkEncoder.nValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadPrintMarkEncoder.nValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].nIdxPrintMarkEncoder;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadPrintMarkSource.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(Vis.PageConfiguration.numHeadPrintMarkSource.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadPrintMarkSource.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].nIdxPrintMarkSensorSource = Vis.PageConfiguration.numHeadPrintMarkSource.nValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadPrintMarkSource.nValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].nIdxPrintMarkSensorSource;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadDistancePrintTrigSensor.nStatus, Vis.PageConfiguration.bInputAllowed == 1 || Vis.PageConfiguration.bInputAllowedInAutomatic);
				if(Vis.PageConfiguration.numHeadDistancePrintTrigSensor.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadDistancePrintTrigSensor.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rDistancePrintMarkTriggerSensor = Vis.PageConfiguration.numHeadDistancePrintTrigSensor.rValue;
					gTriggerSensor.DirectBox.bUpdateBonDevPar = 1;
				}
				else
				{
					Vis.PageConfiguration.numHeadDistancePrintTrigSensor.rValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rDistancePrintMarkTriggerSensor;
				}
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.numHeadDistancePrintVelocityJump.nStatus, Vis.PageConfiguration.numHeadEncoderNumber.nValue != Vis.PageConfiguration.numHeadPrintMarkEncoder.nValue);
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadDistancePrintVelocityJump.nStatus, Vis.PageConfiguration.bInputAllowed == 1 || Vis.PageConfiguration.bInputAllowedInAutomatic);
				if(Vis.PageConfiguration.numHeadDistancePrintVelocityJump.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadDistancePrintVelocityJump.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rDistancePrintMark_VelocityJump = Vis.PageConfiguration.numHeadDistancePrintVelocityJump.rValue;
					gPrintMarkSensor.DirectBox.bUpdateBonDevPar = 1;
				}
				else
				{
					Vis.PageConfiguration.numHeadDistancePrintVelocityJump.rValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rDistancePrintMark_VelocityJump;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadReduceProductLenPercent.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(Vis.PageConfiguration.numHeadReduceProductLenPercent.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadReduceProductLenPercent.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rReduceProductLenPercent = Vis.PageConfiguration.numHeadReduceProductLenPercent.rValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadReduceProductLenPercent.rValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rReduceProductLenPercent;
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CONFIGURATION4:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
				Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
				Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
				Vis.Footer.bShowStopStart = 1;
				Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
				SetNavigationButtons(1, 1, 0, 0);
				if(Vis.PageConfiguration.nSelectedHeadIndex > gPar.MachinePar.nBonDevIndex)
				{
					Vis.PageConfiguration.nSelectedHeadIndex = gPar.MachinePar.nBonDevIndex;
				}
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// K�pfe
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnHeadLeft.nStatus, Vis.PageConfiguration.nSelectedHeadIndex > 0);
				if(BrbVc4HandleButton(&Vis.PageConfiguration.btnHeadLeft) == 1)
				{
					Vis.PageConfiguration.btnHeadLeft.bClicked = 0;
					Vis.PageConfiguration.nSelectedHeadIndex -= 1;
				}
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnHeadRight.nStatus, Vis.PageConfiguration.nSelectedHeadIndex < gPar.MachinePar.nBonDevIndex);
				if(BrbVc4HandleButton(&Vis.PageConfiguration.btnHeadRight) == 1)
				{
					Vis.PageConfiguration.btnHeadRight.bClicked = 0;
					Vis.PageConfiguration.nSelectedHeadIndex += 1;
				}
				if(Vis.PageConfiguration.nSelectedHeadIndex < 0)
				{
					Vis.PageConfiguration.nSelectedHeadIndex = 0;
				}
				else if(Vis.PageConfiguration.nSelectedHeadIndex > gPar.MachinePar.nBonDevIndex)
				{
					Vis.PageConfiguration.nSelectedHeadIndex = gPar.MachinePar.nBonDevIndex;
				}
				Vis.PageConfiguration.ddHeadSensorType.nIndex = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType;
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadValveCompOpenTime.nStatus, Vis.PageConfiguration.bInputAllowed == 1 || Vis.PageConfiguration.bInputAllowedInAutomatic);
				if(Vis.PageConfiguration.numHeadValveCompOpenTime.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadValveCompOpenTime.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rValveCompensationOpenTime = Vis.PageConfiguration.numHeadValveCompOpenTime.rValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadValveCompOpenTime.rValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rValveCompensationOpenTime;
				}
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadValveCompCloseTime.nStatus, Vis.PageConfiguration.bInputAllowed == 1 || Vis.PageConfiguration.bInputAllowedInAutomatic);
				if(Vis.PageConfiguration.numHeadValveCompCloseTime.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadValveCompCloseTime.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rValveCompensationCloseTime = Vis.PageConfiguration.numHeadValveCompCloseTime.rValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadValveCompCloseTime.rValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].rValveCompensationCloseTime;
				}
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.chkHeadProductDetectionOn.nStatus, gPar.MachinePar.nProductDetectIndex > -1 && gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType != eSEN_TYPE_ENCODER);
				BrbVc4SetControlEnability(&Vis.PageConfiguration.chkHeadProductDetectionOn.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkHeadProductDetectionOn) == 1)
				{
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].ProductDetection.bProductDetExist = Vis.PageConfiguration.chkHeadProductDetectionOn.bChecked;
				}
				else
				{
					Vis.PageConfiguration.chkHeadProductDetectionOn.bChecked = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].ProductDetection.bProductDetExist;
				}
				if(gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].eIdxSenType == eSEN_TYPE_ENCODER)
				{
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].ProductDetection.bProductDetExist = 0;
				}
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.numHeadProductDetectSensorNumber.nStatus, gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].ProductDetection.bProductDetExist == 1);
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadProductDetectSensorNumber.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(Vis.PageConfiguration.numHeadProductDetectSensorNumber.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadProductDetectSensorNumber.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].ProductDetection.nIdxProductDetect = Vis.PageConfiguration.numHeadProductDetectSensorNumber.nValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadProductDetectSensorNumber.nValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].ProductDetection.nIdxProductDetect;
				}
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.numHeadProductDetectSensOffset.nStatus, gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].ProductDetection.bProductDetExist == 1);
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numHeadProductDetectSensOffset.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(Vis.PageConfiguration.numHeadProductDetectSensOffset.bInputCompleted == 1)
				{
					Vis.PageConfiguration.numHeadProductDetectSensOffset.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].ProductDetection.nOffset = Vis.PageConfiguration.numHeadProductDetectSensOffset.nValue;
				}
				else
				{
					Vis.PageConfiguration.numHeadProductDetectSensOffset.nValue = gPar.ProductPar.BonDevPar[Vis.PageConfiguration.nSelectedHeadIndex].ProductDetection.nOffset;
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CONFIGURATION5:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
				Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
				Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
				Vis.Footer.bShowStopStart = 1;
				Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
				SetNavigationButtons(1, 1, 0, 0);
				if(gPar.MachinePar.nTriggerSenIndex > -1 && Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue == -1)
				{
					Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue = 0;
				}
				else if(Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue > gPar.MachinePar.nTriggerSenIndex)
				{
					Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue = gPar.MachinePar.nEncoderIndex;
				}
				if(gPar.MachinePar.nEncoderIndex > -1 && Vis.PageConfiguration.numSelectedEncoderIndex.nValue == -1)
				{
					Vis.PageConfiguration.numSelectedEncoderIndex.nValue = 0;
				}
				else if(Vis.PageConfiguration.numSelectedEncoderIndex.nValue > gPar.MachinePar.nEncoderIndex)
				{
					Vis.PageConfiguration.numSelectedEncoderIndex.nValue = gPar.MachinePar.nEncoderIndex;
				}
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Trigger-Sensor
				if(gPar.MachinePar.nTriggerSenIndex > -1)
				{
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numSelectedTriggerSensorIndex.nStatus, 1);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnTriggerSensorLeft.nStatus, Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue > 0);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnTriggerSensorLeft) == 1)
					{
						Vis.PageConfiguration.btnTriggerSensorLeft.bClicked = 0;
						Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue -= 1;
					}
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnTriggerSensorRight.nStatus, Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue < gPar.MachinePar.nTriggerSenIndex);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnTriggerSensorRight) == 1)
					{
						Vis.PageConfiguration.btnTriggerSensorRight.bClicked = 0;
						Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue += 1;
					}
					if(Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue < 0)
					{
						Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue = 0;
					}
					else if(Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue > gPar.MachinePar.nTriggerSenIndex)
					{
						Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue = gPar.MachinePar.nTriggerSenIndex;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.optTriggerSensorEdgeDetectPos.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
					if(BrbVc4HandleOptionbox(&Vis.PageConfiguration.optTriggerSensorEdgeDetectPos) == 1)
					{
						gPar.ProductPar.TriggerSenPar.bPosNegDetection[Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue] = 0;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.optTriggerSensorEdgeDetectNeg.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
					if(BrbVc4HandleOptionbox(&Vis.PageConfiguration.optTriggerSensorEdgeDetectNeg) == 1)
					{
						gPar.ProductPar.TriggerSenPar.bPosNegDetection[Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue] = 1;
					}
					if(gPar.ProductPar.TriggerSenPar.bPosNegDetection[Vis.PageConfiguration.numSelectedTriggerSensorIndex.nValue] == 0)
					{
						Vis.PageConfiguration.optTriggerSensorEdgeDetectPos.bChecked = 1;
						Vis.PageConfiguration.optTriggerSensorEdgeDetectNeg.bChecked = 0;
					}
					else
					{
						Vis.PageConfiguration.optTriggerSensorEdgeDetectPos.bChecked = 0;
						Vis.PageConfiguration.optTriggerSensorEdgeDetectNeg.bChecked = 1;
					}
				}
				else
				{
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numSelectedTriggerSensorIndex.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnTriggerSensorLeft.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnTriggerSensorRight.nStatus, 0);
					BrbVc4SetControlEnability(&Vis.PageConfiguration.optTriggerSensorEdgeDetectPos.nStatus, 0);
					Vis.PageConfiguration.optTriggerSensorEdgeDetectPos.bChecked = 0;
					BrbVc4SetControlEnability(&Vis.PageConfiguration.optTriggerSensorEdgeDetectNeg.nStatus, 0);
					Vis.PageConfiguration.optTriggerSensorEdgeDetectNeg.bChecked = 0;
				}
				// Encoder
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numSelectedEncoderIndex.nStatus, 1);
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnEncoderLeft.nStatus, Vis.PageConfiguration.numSelectedEncoderIndex.nValue > 0);
				if(BrbVc4HandleButton(&Vis.PageConfiguration.btnEncoderLeft) == 1)
				{
					Vis.PageConfiguration.btnEncoderLeft.bClicked = 0;
					Vis.PageConfiguration.numSelectedEncoderIndex.nValue -= 1;
				}
				BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnEncoderRight.nStatus, Vis.PageConfiguration.numSelectedEncoderIndex.nValue < gPar.MachinePar.nEncoderIndex);
				if(BrbVc4HandleButton(&Vis.PageConfiguration.btnEncoderRight) == 1)
				{
					Vis.PageConfiguration.btnEncoderRight.bClicked = 0;
					Vis.PageConfiguration.numSelectedEncoderIndex.nValue += 1;
				}
				if(Vis.PageConfiguration.numSelectedEncoderIndex.nValue < 0)
				{
					Vis.PageConfiguration.numSelectedEncoderIndex.nValue = 0;
				}
				else if(Vis.PageConfiguration.numSelectedEncoderIndex.nValue > gPar.MachinePar.nEncoderIndex)
				{
					Vis.PageConfiguration.numSelectedEncoderIndex.nValue = gPar.MachinePar.nEncoderIndex;
				}
				if(Vis.PageConfiguration.numSelectedEncoderIndex.nValue == 0)
				{
					// Simulierter Geber
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderSim.nStatus, 1);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderReal.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderVirtual.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderTrigger.nStatus, 0);
					BrbVc4SetControlEnability(&Vis.PageConfiguration.chkEncoderSimEnable.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
					if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkEncoderSimEnable) == 1)
					{
						gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].bSimulationEnable = Vis.PageConfiguration.chkEncoderSimEnable.bChecked;
					}
					else
					{
						Vis.PageConfiguration.chkEncoderSimEnable.bChecked = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].bSimulationEnable;
					}
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.numEncoderIncPerRev.nStatus, 1);
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numEncoderIncPerRev.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
					if(Vis.PageConfiguration.numEncoderIncPerRev.bInputCompleted == 1)
					{
						Vis.PageConfiguration.numEncoderIncPerRev.bInputCompleted = 0;
						gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].nIncPerRev = Vis.PageConfiguration.numEncoderIncPerRev.nValue;
					}
					else
					{
						Vis.PageConfiguration.numEncoderIncPerRev.nValue = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].nIncPerRev;
					}
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.numEncoderDistPerRev.nStatus, 1);
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numEncoderDistPerRev.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
					if(Vis.PageConfiguration.numEncoderDistPerRev.bInputCompleted == 1)
					{
						Vis.PageConfiguration.numEncoderDistPerRev.bInputCompleted = 0;
						gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rDistancePerRev = Vis.PageConfiguration.numEncoderDistPerRev.rValue;
					}
					else
					{
						Vis.PageConfiguration.numEncoderDistPerRev.rValue = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rDistancePerRev;
					}
					//// Neu MZ Anfang:					
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.numEncoderVelocityFactor.nStatus, 1);
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numEncoderVelocityFactor.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
					if(Vis.PageConfiguration.numEncoderVelocityFactor.bInputCompleted == 1)
					{
						Vis.PageConfiguration.numEncoderVelocityFactor.bInputCompleted = 0;
						gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rVelocityFactor = Vis.PageConfiguration.numEncoderVelocityFactor.rValue;
					}
					else
					{
						Vis.PageConfiguration.numEncoderVelocityFactor.rValue = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rVelocityFactor;
					}
					//// Neu MZ Ende	
				}
				else
				{
					// Reeller Geber
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderSim.nStatus, 0);
					// Feststellen, ob Geber schon als Source eines virtuellen Gebers verwendet wird
					BOOL bIsAlreadySourceForVirtualEncoder = 0;
					for(nEncoderIndex=0; nEncoderIndex<=nIDX_ENCODER_MAX; nEncoderIndex++)
					{
						if(gPar.MachinePar.EncoderPar[nEncoderIndex].eEncoderType == eENCODER_TYPE_VIRTUAL && gPar.MachinePar.EncoderPar[nEncoderIndex].nSourceEncoderIdx == 
							Vis.PageConfiguration.numSelectedEncoderIndex.nValue)
						{
							bIsAlreadySourceForVirtualEncoder = 1;
						}
					}
					// Unterscheidung Geber-Typ
					if(bIsAlreadySourceForVirtualEncoder == 0)
					{
						Vis.PageConfiguration.ddEncoderType.nOptions[eENCODER_TYPE_VIRTUAL] = eBRB_DD_OPTION_NORMAL;
						Vis.PageConfiguration.ddEncoderType.nOptions[eENCODER_TYPE_TRIGGER] = eBRB_DD_OPTION_NORMAL;
					}
					else
					{
						Vis.PageConfiguration.ddEncoderType.nOptions[eENCODER_TYPE_VIRTUAL] = eBRB_DD_OPTION_DISABLED;
						Vis.PageConfiguration.ddEncoderType.nOptions[eENCODER_TYPE_TRIGGER] = eBRB_DD_OPTION_DISABLED;
					}
					if(gPar.MachinePar.nTriggerSenIndex == -1)
					{
						Vis.PageConfiguration.ddEncoderType.nOptions[eENCODER_TYPE_TRIGGER] = eBRB_DD_OPTION_DISABLED;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.ddEncoderType.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
					if(BrbVc4HandleDropdown(&Vis.PageConfiguration.ddEncoderType) == 1)
					{
						gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].eEncoderType = Vis.PageConfiguration.ddEncoderType.nIndex;
					}
					else
					{
						Vis.PageConfiguration.ddEncoderType.nIndex = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].eEncoderType;
					}
					if(gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].eEncoderType == eENCODER_TYPE_ENCODER)
					{
						// Normaler Geber
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderReal.nStatus, 1);
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderVirtual.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderTrigger.nStatus, 0);
						BrbVc4SetControlEnability(&Vis.PageConfiguration.numEncoderIncPerRev.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
						if(Vis.PageConfiguration.numEncoderIncPerRev.bInputCompleted == 1)
						{
							Vis.PageConfiguration.numEncoderIncPerRev.bInputCompleted = 0;
							gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].nIncPerRev = Vis.PageConfiguration.numEncoderIncPerRev.nValue;
						}
						else
						{
							Vis.PageConfiguration.numEncoderIncPerRev.nValue = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].nIncPerRev;
						}
						BrbVc4SetControlEnability(&Vis.PageConfiguration.numEncoderDistPerRev.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
						if(Vis.PageConfiguration.numEncoderDistPerRev.bInputCompleted == 1)
						{
							Vis.PageConfiguration.numEncoderDistPerRev.bInputCompleted = 0;
							gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rDistancePerRev = Vis.PageConfiguration.numEncoderDistPerRev.rValue;
						}
						else
						{
							Vis.PageConfiguration.numEncoderDistPerRev.rValue = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rDistancePerRev;
						}
						//// Neu MZ Anfang:
						BrbVc4SetControlEnability(&Vis.PageConfiguration.numEncoderVelocityFactor.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
						if(Vis.PageConfiguration.numEncoderVelocityFactor.bInputCompleted == 1)
						{
							Vis.PageConfiguration.numEncoderVelocityFactor.bInputCompleted = 0;
							gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rVelocityFactor = Vis.PageConfiguration.numEncoderVelocityFactor.rValue;
						}
						else
						{
							Vis.PageConfiguration.numEncoderVelocityFactor.rValue = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rVelocityFactor;
						}
					}
						//// Neu MZ Ende:									
					else if(gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].eEncoderType == eENCODER_TYPE_VIRTUAL)
					{
						// Virtueller Geber
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderReal.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderVirtual.nStatus, 1);
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderTrigger.nStatus, 0);
						for(nEncoderIndex=0; nEncoderIndex<=nIDX_ENCODER_MAX; nEncoderIndex++)
						{
							Vis.PageConfiguration.ddEncoderSource.nOptions[nEncoderIndex] = eBRB_DD_OPTION_INVISIBLE;
							if(nEncoderIndex == 0)
							{
								Vis.PageConfiguration.ddEncoderSource.nOptions[nEncoderIndex] = eBRB_DD_OPTION_NORMAL;
							}
							else if(nEncoderIndex > 0 && nEncoderIndex <= gPar.MachinePar.nEncoderIndex)
							{
								if(gPar.MachinePar.EncoderPar[nEncoderIndex].eEncoderType == eENCODER_TYPE_ENCODER)
								{
									Vis.PageConfiguration.ddEncoderSource.nOptions[nEncoderIndex] = eBRB_DD_OPTION_NORMAL;
								}
							}
						}
						if(Vis.PageConfiguration.ddEncoderSource.bInputCompleted == 1)
						{
							Vis.PageConfiguration.ddEncoderSource.bInputCompleted = 0;
							gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].nSourceEncoderIdx = Vis.PageConfiguration.ddEncoderSource.nIndex;
						}
						else
						{
							Vis.PageConfiguration.ddEncoderSource.nIndex = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].nSourceEncoderIdx;
						}
						BrbVc4SetControlEnability(&Vis.PageConfiguration.numEncoderFactor.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
						if(Vis.PageConfiguration.numEncoderFactor.bInputCompleted == 1)
						{
							Vis.PageConfiguration.numEncoderFactor.bInputCompleted = 0;
							gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rFactor = Vis.PageConfiguration.numEncoderFactor.rValue;
						}
						else
						{
							Vis.PageConfiguration.numEncoderFactor.rValue = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rFactor;
						}
					}
					else if(gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].eEncoderType == eENCODER_TYPE_TRIGGER)
					{
						// Trigger-Geber
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderReal.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderVirtual.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderTrigger.nStatus, 1);
						// Feststellen, ob Triggersensor schon als Quelle verwendet wird
						memset(&Vis.PageConfiguration.ddEncoderTriggerSource.nOptions, 0 ,sizeof(Vis.PageConfiguration.ddEncoderTriggerSource.nOptions));
						for(nEncoderIndex=0; nEncoderIndex<=nIDX_ENCODER_MAX; nEncoderIndex++)
						{
							if(gPar.MachinePar.EncoderPar[nEncoderIndex].eEncoderType == eENCODER_TYPE_TRIGGER && gPar.MachinePar.EncoderPar[nEncoderIndex].nTriggerSensorIdx > -1 && nEncoderIndex != Vis.PageConfiguration.numSelectedEncoderIndex.nValue)
							{
								Vis.PageConfiguration.ddEncoderTriggerSource.nOptions[gPar.MachinePar.EncoderPar[nEncoderIndex].nTriggerSensorIdx+1] = eBRB_DD_OPTION_DISABLED;
							}
						}
						BrbVc4SetControlEnability(&Vis.PageConfiguration.ddEncoderTriggerSource.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
						Vis.PageConfiguration.ddEncoderTriggerSource.nIndexMax = gPar.MachinePar.nTriggerSenIndex+1;
						if(BrbVc4HandleDropdown(&Vis.PageConfiguration.ddEncoderTriggerSource) == 1)
						{
							gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].nTriggerSensorIdx = Vis.PageConfiguration.ddEncoderTriggerSource.nIndex-1;
						}
						else
						{
							Vis.PageConfiguration.ddEncoderTriggerSource.nIndex = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].nTriggerSensorIdx+1;
						}
						BrbVc4SetControlEnability(&Vis.PageConfiguration.numEncoderTriggerSignalDistance.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
						if(Vis.PageConfiguration.numEncoderTriggerSignalDistance.bInputCompleted == 1)
						{
							Vis.PageConfiguration.numEncoderTriggerSignalDistance.bInputCompleted = 0;
							gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rTriggerSignalDistance = Vis.PageConfiguration.numEncoderTriggerSignalDistance.rValue;
						}
						else
						{
							Vis.PageConfiguration.numEncoderTriggerSignalDistance.rValue = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rTriggerSignalDistance;
						}
						BrbVc4SetControlEnability(&Vis.PageConfiguration.numEncoderTriggerTimeOut.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
						if(Vis.PageConfiguration.numEncoderTriggerTimeOut.bInputCompleted == 1)
						{
							Vis.PageConfiguration.numEncoderTriggerTimeOut.bInputCompleted = 0;
							gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rTriggerSignalTimeOut = Vis.PageConfiguration.numEncoderTriggerTimeOut.rValue;
						}
						else
						{
							Vis.PageConfiguration.numEncoderTriggerTimeOut.rValue = gPar.MachinePar.EncoderPar[Vis.PageConfiguration.numSelectedEncoderIndex.nValue].rTriggerSignalTimeOut;
						}
					}
					else
					{
						// Kein Typ angegeben
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderReal.nStatus, 1);
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderVirtual.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.PageConfiguration.layEncoderTrigger.nStatus, 0);
					}
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CONFIGURATION6:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
				Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
				Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
				Vis.Footer.bShowStopStart = 1;
				Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
				SetNavigationButtons(1, 1, 0, 0);
				if(gPar.MachinePar.nPrintMarkSensorIndex > -1 && Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue == -1)
				{
					Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue = 0;
				}
				else if(Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue > gPar.MachinePar.nPrintMarkSensorIndex)
				{
					Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue = gPar.MachinePar.nPrintMarkSensorIndex;
				}
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Printmark-Sensors
				if(gPar.MachinePar.nPrintMarkSensorIndex > -1)
				{
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nStatus, 1);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnPrintmarkSensorLeft.nStatus, Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue > 0);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnPrintmarkSensorLeft) == 1)
					{
						Vis.PageConfiguration.btnPrintmarkSensorLeft.bClicked = 0;
						Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue -= 1;
					}
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnPrintmarkSensorRight.nStatus, Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue < gPar.MachinePar.nPrintMarkSensorIndex);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnPrintmarkSensorRight) == 1)
					{
						Vis.PageConfiguration.btnPrintmarkSensorRight.bClicked = 0;
						Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue += 1;
					}
					if(Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue < 0)
					{
						Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue = 0;
					}
					else if(Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue > gPar.MachinePar.nPrintMarkSensorIndex)
					{
						Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue = gPar.MachinePar.nPrintMarkSensorIndex;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.optPrintmarkSensorEdgeDetectPos.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
					if(BrbVc4HandleOptionbox(&Vis.PageConfiguration.optPrintmarkSensorEdgeDetectPos) == 1)
					{
						gPar.ProductPar.PmSenPar.bPosNegDetection[Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue] = 0;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.optPrintmarkSensorEdgeDetectNeg.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
					if(BrbVc4HandleOptionbox(&Vis.PageConfiguration.optPrintmarkSensorEdgeDetectNeg) == 1)
					{
						gPar.ProductPar.PmSenPar.bPosNegDetection[Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue] = 1;
					}
					if(gPar.ProductPar.PmSenPar.bPosNegDetection[Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nValue] == 0)
					{
						Vis.PageConfiguration.optPrintmarkSensorEdgeDetectPos.bChecked = 1;
						Vis.PageConfiguration.optPrintmarkSensorEdgeDetectNeg.bChecked = 0;
					}
					else
					{
						Vis.PageConfiguration.optPrintmarkSensorEdgeDetectPos.bChecked = 0;
						Vis.PageConfiguration.optPrintmarkSensorEdgeDetectNeg.bChecked = 1;
					}
				}
				else
				{
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnPrintmarkSensorLeft.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnPrintmarkSensorRight.nStatus, 0);
					BrbVc4SetControlEnability(&Vis.PageConfiguration.optPrintmarkSensorEdgeDetectPos.nStatus, 0);
					Vis.PageConfiguration.optPrintmarkSensorEdgeDetectPos.bChecked = 0;
					BrbVc4SetControlEnability(&Vis.PageConfiguration.optPrintmarkSensorEdgeDetectNeg.nStatus, 0);
					Vis.PageConfiguration.optPrintmarkSensorEdgeDetectNeg.bChecked = 0;
				}
				// Printmarks
				BrbVc4SetControlEnability(&Vis.PageConfiguration.numPrintmarkTolerance.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
				if(gPar.MachinePar.nPrintMarkSensorIndex > -1)
				{
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numSelectedPrintmarkIndex.nStatus, 1);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnPrintmarkLeft.nStatus, Vis.PageConfiguration.numSelectedPrintmarkIndex.nValue > 0);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnPrintmarkLeft) == 1)
					{
						Vis.PageConfiguration.btnPrintmarkLeft.bClicked = 0;
						Vis.PageConfiguration.numSelectedPrintmarkIndex.nValue -= 1;
					}
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnPrintmarkRight.nStatus, Vis.PageConfiguration.numSelectedPrintmarkIndex.nValue < nIDX_PRINT_MARK_MAX);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnPrintmarkRight) == 1)
					{
						Vis.PageConfiguration.btnPrintmarkRight.bClicked = 0;
						Vis.PageConfiguration.numSelectedPrintmarkIndex.nValue += 1;
					}
					if(Vis.PageConfiguration.numSelectedPrintmarkIndex.nValue < 0)
					{
						Vis.PageConfiguration.numSelectedPrintmarkIndex.nValue = 0;
					}
					else if(Vis.PageConfiguration.numSelectedPrintmarkIndex.nValue > nIDX_PRINT_MARK_MAX)
					{
						Vis.PageConfiguration.numSelectedPrintmarkIndex.nValue = nIDX_PRINT_MARK_MAX;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numPrintmarkLength.nStatus, Vis.PageConfiguration.bInputAllowed == 1);
					if(Vis.PageConfiguration.numPrintmarkLength.bInputCompleted == 1)
					{
						Vis.PageConfiguration.numPrintmarkLength.bInputCompleted = 0;
						gPar.MachinePar.PrintMarkPar.nPrintMarkLen[Vis.PageConfiguration.numSelectedPrintmarkIndex.nValue] = Vis.PageConfiguration.numPrintmarkLength.nValue;
					}
					else
					{
						Vis.PageConfiguration.numPrintmarkLength.nValue = gPar.MachinePar.PrintMarkPar.nPrintMarkLen[Vis.PageConfiguration.numSelectedPrintmarkIndex.nValue];
					}
				}
				else
				{
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numSelectedPrintmarkIndex.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnPrintmarkLeft.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnPrintmarkRight.nStatus, 0);
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numPrintmarkLength.nStatus, 0);
					Vis.PageConfiguration.numPrintmarkLength.nValue = 0;
				}
				// Navigation
			
			
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
				}
				
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
				}
			
				if(Vis.PageHandling.bPageExit == 1)
				{
					Vis.PageHandling.bPageExit = 0;
				}
				break;

				//	--------------------------------------------------------------------------------------------------------------------------------------------------
				case ePAGE_CONFIGURATION7:
				
					if (gPar.MachinePar.MachineModule.bStitchmodeEn != 1)
					{
						BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
						
					}
					else
					{
						if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
						{
							Vis.PageHandling.bPageInit = 0;
							ResetAllMenuButtons();
							Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
							Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
							Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
							Vis.Footer.bShowStopStart = 1;
							Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
							SetNavigationButtons(1, 1, 0, 0);
						}
						
							if(Vis.PageHandling.bPageChangeInProgress == 0)
							{
								// Menu
								HandleMenu();
								// Stitching
								BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkStitchingEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
								if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkStitchingEnable) == 1)
								{
									gPar.MachinePar.MachineModule.bStitchmodeEn = Vis.PageModuleConfig.chkStitchingEnable.bChecked;
								}
								else
								{
									Vis.PageModuleConfig.chkStitchingEnable.bChecked = gPar.MachinePar.MachineModule.bStitchmodeEn;
								}
								BrbVc4SetControlEnability(&Vis.PageConfiguration.nStitchingInputStatus, gPar.MachinePar.MachineModule.bStitchmodeEn == 1);
					
						
			
			
			//Navigation basierend auf Module Konfiguration	
			//Links
			if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
			{
				Vis.Navigation.btnLeft.bClicked = 0;
				BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
			}
									
			//Rechts
			if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
			{     
				Vis.Navigation.btnRight.bClicked = 0;
			
				if(gPar.MachinePar.MachineModule.bCleaningEn== 1)
					{
							
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION8, 0);
					}
							
				else if(gPar.MachinePar.MachineModule.bPressureCtrlEn== 1)
					{
							
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION9, 0);
					}
								
				else if(gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn== 1)
					{
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION10, 0);
					}
							
				else if(gPar.MachinePar.MachineModule.bImgProcessingEn== 1) 
						
					{
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION12, 0);
					}
				else if(gPar.MachinePar.MachineModule.bTempCtrlEn== 1) 
					{
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION13, 0);
					}
							
				else if(gPar.MachinePar.MachineModule.bAnalogFuellStandEn== 1) 
					{
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION15, 0);
					}
				    else
						
					{
						BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1,0);
					}
			}					
						
		}			
			
	}
				if(Vis.PageHandling.bPageExit == 1)
				{
					Vis.PageHandling.bPageExit = 0;
				}
				break;

				//	--------------------------------------------------------------------------------------------------------------------------------------------------
				case ePAGE_CONFIGURATION8:
			
					if ( gPar.MachinePar.MachineModule.bCleaningEn==1)
					
					{
			
						if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
						{
							Vis.PageHandling.bPageInit = 0;
							ResetAllMenuButtons();
							Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
							Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
							Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
							Vis.Footer.bShowStopStart = 1;
							Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
							SetNavigationButtons(1, 1, 0, 0);
						}
						if(Vis.PageHandling.bPageChangeInProgress == 0)
						{
							// Menu
							HandleMenu();
							// Cleaning
							BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkCleaningEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
							if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkCleaningEnable) == 1)
							{
								gPar.MachinePar.MachineModule.bCleaningEn = Vis.PageModuleConfig.chkCleaningEnable.bChecked;
							}
							else
							{
								Vis.PageModuleConfig.chkCleaningEnable.bChecked = gPar.MachinePar.MachineModule.bCleaningEn;
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.nCleaningInputStatus, gPar.MachinePar.MachineModule.bCleaningEn == 1);
							BrbVc4SetControlVisibility(&Vis.PageConfiguration.txtNoCleaningTank.nStatus, gPar.MachinePar.TankType.bTankCleanEn == 0);
							BrbVc4SetControlVisibility(&Vis.PageConfiguration.chkCleaningAutoValveControl.nStatus, gPar.MachinePar.TankType.bTankCleanEn == 1);
							if(gPar.MachinePar.TankType.bTankCleanEn == 0 || gPar.MachinePar.MachineModule.bCleaningEn == 0)
							{
								gPar.MachinePar.TankType.bAutoValveCtrl = 0;
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.chkCleaningAutoValveControl.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bCleaningEn == 1);
							if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkCleaningAutoValveControl) == 1)
							{
								gPar.MachinePar.TankType.bAutoValveCtrl = Vis.PageConfiguration.chkCleaningAutoValveControl.bChecked;
							}
							else
							{
								Vis.PageConfiguration.chkCleaningAutoValveControl.bChecked = gPar.MachinePar.TankType.bAutoValveCtrl;
							}
							// SystemCleaning
							BrbVc4SetControlEnability(&Vis.PageConfiguration.chkSystemCleaningEnable.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bCleaningEn == 1);
							if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkSystemCleaningEnable) == 1)
							{
								gPar.MachinePar.SystemCleaning.bEnable = Vis.PageConfiguration.chkSystemCleaningEnable.bChecked;
							}
							else
							{
								Vis.PageConfiguration.chkSystemCleaningEnable.bChecked = gPar.MachinePar.SystemCleaning.bEnable;
							}
						
							// Navigation basierend auf Module Konfiguration	
							//Links
							if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
							{
								Vis.Navigation.btnLeft.bClicked = 0;
								if (gPar.MachinePar.MachineModule.bStitchmodeEn == 1)
								{
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
								}
								else 
								{
									BrbVc4ChangePage(&Vis.PageHandling,Vis.PageHandling.nPageCurrent-2 , 0);
								}
							}	
			
							//Rechts
							if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
							{     
								Vis.Navigation.btnRight.bClicked = 0;
							
								if(gPar.MachinePar.MachineModule.bPressureCtrlEn== 1)
								{
							
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION9, 0);
								}
								
							
								else if(gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn== 1)
								{
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION10, 0);
								}
							
								else if(gPar.MachinePar.MachineModule.bImgProcessingEn== 1) 
						
								{
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION12, 0);
								}
							
									
								else if(gPar.MachinePar.MachineModule.bTempCtrlEn== 1) 
						
								{
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION13, 0);
								}
							
									
								else if(gPar.MachinePar.MachineModule.bAnalogFuellStandEn== 1) 
							
								{
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION15, 0);
								}
								else
						
								{
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1,0);
								}
							}
						
						
			
						}
			
					}
			
					else
					{
						BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
					}
			
				if(Vis.PageHandling.bPageExit == 1)
				{
					Vis.PageHandling.bPageExit = 0;
				}
				break;

				//	--------------------------------------------------------------------------------------------------------------------------------------------------
				case ePAGE_CONFIGURATION9:
				
					if ( gPar.MachinePar.MachineModule.bPressureCtrlEn!=1 )
					{
						BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
						
					}
			
					else
					{
						if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
						{
							Vis.PageHandling.bPageInit = 0;
							ResetAllMenuButtons();
							Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
							Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
							Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
							Vis.Footer.bShowStopStart = 1;
							Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
							SetNavigationButtons(1, 1, 0, 0);
							if(gPar.MachinePar.nEncoderIndex > -1 && Vis.PageConfiguration.numSelectedEncoderIndex.nValue == -1)
							{
								Vis.PageConfiguration.numSelectedEncoderIndex.nValue = 0;
							}
							else if(Vis.PageConfiguration.numSelectedEncoderIndex.nValue > gPar.MachinePar.nEncoderIndex)
							{
								Vis.PageConfiguration.numSelectedEncoderIndex.nValue = gPar.MachinePar.nEncoderIndex;
							}
						}
						if(Vis.PageHandling.bPageChangeInProgress == 0)
						{
							// Menu
							HandleMenu();
							// Pressure Control
							BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkPressureControlEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
							if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkPressureControlEnable) == 1)
							{
								gPar.MachinePar.MachineModule.bPressureCtrlEn = Vis.PageModuleConfig.chkPressureControlEnable.bChecked;
							}
							else
							{
								Vis.PageModuleConfig.chkPressureControlEnable.bChecked = gPar.MachinePar.MachineModule.bPressureCtrlEn;
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.nPressureControlInputStatus, gPar.MachinePar.MachineModule.bPressureCtrlEn == 1);
							
							// Navigation basierend auf Module Konfiguration	
							
							// Links 
							if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
							{
								Vis.Navigation.btnLeft.bClicked = 0;
								
								if (gPar.MachinePar.MachineModule.bCleaningEn== 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
								}
									
				
								else if (gPar.MachinePar.MachineModule.bStitchmodeEn == 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling,Vis.PageHandling.nPageCurrent-2 , 0);
								}
									else
									{
								  		BrbVc4ChangePage(&Vis.PageHandling,Vis.PageHandling.nPageCurrent-3 , 0);
									}
							}	
						
							//Rechts		
							if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
							{     
								Vis.Navigation.btnRight.bClicked = 0;
							
								if(gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn== 1)
								{
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION10, 0);
								}
							
								else if(gPar.MachinePar.MachineModule.bImgProcessingEn== 1) 
						
								{
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION12, 0);
								}
							
									
								else if(gPar.MachinePar.MachineModule.bTempCtrlEn== 1) 
						
								{
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION13, 0);
								}
							
									
								else if(gPar.MachinePar.MachineModule.bAnalogFuellStandEn== 1) 
							
								{
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION15, 0);
								}
								else
						
								{
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1,0);
								}
							}
						}
					} 
			
				if(Vis.PageHandling.bPageExit == 1)
				{
					Vis.PageHandling.bPageExit = 0;
				}
				break;

				//	--------------------------------------------------------------------------------------------------------------------------------------------------
				case ePAGE_CONFIGURATION10:
				
					if ( gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn !=1 )
					{
						BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+2, 0);
						
					}
			
					else
					{
						if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
						{
							Vis.PageHandling.bPageInit = 0;
							ResetAllMenuButtons();
							Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
							Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
							Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
							Vis.Footer.bShowStopStart = 1;
							Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
							SetNavigationButtons(1, 1, 0, 0);
						}
						if(Vis.PageHandling.bPageChangeInProgress == 0)
						{
							// Menu
							HandleMenu();
							// Motorized Adjustment
							BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkMotorizedAdjustmentEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
							if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkMotorizedAdjustmentEnable) == 1)
							{
								gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn = Vis.PageModuleConfig.chkMotorizedAdjustmentEnable.bChecked;
							}
							else
							{
								Vis.PageModuleConfig.chkMotorizedAdjustmentEnable.bChecked = gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn;
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.nMotorizedAdjustmentInputStatus, gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn == 1);
							BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnHeadLeft.nStatus, Vis.PageConfiguration.nSelectedHeadIndex > 0);
							if(BrbVc4HandleButton(&Vis.PageConfiguration.btnHeadLeft) == 1)
							{
								Vis.PageConfiguration.btnHeadLeft.bClicked = 0;
								Vis.PageConfiguration.nSelectedHeadIndex -= 1;
							}
							BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnHeadRight.nStatus, Vis.PageConfiguration.nSelectedHeadIndex < gPar.MachinePar.nBonDevIndex);
							if(BrbVc4HandleButton(&Vis.PageConfiguration.btnHeadRight) == 1)
							{
								Vis.PageConfiguration.btnHeadRight.bClicked = 0;
								Vis.PageConfiguration.nSelectedHeadIndex += 1;
							}
							if(Vis.PageConfiguration.nSelectedHeadIndex < 0)
							{
								Vis.PageConfiguration.nSelectedHeadIndex = 0;
							}
							else if(Vis.PageConfiguration.nSelectedHeadIndex > gPar.MachinePar.nBonDevIndex)
							{
								Vis.PageConfiguration.nSelectedHeadIndex = gPar.MachinePar.nBonDevIndex;
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.chkMotorizedAdjustmentVertical.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn == 1);
							if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkMotorizedAdjustmentVertical) == 1)
							{
								gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[Vis.PageConfiguration.nSelectedHeadIndex].bVertical = Vis.PageConfiguration.chkMotorizedAdjustmentVertical.bChecked;
							}
							else
							{
								Vis.PageConfiguration.chkMotorizedAdjustmentVertical.bChecked = gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[Vis.PageConfiguration.nSelectedHeadIndex].bVertical;
							}
							if(Vis.PageConfiguration.numMotAdjustRefOffsetVertical.bInputCompleted == 1)
							{
								Vis.PageConfiguration.numMotAdjustRefOffsetVertical.bInputCompleted = 0;
								gPar.MachinePar.MotorisedAdjustment.RefPos[Vis.PageConfiguration.nSelectedHeadIndex].rVertical = Vis.PageConfiguration.numMotAdjustRefOffsetVertical.rValue;
							}
							else
							{
								Vis.PageConfiguration.numMotAdjustRefOffsetVertical.rValue = gPar.MachinePar.MotorisedAdjustment.RefPos[Vis.PageConfiguration.nSelectedHeadIndex].rVertical;
							}
							if(Vis.PageConfiguration.numMotAdjustNegLimitVertical.bInputCompleted == 1)
							{
								Vis.PageConfiguration.numMotAdjustNegLimitVertical.bInputCompleted = 0;
								gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageConfiguration.nSelectedHeadIndex].Alignment[eVERTICAL].rNegativeSwLimit = Vis.PageConfiguration.numMotAdjustNegLimitVertical.rValue;
								if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
								{
									gAxisControl.CallerBox.bInitAllAxesLimits = 1;
								}	
							}
							else
							{
								Vis.PageConfiguration.numMotAdjustNegLimitVertical.rValue = gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageConfiguration.nSelectedHeadIndex].Alignment[eVERTICAL].rNegativeSwLimit;
							}
							if(Vis.PageConfiguration.numMotAdjustPosLimitVertical.bInputCompleted == 1)
							{
								Vis.PageConfiguration.numMotAdjustPosLimitVertical.bInputCompleted = 0;
								gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageConfiguration.nSelectedHeadIndex].Alignment[eVERTICAL].rPositiveSwLimit = Vis.PageConfiguration.numMotAdjustPosLimitVertical.rValue;
								if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
								{
									gAxisControl.CallerBox.bInitAllAxesLimits = 1;
								}	
							}
							else
							{
								Vis.PageConfiguration.numMotAdjustPosLimitVertical.rValue = gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageConfiguration.nSelectedHeadIndex].Alignment[eVERTICAL].rPositiveSwLimit;
							}
							// Navigation basierend auf Module Konfiguration	
							
							//Links
							
							if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
							{
								Vis.Navigation.btnLeft.bClicked = 0;
								
								if (gPar.MachinePar.MachineModule.bPressureCtrlEn== 1)
							
								{
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
								}
									
								else if (gPar.MachinePar.MachineModule.bCleaningEn== 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-2, 0);
								}
									
				
								else if (gPar.MachinePar.MachineModule.bStitchmodeEn == 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling,Vis.PageHandling.nPageCurrent-3 , 0);
								}
								else
								{
									BrbVc4ChangePage(&Vis.PageHandling,Vis.PageHandling.nPageCurrent-4 , 0);
								}
							}
							
						//Rechts		
							if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
							{     
								Vis.Navigation.btnRight.bClicked = 0;
								
								BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION11, 0);
			
							}			
							
						}
					
					}	
							
							
				if(Vis.PageHandling.bPageExit == 1)
				{
					Vis.PageHandling.bPageExit = 0;
				}
				break;

				//	--------------------------------------------------------------------------------------------------------------------------------------------------
				case ePAGE_CONFIGURATION11:
				
					if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
					{
						Vis.PageHandling.bPageInit = 0;
						ResetAllMenuButtons();
						Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
						Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
						Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
						Vis.Footer.bShowStopStart = 1;
						Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
						SetNavigationButtons(1, 1, 0, 0);
					}
				if(Vis.PageHandling.bPageChangeInProgress == 0)
				{
					// Menu
					HandleMenu();
					// Motorized Adjustment
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnHeadLeft.nStatus, Vis.PageConfiguration.nSelectedHeadIndex > 0);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnHeadLeft) == 1)
					{
						Vis.PageConfiguration.btnHeadLeft.bClicked = 0;
						Vis.PageConfiguration.nSelectedHeadIndex -= 1;
					}
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnHeadRight.nStatus, Vis.PageConfiguration.nSelectedHeadIndex < gPar.MachinePar.nBonDevIndex);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnHeadRight) == 1)
					{
						Vis.PageConfiguration.btnHeadRight.bClicked = 0;
						Vis.PageConfiguration.nSelectedHeadIndex += 1;
					}
					if(Vis.PageConfiguration.nSelectedHeadIndex < 0)
					{
						Vis.PageConfiguration.nSelectedHeadIndex = 0;
					}
					else if(Vis.PageConfiguration.nSelectedHeadIndex > gPar.MachinePar.nBonDevIndex)
					{
						Vis.PageConfiguration.nSelectedHeadIndex = gPar.MachinePar.nBonDevIndex;
					}
					BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkMotorizedAdjustmentEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkMotorizedAdjustmentEnable) == 1)
					{
						gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn = Vis.PageModuleConfig.chkMotorizedAdjustmentEnable.bChecked;
					}
					else
					{
						Vis.PageModuleConfig.chkMotorizedAdjustmentEnable.bChecked = gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.nMotorizedAdjustmentInputStatus, gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn == 1);
					BrbVc4SetControlEnability(&Vis.PageConfiguration.chkMotorizedAdjustmentHorizontal.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn == 1);
					if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkMotorizedAdjustmentHorizontal) == 1)
					{
						gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[Vis.PageConfiguration.nSelectedHeadIndex].bHorizontal = Vis.PageConfiguration.chkMotorizedAdjustmentHorizontal.bChecked;
					}
					else
					{
						Vis.PageConfiguration.chkMotorizedAdjustmentHorizontal.bChecked = gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[Vis.PageConfiguration.nSelectedHeadIndex].bHorizontal;
					}
					if(Vis.PageConfiguration.numMotAdjustRefOffsetHorizontal.bInputCompleted == 1)
					{
						Vis.PageConfiguration.numMotAdjustRefOffsetHorizontal.bInputCompleted = 0;
						gPar.MachinePar.MotorisedAdjustment.RefPos[Vis.PageConfiguration.nSelectedHeadIndex].rHorizontal = Vis.PageConfiguration.numMotAdjustRefOffsetHorizontal.rValue;
					}
					else
					{
						Vis.PageConfiguration.numMotAdjustRefOffsetHorizontal.rValue = gPar.MachinePar.MotorisedAdjustment.RefPos[Vis.PageConfiguration.nSelectedHeadIndex].rHorizontal;
					}
					if(Vis.PageConfiguration.numMotAdjustNegLimitHorizontal.bInputCompleted == 1)
					{
						Vis.PageConfiguration.numMotAdjustNegLimitHorizontal.bInputCompleted = 0;
						gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageConfiguration.nSelectedHeadIndex].Alignment[eHORIZONTAL].rNegativeSwLimit = Vis.PageConfiguration.numMotAdjustNegLimitHorizontal.rValue;
						if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
						{
							gAxisControl.CallerBox.bInitAllAxesLimits = 1;
						}	
					}
					else
					{
						Vis.PageConfiguration.numMotAdjustNegLimitHorizontal.rValue = gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageConfiguration.nSelectedHeadIndex].Alignment[eHORIZONTAL].rNegativeSwLimit;
					}
					if(Vis.PageConfiguration.numMotAdjustPosLimitHorizontal.bInputCompleted == 1)
					{
						Vis.PageConfiguration.numMotAdjustPosLimitHorizontal.bInputCompleted = 0;
						gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageConfiguration.nSelectedHeadIndex].Alignment[eHORIZONTAL].rPositiveSwLimit = Vis.PageConfiguration.numMotAdjustPosLimitHorizontal.rValue;
						if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
						{
							gAxisControl.CallerBox.bInitAllAxesLimits = 1;
						}	
					}
					else
					{
						Vis.PageConfiguration.numMotAdjustPosLimitHorizontal.rValue = gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageConfiguration.nSelectedHeadIndex].
							Alignment[eHORIZONTAL].rPositiveSwLimit;
					}
			
					// Navigation basierend auf Module Konfiguration	
					//Links
					if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
					{
						Vis.Navigation.btnLeft.bClicked = 0;
						BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
					}
							
					//Rechts		
					if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
					{     
						Vis.Navigation.btnRight.bClicked = 0;
							
			
						if(gPar.MachinePar.MachineModule.bImgProcessingEn== 1) 
						
						{
							BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION12, 0);
						}
						else if(gPar.MachinePar.MachineModule.bTempCtrlEn== 1) 
						
						{
							BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION13, 0);
						}
						else if(gPar.MachinePar.MachineModule.bAnalogFuellStandEn== 1) 
							
						{
							BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION15, 0);
						}
						else
						{
							BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1,0);
						}
					}
					
				}
				
				if(Vis.PageHandling.bPageExit == 1)
				{
					Vis.PageHandling.bPageExit = 0;
				}
				break;

				//	--------------------------------------------------------------------------------------------------------------------------------------------------
				case ePAGE_CONFIGURATION12:
			
					if (gPar.MachinePar.MachineModule.bImgProcessingEn!=1)
					{
						BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
						
					}
			
					else
					{
			
						if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
						{
							Vis.PageHandling.bPageInit = 0;
							ResetAllMenuButtons();
							Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
							Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
							Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
							Vis.Footer.bShowStopStart = 1;
							Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
							SetNavigationButtons(1, 1, 0, 0);
						}
						if(Vis.PageHandling.bPageChangeInProgress == 0)
						{
							// Menu
							HandleMenu();
							// Image Processing
							BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkImageProcessingEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
							if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkImageProcessingEnable) == 1)
							{
								gPar.MachinePar.MachineModule.bImgProcessingEn = Vis.PageModuleConfig.chkImageProcessingEnable.bChecked;
							}
							else
							{
								Vis.PageModuleConfig.chkImageProcessingEnable.bChecked = gPar.MachinePar.MachineModule.bImgProcessingEn;
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.nImageProcessingInputStatus, gPar.MachinePar.MachineModule.bImgProcessingEn == 1);
							// Navigation basierend auf Module Konfiguration	
							//Links
							
							if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
							{
								Vis.Navigation.btnLeft.bClicked = 0;
								if (gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn== 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-2, 0);
								}
								
								else if (gPar.MachinePar.MachineModule.bPressureCtrlEn== 1)
							
								{
									
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-3, 0);
								}
									
								else if (gPar.MachinePar.MachineModule.bCleaningEn== 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-4, 0);
								}
									
				
								else if (gPar.MachinePar.MachineModule.bStitchmodeEn == 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling,Vis.PageHandling.nPageCurrent-5 , 0);
								}
								else
								{
									BrbVc4ChangePage(&Vis.PageHandling,Vis.PageHandling.nPageCurrent-6 , 0);
								}
							}
							
							//Rechts		
							if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
							{     
								Vis.Navigation.btnRight.bClicked = 0;
							
								if(gPar.MachinePar.MachineModule.bTempCtrlEn== 1) 
						
								{
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION13, 0);
								}
							
									
								else if(gPar.MachinePar.MachineModule.bAnalogFuellStandEn== 1) 
							
								{
									BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION15, 0);
								}
								else
						
								{
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1,0);
								}
							}				
						
						}
					}
				if(Vis.PageHandling.bPageExit == 1)
				{
					Vis.PageHandling.bPageExit = 0;
				}
				break;

				//	--------------------------------------------------------------------------------------------------------------------------------------------------
				case ePAGE_CONFIGURATION13:
				
					if (gPar.MachinePar.MachineModule.bTempCtrlEn!=1 )
					{
						BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+2, 0);
						
					}
			
					else
					{
							if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
						{
							Vis.PageHandling.bPageInit = 0;
							ResetAllMenuButtons();
							Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
							Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
							Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
							Vis.Footer.bShowStopStart = 1;
							Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
							SetNavigationButtons(1, 1, 0, 0);
						}
						if(Vis.PageHandling.bPageChangeInProgress == 0)
						{
							// Menu
							HandleMenu();
							// TempCtrl
							BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkTempCtrlEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
							if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkTempCtrlEnable) == 1)
							{
								gPar.MachinePar.MachineModule.bTempCtrlEn = Vis.PageModuleConfig.chkTempCtrlEnable.bChecked;
							}
							else
							{
								Vis.PageModuleConfig.chkTempCtrlEnable.bChecked = gPar.MachinePar.MachineModule.bTempCtrlEn;
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.nTempCtrlInputStatus, gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
							// Tank-Nummer
							BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnTankLeft.nStatus, Vis.PageConfiguration.nSelectedTankIndex > 0);
							if(BrbVc4HandleButton(&Vis.PageConfiguration.btnTankLeft) == 1)
							{
								Vis.PageConfiguration.btnTankLeft.bClicked = 0;
								Vis.PageConfiguration.nSelectedTankIndex -= 1;
							}
							BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnTankRight.nStatus, Vis.PageConfiguration.nSelectedTankIndex < nIDX_TEMP_CTRL_TANK_MAX);
							if(BrbVc4HandleButton(&Vis.PageConfiguration.btnTankRight) == 1)
							{
								Vis.PageConfiguration.btnTankRight.bClicked = 0;
								Vis.PageConfiguration.nSelectedTankIndex += 1;
							}
							if(Vis.PageConfiguration.nSelectedTankIndex < 0)
							{
								Vis.PageConfiguration.nSelectedTankIndex = 0;
							}
							else if(Vis.PageConfiguration.nSelectedTankIndex > nIDX_TEMP_CTRL_TANK_MAX)
							{
								Vis.PageConfiguration.nSelectedTankIndex = nIDX_TEMP_CTRL_TANK_MAX;
							}
							// Parameter
							BrbVc4SetControlEnability(&Vis.PageConfiguration.chkTempCtrlTankExists.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
							if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkTempCtrlTankExists) == 1)
							{
								gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageConfiguration.nSelectedTankIndex].bExist = Vis.PageConfiguration.chkTempCtrlTankExists.bChecked;
							}
							else
							{
								Vis.PageConfiguration.chkTempCtrlTankExists.bChecked = gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageConfiguration.nSelectedTankIndex].bExist;
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.btnParTempCtrlZoneTank.nStatus, gPar.MachinePar.MachineModule.bTempCtrlEn == 1);
							if(BrbVc4HandleButton(&Vis.PageConfiguration.btnParTempCtrlZoneTank) == 1)
							{
								Vis.PageConfiguration.btnParTempCtrlZoneTank.bClicked = 0;
								// Die Seite "ePAGE_CONFIG_PAR_TMPCTRL_ZONE" zeigt die Zonen-Parameter der verschiedenen Heizzonen an. Dazu m�ssen "eParType" (Anzeige des Seitentitels) und
								// "pParSource" (Zeiger auf die Parameter-Struktur) gesetzt sein.
								Vis.PageConfiguration.ParTempCtrlZone.eParType = eVIS_TEMPCTRL_PAR_TANK;
								Vis.PageConfiguration.ParTempCtrlZone.pParSource = &gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageConfiguration.nSelectedTankIndex].TempZonePar;
								BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIG_PAR_TMPCTRL_ZONE, 0);
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.numTempCtrlToleranceMin.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
							if(Vis.PageConfiguration.numTempCtrlToleranceMin.bInputCompleted == 1)
							{
								Vis.PageConfiguration.numTempCtrlToleranceMin.bInputCompleted = 0;
								gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageConfiguration.nSelectedTankIndex].rToleranzMin = Vis.PageConfiguration.numTempCtrlToleranceMin.rValue;
							}
							else
							{
								Vis.PageConfiguration.numTempCtrlToleranceMin.rValue = gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageConfiguration.nSelectedTankIndex].rToleranzMin;
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.numTempCtrlToleranceMax.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
							if(Vis.PageConfiguration.numTempCtrlToleranceMax.bInputCompleted == 1)
							{
								Vis.PageConfiguration.numTempCtrlToleranceMax.bInputCompleted = 0;
								gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageConfiguration.nSelectedTankIndex].rToleranzMax = Vis.PageConfiguration.numTempCtrlToleranceMax.rValue;
							}
							else
							{
								Vis.PageConfiguration.numTempCtrlToleranceMax.rValue = gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageConfiguration.nSelectedTankIndex].rToleranzMax;
							}
							BrbVc4SetControlEnability(&Vis.PageConfiguration.numTempCtrlTempWarning.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
							if(Vis.PageConfiguration.numTempCtrlTempWarning.bInputCompleted == 1)
							{
								Vis.PageConfiguration.numTempCtrlTempWarning.bInputCompleted = 0;
								gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageConfiguration.nSelectedTankIndex].rTempDiffWarning = Vis.PageConfiguration.numTempCtrlTempWarning.rValue;
							}
							else
							{
								Vis.PageConfiguration.numTempCtrlTempWarning.rValue = gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageConfiguration.nSelectedTankIndex].rTempDiffWarning;
							}
							// Navigation basierend auf Module Konfiguration	
							//Links
							if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
							{
								Vis.Navigation.btnLeft.bClicked = 0;
								
								if (gPar.MachinePar.MachineModule.bImgProcessingEn== 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
								}
								
								else if (gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn== 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-3, 0);
								}
								
								else if (gPar.MachinePar.MachineModule.bPressureCtrlEn== 1)
							
								{
									
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-4, 0);
								}
									
								else if (gPar.MachinePar.MachineModule.bCleaningEn== 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-5, 0);
								}
									
				
								else if (gPar.MachinePar.MachineModule.bStitchmodeEn == 1)
								{
									
									BrbVc4ChangePage(&Vis.PageHandling,Vis.PageHandling.nPageCurrent-6 , 0);
								}
								else
								{
									BrbVc4ChangePage(&Vis.PageHandling,Vis.PageHandling.nPageCurrent-7 , 0);
								}
							}
							
							//Rechts		
							if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
							{     
								Vis.Navigation.btnRight.bClicked = 0;
								BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1,0);
								
							}						

					}
				}		
				if(Vis.PageHandling.bPageExit == 1)
				{
					Vis.PageHandling.bPageExit = 0;
				}
				break;

				//	--------------------------------------------------------------------------------------------------------------------------------------------------
				case ePAGE_CONFIGURATION14:
					if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
					{
						Vis.PageHandling.bPageInit = 0;
						ResetAllMenuButtons();
						Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
						Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
						Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
						Vis.Footer.bShowStopStart = 1;
						Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
						SetNavigationButtons(1, 1, 0, 0);
					}
				if(Vis.PageHandling.bPageChangeInProgress == 0)
				{
					// Menu
					HandleMenu();
					// TempCtrl
					BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkTempCtrlEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkTempCtrlEnable) == 1)
					{
						gPar.MachinePar.MachineModule.bTempCtrlEn = Vis.PageModuleConfig.chkTempCtrlEnable.bChecked;
					}
					else
					{
						Vis.PageModuleConfig.chkTempCtrlEnable.bChecked = gPar.MachinePar.MachineModule.bTempCtrlEn;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.nTempCtrlInputStatus, gPar.MachinePar.MachineModule.bTempCtrlEn == 1);
					// Kopf-Nummer
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnHeadLeft.nStatus, Vis.PageConfiguration.nSelectedHeadIndex > 0);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnHeadLeft) == 1)
					{
						Vis.PageConfiguration.btnHeadLeft.bClicked = 0;
						Vis.PageConfiguration.nSelectedHeadIndex -= 1;
					}
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnHeadRight.nStatus, Vis.PageConfiguration.nSelectedHeadIndex < gPar.MachinePar.nBonDevIndex);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnHeadRight) == 1)
					{
						Vis.PageConfiguration.btnHeadRight.bClicked = 0;
						Vis.PageConfiguration.nSelectedHeadIndex += 1;
					}
					if(Vis.PageConfiguration.nSelectedHeadIndex < 0)
					{
						Vis.PageConfiguration.nSelectedHeadIndex = 0;
					}
					else if(Vis.PageConfiguration.nSelectedHeadIndex > gPar.MachinePar.nBonDevIndex)
					{
						Vis.PageConfiguration.nSelectedHeadIndex = gPar.MachinePar.nBonDevIndex;
					}
					// Parameter
					BrbVc4SetControlEnability(&Vis.PageConfiguration.chkTempCtrlHeadZonesExist.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkTempCtrlHeadZonesExist) == 1)
					{
						gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].bExist = Vis.PageConfiguration.chkTempCtrlHeadZonesExist.bChecked;
					}
					else
					{
						Vis.PageConfiguration.chkTempCtrlHeadZonesExist.bChecked = gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].bExist;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.chkTempCtrlHeadZoneTubeExist.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].bExist == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkTempCtrlHeadZoneTubeExist) == 1)
					{
						gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].TempZone[eTEMP_ZONE_TUBE].bZoneExist = Vis.PageConfiguration.chkTempCtrlHeadZoneTubeExist.bChecked;
					}
					else
					{
						Vis.PageConfiguration.chkTempCtrlHeadZoneTubeExist.bChecked = gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].TempZone[eTEMP_ZONE_TUBE].bZoneExist;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.btnParTempCtrlZoneTube.nStatus, gPar.MachinePar.MachineModule.bTempCtrlEn == 1);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnParTempCtrlZoneTube) == 1)
					{
						Vis.PageConfiguration.btnParTempCtrlZoneTube.bClicked = 0;
						// Die Seite "ePAGE_CONFIG_PAR_TMPCTRL_ZONE" zeigt die Zonen-Parameter der verschiedenen Heizzonen an. Dazu m�ssen "eParType" (Anzeige des Seitentitels) und
						// "pParSource" (Zeiger auf die Parameter-Struktur) gesetzt sein.
						Vis.PageConfiguration.ParTempCtrlZone.eParType = eVIS_TEMPCTRL_PAR_TUBE;
						Vis.PageConfiguration.ParTempCtrlZone.pParSource = &gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].TempZone[eTEMP_ZONE_TUBE];
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIG_PAR_TMPCTRL_ZONE, 0);
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.chkTempCtrlHeadZoneHeadExist.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].bExist == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkTempCtrlHeadZoneHeadExist) == 1)
					{
						gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].TempZone[eTEMP_ZONE_BON_DEV].bZoneExist = Vis.PageConfiguration.chkTempCtrlHeadZoneHeadExist.bChecked;
					}
					else
					{
						Vis.PageConfiguration.chkTempCtrlHeadZoneHeadExist.bChecked = gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].TempZone[eTEMP_ZONE_BON_DEV].bZoneExist;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.btnParTempCtrlZoneHead.nStatus, gPar.MachinePar.MachineModule.bTempCtrlEn == 1);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnParTempCtrlZoneHead) == 1)
					{
						Vis.PageConfiguration.btnParTempCtrlZoneHead.bClicked = 0;
						// Die Seite "ePAGE_CONFIG_PAR_TMPCTRL_ZONE" zeigt die Zonen-Parameter der verschiedenen Heizzonen an. Dazu m�ssen "eParType" (Anzeige des Seitentitels) und
						// "pParSource" (Zeiger auf die Parameter-Struktur) gesetzt sein.
						Vis.PageConfiguration.ParTempCtrlZone.eParType = eVIS_TEMPCTRL_PAR_HEAD;
						Vis.PageConfiguration.ParTempCtrlZone.pParSource = &gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].TempZone[eTEMP_ZONE_BON_DEV];
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIG_PAR_TMPCTRL_ZONE, 0);
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.chkTempCtrlHeadZoneReserveExist.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].bExist == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(BrbVc4HandleCheckbox(&Vis.PageConfiguration.chkTempCtrlHeadZoneReserveExist) == 1)
					{
						gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].TempZone[eTEMP_ZONE_RESERVE].bZoneExist = Vis.PageConfiguration.chkTempCtrlHeadZoneReserveExist.bChecked;
					}
					else
					{
						Vis.PageConfiguration.chkTempCtrlHeadZoneReserveExist.bChecked = gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].TempZone[eTEMP_ZONE_RESERVE].bZoneExist;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.btnParTempCtrlZoneReserve.nStatus, gPar.MachinePar.MachineModule.bTempCtrlEn == 1);
					if(BrbVc4HandleButton(&Vis.PageConfiguration.btnParTempCtrlZoneReserve) == 1)
					{
						Vis.PageConfiguration.btnParTempCtrlZoneReserve.bClicked = 0;
						// Die Seite "ePAGE_CONFIG_PAR_TMPCTRL_ZONE" zeigt die Zonen-Parameter der verschiedenen Heizzonen an. Dazu m�ssen "eParType" (Anzeige des Seitentitels) und
						// "pParSource" (Zeiger auf die Parameter-Struktur) gesetzt sein.
						Vis.PageConfiguration.ParTempCtrlZone.eParType = eVIS_TEMPCTRL_PAR_RESERVE;
						Vis.PageConfiguration.ParTempCtrlZone.pParSource = &gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].TempZone[eTEMP_ZONE_RESERVE];
						BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIG_PAR_TMPCTRL_ZONE, 0);
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.ddTempCtrlHeadTankNumber.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].bExist == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					Vis.PageConfiguration.ddTempCtrlHeadTankNumber.nIndexMax = nIDX_TEMP_CTRL_TANK_MAX;
					for(nIndex=0; nIndex <=nIDX_TEMP_CTRL_TANK_MAX; nIndex++)
					{
						if(gPar.MachinePar.TempCtrlConfig.Tank[nIndex].bExist == 1)
						{
							Vis.PageConfiguration.ddTempCtrlHeadTankNumber.nOptions[nIndex] = eBRB_DD_OPTION_NORMAL;
						}
						else
						{
							Vis.PageConfiguration.ddTempCtrlHeadTankNumber.nOptions[nIndex] = eBRB_DD_OPTION_INVISIBLE;
						}
					}
					if(BrbVc4HandleDropdown(&Vis.PageConfiguration.ddTempCtrlHeadTankNumber) == 1)
					{
						gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].nTankIdx = Vis.PageConfiguration.ddTempCtrlHeadTankNumber.nIndex;
					}
					else
					{
						Vis.PageConfiguration.ddTempCtrlHeadTankNumber.nIndex = gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].nTankIdx;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numTempCtrlToleranceMin.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].bExist == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(Vis.PageConfiguration.numTempCtrlToleranceMin.bInputCompleted == 1)
					{
						Vis.PageConfiguration.numTempCtrlToleranceMin.bInputCompleted = 0;
						gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].rToleranzMin = Vis.PageConfiguration.numTempCtrlToleranceMin.rValue;
					}
					else
					{
						Vis.PageConfiguration.numTempCtrlToleranceMin.rValue = gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].rToleranzMin;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numTempCtrlToleranceMax.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].bExist == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(Vis.PageConfiguration.numTempCtrlToleranceMax.bInputCompleted == 1)
					{
						Vis.PageConfiguration.numTempCtrlToleranceMax.bInputCompleted = 0;
						gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].rToleranzMax = Vis.PageConfiguration.numTempCtrlToleranceMax.rValue;
					}
					else
					{
						Vis.PageConfiguration.numTempCtrlToleranceMax.rValue = gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].rToleranzMax;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numTempCtrlStandbyTemp.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].bExist == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(Vis.PageConfiguration.numTempCtrlStandbyTemp.bInputCompleted == 1)
					{
						Vis.PageConfiguration.numTempCtrlStandbyTemp.bInputCompleted = 0;
						gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].rTempStandby = Vis.PageConfiguration.numTempCtrlStandbyTemp.rValue;
					}
					else
					{
						Vis.PageConfiguration.numTempCtrlStandbyTemp.rValue = gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageConfiguration.nSelectedHeadIndex].rTempStandby;
					}
					// Navigation basierend auf Module Konfiguration	
					
					//Links
					if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
					{
						Vis.Navigation.btnLeft.bClicked = 0;
						BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
					}
				   
					//Rechts		
					if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
					{     
						Vis.Navigation.btnRight.bClicked = 0;
						if(gPar.MachinePar.MachineModule.bAnalogFuellStandEn== 1) 
							
						{
							BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION15, 0);
						}
						else
						
						{
							BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1,0);
						}
					
					}
				}
				if(Vis.PageHandling.bPageExit == 1)
				{
					Vis.PageHandling.bPageExit = 0;
				}
				break;

				//	--------------------------------------------------------------------------------------------------------------------------------------------------
				case ePAGE_CONFIGURATION15:
			
					if (gPar.MachinePar.MachineModule.bAnalogFuellStandEn!=1 )
				
					{
			     		BrbVc4ChangePage(&Vis.PageHandling,ePAGE_CONFIGURATION6 , 0);
					}
			
					else
					{
		
						if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
						{
							Vis.PageHandling.bPageInit = 0;
							ResetAllMenuButtons();
							Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
							Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
							Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
							Vis.Footer.bShowStopStart = 1;
							Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
							SetNavigationButtons(1, 1, 0, 0);
						}
						if(Vis.PageHandling.bPageChangeInProgress == 0)
						{
							// Menu
							HandleMenu();
			
							if(BrbVc4HandleButton(&Vis.PageConfiguration.btnTabOfConsum)== 1)
				
							{
								Vis.PageConfiguration.btnTabOfConsum.bClicked=0;
								BrbVc4ChangePage(&Vis.PageHandling, ePAGE_MEDIENVERBRAUCH, 0);
							}
			
							// Navigation
							//Links
			
							if (gPar.MachinePar.MachineModule.bTempCtrlEn==1)
							{
								if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
								{
									Vis.Navigation.btnLeft.bClicked = 0;
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-2, 0);
								}
							}
							else if (gPar.MachinePar.MachineModule.bImgProcessingEn==1)
							{
								if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
								{
									Vis.Navigation.btnLeft.bClicked = 0;
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-3, 0);
								}
				
							}
							else if (gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn ==1 )
							{
								if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
								{
									Vis.Navigation.btnLeft.bClicked = 0;
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-5, 0);
								}
							}
							else if (gPar.MachinePar.MachineModule.bPressureCtrlEn==1)
			
							{ 
								if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
								{
									Vis.Navigation.btnLeft.bClicked = 0;
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-6, 0);
								}
							}
							else if (gPar.MachinePar.MachineModule.bCleaningEn==1)
							{ 
								if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
								{
									Vis.Navigation.btnLeft.bClicked = 0;
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-7, 0);
								}
							}
							else if (gPar.MachinePar.MachineModule.bStitchmodeEn==1)
							{ 
								if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
								{
									Vis.Navigation.btnLeft.bClicked = 0;
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-8, 0);
								}
							}
							else 
							{
								if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
								{
									Vis.Navigation.btnLeft.bClicked = 0;
									BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-9, 0);
								}	
							}
						}
					}
			}
			
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CONFIGURATION16:
		// !!!Trichter Kopf Seite wird nicht mehr angesprugen
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
				Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
				Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
				Vis.Footer.bShowStopStart = 1;
				Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
				SetNavigationButtons(1, 0, 0, 0);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		
		
		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CONFIGURATION17:
	// Diese Seite (Sammelkopf) wird momentan nicht ben�tigt, weil die Parameter dazu auf "ePAGE_HEAD_PAR" untergebracht wurden. Sp�ter kann diese Seite evtl. gel�scht werden
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
				Vis.PageConfiguration.eLastConfigurationPageIndex = Vis.PageHandling.nPageCurrent;
				Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
				Vis.Footer.bShowStopStart = 1;
				Vis.Menu.nPageCount = Vis.PageHandling.nPageCurrent-ePAGE_CONFIGURATION1+1;
				SetNavigationButtons(1, 0, 0, 0);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		
		case ePAGE_CONFIG_PAR_TMPCTRL_ZONE:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnConfiguration.nColor = eCOLOR_MENU_SELECTED;
				Vis.Menu.eLastMenuPage = ePAGE_CONFIGURATION1;
				Vis.Footer.bShowStopStart = 1;
				SetNavigationButtons(1, 0, 0, 0);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Die Seite "ePAGE_CONFIG_PAR_TMPCTRL_ZONE" zeigt die Zonen-Parameter der verschiedenen Heizzonen an. Dazu m�ssen "eParType" (Anzeige des Seitentitels) und
				// "pParSource" (Zeiger auf die Parameter-Struktur) gesetzt sein.
				if(Vis.PageConfiguration.ParTempCtrlZone.eParType > eVIS_TEMPCTRL_PAR_UNKNOWN && Vis.PageConfiguration.ParTempCtrlZone.pParSource != 0)
				{
					BrbVc4SetControlEnability(&Vis.PageConfiguration.ParTempCtrlZone.numSetTempMax.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(Vis.PageConfiguration.ParTempCtrlZone.numSetTempMax.bInputCompleted == 1)
					{
						Vis.PageConfiguration.ParTempCtrlZone.numSetTempMax.bInputCompleted = 0;
						Vis.PageConfiguration.ParTempCtrlZone.pParSource->rSetTempMax = Vis.PageConfiguration.ParTempCtrlZone.numSetTempMax.rValue;
					}
					else
					{
						Vis.PageConfiguration.ParTempCtrlZone.numSetTempMax.rValue = Vis.PageConfiguration.ParTempCtrlZone.pParSource->rSetTempMax;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.ParTempCtrlZone.numGain.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(Vis.PageConfiguration.ParTempCtrlZone.numGain.bInputCompleted == 1)
					{
						Vis.PageConfiguration.ParTempCtrlZone.numGain.bInputCompleted = 0;
						Vis.PageConfiguration.ParTempCtrlZone.pParSource->rGain = Vis.PageConfiguration.ParTempCtrlZone.numGain.rValue;
					}
					else
					{
						Vis.PageConfiguration.ParTempCtrlZone.numGain.rValue = Vis.PageConfiguration.ParTempCtrlZone.pParSource->rGain;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.ParTempCtrlZone.numIntegrationTime.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(Vis.PageConfiguration.ParTempCtrlZone.numIntegrationTime.bInputCompleted == 1)
					{
						Vis.PageConfiguration.ParTempCtrlZone.numIntegrationTime.bInputCompleted = 0;
						Vis.PageConfiguration.ParTempCtrlZone.pParSource->rIntegrationTime = Vis.PageConfiguration.ParTempCtrlZone.numIntegrationTime.rValue;
					}
					else
					{
						Vis.PageConfiguration.ParTempCtrlZone.numIntegrationTime.rValue = Vis.PageConfiguration.ParTempCtrlZone.pParSource->rIntegrationTime;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.ParTempCtrlZone.numDerivativeTime.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(Vis.PageConfiguration.ParTempCtrlZone.numDerivativeTime.bInputCompleted == 1)
					{
						Vis.PageConfiguration.ParTempCtrlZone.numDerivativeTime.bInputCompleted = 0;
						Vis.PageConfiguration.ParTempCtrlZone.pParSource->rDerivativeTime = Vis.PageConfiguration.ParTempCtrlZone.numDerivativeTime.rValue;
					}
					else
					{
						Vis.PageConfiguration.ParTempCtrlZone.numDerivativeTime.rValue = Vis.PageConfiguration.ParTempCtrlZone.pParSource->rDerivativeTime;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.ParTempCtrlZone.numFilterTime.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(Vis.PageConfiguration.ParTempCtrlZone.numFilterTime.bInputCompleted == 1)
					{
						Vis.PageConfiguration.ParTempCtrlZone.numFilterTime.bInputCompleted = 0;
						Vis.PageConfiguration.ParTempCtrlZone.pParSource->rFilterTime = Vis.PageConfiguration.ParTempCtrlZone.numFilterTime.rValue;
					}
					else
					{
						Vis.PageConfiguration.ParTempCtrlZone.numFilterTime.rValue = Vis.PageConfiguration.ParTempCtrlZone.pParSource->rFilterTime;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.ParTempCtrlZone.numMinOut.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(Vis.PageConfiguration.ParTempCtrlZone.numMinOut.bInputCompleted == 1)
					{
						Vis.PageConfiguration.ParTempCtrlZone.numMinOut.bInputCompleted = 0;
						Vis.PageConfiguration.ParTempCtrlZone.pParSource->rMinOut = Vis.PageConfiguration.ParTempCtrlZone.numMinOut.rValue;
					}
					else
					{
						Vis.PageConfiguration.ParTempCtrlZone.numMinOut.rValue = Vis.PageConfiguration.ParTempCtrlZone.pParSource->rMinOut;
					}
					BrbVc4SetControlEnability(&Vis.PageConfiguration.ParTempCtrlZone.numMaxOut.nStatus, Vis.PageConfiguration.bInputAllowed == 1 && gPar.MachinePar.MachineModule.bTempCtrlEn == 1 && Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
					if(Vis.PageConfiguration.ParTempCtrlZone.numMaxOut.bInputCompleted == 1)
					{
						Vis.PageConfiguration.ParTempCtrlZone.numMaxOut.bInputCompleted = 0;
						Vis.PageConfiguration.ParTempCtrlZone.pParSource->rMaxOut = Vis.PageConfiguration.ParTempCtrlZone.numMaxOut.rValue;
					}
					else
					{
						Vis.PageConfiguration.ParTempCtrlZone.numMaxOut.rValue = Vis.PageConfiguration.ParTempCtrlZone.pParSource->rMaxOut;
					}
				}
				else
				{
					memset(&Vis.PageConfiguration.ParTempCtrlZone, 0, sizeof(Vis.PageConfiguration.ParTempCtrlZone));
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageConfiguration.eLastConfigurationPageIndex, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_SERVICE1:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnService.nColor = eCOLOR_MENU_SELECTED;
				Vis.Menu.eLastMenuPage = ePAGE_SERVICE1;
				Vis.Footer.bShowStopStart = 1;
				SetNavigationButtons(0, 1, 0, 0);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Betriebsstunden
				Vis.PageService.numOperatingHoursTotal.nValue = (DINT)(gParPermanent.nOperatingSecondsTotal / 3600);
				Vis.PageService.numOperatingHoursService.nValue = (DINT)(gParPermanent.nOperatingSecondsService / 3600);
				BrbVc4SetControlEnability(&Vis.PageService.btnReset.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_SERVICE);
				if(BrbVc4HandleButton(&Vis.PageService.btnReset) == 1)
				{
					Vis.PageService.btnReset.bClicked = 0;
					gParPermanent.nOperatingSecondsService = 0;
				}
#ifdef HW_ARSIM
				BrbVc4SetControlEnability(&Vis.PageService.btnCalibrateTouch.nStatus, 0);
#else
				BrbVc4SetControlEnability(&Vis.PageService.btnCalibrateTouch.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_SERVICE);
#endif
				// Simulierter Geber
				BrbVc4SetControlEnability(&Vis.PageService.btnSimEncoderVelocityMinus.nStatus, gPar.MachinePar.EncoderPar[0].bSimulationEnable == 1);
				if(BrbVc4HandleButton(&Vis.PageService.btnSimEncoderVelocityMinus) == 1)
				{
					Vis.PageService.btnSimEncoderVelocityMinus.bClicked = 0;
					gEncoder.Par.rSimulatedEncoderSpeed -= 1.0;
					if(gEncoder.Par.rSimulatedEncoderSpeed < 0.0)
					{
						gEncoder.Par.rSimulatedEncoderSpeed = 0.0;
					}
				}
				BrbVc4SetControlEnability(&Vis.PageService.numSimEncoderVelocity.nStatus, gPar.MachinePar.EncoderPar[0].bSimulationEnable == 1);
				if(Vis.PageService.numSimEncoderVelocity.bInputCompleted == 1)
				{
					Vis.PageService.numSimEncoderVelocity.bInputCompleted = 0;
					gEncoder.Par.rSimulatedEncoderSpeed = Vis.PageService.numSimEncoderVelocity.rValue;
				}
				else
				{
					Vis.PageService.numSimEncoderVelocity.rValue = gEncoder.Par.rSimulatedEncoderSpeed;
				}
				BrbVc4SetControlEnability(&Vis.PageService.btnSimEncoderVelocityPlus.nStatus, gPar.MachinePar.EncoderPar[0].bSimulationEnable == 1);
				if(BrbVc4HandleButton(&Vis.PageService.btnSimEncoderVelocityPlus) == 1)
				{
					Vis.PageService.btnSimEncoderVelocityPlus.bClicked = 0;
					gEncoder.Par.rSimulatedEncoderSpeed += 1.0;
					if(gEncoder.Par.rSimulatedEncoderSpeed > 20.0)
					{
						gEncoder.Par.rSimulatedEncoderSpeed = 20.0;
					}
				}
				BrbVc4SetControlVisibility(&Vis.PageService.btnSimEncoderStart.nStatus, gEncoder.State.bSimulatedEncoderStarted == 0);
				BrbVc4SetControlEnability(&Vis.PageService.btnSimEncoderStart.nStatus, gPar.MachinePar.EncoderPar[0].bSimulationEnable == 1);
				if(BrbVc4HandleButton(&Vis.PageService.btnSimEncoderStart) == 1)
				{
					Vis.PageService.btnSimEncoderStart.bClicked = 0;
					gEncoder.DirectBox.bSimulatedEncoderStart = 1;
				}
				BrbVc4SetControlVisibility(&Vis.PageService.btnSimEncoderStop.nStatus, gEncoder.State.bSimulatedEncoderStarted == 1);
				BrbVc4SetControlEnability(&Vis.PageService.btnSimEncoderStop.nStatus, gPar.MachinePar.EncoderPar[0].bSimulationEnable == 1);
				if(BrbVc4HandleButton(&Vis.PageService.btnSimEncoderStop) == 1)
				{
					Vis.PageService.btnSimEncoderStop.bClicked = 0;
					gEncoder.DirectBox.bSimulatedEncoderStop = 1;
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_SERVICE2:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnService.nColor = eCOLOR_MENU_SELECTED;
				Vis.Menu.eLastMenuPage = ePAGE_SERVICE2;
				Vis.Footer.bShowStopStart = 1;
				SetNavigationButtons(1, 1, 0, 0);
				if(gPar.MachinePar.nPrintMarkSensorIndex > -1 && Vis.PageService.numSelectedPrintmarkSensorIndex.nValue == -1)
				{
					Vis.PageService.numSelectedPrintmarkSensorIndex.nValue = 0;
				}
				else if(Vis.PageService.numSelectedPrintmarkSensorIndex.nValue > gPar.MachinePar.nPrintMarkSensorIndex)
				{
					Vis.PageService.numSelectedPrintmarkSensorIndex.nValue = gPar.MachinePar.nPrintMarkSensorIndex;
				}
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Geschwindigkeiten
				for(nIndex=0; nIndex<=nIDX_ENCODER_MAX; nIndex++)
				{
					Vis.PageService.numVelocityEncoder[nIndex].rValue = gEncoder.State.rVelocity[nIndex] / 100000.0;
				}
				// Printmark-Sensors
				if(gPar.MachinePar.nPrintMarkSensorIndex > -1)
				{
					BrbVc4SetControlEnability(&Vis.PageService.numSelectedPrintmarkSensorIndex.nStatus, 1);
					BrbVc4SetControlVisibility(&Vis.PageService.btnPrintmarkSensorLeft.nStatus, Vis.PageService.numSelectedPrintmarkSensorIndex.nValue > 0);
					if(BrbVc4HandleButton(&Vis.PageService.btnPrintmarkSensorLeft) == 1)
					{
						Vis.PageService.btnPrintmarkSensorLeft.bClicked = 0;
						Vis.PageService.numSelectedPrintmarkSensorIndex.nValue -= 1;
					}
					BrbVc4SetControlVisibility(&Vis.PageService.btnPrintmarkSensorRight.nStatus, Vis.PageService.numSelectedPrintmarkSensorIndex.nValue < gPar.MachinePar.nPrintMarkSensorIndex);
					if(BrbVc4HandleButton(&Vis.PageService.btnPrintmarkSensorRight) == 1)
					{
						Vis.PageService.btnPrintmarkSensorRight.bClicked = 0;
						Vis.PageService.numSelectedPrintmarkSensorIndex.nValue += 1;
					}
					if(Vis.PageService.numSelectedPrintmarkSensorIndex.nValue < 0)
					{
						Vis.PageService.numSelectedPrintmarkSensorIndex.nValue = 0;
					}
					else if(Vis.PageService.numSelectedPrintmarkSensorIndex.nValue > gPar.MachinePar.nPrintMarkSensorIndex)
					{
						Vis.PageService.numSelectedPrintmarkSensorIndex.nValue = gPar.MachinePar.nPrintMarkSensorIndex;
					}
					Vis.PageService.numPrintmarkLen.nValue = gPrintMarkSensor.State.nPmLen[Vis.PageService.numSelectedPrintmarkSensorIndex.nValue];
				}
				else
				{
					BrbVc4SetControlEnability(&Vis.PageConfiguration.numSelectedPrintmarkSensorIndex.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnPrintmarkSensorLeft.nStatus, 0);
					BrbVc4SetControlVisibility(&Vis.PageConfiguration.btnPrintmarkSensorRight.nStatus, 0);
					BrbVc4SetControlEnability(&Vis.PageConfiguration.optPrintmarkSensorEdgeDetectPos.nStatus, 0);
					Vis.PageConfiguration.optPrintmarkSensorEdgeDetectPos.bChecked = 0;
					BrbVc4SetControlEnability(&Vis.PageConfiguration.optPrintmarkSensorEdgeDetectNeg.nStatus, 0);
					Vis.PageConfiguration.optPrintmarkSensorEdgeDetectNeg.bChecked = 0;
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
				Vis.Navigation.btnLeft.bClicked = 0;
				BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_SERVICE3:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnService.nColor = eCOLOR_MENU_SELECTED;
				Vis.Menu.eLastMenuPage = ePAGE_SERVICE3;
				Vis.Footer.bShowStopStart = 1;
				SetNavigationButtons(1, 0, 0, 0);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Datum und Uhrzeit
				BrbVc4SetControlEnability(&Vis.PageService.nSystemTimeStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_SERVICE);
				if(Vis.PageService.bSystemTimeInputCompleted == 1)
				{
					Vis.PageService.bSystemTimeInputCompleted = 0;
					gSystem.DirectBox.bSetSystemTime = 1;
				}
				else if(gSystem.DirectBox.bSetSystemTime == 0)
				{
					memcpy(&gSystem.Par.dtSystemTime, &gSystem.State.dtSystemTime, sizeof(gSystem.State.dtSystemTime));
				}
				// Ethernet-Einstellungen
				for(nIndex=0; nIndex<=nSYSTEM_ETH_INTERFACE_INDEX_MAX; nIndex++)
				{
					if(gSystem.State.EthInterface[nIndex].bExists == 1)
					{
						BrbVc4SetControlVisibility(&Vis.PageService.EthInterface[nIndex].nStatus, 1);
						BrbVc4SetControlEnability(&Vis.PageService.EthInterface[nIndex].nStatus, gSystem.State.EthInterface[nIndex].bChangeable == 1 && gSystem.State.bGettingOrSettingEth == 0 
							&& Vis.GeneralOwn.nUserLevel == eUSERLEVEL_SERVICE);
						if(Vis.PageService.EthInterface[nIndex].bInputCompleted == 1)
						{
							Vis.PageService.EthInterface[nIndex].bInputCompleted = 0;
							if(BrbSetCaller(&gSystem.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
							{
								gSystem.CallerBox.bSetEthSettings = 1;
							}
						}
					}
					else
					{
						BrbVc4SetControlVisibility(&Vis.PageService.EthInterface[nIndex].nStatus, 0);
					}
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_USER:
		   if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnUser.nColor = eCOLOR_MENU_SELECTED;
				Vis.Menu.eLastMenuPage = ePAGE_USER;
				Vis.Footer.bShowStopStart = 0;
				BrbVc4SetControlVisibility(&Vis.PageUser.btnModConfig.nStatus,0);
				BrbVc4SetControlVisibility(&Vis.PageUser.txtMsgPopup.nStatus,0);
				BrbVc4SetControlVisibility(&Vis.PageUser.bmpMsgPopupBell.nStatus,0);
				BrbVc4SetControlVisibility(&Vis.PageUser.btnMsgPopupOk.nStatus,0);
				
			}
		
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Password
				BrbVc4SetControlEnability(&Vis.PageUser.btnPassword.nStatus, 1);
				if(BrbVc4HandleButton(&Vis.PageUser.btnPassword) == 1)
				{
					Vis.PageUser.btnPassword.bClicked = 0;
					BrbVc4OpenTouchpad(&Vis.PageUser.pwPassword.nStatus);
				}
			
				if(Vis.PageUser.pwPassword.bInputCompleted == 1)
				{
					Vis.PageUser.pwPassword.bInputCompleted = 0;
					
				}

			if(Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR )
				{
					BrbVc4SetControlVisibility(&Vis.PageUser.txtMsgPopup.nStatus,1);
					BrbVc4SetControlVisibility(&Vis.PageUser.bmpMsgPopupBell.nStatus,1);
					BrbVc4SetControlVisibility(&Vis.PageUser.btnMsgPopupOk.nStatus,1);
					BrbVc4SetControlEnability(&Vis.PageUser.btnMsgPopupOk.nStatus,1);
				
				}
				if(BrbVc4HandleButton(&Vis.PageUser.btnMsgPopupOk)==1)
				{
					Vis.PageUser.btnMsgPopupOk.bClicked=0;
					BrbVc4SetControlVisibility(&Vis.PageUser.txtMsgPopup.nStatus,0);
					BrbVc4SetControlVisibility(&Vis.PageUser.bmpMsgPopupBell.nStatus,0);
					BrbVc4SetControlVisibility(&Vis.PageUser.btnMsgPopupOk.nStatus,0);
				}
				
			
				if(Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR)
					{	
						BrbVc4SetControlVisibility(&Vis.PageUser.btnModConfig.nStatus,1);
						BrbVc4SetControlEnability(&Vis.PageUser.btnModConfig.nStatus,1);
					}
				if(BrbVc4HandleButton(&Vis.PageUser.btnModConfig) == 1)	
				{
					Vis.PageUser.btnModConfig.bClicked=0;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_MODULE_CONFIG, 1);	
				}
				BrbVc4SetControlEnability(&Vis.PageUser.btnLogout.nStatus, Vis.GeneralOwn.nUserLevel != 0);
				if(BrbVc4HandleButton(&Vis.PageUser.btnLogout) == 1 )
				{
					Vis.PageUser.btnLogout.bClicked = 0;
					Vis.GeneralOwn.nUserLevel = eUSERLEVEL_OPERATOR;
				}
			if(Vis.GeneralOwn.nUserLevel != eUSERLEVEL_CONFIGURATOR)
			{
			     BrbVc4SetControlVisibility(&Vis.PageUser.btnModConfig.nStatus,0);
		    }
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_MODULE_CONFIG:
			//allgemein
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Menu.eLastMenuPage = ePAGE_MODULE_CONFIG;
				Vis.Footer.bShowStopStart = 0;
			}
			

			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkOverviewEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
			//Overview
				if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkOverviewEnable) == 1)
				{
					gPar.MachinePar.MachineModule.bOverviewEn = Vis.PageModuleConfig.chkOverviewEnable.bChecked;
				}
				else
				{
					Vis.PageModuleConfig.chkOverviewEnable.bChecked = gPar.MachinePar.MachineModule.bOverviewEn;
				}
			
			//Cleaning
				BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkCleaningEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
				if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkCleaningEnable) == 1)
				{
					gPar.MachinePar.MachineModule.bCleaningEn = Vis.PageModuleConfig.chkCleaningEnable.bChecked;
				}
				else
				{
					Vis.PageModuleConfig.chkCleaningEnable.bChecked = gPar.MachinePar.MachineModule.bCleaningEn;
				}
			 //Stitching
				BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkStitchingEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
				if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkStitchingEnable) == 1)
				{
					gPar.MachinePar.MachineModule.bStitchmodeEn = Vis.PageModuleConfig.chkStitchingEnable.bChecked;
				}
				else
				{
					Vis.PageModuleConfig.chkStitchingEnable.bChecked = gPar.MachinePar.MachineModule.bStitchmodeEn;
				}
			 //Motorized Adjustment
			
				BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkMotorizedAdjustmentEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
				if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkMotorizedAdjustmentEnable) == 1)
				{
					gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn = Vis.PageModuleConfig.chkMotorizedAdjustmentEnable.bChecked;
				}
				else
				{
					Vis.PageModuleConfig.chkMotorizedAdjustmentEnable.bChecked = gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn;
				}
			
			//Pressure Control
				BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkPressureControlEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
				if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkPressureControlEnable) == 1)
				{
					gPar.MachinePar.MachineModule.bPressureCtrlEn = Vis.PageModuleConfig.chkPressureControlEnable.bChecked;
				}
				else
				{
					Vis.PageModuleConfig.chkPressureControlEnable.bChecked = gPar.MachinePar.MachineModule.bPressureCtrlEn;
				}
			 //ImageProcessing
				BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkImageProcessingEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
				if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkImageProcessingEnable) == 1)
				{
					gPar.MachinePar.MachineModule.bImgProcessingEn = Vis.PageModuleConfig.chkImageProcessingEnable.bChecked;
				}
				else
				{
					Vis.PageModuleConfig.chkImageProcessingEnable.bChecked = gPar.MachinePar.MachineModule.bImgProcessingEn;
				}
			
			//Temperature Control
				BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkTempCtrlEnable.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
				if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkTempCtrlEnable) == 1)
				{
					gPar.MachinePar.MachineModule.bTempCtrlEn = Vis.PageModuleConfig.chkTempCtrlEnable.bChecked;
				}
				else
				{
					Vis.PageModuleConfig.chkTempCtrlEnable.bChecked = gPar.MachinePar.MachineModule.bTempCtrlEn;
				}
			
			//Analog F�llStand
				BrbVc4SetControlEnability(&Vis.PageModuleConfig.chkAnalogFuellstand.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_CONFIGURATOR);
				if(BrbVc4HandleCheckbox(&Vis.PageModuleConfig.chkAnalogFuellstand) == 1)
				{
					gPar.MachinePar.MachineModule.bAnalogFuellStandEn = Vis.PageModuleConfig.chkAnalogFuellstand.bChecked;
				}
				else
				{
					Vis.PageModuleConfig.chkAnalogFuellstand.bChecked = gPar.MachinePar.MachineModule.bAnalogFuellStandEn;
				}
			
			//Navigation
				if(BrbVc4HandleButton(&Vis.PageModuleConfig.btnOk) == 1)
				{
					Vis.PageModuleConfig.btnOk.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_MAIN, 0);
				
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_EVENTS:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnEvents.nColor = eCOLOR_MENU_SELECTED;
				Vis.Menu.eLastMenuPage = ePAGE_EVENTS;
				Vis.Footer.bShowStopStart = 1;
				Vis.tcTabCtrl.nSelectedTabPageIndex = 0;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
				// Listen
				BrbVc4HandleTabCtrl(&Vis.tcTabCtrl);
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CONTACT:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				ResetAllMenuButtons();
				Vis.Menu.btnContact.nColor = eCOLOR_MENU_SELECTED;
				Vis.Menu.eLastMenuPage = ePAGE_CONTACT;
				Vis.Footer.bShowStopStart = 1;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Menu
				HandleMenu();
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_OVERVIEW:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
				if(Vis.Heads.nHeadOffset > gPar.MachinePar.nBonDevIndex)
				{
					Vis.Heads.nHeadOffset = 0;
				}
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				//K�pfe
				{
					// Zeichen-Zugriff erwerben
					BOOL bDraw = 0;
					if(Vis.General.nRedrawCounter == 0)
					{
						UINT nFuncAccess = VA_Saccess(1, Vis.General.nVcHandle);
						if(nFuncAccess == 0)
						{
							bDraw = 1;
						}
					}
					for(nHeadIndex=0; nHeadIndex<=nVIS_HEAD_OVERV_SHOW_INDEX_MAX; nHeadIndex++)
					{
						if(nHeadIndex+Vis.Heads.nHeadOffset <= gPar.MachinePar.nBonDevIndex)
						{
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 1);
							Vis.Heads.Head[nHeadIndex].numHead.nValue = nHeadIndex+Vis.Heads.nHeadOffset + 1;
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].chkHead.nStatus, 1);
							BrbVc4SetControlEnability(&Vis.Heads.Head[nHeadIndex].chkHead.nStatus, 1);
							if(BrbVc4HandleCheckbox(&Vis.Heads.Head[nHeadIndex].chkHead) == 1)
							{
								gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].bBonDevEn = Vis.Heads.Head[nHeadIndex].chkHead.bChecked;
							}
							else
							{
								Vis.Heads.Head[nHeadIndex].chkHead.bChecked = gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].bBonDevEn;
							}
							Vis.Heads.Head[nHeadIndex].btnHeadJog.bSuppressTimeout = 1;
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHeadJog.nStatus, 1);
							BrbVc4HandleJogButton(&Vis.Heads.Head[nHeadIndex].btnHeadJog);
							if(Vis.Heads.Head[nHeadIndex].btnHeadJog.eJogState == eBRB_JOG_BTN_STATE_PUSHED || Vis.Heads.Head[nHeadIndex].btnHeadJog.eJogState == eBRB_JOG_BTN_STATE_PUSHED_EDGE)
							{
								gMainLogic.DirectBox.bCmdOpenBonDev[nHeadIndex+Vis.Heads.nHeadOffset] = 1;
							}
							else if(Vis.Heads.Head[nHeadIndex].btnHeadJog.eJogState == eBRB_JOG_BTN_STATE_UNPUSHED || Vis.Heads.Head[nHeadIndex].btnHeadJog.eJogState == eBRB_JOG_BTN_STATE_UNPUSHED_EDGE)
							{
								gMainLogic.DirectBox.bCmdOpenBonDev[nHeadIndex+Vis.Heads.nHeadOffset] = 0;
							}
							if(gCalcPos.State.HeadState[nHeadIndex+Vis.Heads.nHeadOffset].bHeadError == 1)
							{
								Vis.Heads.Head[nHeadIndex].btnHeadJog.nBmpIndex = eHEAD_BMP_INDEX_SMALL_RED;
							}	
							else if(gBondingDev.State.bCleaningActive[nHeadIndex+Vis.Heads.nHeadOffset] == 1)
							{
								Vis.Heads.Head[nHeadIndex].btnHeadJog.nBmpIndex = eHEAD_BMP_INDEX_SMALL_GREEN;
							}
							else if(gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].bBonDevEn == 1)
							{
								Vis.Heads.Head[nHeadIndex].btnHeadJog.nBmpIndex = eHEAD_BMP_INDEX_SMALL_BLUE;
							}
							else
							{
								Vis.Heads.Head[nHeadIndex].btnHeadJog.nBmpIndex = eHEAD_BMP_INDEX_SMALL_GREY;
							}
							// Zeichnen
							if(bDraw == 1)
							{
								// Zugriff auf die entsprechende Drawbox erwerben
								STRING sHeadIndex[10];
								brsitoa((DINT)nHeadIndex, (UDINT)&sHeadIndex);
								STRING sDrawbox[100];
								strcpy(sDrawbox, "P03000_Overview/Default/dbLines");
								strcat(sDrawbox, sHeadIndex);
								UINT nFuncAttach = VA_Attach(1, Vis.General.nVcHandle, 0, (UDINT)&sDrawbox);
								if(nFuncAttach == 0)
								{
									// Zeichnen
									DINT nDrawboxWidth = 250;
									DINT nDrawboxHeight = 40;
									// L�schen
									BrbVc4Rectangle_TYP Background;
									Background.nLeft = Background.nTop = 0;
									Background.nWidth = nDrawboxWidth;
									Background.nHeight = nDrawboxHeight-3;
									Background.nBorderColor = 59;
									Background.nFillColor = 251;
									Background.nDashWidth = 0;
									BrbVc4DrawRectangle(&Background, &Vis.General);
									REAL rScaleFactor = 1.0;
									if(gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].eIdxSenType == eSEN_TYPE_ENCODER)
									{
										rScaleFactor = (REAL)(nDrawboxWidth - 4) / gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].rCylinderPerimeter;
									}
									else
									{
										rScaleFactor = (REAL)(nDrawboxWidth - 4) / gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].rProductLen;
									}
									BrbVc4Rectangle_TYP Rect;
									Rect.nTop = (DINT)((REAL)nDrawboxHeight / 2.0) - 2;
									Rect.nHeight = 4;
									Rect.nDashWidth = 0;
									DINT rLastPos = 0.0;
									UINT nSelectedPrintmarkIndex = 0;
									for(nSwitchIndex=0; nSwitchIndex<=gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].nSwitchIndexMax[nSelectedPrintmarkIndex]; nSwitchIndex++)
									{
										Rect.nFillColor = Rect.nBorderColor = eBACKCOLOR_BLACK;
										Rect.nLeft = (DINT)((gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].Switches.BondingParIdx[nSelectedPrintmarkIndex].rPause[nSwitchIndex] + rLastPos) * rScaleFactor);
										rLastPos += gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].Switches.BondingParIdx[nSelectedPrintmarkIndex].rPause[nSwitchIndex];
										Rect.nWidth = (DINT)((gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].Switches.BondingParIdx[nSelectedPrintmarkIndex].rStitchLen[nSwitchIndex] + rLastPos) * rScaleFactor) - Rect.nLeft;
										rLastPos += gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].Switches.BondingParIdx[nSelectedPrintmarkIndex].rStitchLen[nSwitchIndex];
										BrbVc4DrawRectangle(&Rect, &Vis.General);
									}
									// Zugriff auf die entsprechende Drawbox freigeben
									VA_Detach(1, Vis.General.nVcHandle);
								}
							}
							// Klick auf Drawbox
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].hsHead.nStatus, 1);
							if(Vis.Heads.Head[nHeadIndex].hsHead.bClicked == 1)
							{
								Vis.Heads.Head[nHeadIndex].hsHead.bClicked = 0;
								Vis.PageHead.nSelectedHeadIndex = nHeadIndex+Vis.Heads.nHeadOffset;
								BrbVc4ChangePage(&Vis.PageHandling, ePAGE_HEAD, 0);
							}
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numEncoderVelocity.nStatus, 1);
							Vis.Heads.Head[nHeadIndex].numEncoderVelocity.rValue = gEncoder.State.rVelocity[gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].nIdxEncoder] / 100000.0;
						}
						else
						{
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 0);
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].chkHead.nStatus, 0);
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHeadJog.nStatus, 0);
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].hsHead.nStatus, 0);
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numEncoderVelocity.nStatus, 0);
						}
					}
					// Zeichen-Zugriff freigeben
					if(bDraw == 1)
					{
						VA_Srelease(1, Vis.General.nVcHandle);
					}
				}
				// Navigation
				if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex <= nVIS_HEAD_OVERV_SHOW_INDEX_MAX)
				{
					SetNavigationButtons(0, 0, 0, 0);
				}
				else if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex > nVIS_HEAD_OVERV_SHOW_INDEX_MAX)
				{
					SetNavigationButtons(0, 0, 0, 1);
				}
				else if(Vis.Heads.nHeadOffset >= gPar.MachinePar.nBonDevIndex - nVIS_HEAD_OVERV_SHOW_INDEX_MAX )
				{
					SetNavigationButtons(0, 0, 1, 0);
				}
				else
				{
					SetNavigationButtons(0, 0, 1, 1);
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnUp) == 1)
				{
					Vis.Navigation.btnUp.bClicked = 0;
					Vis.Heads.nHeadOffset -= 1;
					if(Vis.Heads.nHeadOffset < 0)
					{
						Vis.Heads.nHeadOffset = 0;
					}
					Vis.PageHead.nSwitchOffset = 0;
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnDown) == 1)
				{
					Vis.Navigation.btnDown.bClicked = 0;
					Vis.Heads.nHeadOffset += 1;
					if(Vis.Heads.nHeadOffset > nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
					{
						Vis.Heads.nHeadOffset = nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX;
					}
					Vis.PageHead.nSwitchOffset = 0;
				}
				// Druck-Korrektur
				BrbVc4SetControlVisibility(&Vis.PageOverview.btnCorrectionMinus.nStatus, gPar.MachinePar.MachineModule.bPressureCtrlEn == 1);
				if(BrbVc4HandleButton(&Vis.PageOverview.btnCorrectionMinus) == 1)
				{
					Vis.PageOverview.btnCorrectionMinus.bClicked = 0;
					gPressureCtrl.Par.rPressureCorr -= 0.1;
				}
				BrbVc4SetControlVisibility(&Vis.PageOverview.numCorrection.nStatus, gPar.MachinePar.MachineModule.bPressureCtrlEn == 1);
				if(Vis.PageOverview.numCorrection.bInputCompleted == 1)
				{
					Vis.PageOverview.numCorrection.bInputCompleted = 0;
					gPressureCtrl.Par.rPressureCorr = Vis.PageOverview.numCorrection.rValue;
				}
				else
				{
					Vis.PageOverview.numCorrection.rValue = gPressureCtrl.Par.rPressureCorr;
				}
				BrbVc4SetControlVisibility(&Vis.PageOverview.btnCorrectionPlus.nStatus, gPar.MachinePar.MachineModule.bPressureCtrlEn == 1);
				if(BrbVc4HandleButton(&Vis.PageOverview.btnCorrectionPlus) == 1)
				{
					Vis.PageOverview.btnCorrectionPlus.bClicked = 0;
					gPressureCtrl.Par.rPressureCorr += 0.1;
				}
				// Toolbar
				HandleToolbar();
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CLEANING:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
				SetNavigationButtons(0, 1, 0, 0);
				if(gPar.MachinePar.TankType.bTankCleanEn == 1 && gPar.MachinePar.TankType.bAutoValveCtrl == 1)
				{
					if(gMainLogic.Par.bDetergentActive == 1)
					{
						if(gMainLogic.Par.eCleaningType == eCLEANING_TYPE_STANDARD)
						{
							Vis.PageCleaning.optCleanStandard.bClicked = 1;
						}
						else
						{
							Vis.PageCleaning.optCleanInterval.bClicked = 1;
						}
					}
					else
					{
						if(gMainLogic.Par.eCleaningType == eCLEANING_TYPE_STANDARD)
						{
							Vis.PageCleaning.optBondStandard.bClicked = 1;
						}
						else
						{
							Vis.PageCleaning.optBondInterval.bClicked = 1;
						}
					}
				}
				else
				{
					if(gMainLogic.Par.eCleaningType == eCLEANING_TYPE_STANDARD)
					{
						Vis.PageCleaning.optBondStandard.bClicked = 1;
					}
					else
					{
						Vis.PageCleaning.optBondInterval.bClicked = 1;
					}
				}
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				//K�pfe
				Vis.PageCleaning.nHeadsEnabledCount = 0;
				for(nHeadIndex=0; nHeadIndex<=nDEV_INDEX_MAX; nHeadIndex++)
				{
					if(nHeadIndex <= gPar.MachinePar.nBonDevIndex)
					{
						Vis.Heads.Head[nHeadIndex].btnHeadJog.bSuppressTimeout = 1;
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHeadJog.nStatus, 1);
						BrbVc4HandleJogButton(&Vis.Heads.Head[nHeadIndex].btnHeadJog);
						if((Vis.Heads.Head[nHeadIndex].btnHeadJog.eJogState == eBRB_JOG_BTN_STATE_PUSHED || Vis.Heads.Head[nHeadIndex].btnHeadJog.eJogState == eBRB_JOG_BTN_STATE_PUSHED_EDGE) && gMainLogic.State.bSystemCleaningActive == 0)
						{
							gMainLogic.DirectBox.bCmdOpenBonDev[nHeadIndex] = 1;
						}
						else if(Vis.Heads.Head[nHeadIndex].btnHeadJog.eJogState == eBRB_JOG_BTN_STATE_UNPUSHED || Vis.Heads.Head[nHeadIndex].btnHeadJog.eJogState == eBRB_JOG_BTN_STATE_UNPUSHED_EDGE)
						{
							gMainLogic.DirectBox.bCmdOpenBonDev[nHeadIndex] = 0;
						}
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].chkHead.nStatus, 1);
						BrbVc4SetControlEnability(&Vis.Heads.Head[nHeadIndex].chkHead.nStatus, 1);
						if(BrbVc4HandleCheckbox(&Vis.Heads.Head[nHeadIndex].chkHead) == 1)
						{
							gMainLogic.Par.bBonDevCleaningEn[nHeadIndex] = Vis.Heads.Head[nHeadIndex].chkHead.bChecked;
						}
						else
						{
							Vis.Heads.Head[nHeadIndex].chkHead.bChecked = gMainLogic.Par.bBonDevCleaningEn[nHeadIndex];
						}
						if(gMainLogic.Par.bBonDevCleaningEn[nHeadIndex] == 1)
						{
							Vis.PageCleaning.nHeadsEnabledCount += 1;
						}
						if(gBondingDev.State.bCleaningActive[nHeadIndex] == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHeadJog.nBmpIndex = eHEAD_BMP_INDEX_SMALL_GREEN;
						}
						else if(gPar.ProductPar.BonDevPar[nHeadIndex].bBonDevEn == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHeadJog.nBmpIndex = eHEAD_BMP_INDEX_SMALL_BLUE;
						}
						else
						{
							Vis.Heads.Head[nHeadIndex].btnHeadJog.nBmpIndex = eHEAD_BMP_INDEX_SMALL_GREY;
						}
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 1);
						Vis.Heads.Head[nHeadIndex].numHead.nValue = nHeadIndex + 1;
					}
					else
					{
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHeadJog.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].chkHead.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 0);
					}
				}
				// Tank
				BrbVc4SetControlVisibility(&Vis.PageCleaning.bgClean.nStatus, gPar.MachinePar.TankType.bTankCleanEn == 1);
				Vis.PageCleaning.bgClean.nValue = gMachine.State.TankLvl.nFillLvlClean;
				// Modus
				BrbVc4SetControlVisibility(&Vis.PageCleaning.optBondInterval.nStatus, 1);
				BrbVc4SetControlEnability(&Vis.PageCleaning.optBondInterval.nStatus, gMainLogic.State.bCleaningActive == 0);
				if(BrbVc4HandleOptionbox(&Vis.PageCleaning.optBondInterval) == 1)
				{
					Vis.PageCleaning.optBondInterval.bChecked = 1;
					Vis.PageCleaning.optBondStandard.bChecked = 0;
					Vis.PageCleaning.optCleanInterval.bChecked = 0;
					Vis.PageCleaning.optCleanStandard.bChecked = 0;
					gMainLogic.Par.bDetergentActive = 0;
					gMainLogic.Par.eCleaningType = eCLEANING_TYPE_INTERVAL;
				}
				BrbVc4SetControlVisibility(&Vis.PageCleaning.optBondStandard.nStatus, 1);
				BrbVc4SetControlEnability(&Vis.PageCleaning.optBondStandard.nStatus, gMainLogic.State.bCleaningActive == 0);
				if(BrbVc4HandleOptionbox(&Vis.PageCleaning.optBondStandard) == 1)
				{
					Vis.PageCleaning.optBondInterval.bChecked = 0;
					Vis.PageCleaning.optBondStandard.bChecked = 1;
					Vis.PageCleaning.optCleanInterval.bChecked = 0;
					Vis.PageCleaning.optCleanStandard.bChecked = 0;
					gMainLogic.Par.bDetergentActive = 0;
					gMainLogic.Par.eCleaningType = eCLEANING_TYPE_STANDARD;
				}
				BrbVc4SetControlVisibility(&Vis.PageCleaning.optCleanInterval.nStatus, gPar.MachinePar.TankType.bTankCleanEn == 1 && gPar.MachinePar.TankType.bAutoValveCtrl == 1);
				BrbVc4SetControlEnability(&Vis.PageCleaning.optCleanInterval.nStatus, gMainLogic.State.bCleaningActive == 0);
				if(BrbVc4HandleOptionbox(&Vis.PageCleaning.optCleanInterval) == 1)
				{
					Vis.PageCleaning.optBondInterval.bChecked = 0;
					Vis.PageCleaning.optBondStandard.bChecked = 0;
					Vis.PageCleaning.optCleanInterval.bChecked = 1;
					Vis.PageCleaning.optCleanStandard.bChecked = 0;
					gMainLogic.Par.bDetergentActive = 1;
					gMainLogic.Par.eCleaningType = eCLEANING_TYPE_INTERVAL;
				}
				BrbVc4SetControlVisibility(&Vis.PageCleaning.optCleanStandard.nStatus, gPar.MachinePar.TankType.bTankCleanEn == 1 && gPar.MachinePar.TankType.bAutoValveCtrl == 1);
				BrbVc4SetControlEnability(&Vis.PageCleaning.optCleanStandard.nStatus, gMainLogic.State.bCleaningActive == 0);
				if(BrbVc4HandleOptionbox(&Vis.PageCleaning.optCleanStandard) == 1)
				{
					Vis.PageCleaning.optBondInterval.bChecked = 0;
					Vis.PageCleaning.optBondStandard.bChecked = 0;
					Vis.PageCleaning.optCleanInterval.bChecked = 0;
					Vis.PageCleaning.optCleanStandard.bChecked = 1;
					gMainLogic.Par.bDetergentActive = 1;
					gMainLogic.Par.eCleaningType = eCLEANING_TYPE_STANDARD;
				}
				// Start/Stop
				BrbVc4SetControlVisibility(&Vis.PageCleaning.btnStart.nStatus, gMainLogic.State.bCleaningActive == 0);
				BrbVc4SetControlEnability(&Vis.PageCleaning.btnStart.nStatus, gMainLogic.State.bSystemCleaningActive == 0 && Vis.PageCleaning.nHeadsEnabledCount > 0 && gMainLogic.State.bSystemCleaningActive == 0);
				if(BrbVc4HandleButton(&Vis.PageCleaning.btnStart) == 1)
				{
					Vis.PageCleaning.btnStart.bClicked = 0;
						
					// Das Kommando zur automatischen Reinigung immer schicken, um eine Warnung ausgeben zu k�nnen wenn die Reinigung nicht m�glich ist
					gMainLogic.DirectBox.bAutoCleaningOn = 1;
				}
				BrbVc4SetControlVisibility(&Vis.PageCleaning.btnStop.nStatus, gMainLogic.State.bCleaningActive == 1);				
				if(BrbVc4HandleButton(&Vis.PageCleaning.btnStop) == 1)
				{
					Vis.PageCleaning.btnStop.bClicked = 0;
					gMainLogic.DirectBox.bCleaningStop = 1;
				}
				
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent+1, 0);
				}
				// Toolbar
				HandleToolbar();
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_SYSTEM_CLEANING:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
				SetNavigationButtons(1, 0, 0, 0);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Progress
				BrbVc4SetControlVisibility(&Vis.PageSystemCleaning.strProgress.nStatus, gMainLogic.State.bSystemCleaningActive == 1);
				DATE_AND_TIME dtRestPeriod = gMainLogic.State.nSystemCleaningRestPeriod;
				BrbGetTimeTextDt(dtRestPeriod, Vis.PageSystemCleaning.strProgress.sValue, sizeof(Vis.PageSystemCleaning.strProgress.sValue), "MM:ss");
				// Parameter
				BrbVc4SetControlEnability(&Vis.PageSystemCleaning.numGlueTankCleaningPeriod.nStatus, gMainLogic.State.bSystemCleaningActive == 0 && gPar.MachinePar.MachineModule.bCleaningEn == 1 && gPar.MachinePar.SystemCleaning.bEnable == 1);
				if(Vis.PageSystemCleaning.numGlueTankCleaningPeriod.bInputCompleted == 1)
				{
					Vis.PageSystemCleaning.numGlueTankCleaningPeriod.bInputCompleted = 0;
					gPar.MachinePar.SystemCleaning.nGlueTankCleaningPeriod = Vis.PageSystemCleaning.numGlueTankCleaningPeriod.nValue;
				}
				else
				{
					Vis.PageSystemCleaning.numGlueTankCleaningPeriod.nValue = gPar.MachinePar.SystemCleaning.nGlueTankCleaningPeriod;
				}
				BrbVc4SetControlEnability(&Vis.PageSystemCleaning.numDetergentTankCleaningPeriod.nStatus, gMainLogic.State.bSystemCleaningActive == 0 && gPar.MachinePar.MachineModule.bCleaningEn == 1 && gPar.MachinePar.SystemCleaning.bEnable == 1);
				if(Vis.PageSystemCleaning.numDetergentTankCleaningPeriod.bInputCompleted == 1)
				{
					Vis.PageSystemCleaning.numDetergentTankCleaningPeriod.bInputCompleted = 0;
					gPar.MachinePar.SystemCleaning.nDetergentTankCleaningPeriod = Vis.PageSystemCleaning.numDetergentTankCleaningPeriod.nValue;
				}
				else
				{
					Vis.PageSystemCleaning.numDetergentTankCleaningPeriod.nValue = gPar.MachinePar.SystemCleaning.nDetergentTankCleaningPeriod;
				}
				// Start/Stop
				BrbVc4SetControlVisibility(&Vis.PageSystemCleaning.btnStart.nStatus, gMainLogic.State.bSystemCleaningActive == 0);
				BrbVc4SetControlEnability(&Vis.PageSystemCleaning.btnStart.nStatus, gMainLogic.State.bAutomatic == 0 && gMainLogic.State.bCleaningActive == 0 && gPar.MachinePar.SystemCleaning.bEnable == 1);
				if(BrbVc4HandleButton(&Vis.PageSystemCleaning.btnStart) == 1)
				{
					Vis.PageSystemCleaning.btnStart.bClicked = 0;
					if(BrbSetCaller(&gMainLogic.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gMainLogic.CallerBox.bSystemCleaningStart = 1;
					}
				}
				BrbVc4SetControlVisibility(&Vis.PageSystemCleaning.btnCancel.nStatus, gMainLogic.State.bSystemCleaningActive == 1);				
				if(BrbVc4HandleButton(&Vis.PageSystemCleaning.btnCancel) == 1)
				{
					Vis.PageSystemCleaning.btnCancel.bClicked = 0;
					gMainLogic.DirectBox.bSystemCleaningCancel = 1;
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, Vis.PageHandling.nPageCurrent-1, 0);
				}
				// Toolbar
				HandleToolbar();
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_PRESSURE_CONTROL:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Druck-Korrektur
				if(BrbVc4HandleButton(&Vis.PagePressureControl.btnCorrectionMinus) == 1)
				{
					Vis.PagePressureControl.btnCorrectionMinus.bClicked = 0;
					gPressureCtrl.Par.rPressureCorr -= 0.1;
				}
				if(Vis.PagePressureControl.numCorrection.bInputCompleted == 1)
				{
					Vis.PagePressureControl.numCorrection.bInputCompleted = 0;
					gPressureCtrl.Par.rPressureCorr = Vis.PagePressureControl.numCorrection.rValue;
				}
				else
				{
					Vis.PagePressureControl.numCorrection.rValue = gPressureCtrl.Par.rPressureCorr;
				}
				if(BrbVc4HandleButton(&Vis.PagePressureControl.btnCorrectionPlus) == 1)
				{
					Vis.PagePressureControl.btnCorrectionPlus.bClicked = 0;
					gPressureCtrl.Par.rPressureCorr += 0.1;
				}
				// Toolbar
				HandleToolbar();
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_MOTORIZED_ADJUSTMENT0:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
				Vis.PageMotorizedAdjustment.Vertical.btnAxJogNeg.fbUnpush.PT = nVIS_MOT_ADJUST_JOG_TIMEOUT;
				Vis.PageMotorizedAdjustment.Vertical.btnAxJogPos.fbUnpush.PT = nVIS_MOT_ADJUST_JOG_TIMEOUT;
				Vis.PageMotorizedAdjustment.Horizontal.btnAxJogNeg.fbUnpush.PT = nVIS_MOT_ADJUST_JOG_TIMEOUT;
				Vis.PageMotorizedAdjustment.Horizontal.btnAxJogPos.fbUnpush.PT = nVIS_MOT_ADJUST_JOG_TIMEOUT;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Vertical
				BOOL bVerticalExists = gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].bVertical;
				BOOL bVerticalHoming = gAxisControl.State.AxisState[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Vertical.Homing;
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.numAxLimitNeg.nStatus, bVerticalExists);
				Vis.PageMotorizedAdjustment.Vertical.numAxLimitNeg.rValue = gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Alignment[eVERTICAL].rNegativeSwLimit;
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.numAxLimitPos.nStatus, bVerticalExists);
				Vis.PageMotorizedAdjustment.Vertical.numAxLimitPos.rValue = gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Alignment[eVERTICAL].rPositiveSwLimit;
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.btnAxJogNeg.nStatus, bVerticalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Vertical.btnAxJogNeg.nStatus, bVerticalHoming == 0);
				BrbVc4HandleJogButton(&Vis.PageMotorizedAdjustment.Vertical.btnAxJogNeg);
				if(Vis.PageMotorizedAdjustment.Vertical.btnAxJogNeg.eJogState == eBRB_JOG_BTN_STATE_PUSHED_EDGE || Vis.PageMotorizedAdjustment.Vertical.btnAxJogNeg.eJogState == eBRB_JOG_BTN_STATE_PUSHED)
				{
					gAxisControl.DirectBox.HmiCmd[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Vertical.bJogNeg = 1;
				}
				else
				{
					gAxisControl.DirectBox.HmiCmd[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Vertical.bJogNeg = 0;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.btnAxJogPos.nStatus, bVerticalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Vertical.btnAxJogPos.nStatus, bVerticalHoming == 0);
				BrbVc4HandleJogButton(&Vis.PageMotorizedAdjustment.Vertical.btnAxJogPos);
				if(Vis.PageMotorizedAdjustment.Vertical.btnAxJogPos.eJogState == eBRB_JOG_BTN_STATE_PUSHED_EDGE || Vis.PageMotorizedAdjustment.Vertical.btnAxJogPos.eJogState == eBRB_JOG_BTN_STATE_PUSHED)
				{
					gAxisControl.DirectBox.HmiCmd[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Vertical.bJogPos = 1;
				}
				else
				{
					gAxisControl.DirectBox.HmiCmd[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Vertical.bJogPos = 0;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.numAxPos.nStatus, bVerticalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Vertical.numAxPos.nStatus, bVerticalHoming == 0);
				if(Vis.PageMotorizedAdjustment.Vertical.numAxPos.bInputCompleted == 1)
				{
					Vis.PageMotorizedAdjustment.Vertical.numAxPos.bInputCompleted = 0;
					if(bVerticalHoming == 0)
					{
						if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
						{
							gAxisControl.Par.HmiPar.AbsPos[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Vertical = Vis.PageMotorizedAdjustment.Vertical.numAxPos.rValue;
							gAxisControl.CallerBox.HmiCaller.Vertical[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].bStartAbsMove = 1;
						}
					}
				}
				else
				{
					Vis.PageMotorizedAdjustment.Vertical.numAxPos.rValue = gAxisControl.State.HmiState[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Vertical.rActPosition;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.btnHome.nStatus, bVerticalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Vertical.btnHome.nStatus, bVerticalHoming == 0);
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.Vertical.btnHome) == 1)
				{
					Vis.PageMotorizedAdjustment.Vertical.btnHome.bClicked = 0;
					if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gAxisControl.Par.HmiPar.nRefAxisIdx = Vis.PageMotorizedAdjustment.nSelectedHeadIndex;
						gAxisControl.CallerBox.HmiCaller.Vertical[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].bRefAxis = 1;
					}
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.btnStop.nStatus, bVerticalExists);
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.Vertical.btnStop) == 1)
				{
					Vis.PageMotorizedAdjustment.Vertical.btnStop.bClicked = 0;
					gAxisControl.DirectBox.HmiCmd[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Vertical.bStop = 1;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.numParkPos.nStatus, bVerticalExists);
				if(Vis.PageMotorizedAdjustment.Vertical.numParkPos.bInputCompleted == 1)
				{
					Vis.PageMotorizedAdjustment.Vertical.numParkPos.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eVERTICAL].rParkPosition = Vis.PageMotorizedAdjustment.Vertical.numParkPos.rValue;
				}
				else
				{
					Vis.PageMotorizedAdjustment.Vertical.numParkPos.rValue = gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eVERTICAL].rParkPosition;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.btnMoveToParkPos.nStatus, bVerticalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Vertical.btnMoveToParkPos.nStatus, bVerticalHoming == 0);
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.Vertical.btnMoveToParkPos) == 1)
				{
					Vis.PageMotorizedAdjustment.Vertical.btnMoveToParkPos.bClicked = 0;
					if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gAxisControl.Par.HmiPar.AbsPos[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Vertical = gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eVERTICAL].rParkPosition;
						gAxisControl.CallerBox.HmiCaller.Vertical[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].bStartAbsMove = 1;
					}
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.numWorkPos.nStatus, bVerticalExists);
				if(Vis.PageMotorizedAdjustment.Vertical.numWorkPos.bInputCompleted == 1)
				{
					Vis.PageMotorizedAdjustment.Vertical.numWorkPos.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eVERTICAL].rWorkPosition = Vis.PageMotorizedAdjustment.Vertical.numWorkPos.rValue;
				}
				else
				{
					Vis.PageMotorizedAdjustment.Vertical.numWorkPos.rValue = gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eVERTICAL].rWorkPosition;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Vertical.btnMoveToWorkPos.nStatus, bVerticalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Vertical.btnMoveToWorkPos.nStatus, bVerticalHoming == 0);
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.Vertical.btnMoveToWorkPos) == 1)
				{
					Vis.PageMotorizedAdjustment.Vertical.btnMoveToWorkPos.bClicked = 0;
					if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gAxisControl.Par.HmiPar.AbsPos[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Vertical = gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eVERTICAL].rWorkPosition;
						gAxisControl.CallerBox.HmiCaller.Vertical[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].bStartAbsMove = 1;
					}
				}
				// Alle Achsen
				BOOL bHomingAxis = 0;
				for(nIndex=0; nIndex<=nDEV_INDEX_MAX; nIndex++)
				{
					if(gAxisControl.State.AxisState[nIndex].Vertical.Homing == 1)
					{
						bHomingAxis = 1;
					}
					if(gAxisControl.State.AxisState[nIndex].Horizontal.Homing == 1)
					{
						bHomingAxis = 1;
					}
				}
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.btnHomeAllAxes.nStatus, bHomingAxis == 0);
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.btnHomeAllAxes) == 1)
				{
					Vis.PageMotorizedAdjustment.btnHomeAllAxes.bClicked = 0;
					if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gAxisControl.CallerBox.HmiCaller.bRefAllAxes = 1;
					}
				}
				//K�pfe
				for(nHeadIndex=0; nHeadIndex<=nVIS_HEAD_MAIN_SHOW_INDEX_MAX; nHeadIndex++)
				{
					if(nHeadIndex+Vis.Heads.nHeadOffset <= gPar.MachinePar.nBonDevIndex)
					{
						if(nHeadIndex+Vis.Heads.nHeadOffset == Vis.PageMotorizedAdjustment.nSelectedHeadIndex)
						{
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, Vis.GeneralOwn.bBlink400);
						}
						else
						{
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 1);
						}
						if(gCalcPos.State.HeadState[nHeadIndex+Vis.Heads.nHeadOffset].bHeadError == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_RED;
						}	
						else if(gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].bBonDevEn == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_BLUE;
						}
						else
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_GREY;
						}
						if(Vis.Heads.Head[nHeadIndex].btnHead.bClicked == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.bClicked = 0;
							Vis.PageMotorizedAdjustment.nSelectedHeadIndex = nHeadIndex+Vis.Heads.nHeadOffset;
						}
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 1);
						Vis.Heads.Head[nHeadIndex].numHead.nValue = nHeadIndex+Vis.Heads.nHeadOffset + 1;
					}
					else
					{
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 0);
					}
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.btnRight) == 1)
				{
					Vis.PageMotorizedAdjustment.btnRight.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_MOTORIZED_ADJUSTMENT1, 0);
				}
				if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex <= nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
				{
					SetNavigationButtons(0, 0, 0, 0);
				}
				else if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex > nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
				{
					SetNavigationButtons(0, 1, 0, 0);
				}
				else if(Vis.Heads.nHeadOffset >= gPar.MachinePar.nBonDevIndex - nVIS_HEAD_MAIN_SHOW_INDEX_MAX )
				{
					SetNavigationButtons(1, 0, 0, 0);
				}
				else
				{
					SetNavigationButtons(1, 1, 0, 0);
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					Vis.Heads.nHeadOffset -= nVIS_HEAD_MAIN_SHOW_INDEX_MAX+1;
					if(Vis.Heads.nHeadOffset < 0)
					{
						Vis.Heads.nHeadOffset = 0;
					}
					Vis.PageHead.nSwitchOffset = 0;
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					Vis.Heads.nHeadOffset += nVIS_HEAD_MAIN_SHOW_INDEX_MAX+1;
					if(Vis.Heads.nHeadOffset > nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
					{
						Vis.Heads.nHeadOffset = nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX;
					}
					Vis.PageHead.nSwitchOffset = 0;
				}
				// Toolbar
				HandleToolbar();
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
				for(nIndex=0; nIndex<=nDEV_INDEX_MAX; nIndex++)
				{
					gAxisControl.DirectBox.HmiCmd[nIndex].Vertical.bJogNeg = 0;
					gAxisControl.DirectBox.HmiCmd[nIndex].Vertical.bJogPos = 0;
					gAxisControl.DirectBox.HmiCmd[nIndex].Horizontal.bJogNeg = 0;
					gAxisControl.DirectBox.HmiCmd[nIndex].Horizontal.bJogPos = 0;
				}
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_MOTORIZED_ADJUSTMENT1:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
				Vis.PageMotorizedAdjustment.Vertical.btnAxJogNeg.fbUnpush.PT = nVIS_MOT_ADJUST_JOG_TIMEOUT;
				Vis.PageMotorizedAdjustment.Vertical.btnAxJogPos.fbUnpush.PT = nVIS_MOT_ADJUST_JOG_TIMEOUT;
				Vis.PageMotorizedAdjustment.Horizontal.btnAxJogNeg.fbUnpush.PT = nVIS_MOT_ADJUST_JOG_TIMEOUT;
				Vis.PageMotorizedAdjustment.Horizontal.btnAxJogPos.fbUnpush.PT = nVIS_MOT_ADJUST_JOG_TIMEOUT;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Horizontal
				BOOL bHorizontalExists = gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].bHorizontal;
				BOOL bHorizontalHoming = gAxisControl.State.AxisState[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Horizontal.Homing;
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.numAxLimitNeg.nStatus, bHorizontalExists);
				Vis.PageMotorizedAdjustment.Horizontal.numAxLimitNeg.rValue = gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Alignment[eHORIZONTAL].rNegativeSwLimit;
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.numAxLimitPos.nStatus, bHorizontalExists);
				Vis.PageMotorizedAdjustment.Horizontal.numAxLimitPos.rValue = gPar.MachinePar.MotorisedAdjustment.AxesLimits[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Alignment[eHORIZONTAL].rPositiveSwLimit;
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.btnAxJogNeg.nStatus, bHorizontalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Horizontal.btnAxJogNeg.nStatus, bHorizontalHoming == 0);
				BrbVc4HandleJogButton(&Vis.PageMotorizedAdjustment.Horizontal.btnAxJogNeg);
				if(Vis.PageMotorizedAdjustment.Horizontal.btnAxJogNeg.eJogState == eBRB_JOG_BTN_STATE_PUSHED_EDGE || Vis.PageMotorizedAdjustment.Horizontal.btnAxJogNeg.eJogState == eBRB_JOG_BTN_STATE_PUSHED)
				{
					gAxisControl.DirectBox.HmiCmd[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Horizontal.bJogNeg = 1;
				}
				else
				{
					gAxisControl.DirectBox.HmiCmd[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Horizontal.bJogNeg = 0;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.btnAxJogPos.nStatus, bHorizontalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Horizontal.btnAxJogPos.nStatus, bHorizontalHoming == 0);
				BrbVc4HandleJogButton(&Vis.PageMotorizedAdjustment.Horizontal.btnAxJogPos);
				if(Vis.PageMotorizedAdjustment.Horizontal.btnAxJogPos.eJogState == eBRB_JOG_BTN_STATE_PUSHED_EDGE || Vis.PageMotorizedAdjustment.Horizontal.btnAxJogPos.eJogState == eBRB_JOG_BTN_STATE_PUSHED)
				{
					gAxisControl.DirectBox.HmiCmd[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Horizontal.bJogPos = 1;
				}
				else
				{
					gAxisControl.DirectBox.HmiCmd[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Horizontal.bJogPos = 0;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.numAxPos.nStatus, bHorizontalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Horizontal.numAxPos.nStatus, bHorizontalHoming == 0);
				if(Vis.PageMotorizedAdjustment.Horizontal.numAxPos.bInputCompleted == 1)
				{
					Vis.PageMotorizedAdjustment.Horizontal.numAxPos.bInputCompleted = 0;
					if(bHorizontalHoming == 0)
					{
						if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
						{
							gAxisControl.Par.HmiPar.AbsPos[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Horizontal = Vis.PageMotorizedAdjustment.Horizontal.numAxPos.rValue;
							gAxisControl.CallerBox.HmiCaller.Horizontal[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].bStartAbsMove = 1;
						}
					}
				}
				else
				{
					Vis.PageMotorizedAdjustment.Horizontal.numAxPos.rValue = gAxisControl.State.HmiState[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Horizontal.rActPosition;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.btnHome.nStatus, bHorizontalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Horizontal.btnHome.nStatus, bHorizontalHoming == 0);
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.Horizontal.btnHome) == 1)
				{
					Vis.PageMotorizedAdjustment.Horizontal.btnHome.bClicked = 0;
					if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gAxisControl.Par.HmiPar.nRefAxisIdx = Vis.PageMotorizedAdjustment.nSelectedHeadIndex;
						gAxisControl.CallerBox.HmiCaller.Horizontal[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].bRefAxis = 1;
					}
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.btnStop.nStatus, bHorizontalExists);
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.Horizontal.btnStop) == 1)
				{
					Vis.PageMotorizedAdjustment.Horizontal.btnStop.bClicked = 0;
					gAxisControl.DirectBox.HmiCmd[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Horizontal.bStop = 1;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.numParkPos.nStatus, bHorizontalExists);
				if(Vis.PageMotorizedAdjustment.Horizontal.numParkPos.bInputCompleted == 1)
				{
					Vis.PageMotorizedAdjustment.Horizontal.numParkPos.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eHORIZONTAL].rParkPosition = Vis.PageMotorizedAdjustment.Horizontal.numParkPos.rValue;
				}
				else
				{
					Vis.PageMotorizedAdjustment.Horizontal.numParkPos.rValue = gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eHORIZONTAL].rParkPosition;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.btnMoveToParkPos.nStatus, bHorizontalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Horizontal.btnMoveToParkPos.nStatus, bHorizontalHoming == 0);
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.Horizontal.btnMoveToParkPos) == 1)
				{
					Vis.PageMotorizedAdjustment.Horizontal.btnMoveToParkPos.bClicked = 0;
					if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gAxisControl.Par.HmiPar.AbsPos[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Horizontal = gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eHORIZONTAL].rParkPosition;
						gAxisControl.CallerBox.HmiCaller.Horizontal[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].bStartAbsMove = 1;
					}
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.numWorkPos.nStatus, bHorizontalExists);
				if(Vis.PageMotorizedAdjustment.Horizontal.numWorkPos.bInputCompleted == 1)
				{
					Vis.PageMotorizedAdjustment.Horizontal.numWorkPos.bInputCompleted = 0;
					gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eHORIZONTAL].rWorkPosition = Vis.PageMotorizedAdjustment.Horizontal.numWorkPos.rValue;
				}
				else
				{
					Vis.PageMotorizedAdjustment.Horizontal.numWorkPos.rValue = gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eHORIZONTAL].rWorkPosition;
				}
				BrbVc4SetControlVisibility(&Vis.PageMotorizedAdjustment.Horizontal.btnMoveToWorkPos.nStatus, bHorizontalExists);
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.Horizontal.btnMoveToWorkPos.nStatus, bHorizontalHoming == 0);
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.Horizontal.btnMoveToWorkPos) == 1)
				{
					Vis.PageMotorizedAdjustment.Horizontal.btnMoveToWorkPos.bClicked = 0;
					if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gAxisControl.Par.HmiPar.AbsPos[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].Horizontal = gPar.ProductPar.BonDevPar[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].AxesPosition.Alignment[eHORIZONTAL].rWorkPosition;
						gAxisControl.CallerBox.HmiCaller.Horizontal[Vis.PageMotorizedAdjustment.nSelectedHeadIndex].bStartAbsMove = 1;
					}
				}
				// Alle Achsen
				BOOL bHomingAxis = 0;
				for(nIndex=0; nIndex<=nDEV_INDEX_MAX; nIndex++)
				{
					if(gAxisControl.State.AxisState[nIndex].Vertical.Homing == 1)
					{
						bHomingAxis = 1;
					}
					if(gAxisControl.State.AxisState[nIndex].Horizontal.Homing == 1)
					{
						bHomingAxis = 1;
					}
				}
				BrbVc4SetControlEnability(&Vis.PageMotorizedAdjustment.btnHomeAllAxes.nStatus, bHomingAxis == 0);
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.btnHomeAllAxes) == 1)
				{
					Vis.PageMotorizedAdjustment.btnHomeAllAxes.bClicked = 0;
					if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gAxisControl.CallerBox.HmiCaller.bRefAllAxes = 1;
					}
				}
				//K�pfe
				for(nHeadIndex=0; nHeadIndex<=nVIS_HEAD_MAIN_SHOW_INDEX_MAX; nHeadIndex++)
				{
					if(nHeadIndex+Vis.Heads.nHeadOffset <= gPar.MachinePar.nBonDevIndex)
					{
						if(nHeadIndex+Vis.Heads.nHeadOffset == Vis.PageMotorizedAdjustment.nSelectedHeadIndex)
						{
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, Vis.GeneralOwn.bBlink400);
						}
						else
						{
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 1);
						}
						if(gCalcPos.State.HeadState[nHeadIndex+Vis.Heads.nHeadOffset].bHeadError == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_RED;
						}	
						else if(gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].bBonDevEn == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_BLUE;
						}
						else
						{
							Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_GREY;
						}
						if(Vis.Heads.Head[nHeadIndex].btnHead.bClicked == 1)
						{
							Vis.Heads.Head[nHeadIndex].btnHead.bClicked = 0;
							Vis.PageMotorizedAdjustment.nSelectedHeadIndex = nHeadIndex+Vis.Heads.nHeadOffset;
						}
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 1);
						Vis.Heads.Head[nHeadIndex].numHead.nValue = nHeadIndex+Vis.Heads.nHeadOffset + 1;
					}
					else
					{
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 0);
						BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 0);
					}
				}
				// Navigation
				if(BrbVc4HandleButton(&Vis.PageMotorizedAdjustment.btnLeft) == 1)
				{
					Vis.PageMotorizedAdjustment.btnLeft.bClicked = 0;
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_MOTORIZED_ADJUSTMENT0, 0);
				}
				if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex <= nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
				{
					SetNavigationButtons(0, 0, 0, 0);
				}
				else if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex > nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
				{
					SetNavigationButtons(0, 1, 0, 0);
				}
				else if(Vis.Heads.nHeadOffset >= gPar.MachinePar.nBonDevIndex - nVIS_HEAD_MAIN_SHOW_INDEX_MAX )
				{
					SetNavigationButtons(1, 0, 0, 0);
				}
				else
				{
					SetNavigationButtons(1, 1, 0, 0);
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
				{
					Vis.Navigation.btnLeft.bClicked = 0;
					Vis.Heads.nHeadOffset -= nVIS_HEAD_MAIN_SHOW_INDEX_MAX+1;
					if(Vis.Heads.nHeadOffset < 0)
					{
						Vis.Heads.nHeadOffset = 0;
					}
					Vis.PageHead.nSwitchOffset = 0;
				}
				if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
				{
					Vis.Navigation.btnRight.bClicked = 0;
					Vis.Heads.nHeadOffset += nVIS_HEAD_MAIN_SHOW_INDEX_MAX+1;
					if(Vis.Heads.nHeadOffset > nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
					{
						Vis.Heads.nHeadOffset = nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX;
					}
					Vis.PageHead.nSwitchOffset = 0;
				}
				// Toolbar
				HandleToolbar();
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
				for(nIndex=0; nIndex<=nDEV_INDEX_MAX; nIndex++)
				{
					gAxisControl.DirectBox.HmiCmd[nIndex].Vertical.bJogNeg = 0;
					gAxisControl.DirectBox.HmiCmd[nIndex].Vertical.bJogPos = 0;
					gAxisControl.DirectBox.HmiCmd[nIndex].Horizontal.bJogNeg = 0;
					gAxisControl.DirectBox.HmiCmd[nIndex].Horizontal.bJogPos = 0;
				}
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_CAMERA:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				BrbVc4SetControlEnability(&Vis.PageCamera.btnReset.nStatus, Vis.GeneralOwn.nUserLevel == eUSERLEVEL_SERVICE);
				if(BrbVc4HandleButton(&Vis.PageCamera.btnReset) == 1)
				{
					Vis.PageCamera.btnReset.bClicked = 0;
					gMachine.DirectBox.bResetCntImgProc = 1;
				}
				// Toolbar
				HandleToolbar();
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_TEMPCTRL:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				// Tabs
				BrbVc4HandleTabCtrl(&Vis.tcTabCtrl);
				if(Vis.tcTabCtrl.nSelectedTabPageIndex == 0)
				{
					// Anzahl aktivierter Tanks
					UINT nTankCount = 0;
					for(nTankIndex=0; nTankIndex<=nIDX_TEMP_CTRL_TANK_MAX; nTankIndex++)
					{
						if(gPar.MachinePar.TempCtrlConfig.Tank[nTankIndex].bExist == 1)
						{
							nTankCount += 1;
						}
					}
					// Tank
					BrbVc4SetControlVisibility(&Vis.PageTempCtrl.btnHeatingStartStop.nStatus, nTankCount > 0);
					BrbVc4SetControlEnability(&Vis.PageTempCtrl.btnHeatingStartStop.nStatus, 1);
					if(BrbVc4HandleButton(&Vis.PageTempCtrl.btnHeatingStartStop) == 1)
					{
						Vis.PageTempCtrl.btnHeatingStartStop.bClicked = 0;
						if(gTempCtrl.State.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bHeatingOn == 0)
						{
							if(BrbSetCaller(&gTempCtrl.CallerBox.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
							{
								gTempCtrl.CallerBox.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bStartHeating = 1;
							}		
						}
						else
						{
							gTempCtrl.DirectBox.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bStopHeating = 1;
						}
					}
					Vis.PageTempCtrl.btnHeatingStartStop.nTextIndex = gTempCtrl.State.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bHeatingOn;
					BrbVc4SetControlColor(&Vis.PageTempCtrl.btnHeatingStartStop.nColor, gTempCtrl.State.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bHeatingOn == 1, eBACKCOLOR_GREEN + eFORECOLOR_BLACK, eBACKCOLOR_RED + eFORECOLOR_BLACK);
					BrbVc4SetControlVisibility(&Vis.PageTempCtrl.numActTempTank.nStatus, nTankCount > 0);
					Vis.PageTempCtrl.numActTempTank.rValue = gTempCtrl.State.Tank[Vis.PageTempCtrl.nSelectedTankIndex].rActTemp;
					BrbVc4SetControlVisibility(&Vis.PageTempCtrl.numSetTempTank.nStatus, nTankCount > 0);
					BrbVc4SetControlEnability(&Vis.PageTempCtrl.numSetTempTank.nStatus, 1);
					Vis.PageTempCtrl.numSetTempTank.rMax = gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageTempCtrl.nSelectedTankIndex].TempZonePar.rSetTempMax;
					if(Vis.PageTempCtrl.numSetTempTank.bInputCompleted == 1)
					{
						Vis.PageTempCtrl.numSetTempTank.bInputCompleted = 0;
						gPar.ProductPar.TempCtrlPar.Tank[Vis.PageTempCtrl.nSelectedTankIndex].rSetTemp = Vis.PageTempCtrl.numSetTempTank.rValue;
						if( Vis.PageTempCtrl.numSetTempTank.rValue < gPar.ProductPar.TempCtrlPar.Tank[Vis.PageTempCtrl.nSelectedTankIndex].rTempStandby )
						{
							gPar.ProductPar.TempCtrlPar.Tank[Vis.PageTempCtrl.nSelectedTankIndex].rTempStandby = Vis.PageTempCtrl.numSetTempTank.rValue;
						}
					}
					else
					{
						Vis.PageTempCtrl.numSetTempTank.rValue = gPar.ProductPar.TempCtrlPar.Tank[Vis.PageTempCtrl.nSelectedTankIndex].rSetTemp;
					}
					BrbVc4SetControlVisibility(&Vis.PageTempCtrl.btnStandbyStartStop.nStatus, nTankCount > 0);
					BrbVc4SetControlEnability(&Vis.PageTempCtrl.btnStandbyStartStop.nStatus, gTempCtrl.State.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bHeatingOn == 1);
					if(BrbVc4HandleButton(&Vis.PageTempCtrl.btnStandbyStartStop) == 1)
					{
						Vis.PageTempCtrl.btnStandbyStartStop.bClicked = 0;
						if(gTempCtrl.State.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bStandbyOn == 0)
						{
							if(BrbSetCaller(&gTempCtrl.CallerBox.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
							{
								gTempCtrl.CallerBox.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bStartSystemStandby = 1;
							}		
						}
						else
						{
							if(BrbSetCaller(&gTempCtrl.CallerBox.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
							{
								gTempCtrl.CallerBox.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bStopSystemStandby = 1;
							}		
						}
					}
					Vis.PageTempCtrl.btnStandbyStartStop.nTextIndex = gTempCtrl.State.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bStandbyOn;
					BrbVc4SetControlColor(&Vis.PageTempCtrl.btnStandbyStartStop.nColor, gTempCtrl.State.HeatingSystem[Vis.PageTempCtrl.nSelectedTankIndex].bStandbyOn == 1, eBACKCOLOR_GREEN + eFORECOLOR_BLACK, eBACKCOLOR_RED + eFORECOLOR_BLACK);
					BrbVc4SetControlVisibility(&Vis.PageTempCtrl.numStandbyTemp.nStatus, nTankCount > 0);
					BrbVc4SetControlEnability(&Vis.PageTempCtrl.numStandbyTemp.nStatus, 1);
					Vis.PageTempCtrl.numStandbyTemp.rMax = gPar.MachinePar.TempCtrlConfig.Tank[Vis.PageTempCtrl.nSelectedTankIndex].TempZonePar.rSetTempMax;
					if(Vis.PageTempCtrl.numStandbyTemp.bInputCompleted == 1)
					{
						Vis.PageTempCtrl.numStandbyTemp.bInputCompleted = 0;
						gPar.ProductPar.TempCtrlPar.Tank[Vis.PageTempCtrl.nSelectedTankIndex].rTempStandby = Vis.PageTempCtrl.numStandbyTemp.rValue;
					}
					else
					{
						Vis.PageTempCtrl.numStandbyTemp.rMax = gPar.ProductPar.TempCtrlPar.Tank[Vis.PageTempCtrl.nSelectedTankIndex].rSetTemp;
						Vis.PageTempCtrl.numStandbyTemp.rValue = gPar.ProductPar.TempCtrlPar.Tank[Vis.PageTempCtrl.nSelectedTankIndex].rTempStandby;
					}
					// Tanks
					BOOL bSetTankSelection = 0;
					for(nTankIndex=0; nTankIndex<=nVIS_TANK_SHOW_INDEX_MAX; nTankIndex++)
					{
						if(nTankIndex+Vis.Tanks.nTankOffset <= nIDX_TEMP_CTRL_TANK_MAX)
						{
							if(gPar.MachinePar.TempCtrlConfig.Tank[nTankIndex+Vis.Tanks.nTankOffset].bExist == 1)
							{
								if(bSetTankSelection == 1)
								{
									bSetTankSelection = 0;
									Vis.PageTempCtrl.nSelectedTankIndex = nTankIndex+Vis.Tanks.nTankOffset;
								}
								if(nTankIndex+Vis.Tanks.nTankOffset == Vis.PageTempCtrl.nSelectedTankIndex)
								{
									BrbVc4SetControlVisibility(&Vis.Tanks.Tank[nTankIndex].btnTank.nStatus, Vis.GeneralOwn.bBlink400);
								}
								else
								{
									BrbVc4SetControlVisibility(&Vis.Tanks.Tank[nTankIndex].btnTank.nStatus, 1);
								}
								if(gTempCtrl.State.HeatingSystem[nTankIndex+Vis.Tanks.nTankOffset].bErrorHeatingSystem == 1)
								{
									Vis.Tanks.Tank[nTankIndex].btnTank.nBmpIndex = eTANK_BMP_INDEX_SMALL_RED;
								}
								else if(gTempCtrl.State.Tank[nTankIndex+Vis.Tanks.nTankOffset].bErrorTempZone == 1)
								{
									Vis.Tanks.Tank[nTankIndex].btnTank.nBmpIndex = eTANK_BMP_INDEX_SMALL_RED;
								}
								else if(gTempCtrl.State.Tank[nTankIndex+Vis.Tanks.nTankOffset].bHeatingOn == 1)
								{
									Vis.Tanks.Tank[nTankIndex].btnTank.nBmpIndex = eTANK_BMP_INDEX_SMALL_BLUE;
								}
								else
								{
									Vis.Tanks.Tank[nTankIndex].btnTank.nBmpIndex = eTANK_BMP_INDEX_SMALL_GREY;
								}
								if(Vis.Tanks.Tank[nTankIndex].btnTank.bClicked == 1)
								{
									Vis.Tanks.Tank[nTankIndex].btnTank.bClicked = 0;
									Vis.PageTempCtrl.nSelectedTankIndex = nTankIndex+Vis.Tanks.nTankOffset;
								}
								BrbVc4SetControlVisibility(&Vis.Tanks.Tank[nTankIndex].numTank.nStatus, 1);
								Vis.Tanks.Tank[nTankIndex].numTank.nValue = nTankIndex+Vis.Tanks.nTankOffset + 1;
							}
							else
							{
								BrbVc4SetControlVisibility(&Vis.Tanks.Tank[nTankIndex].btnTank.nStatus, 0);
								BrbVc4SetControlVisibility(&Vis.Tanks.Tank[nTankIndex].numTank.nStatus, 0);
								if(nTankIndex+Vis.Tanks.nTankOffset == Vis.PageTempCtrl.nSelectedTankIndex)
								{
									Vis.PageTempCtrl.nSelectedTankIndex = 0;
									bSetTankSelection = 1;
								}
							}
						}
						else
						{
							BrbVc4SetControlVisibility(&Vis.Tanks.Tank[nTankIndex].btnTank.nStatus, 0);
							BrbVc4SetControlVisibility(&Vis.Tanks.Tank[nTankIndex].numTank.nStatus, 0);
						}
					}
					// Navigation
					if(nTankCount == 0)
					{
						SetNavigationButtons(0, 0, 0, 0);
					}
					else if(Vis.Tanks.nTankOffset == 0 && nIDX_TEMP_CTRL_TANK_MAX <= nVIS_TANK_SHOW_INDEX_MAX)
					{
						SetNavigationButtons(0, 0, 0, 0);
					}
					else if(Vis.Tanks.nTankOffset == 0 && nIDX_TEMP_CTRL_TANK_MAX > nVIS_TANK_SHOW_INDEX_MAX)
					{
						SetNavigationButtons(0, 1, 0, 0);
					}
					else if(Vis.Tanks.nTankOffset >= nIDX_TEMP_CTRL_TANK_MAX - nVIS_TANK_SHOW_INDEX_MAX )
					{
						SetNavigationButtons(1, 0, 0, 0);
					}
					else
					{
						SetNavigationButtons(1, 1, 0, 0);
					}
					if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
					{
						Vis.Navigation.btnLeft.bClicked = 0;
						Vis.Tanks.nTankOffset -= nVIS_TANK_SHOW_INDEX_MAX+1;
						if(Vis.Tanks.nTankOffset < 0)
						{
							Vis.Tanks.nTankOffset = 0;
						}
					}
					if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
					{
						Vis.Navigation.btnRight.bClicked = 0;
						Vis.Tanks.nTankOffset += nVIS_TANK_SHOW_INDEX_MAX+1;
						if(Vis.Tanks.nTankOffset > nIDX_TEMP_CTRL_TANK_MAX - nVIS_TANK_SHOW_INDEX_MAX)
						{
							Vis.Tanks.nTankOffset = nIDX_TEMP_CTRL_TANK_MAX - nVIS_TANK_SHOW_INDEX_MAX;
						}
					}
				}
				else if(Vis.tcTabCtrl.nSelectedTabPageIndex == 1)
				{
					// Gibts den Tank f�r den ausgew�hlten Kopf?
					BOOL bTankExists = gPar.MachinePar.TempCtrlConfig.Tank[gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].nTankIdx].bExist;
					// Anzahl aktivierter K�pfe
					UINT nHeadCount = 0;
					for(nHeadIndex=0; nHeadIndex<=nDEV_INDEX_MAX; nHeadIndex++)
					{
						if(gPar.MachinePar.TempCtrlConfig.BonDev[nHeadIndex].bExist == 1)
						{
							nHeadCount += 1;
						}
					}
					// Kopf
					BrbVc4SetControlVisibility(&Vis.PageTempCtrl.chkHeatingZonesEnable.nStatus, bTankExists == 1 && nHeadCount > 0);
					BrbVc4SetControlEnability(&Vis.PageTempCtrl.chkHeatingZonesEnable.nStatus, 1);
					if(BrbVc4HandleCheckbox(&Vis.PageTempCtrl.chkHeatingZonesEnable) == 1)
					{
						gPar.ProductPar.TempCtrlPar.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].bEnable = Vis.PageTempCtrl.chkHeatingZonesEnable.bChecked;
					}
					else
					{
						Vis.PageTempCtrl.chkHeatingZonesEnable.bChecked = gPar.ProductPar.TempCtrlPar.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].bEnable;
					}
					UINT nZoneIndex = 0;
					for(nZoneIndex=0; nZoneIndex<=nIDX_TEMP_CTRL_ZONE_MAX; nZoneIndex++)
					{
						BrbVc4SetControlVisibility(&Vis.PageTempCtrl.Zone[nZoneIndex].numActTemp.nStatus, bTankExists == 1 && nHeadCount > 0 && gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].TempZone[nZoneIndex].bZoneExist == 1);
						Vis.PageTempCtrl.Zone[nZoneIndex].numActTemp.rValue = gTempCtrl.State.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].TempZone[nZoneIndex].rActTemp;
						BrbVc4SetControlVisibility(&Vis.PageTempCtrl.Zone[nZoneIndex].numSetTemp.nStatus, bTankExists == 1 && nHeadCount > 0 && gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].TempZone[nZoneIndex].bZoneExist == 1);
						BrbVc4SetControlEnability(&Vis.PageTempCtrl.Zone[nZoneIndex].numSetTemp.nStatus, bTankExists == 1 && nHeadCount > 0 && gPar.ProductPar.TempCtrlPar.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].bEnable == 1);
						Vis.PageTempCtrl.Zone[nZoneIndex].numSetTemp.rMax = gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].TempZone[nZoneIndex].rSetTempMax;
						if(Vis.PageTempCtrl.Zone[nZoneIndex].numSetTemp.bInputCompleted == 1)
						{
							Vis.PageTempCtrl.Zone[nZoneIndex].numSetTemp.bInputCompleted = 0;
							gPar.ProductPar.TempCtrlPar.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].rSetTemp[nZoneIndex] = Vis.PageTempCtrl.Zone[nZoneIndex].numSetTemp.rValue;
						}
						else
						{
							Vis.PageTempCtrl.Zone[nZoneIndex].numSetTemp.rValue = gPar.ProductPar.TempCtrlPar.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].rSetTemp[nZoneIndex];
						}
					}
					BrbVc4SetControlVisibility(&Vis.PageTempCtrl.numRelatedTank.nStatus, bTankExists == 1 && nHeadCount > 0);
					Vis.PageTempCtrl.numRelatedTank.nValue = gPar.MachinePar.TempCtrlConfig.BonDev[Vis.PageTempCtrl.nSelectedHeadIndex].nTankIdx+1;
					//K�pfe
					BOOL bSetHeadSelection = 0;
					for(nHeadIndex=0; nHeadIndex<=nVIS_HEAD_MAIN_SHOW_INDEX_MAX; nHeadIndex++)
					{
						if(nHeadIndex+Vis.Heads.nHeadOffset <= gPar.MachinePar.nBonDevIndex)
						{
							if(bTankExists == 1 && gPar.MachinePar.TempCtrlConfig.BonDev[nHeadIndex+Vis.Heads.nHeadOffset].bExist == 1)
							{
								if(bSetHeadSelection == 1)
								{
									bSetHeadSelection = 0;
									Vis.PageTempCtrl.nSelectedHeadIndex = nHeadIndex+Vis.Heads.nHeadOffset;
								}
								if(nHeadIndex+Vis.Heads.nHeadOffset == Vis.PageTempCtrl.nSelectedHeadIndex)
								{
									BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, Vis.GeneralOwn.bBlink400);
								}
								else
								{
									BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 1);
								}
								if(gTempCtrl.State.BonDev[nHeadIndex+Vis.Heads.nHeadOffset].bErrorBonDev == 1)
								{
									Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_RED;
								}	
								else if(gPar.ProductPar.BonDevPar[nHeadIndex+Vis.Heads.nHeadOffset].bBonDevEn == 1)
								{
									Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_BLUE;
								}
								else
								{
									Vis.Heads.Head[nHeadIndex].btnHead.nBmpIndex = eHEAD_BMP_INDEX_SMALL_GREY;
								}
								if(Vis.Heads.Head[nHeadIndex].btnHead.bClicked == 1)
								{
									Vis.Heads.Head[nHeadIndex].btnHead.bClicked = 0;
									Vis.PageTempCtrl.nSelectedHeadIndex = nHeadIndex+Vis.Heads.nHeadOffset;
								}
								BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 1);
								Vis.Heads.Head[nHeadIndex].numHead.nValue = nHeadIndex+Vis.Heads.nHeadOffset + 1;
							}
							else
							{
								BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 0);
								BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 0);
								if(nHeadIndex+Vis.Heads.nHeadOffset == Vis.PageTempCtrl.nSelectedHeadIndex)
								{
									Vis.PageTempCtrl.nSelectedHeadIndex = 0;
									bSetHeadSelection = 1;
								}
							}
						}
						else
						{
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].btnHead.nStatus, 0);
							BrbVc4SetControlVisibility(&Vis.Heads.Head[nHeadIndex].numHead.nStatus, 0);
						}
					}
					// Navigation
					if(bTankExists == 0 && nHeadCount == 0)
					{
						SetNavigationButtons(0, 0, 0, 0);
					}
					else if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex <= nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
					{
						SetNavigationButtons(0, 0, 0, 0);
					}
					else if(Vis.Heads.nHeadOffset == 0 && gPar.MachinePar.nBonDevIndex > nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
					{
						SetNavigationButtons(0, 1, 0, 0);
					}
					else if(Vis.Heads.nHeadOffset >= gPar.MachinePar.nBonDevIndex - nVIS_HEAD_MAIN_SHOW_INDEX_MAX )
					{
						SetNavigationButtons(1, 0, 0, 0);
					}
					else
					{
						SetNavigationButtons(1, 1, 0, 0);
					}
					if(BrbVc4HandleButton(&Vis.Navigation.btnLeft) == 1)
					{
						Vis.Navigation.btnLeft.bClicked = 0;
						Vis.Heads.nHeadOffset -= nVIS_HEAD_MAIN_SHOW_INDEX_MAX+1;
						if(Vis.Heads.nHeadOffset < 0)
						{
							Vis.Heads.nHeadOffset = 0;
						}
					}
					if(BrbVc4HandleButton(&Vis.Navigation.btnRight) == 1)
					{
						Vis.Navigation.btnRight.bClicked = 0;
						Vis.Heads.nHeadOffset += nVIS_HEAD_MAIN_SHOW_INDEX_MAX+1;
						if(Vis.Heads.nHeadOffset > nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX)
						{
							Vis.Heads.nHeadOffset = nDEV_INDEX_MAX - nVIS_HEAD_MAIN_SHOW_INDEX_MAX;
						}
					}
				}
				// Toolbar
				HandleToolbar();
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_DLG_MOVE_AXES:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 0;
				Vis.PageDialogMoveAxes.bAllAxesHomingOk = 1;
				for(nAxisIndex=0; nAxisIndex<=nDEV_INDEX_MAX; nAxisIndex++)
				{
					if(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nAxisIndex].bHorizontal == 1)
					{
						if(gAxisControl.State.AxisStatus[nAxisIndex].Horizontal.DriveStatus.HomingOk == 0)
						{
							Vis.PageDialogMoveAxes.bAllAxesHomingOk = 0;
							break;
						}
					}
					if(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nAxisIndex].bHorizontal == 1)
					{
						if(gAxisControl.State.AxisStatus[nAxisIndex].Vertical.DriveStatus.HomingOk == 0)
						{
							Vis.PageDialogMoveAxes.bAllAxesHomingOk = 0;
							break;
						}
					}
				}
				BrbVc4SetControlVisibility(&Vis.PageDialogMoveAxes.txtHoming.nStatus, Vis.PageDialogMoveAxes.bAllAxesHomingOk == 0);
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				BrbVc4SetControlEnability(&Vis.PageDialogMoveAxes.btnNo.nStatus, Vis.PageDialogMoveAxes.btnNo.bClicked == 0 && Vis.PageDialogMoveAxes.btnYes.bClicked == 0);
				if(BrbVc4HandleButton(&Vis.PageDialogMoveAxes.btnNo) == 1)
				{
					Vis.PageDialogMoveAxes.btnNo.bClicked = 0;
					BrbVc4ChangePageBack(&Vis.PageHandling);
				}
				BrbVc4SetControlEnability(&Vis.PageDialogMoveAxes.btnYes.nStatus, Vis.PageDialogMoveAxes.btnNo.bClicked == 0 && Vis.PageDialogMoveAxes.btnYes.bClicked == 0);
				if(BrbVc4HandleButton(&Vis.PageDialogMoveAxes.btnYes) == 1)
				{
					if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
					{
						gAxisControl.CallerBox.bMoveAllAxesToWorkPosition = 1;
						Vis.PageDialogMoveAxes.btnYes.bClicked = 0;
						BrbVc4ChangePageBack(&Vis.PageHandling);
					}		
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				gVisu.State.bDialogMoveAxesActive = 0;
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_DLG_START_HEATING:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 0;
				for(nTankIndex=0; nTankIndex<=nIDX_TEMP_CTRL_TANK_MAX; nTankIndex++)
				{
					Vis.PageDialogStartHeating.chkTank[nTankIndex].bChecked = 0;
				}
				for(nHeadIndex=0; nHeadIndex<=nDEV_INDEX_MAX; nHeadIndex++)
				{
					if(gPar.ProductPar.BonDevPar[nHeadIndex].bBonDevEn == 1)
					{
						if(gPar.MachinePar.TempCtrlConfig.BonDev[nHeadIndex].bExist == 1)
						{
							nTankIndex = gPar.MachinePar.TempCtrlConfig.BonDev[nHeadIndex].nTankIdx;
							if(gPar.MachinePar.TempCtrlConfig.Tank[nTankIndex].bExist == 1)
							{
								Vis.PageDialogStartHeating.chkTank[nTankIndex].bChecked = 1;
							}
						}
					}
				}

			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				for(nTankIndex=0; nTankIndex<=nIDX_TEMP_CTRL_TANK_MAX; nTankIndex++)
				{
					BrbVc4SetControlVisibility(&Vis.PageDialogStartHeating.chkTank[nTankIndex].nStatus, gPar.MachinePar.TempCtrlConfig.Tank[nTankIndex].bExist == 1);
					BrbVc4HandleCheckbox(&Vis.PageDialogStartHeating.chkTank[nTankIndex]);
				}
				BrbVc4SetControlEnability(&Vis.PageDialogStartHeating.btnOk.nStatus, Vis.PageDialogStartHeating.nStep == 0);
				if(BrbVc4HandleButton(&Vis.PageDialogStartHeating.btnOk) == 1)
				{
					Vis.PageDialogStartHeating.btnOk.bClicked = 0;
					Vis.PageDialogStartHeating.nStep = 1;
				}
				switch(Vis.PageDialogStartHeating.nStep)
				{
					case 0:	// Idle
						Vis.PageDialogStartHeating.nTankIndexStart = 0;
						break;
					
					case 1:	// Start Heating
						if(Vis.PageDialogStartHeating.chkTank[Vis.PageDialogStartHeating.nTankIndexStart].bChecked == 1)
						{
							if(BrbSetCaller(&gTempCtrl.CallerBox.HeatingSystem[Vis.PageDialogStartHeating.nTankIndexStart].Caller, eCALLERID_VISU) == eBRB_CALLER_STATE_OK)
							{
								gTempCtrl.CallerBox.HeatingSystem[Vis.PageDialogStartHeating.nTankIndexStart].bStartHeating = 1;
								Vis.PageDialogStartHeating.nStep = 2;
							}		
						}
						else
						{
							Vis.PageDialogStartHeating.nStep = 2;
						}
						break;
					
					case 2:	// Next Heating
						Vis.PageDialogStartHeating.nTankIndexStart += 1;
						if(Vis.PageDialogStartHeating.nTankIndexStart <= nIDX_TEMP_CTRL_TANK_MAX)
						{
							Vis.PageDialogStartHeating.nStep = 1;
						}
						else
						{
							BrbVc4ChangePage(&Vis.PageHandling, ePAGE_MAIN, 0);
							Vis.PageDialogStartHeating.nStep = 0;
						}
						break;					
				}
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				gVisu.State.bDialogStartHeatingActive = 0;
				Vis.PageHandling.bPageExit = 0;
			}
			break;

	
		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_ANALOGFUELLSTAND:
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
			}
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				
				// Toolbar
				HandleToolbar();
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;

		//	--------------------------------------------------------------------------------------------------------------------------------------------------
		case ePAGE_MEDIENVERBRAUCH:
		
			if(Vis.PageHandling.bPageInit == 1 && Vis.PageHandling.bPageChangeInProgress == 0)
			{
				Vis.PageHandling.bPageInit = 0;
				Vis.Footer.bShowStopStart = 1;
			}
		
			if(Vis.PageHandling.bPageChangeInProgress == 0)
			{
				if(Vis.PageMedienVerbrauch.btnSave.bClicked==1)
				{
					Vis.PageMedienVerbrauch.btnSave.bClicked=0; 	
					BrbVc4ChangePage(&Vis.PageHandling, ePAGE_CONFIGURATION15, 0);
				}
			
			  
			}
			if(Vis.PageHandling.bPageExit == 1)
			{
				Vis.PageHandling.bPageExit = 0;
			}
			break;
				
		//	--------------------------------------------------------------------------------------------------------------------------------------------------	
		
	}
	BrbVc4HandleChangePage(&Vis.PageHandling);
	BrbVc4HandleScreenSaver(&Vis.ScreenSaver, &Vis.General, &Vis.PageHandling);
	gParPermanent.nVisLanguage = Vis.General.nLanguageCurrent;

	
	

	// Direct-Kommandos
	if(gVisu.DirectBox.bShowDialogMoveAxes == 1)
	{
		gVisu.State.bDialogMoveAxesActive = 1;
		BrbVc4ChangePage(&Vis.PageHandling, ePAGE_DLG_MOVE_AXES, 1);
		BrbClearDirectBox((UDINT)&gVisu.DirectBox, sizeof(gVisu.DirectBox));
	}
	if(gVisu.DirectBox.bShowDialogStartHeating == 1)
	{
		gVisu.State.bDialogStartHeatingActive = 1;
		BrbVc4ChangePage(&Vis.PageHandling, ePAGE_DLG_START_HEATING, 1);
		BrbClearDirectBox((UDINT)&gVisu.DirectBox, sizeof(gVisu.DirectBox));
	}
	if(Step.bInitDone == 1)
	{
	}
	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			if(1 == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gVisu.CallerBox, sizeof(gVisu.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			if(gVisu.CallerBox.bDummy == 1)
			{
				Step.eStepNr = eSTEP_CMD1;
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_CMD1:
			strcpy(Step.sStepText, "eSTEP_CMD1");
			Step.eStepNr = eSTEP_CMD1_FINISHED;
			break;
		case eSTEP_CMD1_FINISHED:
			strcpy(Step.sStepText, "eSTEP_CMD1_FINISHED");
			BrbClearCallerBox((UDINT)&gVisu.CallerBox, sizeof(gVisu.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
	}

	//fbOperatingSeconds.IN = 1;
	//fbOperatingSeconds.PT = 7200;
	//TON(&fbOperatingSeconds);	
	

}

	void _EXIT VisuEXIT(void)
	{
	}

