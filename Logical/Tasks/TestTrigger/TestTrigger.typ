(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: TestTrigger
 * File: TestTrigger.typ
 * Author: kusculart
 * Created: May 25, 2015
 ********************************************************************
 * Local data types of program TestTrigger
 ********************************************************************)

TYPE
	TaskVar_TYP : 	STRUCT 
		nTriggerProduct : DINT;
		nTriggerFalse : DINT;
		nCycleCnt : UINT;
		nPositionPrintMarkTriggerOn : DINT;
		nPositionPrintMarkTriggerOff : DINT;
		nPositionPrintMarkTriggerOn_old : DINT;
		nDistanceToPrintMarkTriggerOn : DINT;
		bPrintMarkTrigger : BOOL;
		nPrintMarkCount : USINT;
		nTimeToNegEdgePrintMarkTrigger : DINT;
		nTimePrintMarkTriggerSignal : DINT;
		nTimeToPosEdgePrintMarkTrigger : DINT;
		NegEdge : F_TRIG;
		PosEdge : R_TRIG;
		nAdaptedDistancePmToTrigger : DINT;
		nPositionToSetTrigger : DINT;
		nDistanceToSetTrigger : DINT;
		rEncoderVelocityFactor : REAL;
		nDistanceWithPrintMarkVelocity : DINT;
		nDistanceVelocityCompensation : DINT;
		nDistanceProductToProDetSensor : DINT;
		nPositionToSetProDetTrigger : DINT;
		nTriggerEncoderIdx : SINT;
		nPrintMarkEncoderIdx : SINT;
	END_STRUCT;
	IO_TYP : 	STRUCT 
		Input : IO_Input_TYP;
		Output : IO_Output_TYP;
	END_STRUCT;
	IO_Input_TYP : 	STRUCT 
		Dummy : USINT;
	END_STRUCT;
	IO_Output_TYP : 	STRUCT 
		CopyToARsim_IO_PrintMarkSensor : ARRAY[0..nDEV_INDEX_MAX]OF IoPrintMarkSen_TYP;
		EdgeGeneration : IO_OutputEdgeGeneration_TYP;
		bTrigger : ARRAY[0..nMAX_IDX_TEST_TRIGGER]OF BOOL;
		bProductDetectionTrigger : ARRAY[0..nMAX_IDX_TEST_TRIGGER]OF BOOL;
	END_STRUCT;
	IoPrintMarkSen_TYP : 	STRUCT 
		nRisingCnt : SINT;
		nTsRising : INT;
		nFallingCnt : SINT;
		nTsFalling : INT;
	END_STRUCT;
	IO_OutputEdgeGeneration_TYP : 	STRUCT 
		PrintMark : ARRAY[0..nMAX_IDX_TEST_TRIGGER]OF EdgeGenerationModule_TYP;
		Trigger : ARRAY[0..nMAX_IDX_TEST_TRIGGER]OF EdgeGenerationModule_TYP;
	END_STRUCT;
	EdgeGenerationModule_TYP : 	STRUCT 
		RisingEdge : Edge_TYP;
		FallingEdge : Edge_TYP;
	END_STRUCT;
	Edge_TYP : 	STRUCT 
		bEnable : BOOL;
		nSequence : SINT;
		nTimeStamp : ARRAY[0..3]OF DINT;
		bQuitError : BOOL;
		bQuitWarning : BOOL;
	END_STRUCT;
END_TYPE

(*Kommando-Struktur*)

TYPE
	TestTrigger_TYP : 	STRUCT 
		Cmd : TestTriggerCmd_TYP;
		Par : TestTriggerPar_TYP;
		State : TestTriggerState_TYP;
	END_STRUCT;
	TestTriggerCmd_TYP : 	STRUCT 
		bSetTriggerOnce : BOOL;
		bSetTriggerCyclicOn : BOOL; (*Trigger nach jeden n-ten Zyklus setzen. Nach wie vielen Zyklen wird im Parameter "nSetTriggerCyclicCnt"*)
		bSetTriggerCyclicOff : BOOL;
		bTriggerContinousOn : BOOL;
		bTriggerContinousOff : BOOL;
		bSetPrintMarkOnce : BOOL;
		bPrintMarkContinousOn : BOOL;
		bPrintMarkContinousOff : BOOL;
		bSetPrintMark_TriggerOnce : BOOL;
		bPrintMark_TriggerContinousOn : BOOL;
		bPrintMark_TriggerContinousOff : BOOL;
		bEnableProductDetectionSensor : BOOL;
	END_STRUCT;
	TestTriggerPar_TYP : 	STRUCT 
		nBondingDeviceIdx : USINT;
		nSetTriggerCyclicCnt : UINT := 100;
		rPrintMarkGapBetweenProducts : REAL := 10.0; (*in[mm]*)
		rTriggerGapBetweenProducts : REAL := 10.0; (*in [mm]*)
		nPrintMarkLength : ARRAY[0..nIDX_SIMULATED_PRINT_MARK_MAX]OF DINT;
		rTriggerCorrection : REAL;
		rProDetTriggerCorrection : REAL;
	END_STRUCT;
	TestTriggerState_TYP : 	STRUCT 
		bContinousTriggerIsActive : BOOL;
		bCyclicTriggerIsActive : BOOL;
		bOncePrintMarkIsSet : BOOL;
		bContinousPrintMarkIsActive : BOOL;
		bOncePrintMark_TriggerIsSet : BOOL;
		bContinousPM_TriggerIsActive : BOOL;
		nActPrintMarkLength : DINT;
	END_STRUCT;
END_TYPE
