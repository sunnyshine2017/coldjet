/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: TestTrigger
 * File: TestTriggerCyclic.c
 * Author: kusculart
 * Created: May 25, 2015
 ********************************************************************
 * Implementation of program TestTrigger
 ********************************************************************/
	
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <Global.h>

void _INIT TestTriggerInit(void)
{
	
		
}
	
void _CYCLIC TestTriggerCyclic(void)
{
	for( nTestTriggerIdx=0; nTestTriggerIdx <= nMAX_IDX_TEST_TRIGGER; nTestTriggerIdx++ )
	{
		
		// Parameter aus gPar für den jeweiligen ausgewählten Klebekopf holen
		
		
		
		if( gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR )
		{
			TaskVar[nTestTriggerIdx].nTriggerEncoderIdx = gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder;
			TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx = -1;
		}
		else if( gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR )
		{
			TaskVar[nTestTriggerIdx].nTriggerEncoderIdx = -1;
			TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx = gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder;
		}
		else if( gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
		{
			TaskVar[nTestTriggerIdx].nTriggerEncoderIdx = gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder;
			TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx = gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxPrintMarkEncoder;
		}
		
		// -----------------------
		// Trigger einmalig setzen
		// -----------------------
		if( (TestTrigger[nTestTriggerIdx].Cmd.bSetTriggerOnce == TRUE) &&
			(IO.Output.bTrigger[nTestTriggerIdx] == TRUE) )
		{
			TestTrigger[nTestTriggerIdx].Cmd.bSetTriggerOnce = FALSE;
			IO.Output.bTrigger[nTestTriggerIdx] = FALSE;
		}
		if( TestTrigger[nTestTriggerIdx].Cmd.bSetTriggerOnce == TRUE )
		{
			IO.Output.bTrigger[nTestTriggerIdx] = TRUE;
		}
		
		// -------------------------------------------------------------------------------
		// Trigger durchgängig in Abhängigkeit der Geschwindigkeit und Produktlänge setzen
		// -------------------------------------------------------------------------------
		if( TestTrigger[nTestTriggerIdx].Cmd.bTriggerContinousOn == TRUE )
		{
			if( (gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR) ||
				(gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) )
			{
				TaskVar[nTestTriggerIdx].nTriggerProduct = gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nTriggerEncoderIdx] + (DINT)((gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rProductLen + TestTrigger[nTestTriggerIdx].Par.rTriggerGapBetweenProducts) * rFACTOR_MM_TO_AXIS_UNITS);
				TestTrigger[nTestTriggerIdx].State.bContinousTriggerIsActive = TRUE;
				TestTrigger[nTestTriggerIdx].Cmd.bTriggerContinousOn = FALSE;
			}
			else
			{
				TestTrigger[nTestTriggerIdx].Cmd.bTriggerContinousOn = FALSE;
			}
		}
		
		if( TestTrigger[nTestTriggerIdx].Cmd.bTriggerContinousOff == TRUE )
		{
			TestTrigger[nTestTriggerIdx].State.bContinousTriggerIsActive = FALSE;
			IO.Output.bTrigger[nTestTriggerIdx] = FALSE;
			TestTrigger[nTestTriggerIdx].Cmd.bTriggerContinousOff = FALSE;
		}
		
		if( TestTrigger[nTestTriggerIdx].State.bContinousTriggerIsActive == TRUE )
		{		
			if( IO.Output.bTrigger[nTestTriggerIdx] == TRUE )
			{
				IO.Output.bTrigger[nTestTriggerIdx] = FALSE;
			}
		
			if( gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nTriggerEncoderIdx] - TaskVar[nTestTriggerIdx].nTriggerProduct >= 0 )
			{
				IO.Output.bTrigger[nTestTriggerIdx] = TRUE;
				TaskVar[nTestTriggerIdx].nTriggerProduct = gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nTriggerEncoderIdx] + (DINT)((gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rProductLen + TestTrigger[nTestTriggerIdx].Par.rTriggerGapBetweenProducts) * rFACTOR_MM_TO_AXIS_UNITS);
			}
		}
		
		// -------------------------------------
		// Trigger jeden n-ten Taskzyklus setzen
		// -------------------------------------
		if( TestTrigger[nTestTriggerIdx].Cmd.bSetTriggerCyclicOn == TRUE )
		{
			
			TestTrigger[nTestTriggerIdx].Cmd.bSetTriggerCyclicOn = FALSE;
			TestTrigger[nTestTriggerIdx].State.bCyclicTriggerIsActive = TRUE;
		}
		
		if( TestTrigger[nTestTriggerIdx].Cmd.bSetTriggerCyclicOff == TRUE )
		{
			TestTrigger[nTestTriggerIdx].Cmd.bSetTriggerCyclicOff = FALSE;
			TestTrigger[nTestTriggerIdx].State.bCyclicTriggerIsActive = FALSE;
		}
		
		if( TestTrigger[nTestTriggerIdx].State.bCyclicTriggerIsActive == TRUE )
		{
			if( TaskVar[nTestTriggerIdx].nCycleCnt >= TestTrigger[nTestTriggerIdx].Par.nSetTriggerCyclicCnt )
			{
				IO.Output.bTrigger[nTestTriggerIdx] = TRUE;
				TaskVar[nTestTriggerIdx].nCycleCnt = 0;
			}
			else
			{
				IO.Output.bTrigger[nTestTriggerIdx] = FALSE;
			}
			
			#ifdef HW_ARSIM
				for( i=0; i<=gPar.MachinePar.nTriggerSenIndex; i++ )
				{	
					if( bArSimTriggerSensorIdxEnabled[i] == TRUE )
					{
						gTriggerSensor.Par.SensorTS[i].nRisingCnt += (SINT)IO.Output.bTrigger[nTestTriggerIdx];
					}
				}
			#endif
			
			TaskVar[nTestTriggerIdx].nCycleCnt++;
		}
		else
		{
			TaskVar[nTestTriggerIdx].nCycleCnt = 0;
		}
		
		// -------------------------
		// PrintMark einmalig setzen
		// -------------------------
		if( TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMarkOnce == TRUE )
		{
			if( (gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR) ||
				(gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) )
			{
				TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn = gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx] + (DINT)((gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rProductLen + TestTrigger[nTestTriggerIdx].Par.rPrintMarkGapBetweenProducts) * rFACTOR_MM_TO_AXIS_UNITS);
				TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOff = TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn + TestTrigger[nTestTriggerIdx].Par.nPrintMarkLength[TaskVar[nTestTriggerIdx].nPrintMarkCount];
				TestTrigger[nTestTriggerIdx].State.bOncePrintMarkIsSet = TRUE;
				TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMarkOnce = FALSE;
			}
			else
			{
				TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMarkOnce = FALSE;
			}
		}
		
		if( TestTrigger[nTestTriggerIdx].State.bOncePrintMarkIsSet == TRUE )
		{				
			if( gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx] - TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn >= 0 )
			{
				TaskVar[nTestTriggerIdx].bPrintMarkTrigger = TRUE;
			}
			
			if( gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx] - TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOff >= 0 )
			{
				TaskVar[nTestTriggerIdx].bPrintMarkTrigger = FALSE;
				TestTrigger[nTestTriggerIdx].State.bOncePrintMarkIsSet = FALSE;
			}
		}
		
		// ------------------------------------------------------------------------------------
		// PrintMark kontinuierlich in Abhängigkeit der Geschwindigkeit und Produktlänge setzen
		// ------------------------------------------------------------------------------------
		if( TestTrigger[nTestTriggerIdx].Cmd.bPrintMarkContinousOn == TRUE )
		{
			if( (gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR) ||
				(gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) )
			{
				TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn = gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx] + (DINT)((gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rProductLen + TestTrigger[nTestTriggerIdx].Par.rPrintMarkGapBetweenProducts) * rFACTOR_MM_TO_AXIS_UNITS);
				TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOff = TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn + TestTrigger[nTestTriggerIdx].Par.nPrintMarkLength[TaskVar[nTestTriggerIdx].nPrintMarkCount];
				TestTrigger[nTestTriggerIdx].State.bContinousPrintMarkIsActive = TRUE;
				TestTrigger[nTestTriggerIdx].Cmd.bPrintMarkContinousOn = FALSE;
			}
			else
			{
				TestTrigger[nTestTriggerIdx].Cmd.bPrintMarkContinousOn = FALSE;
			}
		}
		
		if( TestTrigger[nTestTriggerIdx].Cmd.bPrintMarkContinousOff == TRUE )
		{
			TestTrigger[nTestTriggerIdx].State.bContinousPrintMarkIsActive = FALSE;
			TestTrigger[nTestTriggerIdx].Cmd.bPrintMarkContinousOff = FALSE;
		}
		
		if( TestTrigger[nTestTriggerIdx].State.bContinousPrintMarkIsActive == TRUE )
		{
			if( memcmp( &gPar.MachinePar.PrintMarkPar.nPrintMarkLen, &TestTrigger[nTestTriggerIdx].Par.nPrintMarkLength, sizeof( gPar.MachinePar.PrintMarkPar.nPrintMarkLen ) ) != 0 )
			{
				memcpy( &TestTrigger[nTestTriggerIdx].Par.nPrintMarkLength, &gPar.MachinePar.PrintMarkPar.nPrintMarkLen, sizeof( gPar.MachinePar.PrintMarkPar.nPrintMarkLen ) );
			}
			
			if( TestTrigger[nTestTriggerIdx].Par.nPrintMarkLength[TaskVar[nTestTriggerIdx].nPrintMarkCount] > 0 )
			{
				if( gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx] - TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn >= 0 )
				{
					TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn = gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx] + (DINT)((gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rProductLen + TestTrigger[nTestTriggerIdx].Par.rPrintMarkGapBetweenProducts) * rFACTOR_MM_TO_AXIS_UNITS);
					TaskVar[nTestTriggerIdx].bPrintMarkTrigger = TRUE;
				}
				if( gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx] - TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOff >= 0 )
				{
					TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOff = TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn + TestTrigger[nTestTriggerIdx].Par.nPrintMarkLength[TaskVar[nTestTriggerIdx].nPrintMarkCount];
					TaskVar[nTestTriggerIdx].bPrintMarkTrigger = FALSE;
					TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMarkOnce = FALSE;
					
					TaskVar[nTestTriggerIdx].nPrintMarkCount++;
					if( TaskVar[nTestTriggerIdx].nPrintMarkCount > nIDX_SIMULATED_PRINT_MARK_MAX )
					{
						TaskVar[nTestTriggerIdx].nPrintMarkCount = 0;
					}
				}
			}
			else
			{
				TaskVar[nTestTriggerIdx].nPrintMarkCount++;
				if( TaskVar[nTestTriggerIdx].nPrintMarkCount > nIDX_SIMULATED_PRINT_MARK_MAX )
				{
					TaskVar[nTestTriggerIdx].nPrintMarkCount = 0;
				}
			}
		}
		
		
		
		if( (TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMark_TriggerOnce == TRUE) ||
			(TestTrigger[nTestTriggerIdx].State.bOncePrintMark_TriggerIsSet == TRUE) )
		{
			if( gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
			{
				TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMark_TriggerOnce = FALSE;
				
				if( (TestTrigger[nTestTriggerIdx].State.bContinousTriggerIsActive == TRUE) ||
					(TestTrigger[nTestTriggerIdx].State.bCyclicTriggerIsActive == TRUE) ||
					(TestTrigger[nTestTriggerIdx].State.bContinousPrintMarkIsActive == TRUE) )
				{
					TestTrigger[nTestTriggerIdx].Cmd.bTriggerContinousOff = TRUE;
					TestTrigger[nTestTriggerIdx].Cmd.bSetTriggerCyclicOff = TRUE;
					TestTrigger[nTestTriggerIdx].Cmd.bPrintMarkContinousOff = TRUE;
				}
				else
				{
					if( TestTrigger[nTestTriggerIdx].State.bOncePrintMark_TriggerIsSet == FALSE )
					{
						TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMarkOnce = TRUE;
						TestTrigger[nTestTriggerIdx].State.bOncePrintMark_TriggerIsSet = TRUE;
					}
					
					if( (TestTrigger[nTestTriggerIdx].State.bOncePrintMarkIsSet == TRUE) &&
						(TaskVar[nTestTriggerIdx].bPrintMarkTrigger == TRUE) )
					{
						
						if( gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxPrintMarkEncoder != gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder )
						{
							TaskVar[nTestTriggerIdx].nDistanceWithPrintMarkVelocity = (gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rDistancePrintMark_VelocityJump + gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rProductLen) * rFACTOR_MM_TO_AXIS_UNITS;
							TaskVar[nTestTriggerIdx].rEncoderVelocityFactor = gEncoder.State.rVelocity[gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder] / gEncoder.State.rVelocity[gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxPrintMarkEncoder];
							TaskVar[nTestTriggerIdx].nDistanceVelocityCompensation = TaskVar[nTestTriggerIdx].nDistanceWithPrintMarkVelocity * TaskVar[nTestTriggerIdx].rEncoderVelocityFactor;
							TaskVar[nTestTriggerIdx].nAdaptedDistancePmToTrigger = TaskVar[nTestTriggerIdx].nDistanceVelocityCompensation + ((gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rDistancePrintMarkTriggerSensor * rFACTOR_MM_TO_AXIS_UNITS) - TaskVar[nTestTriggerIdx].nDistanceWithPrintMarkVelocity);
							TaskVar[nTestTriggerIdx].nAdaptedDistancePmToTrigger += TestTrigger[nTestTriggerIdx].Par.rTriggerCorrection * rFACTOR_MM_TO_AXIS_UNITS;
						}
						TaskVar[nTestTriggerIdx].nPositionToSetTrigger = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder] + TaskVar[nTestTriggerIdx].nAdaptedDistancePmToTrigger;
					}
					
					if( (TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMarkOnce == FALSE) &&
						(TestTrigger[nTestTriggerIdx].State.bOncePrintMarkIsSet == FALSE) )
					{
						TaskVar[nTestTriggerIdx].nDistanceToSetTrigger = TaskVar[nTestTriggerIdx].nPositionToSetTrigger - gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder];
					}
					
					if( TaskVar[nTestTriggerIdx].nDistanceToSetTrigger < 0 )
					{
						TestTrigger[nTestTriggerIdx].Cmd.bSetTriggerOnce = TRUE;
	//					TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMark_TriggerOnce = FALSE;
						TaskVar[nTestTriggerIdx].nDistanceToSetTrigger = 0;
						TestTrigger[nTestTriggerIdx].State.bOncePrintMark_TriggerIsSet = FALSE;
					}
				}
			}
			else
			{
				TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMark_TriggerOnce = FALSE;
			}
		}
		
		if( (TestTrigger[nTestTriggerIdx].Cmd.bPrintMark_TriggerContinousOn == TRUE) ||
			(TestTrigger[nTestTriggerIdx].State.bContinousPM_TriggerIsActive == TRUE) )
		{
			if( gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
			{
				if( TestTrigger[nTestTriggerIdx].State.bOncePrintMark_TriggerIsSet == FALSE )
				{
					TestTrigger[nTestTriggerIdx].Cmd.bPrintMark_TriggerContinousOn = FALSE;
					TestTrigger[nTestTriggerIdx].Cmd.bSetPrintMark_TriggerOnce = TRUE;
					TestTrigger[nTestTriggerIdx].State.bContinousPM_TriggerIsActive = TRUE;
				}
			}
			else
			{
				TestTrigger[nTestTriggerIdx].Cmd.bPrintMark_TriggerContinousOn = FALSE;
			}
		}
		
		if( TestTrigger[nTestTriggerIdx].Cmd.bPrintMark_TriggerContinousOff == TRUE )
		{
			TestTrigger[nTestTriggerIdx].Cmd.bPrintMark_TriggerContinousOff = FALSE;
			TestTrigger[nTestTriggerIdx].State.bContinousPM_TriggerIsActive = FALSE;
		}
		
		if( TestTrigger[nTestTriggerIdx].Cmd.bEnableProductDetectionSensor == TRUE )
		{
			switch( gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].eIdxSenType )
			{
				case eSEN_TYPE_TRIGGER_SENSOR:
					
					if( IO.Output.bTrigger[nTestTriggerIdx] == TRUE )
					{
						TaskVar[nTestTriggerIdx].nPositionToSetProDetTrigger = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder] + ((gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS) - gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].ProductDetection.nOffset);
						TaskVar[nTestTriggerIdx].nPositionToSetProDetTrigger += (DINT)(TestTrigger[nTestTriggerIdx].Par.rProDetTriggerCorrection * rFACTOR_MM_TO_AXIS_UNITS);
					}
					
					TaskVar[nTestTriggerIdx].nDistanceProductToProDetSensor = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder] - TaskVar[nTestTriggerIdx].nPositionToSetProDetTrigger;
					
					if( TaskVar[nTestTriggerIdx].nDistanceProductToProDetSensor >= 0 )
					{
						if( TaskVar[nTestTriggerIdx].nDistanceProductToProDetSensor <= 3000 )
						{
							IO.Output.bProductDetectionTrigger[nTestTriggerIdx] = TRUE;
						}
						else
						{
							IO.Output.bProductDetectionTrigger[nTestTriggerIdx] = FALSE;
						}
					}
				
					break;
					
				case eSEN_TYPE_PRINT_MARK_SENSOR:
					
					if( TaskVar[nTestTriggerIdx].bPrintMarkTrigger == TRUE )
					{
						TaskVar[nTestTriggerIdx].nPositionToSetProDetTrigger = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder] + ((gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS) - gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].ProductDetection.nOffset);
						TaskVar[nTestTriggerIdx].nPositionToSetProDetTrigger += (DINT)(TestTrigger[nTestTriggerIdx].Par.rProDetTriggerCorrection * rFACTOR_MM_TO_AXIS_UNITS);
					}
					
					TaskVar[nTestTriggerIdx].nDistanceProductToProDetSensor = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder] - TaskVar[nTestTriggerIdx].nPositionToSetProDetTrigger;
					
					if( TaskVar[nTestTriggerIdx].nDistanceProductToProDetSensor >= 0 )
					{
						if( TaskVar[nTestTriggerIdx].nDistanceProductToProDetSensor <= 3000 )
						{
							IO.Output.bProductDetectionTrigger[nTestTriggerIdx] = TRUE;
						}
						else
						{
							IO.Output.bProductDetectionTrigger[nTestTriggerIdx] = FALSE;
						}
					}
					break;
					
				case eSEN_TYPE_PRINT_MARK_TRIG_SENSOR:
					
					if( IO.Output.bTrigger[nTestTriggerIdx] == TRUE )
					{	
						TaskVar[nTestTriggerIdx].nPositionToSetProDetTrigger = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder] + ((gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS) - gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].ProductDetection.nOffset);
						TaskVar[nTestTriggerIdx].nPositionToSetProDetTrigger += (DINT)(TestTrigger[nTestTriggerIdx].Par.rProDetTriggerCorrection * rFACTOR_MM_TO_AXIS_UNITS);
					}
					
					TaskVar[nTestTriggerIdx].nDistanceProductToProDetSensor = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[TestTrigger[nTestTriggerIdx].Par.nBondingDeviceIdx].nIdxEncoder] - TaskVar[nTestTriggerIdx].nPositionToSetProDetTrigger;
					
					if( TaskVar[nTestTriggerIdx].nDistanceProductToProDetSensor >= 0 )
					{
						if( TaskVar[nTestTriggerIdx].nDistanceProductToProDetSensor <= 3000 )
						{
							IO.Output.bProductDetectionTrigger[nTestTriggerIdx] = TRUE;
						}
						else
						{
							IO.Output.bProductDetectionTrigger[nTestTriggerIdx] = FALSE;
						}
					}
					break;
			
			}
		}
		
		
		#ifndef HW_ARSIM
		{	
			if( TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn_old != TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn )
			{
				
				TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn_old = TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn;
				
				TaskVar[nTestTriggerIdx].nDistanceToPrintMarkTriggerOn = TaskVar[nTestTriggerIdx].nPositionPrintMarkTriggerOn - gEncoder.State.nAxisPos[TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx];
				
				
				TaskVar[nTestTriggerIdx].nTimeToPosEdgePrintMarkTrigger = (REAL)( TaskVar[nTestTriggerIdx].nDistanceToPrintMarkTriggerOn / (gEncoder.State.rVelocity[TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx] / 1000 / 1000));
				TaskVar[nTestTriggerIdx].nTimeToPosEdgePrintMarkTrigger += 5000;
				
				TaskVar[nTestTriggerIdx].nTimePrintMarkTriggerSignal = (REAL)(TestTrigger[nTestTriggerIdx].Par.nPrintMarkLength[TaskVar[nTestTriggerIdx].nPrintMarkCount] / (gEncoder.State.rVelocity[TaskVar[nTestTriggerIdx].nPrintMarkEncoderIdx] / 1000 / 1000));
				
				TaskVar[nTestTriggerIdx].nTimeToNegEdgePrintMarkTrigger = TaskVar[nTestTriggerIdx].nTimeToPosEdgePrintMarkTrigger + TaskVar[nTestTriggerIdx].nTimePrintMarkTriggerSignal;
				
				IO.Output.EdgeGeneration.PrintMark[nTestTriggerIdx].RisingEdge.bEnable = TRUE;
				IO.Output.EdgeGeneration.PrintMark[nTestTriggerIdx].RisingEdge.nSequence += 1;
				IO.Output.EdgeGeneration.PrintMark[nTestTriggerIdx].RisingEdge.nTimeStamp[0] = gSystem.State.nSystemTime + TaskVar[nTestTriggerIdx].nTimeToPosEdgePrintMarkTrigger;
				
				IO.Output.EdgeGeneration.PrintMark[nTestTriggerIdx].FallingEdge.bEnable = TRUE;
				IO.Output.EdgeGeneration.PrintMark[nTestTriggerIdx].FallingEdge.nSequence += 1;
				IO.Output.EdgeGeneration.PrintMark[nTestTriggerIdx].FallingEdge.nTimeStamp[0] = IO.Output.EdgeGeneration.PrintMark[nTestTriggerIdx].RisingEdge.nTimeStamp[0] + TaskVar[nTestTriggerIdx].nTimePrintMarkTriggerSignal;
			}
		}
		#endif
		
		#ifdef HW_ARSIM
			TaskVar[nTestTriggerIdx].PosEdge.CLK = TaskVar[nTestTriggerIdx].bPrintMarkTrigger;
			R_TRIG( &TaskVar[nTestTriggerIdx].PosEdge );
			TaskVar[nTestTriggerIdx].NegEdge.CLK = TaskVar[nTestTriggerIdx].bPrintMarkTrigger;
			F_TRIG( &TaskVar[nTestTriggerIdx].NegEdge );
			
			for( i=0; i<=gPar.MachinePar.nPrintMarkSensorIndex; i++ )
			{	
				if( bArSimTriggerSensorIdxEnabled[i] == TRUE )
				{					
					CopyTo_IO_PrintMarkSensor[i].nRisingCnt += (SINT)TaskVar[nTestTriggerIdx].PosEdge.Q;
	
					CopyTo_IO_PrintMarkSensor[i].nFallingCnt += (SINT)TaskVar[nTestTriggerIdx].NegEdge.Q;
				}
			}
		#endif
	}
	
}
