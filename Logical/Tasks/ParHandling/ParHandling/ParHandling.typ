(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: ParHandling
 * File: ParHandling.typ
 * Author: kusculart
 * Created: Oktober 06, 2014
 ********************************************************************
 * Local data types of program ParHandling
 ********************************************************************)

TYPE
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_CHECK_PAR_DIR := 101, (*Parameterverzeichnis �berpr�fen*)
		eSTEP_CREATE_PAR_DIR := 102, (*Parameterverzeichnis erstellen*)
		eSTEP_CREATE_MPAR_DIR := 103, (*Maschinenverzeichnis erstellen*)
		eSTEP_CREATE_PRODUCT_PAR_DIR := 104, (*Produktparameterverzeichnis erstellen*)
		eSTEP_CHECK_PRODUCT_PAR := 110, (*Produktparameter �berpr�fen*)
		eSTEP_GET_PRODUCT_PAR := 111, (*Produktparameternamen ermitteln*)
		eSTEP_LOAD_PRODUCT_PAR := 200, (*Produktparameter laden*)
		eSTEP_SAVE_PRODUCT_PAR := 201, (*Produktparameter speichern*)
		eSTEP_LOAD_M_PAR := 202, (*Maschinenparameter laden*)
		eSTEP_SAVE_M_PAR := 203, (*Maschinenparameter speichern*)
		eSTEP_DELETE_PRODUCT_PAR := 204, (*Produktparameter l�schen*)
		eSTEP_CHECK_AXES := 300, (*Achsen pr�fen*)
		eSTEP_INIT_ALL_AX_LIMITS, (*Alle Achs-Limits initialisieren*)
		eSTEP_CHECK_AXES_WAIT,
		eSTEP_CHECK_HEATING := 310, (*Heizung pr�fen*)
		eSTEP_CHECK_HEATING_WAIT
		);
	ParHandlingStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	DirCheck_TYP : 	STRUCT 
		fbDirInfo : DirInfo; (*DirInfo*)
		fbDirCreate : DirCreate; (*DirCreate*)
		fbDirRead : DirRead; (*DirRead*)
		ParDirData : fiDIR_READ_DATA; (*Daten aus Produktparameterverzeichnis*)
	END_STRUCT;
END_TYPE
