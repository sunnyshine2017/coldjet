/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: ParHandling
 * File: ParHandling.c
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Implementation of program ParHandling
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <ParHandlingFunc.h>

void _INIT ParHandlingINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gParHandling, 0, sizeof(gParHandling));
	gParHandling.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
//	brsmemset((UDINT)&gPar, 0, sizeof(gPar));
	brsmemset((UDINT)&fbLoadVarAscii, 0, sizeof(fbLoadVarAscii));
	brsmemset((UDINT)&fbSaveVarAscii, 0, sizeof(fbSaveVarAscii));
	brsmemset((UDINT)&DirCheck, 0, sizeof(DirCheck));
	gParHandling.State.bMParValid = 0;
	gParHandling.State.bPParValid = 0;
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC ParHandlingCYCLIC(void)
{	

	// ---------- StepHandling ----------
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	
	// ---------- Schrittkette ----------
	switch(Step.eStepNr)
	{
		//=======================================================================================================================
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			
			Step.eStepNr = eSTEP_INIT_FINISHED;
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			
			Step.bInitDone = 1;
			Step.eStepNr = eSTEP_CHECK_PAR_DIR;						// Verzeichnisse �berpr�fen und Maschinen- bzw. Produktparameter laden
			break;

		//=======================================================================================================================
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			
			// ---------- Parameterverzeichnis �berpr�fen ----------
			if (gParHandling.CallerBox.bCheckDir == 1)
			{
				brsmemset((UDINT)&DirCheck, 0, sizeof(DirCheck));
				Step.eStepNr = eSTEP_CHECK_PAR_DIR;			
			}
			
			// ---------- Maschinenparameter laden ----------
			if(gParHandling.CallerBox.bLoadMPar == 1)
			{
				brsmemset((UDINT)&gPar.MachinePar, 0, sizeof(gPar.MachinePar));
				brsmemset((UDINT)&fbLoadVarAscii, 0, sizeof(fbLoadVarAscii));
				gParHandling.State.bParFileLoading = 1;
				gParHandling.State.bMParValid = 0;
				Step.eStepNr = eSTEP_LOAD_M_PAR;
			}
			
			// ---------- Produktparameter laden ----------
			if(gParHandling.CallerBox.bLoadProductPar == 1)
			{
				brsmemset((UDINT)&gPar, 0, sizeof(gPar));
				brsmemset((UDINT)&fbLoadVarAscii, 0, sizeof(fbLoadVarAscii));
				gParHandling.State.bParFileLoading = 1;
				gParHandling.State.bMParValid = 0;
				gParHandling.State.bPParValid = 0;
				Step.eStepNr = eSTEP_LOAD_PRODUCT_PAR;
			}
			
			// ---------- Produktparameter speichern ----------
			if(gParHandling.CallerBox.bSaveProductPar == 1)
			{
				brsmemset((UDINT)&fbSaveVarAscii, 0, sizeof(fbSaveVarAscii));
				Step.eStepNr = eSTEP_SAVE_PRODUCT_PAR;
			}
			
			// ---------- Maschinenparameter speichern ----------
			if(gParHandling.CallerBox.bSaveMPar == 1)
			{
				brsmemset((UDINT)&fbSaveVarAscii, 0, sizeof(fbSaveVarAscii));
				Step.eStepNr = eSTEP_SAVE_M_PAR;
			}
			
			// ---------- Produktparameter l�schen ----------
			if(gParHandling.CallerBox.bDeleteProductPar == 1)
			{
				Step.eStepNr = eSTEP_DELETE_PRODUCT_PAR;
			}
			
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Hier wird �berpr�ft ob das Verzeichnis f�r die Parameter vorhanden ist
		case eSTEP_CHECK_PAR_DIR:
			strcpy(Step.sStepText, "eSTEP_CHECK_PAR_DIR");
			
			DirCheck.fbDirInfo.pDevice	= (UDINT)&sDEV_USER;						// File Device �bergeben
			DirCheck.fbDirInfo.pPath	= (UDINT)&sDIR_PARAMETER;					// Pfad �bergeben
			DirCheck.fbDirInfo.enable	= 1;
			DirInfo(&DirCheck.fbDirInfo);
			
			if (DirCheck.fbDirInfo.status != ERR_FUB_BUSY)
			{
				DirCheck.fbDirInfo.enable = 0;
				if (DirCheck.fbDirInfo.status == 0)									// Verzeichnis "Parameter" existiert
				{
					if (gParHandling.State.bInitDone == 0)
					{
						Step.eStepNr = eSTEP_CHECK_PRODUCT_PAR;						// Parameter Laden
					}
					else
					{
						BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;						// Auf Kommando warten
					}
				}
				else if (DirCheck.fbDirInfo.status == fiERR_DIR_NOT_EXIST)			// Verzeichnis existiert nicht
				{
					Step.eStepNr = eSTEP_CREATE_PAR_DIR;							// Parameterverzeichnis erstellen
				}
				else																// Fehler aufgetreten
				{
					AppSetEvent(eEVT_ERR_READ_PARAMETER, DirCheck.fbDirInfo.status, "ParHandling", 0, &gEventManagement);
					AppResetEvent(eEVT_ERR_READ_PARAMETER, &gEventManagement);
					
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;		
				}
			}
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Falls kein Verzeichnis vorhanden ist, Verzeichnis erstellen
		case eSTEP_CREATE_PAR_DIR:
			strcpy(Step.sStepText, "eSTEP_CREATE_PAR_DIR");
			
			DirCheck.fbDirCreate.pDevice	= (UDINT)&sDEV_USER;				// File Device �bergeben
			DirCheck.fbDirCreate.pName		= (UDINT)&sDIR_PARAMETER;			// Verzichnisname �bergeben
			DirCheck.fbDirCreate.enable		= 1;
			DirCreate(&DirCheck.fbDirCreate);
			
			if (DirCheck.fbDirCreate.status != ERR_FUB_BUSY)
			{
				DirCheck.fbDirCreate.enable = 0;
				if (DirCheck.fbDirCreate.status == 0)								// Verzeichnis erstellt
				{
					Step.eStepNr = eSTEP_CREATE_MPAR_DIR;
				}
				else																// Fehler aufgetreten
				{
					AppSetEvent(eEVT_ERR_CREATING_DIR, DirCheck.fbDirCreate.status, "ParHandling", 0, &gEventManagement);
					AppResetEvent(eEVT_ERR_CREATING_DIR, &gEventManagement);
					
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;			
				}
			}
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Falls kein Verzeichnis vorhanden ist, Verzeichnis erstellen
		case eSTEP_CREATE_MPAR_DIR:
			strcpy(Step.sStepText, "eSTEP_CREATE_MPAR_DIR");
			
			DirCheck.fbDirCreate.pDevice	= (UDINT)&sDEV_USER;				// File Device �bergeben
			DirCheck.fbDirCreate.pName		= (UDINT)&sDIR_MPAR;				// Verzichnisname �bergeben
			DirCheck.fbDirCreate.enable		= 1;
			DirCreate(&DirCheck.fbDirCreate);
			
			if (DirCheck.fbDirCreate.status != ERR_FUB_BUSY)
			{
				DirCheck.fbDirCreate.enable = 0;
				if (DirCheck.fbDirCreate.status == 0)							// Verzeichnis erstellt
				{
					Step.eStepNr = eSTEP_CREATE_PRODUCT_PAR_DIR;
				}
				else															// Fehler aufgetreten
				{
					AppSetEvent(eEVT_ERR_CREATING_DIR, DirCheck.fbDirCreate.status, "ParHandling", 0, &gEventManagement);
					AppResetEvent(eEVT_ERR_CREATING_DIR, &gEventManagement);
					
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;				
				}
			} 
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Falls kein Verzeichnis vorhanden ist, Verzeichnis erstellen
		case eSTEP_CREATE_PRODUCT_PAR_DIR:
			strcpy(Step.sStepText, "eSTEP_CREATE_PRODUCT_PAR_DIR");
			
			DirCheck.fbDirCreate.pDevice	= (UDINT)&sDEV_USER;
			DirCheck.fbDirCreate.pName		= (UDINT)&sDIR_PRODUCT_PAR;
			DirCheck.fbDirCreate.enable		= 1;
			DirCreate(&DirCheck.fbDirCreate);
			
			if (DirCheck.fbDirCreate.status != ERR_FUB_BUSY)
			{
				DirCheck.fbDirCreate.enable = 0;
				if (DirCheck.fbDirCreate.status == 0)
				{	
					if (gParHandling.State.bInitDone == 0)
					{
						Step.eStepNr = eSTEP_CHECK_PRODUCT_PAR;
					}
					else
					{
						BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
					}
				}
				else
				{
					AppSetEvent(eEVT_ERR_CREATING_DIR, DirCheck.fbDirCreate.status, "ParHandling", 0, &gEventManagement);
					AppResetEvent(eEVT_ERR_CREATING_DIR, &gEventManagement);
					
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;			
				}
			} 
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// �berpr�fung ob ein Produktparameter (File) vorhanden ist
		case eSTEP_CHECK_PRODUCT_PAR:
			strcpy(Step.sStepText, "eSTEP_CHECK_PRODUCT_PAR");
			
			DirCheck.fbDirInfo.pDevice	= (UDINT)&sDEV_USER;						// File Device �bergeben
			DirCheck.fbDirInfo.pPath	= (UDINT)&sDIR_PRODUCT_PAR;					// Pfad �bergeben
			DirCheck.fbDirInfo.enable	= 1;
			DirInfo(&DirCheck.fbDirInfo);
			
			if (DirCheck.fbDirInfo.status != ERR_FUB_BUSY)
			{
				DirCheck.fbDirInfo.enable = 0;
				if (DirCheck.fbDirInfo.status == 0)									
				{
					if (DirCheck.fbDirInfo.filenum > 0)								// Produktparameter vorhanden
					{
						brsstrcpy((UDINT)&gParHandling.Par.sFileName,(UDINT)&gParPermanent.sCurrentProductName);
						Step.eStepNr = eSTEP_LOAD_PRODUCT_PAR;						// Produktparameter laden
					}
					else
					{
						Step.eStepNr = eSTEP_LOAD_M_PAR;							// Maschinenparameter laden
					}
				}
				else																// Fehler aufgetreten
				{
					AppSetEvent(eEVT_ERR_READ_PARAMETER, DirCheck.fbDirInfo.status, "ParHandling", 0, &gEventManagement);
					AppResetEvent(eEVT_ERR_READ_PARAMETER, &gEventManagement);
					
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;		
				}
			}
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Falls ein Produktparameter vorhanden ist --> Produktparameter laden
		case eSTEP_GET_PRODUCT_PAR:
			strcpy(Step.sStepText, "eSTEP_GET_PRODUCT_PAR");
			
			DirCheck.fbDirRead.pDevice	= (UDINT)&sDEV_USER;					// File Device �bergeben
			DirCheck.fbDirRead.pPath	= (UDINT)&sDIR_PRODUCT_PAR;				// Pfad �bergeben
			DirCheck.fbDirRead.entry	= 0;									// Bei Initialisierung immmer den ersten Eintrag lesen
			DirCheck.fbDirRead.option	= fiFILE;
			DirCheck.fbDirRead.pData	= (UDINT)&DirCheck.ParDirData;
			DirCheck.fbDirRead.data_len	= sizeof(DirCheck.ParDirData);
			DirCheck.fbDirRead.enable	= 1;
			DirRead(&DirCheck.fbDirRead);
			
			if (DirCheck.fbDirRead.status != ERR_FUB_BUSY)
			{
				DirCheck.fbDirRead.enable = 0;
				if (DirCheck.fbDirRead.status == 0)
				{
					brsmemcpy((UDINT)&gParHandling.Par.sFileName, (UDINT)&DirCheck.ParDirData.Filename[0], sizeof(gParHandling.Par.sFileName));
					Step.eStepNr = eSTEP_LOAD_PRODUCT_PAR;
				}
				else															// Fehler ist aufgetreten
				{
					AppSetEvent(eEVT_ERR_READ_PARAMETER, DirCheck.fbDirRead.status, "ParHandling", 0, &gEventManagement);
					AppResetEvent(eEVT_ERR_READ_PARAMETER, &gEventManagement);
					
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;		
				}
			}
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Produktparameter laden
		case eSTEP_LOAD_PRODUCT_PAR:
			strcpy(Step.sStepText, "eSTEP_LOAD_PRODUCT_PAR");
			
			gParHandling.State.bParFileLoading = 0;
			
			BrbCheckFileEnding((STRING*)&gParHandling.Par.sFileName,(STRING*)&".txt");	// Endung �berpr�fen
			fbLoadVarAscii.pDevice	= (STRING*)&sDEV_PRODUCT_PAR;						// File Device �bergeben
			fbLoadVarAscii.pFile	= (STRING*)&gParHandling.Par.sFileName;				// Filenamen �bergeben
			fbLoadVarAscii.nLinesToReadAtOneStep = 150;
			BrbLoadVarAscii(&fbLoadVarAscii);
			
			if (fbLoadVarAscii.nStatus != ERR_FUB_BUSY)
			{
				if (fbLoadVarAscii.nStatus == 0)										// Daten erfolgreich geladen
				{
					AppSetEvent(eEVT_INF_PARAMETER_LOADED, 0, gParHandling.Par.sFileName, 0, &gEventManagement);
					AppResetEvent(eEVT_INF_PARAMETER_LOADED, &gEventManagement);
					gParHandling.State.bMParValid = 1;									// Daten sind g�ltig
					gParHandling.State.bPParValid = 1;									// Daten sind g�ltig
					brsmemcpy((UDINT)&ParamterCopy,(UDINT)&gPar,sizeof(ParamterCopy));
					brsmemset((UDINT)&gParPermanent.sCurrentProductName, 0, sizeof(gParPermanent.sCurrentProductName));
					strncpy(gParPermanent.sCurrentProductName, gParHandling.Par.sFileName, strlen(gParHandling.Par.sFileName) - 4);
					brsmemset((UDINT)&gParHandling.Par.sFileName, 0, sizeof(gParHandling.Par.sFileName));
					Step.eStepNr = eSTEP_CHECK_AXES;
				}
				else																	// Fehler ist aufgetreten
				{
					brsmemset((UDINT)&gParHandling.Par.sFileName, 0, sizeof(gParHandling.Par.sFileName));
					if ((gParHandling.State.bInitDone == 0) &&
						((fbLoadVarAscii.nStatus == fiERR_FILE) ||
						(fbLoadVarAscii.nStatus == fiERR_FILE_NOT_FOUND)))				
					{
						Step.eStepNr = eSTEP_GET_PRODUCT_PAR;
					}
					else
					{
						AppSetEvent(eEVT_ERR_LOAD_PARAMETER, fbLoadVarAscii.nStatus, gParHandling.Par.sFileName, 0, &gEventManagement);
						AppResetEvent(eEVT_ERR_LOAD_PARAMETER, &gEventManagement);
						
						BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
					}
				}
				if (gParHandling.State.bInitDone == 0)
				{
					gParHandling.State.bInitDone = 1;									// Initialisierung abgeschlossen
				}
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		// Maschinenparameter laden
		case eSTEP_LOAD_M_PAR:
			strcpy(Step.sStepText, "eSTEP_LOAD_M_PAR");
			
			gParHandling.State.bParFileLoading = 0;
			
			fbLoadVarAscii.pDevice	= (STRING*)&sDEV_M_PAR;								// File Device �bergeben
			fbLoadVarAscii.pFile	= (STRING*)&sM_PAR_FILE_NAME;						// Filenamen �bergeben
			fbLoadVarAscii.nLinesToReadAtOneStep = 150;
			BrbLoadVarAscii(&fbLoadVarAscii);
			
			if (fbLoadVarAscii.nStatus != ERR_FUB_BUSY)
			{
				if (fbLoadVarAscii.nStatus == 0)										// Daten erfolgreich geladen
				{
					AppSetEvent(eEVT_INF_PARAMETER_LOADED, 0, (STRING*)&sM_PAR_FILE_NAME, 0, &gEventManagement);
					AppResetEvent(eEVT_INF_PARAMETER_LOADED, &gEventManagement);
					gParHandling.State.bMParValid = 1;									// Daten sind g�ltig
					brsmemcpy((UDINT)&ParamterCopy,(UDINT)&gPar,sizeof(ParamterCopy));
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
				}
				else																	// Fehler ist aufgetreten
				{
					AppSetEvent(eEVT_ERR_LOAD_PARAMETER, fbLoadVarAscii.nStatus, (STRING*)&sM_PAR_FILE_NAME, 0, &gEventManagement);
					AppResetEvent(eEVT_ERR_LOAD_PARAMETER, &gEventManagement);
					
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
				}
				if (gParHandling.State.bInitDone == 0)
				{
					gParHandling.State.bInitDone = 1;									// Initialisierung abgeschlossen
				}
			}
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Produktparameter speichern
		case eSTEP_SAVE_PRODUCT_PAR:
			strcpy(Step.sStepText, "eSTEP_SAVE_PRODUCT_PAR");
			
			BrbCheckFileEnding((STRING*)&gParHandling.Par.sFileName,(STRING*)&".txt");		// Endung �berpr�fen
			fbSaveVarAscii.pDevice	= (STRING*)&sDEV_PRODUCT_PAR;							// File Device �bergeben
			fbSaveVarAscii.pFile	= (STRING*)gParHandling.Par.sFileName;					// Filenamen �bergeben
			fbSaveVarAscii.pVarName	= (STRING*)&sVAR_NAME_PRODUCT_PAR;						// Variablennamen �bergeben
			fbSaveVarAscii.nLinesToWriteAtOneStep = 150;
			BrbSaveVarAscii(&fbSaveVarAscii);			
			
			if (fbSaveVarAscii.nStatus != ERR_FUB_BUSY)
			{
				if (fbSaveVarAscii.nStatus == 0)							// Daten erfolgreich gespeichert
				{
					AppSetEvent(eEVT_INF_PARAMETER_SAVED, 0, gParHandling.Par.sFileName, 0, &gEventManagement);
					AppResetEvent(eEVT_INF_PARAMETER_SAVED, &gEventManagement);
					brsmemcpy((UDINT)&ParamterCopy,(UDINT)&gPar,sizeof(ParamterCopy));
					brsmemset((UDINT)&gParPermanent.sCurrentProductName, 0, sizeof(gParPermanent.sCurrentProductName));
					strncpy(gParPermanent.sCurrentProductName, gParHandling.Par.sFileName, strlen(gParHandling.Par.sFileName) - 4);
					gParHandling.State.bPParValid = 1;
					
//					brsmemset((UDINT)&gParHandling.Par.sFileName, 0, sizeof(gParHandling.Par.sFileName));
//					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
//					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
					Step.eStepNr = eSTEP_SAVE_M_PAR;
				}
				else														// Fehler ist aufgetreten
				{
					AppSetEvent(eEVT_ERR_SAVE_PARAMETER, fbSaveVarAscii.nStatus, gParHandling.Par.sFileName, 0, &gEventManagement);
					AppResetEvent(eEVT_ERR_SAVE_PARAMETER, &gEventManagement);
					
					brsmemset((UDINT)&gParHandling.Par.sFileName, 0, sizeof(gParHandling.Par.sFileName));
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
				}
			}
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Maschinenparameter speichern
		case eSTEP_SAVE_M_PAR:
			strcpy(Step.sStepText, "eSTEP_SAVE_M_PAR");
			
			fbSaveVarAscii.pDevice	= (STRING*)&sDEV_M_PAR;					// File Device �bergebe
			fbSaveVarAscii.pFile	= (STRING*)&sM_PAR_FILE_NAME;			// Filenamen �bergeben
			fbSaveVarAscii.pVarName	= (STRING*)&sVAR_NAME_M_PAR;			// Variablennamen �bergeben
			fbSaveVarAscii.nLinesToWriteAtOneStep = 150;
			BrbSaveVarAscii(&fbSaveVarAscii);
			
			if (fbSaveVarAscii.nStatus != ERR_FUB_BUSY)
			{
				if (fbSaveVarAscii.nStatus == 0)							// Daten erfolgreich gespeichert
				{
					AppSetEvent(eEVT_INF_PARAMETER_SAVED, 0, (STRING*)&sM_PAR_FILE_NAME, 0, &gEventManagement);
					AppResetEvent(eEVT_INF_PARAMETER_SAVED, &gEventManagement);
					gParHandling.State.bMParValid = 1;
					brsmemcpy((UDINT)&ParamterCopy,(UDINT)&gPar,sizeof(ParamterCopy));
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
				}
				else														// Fehler ist aufgetreten
				{
					AppSetEvent(eEVT_ERR_SAVE_PARAMETER, fbSaveVarAscii.nStatus, (STRING*)&sM_PAR_FILE_NAME, 0, &gEventManagement);
					AppResetEvent(eEVT_ERR_SAVE_PARAMETER, &gEventManagement);
					
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
				}
			}
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Produktparameter l�schen
		case eSTEP_DELETE_PRODUCT_PAR:
			strcpy(Step.sStepText, "eSTEP_DELETE_PRODUCT_PAR");
			
			BrbCheckFileEnding((STRING*)&gParHandling.Par.sFileName, (STRING*)&".txt");		// Endung �berpr�fen
			fbDelFile.pDevice	= (UDINT)&sDEV_PRODUCT_PAR;									// Filedevice �bergeben
			fbDelFile.pName		= (UDINT)&gParHandling.Par.sFileName;						// Filenamen �bergeben
			fbDelFile.enable	= 1;
			FileDelete(&fbDelFile);
			
			if (fbDelFile.status != ERR_FUB_BUSY)
			{
				fbDelFile.enable = 0;
				if (fbDelFile.status == 0)
				{
					AppSetEvent(eEVT_INF_PARAMETER_DELETED, 0, gParHandling.Par.sFileName, 0, &gEventManagement);
					AppResetEvent(eEVT_INF_PARAMETER_DELETED, &gEventManagement);

					brsmemset((UDINT)&gParHandling.Par.sFileName, 0, sizeof(gParHandling.Par.sFileName));
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
				}
				else
				{
					AppSetEvent(eEVT_ERR_DEL_PARAMETER, fbDelFile.status, gParHandling.Par.sFileName, 0, &gEventManagement);
					AppResetEvent(eEVT_ERR_DEL_PARAMETER, &gEventManagement);
					
					brsmemset((UDINT)&gParHandling.Par.sFileName, 0, sizeof(gParHandling.Par.sFileName));
					BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;				
				}
			}
			
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		// Achsen pr�fen
		case eSTEP_CHECK_AXES:
			strcpy(Step.sStepText, "eSTEP_CHECK_AXES");
			if(gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn == 1)
			{
				gVisu.DirectBox.bShowDialogMoveAxes = 1;
				Step.eStepNr = eSTEP_INIT_ALL_AX_LIMITS;
			}
			else
			{
				Step.eStepNr = eSTEP_CHECK_HEATING;
			}
			break;

		// Alle Achs-Limits initialisieren
		case eSTEP_INIT_ALL_AX_LIMITS:
			strcpy(Step.sStepText, "eSTEP_INIT_ALL_AX_LIMITS");
			if(BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_PAR_HANDLING) == eBRB_CALLER_STATE_OK)
			{
				gAxisControl.CallerBox.bInitAllAxesLimits = 1;
				Step.eStepNr = eSTEP_CHECK_AXES_WAIT;
			}
			break;

		case eSTEP_CHECK_AXES_WAIT:
			strcpy(Step.sStepText, "eSTEP_CHECK_AXES_WAIT");
			if(gVisu.State.bDialogMoveAxesActive == 0)
			{
				Step.eStepNr = eSTEP_CHECK_HEATING;
			}
			break;

		// Heizung pr�fen
		case eSTEP_CHECK_HEATING:
			strcpy(Step.sStepText, "eSTEP_CHECK_HEATING");
			if(gPar.MachinePar.MachineModule.bTempCtrlEn == 1)
			{
				gVisu.DirectBox.bShowDialogStartHeating = 1;
			}
			Step.eStepNr = eSTEP_CHECK_HEATING_WAIT;
			break;

		case eSTEP_CHECK_HEATING_WAIT:
			strcpy(Step.sStepText, "eSTEP_CHECK_HEATING_WAIT");
			if(gVisu.State.bDialogStartHeatingActive == 0)
			{
				BrbClearCallerBox((UDINT)&gParHandling.CallerBox, sizeof(gParHandling.CallerBox));
				Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			}
			break;
	}
	
	// ---------- Ein Kommando wird ausgef�hrt ----------
	if (Step.eStepNr != eSTEP_WAIT_FOR_COMMAND)
	{
		gParHandling.State.bAction = 1;
	}
	else
	{
		gParHandling.State.bAction = 0;
	}
	
	// ---------- Pr�fen ob Parameter ge�ndert wurden ----------
	nCyclicCnt += 1;
	if (nCyclicCnt == 10)
	{
		nCyclicCnt = 0;
		if(gParHandling.State.bAction == 0)
		{
			nMemCmpResult = brsmemcmp((UDINT)&gPar,(UDINT)&ParamterCopy,sizeof(gPar));
			if (nMemCmpResult != 0)
			{
				gParHandling.State.bParChanged = 1;
			}
			else
			{
				gParHandling.State.bParChanged = 0;
			}
		}
		else
		{
			gParHandling.State.bParChanged = 0;
		}
	}
}

void _EXIT ParHandlingEXIT(void)
{
}
