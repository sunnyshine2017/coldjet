(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: ParHandling
 * File: G_ParHandling.typ
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Global  data types of package ParHandling
 ********************************************************************)

TYPE
	ParHandlingCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bCheckDir : BOOL; (*Parameterverzeichnis �berpr�fen*)
		bLoadProductPar : BOOL; (*Produktparameter laden*)
		bLoadMPar : BOOL; (*Maschinenparameter laden*)
		bSaveProductPar : BOOL; (*Produktparameter speichern*)
		bSaveMPar : BOOL; (*Maschinenparameter speichern*)
		bDeleteProductPar : BOOL; (*Produktparameter l�schen*)
	END_STRUCT;
	ParHandlingDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bDummy : BOOL;
	END_STRUCT;
	ParHandlingPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		sFileName : STRING[nBRB_FILE_NAME_CHAR_MAX]; (*Name des Parameterfiles*)
	END_STRUCT;
	ParHandlingState_TYP : 	STRUCT  (*R�ckmeldungen*)
		bInitDone : BOOL; (*Initialisierung abgeschlossen*)
		bMParValid : BOOL; (*Maschinenparameter g�ltig*)
		bPParValid : BOOL; (*Produktparameter g�ltig*)
		bParChanged : BOOL; (*Parameter wurden ge�ndert*)
		bAction : BOOL; (*Ein Kommando wird ausgef�hrt*)
		bParFileLoading : BOOL; (*Parameter werden geladen*)
	END_STRUCT;
	ParHandling_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : ParHandlingCallerBox_TYP;
		DirectBox : ParHandlingDirectBox_TYP;
		Par : ParHandlingPar_TYP;
		State : ParHandlingState_TYP;
	END_STRUCT;
END_TYPE
