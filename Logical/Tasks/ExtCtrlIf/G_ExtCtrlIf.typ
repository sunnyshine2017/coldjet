(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: ExtCtrlIf
 * File: G_ExtCtrlIf.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Global  data types of package ExtCtrlIf
 ********************************************************************)

TYPE
	ExtCtrlIfCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bDummy : BOOL;
	END_STRUCT;
	ExtCtrlIfDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bStartIntervalCleaning : BOOL; (*Intervallreinigung ("Freischie�en") wird angefordert*)
	END_STRUCT;
	ExtCtrlIfPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		nDummy : UINT;
	END_STRUCT;
	ExtCtrlIfState_TYP : 	STRUCT  (*R�ckmeldungen*)
		bBondingNotAllowed : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Dem Klebekopt hat keine Freigabe von der Schnittstelle zum kleben.*)
		bCleanBondingDevices : BOOL; (*�ber die Schnittstelle wurde das Reinigen aller Klebek�pfe angefordert.*)
		bFillHopperBondigDevice : BOOL; (*Der Klebekopf am Trichter soll mit Klebstoff bef�llt werden. Dies wird �ber die Schnittstelle angefordert.*)
		bFillCollectorBondigDevice : BOOL; (*Der Klebekopf am Sammelzylinder soll mit Klebstoff bef�llt werden. Dies wird �ber die Schnittstelle angefordert.*)
		bMakuTurnOutClosed : BOOL; (*Die Maku-Weiche ist geschlossen. Produkte werden nicht mehr ausgeschleust und kommen am Klebekopf an.*)
		bIntervalCleaningActive : BOOL; (*Die Intervallreinigung ("Freischie�en") wird durchgef�hrt*)
		bCleaningActiveHopperBonDev : BOOL; (*Die Reinigung des Trichterkopfes wurde �ber dei Schnittstelle angefordert und ist zur Zeit aktiv.*)
		bCleaningActiveCollectorBonDev : BOOL; (*Die Reinigung des Klebekopfes am Sammelzylinder wurde �ber dei Schnittstelle angefordert und ist zur Zeit aktiv.*)
		bBondingDeviceCleaned : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Der Reinigugnsvorgang mit Reinigungsmittel ist f�r diesen Reinigungszyklus f�r den jeweiligen Klebekopf abgeschlossen.*)
	END_STRUCT;
	ExtCtrlIf_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : ExtCtrlIfCallerBox_TYP;
		DirectBox : ExtCtrlIfDirectBox_TYP;
		Par : ExtCtrlIfPar_TYP;
		State : ExtCtrlIfState_TYP;
	END_STRUCT;
END_TYPE
