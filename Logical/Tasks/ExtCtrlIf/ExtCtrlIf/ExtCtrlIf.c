/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: ExtCtrlIf
 * File: ExtCtrlIf.c
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Implementation of program ExtCtrlIf
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <ExtCtrlIfFunc.h>

void _INIT ExtCtrlIfINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gExtCtrlIf, 0, sizeof(gExtCtrlIf));
	gExtCtrlIf.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
		
	// Initialisierung der Schrittketten
	Step.eStepNr = eSTEP_INIT;
	CleaningHopperStep.eStepNr = eSTEP_WAIT_CMD_CLEAN_HOPPER;
	CleaningCollectorStep.eStepNr = eSTEP_WAIT_CMD_CLEAN_COLLECTOR;
	
}

void _CYCLIC ExtCtrlIfCYCLIC(void)
{
	// Direct-Kommandos
	if(Step.bInitDone == 1)
	{
//		if(gExtCtrlIf.DirectBox.bDummy == 1)
//		{
//			BrbClearDirectBox((UDINT)&gExtCtrlIf.DirectBox, sizeof(gExtCtrlIf.DirectBox));
//		}
	}
	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			//if(gParHandling.State.bParValid == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gExtCtrlIf.CallerBox, sizeof(gExtCtrlIf.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			if(gExtCtrlIf.CallerBox.bDummy == 1)
			{
				Step.eStepNr = eSTEP_CMD1;
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_CMD1:
			strcpy(Step.sStepText, "eSTEP_CMD1");
			Step.eStepNr = eSTEP_CMD1_FINISHED;
			break;

		case eSTEP_CMD1_FINISHED:
			strcpy(Step.sStepText, "eSTEP_CMD1_FINISHED");
			BrbClearCallerBox((UDINT)&gExtCtrlIf.CallerBox, sizeof(gExtCtrlIf.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;
	
	}
	
	
	// ---------------- �berwachung ob der Betriebsmodus "Automatik" aktiv ----------------
	// Pr�fen ob der Betriebsmodus "Automatik" verlassen wird
	PosEdgeAutomaticOff.CLK = gMainLogic.State.bAutomatic;
	F_TRIG( &PosEdgeAutomaticOff );
	
	// Im Moment in dem Automatik verlassen wird, die Reinigungsschrittketten zur�cksetzen
	if( PosEdgeAutomaticOff.Q == TRUE )
	{
		// Schrittketten zur�cksetzen
		CleaningHopperStep.eStepNr = eSTEP_RESET_HOPPER_CLEANING;
		CleaningCollectorStep.eStepNr = eSTEP_RESET_COLLECTOR_CLEANING;
	}
	
	
	// StepHandling
	if(CleaningHopperStepHandling.Current.bTimeoutElapsed == 1)
	{
		CleaningHopperStepHandling.Current.bTimeoutElapsed = 0;
		CleaningHopperStep.eStepNr = CleaningHopperStepHandling.Current.nTimeoutContinueStep;
	}
	CleaningHopperStepHandling.Current.nStepNr = (DINT)CleaningHopperStep.eStepNr;
	strcpy(CleaningHopperStepHandling.Current.sStepText, CleaningHopperStep.sStepText);
	BrbStepHandler( &CleaningHopperStepHandling );
	
	// ----------------- Ablauf bei der Reinigung des Trichterkopfes -----------------
	switch(CleaningHopperStep.eStepNr)
	{
		// Warten bis System in "Automatik" und Kommando "K�pfe reinigen" kommt
		case eSTEP_WAIT_CMD_CLEAN_HOPPER:
			strcpy( CleaningHopperStep.sStepText, "eSTEP_WAIT_CMD_CLEAN_HOPPER" );
			
			// Das System befindet sich im Automatik-Betrieb
			if( gMainLogic.State.bAutomatic == TRUE )
			{
				// �ber die Schnittstelle wurde das Kommande "K�pfe reinigen" entgegen genommen
				// UND die Reinigung �ber die Schnittstelle ist noch nicht aktiv
				// Damit wird das Starten der Reinigung des Trichterkopfes verhindert, wenn die Reinigung des Kopfes
				// am Sammelzylinder bereits aktiv ist und der Trichterkopf w�hrend dessen aktiv geschaltet wird
				if( (gExtCtrlIf.State.bCleanBondingDevices == TRUE) &&
					(gExtCtrlIf.State.bCleaningActiveCollectorBonDev == FALSE) )
				{
					// Der Klebekopf am Trichter ist aktiviert (in Produktion)
					if( gPar.ProductPar.BonDevPar[0].bBonDevEn == TRUE )
					{
						// Der Klebekopf darf nicht mehr kleben sobald das Kommando zur Reinigung kommt
						gExtCtrlIf.State.bBondingNotAllowed[0] = TRUE;
						
						// Der Reinigugnsvorgang mit Reinigungsmittel ist f�r den Trichterkopf nicht abgeschlossen
						gExtCtrlIf.State.bBondingDeviceCleaned[0] = FALSE;
						
						// N�chster Schritt: Den Trichterkopf mit Reinigungsfl�ssigkeit kontinuierlich sp�len
						CleaningHopperStep.eStepNr = eSTEP_START_CLEAN_HOPPER;
					}
				}
			}
			break;
			
		// Den Trichterkopf mit Reinigungsfl�ssigkeit kontinuierlich sp�len
		case eSTEP_START_CLEAN_HOPPER:
			strcpy( CleaningHopperStep.sStepText, "eSTEP_START_CLEAN_HOPPER" );
				
			// Die Reinigung ist aktiv
			gExtCtrlIf.State.bCleaningActiveHopperBonDev = TRUE;
			
			if( gMainLogic.State.bCleaningWithGlueActive == FALSE )
			{
				// Direktes Kommando "Kontinuierlich Reinigen mit Reinigungsmittel" an MainLogic senden.
				gMainLogic.DirectBox.bContCleanBonDevWithDetergent[0] = TRUE;								
				// Den Trichterkopf sp�len, bis die Reinigungsdauer aus nHopperCleaningPeriod abgelaufen ist.
				BrbStartStepTimeout( &CleaningHopperStepHandling, gPar.MachinePar.nHopperCleaningPeriod, eSTEP_CLEAN_HOPPER_DONE );
			}
			break;
		
		// Der Trichterkopf wurde �ber die parametrierte Dauer mit Reinigungsfl�ssigkeit gesp�lt
		case eSTEP_CLEAN_HOPPER_DONE:
			strcpy( CleaningHopperStep.sStepText, "eSTEP_CLEAN_HOPPER_DONE" );	
			
			// Kommando zur kontinuierliche Reinigung nicht mehr an MainLogic senden
			gMainLogic.DirectBox.bContCleanBonDevWithDetergent[0] = FALSE;
			
			// Der Reinigugnsvorgang mit Reinigungsmittel ist f�r den Trichterkopf abgeschlossen
			gExtCtrlIf.State.bBondingDeviceCleaned[0] = TRUE;
			
			// n�chster Schritt: Warten bis das Kommando "Trichterkopf bef�llen" ansteht
			CleaningHopperStep.eStepNr = eSTEP_WAIT_CMD_FILL_HOPPER;
			
			break;
		
		// Warten bis das Kommando "Trichterkopf bef�llen" ansteht
		case eSTEP_WAIT_CMD_FILL_HOPPER:
			strcpy( CleaningHopperStep.sStepText, "eSTEP_WAIT_CMD_FILL_HOPPER" );
			
			// �ber die Schnittstelle wurde das Kommande "Trichterkopf bef�llen" entgegen genommen
			if( gExtCtrlIf.State.bFillHopperBondigDevice == TRUE )
			{
				// Der Kopf am Sammelzylinder wird ebenfalls gereinigt
				if( gExtCtrlIf.State.bCleaningActiveCollectorBonDev == TRUE )
				{
					// n�chster Schritt: Warten bis der Klebekopf am Sammelzylinder mit dem Reinigen mit Reinigungsmittel fertig ist
					CleaningHopperStep.eStepNr = eSTEP_WAIT_COLLECTOR_CLEANED;
				}
				else
				{
					// n�chster Schritt: Den Trichterkopf mit Klebstoff �ber die parametrierte Dauer bef�llen
					CleaningHopperStep.eStepNr = eSTEP_START_FILL_HOPPER;
				}
			}
			break;
				
		// Warten bis der Klebekopf am Sammelzylinder mit dem Reinigen mit Reinigungsmittel fertig ist
		case eSTEP_WAIT_COLLECTOR_CLEANED:
			strcpy( CleaningHopperStep.sStepText, "eSTEP_WAIT_COLLECTOR_CLEANED" );
			
			// Mit dem bef�llen des Trichterkopfes mit Klebstoff muss gewartet werden, bis der Klebekopf am Sammelzylinder mit dem Reinigen mit
			// Reinigungsmittel fertig ist. Dies ist n�tig, da es nur ein Ventil f�r alle Klebek�pfe f�r die Umschaltung zwischen Reinigungs-
			// mittel und Klebstoff gibt. Somit ist es nicht m�glich den einen Klebekopf mit Klebstoff und den anderen mit Reinigungsmittel
			// gleichzeitig zu durchsp�len. Daher muss hier gepr�ft werden ob das Reinigen mit Reinigungsmittel nicht mehr aktiv ist.
			if( (gMainLogic.State.bCleaningWithDetergentActive == FALSE) &&
				(gExtCtrlIf.State.bBondingDeviceCleaned[1] == TRUE) )
			{
				// n�chster Schritt: Den Trichterkopf mit Klebstoff �ber die parametrierte Dauer bef�llen
				CleaningHopperStep.eStepNr = eSTEP_START_FILL_HOPPER;
			}
			break;
		
		// Den Trichterkopf mit Klebstoff �ber die parametrierte Dauer bef�llen
		case eSTEP_START_FILL_HOPPER:
			strcpy( CleaningHopperStep.sStepText, "eSTEP_START_FILL_HOPPER" );
			
			// Der Bef�llvorgang mit Leim darf erst beginnen, wenn keine Reinigungsfl�ssigkeit mehr gef�rdert wird
			if( gMainLogic.State.bCleaningWithDetergentActive == FALSE )
			{
				// Trichterkopf mit Klebstoff f�llen
				gMainLogic.DirectBox.bContCleanBonDevWithGlue[0] = TRUE;
								
				// Den Trichterkopf mit Klebstoff f�llen, bis die Bef�lldauer aus nHopperFillingPeriod abgelaufen ist.
				BrbStartStepTimeout( &CleaningHopperStepHandling, gPar.MachinePar.nHopperFillingPeriod, eSTEP_FILL_HOPPER_DONE );
			}
			break;
		
		// Der Trichterkopf wurde �ber die parametrierte Dauer mit Klebstoff bef�llt
		case eSTEP_FILL_HOPPER_DONE:
			strcpy( CleaningHopperStep.sStepText, "eSTEP_FILL_HOPPER_DONE" );
			
			// Trichterkopf nicht mehr mit Klebstoff f�llen
			gMainLogic.DirectBox.bContCleanBonDevWithGlue[0] = FALSE;
			
			// Der Trichterkopf darf wieder kleben
			gExtCtrlIf.State.bBondingNotAllowed[0] = FALSE;
			
			// Die Reinigung ist nicht mehr aktiv
			gExtCtrlIf.State.bCleaningActiveHopperBonDev = FALSE;
			
			// n�chster Schritt: Warten bis System in "Automatik" und Kommando "K�pfe reinigen" kommt
			CleaningHopperStep.eStepNr = eSTEP_WAIT_CMD_CLEAN_HOPPER;
			
			break;
		
		// Schrittkette zur�cksetzen
		case eSTEP_RESET_HOPPER_CLEANING:
			strcpy( CleaningHopperStep.sStepText, "eSTEP_RESET_CLEAN_HOPPER" );
			
			// Alle gesetzten Kommandos und TimeOuts zur�cksetzen
			gMainLogic.DirectBox.bContCleanBonDevWithDetergent[0] = FALSE;
			gMainLogic.DirectBox.bContCleanBonDevWithGlue[0] = FALSE;
			BrbStopStepTimeout( &CleaningHopperStepHandling );
			
			// Der Trichterkopf darf wieder kleben
			gExtCtrlIf.State.bBondingNotAllowed[0] = FALSE;
			
			// Der Reinigugnsvorgang mit Reinigungsmittel ist f�r den Trichterkopf nicht abgeschlossen
			gExtCtrlIf.State.bBondingDeviceCleaned[0] = FALSE;
			
			// Die Reinigung ist nicht mehr aktiv
			gExtCtrlIf.State.bCleaningActiveHopperBonDev = FALSE;
			
			// n�chster Schritt: Warten bis System in "Automatik" und Kommando "K�pfe reinigen" kommt
			CleaningHopperStep.eStepNr = eSTEP_WAIT_CMD_CLEAN_HOPPER;
			break;
	}
	
	
	// StepHandling
	if(CleaningCollectorStepHandling.Current.bTimeoutElapsed == 1)
	{
		CleaningCollectorStepHandling.Current.bTimeoutElapsed = 0;
		CleaningCollectorStep.eStepNr = CleaningCollectorStepHandling.Current.nTimeoutContinueStep;
	}
	CleaningCollectorStepHandling.Current.nStepNr = (DINT)CleaningCollectorStep.eStepNr;
	strcpy(CleaningCollectorStepHandling.Current.sStepText, CleaningCollectorStep.sStepText);
	BrbStepHandler( &CleaningCollectorStepHandling );
	
	// ------------ Ablauf bei der Reinigung des Kopfes am Sammelzylinder ------------
	switch(CleaningCollectorStep.eStepNr)
	{
		// Warten bis System in "Automatik" und Kommando "K�pfe Reinigen" kommt
		case eSTEP_WAIT_CMD_CLEAN_COLLECTOR:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_WAIT_CMD_CLEAN_COLLECTOR" );
		
			// Das System befindet sich im Automatik-Betrieb
			if( gMainLogic.State.bAutomatic == TRUE )
			{
				// �ber die Schnittstelle wurde das Kommande "K�pfe reinigen" entgegen genommen
				// UND die Reinigung �ber die Schnittstelle ist noch nicht aktiv
				// Damit wird das Starten der Reinigung des Kopfes am Sammelzylinder verhindert, wenn die Reinigung des
				// Trichterkopfes bereits aktiv ist und der Kopfes am Sammelzylinder w�hrend dessen aktiv geschaltet wird
				if( (gExtCtrlIf.State.bCleanBondingDevices == TRUE) &&
					(gExtCtrlIf.State.bCleaningActiveHopperBonDev == FALSE) )
				{
					// Der Klebekopf am Sammelzylinder ist aktiviert (in Produktion)
					if( gPar.ProductPar.BonDevPar[1].bBonDevEn == TRUE )
					{
						// Der Klebekopf darf nicht mehr kleben sobald das Kommando zur Reinigung kommt
						gExtCtrlIf.State.bBondingNotAllowed[1] = TRUE;
						
						// Der Reinigugnsvorgang mit Reinigungsmittel ist f�r den Klebekopf am Sammelzylinder nicht abgeschlossen
						gExtCtrlIf.State.bBondingDeviceCleaned[1] = FALSE;
						
						// n�chster Schritt: Den Klebekopf am Sammelzylinder in horizontale Richtung zur Reinigungsstation verfahren
						CleaningCollectorStep.eStepNr = eSTEP_MOVE_HEAD_TO_CLEAN_STATION;
					}
				}
			}
			break;
		
		// Den Klebekopf am Sammelzylinder in horizontale Richtung zur Reinigungsstation verfahren
		case eSTEP_MOVE_HEAD_TO_CLEAN_STATION:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_MOVE_HEAD_TO_CLEAN_STATION" );
			
			// Die Reinigung ist aktiv
			gExtCtrlIf.State.bCleaningActiveCollectorBonDev = TRUE;
			
			// Um einen Achsfehler zu vermeiden, wird das Kommando zum Verfahren zur Reinigungsstation erst abgesetzt, nachdem die Achse
			// ihre eventuelle Referenzfahrt (diese wurde sp�testens mit Start Automatik ausgel�st) beendet hat.
			if( gAxisControl.State.AxisState[1].Horizontal.Homing == FALSE )
			{
				// Pr�fen ob das Kommandofach frei ist
				if( BrbSetCaller( &gAxisControl.CallerBox.Caller, eCALLERID_EXT_CTRL_IF) == eBRB_CALLER_STATE_OK )
				{	
					gAxisControl.Par.HmiPar.AbsPos[1].Horizontal = gPar.ProductPar.BonDevPar[1].AxesPosition.Alignment[eHORIZONTAL].rParkPosition;
					gAxisControl.CallerBox.HmiCaller.Horizontal[1].bStartAbsMove = TRUE;
					
					// n�chster Schritt: Warten bis der Klebekopf am Sammelzylinder an der Reinigungsstation angekommen ist
					CleaningCollectorStep.eStepNr = eSTEP_WAIT_HEAD_TO_CLEAN_STATION;
				}
			}
			break;
		
		// Warten bis der Klebekopf am Sammelzylinder an der Reinigungsstation angekommen ist
		case eSTEP_WAIT_HEAD_TO_CLEAN_STATION:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_WAIT_HEAD_TO_CLEAN_STATION" );
			
			// Die Achse hat die Reinigungsstation erreicht
			if( gAxisControl.State.AxisStatus[1].Horizontal.InPosition == TRUE )
			{
				// n�chster Schritt: Den Klebekopf am Sammelzylinder mit Reinigungsfl�ssigkeit kontinuierlich sp�len
				CleaningCollectorStep.eStepNr = eSTEP_START_CLEAN_COLLECTOR;
			}
			break;
			
		// Den Klebekopf am Sammelzylinder mit Reinigungsfl�ssigkeit kontinuierlich sp�len
		case eSTEP_START_CLEAN_COLLECTOR:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_START_CLEAN_COLLECTOR" );
			
			if( gMainLogic.State.bCleaningWithGlueActive == FALSE )
			{
				// Direktes Kommando "Kontinuierlich Reinigen mit Reinigungsmittel" an MainLogic senden.
				gMainLogic.DirectBox.bContCleanBonDevWithDetergent[1] = TRUE;				
				// Den Klebekopf am Sammelzylinder sp�len, bis die Reinigungsdauer aus nCollectorCleaningPeriod abgelaufen ist.
				BrbStartStepTimeout( &CleaningCollectorStepHandling, gPar.MachinePar.nCollectorCleaningPeriod, eSTEP_CLEAN_COLLECTOR_DONE );
			}
			break;
			
		
		// Der Klebekopf am Sammelzylinder wurde �ber die parametrierte Dauer mit Reinigungsfl�ssigkeit gesp�lt
		case eSTEP_CLEAN_COLLECTOR_DONE:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_CLEAN_COLLECTOR_DONE" );	
			
			// Kommando zur kontinuierliche Reinigung nicht mehr an MainLogic senden
			gMainLogic.DirectBox.bContCleanBonDevWithDetergent[1] = FALSE;
			
			// Der Reinigugnsvorgang mit Reinigungsmittel ist f�r den Klebekopf am Sammelzylinder abgeschlossen
			gExtCtrlIf.State.bBondingDeviceCleaned[1] = TRUE;
			
			// n�chster Schritt: Warten bis das Kommando "Klebekopf am Sammelzylinder bef�llen" ansteht
			CleaningCollectorStep.eStepNr = eSTEP_WAIT_CMD_FILL_COLLECTOR;
			break;
		
		// Warten bis das Kommando "Klebekopf am Sammelzylinder bef�llen" ansteht
		case eSTEP_WAIT_CMD_FILL_COLLECTOR:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_WAIT_CMD_FILL_COLLECTOR" );
			
			// �ber die Schnittstelle wurde das Kommande "Klebekopf am Sammelzylinder bef�llen" entgegen genommen
			if( gExtCtrlIf.State.bFillCollectorBondigDevice == TRUE )
			{
				// Der Trichterkopf wird ebenfalls gereinigt
				if( gExtCtrlIf.State.bCleaningActiveHopperBonDev == TRUE )
				{
					// n�chster Schritt: Warten bis der Trichterkopf mit dem Reinigen mit Reinigungsmittel fertig ist
					CleaningCollectorStep.eStepNr = eSTEP_WAIT_HOPPER_CLEANED;
				}
				else
				{
					// n�chster Schritt: Den Den Klebekopf am Sammelzylinder mit Klebstoff �ber die parametrierte Dauer bef�llen
					CleaningCollectorStep.eStepNr = eSTEP_START_FILL_COLLECTOR;
				}
			}
			break;
		
		// Warten bis der Trichterkopf mit dem Reinigen mit Reinigungsmittel fertig ist
		case eSTEP_WAIT_HOPPER_CLEANED:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_WAIT_HOPPER_CLEANED" );
			
			// Mit dem bef�llen des Klebekopfes am Sammelzylinder mit Klebstoff muss gewartet werden, bis der Klebekopf am Trichter mit
			// dem Reinigen mit Reinigungsmittel fertig ist. Dies ist n�tig, da es nur ein Ventil f�r alle Klebek�fe f�r die Umschaltung zwischen
			// Reinigungsmittel und Klebstoff gibt. Somit ist es nicht m�glich den einen Klebekopf mit Klebstoff und den anderen mit Reinigungs-
			// mittel gleichzeitig zu durchsp�len. Daher muss hier gepr�ft werden ob das Reinigen mit Reinigungsmittel nicht mehr aktiv ist.
			if( (gMainLogic.State.bCleaningWithDetergentActive == FALSE) &&
				(gExtCtrlIf.State.bBondingDeviceCleaned[0] == TRUE) )
			{
				// n�chster Schritt: Den Den Klebekopf am Sammelzylinder mit Klebstoff �ber die parametrierte Dauer bef�llen
				CleaningCollectorStep.eStepNr = eSTEP_START_FILL_COLLECTOR;
			}
			break;
		
		// Den Den Klebekopf am Sammelzylinder mit Klebstoff �ber die parametrierte Dauer bef�llen
		case eSTEP_START_FILL_COLLECTOR:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_START_FILL_COLLECTOR" );
			
			// Der Bef�llvorgang mit Leim darf erst beginnen, wenn keine Reinigungsfl�ssigkeit mehr gef�rdert wird
			if( gMainLogic.State.bCleaningWithDetergentActive == FALSE )
			{
				// Trichterkopf mit Klebstoff f�llen
				gMainLogic.DirectBox.bContCleanBonDevWithGlue[1] = TRUE;
								
				// Den Klebekopf am Sammelzylinder mit Klebstoff f�llen, bis die Bef�lldauer aus nCollectorFillingPeriod abgelaufen ist.
				BrbStartStepTimeout( &CleaningCollectorStepHandling, gPar.MachinePar.nCollectorFillingPeriod, eSTEP_FILL_COLLECTOR_DONE );
			}
			break;
		
		// Der Klebekopf am Sammelzylinder wurde �ber die parametrierte Dauer mit Klebstoff bef�llt
		case eSTEP_FILL_COLLECTOR_DONE:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_FILL_COLLECTOR_DONE" );
			
			// Trichterkopf nicht mehr mit Klebstoff f�llen
			gMainLogic.DirectBox.bContCleanBonDevWithGlue[1] = FALSE;
			
			// n�chster Schritt: Den Klebekopf am Sammelzylinder in horizontale Richtung zur Arbeitsposition verfahren
			CleaningCollectorStep.eStepNr = eSTEP_MOVE_HEAD_TO_WORK_POSITION;
			
			break;
			
		// Den Klebekopf am Sammelzylinder in horizontale Richtung zur Arbeitsposition verfahren
		case eSTEP_MOVE_HEAD_TO_WORK_POSITION:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_MOVE_HEAD_TO_WORK_POSITION" );
			
			// Pr�fen ob das Kommandofach frei ist
			if( BrbSetCaller( &gAxisControl.CallerBox.Caller, eCALLERID_EXT_CTRL_IF) == eBRB_CALLER_STATE_OK )
			{	
				gAxisControl.Par.HmiPar.AbsPos[1].Horizontal = gPar.ProductPar.BonDevPar[1].AxesPosition.Alignment[eHORIZONTAL].rWorkPosition;
				gAxisControl.CallerBox.HmiCaller.Horizontal[1].bStartAbsMove = TRUE;
				
				// n�chster Schritt: Warten bis der Klebekopf am Sammelzylinder an der Arbeitsposition angekommen ist
				CleaningCollectorStep.eStepNr = eSTEP_WAIT_HEAD_IN_WORK_POSITION;
			}
			break;
			
		// Warten bis der Klebekopf am Sammelzylinder an der Arbeitsposition angekommen ist
		case eSTEP_WAIT_HEAD_IN_WORK_POSITION:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_WAIT_HEAD_IN_WORK_POSITION" );
			
			// Die Achse hat die Arbeitsposition erreicht
			if( gAxisControl.State.AxisStatus[1].Horizontal.InPosition == TRUE )
			{
				// n�chster Schritt: Warten bis die Maku-Weiche am Klebekopf am Sammelzylinder geschlossen ist
				CleaningCollectorStep.eStepNr = eSTEP_WAIT_MAKU_TURN_OUT_CLOSED;
			}
			break;
			
		// Warten bis die Maku-Weiche am Klebekopf am Sammelzylinder geschlossen ist
		case eSTEP_WAIT_MAKU_TURN_OUT_CLOSED:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_WAIT_MAKU_TURN_OUT_CLOSED" );
			
			// Die Maku-Weiche ist geschlossen
			if( gExtCtrlIf.State.bMakuTurnOutClosed == TRUE )
			{				
				// Direkt-Kommando zum "Freischie�en" des Klebekopfes am Sammelzylinder absetzen
				gExtCtrlIf.DirectBox.bStartIntervalCleaning = TRUE;
			}
			// Der Klebekopf am Sammelzylinder darf wieder kleben
			gExtCtrlIf.State.bBondingNotAllowed[1] = FALSE;
			
			// Die Reinigung ist nicht mehr aktiv
			gExtCtrlIf.State.bCleaningActiveCollectorBonDev = FALSE;
			
			// n�chster Schritt: Warten bis System in "Automatik" und Kommando "K�pfe Reinigen" kommt
			CleaningCollectorStep.eStepNr = eSTEP_WAIT_CMD_CLEAN_COLLECTOR;
			break;
			
		// Schrittkette zur�cksetzen
		case eSTEP_RESET_COLLECTOR_CLEANING:
			strcpy( CleaningCollectorStep.sStepText, "eSTEP_RESET_COLLECTOR_CLEANING" );
			
			// Alle gesetzten Kommandos und TimeOuts zur�cksetzen
			gMainLogic.DirectBox.bContCleanBonDevWithDetergent[1] = FALSE;
			gMainLogic.DirectBox.bContCleanBonDevWithGlue[1] = FALSE;
			gMainLogic.DirectBox.bIntervalCleanBonDevWithGlue[1] = FALSE;
			BrbStopStepTimeout( &CleaningCollectorStepHandling );
			
			// Der Klebekopf am Sammelzylinder darf wieder kleben
			gExtCtrlIf.State.bBondingNotAllowed[1] = FALSE;
			
			// Der Reinigugnsvorgang mit Reinigungsmittel ist f�r den Klebekopf am Sammelzylinder nicht abgeschlossen
			gExtCtrlIf.State.bBondingDeviceCleaned[1] = FALSE;
			
			// Die Reinigung ist nicht mehr aktiv
			gExtCtrlIf.State.bCleaningActiveCollectorBonDev = FALSE;
			
			// n�chster Schritt: Warten bis System in "Automatik" und Kommando "K�pfe reinigen" kommt
			CleaningCollectorStep.eStepNr = eSTEP_WAIT_CMD_CLEAN_COLLECTOR;
			break;
	}
	
	
	// ---------- Freischie�en der Klebek�pfe nach schlie�en der Makuweiche ----------
	// Positive Flanke des Signals "Maku-Weiche geschlossen" ermitteln
	
	PosEdgeMakuTurnOutClosed.CLK = gExtCtrlIf.State.bMakuTurnOutClosed;
	R_TRIG( &PosEdgeMakuTurnOutClosed );
	
	// Auswerten ob Maku-Weiche geschlossen nur im Automatik-Betrieb und nicht
	// w�hrend der Reinigung vom Trichterkopf vor dem Sammelzylinder
	if( (gMainLogic.State.bAutomatic == TRUE) &&
		(gExtCtrlIf.State.bCleaningActiveCollectorBonDev == FALSE ) )
	{
		// Positive Flanke "Maku-Weiche geschlossen" wurde erkannt
		if( PosEdgeMakuTurnOutClosed.Q == TRUE )
		{
			// Intervall-Reinigung anfordern
			gExtCtrlIf.DirectBox.bStartIntervalCleaning = TRUE;
		}
	}
	
	
	
	// ------------------------- Direkt-Kommandos abarbeiten -------------------------
	// Intervall-Reinigung wurde angefordert
	if( gExtCtrlIf.DirectBox.bStartIntervalCleaning == TRUE )
	{
		// Eine Intervall-Reinigung vom Task ExtCrtlIf wird derzeit nicht abgearbeitet
		if( gExtCtrlIf.State.bIntervalCleaningActive == FALSE )
		{	
			// Kommando f�r das Reinigen an den Task MainLogic schicken
			gMainLogic.DirectBox.bIntervalCleanBonDevWithGlue[1] = TRUE;
			
			gExtCtrlIf.DirectBox.bStartIntervalCleaning = FALSE;
			 
			gExtCtrlIf.State.bIntervalCleaningActive = TRUE;
			
			BrbClearDirectBox( (UDINT)&gExtCtrlIf.DirectBox, sizeof( gExtCtrlIf.DirectBox ) );
		}
		// Die Intervall-Reinigung ist gerade aktiv. Der Befehl wird verworfen.
		else
		{
			// Kommando verwerfen
			gExtCtrlIf.DirectBox.bStartIntervalCleaning = FALSE;
		}
		
	}
	
	// Pr�fen wann die Intervall-Reinigung abgeschlossen ist
	if( gExtCtrlIf.State.bIntervalCleaningActive == TRUE )
	{
		if( gMainLogic.State.bCleaningActive == TRUE )
		{
			TON_IntervalCleaningTime.IN = TRUE;
			// L�nge der Intervall-Reinigung anhand der	parametrieren Schussanzahl berechnen
			nIntervarlCleaningTime = (35000 /* mm */ / (nCLEANING_VELOCITY /* 1 mm/ms */ * rFACTOR_MM_TO_AXIS_UNITS)) * gPar.MachinePar.nNrOfRounds;
			// berechnete Intervall-Reinigungsdauer an Funktion �bergeben
			TON_IntervalCleaningTime.PT = nIntervarlCleaningTime;
		}
		// Die Intervall-Reinigungsdauer wurde erreicht
		if( TON_IntervalCleaningTime.Q == TRUE )
		{
			gMainLogic.DirectBox.bIntervalCleanBonDevWithGlue[1] = FALSE;
			TON_IntervalCleaningTime.IN = FALSE;
			// Die Intervall-Reinigung ist nicht mehr aktiv
			gExtCtrlIf.State.bIntervalCleaningActive = FALSE;
		}
		
	}
	// Timer-Funktion aufrufen
	TON( &TON_IntervalCleaningTime );
	
	// -------------------------- Eing�nge als State setzen ---------------------------
	
	// Alle Klebek�pfe reinigen wurde angefordert
	gExtCtrlIf.State.bCleanBondingDevices = IO.Input.bCleanBondingDevices;
	// Klebekopf vor dem Sammelzylinder mit Klebstoff bef�llen wurde angefordert
	gExtCtrlIf.State.bFillCollectorBondigDevice = IO.Input.bFillCollectorBondigDevice;
	// Trichterkopf mit Klebstoff bef�llen wurde angefordert
	gExtCtrlIf.State.bFillHopperBondigDevice = IO.Input.bFillHopperBondigDevice;
	// Die Maku-Weiche ist geschlossen -> Produkte werden nicht mehr ausgeschleust
	gExtCtrlIf.State.bMakuTurnOutClosed = IO.Input.bMakuTurnOutClosed;
	
	// Ermitteln ob das Kleben �ber die Schnittstelle freigegeben ist
	for( nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++ )
	{
		// Klebek�pfe Trichterkopf und vor Sammelzylinder werden bei Reinigung gesondert behandelt
		if( ((nDevIndex == 0) && (gExtCtrlIf.State.bCleaningActiveHopperBonDev == TRUE)) ||
			((nDevIndex == 1) && (gExtCtrlIf.State.bCleaningActiveCollectorBonDev == TRUE)) )
		{
			// hier keine Abarbeitung
		}
		else
		{
			// State ob geklebt werden darf anhand der Schnittstellensignale ermitteln
			gExtCtrlIf.State.bBondingNotAllowed[nDevIndex] = !(IO.Input.bBondingAllowed[nDevIndex]);
		}
	}
	
	// ------------------------------- Ausg�nge setzen --------------------------------
	// Sammelst�rung �ber die Schnittstelle ausgeben
	if( gEventHandling.State.nErrorCount > 0 )
	{
		IO.Output.bGeneralError = TRUE;
	}
	else
	{
		IO.Output.bGeneralError = FALSE;
	}
}

void _EXIT ExtCtrlIfEXIT(void)
{
}
