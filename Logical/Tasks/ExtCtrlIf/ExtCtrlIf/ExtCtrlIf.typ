(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: ExtCtrlIf
 * File: ExtCtrlIf.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Local data types of program ExtCtrlIf
 ********************************************************************)

TYPE
	CleaningCollectorSteps_ENUM : 
		( (*Schritte*)
		eSTEP_WAIT_CMD_CLEAN_COLLECTOR := 100, (*Warten bis System in "Automatik" und Kommando "K�pfe Reinigen" kommt*)
		eSTEP_MOVE_HEAD_TO_CLEAN_STATION, (*Den Klebekopf am Sammelzylinder in horizontale Richtung zur Reinigungsstation verfahren*)
		eSTEP_WAIT_HEAD_TO_CLEAN_STATION, (*Warten bis der Klebekopf am Sammelzylinder an der Reinigungsstation angekommen ist.*)
		eSTEP_START_CLEAN_COLLECTOR, (*Den Klebekopf am Sammelzylinder mit Reinigungsfl�ssigkeit kontinuierlich sp�len*)
		eSTEP_CLEAN_COLLECTOR_DONE, (*Der Klebekopf am Sammelzylinder wurde �ber die parametrierte Dauer mit Reinigungsfl�ssigkeit gesp�lt.*)
		eSTEP_WAIT_CMD_FILL_COLLECTOR, (*Warten bis das Kommando "Klebekopf am Sammelzylinder bef�llen" ansteht*)
		eSTEP_WAIT_HOPPER_CLEANED,
		eSTEP_START_FILL_COLLECTOR, (*Den Klebekopf am Sammelzylinder mit Klebstoff �ber die parametrierte Dauer bef�llen*)
		eSTEP_FILL_COLLECTOR_DONE, (*Der Klebekopf am Sammelzylinder wurde �ber die parametrierte Dauer mit Klebstoff bef�llt*)
		eSTEP_MOVE_HEAD_TO_WORK_POSITION, (*Den Klebekopf am Sammelzylinder in horizontale Richtung zur Arbeitsposition verfahren*)
		eSTEP_WAIT_HEAD_IN_WORK_POSITION, (*Warten bis der Klebekopf am Sammelzylinder an der Arbeitsposition angekommen ist.*)
		eSTEP_WAIT_MAKU_TURN_OUT_CLOSED, (*Warten bis die Maku-Weiche am Klebekopf am Sammelzylinder geschlossen ist.*)
		eSTEP_RESET_COLLECTOR_CLEANING := 200 (*Schrittkette zur�cksetzen*)
		);
	CleaningCollectorStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : CleaningCollectorSteps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	CleaningHopperSteps_ENUM : 
		( (*Schritte*)
		eSTEP_WAIT_CMD_CLEAN_HOPPER := 100, (*Warten bis System in "Automatik" und Kommando "K�pfe Reinigen" kommt*)
		eSTEP_START_CLEAN_HOPPER, (*Den Trichterkopf mit Reinigungsfl�ssigkeit kontinuierlich sp�len*)
		eSTEP_CLEAN_HOPPER_DONE, (*Der Trichterkopf wurde �ber die parametrierte Dauer mit Reinigungsfl�ssigkeit gesp�lt.*)
		eSTEP_WAIT_CMD_FILL_HOPPER, (*Warten bis das Kommando "Trichterkopf bef�llen" ansteht*)
		eSTEP_WAIT_COLLECTOR_CLEANED,
		eSTEP_START_FILL_HOPPER, (*Den Trichterkopf mit Klebstoff �ber die parametrierte Dauer bef�llen*)
		eSTEP_FILL_HOPPER_DONE, (*Der Trichterkopf wurde �ber die parametrierte Dauer mit Klebstoff bef�llt*)
		eSTEP_RESET_HOPPER_CLEANING := 200 (*Schrittkette zur�cksetzen*)
		);
	CleaningHopperStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : CleaningHopperSteps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	ExtCtrlIfStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	IoInput_TYP : 	STRUCT 
		bBondingAllowed : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Das Kleben mit dem jeweiligen Klebekopf wird �ber den digitalen Eingang freigegeben.*)
		bCleanBondingDevices : BOOL; (*Das Reinigen aller Klebek�pfe wird �ber den digitalen Eingang angefordert.*)
		bFillHopperBondigDevice : BOOL; (*Der Klebekopf am Trichter soll mit Klebstoff bef�llt werden. Dies wird �ber den digitalen Eingang angefordert.*)
		bFillCollectorBondigDevice : BOOL; (*Der Klebekopf am Sammelzylinder soll mit Klebstoff bef�llt werden. Dies wird �ber den digitalen Eingang angefordert.*)
		bMakuTurnOutClosed : BOOL; (*Die Maku-Weiche ist geschlossen. Erkannt wird dies �ber Sensorik.*)
	END_STRUCT;
	IoOutput_TYP : 	STRUCT 
		bGeneralError : BOOL; (*Sammelst�rung: Mindestens ein Fehler liegt an. Dies wird �ber den digitalen Ausgang gemeldet.*)
	END_STRUCT;
	IO_TYP : 	STRUCT 
		Output : IoOutput_TYP;
		Input : IoInput_TYP;
	END_STRUCT;
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_CMD1 := 200,
		eSTEP_CMD1_FINISHED
		);
END_TYPE
