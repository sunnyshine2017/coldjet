(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: PressureCtrl
 * File: PressureCtrl.typ
 * Author: kusculart
 * Created: November 28, 2014
 ********************************************************************
 * Local data types of program PressureCtrl
 ********************************************************************)

TYPE
	Steps_enum : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_CMD1 := 200,
		eSTEP_CMD1_FINISHED
		);
	PressureCtrlStep_type : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_enum;
		bInitDone : BOOL;
	END_STRUCT;
	PressureCtrl_type : 	STRUCT 
		fbLevelController : MTBasicsLevelController; (*MTBasicsLevelController*)
		FubPar : FubPar_type;
		rGradient : LREAL; (*Steigung der Geraden f�r die Ermittlung des geschwindigkeitsabh�ngigen Solldrucks*)
		rAdaptedPressure : REAL; (*Angepasster Druck (Geschwindigkeitsabh�ngiger Solldruck) in [bar]*)
		rFubPressure : REAL;
		bPressureCtrlErr : BOOL; (*Fehler ist aufgetreten*)
	END_STRUCT;
	IO_type : 	STRUCT 
		Input : Input_type;
		Output : Output_type;
	END_STRUCT;
	Output_type : 	STRUCT 
		bPressureDecrease : BOOL; (*Druck verringern wenn dieser Ausgang auf FALSE gesetzt wird. Zum Halten des aktuellen Drucks muss dieser Ausgang auf TRUE sein.*)
		bPressureIncrease : BOOL; (*Druck erh�hen wenn der Ausgang auf TRUE steht. Um den aktuellen Druck zu halten, muss dieser Ausgang auf FALSE stehen.*)
		bPressureCtrlMoveGlue : BOOL; (*Klebstoff f�rdern um den gew�nschten Druck der Druckregelung zu erreichen, bzw. um bei Reinigung mit Leim zu f�rdern und bei Reinigung mit Reinigungsmittel nicht zu f�rdern. Bei TRUE wird Klebstoff gef�rdert.*)
		bPressureCtrlVent : BOOL; (*Ventil zum Verringern des Drucks (entl�ften) ansteuern, um den gew�nschten Druck der Druckregelung zu erreichen, bzw. um bei Reinigung mit Leim zu f�rdern und bei Reinigung mit Reinigungsmittel nicht zu f�rdern. Bei FALSE wird die Leitung entl�ftet. Um den Druck zu halten bzw. mit bPressureCtrlMoveGlue zu erh�hen, muss dieser Ausgang auf TRUE stehen.*)
		bOpenBypassValve : BOOL; (*Bypass-Ventil um den bei einem Schnellstopp vorhandenen �berdruck in den Schl�uchen abzubauen.*)
	END_STRUCT;
	Input_type : 	STRUCT 
		nActPressure : INT; (*Aktueller Druck von 0 V bis 10 V*)
	END_STRUCT;
	FubPar_type : 	STRUCT 
		PressureParOld : PressureControl_TYP;
		nCmpResult : DINT;
		rFubPressureOld : REAL;
		rDiffFubPressure : REAL;
		bUpdatePar : BOOL;
	END_STRUCT;
	ActPressure_type : 	STRUCT 
		rGradient : LREAL;
		rMaxPressure : REAL; (*Maximaler Druck in Bar*)
		rMinPressure : REAL; (*Minimaler Druck in Bar*)
	END_STRUCT;
	NegEdge_TYP : 	STRUCT 
		EncVelOfEnabledBonDevNotZero : F_TRIG;
	END_STRUCT;
END_TYPE
