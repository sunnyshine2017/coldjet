/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: PressureCtrl
 * File: PressureCtrl.c
 * Author: niedermeierr
 * Created: November 28, 2014
 ********************************************************************
 * Implementation of program PressureCtrl
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <PressureCtrlFunc.h>

void _INIT PressureCtrlINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gPressureCtrl, 0, sizeof(gPressureCtrl));
	gPressureCtrl.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	IO = IO;
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC PressureCtrlCYCLIC(void)
{	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			//if(gParHandling.State.bParValid == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gPressureCtrl.CallerBox, sizeof(gPressureCtrl.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");

			
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_CMD1:
			strcpy(Step.sStepText, "eSTEP_CMD1");
			Step.eStepNr = eSTEP_CMD1_FINISHED;
			break;

		case eSTEP_CMD1_FINISHED:
			strcpy(Step.sStepText, "eSTEP_CMD1_FINISHED");
			BrbClearCallerBox((UDINT)&gPressureCtrl.CallerBox, sizeof(gPressureCtrl.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;
	}

	// Istdruck ermitteln
	ActPressure.rMaxPressure = (REAL)nMAX_PRESSURE;
	ActPressure.rMinPressure = 0;
	ActPressure.rGradient = (ActPressure.rMaxPressure - ActPressure.rMinPressure) / (nINT_FFFF);
	gPressureCtrl.State.rActPressure = ActPressure.rGradient * IO.Input.nActPressure;
	PressureCtrl.fbLevelController.In = gPressureCtrl.State.rActPressure;
	
	// Pr�fen ob ein Update der Parameter f�r den 3 Punktregler notwendig ist
	PressureCtrl.FubPar.nCmpResult = brsmemcmp((UDINT)&gPar.MachinePar.PressureControl.PreCtrlBasis,(UDINT)&PressureCtrl.FubPar.PressureParOld,
		sizeof(gPar.MachinePar.PressureControl.PreCtrlBasis));
	PressureCtrl.FubPar.rDiffFubPressure = PressureCtrl.FubPar.rFubPressureOld - PressureCtrl.rFubPressure;
	if ((PressureCtrl.FubPar.rDiffFubPressure > 0.05) ||
		(PressureCtrl.FubPar.rDiffFubPressure < -0.05) ||
		(PressureCtrl.FubPar.nCmpResult != 0))
	{
		PressureCtrl.FubPar.bUpdatePar = 1;
	}
	
	
	// Steigung ermitteln
	if (gPar.MachinePar.PressureControl.PreCtrlBasis.rMaxVelocitiy > 0)
	{
		PressureCtrl.rGradient = (gPar.MachinePar.PressureControl.PreCtrlBasis.rMaxPressure - gPar.MachinePar.PressureControl.PreCtrlBasis.rBasePressure) / 
			(gPar.MachinePar.PressureControl.PreCtrlBasis.rMaxVelocitiy * nFACTOR_M_TO_MM * rFACTOR_MM_TO_AXIS_UNITS);	
	}
	// Geschwindigkeitsabh�ngigen Solldruck ermitteln
	PressureCtrl.rAdaptedPressure = (PressureCtrl.rGradient * gEncoder.State.rVelocity[gPar.MachinePar.PressureControl.PreCtrlBasis.nEncoderIdx]) 
		+ gPar.MachinePar.PressureControl.PreCtrlBasis.rBasePressure;
	// Angepassten Druck begrenzen
	if (PressureCtrl.rAdaptedPressure > gPar.MachinePar.PressureControl.PreCtrlBasis.rMaxPressure)
	{
		PressureCtrl.rAdaptedPressure = gPar.MachinePar.PressureControl.PreCtrlBasis.rMaxPressure;
	}
	// Korrektrufaktor begrenzen
	if (gPressureCtrl.Par.rPressureCorr > (gPar.MachinePar.PressureControl.PreCtrlBasis.rMaxPressure - PressureCtrl.rAdaptedPressure))
	{
		gPressureCtrl.Par.rPressureCorr = (gPar.MachinePar.PressureControl.PreCtrlBasis.rMaxPressure - PressureCtrl.rAdaptedPressure);
	}
	
	// ----------------------------------------------------------------------------------------------
	// Ermitteln ob die Geschwindigkeit aller reelen Geber der produzierenden Klebek�pfe auf Null ist
	// ----------------------------------------------------------------------------------------------
	// Hilfsvariable zur�cksetzen
	bEncVelOfEnabledBonDevNotZero = FALSE;
	
	// Schleife �ber alle konfigurierten Klebek�pfe
	for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
	{
		// Dieser Klebekopf ist in Produktion (Enabled)
		if( gPar.ProductPar.BonDevPar[nBonDevIdx].bBonDevEn == TRUE )
		{	
			// Der diesem Klebekopf zugeordnete Geber ist kein simulierter Geber (Simuliert = Idx 0)
			if( gPar.ProductPar.BonDevPar[nBonDevIdx].nIdxEncoder >= 1 )
			{
				// Die erfasste Geschwindigkeit dieses Gebers ist nicht Null
				if( gEncoder.State.rVelocity[gPar.ProductPar.BonDevPar[nBonDevIdx].nIdxEncoder] >= gPar.MachinePar.rEnVelocity * rFACTOR_MM_TO_AXIS_UNITS )
				{
					bEncVelOfEnabledBonDevNotZero = TRUE;
					// Schleife hier verlassen
					break;
				}
			}
		}
	}
	
	// Ist eine Beipass-Zeit konfiguriert, ist ein Beipass-Ventil zur Druckreduzierung in den Leitungen vorhanden
	if( gPar.MachinePar.PressureControl.PreCtrlBasis.rBypassOpenTime != 0.0 )
	{
		// Auswertung der fallenden Flanke um den Stillstand aller relevanten Geber zu ermitteln
		NegEdge.EncVelOfEnabledBonDevNotZero.CLK = bEncVelOfEnabledBonDevNotZero;
		F_TRIG( &NegEdge.EncVelOfEnabledBonDevNotZero );
		// Die Geschwindigkeit der Geber aller f�r die Produktion aktivierten Klebek�pfe ist Null
		if( NegEdge.EncVelOfEnabledBonDevNotZero.Q == TRUE )
		{
			// Es liegt ein �berdruck in den Schl�uchen vor
			if( gPressureCtrl.State.rActPressure > (PressureCtrl.rFubPressure + gPar.MachinePar.PressureControl.PreCtrlBasis.rHysteresis) )
			{
				if( gMainLogic.State.bAutomatic == TRUE )
				{
					// Direkt-Kommando "Druck �ber Bypass-Venil reduzieren" setzen
					gPressureCtrl.DirectBox.bReducePressureByOpenBypass = TRUE;
				}
			}
		}
	}
	
	// Geschwindigkeitsabh�ngige Druckregelung ist konfiguriert und fehlerfrei
	if ((gParHandling.State.bMParValid == 1) &&
		(PressureCtrl.bPressureCtrlErr == 0) &&
		(gPar.MachinePar.MachineModule.bPressureCtrlEn == 1) )
	{
		// Automatikbetrieb ist aktiv
		if( (gMainLogic.State.bAutomatic == TRUE) ||
			(gMainLogic.State.bCleaningWithPressureCtrlActive == TRUE) )
		{

			PressureCtrl.rFubPressure = PressureCtrl.rAdaptedPressure + gPressureCtrl.Par.rPressureCorr;
		
			// FUB f�r 3 Punktregelung aufrufen
			PressureCtrl.fbLevelController.Enable = 1;
			PressureCtrl.fbLevelController.Parameter.OutputValue1 = -1;
			PressureCtrl.fbLevelController.Parameter.OutputValue2 = 0;
			PressureCtrl.fbLevelController.Parameter.OutputValue3 = 1;
			// Schaltschwelle begrenzen
			if (PressureCtrl.rFubPressure + gPar.MachinePar.PressureControl.PreCtrlBasis.rHysteresis > gPar.MachinePar.PressureControl.
				PreCtrlBasis.rMaxPressure)
			{
				PressureCtrl.fbLevelController.Parameter.ThresholdLevel1 = gPar.MachinePar.PressureControl.PreCtrlBasis.rMaxPressure;
			}
			else
			{
				PressureCtrl.fbLevelController.Parameter.ThresholdLevel1 = PressureCtrl.rFubPressure + gPar.MachinePar.PressureControl.
					PreCtrlBasis.rHysteresis;
			}
			PressureCtrl.fbLevelController.Parameter.ThresholdLevel2 = PressureCtrl.rFubPressure;
			PressureCtrl.fbLevelController.Parameter.ThresholdLevel3 = PressureCtrl.rFubPressure;
			PressureCtrl.fbLevelController.Parameter.ThresholdLevel4 = PressureCtrl.rFubPressure - gPar.MachinePar.PressureControl.
				PreCtrlBasis.rHysteresis;				
			// Parameter updaten
			if ((PressureCtrl.fbLevelController.UpdateDone == 0) &&
				(PressureCtrl.fbLevelController.Update == 0) &&
				(PressureCtrl.FubPar.bUpdatePar == 1))			
			{
				PressureCtrl.fbLevelController.Update = 1;								// Neu ermittelte Parameter �ernehmen
				brsmemcpy((UDINT)&PressureCtrl.FubPar.PressureParOld, (UDINT)&gPar.MachinePar.PressureControl, sizeof(PressureCtrl.FubPar.
					PressureParOld));
				PressureCtrl.FubPar.rFubPressureOld = PressureCtrl.rFubPressure;
				PressureCtrl.FubPar.bUpdatePar = 0;
			}
			MTBasicsLevelController(&PressureCtrl.fbLevelController);
			if (PressureCtrl.fbLevelController.UpdateDone == 1)
			{
				PressureCtrl.fbLevelController.Update = 0;
			}
			gPressureCtrl.State.bPressureCtrlActive = 1;
			// --------------------------- Ausgang ansteuern ---------------------------
			// !!!! Wichtig !!!!
			// 03.08.2015; Michael Zimmer
			// Ab dem System manroland Digitaldruckmaschine wird aus Sicherheitsgr�nden die Ansteuerung des
			// Ventils zur Druckverringerung invertiert. D.h zum Verringern des Drucks wird der Ausgang auf FAlSE
			// (nicht wie bisher auf TRUE) gesetzt. Zum Halten des aktuellen Drucks muss dieser Ausgang auf TRUE
			// stehen. Da im Feld keine Systeme mit aktiver Druckregelung laufen, wird das eine Testsystem bei
			// Planatol auch dementsprechend angepasst.
			
			if( gPressureCtrl.DirectBox.bReducePressureByOpenBypass == FALSE )
			{
				// Druck muss erh�ht werden
				if (PressureCtrl.fbLevelController.Out > 0)
				{
					IO.Output.bPressureDecrease = TRUE;
					IO.Output.bPressureIncrease = TRUE;
				}
				// Druck muss verringert werden
				else if (PressureCtrl.fbLevelController.Out < 0)
				{
					IO.Output.bPressureDecrease = FALSE;
					IO.Output.bPressureIncrease = FALSE;
				}
				// Aktuellen Druck halten
				else
				{
					IO.Output.bPressureDecrease = TRUE;
					IO.Output.bPressureIncrease = FALSE;
				}
			}
			else
			{
				// Aktuellen Druck halten
				IO.Output.bPressureDecrease = TRUE;
				IO.Output.bPressureIncrease = FALSE;
			}
			
			
			// Fehler ist aufgetreten
			if (PressureCtrl.fbLevelController.Error == 1)
			{
				// Meldung absetzen
				AppSetEvent(eEVT_ERR_PRESSURE_CONTROL, PressureCtrl.fbLevelController.StatusID, "", 0, &gEventManagement);
				AppResetEvent(eEVT_ERR_PRESSURE_CONTROL, &gEventManagement);
				
				PressureCtrl.bPressureCtrlErr = 1;				// Fehlerbit setzen
				
				PressureCtrl.fbLevelController.Enable = 0;
				PressureCtrl.fbLevelController.In = 0;
				brsmemset((UDINT)&PressureCtrl.fbLevelController.Parameter, 0, sizeof(PressureCtrl.fbLevelController.Parameter));
				MTBasicsLevelController(&PressureCtrl.fbLevelController);
				gPressureCtrl.State.bPressureCtrlActive = 0;			
			}
		}
		else
		{
			PressureCtrl.fbLevelController.Enable = 0;
			PressureCtrl.fbLevelController.In = 0;
			brsmemset((UDINT)&PressureCtrl.fbLevelController.Parameter, 0, sizeof(PressureCtrl.fbLevelController.Parameter));
			MTBasicsLevelController(&PressureCtrl.fbLevelController);
			gPressureCtrl.State.bPressureCtrlActive = 0;
			IO.Output.bPressureDecrease = 0;
			IO.Output.bPressureIncrease = 0;
			gPressureCtrl.Par.rPressureCorr = 0;
		}
		
		// Direkt-Kommando zur Reduzierung des Drucks in den Leitungen wurde angefordert
		if( gPressureCtrl.DirectBox.bReducePressureByOpenBypass == TRUE )
		{
			// Ventil zur Druckreduzierung �ffnen
			IO.Output.bOpenBypassValve = TRUE;
			// Start der konfigurierten �ffnungszeit des Ventils
			TON_CloseBypassValve.IN = TRUE;
			TON_CloseBypassValve.PT = gPar.MachinePar.PressureControl.PreCtrlBasis.rBypassOpenTime * 1000 /*ms*/;
			// Kommando zur�cksetzen
			gPressureCtrl.DirectBox.bReducePressureByOpenBypass = FALSE;
		}
		
		// Funktionsblock aufrufen
		TON( &TON_CloseBypassValve );
		// Beipass-Ventil war lange genug ge�ffnet
		if( TON_CloseBypassValve.Q == TRUE )
		{
			// Ventil zur Druckreduzierung schlie�en
			IO.Output.bOpenBypassValve = FALSE;
			// Funktionsblock zur�cksetzen
			TON_CloseBypassValve.IN = FALSE;
		}
		
		// Sonderfall Reinigung: 
		if( gMainLogic.State.bCleaningActive == TRUE )
		{
			// Keinen Klebstoff f�rdern und die Luftleitung entl�ften, da Reinigung mit Reinigungsmittel gefordert ist
			if( (gMainLogic.State.bCleaningWithDetergentActive == TRUE) ||
				(gMainLogic.State.bDetergentValveOpen == TRUE) )
			{
				IO.Output.bPressureCtrlMoveGlue = FALSE;
				IO.Output.bPressureCtrlVent = FALSE;
			}
			// Klebstoff mittels aktiver Druckregelung f�rdern, da dies gefordert ist
			else if( gMainLogic.State.bCleaningWithPressureCtrlActive == TRUE )
			{
				IO.Output.bPressureCtrlMoveGlue = IO.Output.bPressureIncrease;
				IO.Output.bPressureCtrlVent = IO.Output.bPressureDecrease;
			}
			// Klebstoff f�rdern, da Reinigung mit Klebstoff ohne Druckregelung gefordert ist
			else if( (gMainLogic.State.bCleaningWithGlueActive == TRUE) ||
					 (gMainLogic.State.bGlueValveOpen == TRUE) )
			{
				IO.Output.bPressureCtrlMoveGlue = TRUE;
				IO.Output.bPressureCtrlVent = TRUE;
			}
			
		}
		// Sonderfall Systemreinigung:
		else if( gMainLogic.State.bSystemCleaningActive == TRUE )
		{
			// Keinen Klebstoff f�rdern und die Luftleitung entl�ften, da die System-Reinigung mit Reinigungsmittel gefordert ist
			if( gMainLogic.State.bDetergentValveOpen == TRUE )
			{
				IO.Output.bPressureCtrlMoveGlue = FALSE;
				IO.Output.bPressureCtrlVent = FALSE;
			}
			// Klebstoff f�rdern, da System-Reinigung mit Klebstoff gefordert ist
			else if( gMainLogic.State.bGlueValveOpen == TRUE )
			{
				IO.Output.bPressureCtrlMoveGlue = TRUE;
				IO.Output.bPressureCtrlVent = TRUE;
			}
		}
		// Normalbetrieb: Werte der Druckregelung bei Automatikbetrieb �bernehmen
		else
		{
			IO.Output.bPressureCtrlMoveGlue = IO.Output.bPressureIncrease;
			IO.Output.bPressureCtrlVent = IO.Output.bPressureDecrease;
		}
	}
	else
	{
		PressureCtrl.fbLevelController.Enable = 0;
		PressureCtrl.fbLevelController.In = 0;
		brsmemset((UDINT)&PressureCtrl.fbLevelController.Parameter, 0, sizeof(PressureCtrl.fbLevelController.Parameter));
		MTBasicsLevelController(&PressureCtrl.fbLevelController);
		gPressureCtrl.State.bPressureCtrlActive = 0;
		IO.Output.bPressureDecrease = 0;
		IO.Output.bPressureIncrease = 0;
		gPressureCtrl.Par.rPressureCorr = 0;
	}
	
	// Fehler wurde quittiert
	if( AppIsEventAcknowledged(eEVT_ERR_PRESSURE_CONTROL, &gEventManagement) == 1 )
	{
		PressureCtrl.bPressureCtrlErr = 0;
	}
}

void _EXIT PressureCtrlEXIT(void)
{
}
