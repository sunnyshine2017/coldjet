(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: PressureCtrl
 * File: G_PressureCtrl.typ
 * Author: kusculart
 * Created: November 28, 2014
 ********************************************************************
 * Global  data types of package PressureCtrl
 ********************************************************************)

TYPE
	PressureCtrlCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
	END_STRUCT;
	PressureCtrlDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bReducePressureByOpenBypass : BOOL; (*Der Druck in den Leitungen soll durch �ffnen des Bypass-Ventils reduziert werden. Die Zeit wie lange das Bypass-Ventil ge�ffnet ist, wird in der Konfiguration �ber die Visu definiert.*)
	END_STRUCT;
	PressureCtrlPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		rPressureCorr : REAL; (*Korrekturdruck in [bar]*)
	END_STRUCT;
	PressureCtrlState_TYP : 	STRUCT  (*R�ckmeldungen*)
		rActPressure : REAL; (*Istdruck in bar*)
		bPressureCtrlActive : BOOL;
	END_STRUCT;
	PressureCtrl_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : PressureCtrlCallerBox_TYP;
		DirectBox : PressureCtrlDirectBox_TYP;
		Par : PressureCtrlPar_TYP;
		State : PressureCtrlState_TYP;
	END_STRUCT;
END_TYPE
