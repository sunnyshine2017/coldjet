(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: CalcPos
 * File: G_CalcPos.typ
 * Author: kusculart
 * Created: Oktober 14, 2014
 ********************************************************************
 * Global  data types of package CalcPos
 ********************************************************************)

TYPE
	CalcPosCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bDummy : BOOL;
	END_STRUCT;
	CalcPosDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bDummy : BOOL;
	END_STRUCT;
	CalcPosPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		nDummy : UINT;
	END_STRUCT;
	CalcPosState_TYP : 	STRUCT  (*Rückmeldungen*)
		HeadState : ARRAY[0..nDEV_INDEX_MAX]OF HeadState_TYP;
		bInvalidParEntry : BOOL;
	END_STRUCT;
	CalcPos_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : CalcPosCallerBox_TYP;
		DirectBox : CalcPosDirectBox_TYP;
		Par : CalcPosPar_TYP;
		State : CalcPosState_TYP;
	END_STRUCT;
	HeadState_TYP : 	STRUCT 
		TrackState : ARRAY[0..nIDX_PRINT_MARK_MAX]OF TrackState_TYP;
		nInvalidParIdx : ARRAY[0..nIDX_PRINT_MARK_MAX]OF SINT; (*Index des ungültigen Parameters*)
		bInvalidProductLen : BOOL; (*Ungültige Produktlänge*)
		bHeadError : BOOL; (*Fehler Auftragekopf*)
	END_STRUCT;
	TrackState_TYP : 	STRUCT 
		rCalcTrackLen : REAL; (*Errechnete Produktlänge in mm*)
		bInvalidTrackLen : BOOL; (*Ungültige Track Länge*)
	END_STRUCT;
END_TYPE
