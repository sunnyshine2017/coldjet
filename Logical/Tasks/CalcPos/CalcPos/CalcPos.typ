(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: CalcPos
 * File: CalcPos.typ
 * Author: kusculart
 * Created: Oktober 14, 2014
 ********************************************************************
 * Local data types of program CalcPos
 ********************************************************************)

TYPE
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100
		);
	CalcPosStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	InvalidPar_TYP : 	STRUCT 
		bInvalidParEntry : ARRAY[0..nIDX_PRINT_MARK_MAX,0..nIDX_SWITCHES_MAX]OF BOOL;
		rProductLen : REAL;
	END_STRUCT;
END_TYPE
