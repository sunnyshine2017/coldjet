/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: CalcPos
 * File: CalcPos.c
 * Author: kusculart
 * Created: Oktober 14, 2014
 ********************************************************************
 * Implementation of program CalcPos
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <CalcPosFunc.h>

void _INIT CalcPosINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gCalcPos, 0, sizeof(gCalcPos));
	gCalcPos.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	bStartCalc = 1;
	gCalcPos.State.bInvalidParEntry = 0;
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC CalcPosCYCLIC(void)
{

	// ---------- Direct-Kommandos ----------
	if(Step.bInitDone == 1)
	{
		if(gCalcPos.DirectBox.bDummy == 1)
		{
			BrbClearDirectBox((UDINT)&gCalcPos.DirectBox, sizeof(gCalcPos.DirectBox));
		}
	}
	
	// ---------- StepHandling ----------
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	
	// ---------- Schrittkette ----------
	switch(Step.eStepNr)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			
			if(gParHandling.State.bInitDone == 1)
			{	
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gCalcPos.CallerBox, sizeof(gCalcPos.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			
			break;
	}
		
	// Falls Auftragekopf aktiv geschaltet wird, Positionen auf G�ltigkeit pr�fen
	bEdgePos = 0;
	for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
	{
		fbEdge[nDevIndex].CLK = gPar.ProductPar.BonDevPar[nDevIndex].bBonDevEn;
		RF_TRIG(&fbEdge[nDevIndex]);
		bEdgePos = bEdgePos || fbEdge[nDevIndex].Q;
	} 
	
	// ---------- Parameter haben sich ge�ndert oder ein Fehler liegt vor --> Berechnung starten ----------
	if ((gParHandling.State.bAction == 1) ||
		(gParHandling.State.bParChanged == 1) ||
		(gCalcPos.State.bInvalidParEntry == 1) ||
		(gCalcPos.State.HeadState[0].bHeadError == 1) || 
		(gCalcPos.State.HeadState[1].bHeadError == 1) ||
		(gCalcPos.State.HeadState[2].bHeadError == 1) ||
		(gCalcPos.State.HeadState[3].bHeadError == 1) ||
		(gCalcPos.State.HeadState[4].bHeadError == 1) ||
		(gCalcPos.State.HeadState[5].bHeadError == 1) ||
		(gCalcPos.State.HeadState[6].bHeadError == 1) ||
		(gCalcPos.State.HeadState[7].bHeadError == 1) ||
		(gCalcPos.State.HeadState[8].bHeadError == 1) ||
		(gCalcPos.State.HeadState[9].bHeadError == 1) ||
		(gCalcPos.State.HeadState[10].bHeadError == 1) ||
		(gCalcPos.State.HeadState[11].bHeadError == 1) ||
		(bEdgePos == 1))
	{
		bStartCalc = 1;
	}
	
	if (bStartCalc == 1)
	{
		brsmemset((UDINT)&rLastSwitchPos, 0, sizeof(rLastSwitchPos));
		brsmemset((UDINT)&InvalidPar, 0, sizeof(InvalidPar));
		for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
		{
			brsmemset((UDINT)&gCalcPos.State.HeadState[nDevIndex].TrackState, 0, sizeof(gCalcPos.State.HeadState[nDevIndex].TrackState));
			brsmemset((UDINT)&gCalcPos.State.HeadState[nDevIndex].nInvalidParIdx, -1, sizeof(gCalcPos.State.HeadState[nDevIndex].nInvalidParIdx));
			gCalcPos.State.HeadState[nDevIndex].bInvalidProductLen = 0;
			gCalcPos.State.HeadState[nDevIndex].bHeadError = 0;
		}
		gCalcPos.State.bInvalidParEntry = 0;
	}

	// ---------- Nockenpositionen berechnen ----------
	if (gParHandling.State.bPParValid == 1)
	{
		for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
		{
			// Produktl�nge ermitteln
			if (gPar.ProductPar.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER)
			{
				InvalidPar[nDevIndex].rProductLen = gPar.ProductPar.BonDevPar[nDevIndex].rCylinderPerimeter;
			}
			else
			{
				InvalidPar[nDevIndex].rProductLen = gPar.ProductPar.BonDevPar[nDevIndex].rProductLen;
			}
				
			if (bStartCalc == 1)
			{
				for (nPmIndex = 0; nPmIndex <= nIDX_PRINT_MARK_MAX; nPmIndex++)
				{
					if( (gPar.ProductPar.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR) ||
						(gPar.ProductPar.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) ||
						(((gPar.ProductPar.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER) ||
						(gPar.ProductPar.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR)) &&
						(nPmIndex == 0)) )
					{
						if ((InvalidPar[nDevIndex].rProductLen < rMIN_PRODUCT_LEN) ||					// Minimale Produktl�nge �berschritten
							(InvalidPar[nDevIndex].rProductLen > rMAX_PRODUCT_LEN))						// Maximale Produktl�nge �berschritten)
						{
							InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][0] = 1;
							gCalcPos.State.HeadState[nDevIndex].bHeadError = 1;
							gCalcPos.State.HeadState[nDevIndex].bInvalidProductLen = 1;				
						}
						if ((gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rPause[0] == 0) &&
							(gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rStitchLen[0] == 0))									// Das erste Wertepaar darf nicht Null sein		
						{
							InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][0] = 1;
							gCalcPos.State.HeadState[nDevIndex].bHeadError = 1;
							gCalcPos.State.HeadState[nDevIndex].nInvalidParIdx[nPmIndex] = 0;
						}
						gCalcPos.State.bInvalidParEntry = (gCalcPos.State.bInvalidParEntry || InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][0]);	
						gCalcPos.State.HeadState[nDevIndex].TrackState[nPmIndex].rCalcTrackLen = 0;														// Errechnete Produktl�nge abl�schen

						for (nSwitchIndex = 0; nSwitchIndex <= gPar.ProductPar.BonDevPar[nDevIndex].nSwitchIndexMax[nPmIndex]; nSwitchIndex++)
						{
							if ((gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rPause[nSwitchIndex] != 0) ||
								(gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rStitchLen[nSwitchIndex] != 0))					// �nderung festgestellt
							{
								InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][nSwitchIndex] = 0;
								// Untere Positionsgrenze berechnen
								SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rFirstOnPos[nSwitchIndex] = rLastSwitchPos[nDevIndex][nPmIndex] + gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rPause[nSwitchIndex] * rFACTOR_MM_TO_AXIS_UNITS;
								// Werte auf G�ltigkeit �berpr�fen
								if ((SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rFirstOnPos[nSwitchIndex] >= InvalidPar[nDevIndex].rProductLen * rFACTOR_MM_TO_AXIS_UNITS) ||
									(SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rFirstOnPos[nSwitchIndex] < rLastSwitchPos[nDevIndex][nPmIndex]))
								{
									InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][nSwitchIndex] = 1;
									gCalcPos.State.HeadState[nDevIndex].bHeadError = 1;
									gCalcPos.State.HeadState[nDevIndex].nInvalidParIdx[nPmIndex] = nSwitchIndex;
								}
								rLastSwitchPos[nDevIndex][nPmIndex] = SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rFirstOnPos[nSwitchIndex];
								
								// Obere Positionsgrenze berechnen
								SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rLastOnPos[nSwitchIndex] = rLastSwitchPos[nDevIndex][nPmIndex] + gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rStitchLen[nSwitchIndex] * rFACTOR_MM_TO_AXIS_UNITS;
								// Werte auf G�ltigkeit �berpr�fen
								if ((SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rLastOnPos[nSwitchIndex] < SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rFirstOnPos[nSwitchIndex]) ||
									(SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rLastOnPos[nSwitchIndex] > InvalidPar[nDevIndex].rProductLen * rFACTOR_MM_TO_AXIS_UNITS))
								{
									InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][nSwitchIndex] = 1;
									gCalcPos.State.HeadState[nDevIndex].bHeadError = 1;
									gCalcPos.State.HeadState[nDevIndex].nInvalidParIdx[nPmIndex] = nSwitchIndex;
								}
								
								// Ist das vorige Wertepaar = 0 (FirstOnPos,LastOnPos), dann sind die Werte ung�ltig
								if (nSwitchIndex != 0)
								{
									if ((SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rFirstOnPos[nSwitchIndex - 1] == 0) &&
										(SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rLastOnPos[nSwitchIndex - 1] == 0))
									{
										InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][nSwitchIndex] = 1;
										gCalcPos.State.HeadState[nDevIndex].bHeadError = 1;
										gCalcPos.State.HeadState[nDevIndex].nInvalidParIdx[nPmIndex] = nSwitchIndex - 1;
									}
								}
								gCalcPos.State.HeadState[nDevIndex].TrackState[nPmIndex].rCalcTrackLen = gCalcPos.State.HeadState[nDevIndex].TrackState[nPmIndex].rCalcTrackLen + gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rPause[nSwitchIndex] + gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rStitchLen[nSwitchIndex];					
							}
							else if ((gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rPause[nSwitchIndex] == 0) &&
								(gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rStitchLen[nSwitchIndex] == 0))
							{
								SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rFirstOnPos[nSwitchIndex] = 0;
								SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rLastOnPos[nSwitchIndex] = 0;
								InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][nSwitchIndex] = 0;
							}
							
							if (((gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rPause[nSwitchIndex] != 0) &&
								(gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rStitchLen[nSwitchIndex] == 0)) ||
								((gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rPause[nSwitchIndex]== 0) &&
								(gPar.ProductPar.BonDevPar[nDevIndex].Switches.BondingParIdx[nPmIndex].rStitchLen[nSwitchIndex] != 0) &&
								(nSwitchIndex != 0)))																							// Werte ung�ltig
							{
								InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][nSwitchIndex] = 1;
								gCalcPos.State.HeadState[nDevIndex].bHeadError = 1;
								gCalcPos.State.HeadState[nDevIndex].nInvalidParIdx[nPmIndex] = nSwitchIndex;
							}
							
							if (SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rLastOnPos[nSwitchIndex] != 0)									// Wenn untere Positionsgrenze ungleich Null, dann Wert merken
							{
								rLastSwitchPos[nDevIndex][nPmIndex] = SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rLastOnPos[nSwitchIndex];
							}
							gCalcPos.State.bInvalidParEntry = (gCalcPos.State.bInvalidParEntry || InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][nSwitchIndex]);
						}
						
						if (gCalcPos.State.HeadState[nDevIndex].TrackState[nPmIndex].rCalcTrackLen > InvalidPar[nDevIndex].rProductLen)			// Ung�ltige Track L�nge 
						{
							InvalidPar[nDevIndex].bInvalidParEntry[nPmIndex][nSwitchIndex - 1] = 1;
							gCalcPos.State.HeadState[nDevIndex].TrackState[nPmIndex].bInvalidTrackLen = 1;
						}
						
						if (gCalcPos.State.bInvalidParEntry == 0)					// Positionswerte sind g�ltig
						{
							brsmemcpy((UDINT)&gBondingDev.Par.Switches[nDevIndex].SwitchesIdx[nPmIndex].rFirstOnPos, (UDINT)&SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rFirstOnPos, sizeof(gBondingDev.Par.Switches[nDevIndex].SwitchesIdx[nPmIndex].rFirstOnPos));
							brsmemcpy((UDINT)&gBondingDev.Par.Switches[nDevIndex].SwitchesIdx[nPmIndex].rLastOnPos, (UDINT)&SwitchesBuffer[nDevIndex].SwitchesIdx[nPmIndex].rLastOnPos, sizeof(gBondingDev.Par.Switches[nDevIndex].SwitchesIdx[nPmIndex].rLastOnPos));
						}
					}
				}
			}
			// Fehler Auftragekopf mit FUB-Fehler verkn�pfen
			gCalcPos.State.HeadState[nDevIndex].bHeadError = (gCalcPos.State.HeadState[nDevIndex].bHeadError || gBondingDev.State.bBonDevError[nDevIndex]);
		}
		bStartCalc = 0;
	}
}

void _EXIT CalcPosEXIT(void)
{
}
