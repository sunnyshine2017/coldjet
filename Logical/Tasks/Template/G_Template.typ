(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: Template
 * File: G_Template.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Global  data types of package Template
 ********************************************************************)

TYPE
	TemplateCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bDummy : BOOL;
	END_STRUCT;
	TemplateDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bDummy : BOOL;
	END_STRUCT;
	TemplatePar_TYP : 	STRUCT  (*Kommando-Paramter*)
		nDummy : UINT;
	END_STRUCT;
	TemplateState_TYP : 	STRUCT  (*Rückmeldungen*)
		nDummy : DINT;
	END_STRUCT;
	Template_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : TemplateCallerBox_TYP;
		DirectBox : TemplateDirectBox_TYP;
		Par : TemplatePar_TYP;
		State : TemplateState_TYP;
	END_STRUCT;
END_TYPE
