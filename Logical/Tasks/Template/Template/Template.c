/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Template
 * File: Template.c
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Implementation of program Template
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <TemplateFunc.h>

void _INIT TemplateINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gTemplate, 0, sizeof(gTemplate));
	gTemplate.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC TemplateCYCLIC(void)
{
	// Direct-Kommandos
	if(Step.bInitDone == 1)
	{
		if(gTemplate.DirectBox.bDummy == 1)
		{
			BrbClearDirectBox((UDINT)&gTemplate.DirectBox, sizeof(gTemplate.DirectBox));
		}
	}
	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			//if(gParHandling.State.bParValid == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gTemplate.CallerBox, sizeof(gTemplate.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			if(gTemplate.CallerBox.bDummy == 1)
			{
				Step.eStepNr = eSTEP_CMD1;
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_CMD1:
			strcpy(Step.sStepText, "eSTEP_CMD1");
			Step.eStepNr = eSTEP_CMD1_FINISHED;
			break;

		case eSTEP_CMD1_FINISHED:
			strcpy(Step.sStepText, "eSTEP_CMD1_FINISHED");
			BrbClearCallerBox((UDINT)&gTemplate.CallerBox, sizeof(gTemplate.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

	}
}

void _EXIT TemplateEXIT(void)
{
}
