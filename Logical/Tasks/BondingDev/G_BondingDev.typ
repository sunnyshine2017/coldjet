(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: BondingDev
 * File: G_BondingDev.typ
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Global  data types of package BondingDev
 ********************************************************************)

TYPE
	BondingDevCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bDcsInit : BOOL; (*Initialisierung elektr. Nockenschaltwerk*)
		bApplyPar : BOOL; (*Parameter �bernehmen*)
		bCleanInit : BOOL; (*Initialisierung elektr. Nockenschaltwerk f�r Reinigung*)
	END_STRUCT;
	BondingDevDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bDummy : BOOL;
	END_STRUCT;
	BondingDevPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		Switches : ARRAY[0..nDEV_INDEX_MAX]OF SwitchesIdx_TYP; (*Errechnete Nockenpositionen*)
	END_STRUCT;
	BondingDevState_TYP : 	STRUCT  (*R�ckmeldungen*)
		rVelocitiy : ARRAY[0..nDEV_INDEX_MAX]OF REAL; (*Geschwindigkeit*)
		bVelocityOk : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Geschwindigkeit g�ltig*)
		nEntryCnt : ARRAY[0..nDEV_INDEX_MAX]OF UINT; (*Z�hler f�r Fifo-Eintr�ge die bereits initialisert wurden*)
		nCurrentFifoCnt : ARRAY[0..nDEV_INDEX_MAX]OF UINT; (*Z�hler (Fifo) f�r das Produkt, dass momentan bearbeitet wird*)
		nCurrentFifoCntFlag : ARRAY[0..nDEV_INDEX_MAX]OF UINT; (*Merker f�r Z�hler (Fifo) -> Aktuelles Produkt*)
		nProDetEntryCnt : ARRAY[0..nDEV_INDEX_MAX]OF UINT; (*Z�hler f�r Fifo-Eintr�ge Produkterkennung*)
		bBonDevError : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Bonding Device Fehler*)
		bIntCleanDone : ARRAY[0..nDEV_INDEX_MAX]OF BOOL;
		bCleaningActive : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Reinigung ist aktiv*)
		bDcsCleanInitOk : BOOL; (*Initialisierung f�r Reinigung abgeschlossen*)
	END_STRUCT;
	BondingDev_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : BondingDevCallerBox_TYP;
		DirectBox : BondingDevDirectBox_TYP;
		Par : BondingDevPar_TYP;
		State : BondingDevState_TYP;
	END_STRUCT;
END_TYPE
