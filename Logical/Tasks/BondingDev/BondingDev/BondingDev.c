/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: BondingDev
 * File: BondingDev.c
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Implementation of program BondingDev
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <BondingDevFunc.h>

void _INIT BondingDevINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gBondingDev, 0, sizeof(gBondingDev));
	gBondingDev.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	rNwCompensation = rNETWORK_COMPENSATION;
	bRequestInitClean = 0;
	bRequestApplyData = 0;
	gBondingDev.State.bVelocityOk[nDevIndex] = 0;
	brsmemset((UDINT)&BonDev.bDcsInitOk[0], 0, sizeof(BonDev.bDcsInitOk));
	brsmemset((UDINT)&gBondingDev.State.nEntryCnt[0], 0, sizeof(gBondingDev.State.nEntryCnt));
	brsmemset((UDINT)&BonDev.nIdxCurrentPeriod[0], 0, sizeof(BonDev.nIdxCurrentPeriod));
	brsmemset((UDINT)&BonDev.nIdxNextPeriod[0], 0, sizeof(BonDev.nIdxNextPeriod));
	brsmemset((UDINT)&gBondingDev.State.bBonDevError[0], 0, sizeof(gBondingDev.State.bBonDevError));
	brsmemset((UDINT)&bCalcPeriod[0], 0, sizeof(bCalcPeriod));
	brsmemset((UDINT)&SwitchesBuffer[0], 0, sizeof(SwitchesBuffer));
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC BondingDevCYCLIC(void)
{

	// ---------- Direct-Kommandos ----------
	if(Step.bInitDone == 1)
	{
		if(gBondingDev.DirectBox.bDummy == 1)
		{
			BrbClearDirectBox((UDINT)&gBondingDev.DirectBox, sizeof(gBondingDev.DirectBox));
		}
	}
	
	//// ToDo MZ: Ganz Wichtig!!! Eine Warnung ausgeben wenn nicht geklebt werden kann, weil
	//								a) IO.Input.bBonDevEnable nicht vorhanden ist
	//								b) ExtCtrlIf: IO.Input.bBondingAllowed
	//							  nicht auf TRUE gesetzt sind!!!
	
	// ---------- Initialisierung Reinigung durchf�hren ----------
	fbEdgePosClean.CLK = gMainLogic.State.bCleaningActive;
	R_TRIG(&fbEdgePosClean);
	fbEdgePosCleanMan.CLK = gMainLogic.State.CleaningBonDev.bStartInitCleaning;
	R_TRIG(&fbEdgePosCleanMan);
	if ((fbEdgePosClean.Q == 1) ||
		(fbEdgePosCleanMan.Q == 1))
	{
		bRequestInitClean = 1;
		gBondingDev.State.bDcsCleanInitOk = 0;
		nDevIndex = 0;	
		
		// DCS abl�schen
		for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
		{
			if ((gMainLogic.State.CleaningBonDev.bOpenBonDev[nDevIndex] == 1) &&
				(gBondingDev.State.bCleaningActive[nDevIndex] == 0))
			{
				BonDev.fbBonDevDCS[nDevIndex].Enable = 0;
				BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
				BonDev.fbBonDevDCS[nDevIndex].InitSwitches = 0;
				brsmemset((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches, 0, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches));
				ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);		
			}
		}
	}
	
	if (bRequestInitClean == 1)
	{
		if (BrbSetCaller(&gBondingDev.CallerBox.Caller, eCALLERID_BONDING_DEVICE) == eBRB_CALLER_STATE_OK)
		{
			gBondingDev.CallerBox.bCleanInit = 1;									// Kommando absetzen
			bRequestInitClean = 0;
		}
	}
	
	// ---------- Pr�fen ob Initialisierung Automatikmodus n�tig ist ----------
	fbEdgePosAuto.CLK = gMainLogic.State.bAutomatic;
	R_TRIG(&fbEdgePosAuto);
	if (fbEdgePosAuto.Q == 1)
	{
		// Freigabezust�nde der Auftragek�pfe speichern
		for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
		{
			BonDevEn[nDevIndex].bBonDevEnFlag = gPar.ProductPar.BonDevPar[nDevIndex].bBonDevEn;
			BonDevEn[nDevIndex].bBonDevEnabled = 0;
			BonDevEn[nDevIndex].bBonDevDisabled = 0;
			
			if( gPar.ProductPar.BonDevPar[nDevIndex].eIdxSenType != eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
			{
				// Anpgepasste Produktl�nge errechnen
				gPar.ProductPar.BonDevPar[nDevIndex].rProductLenAdapted = gPar.ProductPar.BonDevPar[nDevIndex].rProductLen * ((100.0 - gPar.ProductPar.BonDevPar[nDevIndex].rReduceProductLenPercent) / 100);
			}
		}
		
		brsmemset((UDINT)&ProductDetect, 0, sizeof(ProductDetect));
		brsmemset((UDINT)&bPeriodInit[0], 0, sizeof(bPeriodInit));
		brsmemset((UDINT)&BonDev.nGap[0], 0, sizeof(BonDev.nGap));
		brsmemset((UDINT)&bStartPosValid[0], 0, sizeof(bStartPosValid));
		brsmemset((UDINT)&rPosDcs[0], 0, sizeof(rPosDcs));
		brsmemset((UDINT)&BonDev.nIdxCurrentPeriod[0], 0, sizeof(BonDev.nIdxCurrentPeriod));
		brsmemset((UDINT)&BonDev.nIdxNextPeriod[0], 0, sizeof(BonDev.nIdxNextPeriod));
		nDevIndex = 0;
		bRequestInitAuto = 1;
	}
	
	if (bRequestInitAuto == 1)
	{
		if (BrbSetCaller(&gBondingDev.CallerBox.Caller, eCALLERID_BONDING_DEVICE) == eBRB_CALLER_STATE_OK)
		{
			gBondingDev.CallerBox.bDcsInit = 1;									// Kommando absetzen
			bRequestInitAuto = 0;
		}	
	}
	
	// ---------- Pr�fen ob Daten�bernahme n�tig ist ----------
	fbEdgeNegClean.CLK = gMainLogic.State.bCleaningActive;
	F_TRIG(&fbEdgeNegClean);
	fbEdgeNegCleanMan.CLK = gMainLogic.State.CleaningBonDev.bStartInitCleaning;
	F_TRIG(&fbEdgeNegCleanMan);
	// Pr�fen ob Intervall Reinigung gestoppt werden muss
	for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
	{
		if ((gMainLogic.State.bCleaningActive == 1) &&
			(gMainLogic.Par.eCleaningType == eCLEANING_TYPE_INTERVAL) &&
			(nCntClean[nDevIndex] >= gPar.MachinePar.nNrOfRounds) &&
			(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 0))
		{
			bStopIntCleaning = 1;
		}
	}
	if ((fbEdgeNegClean.Q == 1) ||
		(fbEdgeNegCleanMan.Q == 1) ||
		(bStopIntCleaning == 1))
	{
		nCntCleanFlag = 0;
		for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
		{
			// Reinigung des Kopfes ist gerade aktiv
			if ((gBondingDev.State.bCleaningActive[nDevIndex] == 1) &&
				// Im Task MainLogic ist die Reinigung des Kopfes schon beendet
				((gMainLogic.State.bCleaningActive == 0) ||
				// Die Anzahl der Durchl�ufe der Intervall-Reinigung wurde erreicht
				((gMainLogic.Par.eCleaningType == eCLEANING_TYPE_INTERVAL) &&
				(nCntClean[nDevIndex] >= gPar.MachinePar.nNrOfRounds) &&
				(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 0)) ||
				// Ein Kopf wurde w�hrend der automatischen Reinigung durch wegnehmen des Hakens f�r die Reinigung deaktiviert
				((gMainLogic.State.bCleaningActive == 1) &&
				(gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIndex] == 0) &&
				(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 0))
				))
			{
				BonDev.fbBonDevDCS[nDevIndex].Enable = 0;
				BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
				BonDev.fbBonDevDCS[nDevIndex].InitSwitches = 0;
				brsmemset((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches, 0, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches));
				ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
				
				bStopIntCleaning = 0;
				bStartPosValid[nDevIndex] = 0;
				bGetStartPos[nDevIndex] = 0;
				bCalcPeriod[nDevIndex] = 0;
				BonDev.bDcsInitOk[nDevIndex] = 0;
				BonDevEn[nDevIndex].bInitializeCounter = 1;
				gBondingDev.State.bCleaningActive[nDevIndex] = 0;
			}
		}
		
		if (gMainLogic.State.bAutomatic == 1)
		{
			bRequestApplyData = 1;
		}
	}
	if (bRequestApplyData == 1)
	{
		// Klebek�pfe
		if (BrbSetCaller(&gBondingDev.CallerBox.Caller, eCALLERID_BONDING_DEVICE) == eBRB_CALLER_STATE_OK)
		{
			gBondingDev.CallerBox.bApplyPar = 1;									// Kommando absetzen
			bRequestApplyData = 0;
		}	
	}
		
	// ---------- StepHandling ----------
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	
	// ---------- Schrittkette ----------
	switch(Step.eStepNr)
	{
		//=======================================================================================================================
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			
			if(gParHandling.State.bInitDone == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gBondingDev.CallerBox, sizeof(gBondingDev.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//=======================================================================================================================
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			
			// Dcs Initialisierung starten
			if(gBondingDev.CallerBox.bDcsInit == 1)
			{
				nDevIndex = 0;										// Initialisierung Index Auftragek�pfe
				Step.eStepNr = eSTEP_DCS_INIT;						// Parameter �berpr�fen
			}
			
			// Nockenpositionen berechnen
			if(gBondingDev.CallerBox.bApplyPar == 1)
			{
				nDevIndex = 0;										// Initialisierung Index Auftragek�pfe
				Step.eStepNr = eSTEP_SWITCHES_INIT;
			}
			
			// Dcs Initialisierung f�r Reinigung starten
			if(gBondingDev.CallerBox.bCleanInit == 1)
			{
				gBondingDev.State.bDcsCleanInitOk = 0;
				nDevIndex = 0;										// Initialisierung Index Auftragek�pfe
				Step.eStepNr = eSTEP_DCS_CLEAN_INIT;				// Parameter �berpr�fen
			}
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Nockenschaltwerk initialisieren
		case eSTEP_DCS_INIT:
			strcpy(Step.sStepText, "eSTEP_DCS_INIT");
			
			for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
			{
				if ((gPar.ProductPar.BonDevPar[nDevIndex].bBonDevEn == 1) &&
				(gBondingDev.State.bCleaningActive[nDevIndex] == 0))				// Auftragekopf aktiviert nur wenn keine Reinigung ausgef�hrt wird
				{
					BonDev.fbBonDevDCS[nDevIndex].PositionConfig.PositionCompensation 	= 0;
					BonDev.fbBonDevDCS[nDevIndex].Switches.StartPosition				= 0;
					BonDev.fbBonDevDCS[nDevIndex].PositionConfig.DataType				= asMCDCS_DATATYPE_DINT;
					BonDev.fbBonDevDCS[nDevIndex].PositionConfig.Period					= 0;
					BonDev.fbBonDevDCS[nDevIndex].TrackOptions.NetworkCompensation		= rNETWORK_COMPENSATION;
					BonDev.fbBonDevDCS[nDevIndex].TrackOptions.OnCompensation			= 0;
					BonDev.fbBonDevDCS[nDevIndex].TrackOptions.OffCompensation			= 0;
					BonDev.fbBonDevDCS[nDevIndex].TrackOptions.Hysteresis				= 0;
					BonDev.fbBonDevDCS[nDevIndex].TrackOptions.DisableNegativeDirection	= nDISABLE_NEG_DIR;
					BonDev.fbBonDevDCS[nDevIndex].InitTrackOptions						= 1;
					BonDev.fbBonDevDCS[nDevIndex].FilterOptions.Mode					= asMCDCS_FILTER_LOW_PASS;
					BonDev.fbBonDevDCS[nDevIndex].FilterOptions.Parameter1				= rFILTER_PAR;
					BonDev.fbBonDevDCS[nDevIndex].FilterOptions.Parameter2				= rFILTER_PAR;
					BonDev.fbBonDevDCS[nDevIndex].InitFilterOptions						= 1;
					BonDev.fbBonDevDCS[nDevIndex].Mode									= ASMCDCS_MODE_PERIOD;
					// Funktion disablen
					BonDev.fbBonDevDCS[nDevIndex].Enable = 0;
					// Funktionsaufruf
					ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
				}
			}
			
			Step.eStepNr = eSTEP_SWITCHES_INIT;
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Nockenpositionen initialisieren
		case eSTEP_SWITCHES_INIT:
			strcpy(Step.sStepText, "eSTEP_SWITCHES_INIT");
			
			brsmemcpy((UDINT)&ProductParDcs, (UDINT)&gPar.ProductPar, sizeof(ProductParDcs));
			brsmemcpy((UDINT)&SwitchesBuffer, (UDINT)&gBondingDev.Par.Switches, sizeof(SwitchesBuffer));
			
			for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
			{				
				if ((gPar.ProductPar.BonDevPar[nDevIndex].bBonDevEn == 1) &&							// Auftragekopf aktiviert
					(BonDev.bDcsInitOk[nDevIndex] == 0) &&
					(gMainLogic.State.bAutomatic == 1) &&		
					(gBondingDev.State.bCleaningActive[nDevIndex] == 0))					// Nur Initialisieren wenn keine Reinigung ausgef�hrt wird
				{
					if( (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR) ||
						(ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER) ||
						(ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR) ||
						(ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) )
					{
						BonDev.nNextPeriod[nDevIndex]							= (UDINT)(nMIN_SENSOR_DISTANCE / 2);			// Periodenl�nge an FUB �bergeben
						BonDev.nIdxCurrentPeriod[nDevIndex]						= 0;											// Index der aktuellen Periode
						BonDev.nIdxNextPeriod[nDevIndex]						= 0;											// Index der n�chsten Periode
						BonDev.fbBonDevDCS[nDevIndex].Switches.Period			= BonDev.nNextPeriod[nDevIndex];
						BonDev.fbBonDevDCS[nDevIndex].EnableTrack				= 0;
						brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition, (UDINT)&nFIRST_ON_POS_DUMMY_START, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition));
						brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition, (UDINT)&nLAST_ON_POS_DUMMY_START, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition));
						BonDev.fbBonDevDCS[nDevIndex].InitSwitches				= 0;
						BonDev.fbBonDevDCS[nDevIndex].Switches.StartPosition 	= gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[nDevIndex].nIdxEncoder];
						BonDev.fbBonDevDCS[nDevIndex].Enable 					= 1;
						BonDev.bDcsInitOk[nDevIndex]							= 1;
						
						// Z�hlerst�nde f�r Fifo-Eintr�ge synchronisieren
//						if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR)
//						{
//							gBondingDev.State.nEntryCnt[nDevIndex] = gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
//							gBondingDev.State.nProDetEntryCnt[nDevIndex] = gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
//						}
//						else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER)
//						{
//							gBondingDev.State.nEntryCnt[nDevIndex] = gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
//							gBondingDev.State.nProDetEntryCnt[nDevIndex] = gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
//						}
//						else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR)
//						{
//							gBondingDev.State.nEntryCnt[nDevIndex] = gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
//							gBondingDev.State.nProDetEntryCnt[nDevIndex] = gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
//						}			
					}
				}
			}
			
			BrbClearCallerBox((UDINT)&gBondingDev.CallerBox, sizeof(gBondingDev.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;		
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Nockenschaltwerk f�r Reinigung initialisieren
		case eSTEP_DCS_CLEAN_INIT:
			strcpy(Step.sStepText, "eSTEP_DCS_CLEAN_INIT");
			
			brsmemset((UDINT)&rPosDcsClean[0], 0, sizeof(rPosDcsClean));
			brsmemset((UDINT)&nCntClean[0], 0, sizeof(nCntClean));
			for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
			{
				if ((gMainLogic.State.CleaningBonDev.bOpenBonDev[nDevIndex] == 1) &&
					(gBondingDev.State.bCleaningActive[nDevIndex] == 0))
				{
					BonDev.fbBonDevDCS[nDevIndex].PositionConfig.PositionCompensation = 0;
					BonDev.fbBonDevDCS[nDevIndex].PositionConfig.DataType		= asMCDCS_DATATYPE_DINT;
					BonDev.fbBonDevDCS[nDevIndex].PositionConfig.Period			= 0;
					BonDev.fbBonDevDCS[nDevIndex].TrackOptions.NetworkCompensation		= rNETWORK_COMPENSATION;
					BonDev.fbBonDevDCS[nDevIndex].TrackOptions.OnCompensation			= 0;
					BonDev.fbBonDevDCS[nDevIndex].TrackOptions.OffCompensation			= 0;
					BonDev.fbBonDevDCS[nDevIndex].TrackOptions.Hysteresis				= 0;
					BonDev.fbBonDevDCS[nDevIndex].TrackOptions.DisableNegativeDirection	= nDISABLE_NEG_DIR;
					BonDev.fbBonDevDCS[nDevIndex].InitTrackOptions		= 1;
					BonDev.fbBonDevDCS[nDevIndex].FilterOptions.Mode	= asMCDCS_FILTER_LOW_PASS;
					BonDev.fbBonDevDCS[nDevIndex].FilterOptions.Parameter1	= rFILTER_PAR;
					BonDev.fbBonDevDCS[nDevIndex].FilterOptions.Parameter2	= rFILTER_PAR;
					BonDev.fbBonDevDCS[nDevIndex].InitFilterOptions		= 1;
					BonDev.fbBonDevDCS[nDevIndex].Mode					= ASMCDCS_MODE_PERIOD;
					BonDev.fbBonDevDCS[nDevIndex].EnableTrack			= 1;
					
					// Reinigungsparameter bestimmen
					if (((gMainLogic.Par.eCleaningType == eCLEANING_TYPE_STANDARD) &&
						(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 0)) ||
						((gMainLogic.State.CleaningBonDev.bCleanStandard[nDevIndex] == 1) &&
						(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 1)))						// Standard Reinigung
					{
						BonDev.fbBonDevDCS[nDevIndex].Switches.Period				= 3000;
						BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition[0]	= 0;
						BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition[0]	= 1000;
						BonDev.fbBonDevDCS[nDevIndex].EnableTrack					= 0;			
					}
					else if (((gMainLogic.Par.eCleaningType == eCLEANING_TYPE_INTERVAL) &&
						(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 0)) ||
						((gMainLogic.State.CleaningBonDev.bCleanInt[nDevIndex] == 1) &&
						(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 1)))																			// Intervall Reinigung
					{
						BonDev.fbBonDevDCS[nDevIndex].Switches.Period				= nPERIOD_LEN_CLEANING_INTERVALL;
						BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition[0]	= 2000;
						BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition[0]	= 2000 + nCLEANING_VELOCITY * rFACTOR_MM_TO_AXIS_UNITS * gPar.MachinePar.rOpenTime;
						BonDev.fbBonDevDCS[nDevIndex].EnableTrack					= 1;
					}
					BonDev.fbBonDevDCS[nDevIndex].Switches.StartPosition = gEncoder.State.nSimAxisPos;
					gBondingDev.State.bCleaningActive[nDevIndex] = 1;
					
					// Dcs Init abl�schen
					BonDev.bDcsInitOk[nDevIndex] = 0;
					BonDev.fbBonDevDCS[nDevIndex].Enable = 0;
					ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
				}						
			}
			
			gBondingDev.State.bDcsCleanInitOk = 1;												// Initialisierung der DCS abgeschlossen		
			BrbClearCallerBox((UDINT)&gBondingDev.CallerBox, sizeof(gBondingDev.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;
	}
	
	
	//---------------------------------------------------------------------------------------------------------------
	// ---------- Pr�fen ob Produkterkennungs-Sensor tats�chlich vorhanden ----------
	//---------------------------------------------------------------------------------------------------------------
	
	// Schleife �ber alle konfigurierten Klebek�pfe
	for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
	{
		// Es ist kein Produkterkennungs-Sensor konfiguriert
		// (0 ist ein g�ltiger Wert, -1 und kleiner bedeutet kein Sensor konfiguriert)
		if( gPar.MachinePar.nProductDetectIndex < 0 )
		{
			// F�r diesen Kopf ist trotz nicht vorhandenen Produkterkennungs-Sensor dieser aktiviert
			if( gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.bProductDetExist == TRUE )
			{
				// -> Produkterkennungs-Sensor f�r diesen Kopf deaktivieren
				gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.bProductDetExist = FALSE;
				// -> Indexzuordnung des Produkterkennungssensors zur�cksetzen
				gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect = 0;
			}
		}
	}
	
	//---------------------------------------------------------------------------------------------------------------
	// ---------- DCS ausf�hren ----------
	//---------------------------------------------------------------------------------------------------------------
	for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
	{	
		if( (gParHandling.State.bMParValid == 1) &&
			(gBondingDev.State.bBonDevError[nDevIndex] == 0) )
		{
			if ((gBondingDev.State.bCleaningActive[nDevIndex] == 1) &&
				(gMainLogic.State.bSealingSystem[nDevIndex] == 1) &&
				(gBondingDev.State.bDcsCleanInitOk == 1))										// Reinigung ausf�hren
			{
				// Bei Intervallreinigung nach parametrierter Schussanzahl aufh�ren
				if (((gMainLogic.Par.eCleaningType == eCLEANING_TYPE_INTERVAL) &&
					(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 0)) ||
					((gMainLogic.State.CleaningBonDev.bCleanInt[nDevIndex] == 1) &&
					(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 1)))
				{
					if (BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition < rPosDcsClean[nDevIndex])
					{
						nCntClean[nDevIndex] = nCntClean[nDevIndex] + 1;
						nCntCleanFlag = nCntClean[nDevIndex];
					}
					rPosDcsClean[nDevIndex] = BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition;
					// Parametriert Schussanzahl erreicht
					if ((nCntClean[nDevIndex] >= gPar.MachinePar.nNrOfRounds) &&
						(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 0))
					{
						BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
						gBondingDev.State.bIntCleanDone[nDevIndex] = 1;
					}
				}
				
				// DCS ausf�hren
				BonDev.fbBonDevDCS[nDevIndex].Enable = 1;
				BonDev.fbBonDevDCS[nDevIndex].Position.Integer = gEncoder.State.nSimAxisPos;	// Bei Reinigung simulierte Geschwindigkeit verwenden
				ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
				gBondingDev.State.rVelocitiy[nDevIndex] = BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.Velocity;
				
				// Bei Standard Reinigung eine gro�e Periode �bergeben, um ein dauerhaftes �ffnen des Ventils zu simulieren
				if ((BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition > 0) &&
					(((gMainLogic.Par.eCleaningType == eCLEANING_TYPE_STANDARD) &&
					(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 0)) ||
					((gMainLogic.State.CleaningBonDev.bCleanStandard[nDevIndex] == 1) &&
					(gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIndex] == 1))))
				{
					BonDev.fbBonDevDCS[nDevIndex].Switches.Period				= nPERIOD_LEN_CLEANING_STANDARD;
					BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition[0]	= 0;
					BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition[0]	= nPERIOD_LEN_CLEANING_STANDARD - 1000;
					BonDev.fbBonDevDCS[nDevIndex].InitSwitches					= 1;
					BonDev.fbBonDevDCS[nDevIndex].EnableTrack					= 1;
				}
						
				if (BonDev.fbBonDevDCS[nDevIndex].Error == 1)																			// Fehler aufgetreten
				{
					AppSetEvent(eEVT_ERR_CLEANING, BonDev.fbBonDevDCS[nDevIndex].ErrorID, "", (nDevIndex + 1), &gEventManagement);		// Meldung absetzen 
					AppResetEvent(eEVT_ERR_CLEANING, &gEventManagement);
					
					gMainLogic.DirectBox.bCleaningStop = 1;
					gBondingDev.State.bCleaningActive[nDevIndex] = 0;
					BonDev.fbBonDevDCS[nDevIndex].Enable = 0;																			// Fub zur�cksetzen
					ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
				}
			}
			
			//---------------------------------------------------------------------------------------------------------------
			// ---------- Automatikbetrieb ----------
			//---------------------------------------------------------------------------------------------------------------
			if ((gBondingDev.State.bCleaningActive[nDevIndex] == 0) &&
				(gMainLogic.State.bSealingSystem[nDevIndex] == 1) &&
				(BonDev.bDcsInitOk[nDevIndex] == 1) &&
				(gMainLogic.State.bAutomatic == 1))																						// Automatikbetrieb aktiv
			{
			
				// ---------- Auftragekopf aktiv ----------
				if (ProductParDcs.BonDevPar[nDevIndex].bBonDevEn == 1)
				{
					
					// -----------------------------------------
					// Startposition ermitteln
					// -----------------------------------------
					if (bGetStartPos[nDevIndex] == 0)
					{
						if ((bStartPosValid[nDevIndex] == 0) &&
							(BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition > 0))											// Fub ist aktiv
						{
							bGetStartPos[nDevIndex] = 1;
							gBondingDev.State.bVelocityOk[nDevIndex] = 1;																	// Startposition ermitteln
							// Z�hler synchronisieren
							if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR)
							{
								// Workaround: Bei Initialisierung w�hrend der Produktion (Automatikbetrieb: nach Reinigung und zuschalten eines Klebekopfes),
								// wird der Leseindex (gBondingDev.State.nEntryCnt[nDevIndex]) um 2 als der Schreibindex erh�ht. Dadurch werden zwei Produkte nicht bearbeitet
								// ---> Notwendig wurde diese Ma�nahme dadurch, dass bei gleichsetzten der Indizes ein Fehler ausgegeben wurde (nGap < 0).
								if (BonDevEn[nDevIndex].bInitializeCounter == 1)
								{
//// ToDo MZ:	Zum Test wurden das �berspringen der n�chsten zwei Produkte nach der Reinigung auskommentiert. Nun wird jedes Produkt geklebt, wenn es vom Abstand (nGap) m�glich ist.
//				Planatol testet ob diese �nderung erfolgreich war und ob diese negative Einfl�sse auf die restliche Funktionalit�t hat.
									gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount /*+ 2*/;
									gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount /*+ 2*/;
									BonDevEn[nDevIndex].bInitializeCounter			= 0;
								}
								else
								{
									gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
									gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
								}
							}
							else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER)
							{
								// Workaround: Bei Initialisierung w�hrend der Produktion (Automatikbetrieb: nach Reinigung und zuschalten eines Klebekopfes),
								// wird der Leseindex (gBondingDev.State.nEntryCnt[nDevIndex]) um 2 als der Schreibindex erh�ht. Dadurch werden zwei Produkte nicht bearbeitet
								// ---> Notwendig wurde diese Ma�nahme dadurch, dass bei gleichsetzten der Indizes ein Fehler ausgegeben wurde (nGap < 0).
								if (BonDevEn[nDevIndex].bInitializeCounter == 1)
								{								
//// ToDo MZ:	Zum Test wurden das �berspringen der n�chsten zwei Produkte nach der Reinigung auskommentiert. Nun wird jedes Produkt geklebt, wenn es vom Abstand (nGap) m�glich ist.
//				Planatol testet ob diese �nderung erfolgreich war und ob diese negative Einfl�sse auf die restliche Funktionalit�t hat.
									gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder].nEntryCount /*+ 2*/;
									gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder].nEntryCount /*+ 2*/;
									BonDevEn[nDevIndex].bInitializeCounter			= 0;
								}
								else
								{
									gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder].nEntryCount;
									gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder].nEntryCount;
								}
							}
							else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR)
							{
								// Workaround: Bei Initialisierung w�hrend der Produktion (Automatikbetrieb: nach Reinigung und zuschalten eines Klebekopfes),
								// wird der Leseindex (gBondingDev.State.nEntryCnt[nDevIndex]) um 2 als der Schreibindex erh�ht. Dadurch werden zwei Produkte nicht bearbeitet
								// ---> Notwendig wurde diese Ma�nahme dadurch, dass bei gleichsetzten der Indizes ein Fehler ausgegeben wurde (nGap < 0).
								if (BonDevEn[nDevIndex].bInitializeCounter == 1)
								{									
//// ToDo MZ:	Zum Test wurden das �berspringen der n�chsten zwei Produkte nach der Reinigung auskommentiert. Nun wird jedes Produkt geklebt, wenn es vom Abstand (nGap) m�glich ist.
//				Planatol testet ob diese �nderung erfolgreich war und ob diese negative Einfl�sse auf die restliche Funktionalit�t hat.
									gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount /*+ 2*/;
									gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount /*+ 2*/;
									BonDevEn[nDevIndex].bInitializeCounter			= 0;
								}
								else
								{
									gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
									gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;								
								}
							}
							else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR)
							{
								// Workaround: Bei Initialisierung w�hrend der Produktion (Automatikbetrieb: nach Reinigung und zuschalten eines Klebekopfes),
								// wird der Leseindex (gBondingDev.State.nEntryCnt[nDevIndex]) um 2 als der Schreibindex erh�ht. Dadurch werden zwei Produkte nicht bearbeitet
								// ---> Notwendig wurde diese Ma�nahme dadurch, dass bei gleichsetzten der Indizes ein Fehler ausgegeben wurde (nGap < 0).
								if (BonDevEn[nDevIndex].bInitializeCounter == 1)
								{									
//// ToDo MZ:	Zum Test wurden das �berspringen der n�chsten zwei Produkte nach der Reinigung auskommentiert. Nun wird jedes Produkt geklebt, wenn es vom Abstand (nGap) m�glich ist.
//				Planatol testet ob diese �nderung erfolgreich war und ob diese negative Einfl�sse auf die restliche Funktionalit�t hat.
									gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].nEntryCount /*+ 2*/;
									gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].nEntryCount /*+ 2*/;
									BonDevEn[nDevIndex].bInitializeCounter			= 0;
								}
								else
								{
									gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].nEntryCount;
									gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].nEntryCount;								
								}
							}
						}
						else
						{
							if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR)
							{
								gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
								gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
							}
							else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER)
							{
								gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder].nEntryCount;
								gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder].nEntryCount;
							}
							else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR)
							{
								gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
								gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount;
							}
							else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR)
							{
								gBondingDev.State.nEntryCnt[nDevIndex] 			= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].nEntryCount;
								gBondingDev.State.nProDetEntryCnt[nDevIndex] 	= gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].nEntryCount;
							}
						}
					}		
					
					// Klebeposition ermitteln
					if ((bGetStartPos[nDevIndex] == 1) &&
						(bStartPosValid[nDevIndex] == 0))
					{
						// Sensortyp feststellen
						if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR)													// Trigger Sensor
						{
							if (gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount > gBondingDev.State.nEntryCnt[nDevIndex])
							{	
								BonDev.nDiffLen[nDevIndex] 		= 0;
							
								BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource], gBondingDev.State.nEntryCnt[nDevIndex], (UDINT)&BonDev.FifoStruct.FifoEntryStandard[nDevIndex]);
								BonDev.nDiffLatchPos[nDevIndex]	= BonDev.FifoStruct.FifoEntryStandard[nDevIndex].nPosToBond;
								bEnableTrack[nDevIndex] = BonDev.FifoStruct.FifoEntryStandard[nDevIndex].bEditProdukt;
								BonDev.nFifoPos[nDevIndex] = BonDev.FifoStruct.FifoEntryStandard[nDevIndex].nPosToBond;
								BonDev.rProductLen[nDevIndex] = ProductParDcs.BonDevPar[nDevIndex].rProductLenAdapted;
								BonDev.nSwitchIdx[nDevIndex] = 0;
								rNwCompensation = rNETWORK_COMPENSATION;
								bCalcPeriod[nDevIndex] = 1;
							}					
						}
						else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER)													// Geber als Quelle
						{
							if (gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder].nEntryCount > gBondingDev.State.nEntryCnt[nDevIndex])
							{
								BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder], gBondingDev.State.nEntryCnt[nDevIndex], (UDINT)&BonDev.FifoStruct.FifoEntryStandard[nDevIndex]);
								BonDev.nFifoPos[nDevIndex] = BonDev.FifoStruct.FifoEntryStandard[nDevIndex].nPosToBond;
								BonDev.rProductLen[nDevIndex] = ProductParDcs.BonDevPar[nDevIndex].rCylinderPerimeter;
								BonDev.nSwitchIdx[nDevIndex] = 0;
								rNwCompensation = rNETWORK_COMPENSATION + 0.001;
								bCalcPeriod[nDevIndex] = 1;
							}
						}
						else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR)											// Druckmarkensensor
						{
							if (gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount > gBondingDev.State.nEntryCnt[nDevIndex])
							{
								BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource], gBondingDev.State.nEntryCnt[nDevIndex], (UDINT)&BonDev.FifoStruct.FifoEntryPm[nDevIndex]);
								BonDev.nFifoPos[nDevIndex] = BonDev.FifoStruct.FifoEntryPm[nDevIndex].nStartPosFromTs - (gPar.ProductPar.BonDevPar[nDevIndex].rPmProductOffset * rFACTOR_MM_TO_AXIS_UNITS);			// Abstand Produktbeginn zu Druckmarke abziehen
								BonDev.rProductLen[nDevIndex] = ProductParDcs.BonDevPar[nDevIndex].rProductLenAdapted;
								BonDev.nSwitchIdx[nDevIndex] = BonDev.FifoStruct.FifoEntryPm[nDevIndex].nPmIdx;
								rNwCompensation = rNETWORK_COMPENSATION;
								bCalcPeriod[nDevIndex] = 1;
							}
						}	
						else if( ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
						{
							if( gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].nEntryCount > gBondingDev.State.nEntryCnt[nDevIndex] )
							{
								BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource], gBondingDev.State.nEntryCnt[nDevIndex], (UDINT)&BonDev.FifoStruct.FifoEntryPm[nDevIndex]);
								if( BonDev.FifoStruct.FifoEntryPm[nDevIndex].bProductPositionSetByTrigger == TRUE )
								{
									BonDev.nFifoPos[nDevIndex] = BonDev.FifoStruct.FifoEntryPm[nDevIndex].nPositionProductBegins;
									BonDev.rProductLen[nDevIndex] = ProductParDcs.BonDevPar[nDevIndex].rProductLen;
									BonDev.nSwitchIdx[nDevIndex] = BonDev.FifoStruct.FifoEntryPm[nDevIndex].nPmIdx;
									rNwCompensation = rNETWORK_COMPENSATION + 0.001;				// Netzwerkkompensation --> Werte wurden empirisch ermitelt;
									// G�ltiger Druckmarken-Index in Fifo-Eintrag enthalten
									if( BonDev.nSwitchIdx[nDevIndex] >= 0 )
									{
										// Perioden werden neu berechnet
										bCalcPeriod[nDevIndex] = 1;
									}
									// Der Fifo-Eintrag enth�lt einen ung�ltigen Druckmarken-Index ( -1 = ung�ltige Druckmarke)
									else
									{
										// Diesen Fifo-Eintrag �berspringen und somit nicht bearbeiten
										gBondingDev.State.nEntryCnt[nDevIndex] += 1;
									}
								}
							}
						}
						
						// Periodenl�nge berechnen
						if (bCalcPeriod[nDevIndex] == 1)
						{
							BonDev.nRemainingDistance[nDevIndex] = (UDINT)(nMIN_SENSOR_DISTANCE / 2) - (UDINT)Round(BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition);								
							BonDev.nGap[nDevIndex] = ((BonDev.nFifoPos[nDevIndex] + (DINT)(gPar.ProductPar.BonDevPar[nDevIndex].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS)) - (gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[nDevIndex].nIdxEncoder] + BonDev.nRemainingDistance[nDevIndex]));	
							BonDev.nGap[nDevIndex] = BonDev.nGap[nDevIndex] - (rNwCompensation * gBondingDev.State.rVelocitiy[nDevIndex]);
							// Hier die Einschaltkompensation des Kopfes (�ffnungszeit * Geschwindigkeit) einrechnen (abziehen von GAP)
							BonDev.nGap[nDevIndex] -= gPar.ProductPar.BonDevPar[nDevIndex].rValveCompensationOpenTime / 1000 * gBondingDev.State.rVelocitiy[nDevIndex];
								
							// Abstand zwischen zwei Produkten ist negativ -> Warnung ausgeben
							if (BonDev.nGap[nDevIndex] < 0)
							{
								if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR)
								{
									AppSetEvent(eEVT_WAR_NEG_PRODUCT_DISTANCE, 0, "Trigger sensor", (nDevIndex + 1), &gEventManagement);					// Meldung absetzen 
								}
								else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER)
								{
									AppSetEvent(eEVT_WAR_NEG_PRODUCT_DISTANCE, 0, "Encoder", (nDevIndex + 1), &gEventManagement);
								}
								else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR)
								{
									AppSetEvent(eEVT_WAR_NEG_PRODUCT_DISTANCE, 0, "Print mark sensor", (nDevIndex + 1), &gEventManagement);
								}
								else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR)
								{
									AppSetEvent(eEVT_WAR_NEG_PRODUCT_DISTANCE, 0, "Print mark and trigger sensor", (nDevIndex + 1), &gEventManagement);
								}
								AppResetEvent(eEVT_WAR_NEG_PRODUCT_DISTANCE, &gEventManagement);
								
								// Es wird kein Fehler mehr ausgegeben, stattdessen wird eine Warnung ausgegeben.
								
//								BonDev.bDcsInitOk[nDevIndex]				= 0;
//								gBondingDev.State.bBonDevError[nDevIndex]	= 1;
//								bStartPosValid[nDevIndex] 					= 0;
//								bGetStartPos[nDevIndex] 					= 0;
//								// DCS Eing�nge abl�schen
//								BonDev.fbBonDevDCS[nDevIndex].Enable = 0;
//								BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
//								BonDev.fbBonDevDCS[nDevIndex].InitSwitches = 0;
//								brsmemset((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches, 0, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches));
//								ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
//								gMainLogic.DirectBox.bStop = 1;	
								
								// Bei neg. Sensorabstand -> Dummy Periode einf�gen
								BonDev.nNextPeriod[nDevIndex] 					= (DINT)(nMIN_SENSOR_DISTANCE / 2);
								BonDev.nIdxNextPeriod[nDevIndex]				= 2;																	// Index der n�chsten Periode	
								BonDev.fbBonDevDCS[nDevIndex].Switches.Period	= BonDev.nNextPeriod[nDevIndex];
								brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition, (UDINT)&nFIRST_ON_POS_DUMMY_START, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition));
								brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition, (UDINT)&nLAST_ON_POS_DUMMY_START, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition));						
								BonDev.fbBonDevDCS[nDevIndex].InitSwitches		= 1;
								bPeriodInit[nDevIndex]							= 0;
								gBondingDev.State.nEntryCnt[nDevIndex]			= gBondingDev.State.nEntryCnt[nDevIndex] + 2;						// Zur Sicherheit werden 2+1 (unten) Produkte ausgelassen, falls Dummy-Periode > Produktl�nge ist
							}
							else
							{
								// Nockenpositionen anpassen
								brsmemset((UDINT)&AdaptedSwitches[nDevIndex], 0, sizeof(AdaptedSwitches[nDevIndex]));
								for (nSwitchIndex = 0; nSwitchIndex <= gPar.ProductPar.BonDevPar[nDevIndex].nSwitchIndexMax[BonDev.nSwitchIdx[nDevIndex]]; nSwitchIndex ++)
								{
									if ((SwitchesBuffer[nDevIndex].SwitchesIdx[BonDev.nSwitchIdx[nDevIndex]].rFirstOnPos[nSwitchIndex] == 0) &&
										(SwitchesBuffer[nDevIndex].SwitchesIdx[BonDev.nSwitchIdx[nDevIndex]].rLastOnPos[nSwitchIndex] == 0))				// Nur bei g�ltigen Werten �bernehmn
									{
										AdaptedSwitches[nDevIndex].rFirstOnPos[nSwitchIndex]	= AdaptedSwitches[nDevIndex].rFirstOnPos[nSwitchIndex];
										AdaptedSwitches[nDevIndex].rLastOnPos[nSwitchIndex]		= AdaptedSwitches[nDevIndex].rLastOnPos[nSwitchIndex];
									}
									else
									{
										AdaptedSwitches[nDevIndex].rFirstOnPos[nSwitchIndex]	= SwitchesBuffer[nDevIndex].SwitchesIdx[BonDev.nSwitchIdx[nDevIndex]].rFirstOnPos[nSwitchIndex] + (REAL)BonDev.nGap[nDevIndex];
										AdaptedSwitches[nDevIndex].rLastOnPos[nSwitchIndex]		= SwitchesBuffer[nDevIndex].SwitchesIdx[BonDev.nSwitchIdx[nDevIndex]].rLastOnPos[nSwitchIndex] + (REAL)BonDev.nGap[nDevIndex];									
										// Hier von rLastOnPos die Ausschaltkompensation mit abziehen
										AdaptedSwitches[nDevIndex].rLastOnPos[nSwitchIndex] -= gPar.ProductPar.BonDevPar[nDevIndex].rValveCompensationCloseTime / 1000 * gBondingDev.State.rVelocitiy[nDevIndex];
									
										// Durch die Ausschalt-Kompensation w�rde der Leimstrich negativ lange sein
										// Dies kann bei sehr kurzen Klebestrichen auftreten
										if( AdaptedSwitches[nDevIndex].rLastOnPos[nSwitchIndex] <= AdaptedSwitches[nDevIndex].rFirstOnPos[nSwitchIndex] )
										{
											AppSetEvent( eEVT_ERR_VAL_CLOSE_COMP_TO_LARGE, 0, "", nDevIndex + 1, &gEventManagement);
											AppResetEvent( eEVT_ERR_VAL_CLOSE_COMP_TO_LARGE, &gEventManagement );
											
											BonDev.bDcsInitOk[nDevIndex]				= 0;
											gBondingDev.State.bBonDevError[nDevIndex]	= 1;
											bStartPosValid[nDevIndex] 					= 0;
											bGetStartPos[nDevIndex] 					= 0;
											// DCS Eing�nge abl�schen
											BonDev.fbBonDevDCS[nDevIndex].Enable = 0;
											BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
											BonDev.fbBonDevDCS[nDevIndex].InitSwitches = 0;
											brsmemset((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches, 0, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches));
											ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
											gMainLogic.DirectBox.bStop = 1;
										}
										
									}
									
								}
								
								brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition, (UDINT)&AdaptedSwitches[nDevIndex].rFirstOnPos, sizeof(AdaptedSwitches[nDevIndex].rFirstOnPos));
								brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition, (UDINT)&AdaptedSwitches[nDevIndex].rLastOnPos, sizeof(AdaptedSwitches[nDevIndex].rLastOnPos));
								BonDev.nNextPeriod[nDevIndex]	= (UDINT)((BonDev.rProductLen[nDevIndex] * rFACTOR_MM_TO_AXIS_UNITS) + BonDev.nGap[nDevIndex]);								
								BonDev.fbBonDevDCS[nDevIndex].Switches.Period		= BonDev.nNextPeriod[nDevIndex];
								BonDev.nIdxNextPeriod[nDevIndex]					= 1;													// Index der n�chsten Periode	
								BonDev.fbBonDevDCS[nDevIndex].InitSwitches			= 1;
								bStartPosValid[nDevIndex]							= 1;
								bPeriodInit[nDevIndex]								= 1;
								gBondingDev.State.nEntryCnt[nDevIndex]				= gBondingDev.State.nEntryCnt[nDevIndex] + 1;
								gBondingDev.State.nCurrentFifoCntFlag[nDevIndex]	= gBondingDev.State.nEntryCnt[nDevIndex];
							}
							bCalcPeriod[nDevIndex]								= 0;
						}
					}
					
					
					// --------------------------------------------------------------------------------------------------------
					// --------------------------------------
					// ---------- Produkterkennung ----------
					// --------------------------------------
					if (gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.bProductDetExist == TRUE)
					{
						// Sensortyp feststellen
						// Triggersensor
						if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR)
						{
							// Die Anzahl der Eintr�ge im Trigger-FIFO sind gr��er als der Produkterkennungs-Z�hler
							if (gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount > gBondingDev.State.nProDetEntryCnt[nDevIndex])
							{
								// Der FIFO-Eintrag mit dem Index "gBondingDev.State.nProDetEntryCnt[nDevIndex]" (Produkterkennungs-Z�hler) wurde noch nicht ausgelesen
								if (ProductDetect.bProductDetected[nDevIndex] == 0)
								{
									BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource], gBondingDev.State.nProDetEntryCnt[nDevIndex], (UDINT)&ProductDetect.FifoType.FifoEntryStandard[nDevIndex].nPosToBond);
									// Position f�r Produkterkennungssensor berechnen
									ProductDetect.nCalcPosProDetect[nDevIndex] = ProductDetect.FifoType.FifoEntryStandard[nDevIndex].nPosToBond + ((DINT)(gPar.ProductPar.BonDevPar[nDevIndex].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS) - gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nOffset);
									// FIFO-Eintrag wurde ausgelesen
									ProductDetect.bProductDetected[nDevIndex] = 1;
									// derzeitigen Z�hlerwert des Produkterkennungssensors merken
									ProductDetect.nProductCnt[nDevIndex] = IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect];
								}
								
								// Abstand zwischen dem �berwachten Produkt zu dem Produkterkennungssensor berechnen
								ProductDetect.nDistanceProductToProDetSensor[nDevIndex] = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[nDevIndex].nIdxEncoder] - (ProductDetect.nCalcPosProDetect[nDevIndex] + (DINT)(gBondingDev.State.rVelocitiy[nDevIndex] * rREAD_LATENCY));
							
								// ----------------
								// Produkterkennung
								// ----------------
								// Das �berwachte Produkt sollte sich im Toleranzbereich am Produkterkennungs-Sensor befinden -> Pr�fen ob das Produkt vorhanden ist
								if( (ProductDetect.nDistanceProductToProDetSensor[nDevIndex] > (-nPRODUCT_DET_BOTTOM_LIMIT)) &&
									(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] < nPRODUCT_DET_LIMIT) )
								{
									// Innerhalb des Toleranzbereiches wurde ein Produkt vom Produkterkennungssensor erfasst -> Produkt ist vorhanden
									if (IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect] != ProductDetect.nProductCnt[nDevIndex])
									{
										// Flag setzen: Produkt darf geklebt werden
										gTriggerSensor.Par.PosBondDev[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoPos[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bEditProdukt = 1;
										// Flag setzen: Produkt wurde erkannt
										gTriggerSensor.Par.PosBondDev[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoPos[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bProductDetected = 1;
										// Flag zur�cksetzen: N�chster Eintrag im Druckmarken-FIFO muss ausgelesen werden
										ProductDetect.bProductDetected[nDevIndex] = 0;
										// Produkterkennungssensor-Z�hler erh�hen um den n�chsten vorhandenen Eintrag auslesen zu k�nnen
										gBondingDev.State.nProDetEntryCnt[nDevIndex] += 1;
										// derzeitigen Z�hlerwert des Produkterkennungssensors merken
										ProductDetect.nProductCnt[nDevIndex] = IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect];
										// Debug/Info-Variable: An welcher relativen Position innerhalb des Toleranzbereiches wurde das Produkt erkannt
										ProductDetect.rProductInWindow[nDevIndex] = (REAL)(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] / rFACTOR_MM_TO_AXIS_UNITS);
									}	
								}
								// Das Produkt kam nicht innerhalb des Toleranzbereiches am Produkterkennungssensor an -> kein Produkt vorhanden
								else if( ProductDetect.nDistanceProductToProDetSensor[nDevIndex] > nPRODUCT_DET_LIMIT )
								{
									// Flag setzen: Produkt darf nicht geklebt werden
									gTriggerSensor.Par.PosBondDev[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoPos[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bEditProdukt = 0;
									gTriggerSensor.Par.PosBondDev[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoPos[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bProductDetected = 1;
									// Flag zur�cksetzen: N�chster Eintrag im Druckmarken-FIFO muss ausgelesen werden
									ProductDetect.bProductDetected[nDevIndex] = 0;
									// Produkterkennungssensor-Z�hler erh�hen um den n�chsten vorhandenen Eintrag auslesen zu k�nnen
									gBondingDev.State.nProDetEntryCnt[nDevIndex] += 1;
									// Debug/Info-Variable: An welcher relativen Position au�erhalb des Toleranzbereiches wurde das Produkt erkannt
									ProductDetect.rProductOutOfWindow[nDevIndex] = (REAL)(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] / rFACTOR_MM_TO_AXIS_UNITS);
								}
							}
						}
						// rotatorischer Klebeauftrag
						else if(ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER)
						{
							// Die Anzahl der Eintr�ge im Encoder-FIFO sind gr��er als der Produkterkennungs-Z�hler
							if (gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder].nEntryCount > gBondingDev.State.nProDetEntryCnt[nDevIndex])
							{
								// Der FIFO-Eintrag mit dem Index "gBondingDev.State.nProDetEntryCnt[nDevIndex]" (Produkterkennungs-Z�hler) wurde noch nicht ausgelesen
								if (ProductDetect.bProductDetected[nDevIndex] == 0)
								{
									BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder], gBondingDev.State.nProDetEntryCnt[nDevIndex], (UDINT)&ProductDetect.FifoType.FifoEntryStandard[nDevIndex].nPosToBond);
									// Position f�r Produkterkennungssensor berechnen
									ProductDetect.nCalcPosProDetect[nDevIndex] = ProductDetect.FifoType.FifoEntryStandard[nDevIndex].nPosToBond + ((DINT)(gPar.ProductPar.BonDevPar[nDevIndex].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS) - gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nOffset);
									// FIFO-Eintrag wurde ausgelesen
									ProductDetect.bProductDetected[nDevIndex] = 1;
									// derzeitigen Z�hlerwert des Produkterkennungssensors merken
									ProductDetect.nProductCnt[nDevIndex] = IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect];
								}
								
								// Abstand zwischen dem �berwachten Produkt zu dem Produkterkennungssensor berechnen
									ProductDetect.nDistanceProductToProDetSensor[nDevIndex] = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[nDevIndex].nIdxEncoder] - (ProductDetect.nCalcPosProDetect[nDevIndex] + (DINT)(gBondingDev.State.rVelocitiy[nDevIndex] * rREAD_LATENCY));
								
								// ----------------
								// Produkterkennung
								// ----------------
								// Das �berwachte Produkt sollte sich im Toleranzbereich am Produkterkennungs-Sensor befinden -> Pr�fen ob das Produkt vorhanden ist
								if( (ProductDetect.nDistanceProductToProDetSensor[nDevIndex] > (-nPRODUCT_DET_BOTTOM_LIMIT)) &&
									(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] < nPRODUCT_DET_LIMIT) )
								{
									// Innerhalb des Toleranzbereiches wurde ein Produkt vom Produkterkennungssensor erfasst -> Produkt ist vorhanden
									if (IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect] != ProductDetect.nProductCnt[nDevIndex])
									{
										// Flag setzen: Produkt darf geklebt werden
										gEncoder.Par.EncFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxEncoder].FifoPos[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bEditProdukt = 1;
										// Flag zur�cksetzen: N�chster Eintrag im Druckmarken-FIFO muss ausgelesen werden
										ProductDetect.bProductDetected[nDevIndex] = 0;
										// Produkterkennungssensor-Z�hler erh�hen um den n�chsten vorhandenen Eintrag auslesen zu k�nnen
										gBondingDev.State.nProDetEntryCnt[nDevIndex] += 1;
										// derzeitigen Z�hlerwert des Produkterkennungssensors merken
										ProductDetect.nProductCnt[nDevIndex] = IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect];
										// Debug/Info-Variable: An welcher relativen Position innerhalb des Toleranzbereiches wurde das Produkt erkannt
										ProductDetect.rProductInWindow[nDevIndex] = (REAL)(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] / rFACTOR_MM_TO_AXIS_UNITS);
									}	
								}
								// Das Produkt kam nicht innerhalb des Toleranzbereiches am Produkterkennungssensor an -> kein Produkt vorhanden
								else if( ProductDetect.nDistanceProductToProDetSensor[nDevIndex] > nPRODUCT_DET_LIMIT )
								{
									// Flag zur�cksetzen: N�chster Eintrag im Druckmarken-FIFO muss ausgelesen werden
									ProductDetect.bProductDetected[nDevIndex] = 0;
									// Produkterkennungssensor-Z�hler erh�hen um den n�chsten vorhandenen Eintrag auslesen zu k�nnen
									gBondingDev.State.nProDetEntryCnt[nDevIndex] += 1;
									// Debug/Info-Variable: An welcher relativen Position au�erhalb des Toleranzbereiches wurde das Produkt erkannt
									ProductDetect.rProductOutOfWindow[nDevIndex] = (REAL)(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] / rFACTOR_MM_TO_AXIS_UNITS);					
								}
							}						
						}
						// Druckmarkenabh�ngiges Kleben
						else if( ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR )
						{
							// Die Anzahl der Eintr�ge im Druckmarken-FIFO sind gr��er als der Produkterkennungs-Z�hler
							if (gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount > gBondingDev.State.nProDetEntryCnt[nDevIndex])
							{
								// Der FIFO-Eintrag mit dem Index "gBondingDev.State.nProDetEntryCnt[nDevIndex]" (Produkterkennungs-Z�hler) wurde noch nicht ausgelesen
								if (ProductDetect.bProductDetected[nDevIndex] == 0)
								{
									BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource], gBondingDev.State.nProDetEntryCnt[nDevIndex], (UDINT)&ProductDetect.FifoType.FifoEntryPm[nDevIndex].nStartPosFromTs);
									// Position f�r Produkterkennungssensor berechnen
									ProductDetect.nCalcPosProDetect[nDevIndex] = ProductDetect.FifoType.FifoEntryPm[nDevIndex].nStartPosFromTs + ((DINT)(gPar.ProductPar.BonDevPar[nDevIndex].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS) - gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nOffset);
									ProductDetect.nCalcPosProDetect[nDevIndex] = ProductDetect.nCalcPosProDetect[nDevIndex] - (gPar.ProductPar.BonDevPar[nDevIndex].rPmProductOffset * rFACTOR_MM_TO_AXIS_UNITS);		// Abstand Produktbeginn zu Druckmarke abziehen
									// FIFO-Eintrag wurde ausgelesen
									ProductDetect.bProductDetected[nDevIndex] = 1;
									// derzeitigen Z�hlerwert des Produkterkennungssensors merken
									ProductDetect.nProductCnt[nDevIndex] = IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect];
								}
								
								// Abstand zwischen dem �berwachten Produkt zu dem Produkterkennungssensor berechnen
								ProductDetect.nDistanceProductToProDetSensor[nDevIndex] = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[nDevIndex].nIdxEncoder] - (ProductDetect.nCalcPosProDetect[nDevIndex] + (DINT)(gBondingDev.State.rVelocitiy[nDevIndex] * rREAD_LATENCY));
								
								// ----------------
								// Produkterkennung
								// ----------------
								// Das �berwachte Produkt sollte sich im Toleranzbereich am Produkterkennungs-Sensor befinden -> Pr�fen ob das Produkt vorhanden ist
								if( (ProductDetect.nDistanceProductToProDetSensor[nDevIndex] > (-nPRODUCT_DET_BOTTOM_LIMIT)) &&
									(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] < nPRODUCT_DET_LIMIT) )
								{
									// Innerhalb des Toleranzbereiches wurde ein Produkt vom Produkterkennungssensor erfasst -> Produkt ist vorhanden
									if (IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect] != ProductDetect.nProductCnt[nDevIndex])
									{
										// Flag setzen: Produkt darf geklebt werden
										gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoEntry[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bEditProdukt = 1;
										// Flag setzen: Produkt wurde erkannt
										gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoEntry[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bProductDetected = 1;
										// Flag zur�cksetzen: N�chster Eintrag im Druckmarken-FIFO muss ausgelesen werden
										ProductDetect.bProductDetected[nDevIndex] = 0;
										// Produkterkennungssensor-Z�hler erh�hen um den n�chsten vorhandenen Eintrag auslesen zu k�nnen
										gBondingDev.State.nProDetEntryCnt[nDevIndex] += 1;
										// derzeitigen Z�hlerwert des Produkterkennungssensors merken
										ProductDetect.nProductCnt[nDevIndex] = IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect];
										// Debug/Info-Variable: An welcher relativen Position innerhalb des Toleranzbereiches wurde das Produkt erkannt
										ProductDetect.rProductInWindow[nDevIndex] = (REAL)(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] / rFACTOR_MM_TO_AXIS_UNITS);
									}	
								}
								// Das Produkt kam nicht innerhalb des Toleranzbereiches am Produkterkennungssensor an -> kein Produkt vorhanden
								else if( ProductDetect.nDistanceProductToProDetSensor[nDevIndex] > nPRODUCT_DET_LIMIT )
								{
									// Flag setzen: Produkt darf nicht geklebt werden
									gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoEntry[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bEditProdukt = 0;
									gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoEntry[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bProductDetected = 1;
									// Flag zur�cksetzen: N�chster Eintrag im Druckmarken-FIFO muss ausgelesen werden
									ProductDetect.bProductDetected[nDevIndex] = 0;
									// Produkterkennungssensor-Z�hler erh�hen um den n�chsten vorhandenen Eintrag auslesen zu k�nnen
									gBondingDev.State.nProDetEntryCnt[nDevIndex] += 1;
									// Debug/Info-Variable: An welcher relativen Position au�erhalb des Toleranzbereiches wurde das Produkt erkannt
									ProductDetect.rProductOutOfWindow[nDevIndex] = (REAL)(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] / rFACTOR_MM_TO_AXIS_UNITS);
								}
							}						
						}
						// Druckmarken- und Triggerabh�ngiges Kleben
						else if( ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
						{
							// Die Anzahl der Eintr�ge im Druckmarken-FIFO sind gr��er als der Produkterkennungs-Z�hler
							if (gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].nEntryCount > gBondingDev.State.nProDetEntryCnt[nDevIndex])
							{
								// Dieser Eintrag aus dem Druckmarken-FIFO wurde bereits vom Trigger-Sensor mit der Position f�r den Produktbeginn beschrieben
								if( gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].FifoEntry[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bProductPositionSetByTrigger == TRUE )
								{
									// Der FIFO-Eintrag mit dem Index "gBondingDev.State.nProDetEntryCnt[nDevIndex]" (Produkterkennungs-Z�hler) wurde noch nicht ausgelesen
									if (ProductDetect.bProductDetected[nDevIndex] == 0)
									{
										BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource], gBondingDev.State.nProDetEntryCnt[nDevIndex], (UDINT)&ProductDetect.FifoType.FifoEntryPm[nDevIndex].nStartPosFromTs);
										// Position f�r Produkterkennungssensor berechnen
	 									// Es muss der Produktbeginn (nPositionProductBegins) aus dem FIFO verwendet werden, und nicht die Position der Druckmarke, da diese bei zwei unterschiedlichen Gebern eine komplett andere Position abbildet. 
										ProductDetect.nCalcPosProDetect[nDevIndex] = ProductDetect.FifoType.FifoEntryPm[nDevIndex].nPositionProductBegins + ((DINT)(gPar.ProductPar.BonDevPar[nDevIndex].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS) - gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nOffset);
										// FIFO-Eintrag wurde ausgelesen
										ProductDetect.bProductDetected[nDevIndex] = 1;
										// derzeitigen Z�hlerwert des Produkterkennungssensors merken
										ProductDetect.nProductCnt[nDevIndex] = IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect];
									}
									// Abstand zwischen dem �berwachten Produkt zu dem Produkterkennungssensor berechnen
									ProductDetect.nDistanceProductToProDetSensor[nDevIndex] = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[nDevIndex].nIdxEncoder] - (ProductDetect.nCalcPosProDetect[nDevIndex] + (DINT)(gBondingDev.State.rVelocitiy[nDevIndex] * rREAD_LATENCY));
								
									// ----------------
									// Produkterkennung
									// ----------------
									// Das �berwachte Produkt sollte sich im Toleranzbereich am Produkterkennungs-Sensor befinden -> Pr�fen ob das Produkt vorhanden ist
									if( (ProductDetect.nDistanceProductToProDetSensor[nDevIndex] > (-nPRODUCT_DET_BOTTOM_LIMIT)) &&
										(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] < nPRODUCT_DET_LIMIT) )
									{
										// Innerhalb des Toleranzbereiches wurde ein Produkt vom Produkterkennungssensor erfasst -> Produkt ist vorhanden
										if (IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect] != ProductDetect.nProductCnt[nDevIndex])
										{
											// Flag setzen: Produkt darf geklebt werden
											gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].FifoEntry[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bEditProdukt = 1;
											// Flag setzen: Produkt wurde erkannt
											gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].FifoEntry[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bProductDetected = 1;
											// Flag zur�cksetzen: N�chster Eintrag im Druckmarken-FIFO muss ausgelesen werden
											ProductDetect.bProductDetected[nDevIndex] = 0;
											// Produkterkennungssensor-Z�hler erh�hen um den n�chsten vorhandenen Eintrag auslesen zu k�nnen
											gBondingDev.State.nProDetEntryCnt[nDevIndex] += 1;
											// derzeitigen Z�hlerwert des Produkterkennungssensors merken
											ProductDetect.nProductCnt[nDevIndex] = IO.Input.nProDetRisingCnt[gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.nIdxProductDetect];
											// Debug/Info-Variable: An welcher relativen Position innerhalb des Toleranzbereiches wurde das Produkt erkannt
											ProductDetect.rProductInWindow[nDevIndex] = (REAL)(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] / rFACTOR_MM_TO_AXIS_UNITS);
										}	
									}
									// Das Produkt kam nicht innerhalb des Toleranzbereiches am Produkterkennungssensor an -> kein Produkt vorhanden
									else if( ProductDetect.nDistanceProductToProDetSensor[nDevIndex] > nPRODUCT_DET_LIMIT )
									{
										// Flag setzen: Produkt darf nicht geklebt werden
										gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].FifoEntry[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bEditProdukt = 0;
										gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].FifoEntry[gBondingDev.State.nProDetEntryCnt[nDevIndex]].bProductDetected = 1;
										// Flag zur�cksetzen: N�chster Eintrag im Druckmarken-FIFO muss ausgelesen werden
										ProductDetect.bProductDetected[nDevIndex] = 0;
										// Produkterkennungssensor-Z�hler erh�hen um den n�chsten vorhandenen Eintrag auslesen zu k�nnen
										gBondingDev.State.nProDetEntryCnt[nDevIndex] += 1;
										// Debug/Info-Variable: An welcher relativen Position au�erhalb des Toleranzbereiches wurde das Produkt erkannt
										ProductDetect.rProductOutOfWindow[nDevIndex] = (REAL)(ProductDetect.nDistanceProductToProDetSensor[nDevIndex] / rFACTOR_MM_TO_AXIS_UNITS);
									}
								}
							}						
						}
					}
					// --------------------------------------------------------------------------------------------------------
					
					
					// Ge�nderte "Switches" wurden �bernommen
					if (BonDev.fbBonDevDCS[nDevIndex].SwitchesInitialized == 1)
					{
						BonDev.fbBonDevDCS[nDevIndex].InitSwitches = 0;
					}
					
					// EnableTrack ermitteln
					if (BonDev.nIdxCurrentPeriod[nDevIndex] == 2)
					{
						BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
					}
					// Erste Produktposition wurde ermittelt
					if ((BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition > nLAST_ON_POS_DUMMY_START[0]) &&
						(BonDev.nIdxCurrentPeriod[nDevIndex] == 0) &&
						(bStartPosValid[nDevIndex] == 1))
					{
						if ((gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.bProductDetExist == 0) ||
							(ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER))
						{
							BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 1;												// Freigabe der Ausg�nge erteilen --> Neues Produkt
						}
					}
					// Enable Track ermitteln
					if ((((BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition > nLAST_ON_POS_DUMMY[0]) &&
						(BonDev.nIdxCurrentPeriod[nDevIndex] == 2)) ||
						(BonDev.nIdxCurrentPeriod[nDevIndex] == 1)) &&
						(BonDev.nIdxNextPeriod[nDevIndex] == 1))
					{
						if ((gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.bProductDetExist == 0) ||
							(ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER))
						{
							BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 1;												// Freigabe der Ausg�nge erteilen
						}
					}
					// Enable Track ermitteln wenn Produkterkennung aktiv ist
					// Wird aktualisiert wenn ein neues Produkt (bNewPeriod) initialisiert wurde
					if ((gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.bProductDetExist == 1) &&
						(BonDev.nIdxCurrentPeriod[nDevIndex] == 1))
					{	
						if ((ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR) &&
						(gTriggerSensor.Par.PosBondDev[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoPos[gBondingDev.State.nCurrentFifoCnt[nDevIndex] - 1].bProductDetected == 1))
						{
							BonDev.fbBonDevDCS[nDevIndex].EnableTrack =	gTriggerSensor.Par.PosBondDev[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoPos[gBondingDev.State.nCurrentFifoCnt[nDevIndex] - 1].bEditProdukt;
						}
						else if( (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR) &&
								 (gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoEntry[gBondingDev.State.nCurrentFifoCnt[nDevIndex] - 1].bProductDetected == 1) )
						{
							BonDev.fbBonDevDCS[nDevIndex].EnableTrack = gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxSenSource].FifoEntry[gBondingDev.State.nCurrentFifoCnt[nDevIndex] - 1].bEditProdukt;
						}
						else if( (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) &&
								 (gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].FifoEntry[gBondingDev.State.nCurrentFifoCnt[nDevIndex] - 1].bProductDetected == 1) )
						{
							BonDev.fbBonDevDCS[nDevIndex].EnableTrack = gPrintMarkSensor.Par.PmFifo[gPar.ProductPar.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].FifoEntry[gBondingDev.State.nCurrentFifoCnt[nDevIndex] - 1].bEditProdukt;
						}
					}
					
					
					// ------------ Pr�fen ob der Klebekopf kleben darf ------------
						// Freigabegeschwindigkeit
					if( (BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.Velocity <= (gPar.MachinePar.rEnVelocity * rFACTOR_MM_TO_AXIS_UNITS)) ||
						// Die generelle Maschinenfreigabe f�r das Kleben ist nicht vorhanden
						(IO.Input.bBonDevEnable == FALSE) ||
						// �ber die Maschinenschnittstelle ist dieser Klebekopf nicht zum Kleben freigegeben
						(gExtCtrlIf.State.bBondingNotAllowed[nDevIndex] == TRUE) )
					{
						BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
					}
					
					// -----------------------------------------
					// DCS ausf�hren
					// -----------------------------------------
					BonDev.fbBonDevDCS[nDevIndex].Position.Integer = gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[nDevIndex].nIdxEncoder];
					ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
					gBondingDev.State.rVelocitiy[nDevIndex] = BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.Velocity;
					if (BonDev.fbBonDevDCS[nDevIndex].Error == 1)																			// Fehler aufgetreten
					{
						AppSetEvent(eEVT_ERR_BONDING_DEVICE, BonDev.fbBonDevDCS[nDevIndex].ErrorID, "", (nDevIndex + 1), &gEventManagement);		// Meldung absetzen 
						AppResetEvent(eEVT_ERR_BONDING_DEVICE, &gEventManagement);
						
						BonDev.bDcsInitOk[nDevIndex]				= 0;
						gBondingDev.State.bBonDevError[nDevIndex]	= 1;
						bStartPosValid[nDevIndex] 					= 0;																	// Startposition muss neu bestimmt werden
						bGetStartPos[nDevIndex] 					= 0;
						// DCS Eing�nge abl�schen
						BonDev.fbBonDevDCS[nDevIndex].Enable = 0;
						BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
						BonDev.fbBonDevDCS[nDevIndex].InitSwitches = 0;
						brsmemset((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches, 0, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches));
						ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
						gMainLogic.DirectBox.bStop = 1;
					}
					
					
					// -----------------------------------------
					// L�nge der n�chsten Periode ermitteln
					// -----------------------------------------
					if (((DINT)(BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition + 1000) < (DINT)rPosDcs[nDevIndex]) &&							// Wird einmal ausgef�hrt bei Periodenbeginn
						(bStartPosValid[nDevIndex] == 1))
					{
						bGetPeriod[nDevIndex]							= 1;																			// Berechnung der Periodenl�nge starten
						BonDev.nIdxCurrentPeriod[nDevIndex]				= BonDev.nIdxNextPeriod[nDevIndex];												// N�chsten Periodenindex �bernehmen
						BonDev.nCurrentPeriod[nDevIndex]				= BonDev.nNextPeriod[nDevIndex];
						// Nur wenn ein Produkt bearbeitet wird soll der Z�hler �bernommen werden
						if (BonDev.nIdxCurrentPeriod[nDevIndex]	== 1)
						{
						gBondingDev.State.nCurrentFifoCnt[nDevIndex] 	= gBondingDev.State.nCurrentFifoCntFlag[nDevIndex];
						}
					}
					
					if ((bGetPeriod[nDevIndex] == 1) &&
//						(BonDev.fbBonDevDCS[nDevIndex].InitSwitches == 0) &&
						(BonDev.fbBonDevDCS[nDevIndex].SwitchesInitialized == 0) &&
						(BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition > 0))
					{
						// Sensortyp feststellen
						if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR)													// Trigger Sensor
						{
							if (gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount > gBondingDev.State.nEntryCnt[nDevIndex])
							{
								// Position aus Fifo lesen
								BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManTrSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource], gBondingDev.State.nEntryCnt[nDevIndex], (UDINT)&BonDev.FifoStruct.FifoEntryStandard[nDevIndex]);	
								// Strecke zwischen zwei Produkten bilden, um falsche Trigger zu ignorieren
								BonDev.nDiffLen[nDevIndex] 	= BonDev.FifoStruct.FifoEntryStandard[nDevIndex].nPosToBond - BonDev.nDiffLatchPos[nDevIndex];
								if (BonDev.nDiffLen[nDevIndex] > (DINT)(BonDev.rProductLen[nDevIndex] * rFACTOR_MM_TO_AXIS_UNITS))
								{
									BonDev.nDiffLatchPos[nDevIndex] = BonDev.FifoStruct.FifoEntryStandard[nDevIndex].nPosToBond;
									
									bEnableTrack[nDevIndex] = BonDev.FifoStruct.FifoEntryStandard[nDevIndex].bEditProdukt;
									BonDev.nFifoPos[nDevIndex] = BonDev.FifoStruct.FifoEntryStandard[nDevIndex].nPosToBond;
									BonDev.rProductLen[nDevIndex] = ProductParDcs.BonDevPar[nDevIndex].rProductLenAdapted;
									BonDev.nSwitchIdx[nDevIndex] = 0;
									rNwCompensation = rNETWORK_COMPENSATION + 0.001;				// Netzwerkkompensation --> Werte wurden empirisch ermitelt 
									bCalcPeriod[nDevIndex] = 1;
								}
								else
								{
									gBondingDev.State.nEntryCnt[nDevIndex] = gBondingDev.State.nEntryCnt[nDevIndex] + 1;
								}
							}					
						}
						else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER)													// Geber als Quelle
						{
							if (gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder].nEntryCount > gBondingDev.State.nEntryCnt[nDevIndex])
							{
								BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManEnc[ProductParDcs.BonDevPar[nDevIndex].nIdxEncoder], gBondingDev.State.nEntryCnt[nDevIndex], (UDINT)&BonDev.FifoStruct.FifoEntryStandard[nDevIndex]);
								bEnableTrack[nDevIndex] = BonDev.FifoStruct.FifoEntryStandard[nDevIndex].bEditProdukt;
								BonDev.nFifoPos[nDevIndex] = BonDev.FifoStruct.FifoEntryStandard[nDevIndex].nPosToBond;
								BonDev.rProductLen[nDevIndex] = ProductParDcs.BonDevPar[nDevIndex].rCylinderPerimeter;
								BonDev.nSwitchIdx[nDevIndex] = 0;
								rNwCompensation = rNETWORK_COMPENSATION + 0.002;				// Netzwerkkompensation --> Werte wurden empirisch ermitelt 
								bCalcPeriod[nDevIndex] = 1;
							}
						}
						else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR)											// Druckmarkensensor
						{
							if (gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource].nEntryCount > gBondingDev.State.nEntryCnt[nDevIndex])
							{
								BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxSenSource], gBondingDev.State.nEntryCnt[nDevIndex], (UDINT)&BonDev.FifoStruct.FifoEntryPm[nDevIndex]);
								bEnableTrack[nDevIndex] = BonDev.FifoStruct.FifoEntryPm[nDevIndex].bEditProdukt;
								BonDev.nFifoPos[nDevIndex] = BonDev.FifoStruct.FifoEntryPm[nDevIndex].nStartPosFromTs - (gPar.ProductPar.BonDevPar[nDevIndex].rPmProductOffset * rFACTOR_MM_TO_AXIS_UNITS);			// Abstand Produktbeginn zu Druckmarke abziehen
								BonDev.rProductLen[nDevIndex] = ProductParDcs.BonDevPar[nDevIndex].rProductLenAdapted;
								BonDev.nSwitchIdx[nDevIndex] = BonDev.FifoStruct.FifoEntryPm[nDevIndex].nPmIdx;
								rNwCompensation = rNETWORK_COMPENSATION + 0.001;				// Netzwerkkompensation --> Werte wurden empirisch ermitelt 
								bCalcPeriod[nDevIndex] = 1;
							}
						}
						else if( ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
						{
							if( gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource].nEntryCount > gBondingDev.State.nEntryCnt[nDevIndex] )
							{
								BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManPmSen[ProductParDcs.BonDevPar[nDevIndex].nIdxPrintMarkSensorSource], gBondingDev.State.nEntryCnt[nDevIndex], (UDINT)&BonDev.FifoStruct.FifoEntryPm[nDevIndex]);
								if( BonDev.FifoStruct.FifoEntryPm[nDevIndex].bProductPositionSetByTrigger == TRUE )
								{
									BonDev.nFifoPos[nDevIndex] = BonDev.FifoStruct.FifoEntryPm[nDevIndex].nPositionProductBegins;
									BonDev.rProductLen[nDevIndex] = ProductParDcs.BonDevPar[nDevIndex].rProductLen;
									BonDev.nSwitchIdx[nDevIndex] = BonDev.FifoStruct.FifoEntryPm[nDevIndex].nPmIdx;
									rNwCompensation = rNETWORK_COMPENSATION + 0.001;				// Netzwerkkompensation --> Werte wurden empirisch ermitelt;
									// G�ltiger Druckmarken-Index in Fifo-Eintrag enthalten
									if( BonDev.nSwitchIdx[nDevIndex] >= 0 )
									{
										// Perioden werden neu berechnet
										bCalcPeriod[nDevIndex] = 1;
									}
									// Der Fifo-Eintrag enth�lt einen ung�ltigen Druckmarken-Index ( -1 = ung�ltige Druckmarke)
									else
									{
										// Diesen Fifo-Eintrag �berspringen und somit nicht bearbeiten
										gBondingDev.State.nEntryCnt[nDevIndex] += 1;
									}
								}
							}
						}
						// N�chste Periodenl�nge = Produktl�nge + Zwischenabstand
						if (bCalcPeriod[nDevIndex] == 1)
						{
							BonDev.nRemainingDistance[nDevIndex] = BonDev.nCurrentPeriod[nDevIndex] - (UDINT)Round(BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition);
							BonDev.nGap[nDevIndex] = ((BonDev.nFifoPos[nDevIndex] + (DINT)(gPar.ProductPar.BonDevPar[nDevIndex].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS)) - (gEncoder.State.nAxisPos[gPar.ProductPar.BonDevPar[nDevIndex].nIdxEncoder] + BonDev.nRemainingDistance[nDevIndex]));
							BonDev.nGap[nDevIndex] = BonDev.nGap[nDevIndex] - (rNwCompensation * gBondingDev.State.rVelocitiy[nDevIndex]);
							// Hier die Einschaltkompensation des Kopfes (�ffnungszeit * Geschwindigkeit) einrechnen (abziehen von GAP)
							BonDev.nGap[nDevIndex] -= gPar.ProductPar.BonDevPar[nDevIndex].rValveCompensationOpenTime / 1000 * gBondingDev.State.rVelocitiy[nDevIndex];
							
							// Abstand zwischen zwei Produkten ist negativ -> Fehler ausgeben
							if (BonDev.nGap[nDevIndex] < 0)
							{
								if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR)
								{
									AppSetEvent(eEVT_WAR_NEG_PRODUCT_DISTANCE, 0, "Trigger sensor", (nDevIndex + 1), &gEventManagement);					// Meldung absetzen 
								}
								else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_ENCODER)
								{
									AppSetEvent(eEVT_WAR_NEG_PRODUCT_DISTANCE, 0, "Encoder", (nDevIndex + 1), &gEventManagement);
								}
								else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR)
								{
									AppSetEvent(eEVT_WAR_NEG_PRODUCT_DISTANCE, 0, "Print mark sensor", (nDevIndex + 1), &gEventManagement);
								}
								else if (ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR)
								{
									AppSetEvent(eEVT_WAR_NEG_PRODUCT_DISTANCE, 0, "Print mark and trigger sensor", (nDevIndex + 1), &gEventManagement);
								}
								AppResetEvent(eEVT_WAR_NEG_PRODUCT_DISTANCE, &gEventManagement);
								
								// Es wird kein Fehler mehr ausgegeben, stattdessen wird eine Warnung ausgegeben.
								
//								BonDev.bDcsInitOk[nDevIndex]				= 0;
//								gBondingDev.State.bBonDevError[nDevIndex]	= 1;
//								bStartPosValid[nDevIndex] 					= 0;
//								bGetStartPos[nDevIndex] 					= 0;
//								// DCS Eing�nge abl�schen
//								BonDev.fbBonDevDCS[nDevIndex].Enable = 0;
//								BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
//								BonDev.fbBonDevDCS[nDevIndex].InitSwitches = 0;
//								brsmemset((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches, 0, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches));
//								ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
//								gMainLogic.DirectBox.bStop = 1;
								
								// Bei neg. Sensorabstand -> Dummy Periode einf�gen
								BonDev.nNextPeriod[nDevIndex] 					= (DINT)(nMIN_SENSOR_DISTANCE / 2);
								BonDev.nIdxNextPeriod[nDevIndex]				= 2;																	// Index der n�chsten Periode	
								BonDev.fbBonDevDCS[nDevIndex].Switches.Period	= BonDev.nNextPeriod[nDevIndex];
								brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition, (UDINT)&nFIRST_ON_POS_DUMMY_START, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition));
								brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition, (UDINT)&nLAST_ON_POS_DUMMY_START, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition));						
								BonDev.fbBonDevDCS[nDevIndex].InitSwitches		= 1;
								bPeriodInit[nDevIndex]							= 0;
								gBondingDev.State.nEntryCnt[nDevIndex]			= gBondingDev.State.nEntryCnt[nDevIndex] + 2;						// Zur Sicherheit werden 2+1 (unten) Produkte ausgelassen, falls Dummy-Periode > Produktl�nge ist
							}							
							else
							{
								// Nockenpositionen anpassen
								brsmemset((UDINT)&AdaptedSwitches[nDevIndex], 0, sizeof(AdaptedSwitches[nDevIndex]));
								for (nSwitchIndex = 0; nSwitchIndex <= gPar.ProductPar.BonDevPar[nDevIndex].nSwitchIndexMax[BonDev.nSwitchIdx[nDevIndex]]; nSwitchIndex ++)
								{
									if ((SwitchesBuffer[nDevIndex].SwitchesIdx[BonDev.nSwitchIdx[nDevIndex]].rFirstOnPos[nSwitchIndex] == 0) &&
										(SwitchesBuffer[nDevIndex].SwitchesIdx[BonDev.nSwitchIdx[nDevIndex]].rLastOnPos[nSwitchIndex] == 0))				// Nur bei g�ltigen Werten �bernehmn
									{
										AdaptedSwitches[nDevIndex].rFirstOnPos[nSwitchIndex]	= AdaptedSwitches[nDevIndex].rFirstOnPos[nSwitchIndex];
										AdaptedSwitches[nDevIndex].rLastOnPos[nSwitchIndex]		= AdaptedSwitches[nDevIndex].rLastOnPos[nSwitchIndex];
									}
									else
									{
										AdaptedSwitches[nDevIndex].rFirstOnPos[nSwitchIndex]	= SwitchesBuffer[nDevIndex].SwitchesIdx[BonDev.nSwitchIdx[nDevIndex]].rFirstOnPos[nSwitchIndex] + (REAL)BonDev.nGap[nDevIndex];
										AdaptedSwitches[nDevIndex].rLastOnPos[nSwitchIndex] 	= SwitchesBuffer[nDevIndex].SwitchesIdx[BonDev.nSwitchIdx[nDevIndex]].rLastOnPos[nSwitchIndex] + (REAL)BonDev.nGap[nDevIndex];
										// Hier von rLastOnPos die Ausschaltkompensation mit abziehen
										AdaptedSwitches[nDevIndex].rLastOnPos[nSwitchIndex] -= gPar.ProductPar.BonDevPar[nDevIndex].rValveCompensationCloseTime / 1000 * gBondingDev.State.rVelocitiy[nDevIndex];
									
										// Durch die Ausschalt-Kompensation w�rde der Leimstrich negativ lange sein
										// Dies kann bei sehr kurzen Klebestrichen auftreten
										if( AdaptedSwitches[nDevIndex].rLastOnPos[nSwitchIndex] <= AdaptedSwitches[nDevIndex].rFirstOnPos[nSwitchIndex] )
										{
											AppSetEvent( eEVT_ERR_VAL_CLOSE_COMP_TO_LARGE, 0, "", nDevIndex + 1, &gEventManagement );
											AppResetEvent( eEVT_ERR_VAL_CLOSE_COMP_TO_LARGE, &gEventManagement );
											
											BonDev.bDcsInitOk[nDevIndex]				= 0;
											gBondingDev.State.bBonDevError[nDevIndex]	= 1;
											bStartPosValid[nDevIndex] 					= 0;
											bGetStartPos[nDevIndex] 					= 0;
											// DCS Eing�nge abl�schen
											BonDev.fbBonDevDCS[nDevIndex].Enable = 0;
											BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
											BonDev.fbBonDevDCS[nDevIndex].InitSwitches = 0;
											brsmemset((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches, 0, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches));
											ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
											gMainLogic.DirectBox.bStop = 1;
										}
									
									}
								}
								
								// Produkterkennung aktiv
								if ((gPar.ProductPar.BonDevPar[nDevIndex].ProductDetection.bProductDetExist == 1) &&
									(BonDev.nIdxCurrentPeriod[nDevIndex] != 0) &&
									((ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_TRIGGER_SENSOR) ||
									(ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR) ||
									(ProductParDcs.BonDevPar[nDevIndex].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR)) )
								{
									ProductDetect.bNewPeriod[nDevIndex] = 1;
								}
						
								brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition, (UDINT)&AdaptedSwitches[nDevIndex].rFirstOnPos, sizeof(AdaptedSwitches[nDevIndex].rFirstOnPos));
								brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition, (UDINT)&AdaptedSwitches[nDevIndex].rLastOnPos, sizeof(AdaptedSwitches[nDevIndex].rLastOnPos));							
								BonDev.nNextPeriod[nDevIndex] = (UDINT)(BonDev.rProductLen[nDevIndex] * rFACTOR_MM_TO_AXIS_UNITS) + BonDev.nGap[nDevIndex];							
								BonDev.nIdxNextPeriod[nDevIndex]	= 1;																		// Index der n�chsten Periode
								BonDev.fbBonDevDCS[nDevIndex].Switches.Period	= BonDev.nNextPeriod[nDevIndex];
								BonDev.fbBonDevDCS[nDevIndex].InitSwitches		= 1;
								bPeriodInit[nDevIndex]							= 1;
								gBondingDev.State.nEntryCnt[nDevIndex]			= gBondingDev.State.nEntryCnt[nDevIndex] + 1;
								gBondingDev.State.nCurrentFifoCntFlag[nDevIndex]= gBondingDev.State.nEntryCnt[nDevIndex];
							}
							bGetPeriod[nDevIndex]							= 0;
							bCalcPeriod[nDevIndex]							= 0;
						}
						// N�chste Periodenl�nge = Minimaler Sensorabstand
						else if ((BonDev.nIdxCurrentPeriod[nDevIndex] == 1) &&
							(bPeriodInit[nDevIndex] == 1))
						{
							BonDev.nNextPeriod[nDevIndex] 					= nMIN_SENSOR_DISTANCE;
							BonDev.nIdxNextPeriod[nDevIndex]				= 2;																	// Index der n�chsten Periode	
							BonDev.fbBonDevDCS[nDevIndex].Switches.Period	= BonDev.nNextPeriod[nDevIndex];
							brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition, (UDINT)&nFIRST_ON_POS_DUMMY, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches.FirstOnPosition));
							brsmemcpy((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition, (UDINT)&nLAST_ON_POS_DUMMY, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches.LastOnPosition));						
							BonDev.fbBonDevDCS[nDevIndex].InitSwitches		= 1;
							bPeriodInit[nDevIndex]							= 0;						
						}
					}
					rPosDcs[nDevIndex] = (DINT)BonDev.fbBonDevDCS[nDevIndex].AdditionalInfo.CalculatedPosition;
				}
			}
		}
	}
	
	// ---------- Kopffreigabe wurde w�hrend Automatikbetrieb ge�ndert ----------
	for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
	{
		if (gMainLogic.State.bAutomatic == 1)
		{		
			// Kopffreigabe wurde erteilt
			if ((gPar.ProductPar.BonDevPar[nDevIndex].bBonDevEn == 1) &&
				(BonDevEn[nDevIndex].bBonDevEnFlag == 0))
			{
				BonDevEn[nDevIndex].bBonDevEnabled 		= 1;
				BonDevEn[nDevIndex].bBonDevEnFlag 		= gPar.ProductPar.BonDevPar[nDevIndex].bBonDevEn;
				BonDevEn[nDevIndex].bInitializeCounter	= 1;
			}
			// Kopffreigabe wurde abgel�scht
			else if ((gPar.ProductPar.BonDevPar[nDevIndex].bBonDevEn == 0) &&
				(BonDevEn[nDevIndex].bBonDevEnFlag == 1))
			{
				BonDevEn[nDevIndex].bBonDevDisabled 	= 1;
				BonDevEn[nDevIndex].bBonDevEnFlag 		= gPar.ProductPar.BonDevPar[nDevIndex].bBonDevEn;
			}
			
			// DCS abl�schen
			if (BonDevEn[nDevIndex].bBonDevDisabled == 1)
			{
				BonDev.fbBonDevDCS[nDevIndex].Enable 		= 0;
				BonDev.fbBonDevDCS[nDevIndex].EnableTrack 	= 0;
				BonDev.fbBonDevDCS[nDevIndex].InitSwitches 	= 0;
				BonDevEn[nDevIndex].bInitializeCounter		= 0;
				brsmemset((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches, 0, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches));
				ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
		
				bStartPosValid[nDevIndex] = 0;
				bGetStartPos[nDevIndex] = 0;
				bCalcPeriod[nDevIndex] = 0;
				BonDev.bDcsInitOk[nDevIndex] = 0;											// DCS muss neu initialisiert werden
				BonDevEn[nDevIndex].bBonDevDisabled = 0;
				
				bGetPeriod[nDevIndex] = 0;			
			}
			
			// DCS und Sensoren initialisieren
			if (BonDevEn[nDevIndex].bBonDevEnabled == 1)
			{
				// DCS
				if (BrbSetCaller(&gBondingDev.CallerBox.Caller, eCALLERID_BONDING_DEVICE) == eBRB_CALLER_STATE_OK)
				{
					gBondingDev.CallerBox.bApplyPar = 1;									// Kommando absetzen
					BonDevEn[nDevIndex].bDcsInitOk = 1;
				}
				// Trigger Sensor
				if (BrbSetCaller(&gTriggerSensor.CallerBox.Caller, eCALLERID_TRIGGER_SENSOR) == eBRB_CALLER_STATE_OK)
				{
					gTriggerSensor.CallerBox.bGetBonDevPar = 1;								// Initialisierung starten
					BonDevEn[nDevIndex].bTriggerInitOk = 1;
				}
				// Geber
				if (BrbSetCaller(&gEncoder.CallerBox.Caller, eCALLERID_ENCODER) == eBRB_CALLER_STATE_OK)
				{
					gEncoder.CallerBox.bGetBonDevPar = 1;									// Initialisierung starten
					BonDevEn[nDevIndex].bEncInitOk = 1;
				}
				// Duckmarkensensor
				if (BrbSetCaller(&gPrintMarkSensor.CallerBox.Caller, eCALLERID_PRINT_MARK_SENSOR) == eBRB_CALLER_STATE_OK)
				{
					gPrintMarkSensor.CallerBox.bGetBonDevPar = 1;							// Initialisierung starten
					BonDevEn[nDevIndex].bPmSenInitOk = 1;
				}
				// Initialisierung abgeschlossen
				if ((BonDevEn[nDevIndex].bDcsInitOk == 1) &&
					(BonDevEn[nDevIndex].bEncInitOk == 1) &&
					(BonDevEn[nDevIndex].bTriggerInitOk == 1) &&
					(BonDevEn[nDevIndex].bPmSenInitOk == 1))
				{
					BonDevEn[nDevIndex].bBonDevEnabled = 0;
					BonDevEn[nDevIndex].bDcsInitOk = 0;
					BonDevEn[nDevIndex].bEncInitOk = 0;
					BonDevEn[nDevIndex].bTriggerInitOk = 0;
					BonDevEn[nDevIndex].bPmSenInitOk = 0;
				}
			}
			else
			{
				BonDevEn[nDevIndex].bDcsInitOk = 0;
				BonDevEn[nDevIndex].bEncInitOk = 0;
				BonDevEn[nDevIndex].bTriggerInitOk = 0;
				BonDevEn[nDevIndex].bPmSenInitOk = 0;
			}
		}
		else
		{
			BonDevEn[nDevIndex].bInitializeCounter = 0;
		}
	}
	
	// Status abl�schen
	if( gMainLogic.State.bCleaningActive == 0 )
	{
		brsmemset((UDINT)&gBondingDev.State.bIntCleanDone, 0, sizeof(gBondingDev.State.bIntCleanDone));
	}
	
	// ---------- DCS abl�schen ----------
	fbEdgeNegAuto.CLK = gMainLogic.State.bAutomatic;
	F_TRIG(&fbEdgeNegAuto);
	if (fbEdgeNegAuto.Q == 1)
	{
		for (nDevIndex = 0; nDevIndex <= gPar.MachinePar.nBonDevIndex; nDevIndex++)
		{
			// K�pfe nur deaktivieren, wenn keine Reinigung ausgef�hrt wird
			if (gBondingDev.State.bCleaningActive[nDevIndex] == 0)
			{
				// Stati abl�schen
				bStartPosValid[nDevIndex]		= 0;
				BonDev.bDcsInitOk[nDevIndex]	= 0;
				bGetStartPos[nDevIndex]			= 0;
				bCalcPeriod[nDevIndex]			= 0;
				
				// DCS Eing�nge abl�schen
				gBondingDev.State.bVelocityOk[nDevIndex] = 0;
				BonDev.fbBonDevDCS[nDevIndex].Enable = 0;
				BonDev.fbBonDevDCS[nDevIndex].EnableTrack = 0;
				BonDev.fbBonDevDCS[nDevIndex].InitSwitches = 0;
				brsmemset((UDINT)&BonDev.fbBonDevDCS[nDevIndex].Switches, 0, sizeof(BonDev.fbBonDevDCS[nDevIndex].Switches));
				ASMcDcsPrecisionDigitalCamSwitch(&BonDev.fbBonDevDCS[nDevIndex]);
			}
		}	
	}
}

void _EXIT BondingDevEXIT(void)
{
}
