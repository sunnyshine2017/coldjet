(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: BondingDev
 * File: BondingDev.typ
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Local data types of program BondingDev
 ********************************************************************)

TYPE
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_DCS_INIT, (*Nockenschaltwerke initialisieren*)
		eSTEP_SWITCHES_INIT, (*Nockenpositionen initialisieren*)
		eSTEP_DCS_CLEAN_INIT (*Nockenschaltwerke f�r Reinigung initialisieren*)
		);
	BondingDevStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	BonDevDcs_TYP : 	STRUCT 
		fbBonDevDCS : ARRAY[0..nDEV_INDEX_MAX]OF ASMcDcsPrecisionDigitalCamSwitch; (*DCS f�r die Auftragek�pfe*)
		FifoStruct : FifoStruct_TYP; (*Fifo Struktur*)
		rProductLen : ARRAY[0..nDEV_INDEX_MAX]OF REAL;
		nFifoPos : ARRAY[0..nDEV_INDEX_MAX]OF DINT; (*Position aus Fifo*)
		nStartPos : ARRAY[0..nDEV_INDEX_MAX]OF DINT; (*Startposition*)
		nGap : ARRAY[0..nDEV_INDEX_MAX]OF DINT; (*Verbleibende Strecke einer Periode*)
		nRemainingDistance : ARRAY[0..nDEV_INDEX_MAX]OF UDINT; (*Verbleibende Strecke einer Periode*)
		nNextPeriod : ARRAY[0..nDEV_INDEX_MAX]OF UDINT; (*N�chste Periodenl�nge*)
		nCurrentPeriod : ARRAY[0..nDEV_INDEX_MAX]OF UDINT; (*Aktuelle Periodenl�nge*)
		nIdxCurrentPeriod : ARRAY[0..nDEV_INDEX_MAX]OF USINT; (*Index der aktuellen Periode*)
		nIdxNextPeriod : ARRAY[0..nDEV_INDEX_MAX]OF USINT; (*Index der n�chsten Periode*)
		nSwitchIdx : ARRAY[0..nDEV_INDEX_MAX]OF SINT; (*Index f�r Klebemuster*)
		bDcsInitOk : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Initialisierung abgeschlossen*)
		nDiffLen : ARRAY[0..nDEV_INDEX_MAX]OF DINT;
		nDiffLatchPos : ARRAY[0..nDEV_INDEX_MAX]OF DINT;
	END_STRUCT;
	ProductDetection_TYP : 	STRUCT 
		FifoType : FifoStruct_TYP;
		nCalcPosProDetect : ARRAY[0..nDEV_INDEX_MAX]OF UDINT; (*Berechnete Position des Sensors f�r Produkterkennung in [1/100mm] (Achseinheiten)*)
		nProductCnt : ARRAY[0..nDEV_INDEX_MAX]OF SINT; (*Produktz�hler aus DS-Modul*)
		bProductDetected : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Produkt wurde erkannt*)
		bNewPeriod : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Neues Produkt wurde initialisiert*)
		nDistanceProductToProDetSensor : ARRAY[0..nDEV_INDEX_MAX]OF DINT; (*Abstand zwischen dem �berwachten Produkt zu dem Produkterkennungssensor in [1/100mm] (Achseinheiten). Negative Werte: Distanze bis zum Sensor; Positive Werte: Distanze nach dem Sensor.*)
		rProductInWindow : ARRAY[0..nDEV_INDEX_MAX]OF REAL; (*Debug/Info-Variable: An welcher relativen Position in [mm] innerhalb des Toleranzbereiches wurde das Produkt erkannt*)
		rProductOutOfWindow : ARRAY[0..nDEV_INDEX_MAX]OF REAL; (*Debug/Info-Variable: An welcher relativen Position in [mm] au�erhalb des Toleranzbereiches wurde das Produkt erkannt*)
	END_STRUCT;
	IO_TYP : 	STRUCT 
		Input : IoInput_TYP;
	END_STRUCT;
	IoInput_TYP : 	STRUCT 
		bBonDevEnable : BOOL; (*Freigabe K�pfe (Maschinenfreigabe)*)
		nProDetRisingCnt : ARRAY[0..nDEV_INDEX_MAX]OF SINT; (*Z�hler f�r steigende Flanke Produkterkennung*)
		nTsProduktDetect : ARRAY[0..nDEV_INDEX_MAX]OF INT; (*Zeitstempel Produkterkennung*)
	END_STRUCT;
	FifoStruct_TYP : 	STRUCT 
		FifoEntryStandard : ARRAY[0..nDEV_INDEX_MAX]OF PosToBond_TYP; (*Positionen aus Schieberegister Standard*)
		FifoEntryPm : ARRAY[0..nDEV_INDEX_MAX]OF PmPos_TYP; (*Positionen aus Schieberegister Druckmarkenauswertung*)
	END_STRUCT;
	BonDevEn_TYP : 	STRUCT 
		bBonDevEnFlag : BOOL; (*Merker f�r Kopffreigabe*)
		bBonDevEnabled : BOOL; (*Kopffreigabe wurde w�hrend des Automatikbetriebs erteilt*)
		bBonDevDisabled : BOOL; (*Kopffreigabe wurde w�hrend des Automatikbetriebs abgel�scht*)
		bDcsInitOk : BOOL; (*Initialisierung Dcs Ok*)
		bEncInitOk : BOOL; (*Initialisierung Geber Ok*)
		bTriggerInitOk : BOOL; (*Initialisierung Trigger Sensor Ok*)
		bPmSenInitOk : BOOL; (*Initialisierung Druckmarkensensor Ok*)
		bInitializeCounter : BOOL; (*Initialisierung Z�hler abgeschlossen*)
	END_STRUCT;
END_TYPE
