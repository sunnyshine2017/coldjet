(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: TempCtrl
 * File: TempCtrl.typ
 * Author: Michael Zimmer
 * Created: 19.01.2015
 ********************************************************************
 * Local data types of program TempCtrl
 ********************************************************************)

TYPE
	HeatingSystemSteps_ENUM : 
		( (*Schritte*)
		eSTEP_HS_INIT := 0,
		eSTEP_HS_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_HS_INIT_FINISHED,
		eSTEP_HS_WAIT_FOR_COMMAND := 100,
		eSTEP_HS_START_HEATING := 200,
		eSTEP_HS_HEATING_TANK_STARTED,
		eSTEP_HS_START_HEATING_BON_DEV,
		eSTEP_HS_HEATING_BON_DEV_STARTED,
		eSTEP_HS_START_STAND_BY,
		eSTEP_HS_ERR_STEP := 300
		);
	TempZoneSteps_ENUM : 
		( (*Schritte der Temperaturzone*)
		eSTEP_TZ_INIT := 0,
		eSTEP_TZ_INIT_WAIT_FOR_PAR_VALID := 1,
		eSTEP_TZ_INIT_FINISHED,
		eSTEP_TZ_WAITING_FOR_COMMAND := 100,
		eSTEP_TZ_START_HEATING := 200,
		eSTEP_TZ_STOP_HEATING,
		eSTEP_TZ_UPDATE_PARAMETER,
		eSTEP_TZ_START_STEP_TUNING,
		eSTEP_TZ_WAIT_HEATING_STARTED,
		eSTEP_TZ_WAIT_TUNING_DONE,
		eSTEP_TZ_TUNING_DONE,
		eSTEP_TZ_ERR_STEP := 300,
		eSTEP_TZ_ERR_RESET
		);
	HeatingSystemStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : HeatingSystemSteps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	TempCtrlStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : HeatingSystemSteps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	TempZoneStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : TempZoneSteps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	IO_InputBonDev_TYP : 	STRUCT 
		nActTemp_Zone : ARRAY[0..nIDX_TEMP_CTRL_ZONE_MAX]OF INT;
		bStatusHeatingDigitalOutput : ARRAY[0..nIDX_TEMP_CTRL_ZONE_MAX]OF BOOL;
	END_STRUCT;
	IO_InputTank_TYP : 	STRUCT 
		nActTemp : INT;
		bStatusHeatingDigitalOutput : BOOL;
	END_STRUCT;
	IO_Input_TYP : 	STRUCT 
		Tank : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF IO_InputTank_TYP;
		BonDev : ARRAY[0..nDEV_INDEX_MAX]OF IO_InputBonDev_TYP;
	END_STRUCT;
	IO_Output_TYP : 	STRUCT 
		HeatingSystem : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF IO_OutputHeatingSystem_TYP;
		Tank : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF IO_OutputTank_TYP;
		BonDev : ARRAY[0..nDEV_INDEX_MAX]OF IO_OutputBonDev_TYP;
	END_STRUCT;
	IO_TYP : 	STRUCT 
		Input : IO_Input_TYP;
		Output : IO_Output_TYP;
	END_STRUCT;
	IO_OutputBonDev_TYP : 	STRUCT 
		bHeatingOn : ARRAY[0..nIDX_TEMP_CTRL_ZONE_MAX]OF BOOL; (*Dieser Ausgang steuert das Heizelement der jeweiligen Heizzone des entsprechenden Klebekopfes an (FALSE: nicht heizen; TRUE: heizen).*)
	END_STRUCT;
	IO_OutputHeatingSystem_TYP : 	STRUCT 
		bHeatingSystemReady : BOOL; (*Dieser Ausgang wird gesetzt, wenn die Ist-Temperatur des Tanks und die Ist-Temperatur aller diesem Tank zugeh�rigen K�pfe (nur die mit eingeschalteter Heizung) und deren konfigurierten Heizzonen im Toleranzfenster der jeweiligen Solltemperatur liegen.*)
		bHeatingMainContactorClosed : BOOL; (*Ausgang zur Ansteuerung des Heizungs-Lastsch�tzes*)
	END_STRUCT;
	IO_OutputTank_TYP : 	STRUCT 
		bHeatingOn : BOOL; (*Dieser Ausgang steuert das Heizelement des Tanks an (FALSE: nicht heizen; TRUE: heizen).*)
	END_STRUCT;
	TempCtrlFubs_TYP : 	STRUCT 
		PID : MTBasicsPID;
		PWM : MTBasicsPWM;
	END_STRUCT;
	TempCtrlTempZoneHeatingCtrl_TYP : 	STRUCT 
		PID : MTBasicsPID;
		PWM : MTBasicsPWM;
		StepTuning : MTBasicsStepTuning;
	END_STRUCT;
	TempCtrlBonDevCmd_TYP : 	STRUCT 
		bStartHeating : BOOL;
		bStopHeating : BOOL;
		bUpdateParameter : BOOL;
		bReset : BOOL;
	END_STRUCT;
	TempCtrlTempZoneCmd_TYP : 	STRUCT 
		bStartHeating : BOOL;
		bStopHeating : BOOL;
		bUpdateParameter : BOOL;
		bReset : BOOL;
		bStartStepTuning : BOOL;
		bApplyPIDParameters : BOOL;
	END_STRUCT;
	TempCtrlTempZonePar_TYP : 	STRUCT 
		rSetTemp : REAL;
		rStandbyTemp : USINT;
	END_STRUCT;
	TempCtrlTempZoneSimulation_TYP : 	STRUCT 
		PT2 : MTBasicsPT2;
		DT2 : MTBasicsDT2;
		Integrator : MTBasicsIntegrator;
		DeadTime : MTBasicsTimeDelay;
	END_STRUCT;
	TempCtrlTempZoneErrors_TYP : 	STRUCT 
		ErrPIDParameterInvalid : SetEvent_TYP;
		ErrTempSensorBrokenWire : SetEvent_TYP;
		ErrTempSensorMinValueReached : SetEvent_TYP;
		ErrTempSensorInvalidValue : SetEvent_TYP;
		ErrExternalHeatingSignal : SetEvent_TYP;
		ErrActTempLeftToleranceWindow : SetEvent_TYP;
		ErrHeatingShorted : SetEvent_TYP;
		ErrOverTemperature : SetEvent_TYP;
	END_STRUCT;
	TempCtrlTempZoneWarnings_TYP : 	STRUCT 
		WarnUpdateAbortedInvalidPIDPar : SetEvent_TYP;
		WarnPIDParameterInvalid : SetEvent_TYP;
		WarnPIDInteral : SetEvent_TYP;
	END_STRUCT;
	TempCtrlTempZoneStatusIDs_TYP : 	STRUCT 
		nPID_StatusID : DINT;
		nPWM_StatusID : DINT;
		nStepTuning_StatusID : DINT;
	END_STRUCT;
	TempCtrlTempZoneNegEdge_TYP : 	STRUCT 
		TempSettled : F_TRIG;
		TempInTolerance : F_TRIG;
	END_STRUCT;
	TempCtrlTempZone_TYP : 	STRUCT 
		Cmd : TempCtrlTempZoneCmd_TYP;
		Par : TempCtrlTempZonePar_TYP;
		Step : TempZoneStep_TYP;
		StepHandling : BrbStepHandling_TYP;
		HeatingCtrl : TempCtrlTempZoneHeatingCtrl_TYP; (*Funktionsbl�cke, Parameter usw. die f�r die Temperaturregelung ben�tigt werden.*)
		Simulation : TempCtrlTempZoneSimulation_TYP;
		Warnings : TempCtrlTempZoneWarnings_TYP;
		Errors : TempCtrlTempZoneErrors_TYP;
		StatusIDs : TempCtrlTempZoneStatusIDs_TYP;
		rTemperatureHeatingSwitchedOff : REAL;
		TempInToleranceAndSettled : TempInToleranceAndSettled_TYP;
		NegEdge : TempCtrlTempZoneNegEdge_TYP;
	END_STRUCT;
	SetEvent_TYP : 	STRUCT 
		bActive : BOOL;
		bEventIsSet : BOOL;
	END_STRUCT;
	TempCtrlBonDev_TYP : 	STRUCT 
		Cmd : TempCtrlBonDevCmd_TYP;
		TempZone : ARRAY[0..nIDX_TEMP_CTRL_ZONE_MAX]OF TempCtrlTempZone_TYP;
	END_STRUCT;
	TempCtrlHeatingSystemWarn_TYP : 	STRUCT 
		WarnHeatingSystemCriticalTemp : SetEvent_TYP;
	END_STRUCT;
	TempCtrlHeatingSystemInfos_TYP : 	STRUCT 
		InfoHeatingSystemWarmUp : SetEvent_TYP;
		InfoHeatingSystemInStandby : SetEvent_TYP;
	END_STRUCT;
	TempCtrlHeatingSystem_TYP : 	STRUCT 
		Step : HeatingSystemStep_TYP;
		StepHandling : BrbStepHandling_TYP;
		Infos : TempCtrlHeatingSystemInfos_TYP;
		Warnings : TempCtrlHeatingSystemWarn_TYP;
	END_STRUCT;
	NegEdge_TYP : 	STRUCT 
		MachineModuleTempCtrlEnabled : F_TRIG;
		TempCtrlTankExist : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF F_TRIG;
		TempCtrlBonDevExist : ARRAY[0..nDEV_INDEX_MAX]OF F_TRIG;
		TempCtrlZoneExist : ARRAY[0..nDEV_INDEX_MAX,0..nIDX_TEMP_CTRL_ZONE_MAX]OF F_TRIG;
		HeatingSystemError : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF F_TRIG;
		BonDevEnable : ARRAY[0..nDEV_INDEX_MAX]OF F_TRIG;
		TempCtrlTankTempSettled : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF F_TRIG;
		TempCtrlTankTempInTolerance : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF F_TRIG;
		TempCtrlZoneTempSettled : ARRAY[0..nDEV_INDEX_MAX,0..nIDX_TEMP_CTRL_ZONE_MAX]OF F_TRIG;
		TempCtrlZoneTempInTolerance : ARRAY[0..nDEV_INDEX_MAX,0..nIDX_TEMP_CTRL_ZONE_MAX]OF F_TRIG;
	END_STRUCT;
	TempInTolAndSettledInternal_TYP : 	STRUCT 
		nStep : UINT;
		NegEdgeTempInWarningWindow : F_TRIG;
		TimeTempSettled : TON;
	END_STRUCT;
	TempInToleranceAndSettled_TYP : 	STRUCT 
		bTempInToleranceWindow : BOOL;
		bTempInWarningWindow : BOOL;
		nSettlingTime : TIME;
		bTempInToleranceAndSettled : BOOL;
		Internal : TempInTolAndSettledInternal_TYP;
	END_STRUCT;
	PosEdge_TYP : 	STRUCT 
		HeatingSystemError : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF R_TRIG;
		BonDevEnable : ARRAY[0..nDEV_INDEX_MAX]OF R_TRIG;
	END_STRUCT;
	Infos_TYP : 	STRUCT 
		InfoAutomaticNotPossible : SetEvent_TYP;
		InfoTankSetTempToLow : SetEvent_TYP;
		InfoBondDevSetTempToLow : SetEvent_TYP;
	END_STRUCT;
END_TYPE
