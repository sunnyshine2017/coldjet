/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: TempCtrl
 * File: TempCtrl.c
 * Author: Michael Zimmer
 * Created: 19.01.2015
 ********************************************************************
 * Implementation of program TempCtrl
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <TempCtrlFunc.h>

void _INIT TempCtrlINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gTempCtrl, 0, sizeof(gTempCtrl));

	// Initialisierung Task
	
	strcpy( sTempZoneName[eTEMP_ZONE_TUBE], "Tube" );
	strcpy( sTempZoneName[eTEMP_ZONE_BON_DEV], "Head" );
	strcpy( sTempZoneName[eTEMP_ZONE_RESERVE], "Reserve" );
	
	for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
	{
		gTempCtrl.CallerBox.HeatingSystem[nBonDevIdx].Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;
	
		for( nTempZoneIdx = 0; nTempZoneIdx <= nIDX_TEMP_CTRL_ZONE_MAX; nTempZoneIdx++ )
		{
			
		}
	}
	
	for( nTankIdx = 0; nTankIdx <= nIDX_TEMP_CTRL_TANK_MAX; nTankIdx++ )
	{
		// Initialisierung der Schrittkette
		HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_INIT;
	}
	
	IO.Output.BonDev[0].bHeatingOn[0] = IO.Output.BonDev[0].bHeatingOn[0];
	
}

void _CYCLIC TempCtrlCYCLIC(void)
{
	// =============================
	// States des Heizsystems setzen
	// =============================
	
	// Negative Flanke des Enable-Flags aus der Modul-Liste f�r die Heizungsregelung ermitteln
	NegEdge.MachineModuleTempCtrlEnabled.CLK = gPar.MachinePar.MachineModule.bTempCtrlEn;
	F_TRIG( &NegEdge.MachineModuleTempCtrlEnabled );
	
	// In der Modul-Liste ist die Heizungsregelung eingeschaltet
	if( gPar.MachinePar.MachineModule.bTempCtrlEn == TRUE )
	{
		// AutomatikPossible-Flag vor der Schleife setzen -> Innerhalb der Schleife wird dieses gegebenenfalls zur�ckgesetzt
		gTempCtrl.State.bAutomaticPossible = TRUE;
		
		// Schleife �ber alle konfigurierbaren Heizsysteme (entspricht der Anzahl der m�glichen Tanks)
		for( nTankIdx = 0; nTankIdx <= nIDX_TEMP_CTRL_TANK_MAX; nTankIdx++ )
		{
			// Negative Flanke des Exist-Flags aus der Konfiguration f�r dieses Heizsystem ermitteln
			NegEdge.TempCtrlTankExist[nTankIdx].CLK = gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].bExist;
			F_TRIG( &NegEdge.TempCtrlTankExist[nTankIdx] );
			
			// Der Tank mit Index nTankIdx besitzt eine Heizung
			if( gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].bExist == TRUE )
			{
				// Flags vor der Schleife zur�cksetzen -> Innerhalb der Schleife werden diese gegebenenfalls gesetzt
				gTempCtrl.State.HeatingSystem[nTankIdx].bTempInWarningWindow = FALSE;
				gTempCtrl.State.HeatingSystem[nTankIdx].bAutomaticPossible = FALSE;
				gTempCtrl.State.HeatingSystem[nTankIdx].bErrorHeatingSystem = FALSE;
				
				// Schleife �ber alle konfigurierten Klebek�pfe
				for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
				{	
					// Der Kopf mit dem Index nBonDevIdx ist mit dem Tank mit dem Index nTankIdx verbunden
					if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx == nTankIdx )
					{
						// Dieser Klebekopf hat eine Heizung konfiguriert
						if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist == TRUE )
						{
							// Dieser Klebekopf ist f�r die Produktion aktiviert (Checkbox aktiv auf Visu Hauptseite)
							if( gPar.ProductPar.BonDevPar[nBonDevIdx].bBonDevEn == TRUE )
							{
								
								gTempCtrl.State.HeatingSystem[nTankIdx].bAutomaticPossible = FALSE;
								gTempCtrl.State.HeatingSystem[nTankIdx].bOneOrMoreBonDevProdutionEnabled = TRUE;
								// Schleife hier verlassen
								break;
							}
						}
					}
					// Die Schleife hat alle konfigurierten Klebek�pfe durchlaufen und es wurde kein f�r die Produktion aktivierter Klebekopf mit Heizung f�r dieses Heizsystem gefunden
					if( nBonDevIdx == gPar.MachinePar.nBonDevIndex )
					{	
						gTempCtrl.State.HeatingSystem[nTankIdx].bAutomaticPossible = TRUE;
						gTempCtrl.State.HeatingSystem[nTankIdx].bOneOrMoreBonDevProdutionEnabled = FALSE;
					}
				}
				
				// Dieses Heizsystem besitzt mindestens einen beheizten Klebekopf der f�r die Produktion aktiviert ist
				if( gTempCtrl.State.HeatingSystem[nTankIdx].bOneOrMoreBonDevProdutionEnabled == TRUE )
				{
					// Ist Standby nicht aktiv, pr�fen ob in den Betriebsmodus "Automatik" geschaltet werden darf
					if( (gTempCtrl.State.HeatingSystem[nTankIdx].bStandbyOn == FALSE) &&
						(gTempCtrl.State.Tank[nTankIdx].bHeatingOn == TRUE) )
					{
						// Pr�fen ob die Ist-Temperatur sich im Toleranzfenster befindet und sich dort beruhigt hab					
						TankAutomaticPossible[nTankIdx].bTempInWarningWindow = gTempCtrl.State.Tank[nTankIdx].bTempInWarningWindow;
						TankAutomaticPossible[nTankIdx].bTempInToleranceWindow = gTempCtrl.State.Tank[nTankIdx].bTempInToleranceWindow;
						TankAutomaticPossible[nTankIdx].nSettlingTime = 5000;
						
						CheckIsTempInToleranceAndSettled( &TankAutomaticPossible[nTankIdx] );
						
						// Ist die Temperatur dieses Tanks im Toleranzfenster und diese hat sich beruhigt, so ist ein wechsel in den Betriebsmodus "Automatik" m�glich
						gTempCtrl.State.HeatingSystem[nTankIdx].bAutomaticPossible = TankAutomaticPossible[nTankIdx].bTempInToleranceAndSettled;
					}
					// Dieses Heizsystem ist in Standby -> es darf nicht m�glich sein in den Betriebsmodus "Automatik" zu wechseln.
					else
					{
						gTempCtrl.State.HeatingSystem[nTankIdx].bAutomaticPossible = FALSE;
					}
				}
				
				gTempCtrl.State.HeatingSystem[nTankIdx].bTempInToleranceWindow = gTempCtrl.State.Tank[nTankIdx].bTempInToleranceWindow;
								
				if( gTempCtrl.State.Tank[nTankIdx].bErrorTempZone == TRUE )
				{
					gTempCtrl.State.HeatingSystem[nTankIdx].bErrorHeatingSystem = TRUE;
				}
				
				if( gTempCtrl.State.Tank[nTankIdx].bTempInWarningWindow == TRUE )
				{
					gTempCtrl.State.HeatingSystem[nTankIdx].bTempInWarningWindow = TRUE;
				}
				
				// Schleife �ber alle Klebek�pfe
				for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
				{
					// Der Klebekopf geh�rt zu dem bereits oben gepr�ften Tank
					if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx == nTankIdx )
					{
						// Der Klebekopf mit Index nBonDevIdx besitzte eine Heizung
						if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist == TRUE )
						{							
							// Dieser Klebekopf ist f�r die Produktion aktiviert
							if( gPar.ProductPar.BonDevPar[nBonDevIdx].bBonDevEn == TRUE )
							{
								if( gTempCtrl.State.BonDev[nBonDevIdx].bAutomaticPossible == FALSE )
								{
									// Es ist nicht m�glich die Automatik zu starten, da der Klebekopf f�r die Produktion aktiviert und die Temperatur einer seiner Heizzonen sich nicht im Toleranzfenster befindet.
									gTempCtrl.State.HeatingSystem[nTankIdx].bAutomaticPossible = FALSE;
								}
							}
										
							// Die Heizzonen dieses Klebekopfes sind aktiv und werden eingeschaltet, sobald das �bergeordnete Heizsystem eingeschaltet wird
							if( gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].bEnable == TRUE )
							{
																
								if( gTempCtrl.State.BonDev[nBonDevIdx].bTempInToleranceWindow == FALSE )
								{
									gTempCtrl.State.HeatingSystem[nTankIdx].bTempInToleranceWindow = FALSE;
								}
								
								if( (gTempCtrl.State.BonDev[nBonDevIdx].bTempInWarningWindow == TRUE) &&
									(gTempCtrl.State.BonDev[nBonDevIdx].bAutomaticPossible == TRUE) )
								{
									gTempCtrl.State.HeatingSystem[nTankIdx].bTempInWarningWindow = TRUE;
								}
							}
						}
						
						if( gTempCtrl.State.BonDev[nBonDevIdx].bErrorBonDev == TRUE )
						{
							gTempCtrl.State.HeatingSystem[nTankIdx].bErrorHeatingSystem = TRUE;
						}
					}
				}
				
				// Die Temperatur mindestens einer Heizzone dieses Heizsystems ( Tank oder Klebekopf ) befindest sich im kritischen Fenster -> Warnung ausgeben
				if( (gTempCtrl.State.HeatingSystem[nTankIdx].bAutomaticPossible == TRUE) &&
					(gTempCtrl.State.HeatingSystem[nTankIdx].bTempInWarningWindow == TRUE ) &&
					(gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingOn == TRUE) )
				{
						if( HeatingSystem[nTankIdx].Warnings.WarnHeatingSystemCriticalTemp.bEventIsSet == FALSE )
						{
							HeatingSystem[nTankIdx].Warnings.WarnHeatingSystemCriticalTemp.bEventIsSet = TRUE;
							AppSetEvent( eEVT_WAR_HEAT_SYS_CRITICAL_TEMP, 0, "", nTankIdx + 1, &gEventManagement );
						}
				}
				// -> Warnung zur�cksetzen
				else
				{
					if( HeatingSystem[nTankIdx].Warnings.WarnHeatingSystemCriticalTemp.bEventIsSet == TRUE )
					{
						HeatingSystem[nTankIdx].Warnings.WarnHeatingSystemCriticalTemp.bEventIsSet = FALSE;
						AppResetEvent( eEVT_WAR_HEAT_SYS_CRITICAL_TEMP, &gEventManagement );
					}
				}
				
				// Ist ein oder mehrere Heizsysteme nicht bereit f�r den Automatik-Modus (Produktion) kann nicht in Automatik geschaltet werden
				if( gTempCtrl.State.HeatingSystem[nTankIdx].bAutomaticPossible == FALSE )
				{
					gTempCtrl.State.bAutomaticPossible = FALSE;
				}
			}
			// Der Tank mit dem Index "nTankIdx" hat keine Heizung oder diese wurde in der Konfiguration abgew�hlt
			else
			{
				// Dieses Heizsystem wurde soeben in der Konfiguration, durch abwahl des Tanks mit dem gleichen Index, deaktiviert
				if( NegEdge.TempCtrlTankExist[nTankIdx].Q == TRUE )
				{
					// Strukturen zur�cksetzen
					memset( &gTempCtrl.State.HeatingSystem[nTankIdx], 0x00, sizeof( gTempCtrl.State.HeatingSystem[nTankIdx] ) );
					memset( &HeatingSystem[nTankIdx], 0x00, sizeof( HeatingSystem[nTankIdx] ) );
					// Ausg�nge zur�cksetzen
					memset( &IO.Output.HeatingSystem[nTankIdx], 0x00, sizeof( IO.Output.HeatingSystem[nTankIdx] ) );
				}
			}
		}
	}
	// Die Heizungsregelung ist in der Modul-Liste nicht aktiv
	else
	{
		// Die Heizungsregelung wurde soeben in der Modul-Liste deaktiviert
		// -> Meldungen, Strukturen und Ausg�nge zur�cksetzen
		if( NegEdge.MachineModuleTempCtrlEnabled.Q == TRUE )
		{
			// Info-Meldungen zur�cksetzen
			AppResetEvent( eEVT_INF_HEAT_SYS_AUTO_NOT_POS, &gEventManagement );
			AppResetEvent( eEVT_INF_HEAT_SYS_WARM_UP, &gEventManagement );
			AppResetEvent( eEVT_INF_HEAT_SYS_IN_STAND_BY, &gEventManagement );
			
			// Warnungen zur�cksetzen
			AppResetEvent( eEVT_WAR_HEAT_SYS_CRITICAL_TEMP, &gEventManagement );
			AppResetEvent( eEVT_WAR_DISABL_HEAT_SYS_IN_PROD, &gEventManagement );	
			AppResetEvent( eEVT_WAR_DISABLED_BONDEV_IN_PROD, &gEventManagement );
			AppResetEvent( eEVT_WAR_TANK_PID_PAR_INVALID, &gEventManagement );
			AppResetEvent( eEVT_WAR_TANK_UPDATE_PID_ABORTED, &gEventManagement );
			AppResetEvent( eEVT_WAR_TANK_PID_INTERNAL, &gEventManagement );
			AppResetEvent( eEVT_WAR_HEAD_PID_PAR_INVALID, &gEventManagement );
			AppResetEvent( eEVT_WAR_HEAD_UPDATE_PID_ABORTED, &gEventManagement );
			AppResetEvent( eEVT_WAR_HEAD_PID_INTERNAL, &gEventManagement );

			// Fehlermeldungen zur�cksetzen
			AppResetEvent( eEVT_ERR_TANK_TEMP_SENS_BRK_WIRE, &gEventManagement );
			AppResetEvent( eEVT_ERR_TANK_TEMP_SENS_MIN_VAL, &gEventManagement );
			AppResetEvent( eEVT_ERR_TANK_TEMP_SENS_INVALID, &gEventManagement );
			AppResetEvent( eEVT_ERR_TEMP_TANK_NOT_IN_TOL, &gEventManagement );
			AppResetEvent( eEVT_ERR_TANK_OVER_TEMP, &gEventManagement );
			AppResetEvent( eEVT_ERR_TANK_HEATING_SHORTED, &gEventManagement );
			AppResetEvent( eEVT_ERR_TANK_PID_PAR_INVALID, &gEventManagement );
			AppResetEvent( eEVT_ERR_TANK_EXT_HEATING_SIGNAL, &gEventManagement );
			AppResetEvent( eEVT_ERR_HEAD_TEMP_SENS_BRK_WIRE, &gEventManagement );
			AppResetEvent( eEVT_ERR_HEAD_TEMP_SENS_MIN_VAL, &gEventManagement );
			AppResetEvent( eEVT_ERR_HEAD_TEMP_SENS_INVALID, &gEventManagement );
			AppResetEvent( eEVT_ERR_TEMP_BON_DEV_NOT_IN_TOL, &gEventManagement );
			AppResetEvent( eEVT_ERR_BON_DEV_OVER_TEMP, &gEventManagement );
			AppResetEvent( eEVT_ERR_BON_DEV_HEATING_SHORTED, &gEventManagement );
			AppResetEvent( eEVT_ERR_HEAD_PID_PAR_INVALID, &gEventManagement);
			AppResetEvent( eEVT_ERR_HEAD_EXT_HEATING_SIGNAL, &gEventManagement );
		
			// Strukturen zur�cksetzen
			memset( &gTempCtrl, 0x00, sizeof( gTempCtrl ) );
			memset( &Tank, 0x00, sizeof ( Tank ) );
			memset( &BonDev, 0x00, sizeof ( BonDev ) );
			memset( &HeatingSystem, 0x00, sizeof( HeatingSystem ) );
			
			// Ausg�nge zur�cksetzen
			memset( &IO.Output, 0x00, sizeof( IO.Output ) );
		}
		
		// Abarbeitung des Tasks hier stoppen
		return;
	}
	
	
	// ------------------------------------------------------------
	// Info-Meldung "Automatik nicht m�glich" setzen / zur�cksetzen
	// ------------------------------------------------------------
	// Produktionsbetrieb (Automatik) ist auf Grund der Temperatur der f�r die Produktion ausgew�hlten Klebek�pfe bzw. deren Tanks nicht m�glich
	if( gTempCtrl.State.bAutomaticPossible == FALSE )
	{
		if( Infos.InfoAutomaticNotPossible.bEventIsSet == FALSE )
		{
			AppSetEvent( eEVT_INF_HEAT_SYS_AUTO_NOT_POS, 0, "", 0, &gEventManagement );
			Infos.InfoAutomaticNotPossible.bEventIsSet = TRUE;
		}
		
		// Sollte das Klebesystem in Produktion sein (Automatik aktiv), wird dieses gestoppt
		if( gMainLogic.State.bAutomatic == TRUE )
		{
			gMainLogic.DirectBox.bStop = TRUE;
		}
	}
	// Automatik ist m�glich -> Info-Meldung zur�cksetzen
	else
	{
		if( Infos.InfoAutomaticNotPossible.bEventIsSet == TRUE )
		{
			AppResetEvent( eEVT_INF_HEAT_SYS_AUTO_NOT_POS, &gEventManagement ); 
			Infos.InfoAutomaticNotPossible.bEventIsSet = FALSE;
		}
	}
	
	// =====================================
	// Abarbeitung der m�glichen Heizsysteme
	// =====================================	
	// Schleife �ber alle m�glichen Heizsysteme
	for( nTankIdx = 0; nTankIdx <= nIDX_TEMP_CTRL_TANK_MAX; nTankIdx++ )
	{
		// Dieses Heizsystem ist vorhanden
		if( gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].bExist == TRUE )
		{
			// Es wird eine Parameter-Datei geladen
			if( gParHandling.State.bParFileLoading == TRUE )
			{
				// Das Heizsystem muss neu initialisiert werden
				HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_INIT;
			}
			
			// ----------------
			// Direkt-Kommandos
			// ----------------
			if( HeatingSystem[nTankIdx].Step.bInitDone == TRUE )
			{
				// Erkennen der positiven und negativen Flanke eines Fehlers
				PosEdge.HeatingSystemError[nTankIdx].CLK = gTempCtrl.State.HeatingSystem[nTankIdx].bErrorHeatingSystem;
				R_TRIG( &PosEdge.HeatingSystemError[nTankIdx] );
				NegEdge.HeatingSystemError[nTankIdx].CLK = gTempCtrl.State.HeatingSystem[nTankIdx].bErrorHeatingSystem;
				F_TRIG( &NegEdge.HeatingSystemError[nTankIdx] );
				
				// Ein Fehler im Heizsystem ist gerade aufgetreten
				if( PosEdge.HeatingSystemError[nTankIdx].Q == TRUE )
				{
					// Schrittkette f�r weitere Kommandos sperren
					if( BrbSetCaller( &gTempCtrl.CallerBox.HeatingSystem[nTankIdx].Caller, eCALLERID_TEMP_CTRL ) != eBRB_CALLER_STATE_OK )
					{
						// Ist die Schrittkette schon von einem anderen Task belegt, dessen CallerID durch die dieses Tasks ersetzen
						gTempCtrl.CallerBox.HeatingSystem[nTankIdx].Caller.nCallerId = eCALLERID_TEMP_CTRL;
					}
					
					// Die Heizung aller Subsysteme (Tank, K�pfe) ausschalten
					SetCmdStopHeatingOfSubSystems( nTankIdx );
					
					// Status setzen: Heizssystem ist nicht im Standby
					gTempCtrl.State.HeatingSystem[nTankIdx].bStandbyOn = FALSE;
					// Status setzen: Die Heizungen des Heizssystems sind nicht eingeschaltet
					gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingOn = FALSE;
					
					// N�chster Schritt: Fehlerschritt, warten bis Fehler quittiert ist
					HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_ERR_STEP;		
				}
				// Der Fehler im Heizsystem wurde zur�ckgesetzt
				if( NegEdge.HeatingSystemError[nTankIdx].Q == TRUE )
				{
					// n�chster Schritt: Warten auf Kommando
					HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_WAIT_FOR_COMMAND;
				
					// Schrittkette f�r Kommandos freigeben
					BrbClearCallerBox( (UDINT)&gTempCtrl.CallerBox.HeatingSystem[nTankIdx], sizeof( gTempCtrl.CallerBox.HeatingSystem[nTankIdx] ) );
				}
				
				// Direkt-Kommando: Heizungen des Heizsystems stoppen
				if( gTempCtrl.DirectBox.HeatingSystem[nTankIdx].bStopHeating == TRUE )
				{
					// Einer der Klebek�pfe dieses Heizsystems ist in Produktion und daher darf das Heizsystem nicht abgeschaltet werden
					if( (gTempCtrl.State.HeatingSystem[nTankIdx].bOneOrMoreBonDevProdutionEnabled == TRUE) &&
						(gMainLogic.State.bAutomatic == TRUE) )
					{
						// Warnung ausgeben
						AppSetEvent( eEVT_WAR_DISABL_HEAT_SYS_IN_PROD, 0, "", nTankIdx + 1, &gEventManagement );
						AppResetEvent( eEVT_WAR_DISABL_HEAT_SYS_IN_PROD, &gEventManagement );
					
//						gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].bEnable = TRUE;
						
						// Schrittkette f�r Kommandos freigeben
						BrbClearDirectBox( (UDINT)&gTempCtrl.DirectBox.HeatingSystem[nTankIdx], sizeof( gTempCtrl.DirectBox.HeatingSystem[nTankIdx] ) );
					}
					else
					{
						// Die Heizung aller Subsysteme (Tank, K�pfe) ausschalten
						SetCmdStopHeatingOfSubSystems( nTankIdx );
						
						// Status setzen: Heizssystem ist nicht im Standby
						gTempCtrl.State.HeatingSystem[nTankIdx].bStandbyOn = FALSE;
						// Status setzen: Die Heizungen des Heizssystems sind nicht eingeschaltet
						gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingOn = FALSE;
						
						// n�chster Schritt: Warten auf Kommando
						HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_WAIT_FOR_COMMAND;
						
						// Schrittkette f�r Kommandos freigeben
						BrbClearDirectBox( (UDINT)&gTempCtrl.DirectBox.HeatingSystem[nTankIdx], sizeof( gTempCtrl.DirectBox.HeatingSystem[nTankIdx] ) );
						
						// Info-Meldungen zur�cksetzen
						if( HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet == TRUE )
						{
							AppResetEvent( eEVT_INF_HEAT_SYS_WARM_UP, &gEventManagement );
							HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet = FALSE;
						}
						
						if( HeatingSystem[nTankIdx].Infos.InfoHeatingSystemInStandby.bEventIsSet == TRUE )
						{
							AppResetEvent( eEVT_INF_HEAT_SYS_IN_STAND_BY, &gEventManagement );
							HeatingSystem[nTankIdx].Infos.InfoHeatingSystemInStandby.bEventIsSet = FALSE;
						}
					}
				}
			
				// -------------------------------------------------------------------------------------------
				// Erkennen ob die Heizung einzelener K�pfe eines Heizsystems ein-/ausgeschaltet werden m�ssen
				// -------------------------------------------------------------------------------------------
				for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
				{
					// Die Heizzonen des Kopfes mit dem Index nBonDevIdx ist ein Subsystem des HeatingSystems mit dem Index nTankIdx
					if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx == nTankIdx )
					{				
						// Heizung dieses Klebek�pfes nur ein-/abschalten, wenn das dazugeh�rige Heizsystem auch heizt
						if( gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingOn == TRUE )
						{
							// Der Klebekopf ist mit Heizzonen ausgestattet
							if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist == TRUE )
							{
								// Die Heizung dieses Klebekopfes soll an sein, ist aber nicht eingeschaltet, oder umgekehrt
								if( gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].bEnable != gTempCtrl.State.BonDev[nBonDevIdx].bHeatingOn )
								{
									// Klebekopf soll abgeschaltet werden
									if( gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].bEnable == FALSE )
									{
										// Dieser Kopf ist in Produktion und daher darf seine Heizung nicht abgeschaltet werden
										if( (gTempCtrl.State.HeatingSystem[nTankIdx].bOneOrMoreBonDevProdutionEnabled == TRUE) &&
											(gMainLogic.State.bAutomatic == TRUE) )
										{
											// Warnung ausgeben
											AppSetEvent( eEVT_WAR_DISABLED_BONDEV_IN_PROD, 0, "", nBonDevIdx + 1, &gEventManagement );
											AppResetEvent( eEVT_WAR_DISABLED_BONDEV_IN_PROD, &gEventManagement );
											
											// Haken in Check-Box wieder setzen
											gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].bEnable = TRUE;
										}
									}
									
									
									// Warten bis Schrittkette frei ist f�r neues Kommando, und anschlie�end die Schrittkette belegen
									if( BrbSetCaller( &gTempCtrl.CallerBox.HeatingSystem[nTankIdx].Caller, eCALLERID_TEMP_CTRL ) == eBRB_CALLER_STATE_OK )
									{
										// Das Heizsystem regelt die Temperatur auf den Arbeits-Sollwert
										if( gTempCtrl.State.HeatingSystem[nTankIdx].bStandbyOn == FALSE )
										{
										
											// Das Kommanodo bStartHeating absetzen. Mit diesem Kommando werden der Tank und die K�pfe eingeschaltet, deren Heizung eingeschaltet werden soll.
											// Die K�pfe deren Heizung nicht eingeschaltet werden sollen, werden abgeschaltet, falls diese K�pfe bereits heizen. Die Temperatur auf die geregelt
											// wird, ist die in der Visu gew�hlte Soll-Temperatur.
											gTempCtrl.CallerBox.HeatingSystem[nTankIdx].bStartHeating = TRUE;
										
										}
										// Das Heizsystem ist im Standy
										else
										{
											// Das Kommanodo bStartSystemStandby absetzen. Mit diesem Kommando werden der Tank und die K�pfe eingeschaltet, deren Heizung eingeschaltet werden soll.
											// Die K�pfe deren Heizung nicht eingeschaltet werden sollen, werden abgeschaltet, falls diese K�pfe bereits heizen. Die Temperatur auf die geregelt
											// wird, ist die in der Visu gew�hlte Standby-Temperatur.
											gTempCtrl.CallerBox.HeatingSystem[nTankIdx].bStartSystemStandby = TRUE;
										}
									}
								}
							}
						}
					}
				}
				
				// Hat ein Heizsystem einen Fehler und dessen beheizter Klebekopf ist gerade in Produktion, wird die Automatik abgeschaltet
				if( gTempCtrl.State.HeatingSystem[nTankIdx].bErrorHeatingSystem == TRUE )
				{
					if( gMainLogic.State.bAutomatic == TRUE )
					{
						if( gTempCtrl.State.HeatingSystem[nTankIdx].bOneOrMoreBonDevProdutionEnabled == TRUE )
						{
							gMainLogic.DirectBox.bStop = TRUE;
						}
					}
				}
			}
			// StepHandling
			if( HeatingSystem[nTankIdx].StepHandling.Current.bTimeoutElapsed == 1)
			{
				HeatingSystem[nTankIdx].StepHandling.Current.bTimeoutElapsed = 0;
				HeatingSystem[nTankIdx].Step.eStepNr = HeatingSystem[nTankIdx].StepHandling.Current.nTimeoutContinueStep;
			}
			HeatingSystem[nTankIdx].StepHandling.Current.nStepNr = (DINT)HeatingSystem[nTankIdx].Step.eStepNr;
			strcpy(HeatingSystem[nTankIdx].StepHandling.Current.sStepText, HeatingSystem[nTankIdx].Step.sStepText);
			BrbStepHandler(&HeatingSystem[nTankIdx].StepHandling);
			
			// Schrittkette der Heizsysteme
			switch( HeatingSystem[nTankIdx].Step.eStepNr )
			{
				
				//-----------------------------------------------------------------------------------------------------------------------
				case eSTEP_HS_INIT:
					strcpy(HeatingSystem[nTankIdx].Step.sStepText, "eSTEP_HS_INIT");					
					
					gTempCtrl.State.bInitDone = FALSE;
					HeatingSystem[nTankIdx].Step.bInitDone = FALSE;
					HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_INIT_WAIT_FOR_PAR_VALID;
					break;
		
				case eSTEP_HS_INIT_WAIT_FOR_PAR_VALID:
					strcpy(HeatingSystem[nTankIdx].Step.sStepText, "eSTEP_HS_INIT_WAIT_FOR_PAR_VALID");
					if( gParHandling.State.bPParValid == TRUE )
					{
						HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_INIT_FINISHED;
					}
					break;
		
				case eSTEP_HS_INIT_FINISHED:
					strcpy(HeatingSystem[nTankIdx].Step.sStepText, "eSTEP_HS_INIT_FINISHED");
					
					Tank[nTankIdx].rTemperatureHeatingSwitchedOff = gPar.ProductPar.TempCtrlPar.Tank[nTankIdx].rSetTemp;
					
					for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
					{
						for( nTempZoneIdx = 0; nTempZoneIdx <= nIDX_TEMP_CTRL_ZONE_MAX; nTempZoneIdx++ )
						{
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].rTemperatureHeatingSwitchedOff = gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].rSetTemp[nTempZoneIdx];
						}
					}
					
					gTempCtrl.State.bInitDone = TRUE;
					HeatingSystem[nTankIdx].Step.bInitDone = TRUE;
					BrbClearCallerBox( (UDINT)&gTempCtrl.CallerBox.HeatingSystem[nTankIdx], sizeof( gTempCtrl.CallerBox.HeatingSystem[nTankIdx] ) );
					HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_WAIT_FOR_COMMAND;
					break;
		
				//-----------------------------------------------------------------------------------------------------------------------
				case eSTEP_HS_WAIT_FOR_COMMAND:
					strcpy(HeatingSystem[nTankIdx].Step.sStepText, "eSTEP_HS_WAIT_FOR_COMMAND");
					
					if( gTempCtrl.CallerBox.HeatingSystem[nTankIdx].bStartHeating == TRUE )
					{
						gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingOn = TRUE;
						
						HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_START_HEATING;
					}
					
					else if( gTempCtrl.CallerBox.HeatingSystem[nTankIdx].bStartSystemStandby == TRUE )
					{
						if( gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingOn == TRUE )
						{
							HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_START_STAND_BY;
						}
						else
						{
							BrbClearCallerBox((UDINT)&gTempCtrl.CallerBox.HeatingSystem[nTankIdx], sizeof( gTempCtrl.CallerBox.HeatingSystem[nTankIdx] ) );
						}
					}
					else if( gTempCtrl.CallerBox.HeatingSystem[nTankIdx].bStopSystemStandby == TRUE )
					{
						if( gTempCtrl.State.HeatingSystem[nTankIdx].bStandbyOn  == TRUE )
						{
							gTempCtrl.State.HeatingSystem[nTankIdx].bStandbyOn = FALSE;
							HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_START_HEATING;
						}
						else
						{
							BrbClearCallerBox((UDINT)&gTempCtrl.CallerBox.HeatingSystem[nTankIdx], sizeof( gTempCtrl.CallerBox.HeatingSystem[nTankIdx] ) );
						}
					}
					break;
				
				//-----------------------------------------------------------------------------------------------------------------------
				case eSTEP_HS_START_HEATING:
					strcpy(HeatingSystem[nTankIdx].Step.sStepText, "eSTEP_HS_START_HEATING");
					
					Tank[nTankIdx].Par.rSetTemp = gPar.ProductPar.TempCtrlPar.Tank[nTankIdx].rSetTemp;
					
					// Netzsch�tz f�r die Versorgung der Heizungen dieses Heizsystems einschalten
					gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingMainContactorClosed = TRUE;
					
					if( gTempCtrl.State.Tank[nTankIdx].bHeatingOn == FALSE )
					{
						Tank[nTankIdx].Cmd.bStartHeating = TRUE;
						
					}
					if( HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet == FALSE )
					{
						AppSetEvent( eEVT_INF_HEAT_SYS_WARM_UP, 0, "", nTankIdx + 1, &gEventManagement );
						HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet = TRUE;
					}
					
					if( HeatingSystem[nTankIdx].Infos.InfoHeatingSystemInStandby.bEventIsSet == TRUE )
					{
						AppResetEvent( eEVT_INF_HEAT_SYS_IN_STAND_BY, &gEventManagement );
						HeatingSystem[nTankIdx].Infos.InfoHeatingSystemInStandby.bEventIsSet = FALSE;
					}
					
					// n�chster Schritt:
					HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_HEATING_TANK_STARTED;
					break;
				
				//-----------------------------------------------------------------------------------------------------------------------
				case eSTEP_HS_HEATING_TANK_STARTED:
					strcpy(HeatingSystem[nTankIdx].Step.sStepText, "eSTEP_HS_HEATING_TANK_STARTED");
					
					if( gTempCtrl.State.Tank[nTankIdx].bHeatingOn == TRUE )
					{
						bBonDevExistInThisHeatingSystem = FALSE;
					
						for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
						{
							if( (gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist == TRUE) )
							{
								if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx == nTankIdx )
								{
									// Klebekopf zum einschalten der Heizung gefunden -> Schleife verlassen
									bBonDevExistInThisHeatingSystem = TRUE;
									break;
								}
							}
						}
						// Es muss keine Heizung eines Klebekopfes dieses Heizsystems eingeschaltet werden
						if( bBonDevExistInThisHeatingSystem == FALSE )
						{
							BrbClearCallerBox((UDINT)&gTempCtrl.CallerBox.HeatingSystem[nTankIdx], sizeof( gTempCtrl.CallerBox.HeatingSystem[nTankIdx] ) );
							HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_WAIT_FOR_COMMAND;
						}
						else
						{
							HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_START_HEATING_BON_DEV;
						}
					}
					break;
				
				//-----------------------------------------------------------------------------------------------------------------------
				case eSTEP_HS_START_HEATING_BON_DEV:
					strcpy(HeatingSystem[nTankIdx].Step.sStepText, "eSTEP_HS_START_HEATING_BON_DEV");
					
					for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
					{
						if( (gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist == TRUE) )
						{
							if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx == nTankIdx )
							{
								if( gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].bEnable == TRUE )
								{
									BonDev[nBonDevIdx].Cmd.bStartHeating = TRUE;
								}
								else
								{
									BonDev[nBonDevIdx].Cmd.bStopHeating = TRUE;
								}
							}
						}
					}
								
					HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_HEATING_BON_DEV_STARTED;
					break;
				
				//-----------------------------------------------------------------------------------------------------------------------
				case eSTEP_HS_HEATING_BON_DEV_STARTED:
					strcpy(HeatingSystem[nTankIdx].Step.sStepText, "eSTEP_HS_HEATING_BON_DEV_STARTED");
					
					bAllBonDevHeatingOn = TRUE;
					
					for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
					{
						if( (gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist == TRUE) )
						{
							if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx == nTankIdx )
							{
								if( gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].bEnable == TRUE )
								{									
									if( gTempCtrl.State.BonDev[nBonDevIdx].bHeatingOn == FALSE )
									{
										bAllBonDevHeatingOn = FALSE;
									}
								}
							}
						}
					}
					
					if( bAllBonDevHeatingOn == TRUE )
					{
						
						HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_WAIT_FOR_COMMAND;
						
						BrbClearCallerBox((UDINT)&gTempCtrl.CallerBox.HeatingSystem[nTankIdx], sizeof( gTempCtrl.CallerBox.HeatingSystem[nTankIdx] ) );
					}
					

					break;
					
				//-----------------------------------------------------------------------------------------------------------------------	
				case eSTEP_HS_START_STAND_BY:
					strcpy( HeatingSystem[nTankIdx].Step.sStepText, "eSTEP_HS_START_STAND_BY" );
					
					gTempCtrl.State.HeatingSystem[nTankIdx].bStandbyOn = TRUE;
					
					Tank[nTankIdx].Par.rSetTemp = gPar.ProductPar.TempCtrlPar.Tank[nTankIdx].rTempStandby;
					
					bBonDevExistInThisHeatingSystem = FALSE;
					
					for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
					{
						if( (gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist == TRUE) )
						{
							if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx == nTankIdx )
							{
								bBonDevExistInThisHeatingSystem = TRUE;
								
								for( nTempZoneIdx = 0; nTempZoneIdx <= nIDX_TEMP_CTRL_ZONE_MAX; nTempZoneIdx++ )
								{
									BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Par.rSetTemp = gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].rTempStandby;
								
									if( gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].bEnable == FALSE )
									{
										BonDev[nBonDevIdx].Cmd.bStopHeating = TRUE;
									}
									else
									{
										if( gTempCtrl.State.BonDev[nBonDevIdx].bHeatingOn == FALSE )
										{
											BonDev[nBonDevIdx].Cmd.bStartHeating = TRUE;
										}
									}
								}
							}
						}
					}
					
					if( bBonDevExistInThisHeatingSystem == TRUE )
					{
						HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_HEATING_BON_DEV_STARTED;
					}
					else
					{
						BrbClearCallerBox((UDINT)&gTempCtrl.CallerBox.HeatingSystem[nTankIdx], sizeof( gTempCtrl.CallerBox.HeatingSystem[nTankIdx] ) );
						// n�chster Schritt: Warten auf Kommando
						HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_WAIT_FOR_COMMAND;
					}
					
					if( HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet == TRUE )
					{
						AppResetEvent( eEVT_INF_HEAT_SYS_WARM_UP, &gEventManagement );
						HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet = FALSE;
					}
					
					if( HeatingSystem[nTankIdx].Infos.InfoHeatingSystemInStandby.bEventIsSet == FALSE )
					{
						AppSetEvent( eEVT_INF_HEAT_SYS_IN_STAND_BY, 0, "", nTankIdx + 1, &gEventManagement );
						HeatingSystem[nTankIdx].Infos.InfoHeatingSystemInStandby.bEventIsSet = TRUE;
					}
					
					break;
				
				
				//-----------------------------------------------------------------------------------------------------------------------
				case eSTEP_HS_ERR_STEP:
					strcpy( HeatingSystem[nTankIdx].Step.sStepText, "eSTEP_HS_ERR_STEP" );
									
					if( HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet == TRUE )
					{
						AppResetEvent( eEVT_INF_HEAT_SYS_WARM_UP, &gEventManagement );
						HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet = FALSE;
					}
					
					if( HeatingSystem[nTankIdx].Infos.InfoHeatingSystemInStandby.bEventIsSet == TRUE )
					{
						AppResetEvent( eEVT_INF_HEAT_SYS_IN_STAND_BY, &gEventManagement );
						HeatingSystem[nTankIdx].Infos.InfoHeatingSystemInStandby.bEventIsSet = FALSE;
					}
										
					// Quittierung der Fehler/Warnungen wurde angefordert
					if( gTempCtrl.DirectBox.bReset == TRUE )
					{
						if( gTempCtrl.State.Tank[nTankIdx].bErrorTempZone == TRUE )
						{
							Tank[nTankIdx].Cmd.bReset = TRUE;
						}
						
						for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
						{
							if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx == nTankIdx )
							{
								if( gTempCtrl.State.BonDev[nBonDevIdx].bErrorBonDev == TRUE )
								{
									BonDev[nBonDevIdx].Cmd.bReset = TRUE;
								}
							}
						}
					}
					if( gTempCtrl.State.HeatingSystem[nTankIdx].bErrorHeatingSystem == FALSE )
					{						
						BrbClearCallerBox((UDINT)&gTempCtrl.CallerBox.HeatingSystem[nTankIdx], sizeof( gTempCtrl.CallerBox.HeatingSystem[nTankIdx] ) );
						// n�chster Schritt:
						HeatingSystem[nTankIdx].Step.eStepNr = eSTEP_HS_WAIT_FOR_COMMAND;
					}
					break;
			}
		
			if( gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingOn == TRUE )
			{
				if( gTempCtrl.State.HeatingSystem[nTankIdx].bAutomaticPossible == TRUE )
				{	
					if( HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet == TRUE )
					{
						AppResetEvent( eEVT_INF_HEAT_SYS_WARM_UP, &gEventManagement );
						HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet = FALSE;
					}
				}
				else
				{
					if( gTempCtrl.State.HeatingSystem[nTankIdx].bStandbyOn == FALSE )
					{
						if( HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet == FALSE )
						{
							AppSetEvent( eEVT_INF_HEAT_SYS_WARM_UP, 0, "", nTankIdx + 1, &gEventManagement );
							HeatingSystem[nTankIdx].Infos.InfoHeatingSystemWarmUp.bEventIsSet = TRUE;
						}
					}
				}
			}
			
			// -------------------------------------
			// Ausg�nge f�r dieses Heizsystem setzen
			// -------------------------------------
			
			// Netzsch�tz f�r die Versorgung der Heizungen dieses Heizsystems ein- bzw. abschalten
			IO.Output.HeatingSystem[nTankIdx].bHeatingMainContactorClosed = gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingMainContactorClosed;
			// Dieser Ausgang meldet, dass die Temperatur des Tank und aller eingeschalteten K�pfe und deren konfigurierten Heizzoonen im Soll-Temperatur Toleranzfenster befinden.
			IO.Output.HeatingSystem[nTankIdx].bHeatingSystemReady = gTempCtrl.State.HeatingSystem[nTankIdx].bTempInToleranceWindow;
			
		}
		// Die Schleife hat den letzten der m�glichen beheizten Tanks durchlaufen
		if( nTankIdx == nIDX_TEMP_CTRL_TANK_MAX )
		{
			gTempCtrl.DirectBox.bReset = FALSE;
		}
	}
	
	// ===============================
	// Abarbeitung der beheizten Tanks
	// ===============================
	// Schleife �ber alle m�glichen beheizten Tanks (Heizsysteme)
	for( nTankIdx = 0; nTankIdx <= nIDX_TEMP_CTRL_TANK_MAX; nTankIdx++ )
	{		
		if( gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].bExist == TRUE )
		{
			// Standby-State vom �bergeordeten Heizsystem �bernehmen
			gTempCtrl.State.Tank[nTankIdx].bStandbyOn = gTempCtrl.State.HeatingSystem[nTankIdx].bStandbyOn;
			
			// ------------------------------------------------------------------------------
			// Pr�fen ob die Regler-Parameter ver�ndert wurden, w�hrend die Heizung aktiv ist
			// ------------------------------------------------------------------------------		
			if( gTempCtrl.State.Tank[nTankIdx].bHeatingOn == TRUE )
			{
				// Pr�fen ob sich die Parameter ver�nadert haben
				if( PIDParametersChanged( &Tank[nTankIdx].HeatingCtrl.PID, &gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].TempZonePar ) == TRUE )
				{
					// Parameter des PID-Reglers wurden ge�ndert -> Kommando zum Update des PID-Reglers absetzen
					Tank[nTankIdx].Cmd.bUpdateParameter = TRUE;
				}
			}
			
			// Soll-Temperaturen aus der Par-Struktur bei eingschaltetem Heizsystem zyklisch �bergeben
			if( gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingOn == TRUE )
			{
				if( gTempCtrl.State.HeatingSystem[nTankIdx].bStandbyOn == FALSE )
				{
					Tank[nTankIdx].Par.rSetTemp = gPar.ProductPar.TempCtrlPar.Tank[nTankIdx].rSetTemp;
				}
				else
				{
					Tank[nTankIdx].Par.rSetTemp = gPar.ProductPar.TempCtrlPar.Tank[nTankIdx].rTempStandby;
				}
			}
			
			// Die eingestellte Soll-Temperatur dieses Tanks betr�gt weniger als 25�C
			if( gPar.ProductPar.TempCtrlPar.Tank[nTankIdx].rSetTemp < 25 )
			{
				Infos.InfoTankSetTempToLow.bActive = TRUE;
				
				// Info ist noch nicht in der Visualisierung gesetzt
				if( Infos.InfoTankSetTempToLow.bEventIsSet == FALSE )
				{
					// Flag setzen, dass Info in Visualisierung gesetzt wurde
					Infos.InfoTankSetTempToLow.bEventIsSet = TRUE;
					// Info in Visualisierung setzen
					AppSetEvent( eEVT_INF_TANK_SET_TEMP_TO_LOW, 0, "", nTankIdx + 1, &gEventManagement );
				}
			}
			// Die eingestellte Soll-Temperatur dieses Tanks liegt �ber 25�C -> keine Info
			else
			{
				Infos.InfoTankSetTempToLow.bActive = FALSE;
				
				// Info steht noch in Visualisierung an
				if( Infos.InfoTankSetTempToLow.bEventIsSet == TRUE )
				{
					// Flag zur�cksetzen, dass die Info noch in der Visualisierung angezeigt wird
					Infos.InfoTankSetTempToLow.bEventIsSet = FALSE;
					// Info in Visualisierung zur�cksetzen
					AppResetEvent( eEVT_INF_TANK_SET_TEMP_TO_LOW, &gEventManagement );
				}
			}
			
			// -----------------------------------------------------
			// Auswertung der Temperatur am X20AT-Modul f�r den Tank
			// -----------------------------------------------------
			#ifndef SIM_TEMP_ZONES
			// Pr�fen ob Tempf�hler dieses Kanal OK ist und Umrechnung des Modulwertes in �C
			StatusCalcTempFromAT4222ErrCheck = CalcTempFromAT4222ErrCheck(	IO.Input.Tank[nTankIdx].nActTemp,
																			&gTempCtrl.State.Tank[nTankIdx],
																			&Tank[nTankIdx] );

			#endif
						
			
			// --------------------------------------------------------------------------------------
			// Fehlerauswertung und setzen der entsprechenden Meldungen bei defektem Temperaturf�hler
			// --------------------------------------------------------------------------------------
			// Die Fehler k�nnen nachdem das Heizsystem durch den Fehler abgeschaltet wurde, in der Visualisierung
			// Quittiert werden, auch wenn der Fehler noch nicht behoben wurde. Beim erneuten Versuch das Heizsystem
			// zu starten, wird der gleiche Fehler wieder gesetzt.
			// Dies wird so gehandhabt, damit auch mit einem Fehler am Temperatursf�hler des Tanks mit K�pfen ohne
			// Heizung oder eines anderen Heizsystem produziert werden kann, w�hrend dieses Heizsystem nicht aktiv ist.
			if( gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingOn == TRUE )
			{
				// Ein Drahtbruch bzw. eine Grenzwert�berschreitung erkannt
				if( Tank[nTankIdx].Errors.ErrTempSensorBrokenWire.bActive == TRUE )
				{
					if( Tank[nTankIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet == FALSE )
					{
						gTempCtrl.State.Tank[nTankIdx].bErrorTempZone = TRUE;
						AppSetEvent( eEVT_ERR_TANK_TEMP_SENS_BRK_WIRE, 0, "", nTankIdx + 1, &gEventManagement );
						Tank[nTankIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet = TRUE;
					}
				}
				// Eine Grenzwertunterschreitung wurde erkannt
				if( Tank[nTankIdx].Errors.ErrTempSensorMinValueReached.bActive == TRUE )
				{
					if( Tank[nTankIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet == FALSE )
					{
						gTempCtrl.State.Tank[nTankIdx].bErrorTempZone = TRUE;
						AppSetEvent( eEVT_ERR_TANK_TEMP_SENS_MIN_VAL, 0, "", nTankIdx + 1, &gEventManagement );
						Tank[nTankIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet = TRUE;
					}
				}
				// Der erfasste Wert liegt au�erhalb den g�ltigen Grenzen
				if( Tank[nTankIdx].Errors.ErrTempSensorInvalidValue.bActive == TRUE )
				{
					if( Tank[nTankIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet == FALSE )
					{
						gTempCtrl.State.Tank[nTankIdx].bErrorTempZone = TRUE;
						AppSetEvent( eEVT_ERR_TANK_TEMP_SENS_INVALID, 0, "", nTankIdx + 1, &gEventManagement );
						Tank[nTankIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet = TRUE;
					}
				}
			}
			// Heizsystem ist abgeschaltet -> die vorhandenen Fehler zur�cksetzen, um diese in der Visu quittieren zu k�nnen
			else
			{
				if( Tank[nTankIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet == TRUE )
				{
					Tank[nTankIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet = FALSE;
					AppResetEvent( eEVT_ERR_TANK_TEMP_SENS_BRK_WIRE, &gEventManagement );
				}
				
				if( Tank[nTankIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet == TRUE )
				{
					Tank[nTankIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet = FALSE;
					AppResetEvent( eEVT_ERR_TANK_TEMP_SENS_MIN_VAL, &gEventManagement );
				}
				
				if( Tank[nTankIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet == TRUE )
				{
					Tank[nTankIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet = FALSE;
					AppResetEvent( eEVT_ERR_TANK_TEMP_SENS_INVALID, &gEventManagement );
				}
			}
			
			// Der Temperaturf�hler liefert plausible Werte -> kein defekt am F�hler erkannt
			if( StatusCalcTempFromAT4222ErrCheck == ERR_OK )
			{
				if( gTempCtrl.State.Tank[nTankIdx].bHeatingOn == TRUE )
				{
					// Pr�fen, ob sich die Ist-Temperatur dieses Tanks im Toleranzfenster befindet
					gTempCtrl.State.Tank[nTankIdx].bTempInToleranceWindow = CheckIsTempInToleranceWindow(	gPar.ProductPar.TempCtrlPar.Tank[nTankIdx].rSetTemp,
																											gTempCtrl.State.Tank[nTankIdx].rActTemp,
													 														gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].rToleranzMin,
													 														gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].rToleranzMax );
	
					// Pr�fen, ob sich die Ist-Temperatur dieses Tanks im kritischen Temperaturbereich im Toleranzfenster befindet
					gTempCtrl.State.Tank[nTankIdx].bTempInWarningWindow = CheckIsTempInWarningWindow(	gPar.ProductPar.TempCtrlPar.Tank[nTankIdx].rSetTemp,
																										gTempCtrl.State.Tank[nTankIdx].rActTemp,
																										gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].rToleranzMin,
																										gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].rToleranzMax,
																										gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].rTempDiffWarning );
				}
				// Die Heizung des Tanks ist nicht mehr aktiv
				else
				{
					gTempCtrl.State.Tank[nTankIdx].bTempInToleranceWindow = FALSE;
					gTempCtrl.State.Tank[nTankIdx].bTempInWarningWindow = FALSE;
				}
			}
			// Es wurde ein Fehler am Temperaturfehler erkannt
			else
			{
				gTempCtrl.State.Tank[nTankIdx].bTempInToleranceWindow = FALSE;
				gTempCtrl.State.Tank[nTankIdx].bTempInWarningWindow = FALSE;
			}
			
			// -----------------------------------------------------------------------------------------------------------------------------------------
			// Funktionalit�t der Heizzone: Heizzone des Tanks ein-/ausschalten, Update der Parameter des PID-Reglers, Tuning mittels Sprungversuch usw.
			// -----------------------------------------------------------------------------------------------------------------------------------------
			HeatingCtrlTempZone( &Tank[nTankIdx], &gTempCtrl.State.Tank[nTankIdx], &gPar.MachinePar.TempCtrlConfig.Tank[nTankIdx].TempZonePar );
			
			// -----------------------------------------------------------------------
			// Simulation der Heizzone wenn dies f�r diese Konfiguration gew�nscht ist
			// -----------------------------------------------------------------------
			#ifdef SIM_TEMP_ZONES
			gTempCtrl.State.Tank[nTankIdx].rActTemp = SimulateTempZone( Tank[nTankIdx].HeatingCtrl.PID.Out, &Tank[nTankIdx].Simulation );
			#endif
			
			
			// --------------------------------------------------------------------------------
			// Fehlerauswertung der Tanks und setzen der entsprechenden Fehlermeldungen in Visu
			// --------------------------------------------------------------------------------

			if( Tank[nTankIdx].Errors.ErrActTempLeftToleranceWindow.bActive == TRUE )
			{
				if( Tank[nTankIdx].Errors.ErrActTempLeftToleranceWindow.bEventIsSet == FALSE )
				{
					
					Tank[nTankIdx].Errors.ErrActTempLeftToleranceWindow.bEventIsSet = TRUE;
					
					AppSetEvent( eEVT_ERR_TEMP_TANK_NOT_IN_TOL, 0, "", nTankIdx + 1, &gEventManagement );
					AppResetEvent( eEVT_ERR_TEMP_TANK_NOT_IN_TOL, &gEventManagement );
					
					if( gTempCtrl.State.HeatingSystem[nTankIdx].bOneOrMoreBonDevProdutionEnabled == TRUE )
					{
						gMainLogic.DirectBox.bStop = TRUE;
					}	
				}
			}

			// �bertemperatur erkannt
			if( Tank[nTankIdx].Errors.ErrOverTemperature.bActive == TRUE )
			{
				if( Tank[nTankIdx].Errors.ErrOverTemperature.bEventIsSet == FALSE )
				{
					Tank[nTankIdx].Errors.ErrOverTemperature.bEventIsSet = TRUE;
					
					AppSetEvent( eEVT_ERR_TANK_OVER_TEMP, 0, "", nTankIdx + 1, &gEventManagement );
					AppResetEvent( eEVT_ERR_TANK_OVER_TEMP, &gEventManagement );
					
					gTempCtrl.State.Tank[nTankIdx].bErrorTempZone = TRUE;
					
					Tank[nTankIdx].rTemperatureHeatingSwitchedOff = gTempCtrl.State.Tank[nTankIdx].rActTemp;
				}
			}
			
			if( Tank[nTankIdx].Errors.ErrHeatingShorted.bActive == TRUE )
			{
				if( Tank[nTankIdx].Errors.ErrHeatingShorted.bEventIsSet == FALSE )
				{
					AppSetEvent( eEVT_ERR_TANK_HEATING_SHORTED, 0, "", nTankIdx + 1, &gEventManagement );
					
					Tank[nTankIdx].Errors.ErrHeatingShorted.bEventIsSet = TRUE;
				}
				
				gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingMainContactorClosed = FALSE;
			}
			else
			{
				if( Tank[nTankIdx].Errors.ErrHeatingShorted.bEventIsSet == TRUE )
				{
					Tank[nTankIdx].Errors.ErrHeatingShorted.bEventIsSet = FALSE;
					
					AppResetEvent( eEVT_ERR_TANK_HEATING_SHORTED, &gEventManagement );
				}
			}
			
			if( Tank[nTankIdx].Errors.ErrPIDParameterInvalid.bActive == TRUE )
			{
				AppSetEvent( eEVT_ERR_TANK_PID_PAR_INVALID, Tank[nTankIdx].StatusIDs.nPID_StatusID, "", nTankIdx + 1, &gEventManagement );
				AppResetEvent( eEVT_ERR_TANK_PID_PAR_INVALID, &gEventManagement );
			}
			
			if( Tank[nTankIdx].Warnings.WarnPIDParameterInvalid.bActive == TRUE )
			{
				if( Tank[nTankIdx].Warnings.WarnPIDParameterInvalid.bEventIsSet == FALSE )
				{
					Tank[nTankIdx].Warnings.WarnPIDParameterInvalid.bEventIsSet = TRUE;
					AppSetEvent( eEVT_WAR_TANK_PID_PAR_INVALID, Tank[nTankIdx].StatusIDs.nPID_StatusID, "", nTankIdx + 1, &gEventManagement );
				}
			}
			else
			{
				if( Tank[nTankIdx].Warnings.WarnPIDParameterInvalid.bEventIsSet == TRUE )
				{
					Tank[nTankIdx].Warnings.WarnPIDParameterInvalid.bEventIsSet = FALSE;
					AppResetEvent( eEVT_WAR_TANK_PID_PAR_INVALID, &gEventManagement );
				}
			}
			
			if( Tank[nTankIdx].Warnings.WarnUpdateAbortedInvalidPIDPar.bActive == TRUE )
			{
				AppSetEvent( eEVT_WAR_TANK_UPDATE_PID_ABORTED, Tank[nTankIdx].StatusIDs.nPID_StatusID, "", nTankIdx + 1, &gEventManagement );
				AppResetEvent( eEVT_WAR_TANK_UPDATE_PID_ABORTED, &gEventManagement );
			}
			
			if( Tank[nTankIdx].Warnings.WarnPIDInteral.bActive == TRUE )
			{
				AppSetEvent( eEVT_WAR_TANK_PID_INTERNAL, Tank[nTankIdx].StatusIDs.nPID_StatusID, "", nTankIdx + 1, &gEventManagement );
				AppResetEvent( eEVT_WAR_TANK_PID_INTERNAL, &gEventManagement );
			}
			
			// ------------------------------------------------------------------
			// Status des Heizungsausgangs des Tanks auf der Ausgangskarte pr�fen
			// ------------------------------------------------------------------
			#ifndef SIM_TEMP_ZONES
			if( IO.Input.Tank[nTankIdx].bStatusHeatingDigitalOutput != IO.Output.Tank[nTankIdx].bHeatingOn )
			{
				// Der digitale Ausgang sollte FALSE sein, der Status des Ausgangs meldet jedoch TRUE -> Fremdsignal liegt am Ausgang an (evtl. Br�cke)
				if( IO.Output.Tank[nTankIdx].bHeatingOn == FALSE )
				{
					// Tank in Fehlerstatus setzen
					gTempCtrl.State.Tank[nTankIdx].bErrorTempZone = TRUE;
					// Netzsch�tz abfallen lassen, um unkontrolliertes gef�hrliches Aufheizen zu verhindern
					gTempCtrl.State.HeatingSystem[nTankIdx].bHeatingMainContactorClosed = FALSE;
					
					if( Tank[nTankIdx].Errors.ErrExternalHeatingSignal.bEventIsSet == FALSE )
					{
						Tank[nTankIdx].Errors.ErrExternalHeatingSignal.bEventIsSet = TRUE;
						// Fehlermeldung absetzen
						AppSetEvent( eEVT_ERR_TANK_EXT_HEATING_SIGNAL, 0, "", nTankIdx + 1, &gEventManagement );
					}
				}
			}
			else
			{
				if( Tank[nTankIdx].Errors.ErrExternalHeatingSignal.bEventIsSet == TRUE )
				{
					Tank[nTankIdx].Errors.ErrExternalHeatingSignal.bEventIsSet = FALSE;
					AppResetEvent( eEVT_ERR_TANK_EXT_HEATING_SIGNAL, &gEventManagement );
				}
			}
			
			// ---------------------------------
			// Heizungsausg�nge des Tanks setzen
			// ---------------------------------
			IO.Output.Tank[nTankIdx].bHeatingOn = gTempCtrl.State.Tank[nTankIdx].bOutputHeating;
			#endif
		
		}
		// ---------------------------------
		// Dieser Tank besitzt keine Heizung
		// ---------------------------------
		else
		{
			// Die Heizung dieses Tanks wurde soeben in der Konfiguration abgew�hlt
			if( NegEdge.TempCtrlTankExist[nTankIdx].Q == TRUE )
			{
				// Strukturen zur�cksetzen
				memset( &gTempCtrl.State.Tank[nTankIdx], 0x00, sizeof( gTempCtrl.State.Tank[nTankIdx] ) );
				memset( &Tank[nTankIdx], 0x00, sizeof( Tank[nTankIdx] ) );
				// Alle Heizungsausg�nge dieses Tanks zur�cksetzen
				memset( &IO.Output.Tank[nTankIdx], 0x00, sizeof( IO.Output.Tank[nTankIdx] ) );
				
				// Schleife �ber alle Klebek�pfe
				for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
				{
					// Der Klebekopf mit Index nBonDevIdx besitzte eine Heizung
					if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist == TRUE )
					{
						// Der Klebekopf geh�rt zu dem bereits oben gepr�ften Tank
						if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx == nTankIdx )
						{
							// Strukturen zur�cksetzen
							memset( &gTempCtrl.State.BonDev[nBonDevIdx], 0x00, sizeof( gTempCtrl.State.BonDev[nBonDevIdx] ) );
							memset( &BonDev[nBonDevIdx], 0x00, sizeof( BonDev[nBonDevIdx] ) );
							// Alle Heizungsausg�nge dieser Klebek�pfe zur�cksetzen
							memset( &IO.Output.BonDev[nBonDevIdx], 0x00, sizeof( IO.Output.BonDev[nBonDevIdx] ) );
						}
					}
				}
			}
		}
	}
	
	// ------------------------------------------------------------------------------------------------------------------
	// Pr�fen ob Fehler-/Warnmeldungen in der Visu quittiert wurden und das bEventIsSet-Flag f�r alle Tanks zur�cksetzen.
	// ------------------------------------------------------------------------------------------------------------------
	// Dies ist n�tig, da das Event-Management nicht multi-instanzf�hig ist und somit nach und nach alle Fehler der ver-
	// schiedenen Tanks in der Visualisierung angezeigt werden.
	
	// Fehlermeldung "�bertemperatur" oder "Temperatur au�erhalb der Toleranzgrenzen" wurde in der Visualisierung quittiert
	if( (AppIsEventAcknowledged( eEVT_ERR_TEMP_TANK_NOT_IN_TOL, &gEventManagement ) == TRUE) ||
		(AppIsEventAcknowledged( eEVT_ERR_TANK_OVER_TEMP, &gEventManagement ) == TRUE) )
	{			
		// Schleife �ber alle m�glichen beheizten Tanks (Heizsysteme)
		for( nTankIdx = 0; nTankIdx <= nIDX_TEMP_CTRL_TANK_MAX; nTankIdx++ )
		{
			Tank[nTankIdx].Errors.ErrActTempLeftToleranceWindow.bEventIsSet = FALSE;
			Tank[nTankIdx].Errors.ErrOverTemperature.bEventIsSet = FALSE;
		}
		gTempCtrl.DirectBox.bReset = TRUE;
	}
	
	// Fehlermeldungen "Drahtbruch", "Grenzwertunterschreitung" oder "ung�lter Wert" wurde in der Visualisierung quittiert
	if( (AppIsEventAcknowledged( eEVT_ERR_TANK_TEMP_SENS_BRK_WIRE, &gEventManagement ) == TRUE) ||
		(AppIsEventAcknowledged( eEVT_ERR_TANK_TEMP_SENS_MIN_VAL, &gEventManagement ) == TRUE) ||
		(AppIsEventAcknowledged( eEVT_ERR_TANK_TEMP_SENS_INVALID, &gEventManagement ) == TRUE) )
	{
		// Schleife �ber alle m�glichen beheizten Tanks (Heizsysteme)
		for( nTankIdx = 0; nTankIdx <= nIDX_TEMP_CTRL_TANK_MAX; nTankIdx++ )
		{
			Tank[nTankIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet = FALSE;
			Tank[nTankIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet = FALSE;
			Tank[nTankIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet = FALSE;
		}
		gTempCtrl.DirectBox.bReset = TRUE;
	}
	
	// Fehlermeldung "Kurzschluss Heizung" wurde in der Visualisierung quittiert
	if( AppIsEventAcknowledged( eEVT_ERR_TANK_HEATING_SHORTED, &gEventManagement ) == TRUE )
	{
		// Schleife �ber alle m�glichen beheizten Tanks (Heizsysteme)
		for( nTankIdx = 0; nTankIdx <= nIDX_TEMP_CTRL_TANK_MAX; nTankIdx++ )
		{
			Tank[nTankIdx].Errors.ErrHeatingShorted.bEventIsSet = FALSE;
		}
		gTempCtrl.DirectBox.bReset = TRUE;
	}
	
	// Fehlermeldung "Externe Heizungsansteuerung" wurde in der Visualisierung quittiert
	if( AppIsEventAcknowledged( eEVT_ERR_TANK_EXT_HEATING_SIGNAL, &gEventManagement ) == TRUE )
	{
		// Schleife �ber alle m�glichen beheizten Tanks (Heizsysteme)
		for( nTankIdx = 0; nTankIdx <= nIDX_TEMP_CTRL_TANK_MAX; nTankIdx++ )
		{
			Tank[nTankIdx].Errors.ErrExternalHeatingSignal.bEventIsSet = FALSE;
		}
		gTempCtrl.DirectBox.bReset = TRUE;
	}
	
	// Fehlermeldung "Ung�ltige PID-Parameter" wurde in der Visualisierung quittiert
	if( (AppIsEventAcknowledged(eEVT_ERR_TANK_PID_PAR_INVALID, &gEventManagement) == 1)	)
	{
		gTempCtrl.DirectBox.bReset = TRUE;
	}
	
	
	// =======================================================================
	// Abarbeitung der beheizten Klebek�pfe und deren konfigurierten Heizzonen
	// =======================================================================
	// Schleife �ber alle konfigurierten Klebek�pfe
	for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
	{
		// Negative Flanke des Exist-Flags aus der Konfiguration f�r diesen Klebekopf ermitteln
		NegEdge.TempCtrlBonDevExist[nBonDevIdx].CLK = gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist;
		F_TRIG( &NegEdge.TempCtrlBonDevExist[nBonDevIdx] );
		
		// ------------------------------------------------
		// Der Klebekopf ist mit einer Heizung ausgestattet
		// ------------------------------------------------
		if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist == TRUE )
		{
			
			for( nTempZoneIdx = 0; nTempZoneIdx <= nIDX_TEMP_CTRL_ZONE_MAX; nTempZoneIdx++ )
			{
				// Dieser Klebekopf soll mit dem Start der Heizzone mit dem Heizen beginnen, und diese Heizzone ist vorhanden
				if( (gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].bEnable == TRUE) &&
					(gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bZoneExist == TRUE) )
				{
					// Flag setzen, welches sp�ter in der Schleife bei einer Heizzone welche au�erhalb des Tolernazfenster ist, zur�ckgesetzt wird.
					gTempCtrl.State.BonDev[nBonDevIdx].bTempInToleranceWindow = TRUE;
					// Flag setzen, welches sp�ter in der Schleife bei nicht eingeschalteter Heizzone zur�ckgesetzt wird.
					gTempCtrl.State.BonDev[nBonDevIdx].bHeatingOn = TRUE;
					// Schleife verlassen
					break;
				}
				else
				{
					// Die Schleife ist bis zum Ende durchgelaufen -> die IF-Bedingung wurde innerhalb der Schleife nicht erf�llt
					if( nTempZoneIdx == nIDX_TEMP_CTRL_ZONE_MAX )
					{
						// Flags zur�cksetzen, da die Heizung dieses Kopfes entweder nicht eingeschaltet wird oder dieser Kopf keine Heizzonen besitzt
						gTempCtrl.State.BonDev[nBonDevIdx].bTempInToleranceWindow = FALSE;
						gTempCtrl.State.BonDev[nBonDevIdx].bHeatingOn = FALSE;
					}
				}
			}
			
			// -------------------------------------------------------------------------------------------------
			// �berpr�fen, ob der Sollwert der vorhandenen Heizzonen dieses Kopfes auf �ber 25�C eingestellt ist
			// -------------------------------------------------------------------------------------------------
			// Schleife �ber alle Heizzonen dieses Kopfes
			for( nTempZoneIdx = 0; nTempZoneIdx <= nIDX_TEMP_CTRL_ZONE_MAX; nTempZoneIdx++ )
			{
				if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bZoneExist == TRUE )
				{
					// Die eingestellte Soll-Temperatur dieses Tanks betr�gt weniger als 25�C
					if( gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].rSetTemp[nTempZoneIdx] < 25 )
					{
						Infos.InfoBondDevSetTempToLow.bActive = TRUE;
						
						// Info ist noch nicht in der Visualisierung gesetzt
						if( Infos.InfoBondDevSetTempToLow.bEventIsSet == FALSE )
						{
							// Flag setzen, dass Info in Visualisierung gesetzt wurde
							Infos.InfoBondDevSetTempToLow.bEventIsSet = TRUE;
							// Info in Visualisierung setzen
							AppSetEvent( eEVT_INF_HEAD_SET_TEMP_TO_LOW, 0, "", nBonDevIdx + 1, &gEventManagement );
						}
						// Schleife verlassen
						break;
					}
				}
				// Die Schleife ist bis zum Ende durchgelaufen -> die Soll-Temperatur aller Heizzonen ist gr��er gleich 25�C konfiguriert
				if( nTempZoneIdx == nIDX_TEMP_CTRL_ZONE_MAX )
				{
					Infos.InfoBondDevSetTempToLow.bActive = FALSE;
					
					if( Infos.InfoBondDevSetTempToLow.bActive == FALSE )
					{	
						// Info steht noch in Visualisierung an
						if( Infos.InfoBondDevSetTempToLow.bEventIsSet == TRUE )
						{
							// Flag zur�cksetzen, dass die Info noch in der Visualisierung angezeigt wird
							Infos.InfoBondDevSetTempToLow.bEventIsSet = FALSE;
							// Info in Visualisierung zur�cksetzen
							AppResetEvent( eEVT_INF_HEAD_SET_TEMP_TO_LOW, &gEventManagement );
						}
					}
				}
			}
			
			// Flags zur�cksetzen
			gTempCtrl.State.BonDev[nBonDevIdx].bErrorBonDev = FALSE;
			gTempCtrl.State.BonDev[nBonDevIdx].bTempInWarningWindow = FALSE;
			
			// Schleife �ber alle m�glichen Heizzonen dieses Kopfes
			for( nTempZoneIdx = 0; nTempZoneIdx <= nIDX_TEMP_CTRL_ZONE_MAX; nTempZoneIdx++ )
			{
				// Negative Flanke des Exist-Flags aus der Konfiguration f�r diese Heizzone des Klebekopfes ermitteln
				NegEdge.TempCtrlZoneExist[nBonDevIdx][nTempZoneIdx].CLK = gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bZoneExist;
				F_TRIG( &NegEdge.TempCtrlZoneExist[nBonDevIdx][nTempZoneIdx] );

				if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bZoneExist == TRUE )
				{
					// Das �bergeordnete Heizsystem ist im Standby -> Den Status dieser Heizzone in Standby setzen
					gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bStandbyOn = gTempCtrl.State.HeatingSystem[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].bStandbyOn;					
					
					// ------------------------------------------------------------------------------
					// Pr�fen ob die Regler-Parameter ver�ndert wurden, w�hrend die Heizung aktiv ist
					// ------------------------------------------------------------------------------
					if( gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bHeatingOn == TRUE )
					{
						// Pr�fen ob sich die Parameter ver�nadert haben
						if( PIDParametersChanged( &BonDev[nBonDevIdx].TempZone[nTempZoneIdx].HeatingCtrl.PID, &gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].TempZone[nTempZoneIdx] ) == TRUE )
						{
							// Parameter des PID-Reglers wurden ge�ndert -> Kommando zum Update des PID-Reglers absetzen
							BonDev[nBonDevIdx].Cmd.bUpdateParameter = TRUE;
						}
					}
					
					// --------------------------------------------------------------------------------------------
					// Kommandos die an den Klebekopf geschickt werden auswerten und auf dessen Heizzonen verteilen
					// --------------------------------------------------------------------------------------------
					if( BonDev[nBonDevIdx].Cmd.bStopHeating == TRUE )
					{
						BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Cmd.bStopHeating = TRUE;
					}
					else if( BonDev[nBonDevIdx].Cmd.bStartHeating == TRUE )
					{
						if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bZoneExist == TRUE )
						{
							
							if( gTempCtrl.State.HeatingSystem[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].bStandbyOn == FALSE )
							{
								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Par.rSetTemp = gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].rSetTemp[nTempZoneIdx];
							}
							else
							{
								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Par.rSetTemp = gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].rTempStandby;
							}
							
							if( gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bHeatingOn == FALSE )
							{
								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Cmd.bStartHeating = TRUE;
							}
						}
					}
					else if( BonDev[nBonDevIdx].Cmd.bUpdateParameter == TRUE )
					{
						BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Cmd.bUpdateParameter = TRUE;
					}
					else if( BonDev[nBonDevIdx].Cmd.bReset == TRUE )
					{
						if( gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bErrorTempZone == TRUE )	
						{
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Cmd.bReset = TRUE;
						}
					}
					
					// Soll-Temperaturen aus der Par-Struktur bei eingschaltetem Kopf zyklisch �bernehmen
					if( gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bHeatingOn == TRUE )
					{	
						if( gTempCtrl.State.HeatingSystem[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].bHeatingOn == TRUE )
						{
							if( gTempCtrl.State.HeatingSystem[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].bStandbyOn == FALSE )
							{
								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Par.rSetTemp = gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].rSetTemp[nTempZoneIdx];
							}
							else
							{
								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Par.rSetTemp = gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].rTempStandby;
							}
						}
					}
					
					// -----------------------------------------------------------------------
					// Auswertung der Temperatur am X20AT-Modul f�r die Heizzone dieses Kopfes
					// -----------------------------------------------------------------------
					#ifndef SIM_TEMP_ZONES
					// Pr�fen ob Tempf�hler dieses Kanals OK ist und Umrechnung des Modulwertes in �C
					StatusCalcTempFromAT4222ErrCheck = CalcTempFromAT4222ErrCheck(	IO.Input.BonDev[nBonDevIdx].nActTemp_Zone[nTempZoneIdx],
																					&gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx],
																					&BonDev[nBonDevIdx].TempZone[nTempZoneIdx] );
					#endif
					
					// Der Temperaturf�hler liefert plausible Werte -> kein defekt am F�hler erkannt
					if( StatusCalcTempFromAT4222ErrCheck == ERR_OK )
					{
						if( gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bHeatingOn == TRUE )
						{
							// Pr�fen, ob sich die Ist-Temperatur dieser Heizzone des Klebekopfes im Toleranzfenster befindet
							gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bTempInToleranceWindow = CheckIsTempInToleranceWindow( gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].rSetTemp[nTempZoneIdx],
																																			 gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].rActTemp,
										 																									 gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].rToleranzMin,
										 																									 gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].rToleranzMax );
	
							// Pr�fen, ob sich die Ist-Temperatur dieser Heizzone im kritischen Temperaturbereich im Toleranzfenster befindet
							gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bTempInWarningWindow = CheckIsTempInWarningWindow( gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].rSetTemp[nTempZoneIdx],
																																		 gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].rActTemp,
										 																								 gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].rToleranzMin,
										 																								 gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].rToleranzMax,
																																		 gPar.MachinePar.TempCtrlConfig.Tank[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].rTempDiffWarning );
						}
						// Die Heizung dieser Heizzone des Kopfes ist nicht mehr aktiv
						else
						{
							gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bTempInToleranceWindow = FALSE;
							gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bTempInWarningWindow = FALSE;
						}
					}
					// Es wurde ein Fehler am Temperaturf�hler erkannt
					else
					{
						gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bTempInToleranceWindow = FALSE;
						gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bTempInWarningWindow = FALSE;
					}
					
					// --------------------------------------------------------------------------------------
					// Fehlerauswertung und setzen der entsprechenden Meldungen bei defektem Temperaturf�hler
					// --------------------------------------------------------------------------------------
					// Die Fehler k�nnen nachdem das Heizsystem durch den Fehler abgeschaltet wurde, in der Visualisierung
					// Quittiert werden, auch wenn der Fehler noch nicht behoben wurde. Beim erneuten Versuch das Heizsystem
					// zu starten, wird der gleiche Fehler wieder gesetzt.
					// Dies wird so gehandhabt, damit auch mit einem Fehler am Temperatursf�hler des Tanks mit K�pfen ohne
					// Heizung oder eines anderen Heizsystem produziert werden kann, w�hrend dieses Heizsystem nicht aktiv ist.
					
					// Die Heizzonen dieses Klebekopfes sind aktiv und werden eingeschaltet, sobald das �bergeordnete Heizsystem eingeschaltet wird
					if( gTempCtrl.State.HeatingSystem[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].bHeatingOn == TRUE )
//					if( gPar.ProductPar.TempCtrlPar.BonDev[nBonDevIdx].bEnable == TRUE )
					{
							
						// Ein Drahtbruch bzw. eine Grenzwert�berschreitung erkannt
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorBrokenWire.bActive == TRUE )
						{
							if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet == FALSE )
							{
								gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bErrorTempZone = TRUE;
								AppSetEvent( eEVT_ERR_HEAD_TEMP_SENS_BRK_WIRE, 0, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement );
								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet = TRUE;
							}
						}
//						// Der Fehler wurde behoben -> Meldung zur�cksetzen
//						else
//						{
//							if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet == TRUE )
//							{
//								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet = FALSE;
//								AppResetEvent( eEVT_ERR_HEAD_TEMP_SENS_BRK_WIRE, &gEventManagement );
//							}
//						}
						
						// Eine Grenzwertunterschreitung wurde erkannt
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorMinValueReached.bActive == TRUE )
						{
							if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet == FALSE )
							{
								gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bErrorTempZone = TRUE;
								AppSetEvent( eEVT_ERR_HEAD_TEMP_SENS_MIN_VAL, 0, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement );
								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet = TRUE;
							}
						}
//						// Der Fehler wurde behoben -> Meldung zur�cksetzen
//						else
//						{
//							if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet == TRUE )
//							{
//								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet = FALSE;
//								AppResetEvent( eEVT_ERR_HEAD_TEMP_SENS_MIN_VAL, &gEventManagement );
//							}
//						}
						
						// Der erfasste Wert liegt au�erhalb den g�ltigen Grenzen
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorInvalidValue.bActive == TRUE )
						{
							if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet == FALSE )
							{
								gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bErrorTempZone = TRUE;
								AppSetEvent( eEVT_ERR_HEAD_TEMP_SENS_INVALID, 0, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement );
								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet = TRUE;
							}
						}
//						// Der Fehler wurde behoben -> Meldung zur�cksetzen
//						else
//						{
//							if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet == TRUE )
//							{
//								BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet = FALSE;
//								AppResetEvent( eEVT_ERR_HEAD_TEMP_SENS_INVALID, &gEventManagement );
//							}
//						}
						
						
					}
					// Die Heizung des Klebekopfes wurde ausgeschaltet bzw. ist nicht eingeschaltet
					// -> Gesetzte Fehlermeldungen zur�cksetzen 
					else
					{
						// Meldung zur�cksetzen falls gesetzt: Ein Drahtbruch bzw. eine Grenzwert�berschreitung erkannt
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet == TRUE )
						{
							AppResetEvent( eEVT_ERR_HEAD_TEMP_SENS_BRK_WIRE, &gEventManagement );
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet = FALSE;
						}
						// Meldung zur�cksetzen falls gesetzt: Eine Grenzwertunterschreitung wurde erkannt
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet == TRUE )
						{
							AppResetEvent( eEVT_ERR_HEAD_TEMP_SENS_MIN_VAL, &gEventManagement );
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet = FALSE;
						}
						// Meldung zur�cksetzen falls gesetzt: Der erfasste Wert liegt au�erhalb den g�ltigen Grenzen
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet == TRUE )
						{
							AppResetEvent( eEVT_ERR_HEAD_TEMP_SENS_INVALID, &gEventManagement );
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet = FALSE;
						}
					}
					
					// -----------------------------------------------------------------------------------------------------------------------------------------
					// Funktionalit�t der Heizzone: Heizzone des Kopfes ein-/ausschalten, Update der Parameter des PID-Reglers, Tuning mittels Sprungversuch usw.
					// -----------------------------------------------------------------------------------------------------------------------------------------
					HeatingCtrlTempZone( &BonDev[nBonDevIdx].TempZone[nTempZoneIdx], &gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx], &gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].TempZone[nTempZoneIdx] );
					
					// -----------------------------------------------------------------------
					// Simulation der Heizzone wenn dies f�r diese Konfiguration gew�nscht ist
					// -----------------------------------------------------------------------
					#ifdef SIM_TEMP_ZONES
					gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].rActTemp = SimulateTempZone( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].HeatingCtrl.PID.Out, &BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Simulation );
					#endif
					
					// ------------------------------------------------------------------------------------
					// Fehlerauswertung der Heizzonen und setzen der entsprechenden Fehlermeldungen in Visu
					// ------------------------------------------------------------------------------------

					if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrActTempLeftToleranceWindow.bActive == TRUE )
					{
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrActTempLeftToleranceWindow.bEventIsSet == FALSE )
						{
							
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrActTempLeftToleranceWindow.bEventIsSet = TRUE;
							
							AppSetEvent( eEVT_ERR_TEMP_BON_DEV_NOT_IN_TOL, 0, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement );
							AppResetEvent( eEVT_ERR_TEMP_BON_DEV_NOT_IN_TOL, &gEventManagement );
							
							if( gTempCtrl.State.HeatingSystem[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].bOneOrMoreBonDevProdutionEnabled == TRUE )
							{
								gMainLogic.DirectBox.bStop = TRUE;
							}	
						}
					}
					
					// �bertemperatur erkannt
					if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrOverTemperature.bActive == TRUE )
					{
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrOverTemperature.bEventIsSet == FALSE )
						{
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrOverTemperature.bEventIsSet = TRUE;
							
							AppSetEvent( eEVT_ERR_BON_DEV_OVER_TEMP, 0, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement );
							AppResetEvent( eEVT_ERR_BON_DEV_OVER_TEMP, &gEventManagement );
							
							gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bErrorTempZone = TRUE;
							
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].rTemperatureHeatingSwitchedOff = gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].rActTemp;
						}
					}

					if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrHeatingShorted.bActive == TRUE )
					{
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrHeatingShorted.bEventIsSet == FALSE )
						{
							AppSetEvent( eEVT_ERR_BON_DEV_HEATING_SHORTED, 0, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement );
							
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrHeatingShorted.bEventIsSet = TRUE;
						}
						
						gTempCtrl.State.HeatingSystem[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].bHeatingMainContactorClosed = FALSE;
					}
					else
					{
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrHeatingShorted.bEventIsSet == TRUE )
						{
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrHeatingShorted.bEventIsSet = FALSE;
							
							AppResetEvent( eEVT_ERR_BON_DEV_HEATING_SHORTED, &gEventManagement );
						}
					}
					
					if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrPIDParameterInvalid.bActive == TRUE )
					{
						AppSetEvent(eEVT_ERR_HEAD_PID_PAR_INVALID, BonDev[nBonDevIdx].TempZone[nTempZoneIdx].StatusIDs.nPID_StatusID, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement);
						AppResetEvent(eEVT_ERR_HEAD_PID_PAR_INVALID, &gEventManagement);
					}
					
					
					
					if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Warnings.WarnPIDParameterInvalid.bActive == TRUE )
					{
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Warnings.WarnPIDParameterInvalid.bEventIsSet == FALSE )
						{
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Warnings.WarnPIDParameterInvalid.bEventIsSet = TRUE;
							AppSetEvent( eEVT_WAR_HEAD_PID_PAR_INVALID, BonDev[nBonDevIdx].TempZone[nTempZoneIdx].StatusIDs.nPID_StatusID, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement );
						}
					}
					else
					{
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Warnings.WarnPIDParameterInvalid.bEventIsSet == TRUE )
						{
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Warnings.WarnPIDParameterInvalid.bEventIsSet = FALSE;
							AppResetEvent( eEVT_WAR_HEAD_PID_PAR_INVALID, &gEventManagement );
						}
					}
					
					if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Warnings.WarnUpdateAbortedInvalidPIDPar.bActive == TRUE )
					{
						AppSetEvent( eEVT_WAR_HEAD_UPDATE_PID_ABORTED, BonDev[nBonDevIdx].TempZone[nTempZoneIdx].StatusIDs.nPID_StatusID, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement );
						AppResetEvent( eEVT_WAR_HEAD_UPDATE_PID_ABORTED, &gEventManagement );
					}
					
					if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Warnings.WarnPIDInteral.bActive == TRUE )
					{
						AppSetEvent( eEVT_WAR_HEAD_PID_INTERNAL, BonDev[nBonDevIdx].TempZone[nTempZoneIdx].StatusIDs.nPID_StatusID, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement );
						AppResetEvent( eEVT_WAR_HEAD_PID_INTERNAL, &gEventManagement );
					}
					
					// ------------------------------------------------------------------------------------------------------------------------------------------
					// Status der einzelnen Heizzonen des Kopfes zusammenf�hren (erspart beim "HeatingSystem" die �berpr�fung jeder einzelner Heizzone des Kopfes)
					// ------------------------------------------------------------------------------------------------------------------------------------------
					if( (gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bHeatingOn == FALSE) ||
						(gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bErrorTempZone == TRUE) )
					{
						gTempCtrl.State.BonDev[nBonDevIdx].bHeatingOn = FALSE;
					}
					
					if( gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bErrorTempZone == TRUE )
					{
						// Mindestens eine Temperaturzone dieses Klebekopfes hat einen Fehler
						gTempCtrl.State.BonDev[nBonDevIdx].bErrorBonDev = TRUE;
					}
						
					if( gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bTempInToleranceWindow == FALSE )
					{
						gTempCtrl.State.BonDev[nBonDevIdx].bTempInToleranceWindow = FALSE;
					}
					
					if( gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bTempInWarningWindow == TRUE )
					{
						gTempCtrl.State.BonDev[nBonDevIdx].bTempInWarningWindow = TRUE;
					}
				}
				// ---------------------------------------------
				// Heizzone mit diesem Index ist nicht vorhanden
				// ---------------------------------------------
				else
				{
					// Diese Heizzone wurde soeben in der Konfiguration abgew�hlt
					if( NegEdge.TempCtrlZoneExist[nBonDevIdx][nTempZoneIdx].Q == TRUE )
					{
						// Struktur "State" dieser Heizzone des Klebek�pfes auf Null setzen, da diese nicht mehr beheizt und auch nicht mehr angezeigt wird
						memset( &gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx], 0x00, sizeof( gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx] ) );
						memset( &BonDev[nBonDevIdx].TempZone[nTempZoneIdx], 0x00, sizeof( BonDev[nBonDevIdx].TempZone[nTempZoneIdx] ) );
					}
				}
				
				
				
				// ---------------------------------------------------------------------------
				// Status des Heizungsausgangs der Temperaturzone auf der Ausgangskarte pr�fen
				// ---------------------------------------------------------------------------
				#ifndef SIM_TEMP_ZONES
				if( IO.Input.BonDev[nBonDevIdx].bStatusHeatingDigitalOutput[nTempZoneIdx] != IO.Output.BonDev[nBonDevIdx].bHeatingOn[nTempZoneIdx] )
				{
					// Der digitale Ausgang sollte FALSE sein, der Status des Ausgangs meldet jedoch TRUE -> Fremdsignal liegt am Ausgang an (evtl. Br�cke)
					if( IO.Output.BonDev[nBonDevIdx].bHeatingOn[nTempZoneIdx] == FALSE )
					{
						// Heizzone in Fehlerstatus setzen
						gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bErrorTempZone = TRUE;
						// Netzsch�tz abfallen lassen, um unkontrolliertes gef�hrliches Aufheizen zu verhindern
						gTempCtrl.State.HeatingSystem[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].bHeatingMainContactorClosed = FALSE;
						
						if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrExternalHeatingSignal.bEventIsSet == FALSE )
						{
							BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrExternalHeatingSignal.bEventIsSet = TRUE;
							// Fehlermeldung absetzen
							AppSetEvent( eEVT_ERR_HEAD_EXT_HEATING_SIGNAL, 0, sTempZoneName[nTempZoneIdx], nBonDevIdx + 1, &gEventManagement );
						}
					}
				}
				else
				{
					if( BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrExternalHeatingSignal.bEventIsSet == TRUE )
					{
						BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrExternalHeatingSignal.bEventIsSet = FALSE;
						AppResetEvent( eEVT_ERR_HEAD_EXT_HEATING_SIGNAL, &gEventManagement );
					}
				}
				
				// -----------------------------------------------------
				// Heizungsausg�nge der jeweiligen Temperaturzone setzen
				// -----------------------------------------------------
				IO.Output.BonDev[nBonDevIdx].bHeatingOn[nTempZoneIdx] = gTempCtrl.State.BonDev[nBonDevIdx].TempZone[nTempZoneIdx].bOutputHeating;
				#endif
			
				// Die Schleife hat alle Temperaturzonen durchlaufen
				if( nTempZoneIdx == nIDX_TEMP_CTRL_ZONE_MAX )
				{
					// Die Kommandos k�nnen nun r�ckgesetzt werden
					BonDev[nBonDevIdx].Cmd.bStopHeating = FALSE;
					BonDev[nBonDevIdx].Cmd.bStartHeating = FALSE;
					BonDev[nBonDevIdx].Cmd.bStopHeating = FALSE;
					BonDev[nBonDevIdx].Cmd.bUpdateParameter = FALSE;
					BonDev[nBonDevIdx].Cmd.bReset = FALSE;
				}
			}
			
			// Pr�fen ob die Temperatur beruhigt ist
			// Wichtig!
			// Diese Funktion muss au�erhalb der TEMP_ZONE-Schleife aufgerufen werden, da die Eingangs-Werte von "gTempCtrl.State.BonDev[nBonDevIdx].bTempInToleranceWindow"
			// und "gTempCtrl.State.BonDev[nBonDevIdx].bTempInWarningWindow" innerhalb der Schleife nicht eindeutig sind.
			BonDevAutomaticPossible[nBonDevIdx].bTempInToleranceWindow = gTempCtrl.State.BonDev[nBonDevIdx].bTempInToleranceWindow;
			BonDevAutomaticPossible[nBonDevIdx].bTempInWarningWindow = gTempCtrl.State.BonDev[nBonDevIdx].bTempInWarningWindow;
			BonDevAutomaticPossible[nBonDevIdx].nSettlingTime = 5000;
			
			CheckIsTempInToleranceAndSettled( &BonDevAutomaticPossible[nBonDevIdx] );
			
			// Ist Standby nicht aktiv, pr�fen ob dieser Kopf in den Betriebsmodus "Automatik" geschaltet werden darf
			if( (gTempCtrl.State.HeatingSystem[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].bStandbyOn == FALSE) &&
				(gTempCtrl.State.HeatingSystem[gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx].bHeatingOn == TRUE) &&
				(BonDevAutomaticPossible[nBonDevIdx].bTempInToleranceAndSettled == TRUE) )
			{
				gTempCtrl.State.BonDev[nBonDevIdx].bAutomaticPossible = TRUE;
			}
			// Dieses Heizsystem ist in Standby -> Automatik nicht m�glich
			else
			{
				gTempCtrl.State.BonDev[nBonDevIdx].bAutomaticPossible = FALSE;
			}
			
			// Die Heizzonen des Klebekopfes sind nicht im Solltemperatur-Fenster bzw. der Kopf heizt nicht
			if( (BonDevAutomaticPossible[nBonDevIdx].bTempInToleranceAndSettled == FALSE) ||
				(gTempCtrl.State.BonDev[nBonDevIdx].bHeatingOn == FALSE) )
			{
				// -> Automatik f�r diesen Kopf ist nicht m�glich
				gTempCtrl.State.BonDev[nBonDevIdx].bAutomaticPossible = FALSE;
			}
		}
		// ----------------------------------------
		// Dieser Klebekopfes besitzt keine Heizung
		// ----------------------------------------
		else
		{
			// Die Heizung dieses Klebekopfes wurde in der Konfiguration soeben abgew�hlt
			if( NegEdge.TempCtrlBonDevExist[nBonDevIdx].Q == TRUE )
			{
				// Struktur "State" dieses Klebek�pfe auf Null setzen, da dieser nicht mehr beheizt und auch nicht mehr angezeigt wird
				memset( &gTempCtrl.State.BonDev[nBonDevIdx], 0x00, sizeof( gTempCtrl.State.BonDev[nBonDevIdx] ) );
				memset( &BonDev[nBonDevIdx], 0x00, sizeof( BonDev[nBonDevIdx] ) );
				// Alle Heizungsausg�nge dieses Klebekopfes zur�cksetzen
				memset( &IO.Output.BonDev[nBonDevIdx], 0x00, sizeof( IO.Output.BonDev[nBonDevIdx] ) );
			}
		}
	}
	
	// ------------------------------------------------------------------------------------------------------------------
	// Pr�fen ob Fehler-/Warnmeldungen in der Visu quittiert wurden und das bEventIsSet-Flag f�r alle K�pfe zur�cksetzen.
	// ------------------------------------------------------------------------------------------------------------------
	// Dies ist n�tig, da das Event-Management nicht multi-instanzf�hig ist und somit nach und nach alle Fehler der ver-
	// schiedenen Klebek�pfe und Heizzonen in der Visualisierung angezeigt werden.
	
	// Fehlermeldung "�bertemperatur" oder "Temperatur au�erhalb der Toleranzgrenzen" wurde in der Visualisierung quittiert
	if( (AppIsEventAcknowledged( eEVT_ERR_TEMP_BON_DEV_NOT_IN_TOL, &gEventManagement ) == TRUE) ||
		(AppIsEventAcknowledged( eEVT_ERR_BON_DEV_OVER_TEMP, &gEventManagement ) == TRUE) )
	{			
		// Schleife �ber alle konfigurierten Klebek�pfe
		for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
		{
			for( nTempZoneIdx = 0; nTempZoneIdx <= nIDX_TEMP_CTRL_ZONE_MAX; nTempZoneIdx++ )
			{
				BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrActTempLeftToleranceWindow.bEventIsSet = FALSE;
				BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrOverTemperature.bEventIsSet = FALSE;
			}
		}
		gTempCtrl.DirectBox.bReset = TRUE;
	}
	
	// Fehlermeldungen "Drahtbruch", "Grenzwertunterschreitung" oder "ung�lter Wert" wurde in der Visualisierung quittiert
	if( (AppIsEventAcknowledged( eEVT_ERR_HEAD_TEMP_SENS_BRK_WIRE, &gEventManagement ) == TRUE) ||
		(AppIsEventAcknowledged( eEVT_ERR_HEAD_TEMP_SENS_MIN_VAL, &gEventManagement ) == TRUE) ||
		(AppIsEventAcknowledged( eEVT_ERR_HEAD_TEMP_SENS_INVALID, &gEventManagement ) == TRUE) )
	{
		for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
		{
			for( nTempZoneIdx = 0; nTempZoneIdx <= nIDX_TEMP_CTRL_ZONE_MAX; nTempZoneIdx++ )
			{
				BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorBrokenWire.bEventIsSet = FALSE;
				BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorMinValueReached.bEventIsSet = FALSE;
				BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrTempSensorInvalidValue.bEventIsSet = FALSE;
			}
		}
		gTempCtrl.DirectBox.bReset = TRUE;
	}
	
	// Fehlermeldung "Kurzschluss Heizung" wurde in der Visualisierung quittiert
	if( AppIsEventAcknowledged( eEVT_ERR_BON_DEV_HEATING_SHORTED, &gEventManagement ) == TRUE )
	{
		for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
		{
			for( nTempZoneIdx = 0; nTempZoneIdx <= nIDX_TEMP_CTRL_ZONE_MAX; nTempZoneIdx++ )
			{
				BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrHeatingShorted.bEventIsSet = FALSE;
			}
		}
		gTempCtrl.DirectBox.bReset = TRUE;
	}
	
	// Fehlermeldung "Externe Heizungsansteuerung" wurde in der Visualisierung quittiert
	if( AppIsEventAcknowledged( eEVT_ERR_HEAD_EXT_HEATING_SIGNAL, &gEventManagement ) == TRUE )
	{
		for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
		{
			for( nTempZoneIdx = 0; nTempZoneIdx <= nIDX_TEMP_CTRL_ZONE_MAX; nTempZoneIdx++ )
			{
				BonDev[nBonDevIdx].TempZone[nTempZoneIdx].Errors.ErrExternalHeatingSignal.bEventIsSet = FALSE;
			}
		}
		gTempCtrl.DirectBox.bReset = TRUE;
	}
	
	// Fehlermeldung "Ung�ltige PID-Parameter" wurde in der Visualisierung quittiert
	if( (AppIsEventAcknowledged(eEVT_ERR_HEAD_PID_PAR_INVALID, &gEventManagement) == 1) )
	{
		gTempCtrl.DirectBox.bReset = TRUE;
	}
	
}

void _EXIT TempCtrlEXIT(void)
{

}
