/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: TempCtrl
 * File: TempCtrlFunc.c
 * Author: Michael Zimmer
 * Created: 19.01.2015
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <TempCtrlFunc.h>

//UINT CalcTempFromAT4222ErrCheck( INT nValueFromAT4222, REAL *pCalcedTemp )
UINT CalcTempFromAT4222ErrCheck( INT nValueFromAT4222, TempCtrlStateTempZone_TYP *pTempZoneState, TempCtrlTempZone_TYP *pTempZone )
{
						
	UINT	RetVal = ERR_OK;
	
	if( (pTempZoneState != 0x00) &&
		(pTempZone != 0x00) )
	{
		// Pr�fen ob der Messwert sich au�erhalb g�ltigen Bereich des Moduls befindet ( G�litger Bereich des X20AT4222: -200�C <= Messwert >= 850 �C )
		if( (nValueFromAT4222 < -2000) ||
			(nValueFromAT4222 > 8500) )
		{
			RetVal = nERR_X20AT_TEMP_SENSORS;
			
			// -> Das X20AT-Temperaturmodul hat einen Fehler auf diesem Kanal erkannt
			if( nValueFromAT4222 == 32767 )
			{
				// Drahtbruch bzw. oberer Grenzwert �berschritten
				pTempZone->Errors.ErrTempSensorBrokenWire.bActive = TRUE;
				// Ein Fehler ist in dieser Heizzone aufgetreten
//				pTempZoneState->bErrorTempZone = TRUE;
	//			return nERR_X20AT_BROKEN_WIRE_DETECTED;
			}
			else if( nValueFromAT4222 == -32767 )
			{
				// Unterer Grenzwert �berschritten
				pTempZone->Errors.ErrTempSensorMinValueReached.bActive = TRUE;
				// Ein Fehler ist in dieser Heizzone aufgetreten
//				pTempZoneState->bErrorTempZone = TRUE;
	
//				return nERR_X20AT_MIN_VALUE_ERR;
			}
			else
			{
				// Ung�ltiger Wert
				// Unterer Grenzwert �berschritten
				pTempZone->Errors.ErrTempSensorInvalidValue.bActive = TRUE;
				// Ein Fehler ist in dieser Heizzone aufgetreten
//				pTempZoneState->bErrorTempZone = TRUE;
	//			return nERR_X20AT_INVALID_VALUE;
			}
		}
		else
		{	
			// Die 10tel �C die das X20AT-Modul liefert, in ganze �C umrechnen
			pTempZoneState->rActTemp = (REAL)(nValueFromAT4222) / 10;
			
			pTempZone->Errors.ErrTempSensorBrokenWire.bActive = FALSE;
			pTempZone->Errors.ErrTempSensorMinValueReached.bActive = FALSE;
			pTempZone->Errors.ErrTempSensorInvalidValue.bActive = FALSE;
			
			RetVal = ERR_OK;
		}
	}
	else
	{
		RetVal = nERR_NULL_POINTER;
	}
	
	return RetVal;
}

BOOL CheckIsTempInToleranceWindow( REAL rSetTemp, REAL rActTemp, REAL rToleranceMin, REAL rToleranceMax )
{
	if( (rActTemp >= rSetTemp - rToleranceMin) &&
		(rActTemp <= rSetTemp + rToleranceMax) )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
	
}

BOOL CheckIsTempInWarningWindow( REAL rSetTemp, REAL rActTemp, REAL rToleranceMin, REAL rToleranceMax, REAL rPercentWarning )
{
	if( (rActTemp >= rSetTemp - rToleranceMin) &&
		(rActTemp <= rSetTemp - (rToleranceMin * rPercentWarning/*%*/ / 100/*%*/ )) )
	{
		return TRUE;
	}
	else if( (rActTemp <= rSetTemp + rToleranceMax) &&
			 (rActTemp >= rSetTemp + (rToleranceMax * rPercentWarning/*%*/ / 100/*%*/ )) )
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


DINT HeatingCtrlTempZone( TempCtrlTempZone_TYP *pTempZone, TempCtrlStateTempZone_TYP *pTempZoneState, TempCtrlTempZoneCfgPar_TYP *pTempZonePar )
{
	// Interne Variablen
	DINT				StatusID = ERR_OK;
	MTBasicsPID_typ		PID_old = {};
	
	// Die Parameter und auch die restlichen Variablen vom PID-Regler f�r diesen Zyklus zwischenspeichern
	memcpy( &PID_old, &pTempZone->HeatingCtrl.PID, sizeof( PID_old ) );
	
	if( pTempZone->Step.bInitDone == TRUE )
	{
		// Kommandos, welche direkt ausgef�hrt werden m�ssen
		if( pTempZone->Cmd.bStopHeating == TRUE )
		{	
			pTempZone->Cmd.bStopHeating = FALSE;
			pTempZone->Step.eStepNr	= eSTEP_TZ_STOP_HEATING;
		}
	}
	
	// Eine Soll-Temperatur�nderung wird angefordert
	if( pTempZone->Par.rSetTemp != pTempZone->HeatingCtrl.PID.SetValue )
	{
		pTempZoneState->bTemperatureChanged = TRUE;
	}
	else if( pTempZoneState->bTempInToleranceWindow == TRUE )
	{
		pTempZoneState->bTemperatureChanged = FALSE;
	}
	
	// StepHandling
	if( pTempZone->StepHandling.Current.bTimeoutElapsed == 1 )
	{
		pTempZone->StepHandling.Current.bTimeoutElapsed = 0;
		pTempZone->Step.eStepNr = pTempZone->StepHandling.Current.nTimeoutContinueStep;
	}
	pTempZone->StepHandling.Current.nStepNr = (DINT)pTempZone->Step.eStepNr;
	strcpy(pTempZone->StepHandling.Current.sStepText, pTempZone->Step.sStepText);
	BrbStepHandler(&pTempZone->StepHandling);
	
	// Schrittkette dieser Heizzone
	switch( pTempZone->Step.eStepNr )
	{
		//
		case eSTEP_TZ_INIT:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_INIT" );
		
			pTempZone->Simulation.DeadTime.DelayTime = 0.9;
			pTempZone->Simulation.PT2.Gain = 3.9;
			pTempZone->Simulation.PT2.TimeConstant1 = 5.0;
			pTempZone->Simulation.PT2.TimeConstant2 = 12.5;
			
			pTempZone->HeatingCtrl.StepTuning.SystemSettlingTime = 5.0;
			pTempZone->HeatingCtrl.StepTuning.StepHeight = 25;
			
			pTempZone->Step.eStepNr	= eSTEP_TZ_INIT_WAIT_FOR_PAR_VALID;
		
			break;
		
		case eSTEP_TZ_INIT_WAIT_FOR_PAR_VALID:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_INIT_WAIT_FOR_PAR_VALID" );
		
			if( gParHandling.State.bPParValid == TRUE )
			{
				pTempZone->Step.eStepNr = eSTEP_TZ_INIT_FINISHED;
			}
			break;
		
		case eSTEP_TZ_INIT_FINISHED:
			strcpy(pTempZone->Step.sStepText, "eSTEP_TZ_INIT_FINISHED");
			pTempZone->Step.bInitDone = TRUE;
			pTempZone->Step.eStepNr = eSTEP_TZ_WAITING_FOR_COMMAND;
			break;
			
		// Warten auf Kommandos
		case eSTEP_TZ_WAITING_FOR_COMMAND:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_WAITING_FOR_COMMAND" );
			
			if( pTempZone->Cmd.bStartHeating == TRUE )
			{
				
				pTempZone->Cmd.bStartHeating = FALSE;
			
				pTempZone->Step.eStepNr	= eSTEP_TZ_START_HEATING;
			}
			
			if( pTempZone->Cmd.bUpdateParameter == TRUE )
			{
				pTempZone->Cmd.bUpdateParameter = FALSE;
				pTempZone->Step.eStepNr	= eSTEP_TZ_UPDATE_PARAMETER;
			}
			
			if( pTempZone->Cmd.bStartStepTuning == TRUE )
			{
				pTempZone->Cmd.bStartStepTuning = FALSE;
				pTempZone->Step.eStepNr	= eSTEP_TZ_START_STEP_TUNING;
			}
			
			if( pTempZone->Cmd.bReset == TRUE )
			{
				pTempZone->Step.eStepNr	= eSTEP_HS_ERR_STEP;
			}
			break;
		
		case eSTEP_TZ_START_HEATING:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_START_HEATING" );
			
			pTempZone->HeatingCtrl.PID.Enable = TRUE;
			
			pTempZone->HeatingCtrl.PWM.Enable = TRUE;
			
			pTempZone->HeatingCtrl.PID.EnableTracking = FALSE;
			
			pTempZone->HeatingCtrl.PID.MaxOut = pTempZonePar->rMaxOut;
			pTempZone->HeatingCtrl.PID.MinOut = pTempZonePar->rMinOut;
			pTempZone->HeatingCtrl.PID.PIDParameters.DerivativeTime = pTempZonePar->rDerivativeTime;
			pTempZone->HeatingCtrl.PID.PIDParameters.FilterTime = pTempZonePar->rFilterTime;
			pTempZone->HeatingCtrl.PID.PIDParameters.Gain = pTempZonePar->rGain;
			pTempZone->HeatingCtrl.PID.PIDParameters.IntegrationTime = pTempZonePar->rIntegrationTime;
			
			pTempZone->Step.eStepNr	= eSTEP_TZ_WAIT_HEATING_STARTED;
			break;
			
		case eSTEP_TZ_WAIT_HEATING_STARTED:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_WAIT_HEATING_STARTED" );
			
			if( (pTempZone->HeatingCtrl.PID.Error == FALSE) &&
				(pTempZone->HeatingCtrl.PWM.Error == FALSE) )
			{
				if( pTempZone->HeatingCtrl.PID.Active == TRUE )
				{
					pTempZoneState->bHeatingOn = TRUE;
					pTempZone->Step.eStepNr	= eSTEP_TZ_WAITING_FOR_COMMAND;
				}
			}
			else
			{
				pTempZone->Step.eStepNr = eSTEP_TZ_ERR_STEP;
			}
			
			break;
		
		case eSTEP_TZ_STOP_HEATING:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_STOP_HEATING" );
			
			// Ist-Temperatur merken:
			// - wenn das Heizsystem abgeschaltet wird (ist nicht schon aus)
			// - UND nicht der Fehler "Kurzschluss Heizung" anliegt
			// Mit der gemerkten Temperatur kann ein Kurzschluss im Heizelement erkannt werden
			if( (pTempZone->Errors.ErrHeatingShorted.bActive == FALSE) &&
				(pTempZoneState->bHeatingOn == TRUE) )
			{
				// Ist-Temperatur bei Abschaltung merken
				pTempZone->rTemperatureHeatingSwitchedOff = pTempZoneState->rActTemp;
			}
			
			pTempZone->HeatingCtrl.PID.Enable = FALSE;
			pTempZone->HeatingCtrl.StepTuning.Enable = FALSE;
			
			pTempZoneState->bHeatingOn = FALSE;
			
			pTempZone->Step.eStepNr	= eSTEP_TZ_WAITING_FOR_COMMAND;
			
			break;
		
		case eSTEP_TZ_UPDATE_PARAMETER:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_UPDATE_PARAMETER" );
			
			// Der PID-Regler ist aktiv
			if( pTempZone->HeatingCtrl.PID.Enable == TRUE )
			{
				// Update-Eingang des PID-Reglers setzen
				pTempZone->HeatingCtrl.PID.Update = TRUE;
								
				// Die ge�nderten Parameter an den PID-Regler Funktionsblock �bergeben
				pTempZone->HeatingCtrl.PID.MaxOut = pTempZonePar->rMaxOut;
				pTempZone->HeatingCtrl.PID.MinOut = pTempZonePar->rMinOut;
				pTempZone->HeatingCtrl.PID.PIDParameters.DerivativeTime = pTempZonePar->rDerivativeTime;
				pTempZone->HeatingCtrl.PID.PIDParameters.FilterTime = pTempZonePar->rFilterTime;
				pTempZone->HeatingCtrl.PID.PIDParameters.Gain = pTempZonePar->rGain;
				pTempZone->HeatingCtrl.PID.PIDParameters.IntegrationTime = pTempZonePar->rIntegrationTime;
				
				// PID-Regler Funktionsblock aufrufen um zu pr�fen ob die ge�nderten Parameter g�ltig sind
				MTBasicsPID( &pTempZone->HeatingCtrl.PID );
				
				// Eine Warnung liegt am PID-Regler Funktionsblock an
				if( ((pTempZone->HeatingCtrl.PID.StatusID & 0xC0000000) != 0xC0000000 ) &&
					((pTempZone->HeatingCtrl.PID.StatusID & 0x80000000) == 0x80000000 ) &&
					((pTempZone->HeatingCtrl.PID.StatusID & 0x20000000) != 0x20000000) )
				{
					// StatusID des PID-Reglers merken
					pTempZone->StatusIDs.nPID_StatusID = pTempZone->HeatingCtrl.PID.StatusID;
					
					// Update-Eingang des PID-Reglers zur�cksetzen
					pTempZone->HeatingCtrl.PID.Update = FALSE;
					
					// PID-Parameter in der Parameter-Struktur auf die Werte zur�cksetzen, die vor dem Update auf die neuen Parameter g�ltig waren
					pTempZonePar->rMaxOut = PID_old.MaxOut;
					pTempZonePar->rMinOut = PID_old.MinOut;
					pTempZonePar->rDerivativeTime = PID_old.PIDParameters.DerivativeTime;
					pTempZonePar->rFilterTime = PID_old.PIDParameters.FilterTime;
					pTempZonePar->rGain = PID_old.PIDParameters.Gain;
					pTempZonePar->rIntegrationTime = PID_old.PIDParameters.IntegrationTime;
					
					// Parameter des PID-Reglers auf die alten Werte setzen
					pTempZone->HeatingCtrl.PID.MaxOut = pTempZonePar->rMaxOut;
					pTempZone->HeatingCtrl.PID.MinOut = pTempZonePar->rMinOut;
					pTempZone->HeatingCtrl.PID.PIDParameters.DerivativeTime = pTempZonePar->rDerivativeTime;
					pTempZone->HeatingCtrl.PID.PIDParameters.FilterTime = pTempZonePar->rFilterTime;
					pTempZone->HeatingCtrl.PID.PIDParameters.Gain = pTempZonePar->rGain;
					pTempZone->HeatingCtrl.PID.PIDParameters.IntegrationTime = pTempZonePar->rIntegrationTime;
					
					// Warnung ausgeben, dass der ge�nderte Parameter nicht �bernommen wurde						
					pTempZone->Warnings.WarnUpdateAbortedInvalidPIDPar.bActive = TRUE;
				
				}
				else if( pTempZone->HeatingCtrl.PID.UpdateDone == TRUE )
				{
					pTempZone->Warnings.WarnUpdateAbortedInvalidPIDPar.bActive = FALSE;
					pTempZone->HeatingCtrl.PID.Update = FALSE;
					pTempZone->Step.eStepNr	= eSTEP_TZ_WAITING_FOR_COMMAND;
				}
			}
			else
			{
				pTempZone->HeatingCtrl.PID.Update = FALSE;
				pTempZone->Step.eStepNr	= eSTEP_TZ_WAITING_FOR_COMMAND;
			}
			break;
			
		case eSTEP_TZ_START_STEP_TUNING:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_START_STEP_TUNING" );
			
			pTempZone->HeatingCtrl.StepTuning.Enable = TRUE;
			pTempZone->HeatingCtrl.StepTuning.Start = TRUE;
			pTempZone->HeatingCtrl.PWM.Enable = TRUE;
			
			pTempZone->HeatingCtrl.StepTuning.MaxActValue = pTempZonePar->rSetTempMax;
			
			pTempZone->HeatingCtrl.PID.Enable = TRUE;
			pTempZone->HeatingCtrl.PID.EnableTracking = TRUE;
						
			if( pTempZone->HeatingCtrl.StepTuning.Active == TRUE )
			{
				pTempZone->Step.eStepNr	= eSTEP_TZ_WAIT_TUNING_DONE;
			}
			else if( pTempZone->HeatingCtrl.StepTuning.Error == TRUE )
			{	
				pTempZone->Step.eStepNr = eSTEP_TZ_ERR_STEP;
			}
			break;
			
		case eSTEP_TZ_WAIT_TUNING_DONE:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_WAIT_TUNING_DONE" );
			
			if( pTempZone->HeatingCtrl.StepTuning.TuningActive == FALSE )
			{
				pTempZone->HeatingCtrl.StepTuning.Start = FALSE;
				
				pTempZone->HeatingCtrl.PID.EnableTracking = FALSE;
				
				pTempZone->HeatingCtrl.PID.Enable = FALSE;
			
				if( pTempZone->HeatingCtrl.StepTuning.TuningDone == TRUE )
				{

					pTempZone->Step.eStepNr	= eSTEP_TZ_TUNING_DONE;
				}
				else if( pTempZone->HeatingCtrl.StepTuning.Error == TRUE )
				{
					pTempZone->StatusIDs.nStepTuning_StatusID = pTempZone->HeatingCtrl.StepTuning.StatusID;
					pTempZone->Step.eStepNr	= eSTEP_TZ_ERR_STEP;
				}
			}
			break;
		
		case eSTEP_TZ_TUNING_DONE:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_TUNING_DONE" );
			
			// Parameter f�r diesen PID-Regler �bernehmen
			if( pTempZone->Cmd.bApplyPIDParameters == TRUE )
			{
				// Wichtig!!!
				// Nachdem diese Werte mittels "bApplyPIDParameters" �bernommen wurden, sinde diese nur fl�chtig in der Parameter-Struktur gPar vorhanden.
				// Um diese Parameter auch nach einem Neustart zur Verf�gung zu haben, m�ssen diese als Rezept gespeichert werden.
				pTempZonePar->rGain = pTempZone->HeatingCtrl.StepTuning.PIDParameters.Gain;
				pTempZonePar->rIntegrationTime = pTempZone->HeatingCtrl.StepTuning.PIDParameters.IntegrationTime;
				pTempZonePar->rDerivativeTime = pTempZone->HeatingCtrl.StepTuning.PIDParameters.DerivativeTime;
				pTempZonePar->rFilterTime = pTempZone->HeatingCtrl.StepTuning.PIDParameters.FilterTime;
				
				pTempZone->Cmd.bApplyPIDParameters = FALSE;
				pTempZone->Step.eStepNr	= eSTEP_TZ_WAITING_FOR_COMMAND;
			}
			
			// Mit dem Kommando "bStopHeating" werden die Parameter nicht �bernommen und es wird in den Schritt eSTEP_TZ_WAITING_FOR_COMMAND gesprungen.
			break;
			
		case eSTEP_TZ_ERR_STEP:
			strcpy( pTempZone->Step.sStepText, "eSTEP_TZ_ERR_STEP" );
		
			pTempZoneState->bErrorTempZone = TRUE;
			
			if( pTempZone->Cmd.bReset == TRUE )
			{
				if( pTempZone->HeatingCtrl.PID.Error == TRUE )
				{
					pTempZone->HeatingCtrl.PID.Enable = FALSE;
				}
				if( pTempZone->HeatingCtrl.PWM.Error == TRUE )
				{
					pTempZone->HeatingCtrl.PWM.Enable = FALSE;
				}
				if( pTempZone->HeatingCtrl.StepTuning.Error == TRUE )
				{
					pTempZone->HeatingCtrl.StepTuning.Enable = FALSE;
				}
				
				pTempZone->Cmd.bReset = FALSE;
				
				pTempZoneState->bErrorTempZone = FALSE;
				
				pTempZone->Step.eStepNr = eSTEP_TZ_WAITING_FOR_COMMAND;
			}
			
			break;
		
		
		default:
			strcpy( pTempZone->Step.sStepText, "default:" );
			
				pTempZone->Step.eStepNr = eSTEP_TZ_WAITING_FOR_COMMAND;
			break;
	}
	
	// -----------------------------------------------------------
	// Aufruf des Funktionsblocks zum Ermitteln der Regelparameter
	// -----------------------------------------------------------
	if( pTempZone->HeatingCtrl.StepTuning.Active == TRUE )
	{
		pTempZone->HeatingCtrl.StepTuning.ActValue = pTempZoneState->rActTemp;
	}	
	
	// Aufruf des Funktionsblocks
	MTBasicsStepTuning( &pTempZone->HeatingCtrl.StepTuning );
		
	if( pTempZone->HeatingCtrl.StepTuning.Active == TRUE )
	{
		// W�hrend des Tuningvorgangs wird die Stellgr��e aus dem Tuning-FUB auf den Force-Eingang des PID-Reglers geschrieben
		pTempZone->HeatingCtrl.PID.TrackingValue = pTempZone->HeatingCtrl.StepTuning.Out;
	}
	
	
	// -------------------------------------------------------------------
	// Aufruf des Funktionsblocks zur Regelung der Temperatur der Heizzone
	// -------------------------------------------------------------------
	pTempZone->HeatingCtrl.PID.SetValue = pTempZone->Par.rSetTemp;
	pTempZone->HeatingCtrl.PID.ActValue = pTempZoneState->rActTemp;
	
	// Aufruf des PID-Funktionsblocks
	MTBasicsPID( &pTempZone->HeatingCtrl.PID );
	
	// Der PID-Regler hat einen Fehler
	if( pTempZone->HeatingCtrl.PID.Error == TRUE )
	{		
		pTempZone->StatusIDs.nPID_StatusID = StatusID = pTempZone->HeatingCtrl.PID.StatusID;
		pTempZone->HeatingCtrl.PID.Enable = FALSE;
		pTempZone->Step.eStepNr = eSTEP_TZ_ERR_STEP;
		
		pTempZone->Errors.ErrPIDParameterInvalid.bActive = TRUE;
	}
	else
	{
		pTempZone->Errors.ErrPIDParameterInvalid.bActive = FALSE;
	}
		
	// Der PID-Regler gibt eine Warnung aus	
	if( (pTempZone->HeatingCtrl.PID.StatusID == mtBCD_WRN_OUTPUT_LIMIT_INVALID ) ||
		(pTempZone->HeatingCtrl.PID.StatusID == mtBCD_WRN_GAIN_ZERO) ||
		(pTempZone->HeatingCtrl.PID.StatusID == mtBCD_WRN_GAIN_NEGATIV ) ||
		(pTempZone->HeatingCtrl.PID.StatusID == mtBCD_WRN_INTEGRATION_TIME) ||
		(pTempZone->HeatingCtrl.PID.StatusID == mtBCD_WRN_DERIVATIVE_TIME_NEG ) ||
		(pTempZone->HeatingCtrl.PID.StatusID == mtBCD_WRN_FILTER_TIME_NEGATIV ) ||
		(pTempZone->HeatingCtrl.PID.StatusID == mtBCD_WRN_FILTER_TIME_LOW ) ||
		(pTempZone->HeatingCtrl.PID.StatusID == mtBCD_WRN_INTEGRATION_TIME) )
	{
		if( pTempZone->Warnings.WarnUpdateAbortedInvalidPIDPar.bActive == FALSE )
		{
			pTempZone->StatusIDs.nPID_StatusID = pTempZone->HeatingCtrl.PID.StatusID;
			pTempZone->Warnings.WarnPIDParameterInvalid.bActive = TRUE;
		}
	}
	else
	{
		pTempZone->Warnings.WarnPIDParameterInvalid.bActive = FALSE;
	}
	
	// Der PID-Regler hat einen internen Fehler und gibt eine Warnung aus
	if( pTempZone->HeatingCtrl.PID.StatusID == mtGNR_WRN_OTHER_FB_USES_SYSREF )
	{
		pTempZone->Warnings.WarnPIDInteral.bActive = TRUE;
	}
	else
	{
		pTempZone->Warnings.WarnPIDInteral.bActive = FALSE;
	}
	
	// --------------------------------------------------------------------------------
	// Aufruf des Funktionsblocks zur Erzeugung des PWM-Signals zur Heizungsansteuerung
	// --------------------------------------------------------------------------------
	pTempZone->HeatingCtrl.PWM.DutyCycle = pTempZone->HeatingCtrl.PID.Out;
	pTempZone->HeatingCtrl.PWM.Enable = TRUE;
	pTempZone->HeatingCtrl.PWM.Period = 1;
	
	// Aufruf des Funktionsblocks
	MTBasicsPWM( &pTempZone->HeatingCtrl.PWM );
	
	if( pTempZone->HeatingCtrl.PWM.Error == TRUE )
	{		
		pTempZone->StatusIDs.nPWM_StatusID = StatusID = pTempZone->HeatingCtrl.PWM.StatusID;
		pTempZone->Step.eStepNr = eSTEP_TZ_ERR_STEP;
	}
	
	pTempZoneState->bOutputHeating = pTempZone->HeatingCtrl.PWM.Out;
	
	
	// Der Task gTempCtrl ist initialisiert
	if( gTempCtrl.State.bInitDone == TRUE )
	{
		// Die aktuelle Temperatur liegt 5�C �ber der beim Heizung ausschalten gemerkten Temperatur
		if( pTempZoneState->rActTemp >= pTempZone->rTemperatureHeatingSwitchedOff + 5.0 )
		{
			// Die Heizung ist nicht an
			if( pTempZoneState->bHeatingOn == FALSE )
			{
				// Fehler "Heizung kurzgeschlossen" setzen
				pTempZone->Errors.ErrHeatingShorted.bActive = TRUE;
				// Fehlerflag dieser Heizzone setzen
				pTempZoneState->bErrorTempZone = TRUE;	
			}
		}
		// Die Ist-Temperatur ist wieder gesunken
		else if( pTempZoneState->rActTemp <= pTempZone->Par.rSetTemp )
		{
			// Fehler "Heizung kurzgeschlossen" zur�cksetzen
			pTempZone->Errors.ErrHeatingShorted.bActive = FALSE;
		}
	}
	
	// Befindet sich die Ist-Temperatur innerhalb des Toleranzfensters und hat sie sich beruhigt
	pTempZone->TempInToleranceAndSettled.bTempInToleranceWindow = pTempZoneState->bTempInToleranceWindow;
	pTempZone->TempInToleranceAndSettled.bTempInWarningWindow = pTempZoneState->bTempInWarningWindow;
	pTempZone->TempInToleranceAndSettled.nSettlingTime = 5000;
	
	CheckIsTempInToleranceAndSettled( &pTempZone->TempInToleranceAndSettled );
	
	pTempZone->NegEdge.TempSettled.CLK = pTempZone->TempInToleranceAndSettled.bTempInToleranceAndSettled;
	F_TRIG( &pTempZone->NegEdge.TempSettled );
	
	// Die Temperatur liegt au�erhalb des Toleranzfensters
	if( pTempZone->NegEdge.TempSettled.Q == TRUE )
	{
		// Die Heizung ist aber weiterhin eingeschaltet
		if( pTempZoneState->bHeatingOn == TRUE )
		{
			// Die Soll-Temperatur dieser Heizzone wurde nicht ver�ndert bzw. das dazugeh�rige Heizsystem ist nicht im Standby
			if( (pTempZoneState->bTemperatureChanged == FALSE) &&
				(pTempZoneState->bStandbyOn == FALSE) )
			{
				// Fehler "Temperatur nicht in Toleranzfenster" setzen
				pTempZone->Errors.ErrActTempLeftToleranceWindow.bActive = TRUE;					
			}
		}
	}
	// Die Temperatur liegt innerhalb des Toleranzfensters
	else
	{
		// Fehler "Temperatur nicht in Toleranzfenster" zur�cksetzen
		pTempZone->Errors.ErrActTempLeftToleranceWindow.bActive = FALSE;
	}
	
	pTempZone->NegEdge.TempInTolerance.CLK = pTempZoneState->bTempInToleranceWindow;
	F_TRIG( &pTempZone->NegEdge.TempInTolerance );
	
	// Die Temperatur verl�sst das Toleranzfenster und es die Soll-Temperatur wurde nicht ver�ndert
	if( (pTempZone->NegEdge.TempInTolerance.Q == TRUE) &&
		(pTempZoneState->bTemperatureChanged == FALSE) )
	{
		// Die Heizung ist eingeschaltet UND NICHT im Standby
		if( (pTempZoneState->bHeatingOn == TRUE) &&
			(pTempZoneState->bStandbyOn == FALSE) )
		{
			// �bertemperatur erkannt
			if( pTempZoneState->rActTemp >= pTempZone->Par.rSetTemp )
			{
				// Fehler "�bertemperatur" setzen
				pTempZone->Errors.ErrOverTemperature.bActive = TRUE;
				
				// Fehlerflag setzen -> Heizsystem wird abgeschaltet 
				pTempZoneState->bErrorTempZone = TRUE;
				
				// Ist-Temperatur bei Abschaltung merken
				pTempZone->rTemperatureHeatingSwitchedOff = pTempZoneState->rActTemp;
			}
		}
	}
	// -> Fehlermeldung zur�cksetzen
	else
	{
		pTempZone->Errors.ErrOverTemperature.bActive = FALSE;
	}
	
	return StatusID;
}

// Diese Fuktion simuliert eine Temperatur Regel-Strecke und gibt die errechnete Ist-Temperatur zur�ck
REAL SimulateTempZone( REAL rInValue, TempCtrlTempZoneSimulation_TYP *pSimulation )
{
	pSimulation->DeadTime.Enable = TRUE;
	pSimulation->DeadTime.In = rInValue;
	
	MTBasicsTimeDelay( &pSimulation->DeadTime );
	
	pSimulation->PT2.Enable = TRUE;
	pSimulation->PT2.In = pSimulation->DeadTime.Out;
	
	MTBasicsPT2( &pSimulation->PT2 );
	
	return pSimulation->PT2.Out + 20.0;
}

BOOL PIDParametersChanged( MTBasicsPID_typ *pPID, TempCtrlTempZoneCfgPar_TYP *pNewPID_Parameter )
{
	
	if( (pPID != 0) &&
		(pNewPID_Parameter != 0) )
	{
		if( (pPID->PIDParameters.Gain == pNewPID_Parameter->rGain) &&
			(pPID->PIDParameters.DerivativeTime == pNewPID_Parameter->rDerivativeTime) &&
			(pPID->PIDParameters.IntegrationTime == pNewPID_Parameter->rIntegrationTime) &&
			(pPID->PIDParameters.FilterTime == pNewPID_Parameter->rFilterTime) &&
			(pPID->MinOut ==  pNewPID_Parameter->rMinOut) &&
			(pPID->MaxOut ==  pNewPID_Parameter->rMaxOut) )
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	else
	{
		return FALSE;
	}
}

void SetCmdStopHeatingOfSubSystems( USINT nTankIdx )
{
	USINT	nBonDevIdx;
	
	Tank[nTankIdx].Cmd.bStopHeating = TRUE;
	
	for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++ )
	{
		if( (gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].bExist == TRUE) )
		{
			if( gPar.MachinePar.TempCtrlConfig.BonDev[nBonDevIdx].nTankIdx == nTankIdx )
			{
				BonDev[nBonDevIdx].Cmd.bStopHeating = TRUE;
			}
		}
	}
}

UINT CheckIsTempInToleranceAndSettled( TempInToleranceAndSettled_TYP *p )
{
	enum
	{
		eSTEP_TEMP_NOT_IN_TOLERANCE_WINDOW = 0,
		eSTEP_WAITING_TEMP_SETTLED,
		eSTEP_TEMP_SETTLED,
	};
	
	p->Internal.TimeTempSettled.PT = p->nSettlingTime;
	TON( &p->Internal.TimeTempSettled );

	p->Internal.NegEdgeTempInWarningWindow.CLK = p->bTempInWarningWindow;
	F_TRIG( &p->Internal.NegEdgeTempInWarningWindow );
	
	
	// Die Temperatur ist im Toleranzfenster und hat soeben den Termperatur-Warnbereich verlassen
//	if( (p->bTempInToleranceWindow == TRUE) &&
//		(p->Internal.NegEdgeTempInWarningWindow.Q == TRUE) )
//	{
//		p->Internal.nStep = eSTEP_WAITING_TEMP_SETTLED;
//	}
	
	if( p->Internal.nStep != eSTEP_TEMP_SETTLED )
	{
		if( (p->bTempInToleranceWindow == TRUE) &&
			(p->bTempInWarningWindow == FALSE) )
		{
			p->Internal.nStep = eSTEP_WAITING_TEMP_SETTLED;
		}
	}
	
	// Die Temperatur liegt nicht innerhalb des Toleranzfensters
	if( p->bTempInToleranceWindow == FALSE )
	{
		p->Internal.nStep = eSTEP_TEMP_NOT_IN_TOLERANCE_WINDOW;
	}
	
	// Schrittkette
	switch( p->Internal.nStep )
	{
		// Die Temperatur befindet sich nicht im Toleranzfenster
		case eSTEP_TEMP_NOT_IN_TOLERANCE_WINDOW:
			
			// Zeitmessung zur�cksetzen
			p->Internal.TimeTempSettled.IN = FALSE;
			
			// Die Temperatur befindet sich au�erhalb des Toleranzbereichs oder ist noch nicht beruhigt
			p->bTempInToleranceAndSettled = FALSE;
			break;
		
		// Abwarten bis Beruhigungszeit abgelaufen ist
		case eSTEP_WAITING_TEMP_SETTLED:
		
			// Messung der Beruhigungszeit starten
			p->Internal.TimeTempSettled.IN = TRUE;
			
			// Die Temperatur ist in den Warnbereich gefallen/gestiegen
			if( p->bTempInWarningWindow == TRUE )
			{
				p->Internal.nStep = eSTEP_TEMP_NOT_IN_TOLERANCE_WINDOW;
			}
			
			// Die Temperatur lag �ber den Zeitraum "nSettlingTime" im Toleranzfenster und au�erhalb des Warnbereichs
			if( p->Internal.TimeTempSettled.Q == TRUE )
			{
				// -> Temperatur ist beruhigt
				p->Internal.nStep = eSTEP_TEMP_SETTLED;
			}
			break;
			
			
		// Temperatur ist im Toleranzbereich und die Beruhigungszeit lange innerhalb der Warnungsgrenzen 
		case eSTEP_TEMP_SETTLED:
			
			p->Internal.TimeTempSettled.IN = FALSE;
			
			// Temperatur ist beruhigt und im Toleranzbereich -> Ausgang der Funktion setzen
			p->bTempInToleranceAndSettled = TRUE;
			break;
	}
	
	return ERR_OK;
}






