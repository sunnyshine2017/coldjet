/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: TempCtrl
 * File: TempCtrlFunc.h
 * Author: Michael Zimmer
 * Created: 19.01.2015
 *******************************************************************/

#include <bur/plctypes.h>
#include <string.h>
#include <Global.h>

// Defines
// Fehler der Funktion CalcTempFromAT4222ErrCheck()
//#define	nERR_X20AT_BROKEN_WIRE_DETECTED		1		// Drahtbruch bzw. oberer Grenzwert überschritten
//#define	nERR_X20AT_MIN_VALUE_ERR			2		// Unterer Grenzwert überschritten
//#define	nERR_X20AT_INVALID_VALUE			3		// Ungültiger Wert


// Prototypen
//UINT CalcTempFromAT4222ErrCheck( INT nValueFromAT4222, REAL *pCalcedTemp );
UINT CalcTempFromAT4222ErrCheck( INT nValueFromAT4222, TempCtrlStateTempZone_TYP *pTempZoneState, TempCtrlTempZone_TYP *pTempZone );
BOOL CheckIsTempInWarningWindow( REAL rSetTemp, REAL rActTemp, REAL rToleranceMin, REAL rToleranceMax, REAL rPercentWarning );
BOOL CheckIsTempInToleranceWindow( REAL rSetTemp, REAL rActTemp, REAL rToleranceMin, REAL rToleranceMax );
DINT HeatingCtrlTempZone( TempCtrlTempZone_TYP *pTempZone, TempCtrlStateTempZone_TYP *pTempZoneState, TempCtrlTempZoneCfgPar_TYP *pTempZonePar );
REAL SimulateTempZone( REAL InValue, TempCtrlTempZoneSimulation_TYP *pSimulation );
BOOL PIDParametersChanged( MTBasicsPID_typ *pPID, TempCtrlTempZoneCfgPar_TYP *pNewPID_Parameter );
void SetCmdStopHeatingOfSubSystems( USINT nTankIdx );
UINT CheckIsTempInToleranceAndSettled( TempInToleranceAndSettled_TYP *p );

