(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: TempCtrl
 * File: G_TempCtrl.typ
 * Author: Michael Zimmer
 * Created: 19.01..2015
 ********************************************************************
 * Global  data types of package TempCtrl
 ********************************************************************)

TYPE
	TempCtrlCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		HeatingSystem : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF TempCtrlCallerBoxHeatingSys_TYP;
	END_STRUCT;
	TempCtrlDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		HeatingSystem : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF TempCtrlDirectBoxHeatingSys_TYP;
		bReset : BOOL; (*Fehler des Heizsystems zur�cksetzen.*)
	END_STRUCT;
	TempCtrlPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		nDummy : UINT;
	END_STRUCT;
	TempCtrlState_TYP : 	STRUCT  (*R�ckmeldungen*)
		HeatingSystem : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF TempCtrlStateHeatingSystem_TYP;
		Tank : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF TempCtrlStateTempZone_TYP;
		BonDev : ARRAY[0..nDEV_INDEX_MAX]OF TempCtrlStateBonDev_TYP;
		bInitDone : BOOL; (*Die Initialisierung des Tasks ist erfolgreich abgeschlossen.*)
		bError : BOOL; (*Ein Fehler ist in diesem Task aufgetreten.*)
		bAutomaticPossible : BOOL; (*Keines der konfigurierten Heizsysteme verhindert das Einschalten der Maschine in den Betriebszustand Automatik. Es werden der Tank und alle Klebek�pfe ber�cksichtigt, die f�r die Produktion (Check-Box auf Visu Hauptseite) eingeschaltet sind.*)
	END_STRUCT;
	TempCtrlDirectBoxHeatingSys_TYP : 	STRUCT 
		bStopHeating : BOOL; (*Die Heizung des gesamten Heizsystems abschalten. Die Heizungsregelung aller diesem Heizsystem zugeordneten K�pfe und des Tanks wird abgeschaltet.*)
	END_STRUCT;
	TempCtrlCallerBoxHeatingSys_TYP : 	STRUCT 
		Caller : BrbCaller_TYP;
		bStartHeating : BOOL; (*Die Heizung des gesamten Heizsystems einschalten.*)
		bStartSystemStandby : BOOL; (*Heizsystem im Standby betreiben. Die Temperatur des gesamten Heizsystems wird auf die konfigurierten Standby-Temperaturen abgesenkt.*)
		bStopSystemStandby : BOOL; (*Standby des Heizsystems aufheben. Es wird wieder auf die Soll-Temperatur geregelt.*)
	END_STRUCT;
	TempCtrlStateBonDev_TYP : 	STRUCT 
		TempZone : ARRAY[0..nIDX_TEMP_CTRL_ZONE_MAX]OF TempCtrlStateTempZone_TYP; (*Struktur mit den Stati der verschiedenen Temperatur-/Heizzonen des Kopfes. Idx 0: Schlauch; 1: Kopf; 2: Reserve.*)
		bHeatingOn : BOOL; (*Die Heizungsregelung dieses Kopfes ist aktiv.*)
		bAutomaticPossible : BOOL; (*Die Heizzonen dieses Kopfes sind im Toleranzfeld und dieser Kopf kann zur Produktion freigegeben werden.*)
		bTempInToleranceWindow : BOOL; (*Die Ist-Temperaturen aller konfigurierten Heizzonen dieses Klebekopfes liegen in dem parametriertem Toleranzfenster.*)
		bTempInWarningWindow : BOOL; (*Die Ist-Temperatur mindestens einer konfigurierten Heizzone dieses Klebekopfes n�hert sich der Toleranzgrenze (�ber- bzw. Untertemperatur).*)
		bErrorBonDev : BOOL; (*Mindestens in einer der Temperatur-/Heizzonen dieses Klebekopfes ist ein Fehler aufgetreten.*)
	END_STRUCT;
	TempCtrlStateTempZone_TYP : 	STRUCT 
		rActTemp : REAL; (*Aktuell gemessene Temperatur in �C dieser Heizzone*)
		bHeatingOn : BOOL; (*Die Heizungsregelung dieser Heizzone ist aktiv*)
		bTempInToleranceWindow : BOOL; (*Die aktuelle Temperatur dieser Heizzone liegt in dem parametriertem Toleranzfenster.*)
		bTempInWarningWindow : BOOL; (*Die aktuelle Temperatur dieser Heizzone n�hert sich der Toleranzgrenze (�ber- bzw. Untertemperatur).*)
		bErrorTempZone : BOOL; (*Ein Fehler ist in dieser Heizzone aufgetrten.*)
		bOutputHeating : BOOL; (*PWM-Ansteuerung des Heizungsrelais ( TRUE: es wird geheizt; FALSE: es wird nicht geheizt ).*)
		bTemperatureChanged : BOOL; (*Die Soll-Temperatur f�r diese Heizzone wurde in der Visualisierung ver�ndert.*)
		bStandbyOn : BOOL; (*Diese Heizzone befindet sich im Standby.*)
	END_STRUCT;
	TempCtrlStateHeatingSystem_TYP : 	STRUCT 
		bHeatingOn : BOOL; (*Das Heizsystem ist eingeschaltet. D.h. die Temperaturregelung der dazugeh�rigen Heizzonen (Klebek�pfe und der Tank) ist aktiv.*)
		bStandbyOn : BOOL; (*Das Heizsystem ist im Standby.*)
		bTempInToleranceWindow : BOOL; (*Die Temperaturen der eingeschalten Heizzonen dieses Heizsystem befinden sich im Toleranzfenster.*)
		bTempInWarningWindow : BOOL; (*Mindestens eine der Temperaturen des Heizsystems n�hert sich der Toleranzgrenze.*)
		bErrorHeatingSystem : BOOL; (*Ein Fehler liegt an diesem Heizsystem an.*)
		bHeatingMainContactorClosed : BOOL; (*Das Heizungs-Lastsch�tz ist geschlossen. Der Ausgang zur Ansteuerung des Heizungs-Lastsch�tzes wird gesetzt.*)
		bAutomaticPossible : BOOL; (*Dieses Heizsystem verhindert nicht das Einschalten der Maschine in den Betriebszustand Automatik. Es werden der Tank und alle Klebek�pfe ber�cksichtigt, die f�r die Produktion (Check-Box auf Visu Hauptseite) eingeschaltet sind.*)
		bOneOrMoreBonDevProdutionEnabled : BOOL; (*Mindestens ein Klebekopf dieses Heizsystems ist f�r die Produktion aktiviert (Check-Box auf Visu Hauptseite).*)
	END_STRUCT;
	TempCtrl_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : TempCtrlCallerBox_TYP;
		DirectBox : TempCtrlDirectBox_TYP;
		Par : TempCtrlPar_TYP;
		State : TempCtrlState_TYP;
	END_STRUCT;
END_TYPE
