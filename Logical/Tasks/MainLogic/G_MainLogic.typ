(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: MainLogic
 * File: G_MainLogic.typ
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Global  data types of package MainLogic
 ********************************************************************)

TYPE
	Cleaning_Type_ENUM : 
		(
		eCLEANING_TYPE_STANDARD,
		eCLEANING_TYPE_INTERVAL
		);
	MainLogicCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bAutoOn : BOOL; (*Kommando Automatikmodus Ein*)
		bCleaningOn : BOOL; (*Kommando Reinigung Ein*)
		bSystemCleaningStart : BOOL; (*Kommando Reinigung des kompletten Systems starten (Sp�lung der Schl�uche)*)
	END_STRUCT;
	MainLogicDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bStop : BOOL; (*Kommando Stop*)
		bCleaningStop : BOOL; (*Kommando Reinigung Stop*)
		bSystemCleaningCancel : BOOL; (*Kommando Reinigung des kompletten Systems abbrechen (Sp�lung der Schl�uche)*)
		bAutoCleaningOn : BOOL; (*Kommando zur Reinigung �ber den Start-Button aus der Visualisierung auf der Seite Reinigung*)
		bCmdOpenBonDev : ARRAY[0..nDEV_INDEX_MAX]OF BOOL;
		bContCleanBonDevWithGlue : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Kommando zur kontinuierlichen Reinigung mit Klebstoff.*)
		bIntervalCleanBonDevWithGlue : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Kommando zur Intervall-Reinigung ("Freischie�en") mit Klebstoff. Wird nur solange durchgef�hrt, wie das Kommando gesetzt ist. Hierbei soll der Druck �ber die Druckregelung gesteuert werden. Daher muss das Klebesystem im Betriebsmodus "Automatik" sein.*)
		bContCleanBonDevWithDetergent : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Kommando zur kontinuierlichen Reinigung mit Reinigungsmittel.*)
	END_STRUCT;
	MainLogicPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		bBonDevCleaningEn : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Enable Reinigung*)
		eCleaningType : Cleaning_Type_ENUM; (*Reinigungsart*)
		bDetergentActive : BOOL; (*Reinigungsmittel aktiv*)
	END_STRUCT;
	MainLogicState_TYP : 	STRUCT  (*R�ckmeldungen*)
		CleaningBonDev : OpenBonDev_TYP;
		bAutomatic : BOOL; (*Status Automatikmodus*)
		bCleaningActive : BOOL; (*Status Reinigung*)
		bAutomaticPossible : BOOL; (*Maschine Bereit*)
		bSealingSystem : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Verschlusssystem*)
		bCleaningContinuous : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Die Reinigung des jeweiligen Klebekopfes mit Klebstoff wurde �ber den dazugeh�rigen Hardware-Taster angefordert.*)
		bCleaningWithDetergentActive : BOOL; (*Die Reinigung des Klebekopfes mit Reinigungsmittel wurde �ber einen Hardware-Taster bzw. Kommandos angefordert.*)
		bCleaningWithGlueActive : BOOL; (*Die Reinigung des Klebekopfes mit Klebstoff wurde �ber einen Hardware-Taster bzw. Kommandos angefordert.*)
		bMainValveOpen : BOOL; (*Das Hauptventil (Y1) ist ge�ffnet.*)
		bGlueValveOpen : BOOL; (*Das Ventil zum durchfluten der Klebek�pfe mit Klebstoff ist ge�ffnet (Y2).*)
		bDetergentValveOpen : BOOL; (*Das Ventil zum durchfluten der Klebek�pfe mit Reinigungsmittel ist ge�ffnet (Y3).*)
		nSystemCleaningProgress : UINT; (*Fortschritt der Systemreinigung in [1/100 %].*)
		nSystemCleaningRestPeriod : UINT; (*Verbleibende Zeit in [s] bis die Systemreinigung abgeschlossen ist.*)
		bSystemCleaningActive : BOOL; (*Die Reinigung des kompletten Systems ist aktiv (Sp�lung der Schl�uche)*)
		bAutoCleaningOn : BOOL; (*Die automatische Reinigung ist aktiv. In der Visualisierung wurde diese �ber den Start-Button auf der Seite Reinigung angefordert*)
		bCleaningWithPressureCtrlActive : BOOL; (*F�r die derzeit aktive Reinigung soll der Druck in den Leitungen �ber die Druckregelung geregelt werden.*)
	END_STRUCT;
	MainLogic_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : MainLogicCallerBox_TYP;
		DirectBox : MainLogicDirectBox_TYP;
		Par : MainLogicPar_TYP;
		State : MainLogicState_TYP;
	END_STRUCT;
	OpenBonDev_TYP : 	STRUCT 
		bOpenBonDev : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Auftragekopf �ffnen*)
		bCleanInt : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Intervall Reinigung aktivieren*)
		bCleanStandard : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Standard Reinigung aktivieren*)
		bOpenBonDevDirect : ARRAY[0..nDEV_INDEX_MAX]OF BOOL;
		bAutoCleaningOn : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*F�r diesen Kopf wurde die automatische Reinigung angefordert und ist derzeit aktiv.*)
		bStartInitCleaning : BOOL; (*Initialisierung f�r Reinigung ausf�hren*)
	END_STRUCT;
END_TYPE
