/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: MainLogic
 * File: MainLogic.c
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Implementation of program MainLogic
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <MainLogicFunc.h>

void _INIT MainLogicINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gMainLogic, 0, sizeof(gMainLogic));
	gMainLogic.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	bStopCleaning = 1;
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
	
	SystemCleaningStep.eStepNr = eSTEP_WAIT_START_SYSTEM_CLEANING;
	SystemCleaningStep.bInitDone = TRUE;
}

void _CYCLIC MainLogicCYCLIC(void)
{	
		
	// ---------------------------- Direkt-Kommandos des Task intern setzen ----------------------------
	for( nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++ )
	{
		/* Negative Flange des Hardware-Tasters "Kontinuierliche Reinigung mit Klebstoff" ermitteln */
		NegEdgeCleaningContinuous[nDevIdx].CLK = gMainLogic.State.bCleaningContinuous[nDevIdx];
		F_TRIG( &NegEdgeCleaningContinuous[nDevIdx] );
		// Kontinuierliche Reinigung mit Klebstoff �ber den Hardware-Taster wurde angefordert
		if( gMainLogic.State.bCleaningContinuous[nDevIdx] == TRUE )
		{
			// Direkt-Kommando kontinuierlich mit Klebstoff sp�len setzen
			gMainLogic.DirectBox.bContCleanBonDevWithGlue[nDevIdx] = TRUE;
		}
		// Kontinuierliche Reinigung mit Klebstoff wird �ber den Hardware-Taster nicht mehr angefordert
		if( NegEdgeCleaningContinuous[nDevIdx].Q == TRUE )
		{
			// Direkt-Kommando kontinuierlich mit Klebstoff sp�len zur�cksetzen
			gMainLogic.DirectBox.bContCleanBonDevWithGlue[nDevIdx] = FALSE;
		}
		
		/* Negative Flange des Hardware-Tasters "Auftragekopf �ffnen" ermitteln */
		NegEdgeOpenBonDev[nDevIdx].CLK = IO.Input.bOpenBonDev[nDevIdx];
		F_TRIG( &NegEdgeOpenBonDev[nDevIdx] );
		// �ffnen des Auftragekopfes wurde �ber den Hardware-Taster angefordert
		if( IO.Input.bOpenBonDev[nDevIdx] == TRUE )
		{
			// Direkt-Kommando "Auftragekopf �ffnen" setzen
			gMainLogic.DirectBox.bCmdOpenBonDev[nDevIdx] = TRUE;
		}
		// �ffnen des Auftragekopfes wurde �ber den Hardware-Taster nicht mehr angefordert
		if( NegEdgeOpenBonDev[nDevIdx].Q == TRUE )
		{
			// Direkt-Kommando "Auftragekopf �ffnen" zur�cksetzen
			gMainLogic.DirectBox.bCmdOpenBonDev[nDevIdx] = FALSE;
		}
	}
	
	// --------------------------- Direkt-Kommandos zur Reinigung auf Durchf�hrbarkeit pr�fen ---------------------------
	// Da alle Klebek�pfe entweder mit Reinigungsmittel oder Klebstoff durchflutet werden k�nnen,
	// m�ssen die Kommandos auf Durchf�hrbarkeit gepr�ft werden.
	
	// Schleife �ber alle Klebek�pfe
	for (nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++)
	{
		// Ist die Systemreinigung aktiv, d�rfen die K�pfe nicht gesp�lt bzw. ge�ffnet werden
		if( gMainLogic.State.bSystemCleaningActive == TRUE )
		{
			// Diese Direkt-Kommandos m�ssen w�hrend der Systemreinigung ignoriert werden -> auf FALSE setzen!
			gMainLogic.DirectBox.bCmdOpenBonDev[nDevIdx] = FALSE;
			gMainLogic.DirectBox.bContCleanBonDevWithDetergent[nDevIdx] = FALSE;
			gMainLogic.DirectBox.bContCleanBonDevWithGlue[nDevIdx] = FALSE;
			gMainLogic.DirectBox.bIntervalCleanBonDevWithGlue[nDevIdx] = FALSE;
			gMainLogic.DirectBox.bCleaningStop = FALSE;
		}
		else
		{
			// Das Ventil f�r den Klebstoff ist ge�ffnet -> durch die Klebek�pfe kann KEIN Reinigungsmittel flie�en
			if( gMainLogic.State.bCleaningWithGlueActive == TRUE )
			{
				// Alle Reinigungsauftr�ge mit Reinigungsmttel werden zur�ckgesetzt und somit verworfen
				gMainLogic.DirectBox.bContCleanBonDevWithDetergent[nDevIdx] = FALSE;
			}
			
			// Das Ventil f�r das Reinigungsmittel ist ge�ffnet -> durch die Klebek�pfe kann KEIN Klebstoff flie�en
			if( (gMainLogic.State.bDetergentValveOpen == TRUE) ||
				// ODER eine Reinigung mit Reinigungsmittel wurde angefordert
				(gMainLogic.State.bCleaningWithDetergentActive == TRUE) )
			{
				// Alle Reinigungsauftr�ge mit Klebst�ff werden zur�ckgesetzt und somit verworfen
				gMainLogic.DirectBox.bContCleanBonDevWithGlue[nDevIdx] = FALSE;
				gMainLogic.DirectBox.bIntervalCleanBonDevWithGlue[nDevIdx] = FALSE;
			}
			
			// Ist das Klebesystem nicht in Produktion (Automatik aktiv) ist auch die Druckregelung nicht aktiv
			// -> daher k�nnen die folgenden Kommandos nicht ausgef�hrt werden
//			if( gMainLogic.State.bAutomatic == FALSE )
//			{
//				// Das Freischie�en des Klebekopfes muss mit dem geregelten Druck geschehsen. Ist die Automatik nicht aktiv, ist dies nicht m�glich.
//				// -> Komando "Freischie�en" zur�cksetzen
//				gMainLogic.DirectBox.bIntervalCleanBonDevWithGlue[nDevIdx] = FALSE;
//			}
		}
		
		// Bei Klebek�pfen mit Heizung muss gepr�ft werden ob die Reinigung durchgef�hrt werden darf
		if( (gPar.MachinePar.MachineModule.bTempCtrlEn == TRUE) &&
			(gPar.MachinePar.TempCtrlConfig.BonDev[nDevIdx].bExist == TRUE) )
		{
			// Die Temperatur des Klebekopfes liegt nicht innerhalb Temperaturfensters in dem gereinigt werden darf
			if( gTempCtrl.State.BonDev[nDevIdx].bAutomaticPossible == FALSE )
			{
				// 
				if( (gMainLogic.DirectBox.bCmdOpenBonDev[nDevIdx] == TRUE) ||
					(gMainLogic.State.bCleaningContinuous[nDevIdx] == TRUE) )
				{
					// Kommandos zur�cksetzen und nicht ausf�hren
					gMainLogic.DirectBox.bCmdOpenBonDev[nDevIdx] = FALSE;
					gMainLogic.State.bCleaningContinuous[nDevIdx] = FALSE;
					// Warnung ausgeben
					AppSetEvent( eEVT_WAR_CLEAN_BONDEV_NOT_POSSIB, 0, "", nDevIdx + 1, &gEventManagement );
					AppResetEvent( eEVT_WAR_CLEAN_BONDEV_NOT_POSSIB, &gEventManagement);
				}
			}
		}
		
		// --------------------------------------------------------------
		// Abarbeitung der automatischen Reinigung aus der Visualisierung
		// --------------------------------------------------------------	
		// Die Reinigung wurde soeben beendet
		NegEdgeStateCleaningActive.CLK = gMainLogic.State.bCleaningActive;
		F_TRIG( &NegEdgeStateCleaningActive );
		// Negative Flanke der Reinigung
		if( NegEdgeStateCleaningActive.Q == TRUE )
		{
			// Die automatische Reinigung wird abgeschaltet
			gMainLogic.State.bAutoCleaningOn = FALSE;
		}
		
		// Das Kommando zur automatischen Reinigung wurde von einem Task gesetzt
		if( gMainLogic.DirectBox.bAutoCleaningOn == TRUE )
		{
			// Soll dieser Kopf auch gereinigt werden? (Haken in der Reinigungsseite f�r diesen Kopf gesetzt)
			if( gMainLogic.Par.bBonDevCleaningEn[nDevIdx] == TRUE )
			{	
				// Dieser Kopf besitzt eine Heizung
				if( (gPar.MachinePar.MachineModule.bTempCtrlEn == TRUE) &&
					(gPar.MachinePar.TempCtrlConfig.BonDev[nDevIdx].bExist == TRUE) )
				{
					// Der Kopf hat die ben�tigte Temperatur erreicht um zu produzieren und um gereinigt zu werden
					if( gTempCtrl.State.BonDev[nDevIdx].bAutomaticPossible == TRUE )
					{
						// State setzen, dass die automatische Reinigung aktiv ist			
						gMainLogic.State.bAutoCleaningOn = TRUE;
						// State setzen, dass dieser Kopf zusammen mit den anderen ausgew�hlten K�pfen automatisch gereinigt wird
						gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] = TRUE;
					}
					else
					{
						// State zur�cksetzen, dass dieser Klebekopf zusammen mit den anderen ausgew�hlten K�pfen automatisch gereinigt wird
						gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] = FALSE;
						// Haken der Checkbox auf der Seite Reinigung in Visu zur�cksetzen
						gMainLogic.Par.bBonDevCleaningEn[nDevIdx] = FALSE;
						// Warnung ausgeben
						AppSetEvent( eEVT_WAR_CLEAN_BONDEV_NOT_POSSIB, 0, "", nDevIdx + 1, &gEventManagement );
						AppResetEvent( eEVT_WAR_CLEAN_BONDEV_NOT_POSSIB, &gEventManagement);
					}
				}
				else
				{
					// State setzen, dass die automatische Reinigung aktiv ist			
					gMainLogic.State.bAutoCleaningOn = TRUE;
					// State setzen, dass dieser Kopf zusammen mit den anderen ausgew�hlten K�pfen automatisch gereinigt wird
					gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] = TRUE;
				}
			}
			
			// Die Schleife ist bis zum Index der konfigurierten K�pfe durchgelaufen
			if( nDevIdx == gPar.MachinePar.nBonDevIndex )
			{
				// Direkt-Kommando zur�cksetzen
				gMainLogic.DirectBox.bAutoCleaningOn = FALSE;
			}
		}
		
		// Die automatische Reinigung ist zur Zeit aktiv
		if( gMainLogic.State.bAutoCleaningOn == TRUE )
		{
			// Werden K�pfe die zur Zeit durch die automatische Reinigung gereinigt werden abgew�hlt, oder ein anderer angew�hlt
			if( gMainLogic.Par.bBonDevCleaningEn[nDevIdx] != gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] )
			{
				// Dieser Kopf besitzt eine Heizung
				if( (gPar.MachinePar.MachineModule.bTempCtrlEn == TRUE) &&
					(gPar.MachinePar.TempCtrlConfig.BonDev[nDevIdx].bExist == TRUE) )
				{
					// Der Kopf hat die ben�tigte Temperatur erreicht um zu produzieren und um gereinigt zu werden
					if( gTempCtrl.State.BonDev[nDevIdx].bAutomaticPossible == TRUE )
					{
						// so wird f�r diesen Kopf der State zur automatischen Reinigung abgew�hlt oder angew�hlt
						gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] = gMainLogic.Par.bBonDevCleaningEn[nDevIdx];
					}
					// Dieser Klebekopf darf wegen falscher Temperatur nicht gereinigt werden
					else
					{
						// Dieser Klebekopf darf nicht mehr gereinigt werden
						gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] = FALSE;
						// Haken der Checkbox auf der Seite Reinigung in Visu zur�cksetzen
						gMainLogic.Par.bBonDevCleaningEn[nDevIdx] = FALSE;
						// Warnung ausgeben
						AppSetEvent( eEVT_WAR_CLEAN_BONDEV_NOT_POSSIB, 0, "", nDevIdx + 1, &gEventManagement );
						AppResetEvent( eEVT_WAR_CLEAN_BONDEV_NOT_POSSIB, &gEventManagement);					
					}
				}
				else
				{
					// so wird f�r diesen Kopf der State zur automatischen Reinigung abgew�hlt oder angew�hlt
					gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] = gMainLogic.Par.bBonDevCleaningEn[nDevIdx];
				}
			}
			
			// Klebek�pfe mit Heizung w�hrend der automatischen Reinigung �berwachen
			if( (gPar.MachinePar.MachineModule.bTempCtrlEn == TRUE) &&
				(gPar.MachinePar.TempCtrlConfig.BonDev[nDevIdx].bExist == TRUE) )
			{
				// W�hrend der automatischen Reinigung verl�sst die Ist-Temperatur das zur Reinigung g�ltige Temperaturfenster
				if( gTempCtrl.State.BonDev[nDevIdx].bAutomaticPossible == FALSE )
				{
					if( gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] == TRUE )
					{
						// Dieser Klebekopf darf nicht mehr gereinigt werden
						gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] = FALSE;
						// Haken der Checkbox auf der Seite Reinigung in Visu zur�cksetzen
						gMainLogic.Par.bBonDevCleaningEn[nDevIdx] = FALSE;
						// Warnung ausgeben
						AppSetEvent( eEVT_WAR_CLEAN_BONDEV_NOT_POSSIB, 0, "", nDevIdx + 1, &gEventManagement );
						AppResetEvent( eEVT_WAR_CLEAN_BONDEV_NOT_POSSIB, &gEventManagement);
					}
				}
			}
		}
		// Die automatische Reinigung ist nicht aktiv
		else
		{
			// die States der K�pfe zur automatischen Reinigung werden zur�ckgesetzt damit diese nicht mehr die Reinigung durchf�hren
			gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] = FALSE;
		}
	}
	
	// Hilfsflag vor Schleife setzen um in der Schleife bestimmen zu k�nnen ob die Reinigung beendet werden muss
	bStopCleaning = 1;
	// Schleife �ber alle konfigurierten Klebek�pfe
	for (nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++)
	{	
		// -------------------------------- Pr�fen ob Auftragekopf gereinigt werden muss --------------------------------
		// Das Kommando zum �ffnen des Klebekopfventils wurde gesetzt (bet�tigen des Klebekopfes auf der Seite Reinigung)
		if( (gMainLogic.DirectBox.bCmdOpenBonDev[nDevIdx] == 1) ||			
			// Die automatische Reinigung im Modus "kontinuierlich" wird f�r diesen Klebekopf angefordert
			((gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] == TRUE) && (gMainLogic.Par.eCleaningType == eCLEANING_TYPE_STANDARD)) ||
			// Die automatische Reinigung im Modus "intervall" wird f�r diesen Klebekopf angefordert und die Anzahl der Intervallzyklen wurde noch nicht erreicht
			((gMainLogic.State.CleaningBonDev.bAutoCleaningOn[nDevIdx] == TRUE) && (gMainLogic.Par.eCleaningType == eCLEANING_TYPE_INTERVAL) && (gBondingDev.State.bIntCleanDone[nDevIdx] == 0) ) ||
			// Kommando zum Intervall-Reinigen mit Klebstoff liegt an
			(gMainLogic.DirectBox.bIntervalCleanBonDevWithGlue[nDevIdx] == TRUE) ||
			// Kommando zur kontinuierlichen Reinigung mit Klebstoff liegt an
			(gMainLogic.DirectBox.bContCleanBonDevWithGlue[nDevIdx] == TRUE) ||
			// Kommando zur kontinuierlichen Reinigung mit Reinigungsmittel liegt an
			(gMainLogic.DirectBox.bContCleanBonDevWithDetergent[nDevIdx] == TRUE) )
		{
			// State zum �ffnen des Klebekopfes setzen -> ist bei einem der Klebek�pfe dieser State gesetzt, wird sp�ter das Kommando zum Reinigen in diesem Task gesetzt
			gMainLogic.State.CleaningBonDev.bOpenBonDev[nDevIdx] = 1;
		}
		// Es ist nicht n�tig diesen Klebekopf f�r die Reinigung zu �ffnen
		else
		{
			// Klebekopf bleibt oder wird geschlossen
			gMainLogic.State.CleaningBonDev.bOpenBonDev[nDevIdx] = 0;
		}
			
		if( (gMainLogic.DirectBox.bCmdOpenBonDev[nDevIdx] == TRUE) ||
			// Kommando zum Intervall-Reinigen mit Klebstoff liegt an
			(gMainLogic.DirectBox.bIntervalCleanBonDevWithGlue[nDevIdx] == TRUE) ||
			// Kommando zur kontinuierlichen Reinigung mit Klebstoff liegt an
			(gMainLogic.DirectBox.bContCleanBonDevWithGlue[nDevIdx] == TRUE) ||
			// Kommando zur kontinuierlichen Reinigung mit Reinigungsmittel liegt an
			(gMainLogic.DirectBox.bContCleanBonDevWithDetergent[nDevIdx] == TRUE ) )
		{
			gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIdx] = 1;
		}
		else
		{
			gMainLogic.State.CleaningBonDev.bOpenBonDevDirect[nDevIdx] = 0;
		}
				
		// Reinigungsmodus feststellen
		
		// Standardreinigung
		if( ((gMainLogic.DirectBox.bCmdOpenBonDev[nDevIdx] == TRUE) &&
			 (gMainLogic.Par.eCleaningType == eCLEANING_TYPE_STANDARD)) ||
			// Kommando zur kontinuierlichen Reinigung mit Klebstoff liegt an
			(gMainLogic.DirectBox.bContCleanBonDevWithGlue[nDevIdx] == TRUE) ||
			// Kommando zur kontinuierlichen Reinigung mit Reinigungsmittel liegt an
			(gMainLogic.DirectBox.bContCleanBonDevWithDetergent[nDevIdx] == TRUE) )
		{
			gMainLogic.State.CleaningBonDev.bCleanStandard[nDevIdx] = 1;
		}
		else
		{
			gMainLogic.State.CleaningBonDev.bCleanStandard[nDevIdx] = 0;
		}
		
		// Intervallreinigung
		if( ((gMainLogic.DirectBox.bCmdOpenBonDev[nDevIdx] == TRUE) &&
			 (gMainLogic.Par.eCleaningType == eCLEANING_TYPE_INTERVAL)) ||
			// Kommando zum Intervall-Reinigen mit Klebstoff liegt an
			(gMainLogic.DirectBox.bIntervalCleanBonDevWithGlue[nDevIdx] == TRUE) )
		
		{
			gMainLogic.State.CleaningBonDev.bCleanInt[nDevIdx] = 1;
		}
		else
		{
			gMainLogic.State.CleaningBonDev.bCleanInt[nDevIdx] = 0;
		}
		
		// Auftragekopf �ffnen
		if( (gMainLogic.State.CleaningBonDev.bOpenBonDev[nDevIdx] == TRUE) &&
			(gMainLogic.State.bCleaningActive == FALSE) )
		{
			if (BrbSetCaller(&gMainLogic.CallerBox.Caller, eCALLERID_MAIN_LOGIC) == eBRB_CALLER_STATE_OK)
			{
				gMainLogic.CallerBox.bCleaningOn = 1;									// Kommando absetzen
			}		
		}
		
		// Pr�fen ob Reinigung gestoppt werden muss -> ist f�r keine der konfigurierten K�pfe bOpenBonDev gesetzt, wird die Reinigung gestoppt
		if( (gMainLogic.State.CleaningBonDev.bOpenBonDev[nDevIdx] == TRUE) )
		{
			bStopCleaning = 0;
		}
	}

	
	fbEdgeStopCleaning.CLK = bStopCleaning;
	R_TRIG(&fbEdgeStopCleaning);
	if ((fbEdgeStopCleaning.Q == 1) &&
		(Step.bInitDone == 1))
	{
		gMainLogic.DirectBox.bCleaningStop = 1;
	}
	
	// Pr�fen ob Initialisierung f�r Reinigung notwendig ist
//	CleaningType.nCmpResult = brsmemcmp((UDINT)&gMainLogic.State.CleaningBonDev.bOpenBonDev, (UDINT)&CleaningType.bCleaningCmpVar, sizeof(nDEV_INDEX_MAX + 1));
	CleaningType.nCmpResult = brsmemcmp( (UDINT)&gMainLogic.State.CleaningBonDev.bOpenBonDev, (UDINT)&CleaningType.bCleaningCmpVar, sizeof( gMainLogic.State.CleaningBonDev.bOpenBonDev) );
	if (CleaningType.nCmpResult != 0)
	{
		// Reinigung aktiv
		if (bStopCleaning == 0)
		{
			gMainLogic.State.CleaningBonDev.bStartInitCleaning = 1;
			CleaningType.nCyclicCnt += 1;
			// Nach 10 Zyklen abl�schen
			if (CleaningType.nCyclicCnt > 10)
			{
				gMainLogic.State.CleaningBonDev.bStartInitCleaning = 0;
				CleaningType.nCyclicCnt = 0;
//				brsmemcpy((UDINT)&CleaningType.bCleaningCmpVar, (UDINT)&gMainLogic.State.CleaningBonDev.bOpenBonDev, sizeof(nDEV_INDEX_MAX + 1));
				brsmemcpy( (UDINT)&CleaningType.bCleaningCmpVar, (UDINT)&gMainLogic.State.CleaningBonDev.bOpenBonDev, sizeof( CleaningType.bCleaningCmpVar ) );
			}
		}
		// Reinigung nicht aktiv
		else
		{
			gMainLogic.State.CleaningBonDev.bStartInitCleaning = 0;
			CleaningType.nCyclicCnt = 0;
//			brsmemcpy((UDINT)&CleaningType.bCleaningCmpVar, (UDINT)&gMainLogic.State.CleaningBonDev.bOpenBonDev, sizeof(nDEV_INDEX_MAX + 1));			
			brsmemcpy((UDINT)&CleaningType.bCleaningCmpVar, (UDINT)&gMainLogic.State.CleaningBonDev.bOpenBonDev, sizeof( CleaningType.bCleaningCmpVar ));			
		}
	}

	// ---------- Automatikbetrieb ausschalten ----------
	if (gMainLogic.DirectBox.bStop == 1)
	{
		Step.eStepNr = eSTEP_STOP;
	}
	
	
	// ---------- Reinigung ausschalten ----------
	if( Step.bInitDone == TRUE )
	{
		if( gMainLogic.DirectBox.bCleaningStop == TRUE )
		{
			Step.eStepNr = eSTEP_STOP_CLEANING;
		}
	}
	// ---------- Reinigung abbrechen ----------
	if( gMainLogic.DirectBox.bSystemCleaningCancel == TRUE)
	{
		// n�chster Schritt: Die Systemreinigung wurde abgebrochen
		SystemCleaningStep.eStepNr =  eSTEP_CANCEL_SYSTEM_CLEANING;
	}
		
	// ---------- StepHandling Hauptschrittkette ----------
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// ---------- StepHandling Schrittkette Systemreinigung ----------
	if(SystemCleaningStepHandling.Current.bTimeoutElapsed == 1)
	{
		SystemCleaningStepHandling.Current.bTimeoutElapsed = 0;
		SystemCleaningStep.eStepNr = SystemCleaningStepHandling.Current.nTimeoutContinueStep;
	}
	SystemCleaningStepHandling.Current.nStepNr = (DINT)SystemCleaningStep.eStepNr;
	strcpy(SystemCleaningStepHandling.Current.sStepText, SystemCleaningStep.sStepText);
	BrbStepHandler(&SystemCleaningStepHandling);
	
	// ***************************************************************************************************	
	// 										Hauptschrittkette
	// ***************************************************************************************************	
	switch(Step.eStepNr)
	{
		//=======================================================================================================================
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			
			if(gParHandling.State.bInitDone == 1)
			{
				// Pr�fen ob g�ltige Werte f�r die Systemreinigung eingetragen sind
				if( gPar.MachinePar.SystemCleaning.nDetergentTankCleaningPeriod == 0 )
				{
					// Null ist kein g�ltiger Wert -> Default-Wert setzen
					gPar.MachinePar.SystemCleaning.nDetergentTankCleaningPeriod = 60;
				}
				if( gPar.MachinePar.SystemCleaning.nGlueTankCleaningPeriod == 0 )
				{
					// Null ist kein g�ltiger Wert -> Default-Wert setzen
					gPar.MachinePar.SystemCleaning.nGlueTankCleaningPeriod = 60;
				}
				
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gMainLogic.CallerBox, sizeof(gMainLogic.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//=======================================================================================================================
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			
			// ---------- Automatikbetrieb einschalten ----------
			if (gMainLogic.CallerBox.bAutoOn == 1)
			{
				Step.eStepNr = eSTEP_AUTOMATIC_ON;				// In den Schritt "Automatikbetrieb aktiv" springen
			}
			
			// ---------- Reinigung einschalten ----------
			if (gMainLogic.CallerBox.bCleaningOn == 1)
			{
				Step.eStepNr = eSTEP_CLEANING;					// In den Schritt "Reinigung einschalten" springen
			}
			// ---------- System-Reinigung starten ----------
			if( gMainLogic.CallerBox.bSystemCleaningStart == TRUE )
			{
				// Automatik ist nicht aktiv
				if( gMainLogic.State.bAutomatic == FALSE )
				{					
					// n�chster Schritt: Systemreinigung starten
					SystemCleaningStep.eStepNr = eSTEP_START_SYSTEM_CLEANING;
				}
				// Systemreinigung nicht m�glich da Automatik aktiv
				else
				{
					// State "Systemreinigung aktiv" zur�cksetzen
					gMainLogic.State.bSystemCleaningActive = FALSE;
					// Kommando aus Caller-Box l�schen
					BrbClearCallerBox((UDINT)&gMainLogic.CallerBox, sizeof(gMainLogic.CallerBox));
				}
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		// Automatikbetrieb einschalten
		case eSTEP_AUTOMATIC_ON:
			strcpy(Step.sStepText, "eSTEP_AUTOMATIC_ON");
			
			IO.Output.bValveY1			= 1;						// Hauptventil Y1 �ffnen
			
			// Die Reinigung ist w�hrend des Startens der Automatik aktiv
			if (gMainLogic.State.bCleaningActive == 1)
			{
				// Automatische Ventilsteuerung aktiv
				if ((gPar.MachinePar.TankType.bTankCleanEn == 1) &&
					(gPar.MachinePar.TankType.bAutoValveCtrl == 1))
				{
					// Reinigungsmittel aktiv und keine Anforderung zum Reinigen mit Leim
					if( ((gMainLogic.Par.bDetergentActive == TRUE) ||			
					 	 (gMainLogic.State.bCleaningWithDetergentActive == TRUE)) &&			
						(gMainLogic.State.bCleaningWithGlueActive == FALSE) )
					{
						IO.Output.bValveY2		= 0;					// Klebstoffventil Y2 schlie�en
						IO.Output.bValveY3		= 1;					// Reinigungsventil Y3 �ffnen					
					}
					// Reinigungsmittel deaktiviert oder Anforderung zum Reinigen mit Leim
					else
					{
						IO.Output.bValveY2		= 1;					// Klebstoffventil Y2 �ffnen
						IO.Output.bValveY3		= 0;					// Reinigungsventil Y3 schlie�en					
					}				
				}
				else
				{
					IO.Output.bValveY2 = 0;
					IO.Output.bValveY3 = 0;				
				}
			}
			// Die Reinigung ist nicht aktiv
			else
			{
				// Automatische Ventilsteuerung aktiv
				if ((gPar.MachinePar.TankType.bTankCleanEn == 1) &&
					(gPar.MachinePar.TankType.bAutoValveCtrl == 1))
				{
					IO.Output.bValveY2		= 1;						// Klebstoffventil Y2 �ffnen
					IO.Output.bValveY3		= 0;						// Reinigungsventil Y3 schlie�en
				}
				else
				{
					IO.Output.bValveY2 = 0;
					IO.Output.bValveY3 = 0;
				}
			}
			
			// Ist die motorische Verstellung aktiv, m�ssen alle konfigurierten Achsen mit dem Automatikstart auf ihre Arbeitsposition verfahren werden
			if( gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn == TRUE )
			{
				// Pr�fen ob Kommandobox von gAxisControl frei ist
				if( (BrbSetCaller(&gAxisControl.CallerBox.Caller, eCALLERID_MAIN_LOGIC) == eBRB_CALLER_STATE_OK ) )
				{
					// Kommando absetzen: Alle motorischen Verstellungen auf ihre Arbeitspositionen verfahren (wenn n�tig wird vorher referenziert)
					gAxisControl.CallerBox.bMoveAllAxesToWorkPosition = TRUE;
					
					gMainLogic.State.bAutomatic = 1;						// Automatikbetrieb ist aktiv
					BrbClearCallerBox((UDINT)&gMainLogic.CallerBox, sizeof(gMainLogic.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
				}
			}
			else
			{
				gMainLogic.State.bAutomatic = 1;						// Automatikbetrieb ist aktiv
				BrbClearCallerBox((UDINT)&gMainLogic.CallerBox, sizeof(gMainLogic.CallerBox));
				Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		// Automatikbetrieb ausschalten
		case eSTEP_STOP:
			strcpy(Step.sStepText, "eSTEP_STOP");
			
			if (gMainLogic.State.bCleaningActive == 1)
			{
				IO.Output.bValveY1 = 1;							// Hauptventil Y1 �ffnen
				// Automatische Ventilsteuerung aktiv
				if ((gPar.MachinePar.TankType.bTankCleanEn == 1) &&
					(gPar.MachinePar.TankType.bAutoValveCtrl == 1))
				{
					// Reinigungsmittel aktiv und keine Anforderung zum Reinigen mit Leim
					if( ((gMainLogic.Par.bDetergentActive == TRUE) ||			
					 	 (gMainLogic.State.bCleaningWithDetergentActive == TRUE)) &&			
						(gMainLogic.State.bCleaningWithGlueActive == FALSE) )
					{
						IO.Output.bValveY2		= 0;				// Klebstoffventil Y2 schlie�en
						IO.Output.bValveY3		= 1;				// Reinigungsventil Y3 �ffnen						
					}
					// Reinigungsmittel deaktiviert oder Anforderung zum Reinigen mit Leim
					else
					{
						IO.Output.bValveY2		= 1;				// Klebstoffventil Y2 �ffnen
						IO.Output.bValveY3		= 0;				// Reinigungsventil Y3 schlie�en					
					}				
				}
				else
				{
					IO.Output.bValveY1 = 1;
					IO.Output.bValveY2 = 0;
					IO.Output.bValveY3 = 0;				
				}	
			}
			else
			{
				IO.Output.bValveY1 = 0;
				IO.Output.bValveY2 = 0;
				IO.Output.bValveY3 = 0;			
			}
			
			gMainLogic.State.bAutomatic = 0;					// Automatikbetrieb ist inaktiv
			// Direkt-Kommando "Automatik stoppen" abl�schen
			gMainLogic.DirectBox.bStop = FALSE;
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Reinigung einschalten
		case eSTEP_CLEANING:
			strcpy(Step.sStepText, "eSTEP_CLEANING");
			
			IO.Output.bValveY1	= 1;							// Hauptventil Y1 �ffnen
			// Automatische Ventilsteuerung aktiv
			if ((gPar.MachinePar.TankType.bTankCleanEn == 1) &&
				(gPar.MachinePar.TankType.bAutoValveCtrl == 1))
			{
				// Reinigungsmittel aktiv und keine Anforderung zum Reinigen mit Leim
				if( ((gMainLogic.Par.bDetergentActive == TRUE) ||			
					 (gMainLogic.State.bCleaningWithDetergentActive == TRUE)) &&
					(gMainLogic.State.bCleaningWithGlueActive == FALSE) )
				{
					IO.Output.bValveY2		= 0;					// Klebstoffventil Y2 schlie�en
					IO.Output.bValveY3		= 1;					// Reinigungsventil Y3 �ffnen					
				}
				// Reinigungsmittel deaktiviert oder Anforderung zum Reinigen mit Leim
				else
				{
					IO.Output.bValveY2		= 1;					// Klebstoffventil Y2 �ffnen
					IO.Output.bValveY3		= 0;					// Reinigungsventil Y3 schlie�en					
				}				
			}
			else
			{
				IO.Output.bValveY2 = 0;
				IO.Output.bValveY3 = 0;				
			}		

			gMainLogic.State.bCleaningActive = 1;				// Reinigung aktiv
			BrbClearCallerBox((UDINT)&gMainLogic.CallerBox, sizeof(gMainLogic.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Reinigung stoppen
		case eSTEP_STOP_CLEANING:
			strcpy(Step.sStepText, "eSTEP_STOP_CLEANING");

			if (gMainLogic.State.bAutomatic == 1)				// Automatikbetrieb 
			{
				IO.Output.bValveY1	= 1;						// Hauptventil Y1 �ffnen
				// Automatische Ventilsteuerung aktiv
				if ((gPar.MachinePar.TankType.bTankCleanEn == 1) &&
					(gPar.MachinePar.TankType.bAutoValveCtrl == 1))
				{
					IO.Output.bValveY2		= 1;					// Klebstoffventil Y2 �ffnen
					IO.Output.bValveY3		= 0;					// Reinigungsventil Y3 schlie�en				
				}
				else
				{
					IO.Output.bValveY2		= 0;					// Klebstoffventil Y2 schlie�en
					IO.Output.bValveY3		= 0;					// Reinigungsventil Y3 schlie�en				
				}			
			}
			else
			{
				IO.Output.bValveY1 = 0;
				IO.Output.bValveY2 = 0;
				IO.Output.bValveY3 = 0;		
			}
			
			gMainLogic.State.bCleaningActive = 0;				// Reinigung nicht aktiv
			// Direkt-Kommando "Reinigung stoppen" abl�schen
			gMainLogic.DirectBox.bCleaningStop = FALSE;
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;
	}
	
	// ******************************************************************************
	// 				Schrittkette f�r den Ablauf der Systemreinigung
	// ******************************************************************************
	switch( SystemCleaningStep.eStepNr )
	{
		//-----------------------------------------------------------------------------------------------------------------------
		// Schritt: Warten bis Systemreinigung gestartet wird
		case eSTEP_WAIT_START_SYSTEM_CLEANING:
			strcpy(SystemCleaningStep.sStepText, "eSTEP_WAIT_START_SYSTEM_CLEANING");
			// keine Aktion
			break;
		
		//-----------------------------------------------------------------------------------------------------------------------
		// Schritt: Systemreinigung starten
		case eSTEP_START_SYSTEM_CLEANING:
			strcpy(SystemCleaningStep.sStepText, "eSTEP_START_SYSTEM_CLEANING");
			
			// Kommando aus Caller-Box l�schen
			BrbClearCallerBox((UDINT)&gMainLogic.CallerBox, sizeof(gMainLogic.CallerBox));
			
			// Die Systemreinigung ist konfiguriert
			if( gPar.MachinePar.SystemCleaning.bEnable == TRUE )
			{
				// State "Systemreinigung aktiv" setzen
				gMainLogic.State.bSystemCleaningActive = TRUE;
				// Fortschrittsbalken auf Null setzen (leerer Balken)
				gMainLogic.State.nSystemCleaningProgress = 0;
				// Restdauer der Systemreinigung mittels konfigurierte Sp�lzeiten errechnen [s]
				gMainLogic.State.nSystemCleaningRestPeriod = gPar.MachinePar.SystemCleaning.nDetergentTankCleaningPeriod + gPar.MachinePar.SystemCleaning.nGlueTankCleaningPeriod;
				// Timer zur Ermitlllung der Restdauer der Systemreinigung parametrieren
				TON_SystemCleaningTimeElapsed.IN = TRUE;
				// Restdauer im [ms] an Funktion �bergeben
				TON_SystemCleaningTimeElapsed.PT = gMainLogic.State.nSystemCleaningRestPeriod * 1000/*ms*/;
				// Hauptventil �ffnen
				IO.Output.bValveY1 = TRUE;
				// n�chster Schritt: R�cksp�lventile (Bypass-Ventile) �ffnen
				SystemCleaningStep.eStepNr = eSTEP_OPEN_SYS_CLEAN_VALVES;				
			}
			// Die Systemreinigung ist nicht konfiguriert -> Systemreinigung abbrechen
			else
			{
				// n�chster Schritt: Die Systemreinigung wurde abgebrochen
				SystemCleaningStep.eStepNr = eSTEP_CANCEL_SYSTEM_CLEANING;
			}
			break;
		
		//-----------------------------------------------------------------------------------------------------------------------		
		// Schritt: R�cksp�lventile (Bypass-Ventile) �ffnen
		case eSTEP_OPEN_SYS_CLEAN_VALVES:
			strcpy(SystemCleaningStep.sStepText, "eSTEP_OPEN_SYS_CLEAN_VALVES");
			// Schleife �ber alle m�glichen K�pfe
			for( nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++ )
			{
				// R�cksp�lventil �ffnen
				IO.Output.bValveSystemCleaning[nDevIdx] = TRUE;
			}
			// n�chster Schritt: Klebstoffpumpe/-ventil starten
			SystemCleaningStep.eStepNr = eSTEP_START_GLUE_PUMP;
			break;
		
		//-----------------------------------------------------------------------------------------------------------------------
		// Schritt: Klebstoffpumpe/-ventil starten
		case eSTEP_START_GLUE_PUMP:
			strcpy(SystemCleaningStep.sStepText, "eSTEP_START_GLUE_PUMP");
			
			// Klebstoff-Ventil �ffnen
			// Ist die Druckregelung Aktiv wird im Task "PressureCtrl" auf den gMainLogic.State.bGlueValveOpen geachtet und entsprechen
			// die Ausg�nge IO.Output.bPressureCtrlMoveGlue bzw. IO.Output.bPressureCtrlVent dieses Task geschaltet.
			IO.Output.bValveY2 = TRUE;
			
			// Zum n�chsten Schritt: Klebstoffpumpe/-ventil stoppen, weiterschalten wenn die konfigurierte Zeit abgelaufen ist
			BrbStartStepTimeout( &SystemCleaningStepHandling, gPar.MachinePar.SystemCleaning.nGlueTankCleaningPeriod * 1000/*ms*/, eSTEP_STOP_GLUE_PUMP );
			
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Schritt: Klebstoffpumpe/-ventil stoppen
		case eSTEP_STOP_GLUE_PUMP:
			strcpy(SystemCleaningStep.sStepText, "eSTEP_STOP_GLUE_PUMP");
			
			// Klebstoff-Ventil schlie�en
			// Ist die Druckregelung Aktiv wird im Task "PressureCtrl" auf den gMainLogic.State.bGlueValveOpen geachtet und entsprechen
			// die Ausg�nge IO.Output.bPressureCtrlMoveGlue bzw. IO.Output.bPressureCtrlVent dieses Task geschaltet.
			IO.Output.bValveY2 = FALSE;
			
			// n�chster Schritt: Reinigungsmittelpumpe/-ventil starten
			SystemCleaningStep.eStepNr = eSTEP_START_DETERGENT_PUMP;
			break;
		
		//-----------------------------------------------------------------------------------------------------------------------
		// Schritt: Reinigungsmittelpumpe/-ventil starten
		case eSTEP_START_DETERGENT_PUMP:
			strcpy(SystemCleaningStep.sStepText, "eSTEP_START_DETERGENT_PUMP");
			
			// Reinigungsmittel-Ventil �ffnen
			// Ist die Druckregelung Aktiv wird im Task "PressureCtrl" auf den gMainLogic.State.bDetergentValveOpen geachtet und entsprechen
			// die Ausg�nge IO.Output.bPressureCtrlMoveGlue bzw. IO.Output.bPressureCtrlVent dieses Task geschaltet.
			IO.Output.bValveY3 = TRUE;
			
			// Zum n�chsten Schritt: Reinigungsmittelpumpe/-ventil stoppen, weiterschalten wenn die konfigurierte Zeit abgelaufen ist
			BrbStartStepTimeout( &SystemCleaningStepHandling, gPar.MachinePar.SystemCleaning.nDetergentTankCleaningPeriod * 1000/*ms*/, eSTEP_STOP_DETERGENT_PUMP );
			break;
		
		//-----------------------------------------------------------------------------------------------------------------------
		// Schritt: Reinigungsmittelpumpe/-ventil stoppen
		case eSTEP_STOP_DETERGENT_PUMP:
			strcpy(SystemCleaningStep.sStepText, "eSTEP_STOP_DETERGENT_PUMP");
			
			// Reinigungsmittel-Ventil schlie�en
			// Ist die Druckregelung Aktiv wird im Task "PressureCtrl" auf den gMainLogic.State.bDetergentValveOpen geachtet und entsprechen
			// die Ausg�nge IO.Output.bPressureCtrlMoveGlue bzw. IO.Output.bPressureCtrlVent dieses Task geschaltet.
			IO.Output.bValveY3 = FALSE;
			
			// n�chster Schritt: R�cksp�lventile (Bypass-Ventile) schlie�en
			SystemCleaningStep.eStepNr = eSTEP_CLOSE_SYS_CLEAN_VALVES;
			break;
		
		//-----------------------------------------------------------------------------------------------------------------------
		// Schritt: R�cksp�lventile (Bypass-Ventile) schlie�en		
		case eSTEP_CLOSE_SYS_CLEAN_VALVES:
			strcpy(SystemCleaningStep.sStepText, "eSTEP_CLOSE_SYS_CLEAN_VALVES");
			
			// Schleife �ber alle m�glichen K�pfe
			for( nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++ )
			{
				// R�cksp�lventil schli�en
				IO.Output.bValveSystemCleaning[nDevIdx] = FALSE;
			}
			// n�chster Schritt: Systemreinigung wurde abgeschlossen
			SystemCleaningStep.eStepNr = eSTEP_SYSTEM_CLEANING_DONE;
			break; 
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Schritt: Die Systemreinigung wurde abgebrochen (Button Visualisierung)
		case eSTEP_CANCEL_SYSTEM_CLEANING:
		default:
			// Warnung "Systemreinigung abgebrochen" setzen
			AppSetEvent( eEVT_WAR_SYSTEM_CLEAN_CANCELED, 0, "", 0, &gEventManagement );
			AppResetEvent( eEVT_WAR_SYSTEM_CLEAN_CANCELED, &gEventManagement );
			// Direkt-Kommando "Systemreinigung stoppen" zur�cksetzen
			gMainLogic.DirectBox.bSystemCleaningCancel = FALSE;
			
			// WICHTIG!!!
			// Hier darf kein "break;" stehen, da die Aktionen des Schrittes eSTEP_SYSTEM_CLEANING_DONE ausgef�hrt werden m�ssen!
		
		// Schritt: Systemreinigung wurde abgeschlossen
		case eSTEP_SYSTEM_CLEANING_DONE:
			strcpy(SystemCleaningStep.sStepText, "eSTEP_SYSTEM_CLEANING_DONE");
			
			// Alles in den Ausgangszustand zur�cksetzen
			
			// States zur�cksetzen
			gMainLogic.State.bSystemCleaningActive = FALSE;
			gMainLogic.State.nSystemCleaningProgress = 0;
			gMainLogic.State.nSystemCleaningRestPeriod = 0;
			
			// Alle Ventile schlie�en bzw. in Grundstellung bringen
			IO.Output.bValveY1 = FALSE;
			IO.Output.bValveY2 = FALSE;
			IO.Output.bValveY3 = FALSE;
			for( nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++ )
			{
				IO.Output.bValveSystemCleaning[nDevIdx] = FALSE;
			}
			
			// Timer der Zeitdauer der Systemreinigung abschalten
			TON_SystemCleaningTimeElapsed.IN = FALSE;
			// Schrittketten TimeOut-Funktion zur�cksetzen
			BrbStopStepTimeout( &SystemCleaningStepHandling );
			// n�chster Schritt: Warten bis Systemreinigung gestartet wird
			SystemCleaningStep.eStepNr = eSTEP_WAIT_START_SYSTEM_CLEANING;
			break;
	}
	
	// Timerfunktion zur Ermittlung der Restdauer der Systemreinigung aufrufen
	TON( &TON_SystemCleaningTimeElapsed );
	
	// Systemreinigung wurde gestartet
	if( SystemCleaningStep.eStepNr > eSTEP_WAIT_START_SYSTEM_CLEANING )
	{
		// Verbleibende Dauer bis Systemsp�lung abgeschlossen ist berechnen
		gMainLogic.State.nSystemCleaningRestPeriod = (TON_SystemCleaningTimeElapsed.PT - TON_SystemCleaningTimeElapsed.ET) / 1000;
		
		// Division durch Null vermeiden
		if( TON_SystemCleaningTimeElapsed.PT > 0 )
		{
			// F�llstand des Fortschrittsbalken der Systemreinigung berechnen
			gMainLogic.State.nSystemCleaningProgress = 10000 * (REAL)TON_SystemCleaningTimeElapsed.ET / (REAL)TON_SystemCleaningTimeElapsed.PT;
		}
	}
	
	// Maschine bereit
	if( (gParHandling.State.bMParValid == 1) &&
		(gParHandling.State.bInitDone == 1) &&
		(gEventHandling.State.nErrorCount == 0) &&
		(gMainLogic.State.bSystemCleaningActive == FALSE) )
	{
		if( (gPar.MachinePar.MachineModule.bTempCtrlEn == TRUE) &&
			(gTempCtrl.State.bAutomaticPossible == FALSE) )
		{	
			gMainLogic.State.bAutomaticPossible = FALSE;
		}
		else
		{
			gMainLogic.State.bAutomaticPossible = TRUE;
		}
	}
	else
	{
		gMainLogic.State.bAutomaticPossible = 0;
	}
	
	// ---------------------------------- Verschlusssystem ansteuern ----------------------------------
	// Automatik ist aktiv
	if( gMainLogic.State.bAutomatic == TRUE )
	{
		// Schleife �ber alle K�pfe
		for (nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++)
		{
			// Wenn Auftragekopf aktiviert ist --> Verschlusssystem �ffnen
			if( gPar.ProductPar.BonDevPar[nDevIdx].bBonDevEn == 1 )
			{
				// Verschlusssystem �ffnen
				IO.Output.bSealingSystem[nDevIdx] = TRUE;
				gMainLogic.State.bSealingSystem[nDevIdx] = TRUE;
			}
			else
			{
				// Verschlusssystem schlie�en
				IO.Output.bSealingSystem[nDevIdx] = FALSE;
				gMainLogic.State.bSealingSystem[nDevIdx] = FALSE;
			}
		}
	}
	// Automatik ist nicht aktiv
	else
	{
		// Schleife �ber alle K�pfe
		for (nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++)
		{
			// Verschlusssystem schlie�en
			IO.Output.bSealingSystem[nDevIdx] = FALSE;
			gMainLogic.State.bSealingSystem[nDevIdx] = FALSE;
		}
	}
	
	// Reinigung ist aktiv	->	Egal ob im Automatik-Betrieb oder Manuell, die Verschlusssysteme
	//							der zu reinigendne K�pfe werden vorrangig ge�ffnet.
	if( gMainLogic.State.bCleaningActive == TRUE )
	{
		// Schleife �ber alle K�pfe
		for( nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++ )
		{
			if( (gMainLogic.Par.bBonDevCleaningEn[nDevIdx] == TRUE) ||
				(gMainLogic.State.CleaningBonDev.bOpenBonDev[nDevIdx] == TRUE) )
			{
				// Verschlusssystem �ffnen
				IO.Output.bSealingSystem[nDevIdx] = TRUE;
				gMainLogic.State.bSealingSystem[nDevIdx] = TRUE;
			}
		}
	}

	// ------------------------------- gemeinsames Verschlusssystem ansteuern -------------------------------
	IO.Output.bSealingSystemCommon = FALSE;
	
	// Schleife �ber alle konfigurierten K�pfe
	for( nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++ )
	{
		// Das Verschlusssystem dieses Auftragekopfes ist ge�ffnet
		if( gMainLogic.State.bSealingSystem[nDevIdx] == TRUE )
		{
			// Alle Verschlusssysteme �ffnen, sobald es f�r einen Kopf n�tig ist
			IO.Output.bSealingSystemCommon = TRUE;
		}
	}
	
	// --------------------------------------- IOs auf States ver�ffentlichen ---------------------------------------
		
	for (nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++)
	{
		// Eingang des Hardware-Tasters der den jeweiligen Klebekopf kontinuierlich mit Klebstoff reinigt abfragen
		if( IO.Input.bCleaningContinuous[nDevIdx] == TRUE )
		{
			gMainLogic.State.bCleaningContinuous[nDevIdx] = TRUE;	
		}
		else
		{
			gMainLogic.State.bCleaningContinuous[nDevIdx] = FALSE;
		}
	}	
		
	// ---------------------------- Status der Ventil-Ausg�nge �ber State ver�ffentlichen ----------------------------
	gMainLogic.State.bMainValveOpen = IO.Output.bValveY1;
	gMainLogic.State.bGlueValveOpen = IO.Output.bValveY2;
	gMainLogic.State.bDetergentValveOpen = IO.Output.bValveY3;
	
	// Hilfsvariablen setzen
	bCleaningWithGlueActive = FALSE;
	bCleaningWithDetergentActive = FALSE;
	
	for (nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++)
	{
		if( gMainLogic.State.bDetergentValveOpen == FALSE )
		{
			// Ist einer der Hardware-Taster gesetzt?
//			bCleaningWithGlueActive |= IO.Input.bCleaningContinuous[nDevIdx];
			// Liegt das Direkt-Kommando zur kontinuierlichen Reinigung mit Klebstoff an?
			bCleaningWithGlueActive |= gMainLogic.DirectBox.bContCleanBonDevWithGlue[nDevIdx];
			// Liegt das Direkt-Kommando zum Intervall-Reinigen mit Klebstoff an?
			bCleaningWithGlueActive |= gMainLogic.DirectBox.bIntervalCleanBonDevWithGlue[nDevIdx];
		}
		
		if( bCleaningWithGlueActive == FALSE )
		{
			// Liegt das Direkt-Kommando zur kontinuierlichen Reinigung mit Reinigungsmittel an?
			bCleaningWithDetergentActive |= gMainLogic.DirectBox.bContCleanBonDevWithDetergent[nDevIdx];
		}
	}

	// Zum Reinigen wird Klebstoff verwendet
	gMainLogic.State.bCleaningWithGlueActive = bCleaningWithGlueActive;
	// Zum Reinigen wird Reinigungsmittel verwendet
	gMainLogic.State.bCleaningWithDetergentActive = bCleaningWithDetergentActive;
	
	// Ein Beipass-Ventil ist im System vorhanden -> die Reinigungsfunktionen arbeiten mit dem Basis-Druck und nicht mit dem maximal m�glichen
	if( gPar.MachinePar.PressureControl.PreCtrlBasis.rBypassOpenTime != 0.0 )
	{
		gMainLogic.State.bCleaningWithPressureCtrlActive = gMainLogic.State.bCleaningActive;
	}
}

void _EXIT MainLogicEXIT(void)
{
}
