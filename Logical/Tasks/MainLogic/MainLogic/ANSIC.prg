﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.0.19.69 SP?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Task">MainLogic.c</File>
    <File Description="Lokale Datentypen des Tasks" Private="true">MainLogic.typ</File>
    <File Description="Lokale Variablen des Tasks" Private="true">MainLogic.var</File>
    <File Description="Hilfsfunktionen des Tasks">MainLogicFunc.c</File>
    <File Description="Prototypen der Hilfsfuntkionen">MainLogicFunc.h</File>
    <File Reference="true">\Logical\Global\Global.c</File>
  </Files>
</Program>