(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: MainLogic
 * File: MainLogic.typ
 * Author: kusculart
 * Created: Oktober 02, 2014
 ********************************************************************
 * Local data types of program MainLogic
 ********************************************************************)

TYPE
	SystemCleaningSteps_ENUM : 
		( (*Schritte der Systemreinigung*)
		eSTEP_WAIT_START_SYSTEM_CLEANING := 0,
		eSTEP_START_SYSTEM_CLEANING,
		eSTEP_OPEN_SYS_CLEAN_VALVES,
		eSTEP_START_GLUE_PUMP,
		eSTEP_STOP_GLUE_PUMP,
		eSTEP_START_DETERGENT_PUMP,
		eSTEP_STOP_DETERGENT_PUMP,
		eSTEP_CLOSE_SYS_CLEAN_VALVES,
		eSTEP_SYSTEM_CLEANING_DONE,
		eSTEP_CANCEL_SYSTEM_CLEANING
		);
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_AUTOMATIC_ON := 200, (*Automatikbetrieb aktiv*)
		eSTEP_STOP := 201, (*Maschine ausschalten*)
		eSTEP_CLEANING := 202, (*Reinigung einschalten*)
		eSTEP_STOP_CLEANING := 203 (*Reinigung stoppen*)
		);
	SystemCleaningStep_TYP : 	STRUCT 
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : SystemCleaningSteps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	MainLogicStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	IO_TYP : 	STRUCT 
		Output : IoOutput_TYP;
		Input : IoInput_TYP;
	END_STRUCT;
	IoOutput_TYP : 	STRUCT 
		bValveY1 : BOOL; (*Hauptventil Y1*)
		bValveY2 : BOOL; (*Klebstoffventil Y2*)
		bValveY3 : BOOL; (*Reinigungsventil Y3*)
		bSealingSystemCommon : BOOL; (*Alle Verschlusssysteme �ffnen, sobald es f�r einen Kopf n�tig ist. Diese Variable wird verwendet, wenn es nur einen Ausgang f�r alle Verschlusssysteme gibt (gemeinsames Verschlusssystem).*)
		bSealingSystem : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Verschlusssystem f�r jeden Kopf einzeln �ffnen*)
		bValveSystemCleaning : ARRAY[0..nDEV_INDEX_MAX]OF BOOL;
	END_STRUCT;
	IoInput_TYP : 	STRUCT 
		bOpenBonDev : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Auftragk�pfe �ffnen*)
		bCleaningInt : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Reserve*)
		bCleaningContinuous : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Eingang von einem Hardware-Taster, das den jeweiligen zugeordneten Klebekopf kontinuierlich mit Klebstoff reinigt*)
	END_STRUCT;
	Cleaning_TYP : 	STRUCT 
		bCleaningCmpVar : ARRAY[0..nDEV_INDEX_MAX]OF BOOL;
		nCmpResult : DINT;
		nCyclicCnt : USINT;
	END_STRUCT;
END_TYPE
