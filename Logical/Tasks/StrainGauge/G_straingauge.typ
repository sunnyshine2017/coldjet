(********************************************************************
 * COPYRIGHT -- Planatol System GmbH
 ********************************************************************
 * Package: Straingauge
 * File: Straingauge.typ
 * Author: Resmi
 * Created: May 15, 2017
 ********************************************************************
 * Data types of package Straingauge
 ********************************************************************)

TYPE
	StrainGaugePar_TYP : 	STRUCT  (*Kommmando-Parameter*)
		nDummy : UINT;
	END_STRUCT;
	StrainGaugeDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bDummy : BOOL;
	END_STRUCT;
	StrainGaugeCallerBox_TYP : 	STRUCT  (*Reservierung-Kommandos*)
		Caller : BrbCaller_TYP;
		bDummy : BOOL;
	END_STRUCT;
	StainGaugeState_TYP : 	STRUCT  (*Rückmeldungen*)
		nDummy : DINT;
	END_STRUCT;
	StrainGauge_TYP : 	STRUCT 
		CallerBox : StrainGaugeCallerBox_TYP;
		DirectBox : StrainGaugeDirectBox_TYP;
		Par : StrainGaugePar_TYP;
		State : StainGaugeState_TYP;
	END_STRUCT;
END_TYPE
