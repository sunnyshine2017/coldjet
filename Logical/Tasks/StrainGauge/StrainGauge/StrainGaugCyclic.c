/********************************************************************
 * COPYRIGHT -- Planatol System GmbH
 ********************************************************************
 * Program: strainGauge
 * File: strainGaugeCyclic.c
 * Author: Resmi
 * Created: May 16, 2017
 ********************************************************************
 * Implementation of program strainGauge
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _CYCLIC strainGaugeCyclic(void)
{
	
	// Direct-Kommandos
	if(Step.bInitDone == 1)
	{
		if(gStraingaugeLogic.DirectBox.bDummy == 1)
		{
			BrbClearDirectBox((UDINT)&gStraingaugeLogic.DirectBox, sizeof(gStraingaugeLogic.DirectBox));
		}
	}
	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			//if(gParHandling.State.bParValid == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gEventHandling.CallerBox, sizeof(gEventHandling.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			if(gEventHandling.CallerBox.bDummy == 1)
			{
				Step.eStepNr = eSTEP_CMD1;
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_CMD1:
			strcpy(Step.sStepText, "eSTEP_CMD1");
			Step.eStepNr = eSTEP_CMD1_FINISHED;
			break;

		case eSTEP_CMD1_FINISHED:
			strcpy(Step.sStepText, "eSTEP_CMD1_FINISHED");
			BrbClearCallerBox((UDINT)&gEventHandling.CallerBox, sizeof(gEventHandling.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

	}
	
/*	//Passing the pointer to functionblock WGH Scale.

	fb_WGHscale.pWGHscalePara=&pWGHscalePara;
	// MTFilter function is used find the stable value of the analog input
	fb_MTFilterMovAvg.In=analogInput;
	MTFilterMovingAverage(&fb_MTFilterMovAvg);
	
	//Calculating the actual voltage
	rActVoltage=(REAL)fb_MTFilterMovAvg.Out/10000;
	
	
	
	fb_WGHscale.raw_value=fb_MTFilterMovAvg.Out;
	WGHscale(&fb_WGHscale);
	
	// calculating the actual tank weight from the value returned by the function WGH Scale

	rTank_Weight=(REAL)fb_WGHscale.std_value/1000;*/
	
	// Calculations for the Page Analog Füllstand
	
	// Glue Tank1 
	
	rTank_WeightFull1=(REAL)31500/1000;
	rActVoltageFull1=(REAL)23700/10000;
	
	rTank_WeightTara1=(REAL)302/1000;
	rActVoltageTara1=(REAL)2522/10000;
	

}
