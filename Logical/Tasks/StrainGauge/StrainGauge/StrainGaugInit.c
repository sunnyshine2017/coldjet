/********************************************************************
 * COPYRIGHT -- Planatol System GmbH
 ********************************************************************
 * Program: strainGauge
 * File: strainGaugeInit.c
 * Author: Resmi
 * Created: May 16, 2017
 ********************************************************************
 * Implementation of program strainGauge
 ********************************************************************/

/*#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT strainGaugeInit(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gStraingaugeLogic, 0, sizeof(gStraingaugeLogic));
	gStraingaugeLogic.CallerBox.Caller.nCallerId=eBRB_CALLER_STATE_NOT_READY;
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
	
	
	// Intialising parameters for inbuilt function AsWeigh
	
	fb_WGHscale.enable=1;
	fb_WGHscale.t_measure=1;
	
	//Intialising parameters to Pointer to WGHscale

	pWGHscalePara.ref1_raw=3000;
	pWGHscalePara.ref1_std=1000;
	pWGHscalePara.ref2_raw=23700;
	pWGHscalePara.ref2_std=31500;
	
	//Intialising parametrs for inbuilt function MTMoving Average Filter
	
	fb_MTFilterMovAvg.Enable=1;
	fb_MTFilterMovAvg.WindowLength=7000;
} */
