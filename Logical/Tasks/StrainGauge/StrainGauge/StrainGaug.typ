(********************************************************************
 * COPYRIGHT -- Planatol System GmbH
 ********************************************************************
 * Program: strainGauge
 * File: strainGauge.typ
 * Author: Resmi
 * Created: May 16, 2017
 ********************************************************************
 * Local data types of program strainGauge
 ********************************************************************)

TYPE
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_CMD1 := 200,
		eSTEP_CMD1_FINISHED
		);
	StarinGaugeStep_TYP : 	STRUCT 
		sStepText : STRING[80]; (*Schrittketten-Struktur*)
		eStepNr : {REDUND_UNREPLICABLE} Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
END_TYPE
