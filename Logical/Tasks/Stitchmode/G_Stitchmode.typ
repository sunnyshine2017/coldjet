(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: Stitchmode
 * File: G_Stitchmode.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Global  data types of package Stitchmode
 ********************************************************************)

TYPE
	StitchmodeCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bDummy : BOOL;
	END_STRUCT;
	StitchmodeDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bDummy : BOOL;
	END_STRUCT;
	StitchmodePar_TYP : 	STRUCT  (*Kommando-Paramter*)
		nDummy : UINT;
	END_STRUCT;
	StitchmodeState_TYP : 	STRUCT  (*Rückmeldungen*)
		bEnableStitchmode : BOOL; (*Stitchmode freigeben*)
		nStitchingActiveX2X : ARRAY[0..nDEV_INDEX_MAX]OF USINT; (*Stitching Aktiv (1 = Inaktiv / 2 = Aktiv)*)
		nStitchingActive : ARRAY[0..nDEV_INDEX_MAX]OF DINT; (*Stitching Aktiv (1 = Inaktiv / 2 = Aktiv)*)
	END_STRUCT;
	Stitchmode_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : StitchmodeCallerBox_TYP;
		DirectBox : StitchmodeDirectBox_TYP;
		Par : StitchmodePar_TYP;
		State : StitchmodeState_TYP;
	END_STRUCT;
END_TYPE
