(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Stitchmode
 * File: Stitchmode.typ
 * Author: kusculart
 * Created: March 26, 2015
 ********************************************************************
 * Local data types of program Stitchmode
 ********************************************************************)

TYPE
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_CMD1 := 200,
		eSTEP_CMD1_FINISHED
		);
	StitchmodeStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	StitchTime_TYP : 	STRUCT 
		nReactionCycleOnX2X : DINT; (*�ffnungszeit in Reaction-Zyklen (Zeit = nReactionCycleOn * Reaction Zykluszeit); Da das X20RT8001 am X2X-Bus nur 30 Byte an zyklischen Ausgangsdaten zur Verf�gung hat, wird dieser Wert azyklisch �ber die Funktion AsIOAccWrite() an das Modul �bertragen.*)
		nReactionCycleOn : DINT; (*�ffnungszeit in Reaction-Zyklen (Zeit = nReactionCycleOn * Reaction Zykluszeit); Wird mit jedem Powerlink-Zyklus an das X67 RT-Modul gesendet.*)
		nReactionCycleOff : DINT; (*Ausschaltzeit in Reaction-Zyklen (Zeit = nReactionCycleOff * Reaction Zykluszeit); Wird mit jedem Buszyklus an das X67 bzw. X20 RT-Modul gesendet.*)
	END_STRUCT;
	WriteAsyncDataToX2XModule_TYP : 	STRUCT 
		AsIO : USINT;
		New_Member1 : USINT;
		New_Member : USINT;
	END_STRUCT;
END_TYPE
