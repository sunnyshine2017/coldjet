/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Stitchmode
 * File: Stitchmode.c
 * Author: kusculart
 * Created: March 26, 2015
 ********************************************************************
 * Implementation of program Stitchmode
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <StitchmodeFunc.h>

void _INIT StitchmodeINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gStitchmode, 0, sizeof(gStitchmode));
	gStitchmode.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	// Enable Stitchmode
	gStitchmode.State.bEnableStitchmode = 1;

	// ******************** Parameter des Funktionsblock AsIOAccWrite() bef�llen ********************
	// Pfad an dem das X2X-Modul des jeweiligen Kopfes ( [Index] = Kopf - 1 ) zu finden ist, an die Funktion �bergeben
	#ifdef HW_C70
		#ifndef HW_DC1176_NOT_AVAILABLE
			WriteAsyncPVARtoRT_Module_X2X[0].pDeviceName = (UDINT)"IF5.ST4";
			WriteAsyncPVARtoRT_Module_X2X[1].pDeviceName = (UDINT)"IF5.ST4";
			WriteAsyncPVARtoRT_Module_X2X[2].pDeviceName = (UDINT)"IF5.ST4";
			WriteAsyncPVARtoRT_Module_X2X[3].pDeviceName = (UDINT)"IF5.ST4";
			WriteAsyncPVARtoRT_Module_X2X[4].pDeviceName = (UDINT)"IF5.ST6";
			WriteAsyncPVARtoRT_Module_X2X[5].pDeviceName = (UDINT)"IF5.ST6";
			WriteAsyncPVARtoRT_Module_X2X[6].pDeviceName = (UDINT)"IF5.ST6";
			WriteAsyncPVARtoRT_Module_X2X[7].pDeviceName = (UDINT)"IF5.ST6";
			WriteAsyncPVARtoRT_Module_X2X[8].pDeviceName = (UDINT)"IF5.ST8";
			WriteAsyncPVARtoRT_Module_X2X[9].pDeviceName = (UDINT)"IF5.ST8";
			WriteAsyncPVARtoRT_Module_X2X[10].pDeviceName = (UDINT)"IF5.ST8";
			WriteAsyncPVARtoRT_Module_X2X[11].pDeviceName = (UDINT)"IF5.ST8";
		#else
			WriteAsyncPVARtoRT_Module_X2X[0].pDeviceName = (UDINT)"IF5.ST3";
			WriteAsyncPVARtoRT_Module_X2X[1].pDeviceName = (UDINT)"IF5.ST3";
			WriteAsyncPVARtoRT_Module_X2X[2].pDeviceName = (UDINT)"IF5.ST3";
			WriteAsyncPVARtoRT_Module_X2X[3].pDeviceName = (UDINT)"IF5.ST3";
			WriteAsyncPVARtoRT_Module_X2X[4].pDeviceName = (UDINT)"IF5.ST5";
			WriteAsyncPVARtoRT_Module_X2X[5].pDeviceName = (UDINT)"IF5.ST5";
			WriteAsyncPVARtoRT_Module_X2X[6].pDeviceName = (UDINT)"IF5.ST5";
			WriteAsyncPVARtoRT_Module_X2X[7].pDeviceName = (UDINT)"IF5.ST5";
			WriteAsyncPVARtoRT_Module_X2X[8].pDeviceName = (UDINT)"IF5.ST7";
			WriteAsyncPVARtoRT_Module_X2X[9].pDeviceName = (UDINT)"IF5.ST7";
			WriteAsyncPVARtoRT_Module_X2X[10].pDeviceName = (UDINT)"IF5.ST7";
			WriteAsyncPVARtoRT_Module_X2X[11].pDeviceName = (UDINT)"IF5.ST7";
		#endif
	#elif HW_CP1585		
		WriteAsyncPVARtoRT_Module_X2X[0].pDeviceName = (UDINT)"IF6.ST10";
		WriteAsyncPVARtoRT_Module_X2X[1].pDeviceName = (UDINT)"IF6.ST10";
		WriteAsyncPVARtoRT_Module_X2X[2].pDeviceName = (UDINT)"IF6.ST10";
		WriteAsyncPVARtoRT_Module_X2X[3].pDeviceName = (UDINT)"IF6.ST10";
		WriteAsyncPVARtoRT_Module_X2X[4].pDeviceName = (UDINT)"IF6.ST11";
		WriteAsyncPVARtoRT_Module_X2X[5].pDeviceName = (UDINT)"IF6.ST11";
		WriteAsyncPVARtoRT_Module_X2X[6].pDeviceName = (UDINT)"IF6.ST11";
		WriteAsyncPVARtoRT_Module_X2X[7].pDeviceName = (UDINT)"IF6.ST11";
		WriteAsyncPVARtoRT_Module_X2X[8].pDeviceName = (UDINT)"IF6.ST12";
		WriteAsyncPVARtoRT_Module_X2X[9].pDeviceName = (UDINT)"IF6.ST12";
		WriteAsyncPVARtoRT_Module_X2X[10].pDeviceName = (UDINT)"IF6.ST12";
		WriteAsyncPVARtoRT_Module_X2X[11].pDeviceName = (UDINT)"IF6.ST12";
	#endif
	
	// Name des Registers (PVARxxx) auf die die "Ventil offen"-Zyklen des jeweiligen Kopfes �bertragen werden
	WriteAsyncPVARtoRT_Module_X2X[0].pChannelName = (UDINT)"PVAR100";
	WriteAsyncPVARtoRT_Module_X2X[1].pChannelName = (UDINT)"PVAR101";
	WriteAsyncPVARtoRT_Module_X2X[2].pChannelName = (UDINT)"PVAR102";
	WriteAsyncPVARtoRT_Module_X2X[3].pChannelName = (UDINT)"PVAR103";
	WriteAsyncPVARtoRT_Module_X2X[4].pChannelName = (UDINT)"PVAR100";
	WriteAsyncPVARtoRT_Module_X2X[5].pChannelName = (UDINT)"PVAR101";
	WriteAsyncPVARtoRT_Module_X2X[6].pChannelName = (UDINT)"PVAR102";
	WriteAsyncPVARtoRT_Module_X2X[7].pChannelName = (UDINT)"PVAR103";
	WriteAsyncPVARtoRT_Module_X2X[8].pChannelName = (UDINT)"PVAR100";
	WriteAsyncPVARtoRT_Module_X2X[9].pChannelName = (UDINT)"PVAR101";
	WriteAsyncPVARtoRT_Module_X2X[10].pChannelName = (UDINT)"PVAR102";
	WriteAsyncPVARtoRT_Module_X2X[11].pChannelName = (UDINT)"PVAR103";
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC StitchmodeCYCLIC(void)
{
	// Direct-Kommandos
	if(Step.bInitDone == 1)
	{
		if(gStitchmode.DirectBox.bDummy == 1)
		{
			BrbClearDirectBox((UDINT)&gStitchmode.DirectBox, sizeof(gStitchmode.DirectBox));
		}
	}
	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	brsstrcpy((UDINT)StepHandling.Current.sStepText, (UDINT)Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_INIT:
			brsstrcpy((UDINT)Step.sStepText, (UDINT)"eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			brsstrcpy((UDINT)Step.sStepText, (UDINT)"eSTEP_INIT_WAIT_FOR_PAR_VALID");
			//if(gParHandling.State.bParValid == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			brsstrcpy((UDINT)Step.sStepText, (UDINT)"eSTEP_INIT_FINISHED");
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gStitchmode.CallerBox, sizeof(gStitchmode.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_WAIT_FOR_COMMAND:
			brsstrcpy((UDINT)Step.sStepText, (UDINT)"eSTEP_WAIT_FOR_COMMAND");
			if(gStitchmode.CallerBox.bDummy == 1)
			{
				Step.eStepNr = eSTEP_CMD1;
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_CMD1:
			brsstrcpy((UDINT)Step.sStepText, (UDINT)"eSTEP_CMD1");
			Step.eStepNr = eSTEP_CMD1_FINISHED;
			break;

		case eSTEP_CMD1_FINISHED:
			brsstrcpy((UDINT)Step.sStepText, (UDINT)"eSTEP_CMD1_FINISHED");
			BrbClearCallerBox((UDINT)&gStitchmode.CallerBox, sizeof(gStitchmode.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

	}

	// �ffnungszeiten anhand von Stitchgr��e und Geschwindigkeit berechnen
	if (gParHandling.State.bPParValid == 1)
	{
		// For Schleife f�r die Ventile
		for (nForIdx = 0; nForIdx <= gPar.MachinePar.nBonDevIndex; nForIdx++)
		{
			// Das Stitching ist im allgemeinen und f�r diesen Kopf aktiviert
			if ((gPar.MachinePar.MachineModule.bStitchmodeEn == 1) &&
				(gPar.ProductPar.BonDevPar[nForIdx].bStitchmode == 1))
			{	
				
				// Die Kopfreinigung hat Vorrang gegen�ber dem Stitching
				if( gMainLogic.State.bCleaningActive == 0 )
				{
					// Stitching Aktiv
					gStitchmode.State.nStitchingActive[nForIdx] = gStitchmode.State.nStitchingActiveX2X[nForIdx] = 1;
				}
				else
				{
					// Stitching inaktiv
					gStitchmode.State.nStitchingActive[nForIdx] = gStitchmode.State.nStitchingActiveX2X[nForIdx] = 0;
				}
				// Berechnung der Stitchl�nge
				// �ffnungszeit
				if (gPar.ProductPar.BonDevPar[nForIdx].nStitchSize == eSTITCH_SIZE_SMALL)
				{
					if( (gPar.MachinePar.StitchmodeDistance.rOpenTimeSmall != rOpenTimeSmall_old) ||
						(gPar.ProductPar.BonDevPar[nForIdx].nStitchSize != nStitchSize_old[nForIdx]) )
					{
						// Der errechnete Wert gilt f�r einen Reactionzyklus von 5�s
						// Das hei�t, Wert * Reaction-Zyklus(5�s) = �ffnungszeit
						StitchTime[nForIdx].nReactionCycleOn = (DINT)Round((gPar.MachinePar.StitchmodeDistance.rOpenTimeSmall * nFACTOR_MM_TO_MIKRO) / nREACTION_CYCLE_TIME);

						// Da das X20RT8001 am X2X-Bus nur 30 Byte an zyklischen Ausgangsdaten zur Verf�gung hat, wird dieser Wert azyklisch �ber die Funktion AsIOAccWrite() an das Modul �bertragen.
						StitchTime[nForIdx].nReactionCycleOnX2X = (DINT)Round((gPar.MachinePar.StitchmodeDistance.rOpenTimeSmall * nFACTOR_MM_TO_MIKRO) / nREACTION_CYCLE_TIME);
						
						// Wert Merken, wenn die Schleife am Ende angelangt ist und alle Indexe berechnet wurden
						if( nForIdx == gPar.MachinePar.nBonDevIndex )
						{
							// Die Werte aus der Konfigurationsseite Stitching merken um im n�chsten Zyklus auf �nderungen pr�fen zu k�nnen
							rOpenTimeSmall_old = gPar.MachinePar.StitchmodeDistance.rOpenTimeSmall;
							
						}
					}
				}
				else if (gPar.ProductPar.BonDevPar[nForIdx].nStitchSize == eSTITCH_SIZE_MEDIUM)
				{
					if( (gPar.MachinePar.StitchmodeDistance.rOpenTimeMedium != rOpenTimeMedium_old) ||
						(gPar.ProductPar.BonDevPar[nForIdx].nStitchSize != nStitchSize_old[nForIdx]) )
					{
						// Der errechnete Wert gilt f�r einen Reactionzyklus von 5�s
						// Das hei�t, Wert * Reaction-Zyklus(5�s) = �ffnungszeit
						StitchTime[nForIdx].nReactionCycleOn = (DINT)Round((gPar.MachinePar.StitchmodeDistance.rOpenTimeMedium * nFACTOR_MM_TO_MIKRO) / nREACTION_CYCLE_TIME);

						// Da das X20RT8001 am X2X-Bus nur 30 Byte an zyklischen Ausgangsdaten zur Verf�gung hat, wird dieser Wert azyklisch �ber die Funktion AsIOAccWrite() an das Modul �bertragen.
						StitchTime[nForIdx].nReactionCycleOnX2X = (DINT)Round((gPar.MachinePar.StitchmodeDistance.rOpenTimeMedium * nFACTOR_MM_TO_MIKRO) / nREACTION_CYCLE_TIME);
							
						// Wert Merken, wenn die Schleife am Ende angelangt ist und alle Indexe berechnet wurden
						if( nForIdx == gPar.MachinePar.nBonDevIndex )
						{
							// Die Werte aus der Konfigurationsseite Stitching merken um im n�chsten Zyklus auf �nderungen pr�fen zu k�nnen
							rOpenTimeMedium_old = gPar.MachinePar.StitchmodeDistance.rOpenTimeMedium;
						}
					}
				}
				else if (gPar.ProductPar.BonDevPar[nForIdx].nStitchSize == eSTITCH_SIZE_LARGE)
				{
					if( (gPar.MachinePar.StitchmodeDistance.rOpenTimeLarge != rOpenTimeLarge_old) ||
						(gPar.ProductPar.BonDevPar[nForIdx].nStitchSize != nStitchSize_old[nForIdx]) )
					{
						// Der errechnete Wert gilt f�r einen Reactionzyklus von 5�s
						// Das hei�t, Wert * Reaction-Zyklus(5�s) = �ffnungszeit
						StitchTime[nForIdx].nReactionCycleOn = (DINT)Round((gPar.MachinePar.StitchmodeDistance.rOpenTimeLarge * nFACTOR_MM_TO_MIKRO) / nREACTION_CYCLE_TIME);

						// Da das X20RT8001 am X2X-Bus nur 30 Byte an zyklischen Ausgangsdaten zur Verf�gung hat, wird dieser Wert azyklisch �ber die Funktion AsIOAccWrite() an das Modul �bertragen.
						StitchTime[nForIdx].nReactionCycleOnX2X = (DINT)Round((gPar.MachinePar.StitchmodeDistance.rOpenTimeLarge * nFACTOR_MM_TO_MIKRO) / nREACTION_CYCLE_TIME);

						// Wert Merken, wenn die Schleife am Ende angelangt ist und alle Indexe berechnet wurden
						if( nForIdx == gPar.MachinePar.nBonDevIndex )
						{
							// Die Werte aus der Konfigurationsseite Stitching merken um im n�chsten Zyklus auf �nderungen pr�fen zu k�nnen
							rOpenTimeLarge_old = gPar.MachinePar.StitchmodeDistance.rOpenTimeLarge;
						}
					}
				}
				
				// Ung�ltigen Parameter rStitchCloseDistance <= 0 mm auf den Minimalwert 0,2 mm setzen 
				if( gPar.ProductPar.BonDevPar[nForIdx].rStitchCloseDistance <= 0.0 )
				{
					gPar.ProductPar.BonDevPar[nForIdx].rStitchCloseDistance = 0.2;
				}
				
				// Ausschaltzeit
				// Der errechnete Wert gilt f�r einen Reactionzyklus von 5�s
				// Division durch Null vermeiden wenn die Maschinengeschwindigkeit Null ist
				if( gEncoder.State.rVelocity[gPar.ProductPar.BonDevPar[nForIdx].nIdxEncoder] != 0.0 )
				{
					// Das hei�t, Wert * Reaction-Zyklus(5�s) = Ausschaltzeit
					StitchTime[nForIdx].nReactionCycleOff = (DINT)Round((gPar.ProductPar.BonDevPar[nForIdx].rStitchCloseDistance * rFACTOR_MM_TO_AXIS_UNITS * nFACTOR_TIME_TO_CYCLE) / gEncoder.State.rVelocity[gPar.ProductPar.BonDevPar[nForIdx].nIdxEncoder]);
					// Da der Reaction-Zyklus 5�s betr�gt, muss das Ergebnis verdoppelt werden
					StitchTime[nForIdx].nReactionCycleOff = (StitchTime[nForIdx].nReactionCycleOff * 2) - nREACTION_CYCLE;
					
					// Die Einschaltzyklen von den Ausschaltzyklen abziehen, um den Punkt auch wirklich auf den Punktabstand zu setzen
					if( StitchTime[nForIdx].nReactionCycleOff >= StitchTime[nForIdx].nReactionCycleOnX2X )
					{
						StitchTime[nForIdx].nReactionCycleOff -= StitchTime[nForIdx].nReactionCycleOnX2X;
					}
					// Die Subtraktion w�rde <= 0 ergeben -> kein Stitching sondern durchg�ngier Klebeauftrag
					else
					{
						// Da nReactionCycleOff = 0 keinen durchg�ngigen Klebeauftrag m�glich macht wird dieser auf 1 gesetzt
						StitchTime[nForIdx].nReactionCycleOff = 1;
					}
				}
				else
				{
					StitchTime[nForIdx].nReactionCycleOff = 0;
				}			

				// ********** �bertragen der "Ventil offen"-Zyklen �ber den asynchronen Kanal des X2X-Busses an die jeweiligen X20RT8001 **********
				// Die errechneten "Ventil offen"-Zyklen haben sich ge�ndert
				if( StitchTime[nForIdx].nReactionCycleOnX2X != nReactionCycleOnX2X_old[nForIdx] )
				{
					// Parameter des Funktionsblock AsIOAccWrite() bef�llen
					WriteAsyncPVARtoRT_Module_X2X[nForIdx].enable = TRUE;
					WriteAsyncPVARtoRT_Module_X2X[nForIdx].value = StitchTime[nForIdx].nReactionCycleOnX2X;
					AsIOAccWrite( &WriteAsyncPVARtoRT_Module_X2X[nForIdx] );
				
					// Das asynchrone Schreiben des Registers ist nicht mehr aktiv
					if( WriteAsyncPVARtoRT_Module_X2X[nForIdx].status != ERR_FUB_BUSY )
					{
						// Das Register wurde mit den "Ventil offen"-Zyklen beschrieben
						if( WriteAsyncPVARtoRT_Module_X2X[nForIdx].status == ERR_OK )
						{
							// "Ventil offen"-Zyklen merken
							nReactionCycleOnX2X_old[nForIdx] = StitchTime[nForIdx].nReactionCycleOnX2X;
							// Z�hler mit der Anzahl der fehlerhaften Versuche zu schreiben zur�cksetzen
							nWriteAsyncPVARtoX2X_ErrCnt[nForIdx] = 0;
						}
						// Ein Fehler ist beim Schreiben aufgetreten
						else
						{
							// Z�hler der fehlerhaften Versuche den Wert zu schreiben erh�hen
							nWriteAsyncPVARtoX2X_ErrCnt[nForIdx]++;
							
							// Schreiben des Wertes �ber den asynchronen Kanal ist drei mal fehlgeschlagen
							if( nWriteAsyncPVARtoX2X_ErrCnt[nForIdx] >= 3 )
							{
								// Warnung: Der Parameter Punktgr��e konnte nicht an das X20 Stitching-Modul �bertragen werden, setzen
								AppSetEvent( eEVT_WAR_STITCH_WRITE_ERR_PVAR, WriteAsyncPVARtoRT_Module_X2X[nForIdx].status, "", nForIdx + 1, &gEventManagement );
								AppResetEvent( eEVT_WAR_STITCH_WRITE_ERR_PVAR, &gEventManagement );
								
								// Z�hler mit der Anzahl der fehlerhaften Versuche zu schreiben zur�cksetzen
								nWriteAsyncPVARtoX2X_ErrCnt[nForIdx] = 0;
								// "Ventil offen"-Zyklen merken
								nReactionCycleOnX2X_old[nForIdx] = StitchTime[nForIdx].nReactionCycleOnX2X;
							}
						}
					}
				}
				
				// nStitchSize des jeweiligen Kopfes merken
				nStitchSize_old[nForIdx] = gPar.ProductPar.BonDevPar[nForIdx].nStitchSize;
			}
			// Das Stitching ist nicht aktiviert
			else
			{
				// Stitching Inaktiv
				gStitchmode.State.nStitchingActive[nForIdx] = gStitchmode.State.nStitchingActiveX2X[nForIdx] = 0;			
			}
		}
		
		// ***************** Warnungen und Fehlermeldungen wurden in Visu quittiert *****************
		
		// Warnung: Der Parameter Punktgr��e konnte nicht an das X20 Stitching-Modul �bertragen werden, wurde quittiert
		if( AppIsEventAcknowledged( eEVT_WAR_STITCH_WRITE_ERR_PVAR, &gEventManagement ) == 1 )
		{
			// For Schleife �ber alle konfigurierten K�pfe
			for (nForIdx = 0; nForIdx <= gPar.MachinePar.nBonDevIndex; nForIdx++)
			{
				// Die Werte von ReactionCyclicOnX2X f�r alle K�pfe erneut senden, wenn beim �bertagen ein Fehler aufgetrten ist (Status FUB AsIOAccWrite != ERR_OK)
				if( WriteAsyncPVARtoRT_Module_X2X[nForIdx].status != ERR_OK )
				{
					// Durch setzen des gemerkten Wertes der ReactionCyclicOnX2X auf 0, wird der tats�chliche Wert erneut �ber den asynchronen Kanal gesendet 
					nReactionCycleOnX2X_old[nForIdx] = 0;
				}
			}
		}
	}
}

void _EXIT StitchmodeEXIT(void)
{
}
