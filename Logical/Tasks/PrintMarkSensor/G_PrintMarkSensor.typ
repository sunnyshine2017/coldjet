(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: PrintMarkSensor
 * File: G_PrintMarkSensor.typ
 * Author: kusculart
 * Created: November 17, 2014
 ********************************************************************
 * Global  data types of package PrintMarkSensor
 ********************************************************************)

TYPE
	PrintMarkSensorCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bPmInit : BOOL;
		bGetBonDevPar : BOOL;
	END_STRUCT;
	PrintMarkSensorDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bUpdateBonDevPar : BOOL; (*Druckmarken-Parameter der Klebek�pfe updaten, da sich diese in der Visu ge�ndert haben (�bernahmen w�hrend Automatik aktiv)*)
	END_STRUCT;
	PrintMarkSensorPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		PmFifo : ARRAY[0..nDEV_INDEX_MAX]OF PmFifoEntry_TYP; (*Druckmarken Fifo*)
	END_STRUCT;
	PrintMarkSensorState_TYP : 	STRUCT  (*R�ckmeldungen*)
		bPmSenReady : BOOL; (*Druckmarkensensor bereit*)
		nPmLen : ARRAY[0..nDEV_INDEX_MAX]OF DINT; (*Druckmarkenl�nge [1/100 mm]*)
	END_STRUCT;
	PrintMarkSensor_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : PrintMarkSensorCallerBox_TYP;
		DirectBox : PrintMarkSensorDirectBox_TYP;
		Par : PrintMarkSensorPar_TYP;
		State : PrintMarkSensorState_TYP;
	END_STRUCT;
	PmFifoEntry_TYP : 	STRUCT 
		FifoEntry : ARRAY[0..nFIFO_SIZE_INDEX]OF PmPos_TYP;
	END_STRUCT;
END_TYPE
