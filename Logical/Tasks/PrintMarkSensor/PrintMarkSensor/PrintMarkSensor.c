/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: PrintMarkSensor
 * File: PrintMarkSensor.c
 * Author: kusculart
 * Created: November 17, 2014
 ********************************************************************
 * Implementation of program PrintMarkSensor
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <PrintMarkSensorFunc.h>

void _INIT PrintMarkSensorINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gPrintMarkSensor, 0, sizeof(gPrintMarkSensor));
	gPrintMarkSensor.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC PrintMarkSensorCYCLIC(void)
{
	// Automatik wurde eingeschaltet
	fbEdgePosAuto.CLK = gMainLogic.State.bAutomatic;
	R_TRIG(&fbEdgePosAuto);
	if (fbEdgePosAuto.Q == 1)
	{
		//gTriggerSensor.CallerBox.bInit = 1;
		bRequestInit = 1;
	}
	if (bRequestInit == 1)
	{
		if (BrbSetCaller(&gPrintMarkSensor.CallerBox.Caller, eCALLERID_PRINT_MARK_SENSOR) == eBRB_CALLER_STATE_OK)
		{
			gPrintMarkSensor.CallerBox.bPmInit = 1;									// Initialisierung starten
			bRequestInit = 0;
		}	
	}
	
	// --------- Direkt-Kommandos ---------
	// Die Initialisierungs-Schrittkette erfolgreich durchgelaufen -> Direkt-Kommandos sind m�glich
	if(Step.bInitDone == TRUE)
	{
		// Druckmarken-Parameter der Klebek�pfe updaten, da sich diese in der Visu ge�ndert haben (�bernahmen w�hrend Automatik aktiv)
		if( gPrintMarkSensor.DirectBox.bUpdateBonDevPar == TRUE )
		{
			gPrintMarkSensor.DirectBox.bUpdateBonDevPar = FALSE;
			
			// Das Klebesystem befindet sich im Automatik-Modus
			if( gMainLogic.State.bAutomatic == TRUE )
			{
				for (nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++)	
				{
					if( (gPar.ProductPar.BonDevPar[nDevIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR) &&
						(gPar.ProductPar.BonDevPar[nDevIdx].bBonDevEn == TRUE) )
					{
						nIdxSenSource = gPar.ProductPar.BonDevPar[nDevIdx].nIdxSenSource;
						
						// Diese Parameter k�nnen w�hrend der Automatik-Betrieb aktiv ist ge�ndert werden und werden daraufhin sofort �bernommen.
						BonDevPar[nIdxSenSource].nPrintMarkProductOffset = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rPmProductOffset * rFACTOR_MM_TO_AXIS_UNITS);
						BonDevPar[nIdxSenSource].nSensorOffset = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS);
					}
					else if( (gPar.ProductPar.BonDevPar[nDevIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) &&
							 (gPar.ProductPar.BonDevPar[nDevIdx].bBonDevEn == TRUE) )
					{
						nIdxPrintMarkSensorSource = gPar.ProductPar.BonDevPar[nDevIdx].nIdxPrintMarkSensorSource;
						
						// Diese Parameter k�nnen w�hrend der Automatik-Betrieb aktiv ist ge�ndert werden und werden daraufhin sofort �bernommen.
						BonDevPar[nIdxPrintMarkSensorSource].nPrintMarkProductOffset = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rPmProductOffset * rFACTOR_MM_TO_AXIS_UNITS);
						BonDevPar[nIdxPrintMarkSensorSource].nDistancePrintMark_VelocityJump = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rDistancePrintMark_VelocityJump * rFACTOR_MM_TO_AXIS_UNITS);
						BonDevPar[nIdxPrintMarkSensorSource].nSensorOffset = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS);					
					}
				}
			}
		}
	}
	// Initialisierungs-Schrittkette ist noch nicht abgelaufen -> keine Direkt-Kommandos m�glich
	else
	{
		// Direkt-Kommandos sofort zur�cksetzen
		BrbClearDirectBox( (UDINT)&gPrintMarkSensor.DirectBox, sizeof(gPrintMarkSensor.DirectBox) );
	}
	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//=======================================================================================================================
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			
			for(nDevIdx = 0; nDevIdx <= nDEV_INDEX_MAX; nDevIdx++)															// Schieberegister f�r Position initialisieren
			{
				gFifoManPmSen[nDevIdx].pList			= (UDINT)&gPrintMarkSensor.Par.PmFifo[nDevIdx].FifoEntry[0];		// Adressen der Schieberegister �bergeben
				gFifoManPmSen[nDevIdx].nEntryLength		= sizeof(gPrintMarkSensor.Par.PmFifo[nDevIdx].FifoEntry[0]);		// Gr��e eines Eintrages
				gFifoManPmSen[nDevIdx].nIndexMax		= nFIFO_SIZE_INDEX;													// Max. Index des Schieberegisters
				gFifoManPmSen[nDevIdx].nEntryCount		= 0;																// Momentane Anzahl an g�ltigen Eintr�gen
			}
			
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			
			if(gParHandling.State.bInitDone == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gPrintMarkSensor.CallerBox, sizeof(gPrintMarkSensor.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//=======================================================================================================================
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			
			
			// Initialisierung ausf�hren
			if(gPrintMarkSensor.CallerBox.bPmInit == 1)
			{
				Step.eStepNr = eSTEP_INIT_PM_SENSOR;
			}
			
			// Bonding Device Parameter ermitteln
			if(gPrintMarkSensor.CallerBox.bGetBonDevPar == 1)
			{
				Step.eStepNr = eSTEP_GET_BONDEV_PAR;
			}
						
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		// Initialisierung
		case eSTEP_INIT_PM_SENSOR:
			strcpy(Step.sStepText, "eSTEP_INIT_PM_SENSOR");
			
			brsmemset((UDINT)&BonDevPar, 0, sizeof(BonDevPar));
			brsmemset((UDINT)&PmState, 0, sizeof(PmState));
			
			for (nDevIdx = 0; nDevIdx <= gPar.MachinePar.nPrintMarkSensorIndex; nDevIdx++)	
			{
				// Fifo abl�schen Trigger Sensor
				BrbMemListClear((BrbMemListManagement_Typ*)&gFifoManPmSen[nDevIdx]);
			}
			
			Step.eStepNr = eSTEP_GET_BONDEV_PAR;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		// Bonding Device Parameter ermitteln
		case eSTEP_GET_BONDEV_PAR:
			strcpy(Step.sStepText, "eSTEP_GET_BONDEV_PAR");
			
			for (nDevIdx = 0; nDevIdx <= gPar.MachinePar.nBonDevIndex; nDevIdx++)	
			{
				if( (gPar.ProductPar.BonDevPar[nDevIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR) &&
					(gPar.ProductPar.BonDevPar[nDevIdx].bBonDevEn == TRUE) &&
					(BonDevPar[gPar.ProductPar.BonDevPar[nDevIdx].nIdxSenSource].bEnPmSen == FALSE) )
				{
					nIdxSenSource = gPar.ProductPar.BonDevPar[nDevIdx].nIdxSenSource;
					
					BonDevPar[nIdxSenSource].bEnPmSen = TRUE;
					BonDevPar[nIdxSenSource].nBonDevIdx = nDevIdx;
					BonDevPar[nIdxSenSource].nEncIdx = gPar.ProductPar.BonDevPar[nDevIdx].nIdxEncoder;
					BonDevPar[nIdxSenSource].nTriggerSensorIdx = -1;
					BonDevPar[nIdxSenSource].nSourceEncoderIdx = gPar.MachinePar.EncoderPar[gPar.ProductPar.BonDevPar[nDevIdx].nIdxEncoder].nSourceEncoderIdx;
					BonDevPar[nIdxSenSource].nProductLen = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rProductLen * rFACTOR_MM_TO_AXIS_UNITS);
					BonDevPar[nIdxSenSource].eIdxSenType = gPar.ProductPar.BonDevPar[nDevIdx].eIdxSenType;
					BonDevPar[nIdxSenSource].nPrintMarkProductOffset = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rPmProductOffset * rFACTOR_MM_TO_AXIS_UNITS);
					BonDevPar[nIdxSenSource].nSensorOffset = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS);
					
					if( gPar.MachinePar.EncoderPar[gPar.ProductPar.BonDevPar[nDevIdx].nIdxEncoder].eEncoderType == eENCODER_TYPE_VIRTUAL )
					{
						BonDevPar[nIdxSenSource].bVirtualEncoderEnable = TRUE;
					}
					else
					{
						BonDevPar[nIdxSenSource].bVirtualEncoderEnable = FALSE;
					}
				}
				else if( (gPar.ProductPar.BonDevPar[nDevIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR) &&
						 (gPar.ProductPar.BonDevPar[nDevIdx].bBonDevEn == TRUE) &&
						 (BonDevPar[gPar.ProductPar.BonDevPar[nDevIdx].nIdxPrintMarkSensorSource].bEnPmSen == FALSE) )
				{
					nIdxPrintMarkSensorSource = gPar.ProductPar.BonDevPar[nDevIdx].nIdxPrintMarkSensorSource;
					
					BonDevPar[nIdxPrintMarkSensorSource].bEnPmSen = TRUE;
					BonDevPar[nIdxPrintMarkSensorSource].nBonDevIdx = nDevIdx;
					BonDevPar[nIdxPrintMarkSensorSource].nEncIdx = gPar.ProductPar.BonDevPar[nDevIdx].nIdxPrintMarkEncoder;
					BonDevPar[nIdxPrintMarkSensorSource].nTriggerSensorIdx = gPar.ProductPar.BonDevPar[nDevIdx].nIdxSenSource;
					BonDevPar[nIdxPrintMarkSensorSource].nIdxTriggerEncoder = gPar.ProductPar.BonDevPar[nDevIdx].nIdxEncoder;
					BonDevPar[nIdxPrintMarkSensorSource].nSourceEncoderIdx = gPar.MachinePar.EncoderPar[BonDevPar[nIdxPrintMarkSensorSource].nEncIdx].nSourceEncoderIdx;
					BonDevPar[nIdxPrintMarkSensorSource].nProductLen = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rProductLen * rFACTOR_MM_TO_AXIS_UNITS);
					BonDevPar[nIdxPrintMarkSensorSource].nPrintMarkProductOffset = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rPmProductOffset * rFACTOR_MM_TO_AXIS_UNITS);
					BonDevPar[nIdxPrintMarkSensorSource].nDistancePrintMark_VelocityJump = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rDistancePrintMark_VelocityJump * rFACTOR_MM_TO_AXIS_UNITS);
					BonDevPar[nIdxPrintMarkSensorSource].nSensorOffset = (DINT)(gPar.ProductPar.BonDevPar[nDevIdx].rSenOffset * rFACTOR_MM_TO_AXIS_UNITS);
					BonDevPar[nIdxPrintMarkSensorSource].eIdxSenType = gPar.ProductPar.BonDevPar[nDevIdx].eIdxSenType;
					
					if( gPar.MachinePar.EncoderPar[gPar.ProductPar.BonDevPar[nDevIdx].nIdxEncoder].eEncoderType == eENCODER_TYPE_VIRTUAL )
					{
						BonDevPar[nIdxPrintMarkSensorSource].bVirtualEncoderEnable = TRUE;
					}
					else
					{
						BonDevPar[nIdxPrintMarkSensorSource].bVirtualEncoderEnable = FALSE;
					}
				}
			}
			
			gPrintMarkSensor.State.bPmSenReady = 1;
			BrbClearCallerBox((UDINT)&gPrintMarkSensor.CallerBox, sizeof(gPrintMarkSensor.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

	}
	
	
	// ---------- Zeitstempel Aufzeichnung aktiv ----------
	for (nDevIdx = 0; nDevIdx <= gPar.MachinePar.nPrintMarkSensorIndex; nDevIdx++)	
	{	
		if ((gParHandling.State.bMParValid == 1) &&
			(gMainLogic.State.bAutomatic == 1))																												// Automatikbetrieb ist aktiv
		{
			if ((gPrintMarkSensor.State.bPmSenReady == 1) &&
				(gBondingDev.State.bVelocityOk[BonDevPar[nDevIdx].nBonDevIdx]))
			{
				if (BonDevPar[nDevIdx].bEnPmSen == 1)																									// Nur Ausf�hren, wenn Sensor benutzt wird
				{
					// Auswertung der Druckmarkenl�nge
					if (PmState.RisingDetection[nDevIdx].bCntOk == 0)
					{
						if (IO.Input.PrintMarkSen[nDevIdx].nRisingCnt != nOldRisingCnt[nDevIdx])															// Steigende Flanke
						{
							PmState.RisingDetection[nDevIdx].nDiffTime = (INT)gSystem.State.nSystemTime - IO.Input.PrintMarkSen[nDevIdx].nTsRising;			// Differenzzeit bilden
							// Exakte Position aus Zeitstempel errechnen
							{
								PmState.RisingDetection[nDevIdx].nPosFromTs = gEncoder.State.nAxisPos[BonDevPar[nDevIdx].nEncIdx] - (DINT) ((gEncoder.State.rVelocity[BonDevPar[nDevIdx].nEncIdx] * (REAL)PmState.RisingDetection[nDevIdx].nDiffTime) / nFACTOR_VELOCITY);
							}
							// Virtueller Geber wird verwendet ->	Position des Quellgebers zur sp�teren Berechnung der Druckmarkenl�nge
							//										(tats�chliche L�nge, da Druckmarkensensor im Geschwindigkeitsbereich von dem Quellgeber sitzt)
							if( BonDevPar[nDevIdx].bVirtualEncoderEnable == TRUE )
							{
								PmState.RisingDetection[nDevIdx].nVirtualSourceEncoderPosFromTs = gEncoder.State.nAxisPos[BonDevPar[nDevIdx].nSourceEncoderIdx] - (DINT) ((gEncoder.State.rVelocity[BonDevPar[nDevIdx].nSourceEncoderIdx] * (REAL)PmState.RisingDetection[nDevIdx].nDiffTime) / nFACTOR_VELOCITY);
							}
							PmState.RisingDetection[nDevIdx].bCntOk = 1;
							nOldRisingCnt[nDevIdx] = IO.Input.PrintMarkSen[nDevIdx].nRisingCnt;
						}
					}
					
					if (PmState.FallingDetection[nDevIdx].bCntOk == 0)
					{					
						if (IO.Input.PrintMarkSen[nDevIdx].nFallingCnt != nOldFallingCnt[nDevIdx])															// Fallende Flanke
						{
							PmState.FallingDetection[nDevIdx].nDiffTime = (INT)gSystem.State.nSystemTime - IO.Input.PrintMarkSen[nDevIdx].nTsFalling;		// Differenzzeit bilden
							// Exakte Position aus Zeitstempel errechnen
							{
								PmState.FallingDetection[nDevIdx].nPosFromTs = gEncoder.State.nAxisPos[BonDevPar[nDevIdx].nEncIdx] - (DINT)((gEncoder.State.rVelocity[BonDevPar[nDevIdx].nEncIdx] * (REAL)PmState.FallingDetection[nDevIdx].nDiffTime) / nFACTOR_VELOCITY);
							}
							// Virtueller Geber wird verwendet ->	Position des Quellgebers zur sp�teren Berechnung der Druckmarkenl�nge
							//										(tats�chliche L�nge, da Druckmarkensensor im Geschwindigkeitsbereich von dem Quellgeber sitzt)
							if( BonDevPar[nDevIdx].bVirtualEncoderEnable == TRUE )
							{
								PmState.FallingDetection[nDevIdx].nVirtualSourceEncoderPosFromTs = gEncoder.State.nAxisPos[BonDevPar[nDevIdx].nSourceEncoderIdx] - (DINT)((gEncoder.State.rVelocity[BonDevPar[nDevIdx].nSourceEncoderIdx] * (REAL)PmState.FallingDetection[nDevIdx].nDiffTime) / nFACTOR_VELOCITY);
							}
							PmState.FallingDetection[nDevIdx].bCntOk = 1;
							nOldFallingCnt[nDevIdx] = IO.Input.PrintMarkSen[nDevIdx].nFallingCnt;					
						}
					}
				
					// ------------------------------------------
					// Plausibilit�t der erkannten Flanken pr�fen
					// ------------------------------------------
					// Es wurde jeweils eine Position f�r die fallende und steigende Flanke gelatched
					// -> Pr�fen ob die Reihenfolge der Flanken plausibel ist (Positionen)
					if( (PmState.RisingDetection[nDevIdx].bCntOk == TRUE) &&
						(PmState.FallingDetection[nDevIdx].bCntOk == TRUE) )
					{
						// Positive Auswertung der Flanke
						if( gPar.ProductPar.PmSenPar.bPosNegDetection[nDevIdx] == FALSE )
						{
							// Reeler Geber wird verwendet
							if( BonDevPar[nDevIdx].bVirtualEncoderEnable == FALSE )
							{
								// Die gelatchte reele Geber-Position bei fallender Flanke ist kleiner als die der steigenden Flanke -> Die fallende Flanke wurde vor der steigenden Flanke gelatched 
								if( PmState.FallingDetection[nDevIdx].nPosFromTs - PmState.RisingDetection[nDevIdx].nPosFromTs < 0 )
								{
									// Fallende Flanke erneut auswerten
									PmState.FallingDetection[nDevIdx].bCntOk = FALSE;
								}
							}
							// Viruteller Geber wird verwendet
							else
							{
								// Die gelatchte virtuelle Geber-Position bei fallender Flanke ist kleiner als die der steigenden Flanke -> Die fallende Flanke wurde vor der steigenden Flanke gelatched 
								if( PmState.FallingDetection[nDevIdx].nVirtualSourceEncoderPosFromTs - PmState.RisingDetection[nDevIdx].nVirtualSourceEncoderPosFromTs < 0 )
								{
									// Fallende Flanke erneut auswerten
									PmState.FallingDetection[nDevIdx].bCntOk = FALSE;
								}
							}
							
						}
						// Negative Auswertung der Flanke
						else
						{
							// Reeler Geber wird verwendet
							if( BonDevPar[nDevIdx].bVirtualEncoderEnable == FALSE )
							{
								// Die gelatchte reele Geber-Position bei steigende Flanke ist kleiner als die der fallenden Flanke -> Die steigende Flanke wurde vor der fallenden Flanke gelatched 
								if( PmState.RisingDetection[nDevIdx].nPosFromTs - PmState.FallingDetection[nDevIdx].nPosFromTs < 0)
								{
									// steigende Flanke erneut auswerten
									PmState.RisingDetection[nDevIdx].bCntOk = FALSE;
								}
							}
							// Viruteller Geber wird verwendet
							else
							{
								// Die gelatchte virtuelle Geber-Position bei steigende Flanke ist kleiner als die der fallenden Flanke -> Die steigende Flanke wurde vor der fallenden Flanke gelatched 
								if( PmState.RisingDetection[nDevIdx].nVirtualSourceEncoderPosFromTs - PmState.FallingDetection[nDevIdx].nVirtualSourceEncoderPosFromTs < 0)
								{
									// steigende Flanke erneut auswerten
									PmState.RisingDetection[nDevIdx].bCntOk = FALSE;
								}
							}
						}
					}
					
					if ((PmState.RisingDetection[nDevIdx].bCntOk == TRUE) &&
						(PmState.FallingDetection[nDevIdx].bCntOk == TRUE))
					{
						FifoEntry[nDevIdx].bEditProdukt = FALSE;
						FifoEntry[nDevIdx].bProductDetected = FALSE;
						// Ermittlung der Druckmarkenl�nge
						if (gPar.ProductPar.PmSenPar.bPosNegDetection[nDevIdx] == FALSE)														// Pos. Auswertung der Flanke
						{
							// Reeler Geber wird verwendet: Errechnen der Druckmarkenl�nge �ber die ermittelten Positionen des verwendeten Gebers (reeler Geber)
							if( BonDevPar[nDevIdx].bVirtualEncoderEnable == FALSE )
							{
								gPrintMarkSensor.State.nPmLen[nDevIdx] = PmState.FallingDetection[nDevIdx].nPosFromTs - PmState.RisingDetection[nDevIdx].nPosFromTs;
							}
							// Viruteller Geber wird verwendet: Errechnen der Druckmarkenl�nge �ber die ermittelten Positionen des Quellgebers der dem virtuellen Geber zugeordnet ist
							else
							{
								gPrintMarkSensor.State.nPmLen[nDevIdx] = PmState.FallingDetection[nDevIdx].nVirtualSourceEncoderPosFromTs - PmState.RisingDetection[nDevIdx].nVirtualSourceEncoderPosFromTs;
							}
						}
						else
						{
							// Reeler Geber wird verwendet: Errechnen der Druckmarkenl�nge �ber die ermittelten Positionen des verwendeten Gebers (reeler Geber)
							if( BonDevPar[nDevIdx].bVirtualEncoderEnable == FALSE )
							{
								gPrintMarkSensor.State.nPmLen[nDevIdx] = PmState.RisingDetection[nDevIdx].nPosFromTs - PmState.FallingDetection[nDevIdx].nPosFromTs;
							}
							// Viruteller Geber wird verwendet: Errechnen der Druckmarkenl�nge �ber die ermittelten Positionen des Quellgebers der dem virtuellen Geber zugeordnet ist
							else
							{
								gPrintMarkSensor.State.nPmLen[nDevIdx] = PmState.RisingDetection[nDevIdx].nVirtualSourceEncoderPosFromTs - PmState.FallingDetection[nDevIdx].nVirtualSourceEncoderPosFromTs;
							}
						}
						
						if (gPrintMarkSensor.State.nPmLen[nDevIdx] > 0)
						{
							for( nPrintMarkIdx = 0; nPrintMarkIdx <= nIDX_PRINT_MARK_MAX; nPrintMarkIdx++ )
							{
								if ((gPrintMarkSensor.State.nPmLen[nDevIdx] < (gPar.MachinePar.PrintMarkPar.nPrintMarkLen[nPrintMarkIdx] + gPar.MachinePar.PrintMarkPar.nTolerance)) &&
									(gPrintMarkSensor.State.nPmLen[nDevIdx] > (gPar.MachinePar.PrintMarkPar.nPrintMarkLen[nPrintMarkIdx] - gPar.MachinePar.PrintMarkPar.nTolerance)) &&
									(gPar.MachinePar.PrintMarkPar.nPrintMarkLen[nPrintMarkIdx] > 0))
								{

									// -> gelatchte Position des Druckmarkensensors in FIFO �bernehmen
									// Positive Auswertung der Flanke
									if (gPar.ProductPar.PmSenPar.bPosNegDetection[nDevIdx] == FALSE)
									{								
										FifoEntry[nDevIdx].nStartPosFromTs = PmState.RisingDetection[nDevIdx].nPosFromTs;
										// Verstrichene Zeit seit dem erkennen der positiven Flanke des Druckmarkensensors
										nDiffTime = (INT)gSystem.State.nSystemTime - IO.Input.PrintMarkSen[nDevIdx].nTsRising;
									}
									// Negative Auswertung der Flanke
									else
									{
										FifoEntry[nDevIdx].nStartPosFromTs = PmState.FallingDetection[nDevIdx].nPosFromTs;
										// Verstrichene Zeit seit dem erkennen der fallenden Flanke des Druckmarkensensors
										nDiffTime = (INT)gSystem.State.nSystemTime - IO.Input.PrintMarkSen[nDevIdx].nTsFalling;
									}

									// ----- Druckmarkensensor ist hinter dem Klebekopf angebracht -----
									// d.h. das Produkt welches erkannt wird, ist schon unter dem Klebekopf hindurchgefahren und dieses kann nicht mehr geklebt werden. Ist das Produkt ein Endlos-Papier, dann kann
									// anhand dieser Druckmarke �ber die Produktl�nge auf die Position des folgenden x-ten Produktes geschlossen werden und dieses kann geklebt werden.
									
									// Der Abstand zwischen Druckmarkensensor und Klebekopf ist negativ
									// -> Der Druckmarkensensor ist nicht wie �blich vor dem Klebekopf, sondern sitzt hinter diesem
									if( BonDevPar[nDevIdx].nSensorOffset < 0 )
									{
										// Die Betriebsart des Druckmarkensensors muss "Druckmarke erkennen und Position erfassen" sein
										if( BonDevPar[nDevIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR )
										{
											// Den negativen Sensorabstand in einen Absolutwert wandeln
											NegativeSensorOffset.nNegativeSensorOffset = -BonDevPar[nDevIdx].nSensorOffset;
											// Anhand der Produktl�nge errechnen, wie viele ganzzahlige sich zwischen Klebekopf und Druckmarkensensor befinden
											NegativeSensorOffset.nProductsBetweenBonDev_Sensor = NegativeSensorOffset.nNegativeSensorOffset / BonDevPar[nDevIdx].nProductLen;
											// Die L�nge des Teiles des Produktes in [1/100 mm] welcher nicht mehr ganz zwischen Klebekopf und Sensor passt
											NegativeSensorOffset.nProductRestBetweenBonDev_Sensor = NegativeSensorOffset.nNegativeSensorOffset % BonDevPar[nDevIdx].nProductLen;
											// Die Produkte passen nicht ganzzahlig in den Abstand zwischen Klebekopf und Sensor -> Es bleibt ein Rest �brig
											if( NegativeSensorOffset.nProductRestBetweenBonDev_Sensor > 0 )
											{
												// Produktl�nge in [1/100 mm] welche nicht mehr in den Abstand zwischen Klebekopf und Sensor passt -> Steht �ber den Klebekopf hinaus
												NegativeSensorOffset.nProductRest = BonDevPar[nDevIdx].nProductLen - NegativeSensorOffset.nProductRestBetweenBonDev_Sensor;
											}
											// Die Produkte passen ganzzahlig in den Abstand zwischen Klebekopf und Sensor
											else
											{
												// Das letzte Produkt schlie�t b�ndig mit dem Klebekopf ab
												NegativeSensorOffset.nProductRest = 0;
											}
											
											NegativeSensorOffset.nNegativeOffsetToBonDev = NegativeSensorOffset.nProductRest;
											
											for( nProductCnt = 0; nProductCnt < 10; nProductCnt++ )
											{	
												if( NegativeSensorOffset.nNegativeOffsetToBonDev < ((100/*mm*/ * rFACTOR_MM_TO_AXIS_UNITS) + BonDevPar[nDevIdx].nPrintMarkProductOffset) )
												{
													// Die Produktl�nge so lange aufaddieren, bis der nNegativeOffsetToBonDev mindestens 100 mm + Druckmarkenoffset gro� ist
													NegativeSensorOffset.nNegativeOffsetToBonDev += BonDevPar[nDevIdx].nProductLen;
												}
												else
												{
													// Schleife hier verlassen
													break;
												}
											}
											// Offset errechnen, der auf die gelatchte Position addiert wird um an der richtigen Stelle des x-ten nachfolgenden Produktes kleben zu k�nnen	
											NegativeSensorOffset.nNegativePrintMarkPositionOffset = NegativeSensorOffset.nNegativeSensorOffset + NegativeSensorOffset.nNegativeOffsetToBonDev;
											// Position des x-ten zu klebenden Produktes durch addieren des Offsets ermitteln
											FifoEntry[nDevIdx].nStartPosFromTs += NegativeSensorOffset.nNegativePrintMarkPositionOffset;
										}
										else
										{
											//// ToDo MZ: Fehler ausgeben
										}
									}
									
									// Der Druckmarkensensor wird in kombination mit einem Trigger-Sensor verwendet
									if( BonDevPar[nDevIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
									{
										// Position des Triggersensor-Gebers im Moment der Druckmarkenerkennung errechnen und in Fifo eintragen (Diese wird f�r die Druckmarkenzuordnung im Task "TriggerSensor" ben�tigt)
										// Die tats�chliche Klebeposition wird bei dieser Betriebsart vom dazugeh�rigen Trigger-Sensor erfasst (Task "TriggerSensor").
										FifoEntry[nDevIdx].nPositionProductBegins = gEncoder.State.nAxisPos[BonDevPar[nDevIdx].nIdxTriggerEncoder] -
																					(DINT)((gEncoder.State.rVelocity[BonDevPar[nDevIdx].nIdxTriggerEncoder] * (REAL)nDiffTime) / nFACTOR_VELOCITY);
										// Um den Beginn des Produktes zu erhalten, muss der Druckmarken-Produkt-Offset von der ermittelten Position abgezogen werden
										FifoEntry[nDevIdx].nPositionProductBegins -= BonDevPar[nDevIdx].nPrintMarkProductOffset;
										// Druckmarken- und Triggersensor haben unterschiedliche Geber als Positionsbezugsquellen
										if( BonDevPar[nDevIdx].nEncIdx != BonDevPar[nDevIdx].nIdxTriggerEncoder )
										{
											// Wegstrecke, die das Produkt mit der Geschwindigkeit des Druckmarkensensor-Gebers zur�cklegt
											nDistanceWithPrintMarkVelocity[nDevIdx] = BonDevPar[nDevIdx].nDistancePrintMark_VelocityJump  + BonDevPar[nDevIdx].nProductLen - BonDevPar[nDevIdx].nPrintMarkProductOffset;
											// Verh�ltnis der aktuellen Geschwindigkeiten des Triggersensor- und Druckmarkensensor-Gebers
											rVelocityFactorTriggerPrintMark[nDevIdx] = gEncoder.State.rVelocity[BonDevPar[nDevIdx].nIdxTriggerEncoder] / gEncoder.State.rVelocity[BonDevPar[nDevIdx].nEncIdx];
											
											// ----- Distanz berechnen, mit der der Geschwindigkeitsunterschied kompensiert wird: -----
											// - wenn Geber Trigger-Sensor schneller -> Strecke die zur�ckgelegt werden muss verl�ngern (Kompensationsstrecke ist positiv)(versch. der Position Richtung Trigger)
											// - wenn Geber Druckmarken-Sensor schneller -> Strecke die zur�ckgelegt werden muss verk�rzen ( Kompensationsstrecke ist negativ )
											nDistanceVelocityCompensation[nDevIdx] = (DINT)(rVelocityFactorTriggerPrintMark[nDevIdx] * nDistanceWithPrintMarkVelocity[nDevIdx]) - nDistanceWithPrintMarkVelocity[nDevIdx];
											
											// ----- Distanz f�r Geschwindigkeitskompensation mit der aktuellen Position des Trigger-Gebers verrechnen -----
											// Im Task "TriggerSensor" wird eine Differenz aus dieser gerechneten Position und der aktuellen Position des Gebers gebildet. Ist diese Differenz gleich der Abstand
											// zwischen Druckmarken- und Triggersensor (+/- Toleranzfenster 0,5*Produktl�nge) hat das Produkt den Triggersensor erreicht.
											// -> Um den Geschwindigkeitskompensationsweg hier korrekt einzubeziehen, ist es bedingt durch die Differenzbildung im Task "TriggerSensor" n�tig, diese hier zu addieren.
											FifoEntry[nDevIdx].nPositionProductBegins += nDistanceVelocityCompensation[nDevIdx];
										}
										// Sollte zwar FALSE sein, wird hier aber nochmal auf FALSE gesetzt
										FifoEntry[nDevIdx].bProductPositionSetByTrigger = FALSE;
									}
									FifoEntry[nDevIdx].nPmIdx = nPrintMarkIdx;
									// Eintrag mit den entsprechenden Werten f�r diese erkannte Druckmarke in Fifo erstellen
									BrbFifoIn((BrbMemListManagement_Typ*)&gFifoManPmSen[nDevIdx], (UDINT)&FifoEntry[nDevIdx]);
									// Flags zur�cksetzen
									PmState.RisingDetection[nDevIdx].bCntOk = 0;
									PmState.FallingDetection[nDevIdx].bCntOk = 0;
									
									// Die erfasste Druckmarke konnte einer konfigurierten zugeordnet werden -> Schleife hier verlassen
									break;
								}
								
								// Die erfasste Druckmarke konnte keiner konfigurierten zugeordnet werden
								if( nPrintMarkIdx == nIDX_PRINT_MARK_MAX )
								{
									// Flags zur�cksetzen
									PmState.RisingDetection[nDevIdx].bCntOk = 0;
									PmState.FallingDetection[nDevIdx].bCntOk = 0;
								}
							}
						}
						else
						{	
//							rPmLen = gPrintMarkSensor.State.nPmLen[nDevIdx] / rFACTOR_MM_TO_AXIS_UNITS;
//							brsmemset((UDINT)&sPmLen, 0, sizeof(sPmLen));
//							ftoa(rPmLen, (UDINT)&sPmLen);
//							AppSetEvent(eEVT_ERR_INVALID_PRINT_MARK_LEN, 0, sPmLen, (nDevIdx + 1), &gEventManagement);					// Meldung absetzen 
//							AppResetEvent(eEVT_ERR_INVALID_PRINT_MARK_LEN, &gEventManagement);
//							PmState.bError[nDevIdx] = 1;							

							PmState.RisingDetection[nDevIdx].bCntOk = 0;
							PmState.FallingDetection[nDevIdx].bCntOk = 0;					
						}
					}
				}
				else
				{
					nOldRisingCnt[nDevIdx] = IO.Input.PrintMarkSen[nDevIdx].nRisingCnt;
					nOldFallingCnt[nDevIdx] = IO.Input.PrintMarkSen[nDevIdx].nFallingCnt;
				}
			}
			else
			{
				nOldRisingCnt[nDevIdx] = IO.Input.PrintMarkSen[nDevIdx].nRisingCnt;
				nOldFallingCnt[nDevIdx] = IO.Input.PrintMarkSen[nDevIdx].nFallingCnt;		
			}
		}
		else
		{
			nOldRisingCnt[nDevIdx] = IO.Input.PrintMarkSen[nDevIdx].nRisingCnt;
			nOldFallingCnt[nDevIdx] = IO.Input.PrintMarkSen[nDevIdx].nFallingCnt;
			gPrintMarkSensor.State.bPmSenReady = 0;		
		}
		
		// ---------- Wenn Position im Fifo den Auftragekopf passiert, den Positionswert aus Fifo l�schen ----------
		// Betriebsart Druckmarkenerkennung und gleichzeitig Position ermitteln
		if( BonDevPar[nDevIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR )
		{
			if (gFifoManPmSen[nDevIdx].nEntryCount > 0)
			{
				BrbMemListGetEntry((BrbMemListManagement_Typ*)&gFifoManPmSen[nDevIdx], 0, (UDINT)&FirstEntry[nDevIdx]);
				if( (gEncoder.State.nAxisPos[BonDevPar[nDevIdx].nEncIdx] - FirstEntry[nDevIdx].nStartPosFromTs) > ((DINT)nMAX_SENSOR_DISTANCE) )
				{
					BrbFifoOut((BrbMemListManagement_Typ*)&gFifoManPmSen[nDevIdx], (UDINT)&FirstEntry[nDevIdx]);
					for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx++)
					{
						if ((gPar.ProductPar.BonDevPar[nBonDevIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_SENSOR) &&
							(gPar.ProductPar.BonDevPar[nBonDevIdx].nIdxSenSource == nDevIdx) &&
							(gPar.ProductPar.BonDevPar[nBonDevIdx].bBonDevEn == 1))
						{
							if (gBondingDev.State.nEntryCnt[nBonDevIdx] > 0)
							{
								gBondingDev.State.nEntryCnt[nBonDevIdx] -= 1;
							}
							if (gBondingDev.State.nCurrentFifoCnt[nBonDevIdx] > 0)
							{
								gBondingDev.State.nCurrentFifoCnt[nBonDevIdx] -= 1;
							}
							if (gBondingDev.State.nCurrentFifoCntFlag[nBonDevIdx] > 0)
							{
								gBondingDev.State.nCurrentFifoCntFlag[nBonDevIdx] -= 1;
							}					
							// Produkterkennung aktiv
							if ((gPar.ProductPar.BonDevPar[nBonDevIdx].ProductDetection.bProductDetExist == 1) &&
							(gBondingDev.State.nProDetEntryCnt[nBonDevIdx] > 0))
							{
								gBondingDev.State.nProDetEntryCnt[nBonDevIdx] -= 1;
							}
						}
					}
				}
			}			
		}
		// Betriebsart Druckmarkenerkennung, jedoch Positionserkennung �ber nachfolgendem Trigger-Sensor
		else if( BonDevPar[nDevIdx].eIdxSenType == eSEN_TYPE_PRINT_MARK_TRIG_SENSOR )
		{
			// -> kein l�schen der Fifo-Eintr�ge in diesem Task! Dies geschieht im Task "TriggerSensor"
		}
	}
	
	// Fehler Quittierung
	if (AppIsEventAcknowledged(eEVT_ERR_INVALID_PRINT_MARK_LEN, &gEventManagement) == 1)
	{
		brsmemset((UDINT)&PmState.bError, 0, sizeof(PmState.bError));
	}
}

void _EXIT PrintMarkSensorEXIT(void)
{
}
