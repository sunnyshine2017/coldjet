(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: PrintMarkSensor
 * File: PrintMarkSensor.typ
 * Author: kusculart
 * Created: November 17, 2014
 ********************************************************************
 * Local data types of program PrintMarkSensor
 ********************************************************************)

TYPE
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_INIT_PM_SENSOR := 200,
		eSTEP_GET_BONDEV_PAR
		);
	PrintMarkSensorStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	IO_TYP : 	STRUCT 
		Input : IoInput_TYP;
	END_STRUCT;
	IoInput_TYP : 	STRUCT 
		PrintMarkSen : ARRAY[0..nDEV_INDEX_MAX]OF IoPrintMarkSen_TYP;
	END_STRUCT;
	IoPrintMarkSen_TYP : 	STRUCT 
		nRisingCnt : SINT;
		nTsRising : INT;
		nFallingCnt : SINT;
		nTsFalling : INT;
	END_STRUCT;
	BonDev_TYP : 	STRUCT 
		bEnPmSen : BOOL;
		nBonDevIdx : SINT; (*Index des diesem Druckmarkensensors zugeh�rigen Klebekopftes*)
		nEncIdx : SINT; (*Index des diesem Druckmarkensensors zugeh�rigen Gebers*)
		nIdxTriggerEncoder : SINT;
		nTriggerSensorIdx : SINT; (*Bei Betriebsart PrintMark_Trigger: Index des verkn�pften Triggersensor zur Produktvorderkantenerkennung*)
		bVirtualEncoderEnable : BOOL; (*Der zugeh�rige Geber dieses Druckmarkensensor ist ein virtueller Geber [FALSE = reeler Geber; TRUE = virtueller Geber]*)
		nSourceEncoderIdx : USINT; (*Bei virtuellem Geber: Quellgeberindex des virtuellen Gebers*)
		eIdxSenType : SensorType_ENUM; (*Betriebsart des Druckmarkensensors*)
		nProductLen : DINT; (*Produktl�nge in [1/100 mm] (Achseinheiten)*)
		nPrintMarkProductOffset : DINT; (*Abstand Produktbeginn zu Druckmarke in [1/100 mm] (Achseinheiten)*)
		nSensorOffset : DINT; (*Sensorabstand in [1/100 mm] (Achseinheiten)*)
		nDistancePrintMark_VelocityJump : REAL; (*Bei PrintMark_Trigger: Distanz in [1/100 mm] vom Druckmarken-Sensor bis zu der Stelle, an der sich die Geschwindigkeit ver�ndert (wird nur ben�tigt, wenn der Geber-Index des Druckmarkensensors und der des Triggersensors nicht gleich sind!)*)
	END_STRUCT;
	PmState_TYP : 	STRUCT 
		RisingDetection : ARRAY[0..nDEV_INDEX_MAX]OF PmDetection_TYP;
		FallingDetection : ARRAY[0..nDEV_INDEX_MAX]OF PmDetection_TYP;
		bError : ARRAY[0..nDEV_INDEX_MAX]OF BOOL;
	END_STRUCT;
	PmDetection_TYP : 	STRUCT 
		nPosFromTs : DINT;
		nVirtualSourceEncoderPosFromTs : DINT; (*nur bei virutellem Geber: Um die tats�chliche Druckmarkenl�nge (ohne Faktor des virtuellen Gebers) ermitteln zu k�nnen, wird die Position des Quellgebers verwendet.*)
		nDiffTime : INT;
		bCntOk : BOOL;
	END_STRUCT;
END_TYPE
