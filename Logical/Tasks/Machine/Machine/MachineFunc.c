/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Machine
 * File: MachineFunc.c
 * Author: kusculart
 * Created: November 11, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <MachineFunc.h>


// Resetten der Warnung eEVT_WAR_TANK_EMPTY_STOP_DELAY_0 + nTankIdx wenn n�tig
void ResetWarningTankEmptyStopDelay( USINT nTankIdx )
{
	// Timer-Funktion zur�cksetzen -> keine Abschaltung der Automatik nach Zeitablauf
	TON_TankEmptyStopDelay[nTankIdx].IN = FALSE;
	// Ausschaltzeitpunkt an dem die Automatik abgeschaltet werden soll l�schen
	dtSystemStopTimeTankEmpty[nTankIdx] = 0;
	// Zeitpunkt zu dem die Meldung Tank leer gesetzt wurde l�schen
	dtCurrentTimeTankEmpty[nTankIdx] = 0;
	
	// Warnung wurde bereits gesetzt
	if( bEventSetTankEmptyStopDelay[nTankIdx] == TRUE )
	{
		// Flag zur�cksetzen
		bEventSetTankEmptyStopDelay[nTankIdx] = FALSE;
		// Meldung zur�cksetzen
		AppResetEvent( eEVT_WAR_TANK_EMPTY_STOP_DELAY_0 + nTankIdx, &gEventManagement );
	}
}

// Diese Funktion errechnet je nach Konfiguration den Abschaltzeitpunkt des Automatik-Betriebes bzw. schaltet sofort ab
void HandleTankEmptyStopDelay( USINT nTankIdx, SINT nTankEmptyStopDelay  )
{
	// Automatik ist aktiv
	if( gMainLogic.State.bAutomatic == TRUE )
	{
		// Bei einerm Wert f�r nTankEmptyStopDelay kleiner Null wird die Automatik bei leerem Tank nicht gestoppt
		if( nTankEmptyStopDelay < 0 )
		{
			// keine Aktion
		}
		// Wartezeit bist der Automatik-Betrieb abgeschaltet wird ist konfiguriert
		else if( nTankEmptyStopDelay > 0 )
		{
			// Timer bis die Automatik abgeschaltet wird einschalten und Konfigurieren
			TON_TankEmptyStopDelay[nTankIdx].IN = TRUE;
			// Dauer bis Abschaltung erfolgt in Millisekunden berechnen
			TON_TankEmptyStopDelay[nTankIdx].PT = nTankEmptyStopDelay * 60 /*Sekunden*/ * 1000 /*Millisekunden*/;
			
			// Aktuelle Systemzeit ermitteln
			GetCurrentTimeTankEmpty[nTankIdx].enable = TRUE;
			DTGetTime( &GetCurrentTimeTankEmpty[nTankIdx] );
			dtCurrentTimeTankEmpty[nTankIdx] = GetCurrentTimeTankEmpty[nTankIdx].DT1;
			
			// Abschaltzeitpunkt errechnen
			dtSystemStopTimeTankEmpty[nTankIdx] = GetCurrentTimeTankEmpty[nTankIdx].DT1 + nTankEmptyStopDelay * 60 /*Sekunden*/;
			
			// Abschaltzeitpunkt als String im Format [hh:mm] ermitteln, um diesen Sp�ter mit der Fehlermeldung in der Visu anzuzeigen
			BrbGetTimeTextDt( dtSystemStopTimeTankEmpty[nTankIdx], &sSystemStopTimeTankEmpty[nTankIdx][0], sizeof( sSystemStopTimeTankEmpty[nTankIdx] ), "hh:MM" );
			
		}
		// Null Minuten warten bevor der Automatik-Betrieb abgeschaltet wird -> Sofortiges stoppen des Systems
		else if( nTankEmptyStopDelay == 0 )
		{
			
			// Automatik stoppen
			gMainLogic.DirectBox.bStop = TRUE;
		}
	}
}
