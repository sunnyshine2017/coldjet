/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Machine
 * File: Machine.c
 * Author: kusculart
 * Created: November 11, 2014
 ********************************************************************
 * Implementation of program Machine
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <MachineFunc.h>

void _INIT MachineINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gMachine, 0, sizeof(gMachine));
	gMachine.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	bCalcPos = 0;
	
	// Initialisierung der F�llstandssensoren
	// Nur f�r Simulation. Sind diese an IOs gemappt, werden diese sofort mit den richtigen Werten �berschrieben )
	IO.Input.bFillLvlGlue = TRUE;
	IO.Input.bFillLvlSoft = TRUE;
	IO.Input.bFillLvlClean = TRUE;
	
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
	
	
	// Intialising parameters for inbuilt function AsWeigh
	
	fb_WGHscale.enable=1;
	fb_WGHscale.t_measure=1;
	
	//Intialising parameters to Pointer to WGHscale

	pWGHscalePara.ref1_raw=3000;
	pWGHscalePara.ref1_std=1000;
	pWGHscalePara.ref2_raw=23700;
	pWGHscalePara.ref2_std=31500;
	
	//Intialising parametrs for inbuilt function MTMoving Average Filter
	
	fb_MTFilterMovAvg.Enable=1;
	fb_MTFilterMovAvg.WindowLength=7000;
}

void _CYCLIC MachineCYCLIC(void)
{	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			//if(gParHandling.State.bParValid == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gMachine.CallerBox, sizeof(gMachine.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			if(gMachine.CallerBox.bDummy == 1)
			{
				Step.eStepNr = eSTEP_CMD1;
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		case eSTEP_CMD1:
			strcpy(Step.sStepText, "eSTEP_CMD1");
			Step.eStepNr = eSTEP_CMD1_FINISHED;
			break;

		case eSTEP_CMD1_FINISHED:
			strcpy(Step.sStepText, "eSTEP_CMD1_FINISHED");
			BrbClearCallerBox((UDINT)&gMachine.CallerBox, sizeof(gMachine.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;

	}
	
	// ---------- Bildverarbeitung ----------
	if (gPar.MachinePar.MachineModule.bImgProcessingEn == 1)
	{
		fbPosImgProc.CLK = IO.Input.bImgProcErr;
		R_TRIG(&fbPosImgProc); 
		if (fbPosImgProc.Q == 1) 
		{
			gParPermanent.nImgProcessingErrCnt += 1;																		// Z�hler inkrementieren
			AppSetEvent(eEVT_ERR_IMAGE_PROCESSING, 0, "", (DINT)gParPermanent.nImgProcessingErrCnt, &gEventManagement);		// Meldung ausl�sen
			AppResetEvent(eEVT_ERR_IMAGE_PROCESSING, &gEventManagement);
		}
		if (gMachine.DirectBox.bResetCntImgProc == 1)
		{
			gParPermanent.nImgProcessingErrCnt = 0;
			gMachine.DirectBox.bResetCntImgProc = 0;
		}
	}

	// ---------- F�llstandsmessung ----------
	// *******************************************************************************
	// 									Klebstoff-Tank
	// *******************************************************************************
	if (gPar.MachinePar.TankType.bTankGlueEn == 1)
	{
		// Klebstofftank ist gef�llt
		if (IO.Input.bFillLvlGlue == 1)
		{
			// Meldung "Klebstofftank leer" steht an und kann zur�ckgesetzt werden
			if( bEventSetTankIsEmpty[eGLUE_TANK_IDX] == TRUE )
			{
				bEventSetTankIsEmpty[eGLUE_TANK_IDX] = FALSE;
				// Fehlermeldung zur�cksetzen
				AppResetEvent( eEVT_ERR_GLUE_TANK_EMPTY, &gEventManagement );	
				// Warnung zur�cksetzen
				AppResetEvent( eEVT_WAR_GLUE_TANK_EMPTY, &gEventManagement );
				// Resetten der Warnung eEVT_WAR_TANK_EMPTY_STOP_DELAY_0 wenn diese gesetzt ist
				ResetWarningTankEmptyStopDelay( eGLUE_TANK_IDX );
			}
			
			// Calculating the actual value of glue in the tank 
			
			fb_WGHscale.pWGHscalePara=&pWGHscalePara;
			// MTFilter function is used find the stable value of the analog input
			fb_MTFilterMovAvg.In= Analog_Input;
			MTFilterMovingAverage(&fb_MTFilterMovAvg);
	
			//Calculating the actual voltage
			//rActVoltage=(REAL)fb_MTFilterMovAvg.Out/10000;
	
	
			fb_WGHscale.raw_value=fb_MTFilterMovAvg.Out;
			WGHscale(&fb_WGHscale);
	
			// calculating the actual tank weight from the value returned by the function WGH Scale

			rTank_Weight=(REAL)fb_WGHscale.std_value/1000;
			gMachine.State.TankLvl.nFillLvlGlue =rTank_Weight;
			
		}
		// Klebstofftank ist leer
		else
		{
			// Fehlermeldung ist noch nicht abgesetzt worden
			if( bEventSetTankIsEmpty[eGLUE_TANK_IDX] == FALSE )
			{
				
				// Meldung "Klebstofftank leer" wurde abgesetzt
				bEventSetTankIsEmpty[eGLUE_TANK_IDX] = TRUE;
				
				// Laut Konfiguration soll bei leerem Klebstofftank eine Fehlermeldung abgesetzt werden
				if( gPar.MachinePar.TankType.nTankGlueStopDelay >= 0 )
				{
					// Fehlermeldung ausl�sen
					AppSetEvent( eEVT_ERR_GLUE_TANK_EMPTY, 0, "", 0, &gEventManagement );
					// Je nach Konfiguration den Abschaltzeitpunkt des Automatik-Betriebes errechnen bzw. sofort abschalten
					HandleTankEmptyStopDelay( eGLUE_TANK_IDX, gPar.MachinePar.TankType.nTankGlueStopDelay );
				}
				// Laut Konfiguration soll bei leerem Klebstofftank eine Warnung abgesetzt werden
				else
				{
					// Warnung ausl�sen
					AppSetEvent( eEVT_WAR_GLUE_TANK_EMPTY, 0, "", 0, &gEventManagement );
				}
			}
			
			// F�llstandsanzeige in der Visualisierung als leer (0%) anzeigen
			gMachine.State.TankLvl.nFillLvlGlue = 0;
		}
	}
	// Kein Klebstoff-Tank konfiguriert
	else
	{
		// Anstehende Fehlermeldungen k�nnen zur�ckgesetzt werden
		if( bEventSetTankIsEmpty[eGLUE_TANK_IDX] == TRUE )
		{
			bEventSetTankIsEmpty[eGLUE_TANK_IDX] = FALSE;
			// Fehlermeldung "Klebstofftank leer" zur�cksetzen
			AppResetEvent( eEVT_ERR_GLUE_TANK_EMPTY, &gEventManagement );
			// Warnung "Klebstofftank leer" zur�cksetzen
			AppResetEvent( eEVT_WAR_GLUE_TANK_EMPTY, &gEventManagement );
		}
		// Resetten der Warnung eEVT_WAR_TANK_EMPTY_STOP_DELAY_0 wenn diese gesetzt ist
		ResetWarningTankEmptyStopDelay( eGLUE_TANK_IDX );
		// F�llstandsanzeige in der Visualisierung als leer (0%) anzeigen
		gMachine.State.TankLvl.nFillLvlGlue = 0;
	}
	
	// *******************************************************************************
	// 									Softing-Tank
	// *******************************************************************************
	if (gPar.MachinePar.TankType.bTankSoftEn == 1)
	{
		// Softing-Tank ist gef�llt
		if (IO.Input.bFillLvlSoft == 1)
		{
			// Fehlermeldung steht an und kann zur�ckgesetzt werden
			if( bEventSetTankIsEmpty[eSOFTING_TANK_IDX] == TRUE )
			{
				bEventSetTankIsEmpty[eSOFTING_TANK_IDX] = FALSE;
				// Fehlermeldung "Softingtank leer" zur�cksetzen
				AppResetEvent( eEVT_ERR_SOFTING_TANK_EMPTY, &gEventManagement );
				// Warnung "Softingtank leer" zur�cksetzen
				AppResetEvent( eEVT_WAR_SOFTING_TANK_EMPTY, &gEventManagement );
				// Resetten der Warnung eEVT_WAR_TANK_EMPTY_STOP_DELAY_1 wenn diese gesetzt ist
				ResetWarningTankEmptyStopDelay( eSOFTING_TANK_IDX );
			}
			// F�llstandsanzeige in der Visualisierung als voll (100%) anzeigen
			
			
			gMachine.State.TankLvl.nFillLvlSoft = 100;
		}
		// Softing-Tank ist leer
		else
		{
			// Fehlermeldung ist noch nicht abgesetzt worden
			if( bEventSetTankIsEmpty[eSOFTING_TANK_IDX] == FALSE )
			{
				bEventSetTankIsEmpty[eSOFTING_TANK_IDX] = TRUE;
				// Laut Konfiguration soll bei leerem Softingtank eine Fehlermeldung abgesetzt werden
				if( gPar.MachinePar.TankType.nTankSoftStopDelay >= 0 )
				{
					// Fehlermeldung "Softingtank leer" absetzen
					AppSetEvent( eEVT_ERR_SOFTING_TANK_EMPTY, 0, "", 0, &gEventManagement );
					// Je nach Konfiguration den Abschaltzeitpunkt des Automatik-Betriebes errechnen bzw. sofort abschalten
					HandleTankEmptyStopDelay( eSOFTING_TANK_IDX, gPar.MachinePar.TankType.nTankSoftStopDelay );
				}
				// Laut Konfiguration soll bei leerem Softingtank eine Warnung abgesetzt werden
				else
				{
					// Warnung "Softingtank leer" absetzen
					AppSetEvent( eEVT_WAR_SOFTING_TANK_EMPTY, 0, "", 0, &gEventManagement );
				}
			}
			// F�llstandsanzeige in der Visualisierung als leer (0%) anzeigen
			
			gMachine.State.TankLvl.nFillLvlSoft = 0;
		}
	}
	// Kein Softing-Tank konfiguriert
	else
	{
		// Fehlermeldung steht an und kann zur�ckgesetzt werden
		if( bEventSetTankIsEmpty[eSOFTING_TANK_IDX] == TRUE )
		{
			bEventSetTankIsEmpty[eSOFTING_TANK_IDX] = FALSE;
			// Fehlermeldung Warnung "Softingtank leer" zur�cksetzen
			AppResetEvent( eEVT_ERR_SOFTING_TANK_EMPTY, &gEventManagement );
			// Warnung "Softingtank leer" zur�cksetzen
			AppResetEvent( eEVT_WAR_SOFTING_TANK_EMPTY, &gEventManagement );
		}
		// Resetten der Warnung eEVT_WAR_TANK_EMPTY_STOP_DELAY_1 wenn diese gesetzt ist
		ResetWarningTankEmptyStopDelay( eSOFTING_TANK_IDX );
		// F�llstandsanzeige in der Visualisierung als leer (0%) anzeigen
		gMachine.State.TankLvl.nFillLvlSoft = 0;
	}
	// *******************************************************************************
	// 								Reinigungsmittel-Tank
	// *******************************************************************************
	if (gPar.MachinePar.TankType.bTankCleanEn == 1)
	{
		// Reinigungsmittel-Tank ist gef�llt
		if (IO.Input.bFillLvlClean == 1)
		{
			// Fehlermeldung steht an und kann zur�ckgesetzt werden
			if( bEventSetTankIsEmpty[eCLEANING_TANK_IDX] == TRUE )
			{
				bEventSetTankIsEmpty[eCLEANING_TANK_IDX] = FALSE;
				// Fehlermeldung "Reinigungsmitteltank leer" zur�cksetzen
				AppResetEvent( eEVT_ERR_CLEANING_TANK_EMPTY, &gEventManagement );
				// Warnung "Reinigungsmitteltank leer" zur�cksetzen
				AppResetEvent( eEVT_WAR_CLEANING_TANK_EMPTY, &gEventManagement );
				// Resetten der Warnung eEVT_WAR_TANK_EMPTY_STOP_DELAY_2 wenn diese gesetzt ist
				ResetWarningTankEmptyStopDelay( eCLEANING_TANK_IDX );
			}
			// F�llstandsanzeige in der Visualisierung als voll (100%) anzeigen
			gMachine.State.TankLvl.nFillLvlClean = 100;
		}
		// Reinigungsmittel-Tank ist leer
		else
		{
			// Fehlermeldung ist noch nicht abgesetzt worden
			if( bEventSetTankIsEmpty[eCLEANING_TANK_IDX] == FALSE )
			{
				bEventSetTankIsEmpty[eCLEANING_TANK_IDX] = TRUE;
				// Laut Konfiguration soll bei leerem Reinigungsmitteltank eine Fehlermeldung abgesetzt werden
				if( gPar.MachinePar.TankType.nTankCleanStopDelay >= 0 )
				{
					// Fehlermeldung "Softingtank leer" absetzen
					AppSetEvent( eEVT_ERR_CLEANING_TANK_EMPTY, 0, "", 0, &gEventManagement );
					
					// Je nach Konfiguration den Abschaltzeitpunkt des Automatik-Betriebes errechnen bzw. sofort abschalten
					HandleTankEmptyStopDelay( eCLEANING_TANK_IDX, gPar.MachinePar.TankType.nTankCleanStopDelay );
				}
				// Laut Konfiguration soll bei leerem Reinigungsmitteltank eine Warung abgesetzt werden
				else
				{
					// Warnung "Softingtank leer" absetzen
					AppSetEvent( eEVT_WAR_CLEANING_TANK_EMPTY, 0, "", 0, &gEventManagement );
				}
			}
			// F�llstandsanzeige in der Visualisierung als leer (0%) anzeigen
			gMachine.State.TankLvl.nFillLvlClean = 0;
		}
	}
	else
	{
		// Fehlermeldung steht an und kann zur�ckgesetzt werden
		if( bEventSetTankIsEmpty[eCLEANING_TANK_IDX] == TRUE )
		{
			bEventSetTankIsEmpty[eCLEANING_TANK_IDX] = FALSE;
			// Fehlermeldung "Reinigungsmitteltank leer" zur�cksetzen
			AppResetEvent(eEVT_ERR_CLEANING_TANK_EMPTY, &gEventManagement);
			// Warnung "Reinigungsmitteltank leer" zur�cksetzen
			AppResetEvent(eEVT_WAR_CLEANING_TANK_EMPTY, &gEventManagement);
		}
		// Resetten der Warnung eEVT_WAR_TANK_EMPTY_STOP_DELAY_2 wenn diese gesetzt ist
		ResetWarningTankEmptyStopDelay( eCLEANING_TANK_IDX );
		// F�llstandsanzeige in der Visualisierung als leer (0%) anzeigen
		gMachine.State.TankLvl.nFillLvlClean = 0;
	}
	
	// *******************************************************************************
	// 				Ermitteln des Tanks mit dem fr�hesten Abschaltzeitpunkt
	// *******************************************************************************
	// Mindestens ein Tank mit verz�gerter Abschaltung meldet w�hrend des Automatik-Betriebs das der Tank leer ist
	bTankWithDelayedSystemStopEmpty = FALSE;
	// Schleife �ber alle Tanks
	for( i=0; i<eMAX_CNT_TANK_IDX; i++ )
	{
		// Tank mit diesem Index verursacht in k�rze eine Abschaltung des Automatikbetriebes
		if( dtSystemStopTimeTankEmpty[i] > 0 )
		{
			// Mindestens ein Tank mit verz�gerter Abschaltung meldet w�hrend des Automatik-Betriebs das der Tank leer ist
			bTankWithDelayedSystemStopEmpty = TRUE;
		}
	}
	
	// Ermitteln des Tanks mit dem fr�hesten Ausschaltzeitpunkt und Warnung absetzen
	if( bTankWithDelayedSystemStopEmpty == TRUE )
	{
		// Suche des Tank-Indexes mit der fr�hesten Ausschaltzeit bei Index 0 beginnen
		nShortestDelayTimeIdx = 0;

		// Schleife �ber alle Tanks
		for( i=0; i<eMAX_CNT_TANK_IDX; i++ )
		{
			// Der Ausschaltzeitpunkt des Tanks mit Index i ist gesetzt (gr��er 0)
			// UND dieser ist fr�her als der bisher gemerkte fr�heste Ausschaltzeitpunkt (Index nShortestDelayTimeIdx)
			if( (dtSystemStopTimeTankEmpty[i] > 0) &&
				(dtSystemStopTimeTankEmpty[i] < dtSystemStopTimeTankEmpty[nShortestDelayTimeIdx]) )
			{
				
				// Index mit der fr�hesten Ausschaltzeit merken
				nShortestDelayTimeIdx = i;
			}
			// Der Ausschaltzeitpunkt des Tanks mit Index i ist gesetzt (gr��er 0)
			else if( dtSystemStopTimeTankEmpty[i] > 0 )
			{
				// Der bisher gemerkte fr�heste Ausschaltzeitpunkt ist Null (m�glich beim ersten Durchlauf wenn nShortestDelayTimeIdx = 0 )
				if( dtSystemStopTimeTankEmpty[nShortestDelayTimeIdx] == 0 )
				{
					// Index mit der fr�hesten Ausschaltzeit merken
					nShortestDelayTimeIdx = i;
				}
			}
		}

		// Warnung eEVT_WAR_TANK_EMPTY_STOP_DELAY f�r den Tank mit dem fr�hesten Abschaltzeitpunkt setzen
		if( bEventSetTankEmptyStopDelay[nShortestDelayTimeIdx] == FALSE )
		{
			// Flag setzen, Meldung gesetzt
			bEventSetTankEmptyStopDelay[nShortestDelayTimeIdx] = TRUE;
								
			// Meldung ausl�sen
			AppSetEvent( eEVT_WAR_TANK_EMPTY_STOP_DELAY_0 + nShortestDelayTimeIdx, 0, sSystemStopTimeTankEmpty[nShortestDelayTimeIdx], 0, &gEventManagement );
			
			// Die aktiven Warnungen eEVT_WAR_TANK_EMPTY_STOP_DELAY der anderen Tanks zur�cksetzen
			for( i=0; i<eMAX_CNT_TANK_IDX; i++ )
			{
				if( (i != nShortestDelayTimeIdx) &&
					(bEventSetTankEmptyStopDelay[i] == TRUE) )
				{
					// Flag zur�cksetzen, Meldung gesetzt
					bEventSetTankEmptyStopDelay[i] = FALSE;
					
					// Meldung zur�cksetzen
					AppResetEvent( eEVT_WAR_TANK_EMPTY_STOP_DELAY_0 + i, &gEventManagement );
				}
			}
		}
	}

	// Automatik-Betrieb wurde gestoppt
	if( gMainLogic.State.bAutomatic == FALSE )
	{
		// Schleife �ber alle Tanks
		for( i=0; i<eMAX_CNT_TANK_IDX; i++ )
		{
			// Resetten der Warnung eEVT_WAR_TANK_EMPTY_STOP_DELAY_i wenn diese gesetzt ist
			ResetWarningTankEmptyStopDelay( i );		
		}
	}
	
	// Timer f�r Systemabschaltung zyklisch aufrufen
	for( i=0; i<eMAX_CNT_TANK_IDX; i++ )
	{
		TON( &TON_TankEmptyStopDelay[i] );
	}
	
	// Der fr�heste Ausschaltzeitpunkt wurder erreicht -> System abschalten (Automatik beenden)
	if( (TON_TankEmptyStopDelay[eGLUE_TANK_IDX].Q == TRUE) ||
		(TON_TankEmptyStopDelay[eSOFTING_TANK_IDX].Q == TRUE) ||
		(TON_TankEmptyStopDelay[eCLEANING_TANK_IDX].Q == TRUE) )
	{
		// Schleife �ber alle Tanks
		for( i=0; i<eMAX_CNT_TANK_IDX; i++ )
		{			
			// Resetten der Warnung eEVT_WAR_TANK_EMPTY_STOP_DELAY_i wenn diese gesetzt ist
			ResetWarningTankEmptyStopDelay( i );		
		}
		// Direkt-Kommando "Automatik-Betrieb stoppen" an Main-Logic senden
		gMainLogic.DirectBox.bStop = TRUE;
	}
	
	// Tank ist leer
	if (((gMachine.State.TankLvl.nFillLvlClean == 0) && (gPar.MachinePar.TankType.bTankCleanEn == 1)) ||
		((gMachine.State.TankLvl.nFillLvlGlue == 0) && (gPar.MachinePar.TankType.bTankGlueEn == 1)) ||
		((gMachine.State.TankLvl.nFillLvlSoft == 0) && (gPar.MachinePar.TankType.bTankSoftEn == 1)))
	{
		IO.Output.bTankEmpty = 1;
	}
	else
	{
		IO.Output.bTankEmpty = 0;
	}
	
	// ---------- Pr�fen ob ein Fehler anliegt ----------
	if( (gEventHandling.State.nErrorCount == 0) ||
			(IO.Output.bTankEmpty == 1) )
	{
		IO.Output.bError = 0;
	}
	else
	{
		IO.Output.bError = 1;
	}
	
	// ---------- Wurde BonDev Fehler quittiert ----------	
	if ((AppIsEventAcknowledged(eEVT_ERR_BONDING_DEVICE, &gEventManagement) == 1) ||
		(AppIsEventAcknowledged(eEVT_ERR_SENSOR_DISTANCE, &gEventManagement) == 1) ||
		(AppIsEventAcknowledged(eEVT_ERR_NEG_PRODUCT_DISTANCE, &gEventManagement) == 1) ||
		//// ToDo MZ: den Tats�chlichen Fehler aus BondingDev zur�cksetzen
		(AppIsEventAcknowledged(eEVT_ERR_VAL_CLOSE_COMP_TO_LARGE, &gEventManagement) == 1))
	{
		brsmemset((UDINT)&gBondingDev.State.bBonDevError, 0, sizeof(gBondingDev.State.bBonDevError));
		bCalcPos = 1;
	}
	if (bCalcPos == 1)
	{
		if (BrbSetCaller(&gBondingDev.CallerBox.Caller, eCALLERID_BONDING_DEVICE) == eBRB_CALLER_STATE_OK)
		{
			gBondingDev.CallerBox.bApplyPar = 1;									// Switch Positionen
			bCalcPos = 0;
		}
	}
}

void _EXIT MachineEXIT(void)
{
}
