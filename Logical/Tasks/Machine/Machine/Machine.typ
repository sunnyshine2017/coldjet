(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Machine
 * File: Machine.typ
 * Author: kusculart
 * Created: November 11, 2014
 ********************************************************************
 * Local data types of program Machine
 ********************************************************************)

TYPE
	Steps_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_CMD1 := 200,
		eSTEP_CMD1_FINISHED
		);
	Tank_Idx_ENUM : 
		(
		eGLUE_TANK_IDX, (*0*)
		eSOFTING_TANK_IDX,
		eCLEANING_TANK_IDX,
		eMAX_CNT_TANK_IDX
		);
	MachineStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : Steps_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	IO_TYP : 	STRUCT 
		Input : IoInput_TYP;
		Output : IoOutput_TYP;
	END_STRUCT;
	IoInput_TYP : 	STRUCT 
		bImgProcErr : BOOL; (*Bildverarbeitungssystem meldet einen Fehler*)
		bFillLvlSoft : BOOL; (*Füllstandsensor Softing*)
		bFillLvlClean : BOOL; (*Füllstandsensor Reinigungsmittel*)
		bFillLvlGlue : BOOL; (*Füllstandsensor Klebemittel*)
	END_STRUCT;
	IoOutput_TYP : 	STRUCT 
		bError : BOOL; (*Störung liegt an; Wird bei Tank leer nicht gesetzt.*)
		bTankEmpty : BOOL; (*Tank ist leer*)
	END_STRUCT;
END_TYPE
