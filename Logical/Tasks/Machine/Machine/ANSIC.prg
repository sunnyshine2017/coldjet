﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.1.4.390?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Task">Machine.c</File>
    <File Description="Lokale Datentypen des Tasks" Private="true">Machine.typ</File>
    <File Description="Lokale Variablen des Tasks" Private="true">Machine.var</File>
    <File Description="Hilfsfunktionen des Tasks">MachineFunc.c</File>
    <File Description="Prototypen der Hilfsfuntkionen">MachineFunc.h</File>
    <File Reference="true">\Logical\Global\Global.c</File>
  </Files>
</Program>