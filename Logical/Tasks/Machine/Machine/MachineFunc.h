/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Machine
 * File: MachineFunc.h
 * Author: kusculart
 * Created: November 11, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#include <string.h>
#include <Global.h>

// Prototypen
void ResetWarningTankEmptyStopDelay( USINT nTankIdx );
void HandleTankEmptyStopDelay( USINT nTankIdx, SINT nTankEmptyStopDelay  );

