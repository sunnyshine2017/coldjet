(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: Machine
 * File: G_Machine.typ
 * Author: kusculart
 * Created: November 11, 2014
 ********************************************************************
 * Global  data types of package Machine
 ********************************************************************)

TYPE
	MachineCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP;
		bDummy : BOOL;
	END_STRUCT;
	MachineDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		bResetCntImgProc : BOOL; (*Fehlermeldungszähler Bildverarbeitung zurücksetzen*)
	END_STRUCT;
	MachineParameter_TYP : 	STRUCT  (*Kommando-Paramter*)
		nDummy : UINT;
	END_STRUCT;
	MachineState_TYP : 	STRUCT  (*Rückmeldungen*)
		TankLvl : TankLvl_TYP;
	END_STRUCT;
	Machine_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : MachineCallerBox_TYP;
		DirectBox : MachineDirectBox_TYP;
		Par : MachineParameter_TYP;
		State : MachineState_TYP;
	END_STRUCT;
	TankLvl_TYP : 	STRUCT 
		nFillLvlGlue : REAL;
		nFillLvlSoft : USINT;
		nFillLvlClean : USINT;
	END_STRUCT;
END_TYPE
