/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: AxisControl
 * File: AxisControlFunc.c
 * Author: kusculart
 * Created: November 24, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <AxisControlFunc.h>
#include <Global.h>

void SetAxisErrorEvent(UINT nErrorId, STRING* pAxisName, DINT nHeadIndex)
{
	if(nErrorId == 4007)
	{
		// Schleppfehler
		AppSetEvent(eEVT_ERR_AXIS_LAG_ERROR, nErrorId, pAxisName, (nHeadIndex + 1), &gEventManagement);
		AppResetEvent(eEVT_ERR_AXIS_LAG_ERROR, &gEventManagement);
	}
	else if(nErrorId == 5026)
	{
		// Überschreitung Geschwindigkeits-Grenze
		AppSetEvent(eEVT_ERR_AXIS_VELOCITY_LIMIT, nErrorId, pAxisName, (nHeadIndex + 1), &gEventManagement);
		AppResetEvent(eEVT_ERR_AXIS_VELOCITY_LIMIT, &gEventManagement);
	}
	else if(nErrorId == 5027)
	{
		// Überschreitung Beschleunigungs-Grenze
		AppSetEvent(eEVT_ERR_AXIS_ACCELERATION_LIMIT, nErrorId, pAxisName, (nHeadIndex + 1), &gEventManagement);
		AppResetEvent(eEVT_ERR_AXIS_ACCELERATION_LIMIT, &gEventManagement);
	}
	else if(nErrorId == 29209)
	{
		// PlcOpen: Antrieb im Fehler -Zustand
		AppSetEvent(eEVT_ERR_AXIS_PLCOPEN_ERROR, nErrorId, pAxisName, (nHeadIndex + 1), &gEventManagement);
		AppResetEvent(eEVT_ERR_AXIS_PLCOPEN_ERROR, &gEventManagement);
	}
	else if(nErrorId == 29215)
	{
		// PlcOpen: Diskrete Bewegung nicht möglich
		AppSetEvent(eEVT_ERR_AXIS_DISCRETE_MOTION, nErrorId, pAxisName, (nHeadIndex + 1), &gEventManagement);
		AppResetEvent(eEVT_ERR_AXIS_DISCRETE_MOTION, &gEventManagement);
	}
	else if(nErrorId == 29217)
	{
		// PlcOpen: Ungültiger Eingabeparameter
		AppSetEvent(eEVT_ERR_AXIS_INVALID_FB_PARAM, nErrorId, pAxisName, (nHeadIndex + 1), &gEventManagement);
		AppResetEvent(eEVT_ERR_AXIS_INVALID_FB_PARAM, &gEventManagement);
	}
	else
	{
		// Sonstige Fehler
		AppSetEvent(eEVT_ERR_AXIS, nErrorId, pAxisName, (nHeadIndex + 1), &gEventManagement);
		AppResetEvent(eEVT_ERR_AXIS, &gEventManagement);
	}
}


BOOL AllAxesPowerOn( void )
{
	USINT	nBonDevIdx;
	
	for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++ )
	{
	
		if( ((gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == TRUE) &&
			 (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerStatus == FALSE)) ||
			((gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == TRUE) &&
			 (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerStatus == FALSE)) )
		{
			return FALSE;
		}
	}
	return TRUE;
}

BOOL AllAxesHomed( void )
{
	USINT	nBonDevIdx;
	
	for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++ )
	{
	
		if( ((gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == TRUE) &&
			 (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.HomingOk == FALSE)) ||
			((gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == TRUE) &&
			 (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.HomingOk == FALSE)) )
		{
			return FALSE;
		}
	}
	return TRUE;
}



