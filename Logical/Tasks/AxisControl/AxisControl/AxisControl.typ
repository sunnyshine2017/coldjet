(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: AxisControl
 * File: AxisControl.typ
 * Author: kusculart
 * Created: November 24, 2014
 ********************************************************************
 * Local data types of program AxisControl
 ********************************************************************)

TYPE
	AxisControl_ENUM : 
		( (*Schritte*)
		eSTEP_INIT := 1,
		eSTEP_INIT_WAIT_FOR_PAR_VALID,
		eSTEP_INIT_FINISHED,
		eSTEP_WAIT_FOR_COMMAND := 100,
		eSTEP_NO_AXIS_EXISTS,
		eSTEP_POWER_ON := 200,
		eSTEP_POWER_OFF,
		eSTEP_HOMING_ALL,
		eSTEP_HOMING_ALL_ACTIVE,
		eSTEP_INIT_ALL_AXES_LIMITS,
		eSTEP_MOVE_ALL_AXES_TO_WORK_POS,
		eSTEP_HOMING,
		eSTEP_STOP,
		eSTEP_MOVE_ABS,
		eSTEP_MOVE_ADD_POS,
		eSTEP_MOVE_ADD_NEG,
		eSTEP_JOG_POS,
		eSTEP_JOG_NEG
		);
	AxisControlStep_TYP : 	STRUCT  (*Schrittketten-Struktur*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX];
		eStepNr : AxisControl_ENUM;
		bInitDone : BOOL;
	END_STRUCT;
	Error_type : 	STRUCT 
		nHorizontalErrId : UINT;
		bHorizontalErr : BOOL;
		bHorizontalCtrlErr : BOOL;
		nVerticalErrId : UINT;
		bVerticalErr : BOOL;
		bVerticalCtrlErr : BOOL;
		bMsgDoneControllerV : BOOL;
		bMsgDoneControllerH : BOOL;
		bMsgDoneH : BOOL;
		bMsgDoneV : BOOL;
		bInfoDoneH : BOOL;
		bInfoDoneV : BOOL;
	END_STRUCT;
	AuxVar_type : 	STRUCT 
		bControllerOff : BOOL;
		bJogPos : BOOL;
		bJogPosActive : BOOL;
		bJogNeg : BOOL;
		bJogNegActive : BOOL;
		bStop : BOOL;
		bHoming : BOOL;
		bMoveAbs : BOOL;
		HomingDone : ARRAY[0..nDEV_INDEX_MAX]OF Homing_type;
		Move_Add_Pos : ARRAY[0..nDEV_INDEX_MAX]OF Move_Add_Type;
		Move_Add_Neg : ARRAY[0..nDEV_INDEX_MAX]OF Move_Add_Type;
	END_STRUCT;
	Homing_type : 	STRUCT 
		bHomingVertical : BOOL;
		bHomingHorizontal : BOOL;
	END_STRUCT;
	Move_Add_Type : 	STRUCT 
		Horizontal : BOOL;
		Vertical : BOOL;
	END_STRUCT;
END_TYPE
