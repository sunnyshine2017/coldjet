/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: AxisControl
 * File: AxisControlFunc.h
 * Author: kusculart
 * Created: November 24, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#include <string.h>

void SetAxisErrorEvent(UINT nErrorId, STRING* pAxisName, DINT nHeadIndex);
BOOL AllAxesPowerOn( void );
BOOL AllAxesHomed( void );
