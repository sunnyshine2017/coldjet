/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: AxisControl
 * File: AxisControl.c
 * Author: kusculart
 * Created: November 24, 2014
 ********************************************************************
 * Implementation of program AxisControl
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <AxisControlFunc.h>
#include "global.h"

void _INIT AxisControlINIT(void)
{
	// Initialisierung der Task-Struktur
	brsmemset((UDINT)&gAxisControl, 0, sizeof(gAxisControl));
	gAxisControl.CallerBox.Caller.nCallerId = eBRB_CALLER_STATE_NOT_READY;

	// Initialisierung Task
	// In Konfigurationen in denen die Schrittmotoren simuliert verwendet werden sollen, wird auf Grund
	// der fehlenden Referenzschalter der Referenziermodus mcHOME_DIRECT verwendet.
	#ifdef SIM_STEPPER
		// Direktes Referenzieren w�hlen
		nHomingMode = mcHOME_DIRECT;
		
		// Wird die ARsim zur Simulation des Projektes verwendet, soll die Meldung "Schrittmotoren simuliert"
		// nicht angezeigt werden. Dies ist f�r die Erstellung der Bedienungsanleitung hilfreich.
		#ifndef HW_ARSIM
			// Meldung "Schrittmotoren simuliert" im Display anzeigen
			gAxisControl.State.bStepperSimulated = 1;
		#else
			// Meldung "Schrittmotoren simuliert" im Display nicht anzeigen
			gAxisControl.State.bStepperSimulated = 0;
	#endif
	// Bei allen anderen Konfigurationen wird der Referenziermodus mcHOME_ABS_SWITCH verwendet
	#else
		// Referenzieren �ber Referenzschalter
		nHomingMode = mcHOME_ABS_SWITCH;
	#endif
	
	// Initialisierung der Schrittkette
	Step.eStepNr = eSTEP_INIT;
}

void _CYCLIC AxisControlCYCLIC(void)
{		
	// Direkt Kommandos
	// Regler einschalten
	if (gAxisControl.DirectBox.bPowerOn == 1)
	{
		Step.eStepNr = eSTEP_POWER_ON;
		BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
	}
	// Regler ausschalten
	if (gAxisControl.DirectBox.bPowerOff == 1)
	{
		Step.eStepNr = eSTEP_POWER_OFF;
		BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
	}
	// Bewegung stoppen
	AuxVar.bStop = 0;
	for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
	{
		if ((gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bStop == 1) ||
			(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bStop == 1))
		{
			AuxVar.bStop = 1;
		}
	}
	if (AuxVar.bStop == 1)
	{
		AuxVar.bControllerOff = 0;
		for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
		{	
			// Regler �berpr�fen
			if (((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerStatus == 0) &&
				(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1) &&
				(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == 1) &&
				(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bStop == 1)) ||
				((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerStatus == 0) &&
				(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1) &&
				(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == 1) &&
				(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bStop == 1)))
			{
				AuxVar.bControllerOff = 1;							// Regler ist ausgeschaltet
				BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
			}
		}
		if (AuxVar.bControllerOff == 0)
		{	
			AuxVar.bStop = 0;
			Step.eStepNr = eSTEP_STOP;
		}
		else
		{
			AuxVar.bControllerOff = 0;
			Step.eStepNr = eSTEP_POWER_ON;
		}
	}
	// Jog Pos
	AuxVar.bJogPos = 0;
	for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
	{
		if (((gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bJogPos == 1) ||
			(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bJogPos == 1)) &&
			(Step.eStepNr != eSTEP_MOVE_ADD_POS) &&
			(Step.eStepNr != eSTEP_JOG_POS))
		{
			AuxVar.bJogPos = 1;
		}
	}
	
	if (AuxVar.bJogPos == 1)				// Tippbetrieb aktiv
	{
		AuxVar.bControllerOff = 0;
		for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
		{	
			// Regler �berpr�fen
			if (((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerStatus == 0) &&
				(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1) &&
				(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == 1) &&
				(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bJogPos == 1)) ||
				((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerStatus == 0) &&
				(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1) &&
				(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == 1) &&
				(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bJogPos == 1)))
			{
				AuxVar.bControllerOff = 1;							// Regler ist ausgeschaltet
			}
		}
		if (AuxVar.bControllerOff == 0)
		{	
			Step.eStepNr = eSTEP_MOVE_ADD_POS;
		}
		else
		{
			AuxVar.bControllerOff = 0;
			Step.eStepNr = eSTEP_POWER_ON;
		}
	}	
	// Jog Neg
	AuxVar.bJogNeg = 0;
	for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
	{
		if (((gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bJogNeg == 1) ||
			(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bJogNeg == 1)) &&
			(Step.eStepNr != eSTEP_MOVE_ADD_NEG) &&
			(Step.eStepNr != eSTEP_JOG_NEG))
		{
			AuxVar.bJogNeg = 1;
		}
	}
	
	if (AuxVar.bJogNeg == 1)				// Tippbetrieb aktiv
	{
		AuxVar.bControllerOff = 0;
		for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
		{	
			// Regler �berpr�fen
			if (((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerStatus == 0) &&
				(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1) &&
				(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == 1) &&
				(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bJogNeg == 1)) ||
				((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerStatus == 0) &&
				(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1) &&
				(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == 1) &&
				(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bJogNeg == 1)))
			{
				AuxVar.bControllerOff = 1;							// Regler ist ausgeschaltet
			}
		}
		if (AuxVar.bControllerOff == 0)
		{	
			Step.eStepNr = eSTEP_MOVE_ADD_NEG;
		}
		else
		{
			AuxVar.bControllerOff = 0;
			Step.eStepNr = eSTEP_POWER_ON;
		}
	}	
	
	// StepHandling
	if(StepHandling.Current.bTimeoutElapsed == 1)
	{
		StepHandling.Current.bTimeoutElapsed = 0;
		Step.eStepNr = StepHandling.Current.nTimeoutContinueStep;
	}
	StepHandling.Current.nStepNr = (DINT)Step.eStepNr;
	strcpy(StepHandling.Current.sStepText, Step.sStepText);
	BrbStepHandler(&StepHandling);
	
	// Schrittkette
	switch(Step.eStepNr)
	{
		//=======================================================================================================================
		case eSTEP_INIT:
			strcpy(Step.sStepText, "eSTEP_INIT");
			Step.eStepNr = eSTEP_INIT_WAIT_FOR_PAR_VALID;
			break;

		case eSTEP_INIT_WAIT_FOR_PAR_VALID:
			strcpy(Step.sStepText, "eSTEP_INIT_WAIT_FOR_PAR_VALID");
			
			if(gParHandling.State.bInitDone == 1)
			{
				Step.eStepNr = eSTEP_INIT_FINISHED;
			}
			
			break;

		case eSTEP_INIT_FINISHED:
			strcpy(Step.sStepText, "eSTEP_INIT_FINISHED");

			// Achsparameter initialisieren			
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				// Horizontale Achse
				gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Direction = mcPOSITIVE_DIR;
				// Vertikale Achse
				gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Direction = mcPOSITIVE_DIR;
				
				// Wird die ARsim zur Simulation des Projektes verwendet, soll die Meldung "Schrittmotoren simuliert"
				// nicht angezeigt werden. Dies ist f�r die Erstellung der Bedienungsanleitung hilfreich.
				#ifndef HW_ARSIM
					// Pr�fen ob sich eine der konfigurierten Achsen in einem Hardware-Simulationsmodus befindet
					if( (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveSimulationStatus >= ncACP_SIM_STANDARD ) ||
						(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveSimulationStatus) >= ncACP_SIM_STANDARD )
					{
						// Die Hardware mindestens einer der konfigurierten Achsen wird simuliert
						gAxisControl.State.bStepperSimulated = 1;
					}
				#endif
			}
			
			Step.bInitDone = 1;
			BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
			Step.eStepNr = eSTEP_POWER_ON;
			break;

		//=======================================================================================================================
		case eSTEP_WAIT_FOR_COMMAND:
			strcpy(Step.sStepText, "eSTEP_WAIT_FOR_COMMAND");
			
			// Alle Achsen referenzieren
			if (gAxisControl.CallerBox.HmiCaller.bRefAllAxes == 1)
			{
				AuxVar.bControllerOff = 0;
				for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
				{
					// Regler �berpr�fen
					if (((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerStatus == 0) &&
						(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == 1) &&
						(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1)) ||
						((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerStatus == 0) &&
						(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == 1) &&
						(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1)))
						{
							AuxVar.bControllerOff = 1;
						}				
				}
				if (AuxVar.bControllerOff == 0)
				{	
					Step.eStepNr = eSTEP_HOMING_ALL;
				}
				else
				{
					AuxVar.bControllerOff = 0;
					Step.eStepNr = eSTEP_POWER_ON;
				}
			}
			
			// Die Achslimits aller Achsen (motorische Verstellung) initialisieren
			if( gAxisControl.CallerBox.bInitAllAxesLimits == TRUE )
			{
				Step.eStepNr = eSTEP_INIT_ALL_AXES_LIMITS;
			}
			// Das Kommando alle Achsen in ihrer Arbeitsposition verfahren ist angefordert
			if( gAxisControl.CallerBox.bMoveAllAxesToWorkPosition == TRUE )
			{
				Step.eStepNr = eSTEP_MOVE_ALL_AXES_TO_WORK_POS;
			}
			
			// Achse referenzieren
			AuxVar.bHoming = 0;
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				if ((gAxisControl.CallerBox.HmiCaller.Horizontal[nBonDevIdx].bRefAxis == 1) ||
					(gAxisControl.CallerBox.HmiCaller.Vertical[nBonDevIdx].bRefAxis == 1))
				{
					AuxVar.bHoming = 1;
				}
			}
			if (AuxVar.bHoming == 1)
			{
				AuxVar.bControllerOff = 0;
				for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
				{	
					// Regler �berpr�fen
					if (((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerStatus == 0) &&
						(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1) &&
						(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == 1) &&
						(gAxisControl.CallerBox.HmiCaller.Horizontal[nBonDevIdx].bRefAxis == 1)) ||
						((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerStatus == 0) &&
						(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1) &&
						(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == 1) &&
						(gAxisControl.CallerBox.HmiCaller.Vertical[nBonDevIdx].bRefAxis == 1)))
					{
						AuxVar.bControllerOff = 1;							// Regler ist ausgeschaltet
					}
				}
				if (AuxVar.bControllerOff == 0)
				{	
					AuxVar.bHoming = 0;
					Step.eStepNr = eSTEP_HOMING;
				}
				else
				{
					AuxVar.bControllerOff = 0;
					Step.eStepNr = eSTEP_POWER_ON;
				}
			}
			
			// Absolute Bewegung ausf�hren
			AuxVar.bMoveAbs = 0;
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				if ((gAxisControl.CallerBox.HmiCaller.Horizontal[nBonDevIdx].bStartAbsMove == 1) ||
					(gAxisControl.CallerBox.HmiCaller.Vertical[nBonDevIdx].bStartAbsMove == 1))
				{
					AuxVar.bMoveAbs = 1;
				}
			}
			if (AuxVar.bMoveAbs == 1)
			{
				AuxVar.bControllerOff = 0;
				for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
				{	
					// Regler �berpr�fen
					if (((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerStatus == 0) &&
						(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1) &&
						(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == 1) &&
						(gAxisControl.CallerBox.HmiCaller.Horizontal[nBonDevIdx].bStartAbsMove == 1)) ||
						((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerStatus == 0) &&
						(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1) &&
						(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == 1) &&
						(gAxisControl.CallerBox.HmiCaller.Vertical[nBonDevIdx].bStartAbsMove == 1)))
					{
						AuxVar.bControllerOff = 1;							// Regler ist ausgeschaltet
					}
				}
				if (AuxVar.bControllerOff == 0)
				{	
					AuxVar.bMoveAbs = 0;
					Step.eStepNr = eSTEP_MOVE_ABS;
				}
				else
				{
					AuxVar.bControllerOff = 0;
					Step.eStepNr = eSTEP_POWER_ON;
				}
			}
			
			// Pr�fen ob einer der horizontalen oder vertikalen motorischen Verstellungen konfiguriert wurde
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				if( (gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == TRUE) || 
					(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == TRUE) )
				{
					bOneOrMoreAxisIsEnabled = TRUE;
					break;
				}
				else
				{
					bOneOrMoreAxisIsEnabled = FALSE;
				}
			}
			// Es ist keine Achse konfiguriert
			if( bOneOrMoreAxisIsEnabled == FALSE )
			{
				// In Schritt "Keine Achsen konfiguriert" springen
				Step.eStepNr = eSTEP_NO_AXIS_EXISTS;
			}
			break;

		//-----------------------------------------------------------------------------------------------------------------------
		// Das Modul "Motorische Verstellung" ist zwar angew�hlt, aber es sind keine weiteren Achsen konfiguriert
		case eSTEP_NO_AXIS_EXISTS:
			strcpy(Step.sStepText, "eSTEP_NO_AXIS_EXISTS");
			
			// Kommando bInitAllAxesLimits wird ignoriert und das Kommandofach geleert			
			if( gAxisControl.CallerBox.bInitAllAxesLimits == TRUE )
			{
				BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
			}
			// Kommando bInitAllAxesLimits wird ignoriert und das Kommandofach geleert
			if( gAxisControl.CallerBox.bMoveAllAxesToWorkPosition == TRUE )
			{
				BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
			}
			// Kommando bInitAllAxesLimits wird ignoriert und das Kommandofach geleert
			if( gAxisControl.CallerBox.HmiCaller.bRefAllAxes == TRUE )
			{
				BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
			}
			
			// Schleife �ber alle konfigurierten K�pfe
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				// Kommando bInitAllAxesLimits wird ignoriert und das Kommandofach geleert
				if ((gAxisControl.CallerBox.HmiCaller.Horizontal[nBonDevIdx].bStartAbsMove == TRUE) ||
					(gAxisControl.CallerBox.HmiCaller.Vertical[nBonDevIdx].bStartAbsMove == TRUE))
				{
					BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
				}
				// Kommando bInitAllAxesLimits wird ignoriert und das Kommandofach geleert
				if( (gAxisControl.CallerBox.HmiCaller.Horizontal[nBonDevIdx].bRefAxis == TRUE) ||
					(gAxisControl.CallerBox.HmiCaller.Vertical[nBonDevIdx].bRefAxis == TRUE) )
				{
					BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
				}
				
				// Pr�fen ob einer der horizontalen oder vertikalen motorischen Verstellungen nachtr�glich konfiguriert wurde
				if( (gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == TRUE) || 
					(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == TRUE) )
				{
					// In Schritt "Warten auf Kommando" springen, da nun 
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
					break;
				}
			}
			
			break;
		
		//-----------------------------------------------------------------------------------------------------------------------
		// Alle Achsen in ihrer Arbeitsposition verfahren (wenn n�tig wird POWER_ON und HOMING durchgef�hrt)
		case eSTEP_MOVE_ALL_AXES_TO_WORK_POS:
			strcpy(Step.sStepText, "eSTEP_MOVE_ALL_AXES_TO_WORK_POS");
			
			// mindestens ein Regler ist nicht eingeschaltet
			if( AllAxesPowerOn() == FALSE )
			{
				Step.eStepNr = eSTEP_POWER_ON;
				break;
			}
		
			// mindestens eine Achse ist nicht referenziert
			if( AllAxesHomed() == FALSE )
			{
				for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++ )
				{
					if( (gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == TRUE) &&
						(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.HomingOk == FALSE) )
					{
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.HomeMode = nHomingMode;
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.HomePosition	= (gPar.MachinePar.MotorisedAdjustment.RefPos[nBonDevIdx].rHorizontal * rFACTOR_MM_TO_AXIS_UNITS);
						gAxisControl.AxisCmd[nBonDevIdx].Horizontal.Home = 1;
					}
					
					if( (gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == TRUE) &&
						(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.HomingOk == FALSE) )
					{
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.HomeMode = nHomingMode;
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.HomePosition	= (gPar.MachinePar.MotorisedAdjustment.RefPos[nBonDevIdx].rVertical * rFACTOR_MM_TO_AXIS_UNITS);
						gAxisControl.AxisCmd[nBonDevIdx].Vertical.Home = 1;				
					}
				}
				
				Step.eStepNr = eSTEP_HOMING_ALL_ACTIVE;
				break;
			}
			
			if( AllAxesHomed() == TRUE )
			{
				for( nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++ )
				{
					if( gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == TRUE )
					{
						gAxisControl.CallerBox.HmiCaller.Horizontal[nBonDevIdx].bStartAbsMove = TRUE;
						gAxisControl.Par.HmiPar.AbsPos[nBonDevIdx].Horizontal = gPar.ProductPar.BonDevPar[nBonDevIdx].AxesPosition.Alignment[eHORIZONTAL].rWorkPosition;
					}
					
					if( gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == TRUE )
					{
						gAxisControl.CallerBox.HmiCaller.Vertical[nBonDevIdx].bStartAbsMove = TRUE;
						gAxisControl.Par.HmiPar.AbsPos[nBonDevIdx].Vertical = gPar.ProductPar.BonDevPar[nBonDevIdx].AxesPosition.Alignment[eVERTICAL].rWorkPosition;
					}
				
				}
				
				Step.eStepNr = eSTEP_MOVE_ABS;
			}
			break;
			
		
		
		//-----------------------------------------------------------------------------------------------------------------------
		// Regler einschalten
		case eSTEP_POWER_ON:
			strcpy(Step.sStepText, "eSTEP_POWER_ON");
			
			// Flag zum �berpr�fen ob mindestens eine Achse konfiguriert ist, zur�cksetzen
			bOneOrMoreAxisIsEnabled = FALSE;
			
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				if ((gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == 1) &&
					(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1))
				{
					gAxisControl.AxisCmd[nBonDevIdx].Horizontal.Power	= 1;
					// Flag zum �berpr�fen ob mindestens eine Achse konfiguriert ist, setzen
					bOneOrMoreAxisIsEnabled = TRUE;
				}
				if ((gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == 1) &&
					(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1))
				{
					gAxisControl.AxisCmd[nBonDevIdx].Vertical.Power	= 1;
					// Flag zum �berpr�fen ob mindestens eine Achse konfiguriert ist, setzen
					bOneOrMoreAxisIsEnabled = TRUE;
				}
			}
			
			// Das Modul "Motorische Verstellung" ist zwar angew�hlt, aber es sind keine weiteren Achsen konfiguriert
			if( bOneOrMoreAxisIsEnabled == FALSE )
			{
				Step.eStepNr = eSTEP_NO_AXIS_EXISTS;
			}
			// Das Kommando alle Achsen in ihrer Arbeitsposition verfahren ist angefordert
			else if( gAxisControl.CallerBox.bMoveAllAxesToWorkPosition == TRUE )
			{
				Step.eStepNr = eSTEP_MOVE_ALL_AXES_TO_WORK_POS;
			}
			else
			{	
				Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			}
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Regler ausschalten
		case eSTEP_POWER_OFF:
			strcpy(Step.sStepText, "eSTEP_POWER_OFF");
			
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				gAxisControl.AxisCmd[nBonDevIdx].Horizontal.Power = 0;
				gAxisControl.AxisCmd[nBonDevIdx].Vertical.Power	= 0;
			}			
			
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Alle Achsen referenzieren
		case eSTEP_HOMING_ALL:
			strcpy(Step.sStepText, "eSTEP_HOMING_ALL");
			
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				if (gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == 1)
				{
					gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.HomeMode = nHomingMode;
					gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.HomePosition	= (gPar.MachinePar.MotorisedAdjustment.RefPos[nBonDevIdx].rHorizontal * rFACTOR_MM_TO_AXIS_UNITS);
					gAxisControl.AxisCmd[nBonDevIdx].Horizontal.Home = 1;
				}
				if (gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == 1)
				{
					gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.HomeMode = nHomingMode;
					gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.HomePosition	= (gPar.MachinePar.MotorisedAdjustment.RefPos[nBonDevIdx].rVertical * rFACTOR_MM_TO_AXIS_UNITS);
					gAxisControl.AxisCmd[nBonDevIdx].Vertical.Home = 1;				
				}
			}
			
			Step.eStepNr = eSTEP_HOMING_ALL_ACTIVE;			
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Referenzierung aller Achsen aktiv
		case eSTEP_HOMING_ALL_ACTIVE:
			strcpy(Step.sStepText, "eSTEP_HOMING_ALL_ACTIVE");
			
			brsmemset((UDINT)&AuxVar.HomingDone[0], 0, sizeof(AuxVar.HomingDone));
			for (nBonDevIdx = 0; nBonDevIdx <= nDEV_INDEX_MAX; nBonDevIdx ++)
			{
				// Horizontale Achse
				if (gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == 1)
				{
					if ((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.HomingOk == 1) ||
						(gAxisControl.State.AxisState[nBonDevIdx].Horizontal.Homing == 1) ||
						(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != 0) ||
						(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 0))
					{	
						AuxVar.HomingDone[nBonDevIdx].bHomingHorizontal = 1;
						gAxisControl.AxisCmd[nBonDevIdx].Horizontal.Home = 0;
					}
				}
				else
				{
					AuxVar.HomingDone[nBonDevIdx].bHomingHorizontal = 1;
					gAxisControl.AxisCmd[nBonDevIdx].Horizontal.Home = 0;
				}	
				// Vertikale Achse
				if (gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == 1)
				{
					if ((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.HomingOk == 1) ||
						(gAxisControl.State.AxisState[nBonDevIdx].Vertical.Homing == 1) ||
						(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != 0) ||
						(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 0))
					{	
						AuxVar.HomingDone[nBonDevIdx].bHomingVertical = 1;
						gAxisControl.AxisCmd[nBonDevIdx].Vertical.Home = 0;	
					}
				}
				else
				{
					AuxVar.HomingDone[nBonDevIdx].bHomingVertical = 1;
					gAxisControl.AxisCmd[nBonDevIdx].Vertical.Home = 0;				
				}
			}
			
			if ((AuxVar.HomingDone[0].bHomingHorizontal == 1) && (AuxVar.HomingDone[0].bHomingVertical == 1) &&
				(AuxVar.HomingDone[1].bHomingHorizontal == 1) && (AuxVar.HomingDone[1].bHomingVertical == 1) &&
				(AuxVar.HomingDone[2].bHomingHorizontal == 1) && (AuxVar.HomingDone[2].bHomingVertical == 1) &&
				(AuxVar.HomingDone[3].bHomingHorizontal == 1) && (AuxVar.HomingDone[3].bHomingVertical == 1) &&
				(AuxVar.HomingDone[4].bHomingHorizontal == 1) && (AuxVar.HomingDone[4].bHomingVertical == 1) &&
				(AuxVar.HomingDone[5].bHomingHorizontal == 1) && (AuxVar.HomingDone[5].bHomingVertical == 1) &&
				(AuxVar.HomingDone[6].bHomingHorizontal == 1) && (AuxVar.HomingDone[6].bHomingVertical == 1) &&
				(AuxVar.HomingDone[7].bHomingHorizontal == 1) && (AuxVar.HomingDone[7].bHomingVertical == 1) &&
				(AuxVar.HomingDone[8].bHomingHorizontal == 1) && (AuxVar.HomingDone[8].bHomingVertical == 1) &&
				(AuxVar.HomingDone[9].bHomingHorizontal == 1) && (AuxVar.HomingDone[9].bHomingVertical == 1) &&
				(AuxVar.HomingDone[10].bHomingHorizontal == 1) && (AuxVar.HomingDone[10].bHomingVertical == 1) &&
				(AuxVar.HomingDone[11].bHomingHorizontal == 1) && (AuxVar.HomingDone[11].bHomingVertical == 1))
			{
				// Das Kommando alle Achsen in ihrer Arbeitsposition verfahren ist angefordert
				if( gAxisControl.CallerBox.bMoveAllAxesToWorkPosition == TRUE )
				{
					Step.eStepNr = eSTEP_MOVE_ALL_AXES_TO_WORK_POS;
				}
				else
				{
					BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
				}
			}							
			break;	
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Die Achslimits aller Achsen initalialisieren
		case eSTEP_INIT_ALL_AXES_LIMITS:
			strcpy(Step.sStepText, "eSTEP_INIT_ALL_AXES_LIMITS" );
			
			/* Schleife �ber alle konfigurierten motorischen Verstellungen */
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				/* Horizontale Verstellung ist konfiguriert f�r diesen Klebe-Kopf */
				if( gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == TRUE )
				{
					gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.NegativeSwLimit = gPar.MachinePar.MotorisedAdjustment.AxesLimits[nBonDevIdx].Alignment[eHORIZONTAL].rNegativeSwLimit * rFACTOR_MM_TO_AXIS_UNITS;
					gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.PositiveSwLimit = gPar.MachinePar.MotorisedAdjustment.AxesLimits[nBonDevIdx].Alignment[eHORIZONTAL].rPositiveSwLimit * rFACTOR_MM_TO_AXIS_UNITS;
					gAxisControl.AxisCmd[nBonDevIdx].Horizontal.InitAxisLimits = TRUE;
				}
				
				/* Vertikale Verstellung ist konfiguriert f�r diesen Klebe-Kopf */
				if( gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == TRUE )
				{
					gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.NegativeSwLimit = gPar.MachinePar.MotorisedAdjustment.AxesLimits[nBonDevIdx].Alignment[eVERTICAL].rNegativeSwLimit * rFACTOR_MM_TO_AXIS_UNITS;
					gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.PositiveSwLimit = gPar.MachinePar.MotorisedAdjustment.AxesLimits[nBonDevIdx].Alignment[eVERTICAL].rPositiveSwLimit * rFACTOR_MM_TO_AXIS_UNITS;
					gAxisControl.AxisCmd[nBonDevIdx].Vertical.InitAxisLimits = TRUE;
				}
			}
			// Kommandoschnittstelle f�r neues Kommando freigeben
			BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
			Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
			break;
		
		//-----------------------------------------------------------------------------------------------------------------------
		// Achse referenzieren
		case eSTEP_HOMING:
			strcpy(Step.sStepText, "eSTEP_HOMING");
	
			if (gAxisControl.CallerBox.HmiCaller.Horizontal[gAxisControl.Par.HmiPar.nRefAxisIdx].bRefAxis == 1)
			{
				if (gAxisControl.State.AxisStatus[gAxisControl.Par.HmiPar.nRefAxisIdx].Horizontal.DriveStatus.ControllerReady == 1)
				{	
					gAxisControl.Par.AxisPar[gAxisControl.Par.HmiPar.nRefAxisIdx].Horizontal.HomeMode = nHomingMode;
					gAxisControl.Par.AxisPar[gAxisControl.Par.HmiPar.nRefAxisIdx].Horizontal.HomePosition	= (gPar.MachinePar.MotorisedAdjustment.RefPos[gAxisControl.Par.HmiPar.nRefAxisIdx].rHorizontal * rFACTOR_MM_TO_AXIS_UNITS);
					gAxisControl.AxisCmd[gAxisControl.Par.HmiPar.nRefAxisIdx].Horizontal.Home = 1;
					if ((gAxisControl.State.AxisState[gAxisControl.Par.HmiPar.nRefAxisIdx].Horizontal.Homing == 1) || 
						(gAxisControl.State.AxisStatus[gAxisControl.Par.HmiPar.nRefAxisIdx].Horizontal.ErrorID != 0))
					{
						gAxisControl.AxisCmd[gAxisControl.Par.HmiPar.nRefAxisIdx].Horizontal.Home = 0;
						BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
					}
				}
				else
				{
					BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
				}
			}
			if (gAxisControl.CallerBox.HmiCaller.Vertical[gAxisControl.Par.HmiPar.nRefAxisIdx].bRefAxis == 1)
			{
				if (gAxisControl.State.AxisStatus[gAxisControl.Par.HmiPar.nRefAxisIdx].Vertical.DriveStatus.ControllerReady == 1)
				{
					gAxisControl.Par.AxisPar[gAxisControl.Par.HmiPar.nRefAxisIdx].Vertical.HomeMode = nHomingMode;
					gAxisControl.Par.AxisPar[gAxisControl.Par.HmiPar.nRefAxisIdx].Vertical.HomePosition	= (gPar.MachinePar.MotorisedAdjustment.RefPos[gAxisControl.Par.HmiPar.nRefAxisIdx].rVertical * rFACTOR_MM_TO_AXIS_UNITS);
					gAxisControl.AxisCmd[gAxisControl.Par.HmiPar.nRefAxisIdx].Vertical.Home = 1;
					if ((gAxisControl.State.AxisState[gAxisControl.Par.HmiPar.nRefAxisIdx].Vertical.Homing == 1) ||
						(gAxisControl.State.AxisStatus[gAxisControl.Par.HmiPar.nRefAxisIdx].Vertical.ErrorID != 0))
					{
						gAxisControl.AxisCmd[gAxisControl.Par.HmiPar.nRefAxisIdx].Vertical.Home = 0;
						BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;					
					}
				}
				else
				{
					BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
				}
			}		
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Bewegung stoppen
		case eSTEP_STOP:
			strcpy(Step.sStepText, "eSTEP_STOP");
			
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				// Horizontale Achse
				if (gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bStop == 1)
				{
					if (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1)
					{
						gAxisControl.AxisCmd[nBonDevIdx].Horizontal.Stop = 1;
						if ((gAxisControl.State.AxisState[nBonDevIdx].Horizontal.Stopping == 1) ||
							(gAxisControl.State.AxisState[nBonDevIdx].Horizontal.StandStill == 1) ||
							(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != 0))
						{
							gAxisControl.AxisCmd[nBonDevIdx].Horizontal.Stop = 0;
							BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
						}
					}
					else
					{
						gAxisControl.AxisCmd[nBonDevIdx].Horizontal.Stop = 0;
						BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;						
					}
				}
				// Vertikale Achse
				if (gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bStop == 1)
				{
					if (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1)
					{
						gAxisControl.AxisCmd[nBonDevIdx].Vertical.Stop = 1;
						if ((gAxisControl.State.AxisState[nBonDevIdx].Vertical.Stopping == 1) ||
							(gAxisControl.State.AxisState[nBonDevIdx].Vertical.StandStill == 1) ||
							(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != 0))
						{
							gAxisControl.AxisCmd[nBonDevIdx].Vertical.Stop = 0;
							BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
						}
					}
					else
					{
						gAxisControl.AxisCmd[nBonDevIdx].Vertical.Stop = 0;
						BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;					
					}					
				}
			}
			
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Absolute Bewegung ausf�hren
		case eSTEP_MOVE_ABS:
			strcpy(Step.sStepText, "eSTEP_MOVE_ABS");
			
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				// Horizontale Achse
				if (gAxisControl.CallerBox.HmiCaller.Horizontal[nBonDevIdx].bStartAbsMove == 1)
				{
					if (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1)
					{
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Velocity	= (gPar.MachinePar.MotorisedAdjustment.rVelocityHorizontal * rFACTOR_MM_TO_AXIS_UNITS);
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Position	= (gAxisControl.Par.HmiPar.AbsPos[nBonDevIdx].Horizontal * rFACTOR_MM_TO_AXIS_UNITS);
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Acceleration	= nACCELERATION_HOR;
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Deceleration	= nDECELERATION_HOR;
						gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveAbsolute = 1;
						// Achse verf�hrt oder ein Fehler ist aufgetreten
						if ((gAxisControl.State.AxisState[nBonDevIdx].Horizontal.DiscreteMotion == 1) ||
							(gAxisControl.State.AxisState[nBonDevIdx].Horizontal.ErrorStop == 1) ||
							(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != 0))
						{
							BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
						}
					}
					else
					{
						BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
						gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveAbsolute = 0;
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
					}
				}
				// Vertikale Achse
				if (gAxisControl.CallerBox.HmiCaller.Vertical[nBonDevIdx].bStartAbsMove == 1)
				{
					if (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerStatus == 1)
					{
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Velocity	= (gPar.MachinePar.MotorisedAdjustment.rVelocityVertical * rFACTOR_MM_TO_AXIS_UNITS);
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Position	= (gAxisControl.Par.HmiPar.AbsPos[nBonDevIdx].Vertical * rFACTOR_MM_TO_AXIS_UNITS);
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Acceleration	= nACCELERATION_VER;
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Deceleration	= nDECELERATION_VER;
						gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveAbsolute = 1;
						// Achse verf�hrt oder ein Fehler ist aufgetreten
						if ((gAxisControl.State.AxisState[nBonDevIdx].Vertical.DiscreteMotion == 1) ||
							(gAxisControl.State.AxisState[nBonDevIdx].Vertical.ErrorStop == 1) ||
							(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != 0))
						{
							BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
						}
					}
					else
					{
						BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
						gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveAbsolute = 0;
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
					}
				}
			}			
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Additive Bewegung in positive Richtung ausf�hren
		case eSTEP_MOVE_ADD_POS:
			strcpy(Step.sStepText, "eSTEP_MOVE_ADD_POS");
			
			bMoveAddPosActive = 0;
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
			
				// Horizontale Achse --> Positive Richtung
				if (gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bJogPos == 1)
				{
					if (AuxVar.Move_Add_Pos[nBonDevIdx].Horizontal == 0)
					{
						// Flag setzen: Additive Bewegung in pos. Richtung aktiv
						bMoveAddPosActive = 1;																						// Befehl wurd abgesetzt
						if (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1)
						{	
							gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Velocity 		= (gPar.MachinePar.MotorisedAdjustment.rVelocityHorizontal * rFACTOR_MM_TO_AXIS_UNITS * rJOG_V_REDUCE);
							gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Distance		= nINC_HORIZONTAL;						// Schrittweite f�r Horizontale Achse -> 0,1 mm
							gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Acceleration	= nACCELERATION_HOR;
							gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Deceleration	= nDECELERATION_HOR;
							gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveAdditive = 1;
							AuxVar.Move_Add_Pos[nBonDevIdx].Horizontal = 1;
							// Achse verf�hrt oder ein Fehler ist aufgetreten
							if ((gAxisControl.State.AxisState[nBonDevIdx].Horizontal.ErrorStop == 1) ||
								(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != 0))
							{
								fbTon.IN = 0;
								TON_10ms(&fbTon);
								
								gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveAdditive = 0;
								brsmemset((UDINT)&AuxVar.Move_Add_Pos, 0, sizeof(AuxVar.Move_Add_Pos));
								BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
								Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
							}
						}
						else
						{
							fbTon.IN = 0;
							TON_10ms(&fbTon);
								
							brsmemset((UDINT)&AuxVar.Move_Add_Pos, 0, sizeof(AuxVar.Move_Add_Pos));
							BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;					
						}
					}
				}

				// Vertikale Achse --> Positive Richtung
				if (gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bJogPos == 1)
				{
					// Die vertikale Achse macht noch keine pos. additive Bewegung (Achse steht)
					if (AuxVar.Move_Add_Pos[nBonDevIdx].Vertical == 0)
					{
						// Flag setzen: Additive Bewegung in pos. Richtung aktiv
						bMoveAddPosActive = 1;																						// Befehl wurd abgesetzt
						if (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1)
						{	
							gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Velocity 		= (gPar.MachinePar.MotorisedAdjustment.rVelocityVertical * rFACTOR_MM_TO_AXIS_UNITS * rJOG_V_REDUCE);
							gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Distance		= nINC_VERTICAL;						// Schrittweite f�r Vertikale Achse -> 0,05 mm
							gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Acceleration	= nACCELERATION_VER;
							gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Deceleration	= nDECELERATION_VER;
							gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveAdditive = 1;
							AuxVar.Move_Add_Pos[nBonDevIdx].Vertical = 1;
							// Achse verf�hrt oder ein Fehler ist aufgetreten
							if ((gAxisControl.State.AxisState[nBonDevIdx].Vertical.ErrorStop == 1) ||
								(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != 0))
							{
								fbTon.IN = 0;
								TON_10ms(&fbTon);
								
								gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveAdditive = 0;
								brsmemset((UDINT)&AuxVar.Move_Add_Pos, 0, sizeof(AuxVar.Move_Add_Pos));
								BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
								Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
							}
						}
						else
						{
							fbTon.IN = 0;
							TON_10ms(&fbTon);
								
							brsmemset((UDINT)&AuxVar.Move_Add_Pos, 0, sizeof(AuxVar.Move_Add_Pos));
							BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;					
						}
					}
				}
			}
			
			// *************************************
			// Umschaltung von additiver Bewegung in
			// eine absolute Bewegung bis SW-Endlage
			// *************************************
			
			// Befehl zur positiven additiven Bewegung wurde abgesetzt,
			// und in der Visu wird weiterhin der Jog-Button bet�tigt
			if (bMoveAddPosActive == 0)
			{
				// Zeitmessung der Bet�tigungsdauer des Jog-Button in Visu
				fbTon.IN = 1;
				fbTon.PT = nTIMER_DELAY;
				TON_10ms(&fbTon);
				
				AuxVar.bJogPosActive = 0;
				
				for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
				{
					// Tippbetrieb (Jog-Button) in der Visu ist weiterhin aktiv
					if ((gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bJogPos == 1) ||
						(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bJogPos == 1))
					{
						AuxVar.bJogPosActive = 1;
					}					
				}
				
				// In der Visualisierung wurde �ber die Zeit
				// nTIMER_DELAY der Jog-Button bet�tigt
				// -> In den Schritt Jog Pos springen
				if ((AuxVar.bJogPosActive == 1) &&
					(fbTon.Q == 1))
				{
					fbTon.IN = 0;
					TON_10ms(&fbTon);
					
					brsmemset((UDINT)&AuxVar.Move_Add_Pos, 0, sizeof(AuxVar.Move_Add_Pos));
					Step.eStepNr = eSTEP_JOG_POS;		
				}
				// Der Jog-Button wurde in der Visu vor Ablauf
				// der Zeit nTIMER_DELAY losgelassen
				// -> In den Warteschritt springen
				else if (AuxVar.bJogPosActive == 0)
				{
					fbTon.IN = 0;
					TON_10ms(&fbTon);
						
					brsmemset((UDINT)&AuxVar.Move_Add_Pos, 0, sizeof(AuxVar.Move_Add_Pos));
					BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;				
				}
			}

			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Additive Bewegung in negative Richtung ausf�hren
		case eSTEP_MOVE_ADD_NEG:
			strcpy(Step.sStepText, "eSTEP_MOVE_ADD_NEG");
			
			bMoveAddNegActive = 0;
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
			
				// Horizontale Achse --> Negative Richtung
				if (gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bJogNeg == 1)
				{
					if (AuxVar.Move_Add_Neg[nBonDevIdx].Horizontal == 0)
					{
						// Flag setzen: Additive Bewegung in neg. Richtung aktiv
						bMoveAddNegActive = 1;																						// Befehl wurd abgesetzt
						if (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1)
						{	
//							REAL CalcAdditiveDistance( USINT Direction, MoveDistance, SwLimit )
//							{
//								
//							}
							
							gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Velocity 		= (gPar.MachinePar.MotorisedAdjustment.rVelocityHorizontal * rFACTOR_MM_TO_AXIS_UNITS * rJOG_V_REDUCE);
							gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Distance		= - nINC_HORIZONTAL;						// Schrittweite f�r Horizontale Achse -> -0,1 mm
							gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Acceleration	= nACCELERATION_HOR;
							gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Deceleration	= nDECELERATION_HOR;
							gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveAdditive = 1;
							AuxVar.Move_Add_Neg[nBonDevIdx].Horizontal = 1;
							// Achse verf�hrt oder ein Fehler ist aufgetreten
							if ((gAxisControl.State.AxisState[nBonDevIdx].Horizontal.ErrorStop == 1) ||
								(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != 0))
							{
								fbTon.IN = 0;
								TON_10ms(&fbTon);
								
								gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveAdditive = 0;
								brsmemset((UDINT)&AuxVar.Move_Add_Neg, 0, sizeof(AuxVar.Move_Add_Neg));
								BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
								Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
							}
						}
						else
						{
							fbTon.IN = 0;
							TON_10ms(&fbTon);
								
							brsmemset((UDINT)&AuxVar.Move_Add_Neg, 0, sizeof(AuxVar.Move_Add_Neg));
							BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;					
						}
					}
				}

				// Vertikale Achse --> Negative Richtung
				if (gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bJogNeg == 1)
				{
					if (AuxVar.Move_Add_Neg[nBonDevIdx].Vertical == 0)
					{
						// Flag setzen: Additive Bewegung in neg. Richtung aktiv
						bMoveAddNegActive = 1;																						// Befehl wurd abgesetzt
						if (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1)
						{	
							gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Velocity 		= (gPar.MachinePar.MotorisedAdjustment.rVelocityVertical * rFACTOR_MM_TO_AXIS_UNITS * rJOG_V_REDUCE);
							gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Distance		= - nINC_VERTICAL;						// Schrittweite f�r Vertikale Achse -> -0,05 mm
							gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Acceleration	= nACCELERATION_VER;
							gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Deceleration	= nDECELERATION_VER;
							gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveAdditive = 1;
							AuxVar.Move_Add_Neg[nBonDevIdx].Vertical = 1;
							// Achse verf�hrt oder ein Fehler ist aufgetreten
							if ((gAxisControl.State.AxisState[nBonDevIdx].Vertical.ErrorStop == 1) ||
								(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != 0))
							{
								fbTon.IN = 0;
								TON_10ms(&fbTon);
								
								gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveAdditive = 0;
								brsmemset((UDINT)&AuxVar.Move_Add_Neg, 0, sizeof(AuxVar.Move_Add_Neg));
								BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
								Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
							}
						}
						else
						{
							fbTon.IN = 0;
							TON_10ms(&fbTon);
								
							brsmemset((UDINT)&AuxVar.Move_Add_Neg, 0, sizeof(AuxVar.Move_Add_Neg));
							BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;					
						}
					}
				}
			}
					
			// *************************************
			// Umschaltung von additiver Bewegung in
			// eine absolute Bewegung bis SW-Endlage
			// *************************************
			
			// Befehl zur negativen additiven Bewegung wurde abgesetzt,
			// und in der Visu wird weiterhin der Jog-Button bet�tigt
			if (bMoveAddNegActive == 0)
			{
				// Zeitmessung der Bet�tigungsdauer des Jog-Button in Visu
				fbTon.IN = 1;
				fbTon.PT = nTIMER_DELAY;
				TON_10ms(&fbTon);
				
				AuxVar.bJogNegActive = 0;
				for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
				{
					// Tippbetrieb (Jog-Button) in der Visu ist weiterhin aktiv
					if ((gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bJogNeg == 1) ||
						(gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bJogNeg == 1))
					{
						AuxVar.bJogNegActive = 1;
					}					
				}
				
				// In der Visualisierung wurde �ber die Zeit
				// nTIMER_DELAY der Jog-Button bet�tigt
				// -> In den Schritt Jog Pos springen
				if ((AuxVar.bJogNegActive == 1) &&
					(fbTon.Q == 1))
				{
					fbTon.IN = 0;
					TON_10ms(&fbTon);
					
					brsmemset((UDINT)&AuxVar.Move_Add_Neg, 0, sizeof(AuxVar.Move_Add_Neg));
					Step.eStepNr = eSTEP_JOG_NEG;		
				}
				// Der Jog-Button wurde in der Visu vor Ablauf
				// der Zeit nTIMER_DELAY losgelassen
				// -> In den Warteschritt springen
				else if (AuxVar.bJogNegActive == 0)
				{
					fbTon.IN = 0;
					TON_10ms(&fbTon);
						
					brsmemset((UDINT)&AuxVar.Move_Add_Neg, 0, sizeof(AuxVar.Move_Add_Neg));
					BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
					Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;				
				}
			}
			
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Jog Pos
		case eSTEP_JOG_POS:
			strcpy(Step.sStepText, "eSTEP_JOG_POS");
			
			bJogPosActive = 0;
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				// Horizontale Achse
				if (gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bJogPos == 1)
				{
					if (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1)
					{	
						bJogPosActive = 1;
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.JogVelocity 	= (gPar.MachinePar.MotorisedAdjustment.rVelocityHorizontal * rFACTOR_MM_TO_AXIS_UNITS * rJOG_V_REDUCE);
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Acceleration	= nACCELERATION_HOR;
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Deceleration	= nDECELERATION_HOR;
						gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveJogPos = 1;
						if (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != 0)
						{
							gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveJogPos = 0;
							BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
						}
					}
					else
					{
						BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;					
					}
				}
				// Vertikale Achse
				if (gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bJogPos == 1)
				{
					if (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1)
					{
						bJogPosActive = 1;
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.JogVelocity = (gPar.MachinePar.MotorisedAdjustment.rVelocityVertical * rFACTOR_MM_TO_AXIS_UNITS * rJOG_V_REDUCE);
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Acceleration	= nACCELERATION_VER;
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Deceleration	= nDECELERATION_VER;
						gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveJogPos = 1;
						if (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != 0)
						{
							gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveJogPos = 0;
							BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
						}
					}
					else
					{
						BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;					
					}
				}
			}
			
			if (bJogPosActive == 0)
			{
				for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
				{
					gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveJogPos = 0;
					gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveJogPos = 0;
				}
				BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
				Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;			
			}
			break;
			
		//-----------------------------------------------------------------------------------------------------------------------
		// Jog Neg
		case eSTEP_JOG_NEG:
			strcpy(Step.sStepText, "eSTEP_JOG_NEG");
			
			bJogNegActive = 0;
			for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
			{
				// Horizontale Achse
				if (gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Horizontal.bJogNeg == 1)
				{
					if (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 1)
					{
						bJogNegActive = 1;
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.JogVelocity = (gPar.MachinePar.MotorisedAdjustment.rVelocityHorizontal * rFACTOR_MM_TO_AXIS_UNITS * rJOG_V_REDUCE);
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Acceleration	= nACCELERATION_HOR;
						gAxisControl.Par.AxisPar[nBonDevIdx].Horizontal.Deceleration	= nDECELERATION_HOR;						
						gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveJogNeg = 1;
						if (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != 0)
						{
							gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveJogNeg = 0;
							BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
						}
					}
					else
					{
						BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;					
					}
				}
				// Vertikale Achse
				if (gAxisControl.DirectBox.HmiCmd[nBonDevIdx].Vertical.bJogNeg == 1)
				{
					if (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 1)
					{
						bJogNegActive = 1;
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.JogVelocity = (gPar.MachinePar.MotorisedAdjustment.rVelocityVertical * rFACTOR_MM_TO_AXIS_UNITS * rJOG_V_REDUCE);;
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Acceleration	= nACCELERATION_VER;
						gAxisControl.Par.AxisPar[nBonDevIdx].Vertical.Deceleration	= nDECELERATION_VER;						
						gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveJogNeg = 1;
						if (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != 0)
						{
							gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveJogNeg = 0;
							BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
							Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;
						}
					}
					else
					{
						BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
						Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;					
					}
				}
			}
			
			if (bJogNegActive == 0)
			{
				for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
				{
					gAxisControl.AxisCmd[nBonDevIdx].Horizontal.MoveJogNeg = 0;
					gAxisControl.AxisCmd[nBonDevIdx].Vertical.MoveJogNeg = 0;
				}
				BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
				Step.eStepNr = eSTEP_WAIT_FOR_COMMAND;			
			}
			break;
	}
	
	
	
	for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
	{
		// Achsposition in mm
		gAxisControl.State.HmiState[nBonDevIdx].Horizontal.rActPosition = (gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ActPosition / rFACTOR_MM_TO_AXIS_UNITS);
		gAxisControl.State.HmiState[nBonDevIdx].Vertical.rActPosition = (gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ActPosition / rFACTOR_MM_TO_AXIS_UNITS);
		
		// Meldung absetzen: Achse wurde referenziert
		if ((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.HomingOk == 1) &&
			(ErrId[nBonDevIdx].bInfoDoneH == 0))
		{
			AppSetEvent(eEVT_INF_AXIS_INITIALIZED, 0, "Horizontal", (nBonDevIdx + 1), &gEventManagement);
			AppResetEvent(eEVT_INF_AXIS_INITIALIZED, &gEventManagement);
			ErrId[nBonDevIdx].bInfoDoneH = 1;
		}
		if ((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.HomingOk == 1) &&
			(ErrId[nBonDevIdx].bInfoDoneV == 0))
		{
			AppSetEvent(eEVT_INF_AXIS_INITIALIZED, 0, "Vertical", (nBonDevIdx + 1), &gEventManagement);
			AppResetEvent(eEVT_INF_AXIS_INITIALIZED, &gEventManagement);
			ErrId[nBonDevIdx].bInfoDoneH = 1;			
		}
	}
	
	// Pr�fen ob Meldung quittiert wurde
	if ((AppIsEventAcknowledged(eEVT_ERR_AXIS, &gEventManagement) == 1) ||
	(AppIsEventAcknowledged(eEVT_ERR_AXIS_LAG_ERROR, &gEventManagement) == 1) ||
	(AppIsEventAcknowledged(eEVT_ERR_AXIS_VELOCITY_LIMIT, &gEventManagement) == 1) ||
	(AppIsEventAcknowledged(eEVT_ERR_AXIS_ACCELERATION_LIMIT, &gEventManagement) == 1) ||
	(AppIsEventAcknowledged(eEVT_ERR_AXIS_PLCOPEN_ERROR, &gEventManagement) == 1) ||
	(AppIsEventAcknowledged(eEVT_ERR_AXIS_DISCRETE_MOTION, &gEventManagement) == 1) ||
	(AppIsEventAcknowledged(eEVT_ERR_AXIS_INVALID_FB_PARAM, &gEventManagement) == 1))
	{
		bAxisErrAck = 1;
	}
	if (AppIsEventAcknowledged(eEVT_ERR_AXIS_CONTROLLER, &gEventManagement) == 1)
	{
		bAxisErrControllerAck = 1;	
	}
	// Pr�fen ob eine Achse einen Fehler hat
	for (nBonDevIdx = 0; nBonDevIdx <= gPar.MachinePar.nBonDevIndex; nBonDevIdx ++)
	{
		if (gPar.MachinePar.MachineModule.bMotorisedAdjustmentEn == 1)
		{
			// Horizontale Achse
			// Regler nicht bereit
			if ((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.DriveStatus.ControllerReady == 0) &&
				(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bHorizontal == 1) &&
				(ErrId[nBonDevIdx].bMsgDoneControllerH == 0) &&
				(Step.bInitDone == 1))
			{
				if(gEventManagement.Events[eEVT_ERR_AXIS_CONTROLLER].bSet == 0 && gEventManagement.Events[eEVT_ERR_AXIS_CONTROLLER].bReset == 0 && gEventManagement.Events[eEVT_ERR_AXIS_CONTROLLER].bWaitForAcknowledge == 0)
				{
					AppSetEvent(eEVT_ERR_AXIS_CONTROLLER, 0, "Horizontal", (nBonDevIdx + 1), &gEventManagement);
					AppResetEvent(eEVT_ERR_AXIS_CONTROLLER, &gEventManagement);
				}
				ErrId[nBonDevIdx].bMsgDoneControllerH = 1;
				ErrId[nBonDevIdx].bHorizontalErr = 1;			
			}
			// Achsfehler
			if ((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != 0) &&
				(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != ErrId[nBonDevIdx].nHorizontalErrId) &&
				(ErrId[nBonDevIdx].bMsgDoneH == 0))
			{
				SetAxisErrorEvent(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID, "Horizontal", (nBonDevIdx));
				//AppResetEvent(eEVT_ERR_AXIS, &gEventManagement);
				ErrId[nBonDevIdx].bHorizontalErr = 1;
				// Kommandos abl�schen
				BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
				BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
				brsmemset((UDINT)&gAxisControl.AxisCmd[nBonDevIdx].Horizontal, 0, sizeof(gAxisControl.AxisCmd[nBonDevIdx].Horizontal));
				ErrId[nBonDevIdx].bMsgDoneH = 1;
			}
			
			// Vertikale Achse
			// Regler nicht bereit
			if ((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.DriveStatus.ControllerReady == 0) &&
				(gPar.MachinePar.MotorisedAdjustment.MotorisedAdjExists[nBonDevIdx].bVertical == 1) &&
				(ErrId[nBonDevIdx].bMsgDoneControllerV == 0) &&
				(Step.bInitDone == 1))
			{
				AppSetEvent(eEVT_ERR_AXIS_CONTROLLER, 0, "Vertical", (nBonDevIdx + 1), &gEventManagement);
				AppResetEvent(eEVT_ERR_AXIS_CONTROLLER, &gEventManagement);	
				ErrId[nBonDevIdx].bMsgDoneControllerV = 1;
				ErrId[nBonDevIdx].bVerticalErr = 1;		
			}
			// Achsfehler		
			if ((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != 0) &&
				(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != ErrId[nBonDevIdx].nVerticalErrId) &&
				(ErrId[nBonDevIdx].bMsgDoneV == 0))
			{
				SetAxisErrorEvent(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID, "Vertical", (nBonDevIdx));
				//AppResetEvent(eEVT_ERR_AXIS, &gEventManagement);
				ErrId[nBonDevIdx].bVerticalErr = 1;
				// Kommandos abl�schen
				BrbClearCallerBox((UDINT)&gAxisControl.CallerBox, sizeof(gAxisControl.CallerBox));
				BrbClearDirectBox((UDINT)&gAxisControl.DirectBox, sizeof(gAxisControl.DirectBox));
				brsmemset((UDINT)&gAxisControl.AxisCmd[nBonDevIdx].Vertical, 0, sizeof(gAxisControl.AxisCmd[nBonDevIdx].Vertical));
				ErrId[nBonDevIdx].bMsgDoneV = 1;
			}
			
			
			// Quittierung wurde ausgef�hrt
			if (bAxisErrAck == 1)
			{
				// Horizontale Achse
				if ((gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != 0) &&
					(gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID != ErrId[nBonDevIdx].nHorizontalErrId))
				{
					gAxisControl.AxisCmd[nBonDevIdx].Horizontal.ErrorAcknowledge = 1;
					ErrId[nBonDevIdx].nHorizontalErrId = gAxisControl.State.AxisStatus[nBonDevIdx].Horizontal.ErrorID;
					ErrId[nBonDevIdx].bHorizontalErr = 0;
					ErrId[nBonDevIdx].bMsgDoneH = 0;
				}
				// Vertikale Achse
				if ((gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != 0) &&
					(gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID != ErrId[nBonDevIdx].nVerticalErrId))
				{
					gAxisControl.AxisCmd[nBonDevIdx].Vertical.ErrorAcknowledge = 1;
					ErrId[nBonDevIdx].nVerticalErrId = gAxisControl.State.AxisStatus[nBonDevIdx].Vertical.ErrorID;
					//ErrId[nBonDevIdx].nVerticalErrId = 0;
					ErrId[nBonDevIdx].bVerticalErr = 0;
					ErrId[nBonDevIdx].bMsgDoneV = 0;
				}		
			}
			if (bAxisErrControllerAck == 1)
			{
				ErrId[nBonDevIdx].bMsgDoneControllerH = 0;
				ErrId[nBonDevIdx].bHorizontalErr = 0;
				ErrId[nBonDevIdx].bMsgDoneControllerV = 0;
				ErrId[nBonDevIdx].bVerticalErr = 0;
			}
		}
		else
		{
			brsmemset((UDINT)&ErrId, 0, sizeof(ErrId));
		}
	}

	// Keine Fehler vorhanden
	if ((gAxisControl.State.AxisStatus[0].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[0].Vertical.ErrorID == 0) &&
		(ErrId[0].bHorizontalErr == 0) && (ErrId[0].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[1].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[1].Vertical.ErrorID == 0) &&
		(ErrId[1].bHorizontalErr == 0) && (ErrId[1].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[2].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[2].Vertical.ErrorID == 0) &&
		(ErrId[2].bHorizontalErr == 0) && (ErrId[2].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[3].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[3].Vertical.ErrorID == 0) &&
		(ErrId[3].bHorizontalErr == 0) && (ErrId[3].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[4].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[4].Vertical.ErrorID == 0) &&
		(ErrId[4].bHorizontalErr == 0) && (ErrId[4].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[5].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[5].Vertical.ErrorID == 0) &&
		(ErrId[5].bHorizontalErr == 0) && (ErrId[5].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[6].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[6].Vertical.ErrorID == 0) &&
		(ErrId[6].bHorizontalErr == 0) && (ErrId[6].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[7].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[7].Vertical.ErrorID == 0) &&
		(ErrId[7].bHorizontalErr == 0) && (ErrId[7].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[8].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[8].Vertical.ErrorID == 0) &&
		(ErrId[8].bHorizontalErr == 0) && (ErrId[8].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[9].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[9].Vertical.ErrorID == 0) &&
		(ErrId[9].bHorizontalErr == 0) && (ErrId[9].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[10].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[10].Vertical.ErrorID == 0) &&
		(ErrId[10].bHorizontalErr == 0) && (ErrId[10].bVerticalErr == 0) &&
		(gAxisControl.State.AxisStatus[11].Horizontal.ErrorID == 0) && (gAxisControl.State.AxisStatus[11].Vertical.ErrorID == 0) &&
		(ErrId[11].bHorizontalErr == 0) && (ErrId[11].bVerticalErr == 0)		)
	{
		bAxisErrAck = 0;
		bAxisErrControllerAck = 0;
		brsmemset((UDINT)&ErrId, 0, sizeof(ErrId));
	}
}

void _EXIT AxisControlEXIT(void)
{
}
