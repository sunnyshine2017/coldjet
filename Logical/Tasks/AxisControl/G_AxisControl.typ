(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: AxisControl
 * File: G_AxisControl.typ
 * Author: kusculart
 * Created: November 24, 2014
 ********************************************************************
 * Global  data types of package AxisControl
 ********************************************************************)

TYPE
	AxisControlCallerBox_TYP : 	STRUCT  (*Reservierungs-Kommandos*)
		Caller : BrbCaller_TYP; (*Reservierungs-Kommandos an die Achse*)
		HmiCaller : HmiCallerAxis_TYP;
		bMoveAllAxesToWorkPosition : BOOL; (*Alle Achsen zur Arbeitsposition fahren. Sind welche davon nicht eingeschaltet bzw. referenziert werden diese eingeschaltet, refereniziert und anschlie�end verfahren.*)
		bInitAllAxesLimits : BOOL; (*Die Achslimits aller Achsen (motorische Verstellung) initialisieren*)
	END_STRUCT;
	AxisControlDirectBox_TYP : 	STRUCT  (*Direkt-Kommandos*)
		HmiCmd : ARRAY[0..nDEV_INDEX_MAX]OF HmiDirectAxis_TYP; (*Kommandos von der Visualisierung*)
		bPowerOn : BOOL;
		bPowerOff : BOOL;
	END_STRUCT;
	AxisControlPar_TYP : 	STRUCT  (*Kommando-Paramter*)
		AxisPar : ARRAY[0..nDEV_INDEX_MAX]OF Basic_parameter_Axis_TYP; (*Parameter an die Achse*)
		HmiPar : HmiPar_TYP; (*Parameter von der Visualisierung*)
	END_STRUCT;
	AxisControlState_TYP : 	STRUCT  (*R�ckmeldungen*)
		bStepperSimulated : BOOL; (*Simulation der Schrittmotoren: 0=Simulation nicht aktiv; 1=Simulation aktiv*)
		AxisState : ARRAY[0..nDEV_INDEX_MAX]OF Basic_axisState_Axis_TYP;
		AxisStatus : ARRAY[0..nDEV_INDEX_MAX]OF Basic_status_Axis_TYP;
		HmiState : ARRAY[0..nDEV_INDEX_MAX]OF HmiStateAxis_TYP;
	END_STRUCT;
	AxisControl_TYP : 	STRUCT  (*Kommando-Struktur*)
		CallerBox : AxisControlCallerBox_TYP;
		DirectBox : AxisControlDirectBox_TYP;
		Par : AxisControlPar_TYP;
		State : AxisControlState_TYP;
		AxisCmd : ARRAY[0..nDEV_INDEX_MAX]OF Basic_command_Axis_TYP; (*Direkt-Kommandos an die Achse*)
	END_STRUCT;
	Basic_command_Axis_TYP : 	STRUCT 
		Vertical : Basic_command_TYP; (*Vertikale Achse*)
		Horizontal : Basic_command_TYP; (*Horizontale Achse*)
	END_STRUCT;
	Basic_command_TYP : 	STRUCT  (*command structure*)
		Power : BOOL; (*switch on the controller*)
		Home : BOOL; (*reference the axis*)
		MoveAbsolute : BOOL; (*move to an defined position*)
		MoveAdditive : BOOL; (*move a defiened distance*)
		MoveVelocity : BOOL; (*start a movement with a defiened velocity*)
		Halt : BOOL; (*stop every active movement once*)
		Stop : BOOL; (*stop every active movement as long as set*)
		MoveJogPos : BOOL; (*move in positive direction as long as is set*)
		MoveJogNeg : BOOL; (*move in negative direction as long as is set*)
		InitAxisLimits : BOOL; (*Die Achslimits dieser Achse werden initialisiert.*)
		ErrorAcknowledge : BOOL; (*reset active errors*)
	END_STRUCT;
	Basic_parameter_Axis_TYP : 	STRUCT 
		Vertical : Basic_parameter_TYP; (*Vertikale Achse*)
		Horizontal : Basic_parameter_TYP; (*Horizontale Achse*)
	END_STRUCT;
	Basic_parameter_TYP : 	STRUCT  (*parameter structure*)
		Position : REAL; (*target-position for MoveAbsolute-Command*)
		Distance : REAL; (*distance for MoveAdditive-Command*)
		Velocity : REAL; (*velocity for MoveVelocity-Command*)
		Direction : USINT; (*direction for commanded movements*)
		Acceleration : REAL; (*acceleration for commanded movements*)
		Deceleration : REAL; (*deceleration for commanded movements*)
		HomePosition : REAL; (*target-position for referencing the axis*)
		HomeMode : USINT; (*homing mode*)
		JogVelocity : REAL; (*velocity for jogging movement*)
		PositiveSwLimit : REAL;
		NegativeSwLimit : REAL;
	END_STRUCT;
	Basic_status_Axis_TYP : 	STRUCT 
		Vertical : Basic_status_TYP; (*Vertikale Achse*)
		Horizontal : Basic_status_TYP; (*Horizontale Achse*)
	END_STRUCT;
	Basic_status_TYP : 	STRUCT  (*status structure*)
		ErrorID : UINT; (*ErrorID of any occured error*)
		ErrorText : ARRAY[0..3]OF STRING[79]; (*Error Text*)
		ActPosition : REAL; (*actual position of the axis*)
		ActVelocity : REAL; (*actual velocity of the axis*)
		InPosition : BOOL; (*Die Achse hat die Position erreicht, auf die Sie mit MC_MoveAbsolute() fahren sollte.*)
		DriveSimulationStatus : USINT; (*Dieser Status gibt Auskunft �ber den Simulationsmodus der Hardware der Achse (Einstellung in der Zuordnungstabelle; Wichtig: NICHT Motorsimulation): 0 = keine HW-Simulation aktiv; 1 = ncACP_SIM_STANDARD; 2 = ncACP_SIM_COMPLETE;*)
		DriveStatus : MC_DRIVESTATUS_TYP; (*actual status of the axis*)
	END_STRUCT;
	Basic_axisState_Axis_TYP : 	STRUCT 
		Vertical : Basic_axisState_TYP; (*Vertikale Achse*)
		Horizontal : Basic_axisState_TYP; (*Horizontale Achse*)
	END_STRUCT;
	Basic_axisState_TYP : 	STRUCT  (*axis state structure*)
		Disabled : BOOL; (*if set, axis is in state Disabled*)
		StandStill : BOOL; (*if set, axis is in state StandsStill*)
		Homing : BOOL; (*if set, axis is in state Homing*)
		Stopping : BOOL; (*if set, axis is in state Stopping*)
		DiscreteMotion : BOOL; (*if set, axis is in state DiscreteMotion*)
		ContinuousMotion : BOOL; (*if set, axis is in state ContinuousMotion*)
		SynchronizedMotion : BOOL; (*if set, axis is in state SynchronizedMotion*)
		ErrorStop : BOOL; (*if set, axis is in state ErrorStop*)
	END_STRUCT;
	HmiDirectAxis_TYP : 	STRUCT 
		Vertical : HmiDirect_TYP; (*Vertikale Achse*)
		Horizontal : HmiDirect_TYP; (*Horizontale Achse*)
	END_STRUCT;
	HmiDirect_TYP : 	STRUCT 
		bStop : BOOL; (*Bewegung toppen*)
		bJogPos : BOOL; (*Tippbetrieb Positiv*)
		bJogNeg : BOOL; (*Tippbetrieb Negativ*)
	END_STRUCT;
	HmiPar_TYP : 	STRUCT 
		AbsPos : ARRAY[0..nDEV_INDEX_MAX]OF AbsPosAxis_TYP; (*Position f�r absolute Bewegung in mm*)
		nRefAxisIdx : USINT; (*Index der Achse f�r das Referenzieren*)
	END_STRUCT;
	AbsPosAxis_TYP : 	STRUCT 
		Vertical : REAL; (*Vertikale Achse, Position in mm*)
		Horizontal : REAL; (*Horizontale Achse, Position in mm*)
	END_STRUCT;
	HmiCallerAxis_TYP : 	STRUCT 
		Vertical : ARRAY[0..nDEV_INDEX_MAX]OF HmiCaller_TYP; (*Vertikale Achse*)
		Horizontal : ARRAY[0..nDEV_INDEX_MAX]OF HmiCaller_TYP; (*Horizontale Achse*)
		bRefAllAxes : BOOL; (*Alle Achsen Referenzieren*)
	END_STRUCT;
	HmiCaller_TYP : 	STRUCT 
		bStartAbsMove : BOOL; (*Starte Absolute Bewegung*)
		bRefAxis : BOOL; (*Achse Referenzieren*)
	END_STRUCT;
	HmiStateAxis_TYP : 	STRUCT 
		Vertical : HmiState_TYP; (*Vertikale Achse*)
		Horizontal : HmiState_TYP; (*Horizontale Achse*)
	END_STRUCT;
	HmiState_TYP : 	STRUCT 
		rActPosition : REAL; (*Aktuelle Achsposition in mm*)
	END_STRUCT;
END_TYPE
