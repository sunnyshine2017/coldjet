(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: ncsdcctrl
 * File: ncsdcctrlInit.st
 * Author: kusculart
 * Created: November 24, 2014
 ********************************************************************
 * Implementation of program ncsdcctrl
 ********************************************************************)

PROGRAM _INIT

(* ### BEGIN gAxis01 ### *)
 
(* initialize general interfaces *)
 gAxis01.size := gAxis01.size;
 gAxis01_HW.DrvIf_Typ := ncSDC_DRVSM16;
 strcpy( ADR(gAxis01_HW.DrvIf_Name[0]), ADR('gAxis01_DrvIf') );
 gAxis01_HW.DiDoIf_Typ := ncSDC_DIDO;
 strcpy( ADR(gAxis01_HW.DiDoIf_Name[0]), ADR('gAxis01_DiDoIf') );
 gAxis01_HW.EncIf1_Typ := ncSDC_ENC16;
 strcpy( ADR(gAxis01_HW.EncIf1_Name[0]), ADR('gAxis01_EncIf1') );
 gAxis01_EncIf1.iLifeCnt := gAxis01_EncIf1.iLifeCnt;
 
(* initialize old-value for delta step calculation *)
 gAxis01_oSetPosOld := gAxis01_DrvIf.oSetPos;
  
(* ### END gAxis01 ### *)


(* ### BEGIN gAxis02 ### *)
 
(* initialize general interfaces *)
 gAxis02.size := gAxis02.size;
 gAxis02_HW.DrvIf_Typ := ncSDC_DRVSM16;
 strcpy( ADR(gAxis02_HW.DrvIf_Name[0]), ADR('gAxis02_DrvIf') );
 gAxis02_HW.DiDoIf_Typ := ncSDC_DIDO;
 strcpy( ADR(gAxis02_HW.DiDoIf_Name[0]), ADR('gAxis02_DiDoIf') );
 gAxis02_HW.EncIf1_Typ := ncSDC_ENC16;
 strcpy( ADR(gAxis02_HW.EncIf1_Name[0]), ADR('gAxis02_EncIf1') );
 gAxis02_EncIf1.iLifeCnt := gAxis02_EncIf1.iLifeCnt;
 
(* initialize old-value for delta step calculation *)
 gAxis02_oSetPosOld := gAxis02_DrvIf.oSetPos;
  
(* ### END gAxis02 ### *)


(* ### BEGIN gAxis03 ### *)
 
(* initialize general interfaces *)
 gAxis03.size := gAxis03.size;
 gAxis03_HW.DrvIf_Typ := ncSDC_DRVSM16;
 strcpy( ADR(gAxis03_HW.DrvIf_Name[0]), ADR('gAxis03_DrvIf') );
 gAxis03_HW.DiDoIf_Typ := ncSDC_DIDO;
 strcpy( ADR(gAxis03_HW.DiDoIf_Name[0]), ADR('gAxis03_DiDoIf') );
 gAxis03_HW.EncIf1_Typ := ncSDC_ENC16;
 strcpy( ADR(gAxis03_HW.EncIf1_Name[0]), ADR('gAxis03_EncIf1') );
 gAxis03_EncIf1.iLifeCnt := gAxis03_EncIf1.iLifeCnt;
 
(* initialize old-value for delta step calculation *)
 gAxis03_oSetPosOld := gAxis03_DrvIf.oSetPos;
  
(* ### END gAxis03 ### *)

(* ### BEGIN gAxis04 ### *)
 
(* initialize general interfaces *)
 gAxis04.size := gAxis04.size;
 gAxis04_HW.DrvIf_Typ := ncSDC_DRVSM16;
 strcpy( ADR(gAxis04_HW.DrvIf_Name[0]), ADR('gAxis04_DrvIf') );
 gAxis04_HW.DiDoIf_Typ := ncSDC_DIDO;
 strcpy( ADR(gAxis04_HW.DiDoIf_Name[0]), ADR('gAxis04_DiDoIf') );
 gAxis04_HW.EncIf1_Typ := ncSDC_ENC16;
 strcpy( ADR(gAxis04_HW.EncIf1_Name[0]), ADR('gAxis04_EncIf1') );
 gAxis04_EncIf1.iLifeCnt := gAxis04_EncIf1.iLifeCnt;
 
(* initialize old-value for delta step calculation *)
 gAxis04_oSetPosOld := gAxis04_DrvIf.oSetPos;
  
(* ### END gAxis04 ### *)


(* ### BEGIN gAxis05 ### *)
      
(* initialize general interfaces *)
      gAxis05.size := gAxis05.size;
      gAxis05_HW.DrvIf_Typ := ncSDC_DRVSM16;
      strcpy( ADR(gAxis05_HW.DrvIf_Name[0]), ADR('gAxis05_DrvIf') );
      gAxis05_HW.DiDoIf_Typ := ncSDC_DIDO;
      strcpy( ADR(gAxis05_HW.DiDoIf_Name[0]), ADR('gAxis05_DiDoIf') );
      gAxis05_HW.EncIf1_Typ := ncSDC_ENC16;
      strcpy( ADR(gAxis05_HW.EncIf1_Name[0]), ADR('gAxis05_EncIf1') );
      gAxis05_EncIf1.iLifeCnt := gAxis05_EncIf1.iLifeCnt;
      
(* initialize old-value for delta step calculation *)
      gAxis05_oSetPosOld := gAxis05_DrvIf.oSetPos;
       
(* ### END gAxis05 ### *)


(* ### BEGIN gAxis06 ### *)
      
(* initialize general interfaces *)
      gAxis06.size := gAxis06.size;
      gAxis06_HW.DrvIf_Typ := ncSDC_DRVSM16;
      strcpy( ADR(gAxis06_HW.DrvIf_Name[0]), ADR('gAxis06_DrvIf') );
      gAxis06_HW.DiDoIf_Typ := ncSDC_DIDO;
      strcpy( ADR(gAxis06_HW.DiDoIf_Name[0]), ADR('gAxis06_DiDoIf') );
      gAxis06_HW.EncIf1_Typ := ncSDC_ENC16;
      strcpy( ADR(gAxis06_HW.EncIf1_Name[0]), ADR('gAxis06_EncIf1') );
      gAxis06_EncIf1.iLifeCnt := gAxis06_EncIf1.iLifeCnt;
      
(* initialize old-value for delta step calculation *)
      gAxis06_oSetPosOld := gAxis06_DrvIf.oSetPos;
       
(* ### END gAxis06 ### *)


(* ### BEGIN gAxis07 ### *)
      
(* initialize general interfaces *)
      gAxis07.size := gAxis07.size;
      gAxis07_HW.DrvIf_Typ := ncSDC_DRVSM16;
      strcpy( ADR(gAxis07_HW.DrvIf_Name[0]), ADR('gAxis07_DrvIf') );
      gAxis07_HW.DiDoIf_Typ := ncSDC_DIDO;
      strcpy( ADR(gAxis07_HW.DiDoIf_Name[0]), ADR('gAxis07_DiDoIf') );
      gAxis07_HW.EncIf1_Typ := ncSDC_ENC16;
      strcpy( ADR(gAxis07_HW.EncIf1_Name[0]), ADR('gAxis07_EncIf1') );
      gAxis07_EncIf1.iLifeCnt := gAxis07_EncIf1.iLifeCnt;
      
(* initialize old-value for delta step calculation *)
      gAxis07_oSetPosOld := gAxis07_DrvIf.oSetPos;
       
(* ### END gAxis07 ### *)


(* ### BEGIN gAxis08 ### *)
      
(* initialize general interfaces *)
      gAxis08.size := gAxis08.size;
      gAxis08_HW.DrvIf_Typ := ncSDC_DRVSM16;
      strcpy( ADR(gAxis08_HW.DrvIf_Name[0]), ADR('gAxis08_DrvIf') );
      gAxis08_HW.DiDoIf_Typ := ncSDC_DIDO;
      strcpy( ADR(gAxis08_HW.DiDoIf_Name[0]), ADR('gAxis08_DiDoIf') );
      gAxis08_HW.EncIf1_Typ := ncSDC_ENC16;
      strcpy( ADR(gAxis08_HW.EncIf1_Name[0]), ADR('gAxis08_EncIf1') );
      gAxis08_EncIf1.iLifeCnt := gAxis08_EncIf1.iLifeCnt;
      
(* initialize old-value for delta step calculation *)
      gAxis08_oSetPosOld := gAxis08_DrvIf.oSetPos;
       
(* ### END gAxis08 ### *)

END_PROGRAM

