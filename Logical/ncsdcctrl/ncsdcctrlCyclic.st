(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: ncsdcctrl
 * File: ncsdcctrlCyclic.st
 * Author: kusculart
 * Created: November 24, 2014
 ********************************************************************
 * Implementation of program ncsdcctrl
 ********************************************************************)

PROGRAM _CYCLIC

(* ### BEGIN gAxis01 ### *)
 
(* life counter emulation of digital inputs *)
 gAxis01_DiDoIf.iLifeCntReference := gAxis01_DiDoIf.iLifeCntReference + 1;
 gAxis01_DiDoIf.iLifeCntNegHwEnd  := gAxis01_DiDoIf.iLifeCntNegHwEnd  + 1;
 gAxis01_DiDoIf.iLifeCntPosHwEnd  := gAxis01_DiDoIf.iLifeCntPosHwEnd  + 1;
  
(* default function model of ACOPOSmicro: convert absolute steps to delta steps - overflow save delta calculation *)
 gAxis01_MotorStep0 := gAxis01_DrvIf.oSetPos - gAxis01_oSetPosOld;
 gAxis01_oSetPosOld := gAxis01_DrvIf.oSetPos;
  
(* ### END gAxis01 ### *)


(* ### BEGIN gAxis02 ### *)
 
(* life counter emulation of digital inputs *)
 gAxis02_DiDoIf.iLifeCntReference := gAxis02_DiDoIf.iLifeCntReference + 1;
 gAxis02_DiDoIf.iLifeCntNegHwEnd  := gAxis02_DiDoIf.iLifeCntNegHwEnd  + 1;
 gAxis02_DiDoIf.iLifeCntPosHwEnd  := gAxis02_DiDoIf.iLifeCntPosHwEnd  + 1;
  
(* default function model of ACOPOSmicro: convert absolute steps to delta steps - overflow save delta calculation *)
 gAxis02_MotorStep0 := gAxis02_DrvIf.oSetPos - gAxis02_oSetPosOld;
 gAxis02_oSetPosOld := gAxis02_DrvIf.oSetPos;
  
(* ### END gAxis02 ### *)


(* ### BEGIN gAxis03 ### *)
 
(* life counter emulation of digital inputs *)
 gAxis03_DiDoIf.iLifeCntReference := gAxis03_DiDoIf.iLifeCntReference + 1;
 gAxis03_DiDoIf.iLifeCntNegHwEnd  := gAxis03_DiDoIf.iLifeCntNegHwEnd  + 1;
 gAxis03_DiDoIf.iLifeCntPosHwEnd  := gAxis03_DiDoIf.iLifeCntPosHwEnd  + 1;
  
(* default function model of ACOPOSmicro: convert absolute steps to delta steps - overflow save delta calculation *)
 gAxis03_MotorStep0 := gAxis03_DrvIf.oSetPos - gAxis03_oSetPosOld;
 gAxis03_oSetPosOld := gAxis03_DrvIf.oSetPos;

//// ToDo MZ Testen ob dies so tats�chlich funktioniert ohne Aufbau eines Schleppfehlers
// TC1_DeltaStepRest[3] := gAxis03_MotorStep0 MOD 2; 
// TC1_DeltaStep[3] := gAxis03_MotorStep0 / 2;
// 
// TC1_DeltaStepOld[3] := TC1_DeltaStepOld[3] + TC1_DeltaStep[3];
// 
// 
// IF( TC1_IsSecondCycleInSDCCycle[3] = FALSE )THEN
// 	
//// 	TC1_DeltaStep[3] := TC1_DeltaStep[3];
// 
// ELSE
// 	TC1_DeltaStep[3] := TC1_DeltaStep[3] + TC1_DeltaStepRest[3];
//	
////	IF( TC1_DeltaStepOld[3] >= (2 * TC1_DeltaStep[3])  )THEN
//		gAxis03_oSetPosOld := gAxis03_DrvIf.oSetPos;
//// 	END_IF
// END_IF
// 
// IF( gAxis03_MotorStep0 <> 0 )THEN
//	TC1_IsSecondCycleInSDCCycle[3] := NOT( TC1_IsSecondCycleInSDCCycle[3] );
// END_IF
//// // Bei jedem zweiten Durchlauf
//// IF( TC1_DeltaStepOld[3] > TC1_DeltaStep[3] )THEN
//// 	
//// 	TC1_DeltaStepOld[3] := 0;
////	
////	TC1_DeltaStep[3] := TC1_DeltaStep[3] - TC1_DeltaStepOld[3];
////	
////	gAxis03_oSetPosOld := gAxis03_DrvIf.oSetPos;
//// END_IF
// 
// gAxis03_MotorStep0 := TC1_DeltaStep[3];
 



(* ### END gAxis03 ### *)

(* ### BEGIN gAxis04 ### *)
 
(* life counter emulation of digital inputs *)
 gAxis04_DiDoIf.iLifeCntReference := gAxis04_DiDoIf.iLifeCntReference + 1;
 gAxis04_DiDoIf.iLifeCntNegHwEnd  := gAxis04_DiDoIf.iLifeCntNegHwEnd  + 1;
 gAxis04_DiDoIf.iLifeCntPosHwEnd  := gAxis04_DiDoIf.iLifeCntPosHwEnd  + 1;
  
(* default function model of ACOPOSmicro: convert absolute steps to delta steps - overflow save delta calculation *)
 gAxis04_MotorStep0 := gAxis04_DrvIf.oSetPos - gAxis04_oSetPosOld;
 //// Test MZ
// w�rde so gehen. IO-Mapping am Stepper-Modul muss auf Cyclic 2 zus�tzlich ge�ndert werden und dieser Task in TK#2 verschieben.
// gAxis04_MotorStep0 := gAxis04_MotorStep0 / 2;
 gAxis04_oSetPosOld := gAxis04_DrvIf.oSetPos;
  
(* ### END gAxis03 ### *)


(* ### BEGIN gAxis05 ### *)
      
(* life counter emulation of digital inputs *)
      gAxis05_DiDoIf.iLifeCntReference := gAxis05_DiDoIf.iLifeCntReference + 1;
      gAxis05_DiDoIf.iLifeCntNegHwEnd  := gAxis05_DiDoIf.iLifeCntNegHwEnd  + 1;
      gAxis05_DiDoIf.iLifeCntPosHwEnd  := gAxis05_DiDoIf.iLifeCntPosHwEnd  + 1;
       
(* default function model of ACOPOSmicro: convert absolute steps to delta steps - overflow save delta calculation *)
      gAxis05_MotorStep0 := gAxis05_DrvIf.oSetPos - gAxis05_oSetPosOld;
      gAxis05_oSetPosOld := gAxis05_DrvIf.oSetPos;
       
(* ### END gAxis05 ### *)


(* ### BEGIN gAxis06 ### *)
      
(* life counter emulation of digital inputs *)
      gAxis06_DiDoIf.iLifeCntReference := gAxis06_DiDoIf.iLifeCntReference + 1;
      gAxis06_DiDoIf.iLifeCntNegHwEnd  := gAxis06_DiDoIf.iLifeCntNegHwEnd  + 1;
      gAxis06_DiDoIf.iLifeCntPosHwEnd  := gAxis06_DiDoIf.iLifeCntPosHwEnd  + 1;
       
(* default function model of ACOPOSmicro: convert absolute steps to delta steps - overflow save delta calculation *)
      gAxis06_MotorStep0 := gAxis06_DrvIf.oSetPos - gAxis06_oSetPosOld;
      gAxis06_oSetPosOld := gAxis06_DrvIf.oSetPos;
       
(* ### END gAxis06 ### *)


(* ### BEGIN gAxis07 ### *)
      
(* life counter emulation of digital inputs *)
      gAxis07_DiDoIf.iLifeCntReference := gAxis07_DiDoIf.iLifeCntReference + 1;
      gAxis07_DiDoIf.iLifeCntNegHwEnd  := gAxis07_DiDoIf.iLifeCntNegHwEnd  + 1;
      gAxis07_DiDoIf.iLifeCntPosHwEnd  := gAxis07_DiDoIf.iLifeCntPosHwEnd  + 1;
       
(* default function model of ACOPOSmicro: convert absolute steps to delta steps - overflow save delta calculation *)
      gAxis07_MotorStep0 := gAxis07_DrvIf.oSetPos - gAxis07_oSetPosOld;
      gAxis07_oSetPosOld := gAxis07_DrvIf.oSetPos;
       
(* ### END gAxis07 ### *)


(* ### BEGIN gAxis08 ### *)
      
(* life counter emulation of digital inputs *)
      gAxis08_DiDoIf.iLifeCntReference := gAxis08_DiDoIf.iLifeCntReference + 1;
      gAxis08_DiDoIf.iLifeCntNegHwEnd  := gAxis08_DiDoIf.iLifeCntNegHwEnd  + 1;
      gAxis08_DiDoIf.iLifeCntPosHwEnd  := gAxis08_DiDoIf.iLifeCntPosHwEnd  + 1;
       
(* default function model of ACOPOSmicro: convert absolute steps to delta steps - overflow save delta calculation *)
      gAxis08_MotorStep0 := gAxis08_DrvIf.oSetPos - gAxis08_oSetPosOld;
      gAxis08_oSetPosOld := gAxis08_DrvIf.oSetPos;
       
(* ### END gAxis08 ### *)

END_PROGRAM

