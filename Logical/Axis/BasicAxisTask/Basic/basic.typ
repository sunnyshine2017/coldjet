
TYPE
	Step_ENUM : 
		(
		STATE_WAIT,
		STATE_POWER_ON,
		STATE_READY := 100,
		STATE_HOME,
		STATE_HALT,
		STATE_STOP,
		STATE_JOG_POSITIVE,
		STATE_JOG_NEGATIVE,
		STATE_MOVE_ABSOLUTE,
		STATE_MOVE_ADDITIVE,
		STATE_MOVE_VELOCITY,
		STATE_ERROR_AXIS := 200,
		STATE_ERROR,
		STATE_ERROR_RESET
		);
	basic_typ : {REDUND_UNREPLICABLE} 	STRUCT  (*control structure*)
		Command : REFERENCE TO Basic_command_TYP; (*command structure*)
		Parameter : REFERENCE TO Basic_parameter_TYP; (*parameter structure*)
		Status : REFERENCE TO Basic_status_TYP; (*status structure*)
		AxisState : REFERENCE TO Basic_axisState_TYP; (*axis state structure*)
	END_STRUCT;
END_TYPE
