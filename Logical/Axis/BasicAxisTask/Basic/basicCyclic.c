/*******************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * PROGRAM: Basic
 * File: Basic.c
 * Author: Bernecker + Rainer
 * Created: December 01, 2009
 ********************************************************************
 * Implementation of Program Basic
 ********************************************************************/

#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
#include <string.h>
#include "global.h"

#define POS_SW_LIMIT	0
#define	NEG_SW_LIMIT	1

_CYCLIC void BasicControl_cyclic(void)
{
	// ******************
	// Interne Funktionen
	// ******************	

	// Ermitteln der SW-Endlagen welche auf der Achse initialisiert sind
	UINT GetSwLimitOfAxis( UDINT AdrAxisRef, USINT PosOrNegLimit, REAL *pValue )
	{
		// Interne Variablen
		MC_ReadParameter_typ	MC_ReadParameter_0 = {};
		UINT					RetVal = ERR_OK;
		
		// Positive SW-Endlage ermittlen
		if( PosOrNegLimit == POS_SW_LIMIT )
		{
			MC_ReadParameter_0.ParameterNumber = mcSW_LIMIT_POS;
		}
		// Negative SW-Endlage ermittlen
		else if( PosOrNegLimit == NEG_SW_LIMIT )
		{
			MC_ReadParameter_0.ParameterNumber = mcSW_LIMIT_NEG;
		}
		// Parameter �bergeben
		MC_ReadParameter_0.Enable = TRUE;
		MC_ReadParameter_0.Axis = AdrAxisRef;
		// Parameter durch Funktionsblock auslesen
		MC_ReadParameter( &MC_ReadParameter_0 );
		// Software-Endlage wurde erfolgreich ausgelesen
		if( MC_ReadParameter_0.Valid == TRUE )
		{
			// Ermittelte SW-Endlage auf �bergebene Variable schreiben
			*pValue = MC_ReadParameter_0.Value;
			RetVal = MC_ReadParameter_0.ErrorID;
		}
		// Ein Fehler ist beim Ermitteln der SW-Endlage aufgetreten
		else
		{
			// Fehlernummer des Funktionsblocks zur�ckgeben
			RetVal = MC_ReadParameter_0.ErrorID;
		}
		// Enable zur�cknehmen
		MC_ReadParameter_0.Enable = FALSE;
		return RetVal;
	}
	
	/* Nullpointer abfangen */
	if( nAxisAdr != 0x00 )
	{
		/* Pointer auf die Achsstruktur ACP10AXIS_typ */
		pAxisStrukt = (ACP10AXIS_typ*)nAxisAdr;
	}

	/***************************************************************
	        Control Sequence
	***************************************************************/
	
	/* status information is read before the step sequencer to attain a shorter reaction time */
	/************************ MC_READSTATUS *************************/
	MC_ReadStatus_0.Enable = !MC_ReadStatus_0.Error;
	MC_ReadStatus_0.Axis = nAxisAdr;
	MC_ReadStatus(&MC_ReadStatus_0);
	BasicControl.AxisState->Disabled             = MC_ReadStatus_0.Disabled;
	BasicControl.AxisState->StandStill           = MC_ReadStatus_0.StandStill;
	BasicControl.AxisState->Stopping             = MC_ReadStatus_0.Stopping;
	BasicControl.AxisState->Homing               = MC_ReadStatus_0.Homing;
	BasicControl.AxisState->DiscreteMotion       = MC_ReadStatus_0.DiscreteMotion;
	BasicControl.AxisState->ContinuousMotion     = MC_ReadStatus_0.ContinuousMotion;
	BasicControl.AxisState->SynchronizedMotion   = MC_ReadStatus_0.SynchronizedMotion;
	BasicControl.AxisState->ErrorStop            = MC_ReadStatus_0.Errorstop;
	
	/********************MC_BR_READDRIVESTATUS***********************/
	MC_BR_ReadDriveStatus_0.Enable = !MC_BR_ReadDriveStatus_0.Error;
	MC_BR_ReadDriveStatus_0.Axis = nAxisAdr; 
	MC_BR_ReadDriveStatus_0.AdrDriveStatus = (UDINT)&(BasicControl.Status->DriveStatus);
	MC_BR_ReadDriveStatus(&MC_BR_ReadDriveStatus_0);
	
	/******************** MC_READACTUALPOSITION *********************/
	MC_ReadActualPosition_0.Enable = (!MC_ReadActualPosition_0.Error);
	MC_ReadActualPosition_0.Axis = nAxisAdr;
	MC_ReadActualPosition(&MC_ReadActualPosition_0);
	if(MC_ReadActualPosition_0.Valid == 1)
	{
	    BasicControl.Status->ActPosition = MC_ReadActualPosition_0.Position;
	}
	
	/******************** MC_READACTUALVELOCITY *********************/
	MC_ReadActualVelocity_0.Enable = (!MC_ReadActualVelocity_0.Error);
	MC_ReadActualVelocity_0.Axis = nAxisAdr;
	MC_ReadActualVelocity(&MC_ReadActualVelocity_0);
	if(MC_ReadActualVelocity_0.Valid == 1)
	{
	    BasicControl.Status->ActVelocity = MC_ReadActualVelocity_0.Velocity;
	}

	/************************ MC_READAXISERROR **********************/
	MC_ReadAxisError_0.Enable = !MC_ReadAxisError_0.Error;
	MC_ReadAxisError_0.Axis = nAxisAdr;
	MC_ReadAxisError_0.DataAddress = (UDINT)&(BasicControl.Status->ErrorText);
	MC_ReadAxisError_0.DataLength = sizeof(BasicControl.Status->ErrorText);
	//strcpy((void*)&MC_ReadAxisError_0.DataObjectName,"acp10etxen");
	brsstrcpy((UDINT)&MC_ReadAxisError_0.DataObjectName, (UDINT)&sErrTxt);
	MC_ReadAxisError(&MC_ReadAxisError_0);

		/**************** CHECK FOR GENERAL AXIS ERROR ******************/
	    if ((MC_ReadAxisError_0.AxisErrorID != 0) && (MC_ReadAxisError_0.Valid == 1))
	    {
	        nAxisStep = STATE_ERROR_AXIS;
	    }
		/***************** CHECK IF POWER SHOULD BE OFF *******************/
	    else if ((BasicControl.Command->Power == 0) && (MC_ReadAxisError_0.Valid == 1))
	    {
	        if ((MC_ReadStatus_0.Errorstop == 1) && (MC_ReadStatus_0.Valid == 1))
	        {
	            nAxisStep = STATE_ERROR_RESET;
	        }
			else
	        {
				nAxisStep = STATE_WAIT;
	        }
	    }
	
	/* central monitoring of stop command attains a shorter reaction time in case of emergency stop */
	/******************CHECK for STOP COMMAND************************/
    if (BasicControl.Command->Stop == 1)
    {
        if ((nAxisStep >= STATE_READY) && (nAxisStep <= STATE_ERROR))
        {
            /* reset all FB execute inputs we use */
            MC_Home_0.Execute = 0;
            MC_Stop_0.Execute = 0;
            MC_MoveAbsolute_0.Execute = 0;
            MC_MoveAdditive_0.Execute = 0;
            MC_MoveVelocity_0.Execute = 0;
            MC_Halt_0.Execute = 0;
            MC_ReadAxisError_0.Acknowledge = 0;
            MC_Reset_0.Execute = 0;

            /* reset user commands */
            BasicControl.Command->Halt = 0;
            BasicControl.Command->Home = 0;
            BasicControl.Command->MoveJogPos = 0;
            BasicControl.Command->MoveJogNeg = 0;
            BasicControl.Command->MoveVelocity = 0;
            BasicControl.Command->MoveAbsolute = 0;
            BasicControl.Command->MoveAdditive = 0;
            nAxisStep = STATE_STOP;
        }
    }
	
	/***************** Initialisieren der SW-Endlagen *****************/
	/* Das Initialisieren der SW-Endlagen muss au�erhalb der Schrittkette geschehen, um die SW-Endlagen auch bei einem Achsfehler */
	/* (Fehlerschritt) initialisieren zu k�nnen */
	if( BasicControl.Command->InitAxisLimits == TRUE )
	{	
		/* Das initialisieren ist nur erlaubt, wenn sich die Achse nicht in diesen Achszust�nden befindet. */
		/* Das Kommando wird nicht verworfen. Es wird ausgef�hrt wenn die Achse in einen f�r diese Funktion	*/
		/* erlaubten Zustand wechselt (z.B. Standstill, Disabled, usw.). */
		if( (BasicControl.AxisState->ContinuousMotion == FALSE) &&
			(BasicControl.AxisState->DiscreteMotion == FALSE) &&
			(BasicControl.AxisState->SynchronizedMotion == FALSE) &&
			(BasicControl.AxisState->Homing == FALSE) &&
			(BasicControl.AxisState->Stopping == FALSE) )
		{
			/* Nullpointer abfangen */
			if( pAxisStrukt != 0x00 )
			{
				/* negative und positive Softwareendlage in die Achsstruktur eintragen */
				pAxisStrukt->limit.parameter.neg_sw_end = (DINT)BasicControl.Parameter->NegativeSwLimit;
				pAxisStrukt->limit.parameter.pos_sw_end = (DINT)BasicControl.Parameter->PositiveSwLimit;
			}
			/* nur die Achslimits sollen initialisiert werden (Subjekt Limits)  */
			MC_BR_InitAxisSubjectPar_0.Subject = ncLIMITS;
	        MC_BR_InitAxisSubjectPar_0.Execute = TRUE;
			/* Funktion wurde ausgef�hrt */
	        if (MC_BR_InitAxisSubjectPar_0.Done == TRUE)
	        {
	           	/* Kommando zur�cksetzen */
				BasicControl.Command->InitAxisLimits = FALSE;
				
				MC_BR_InitAxisSubjectPar_0.Execute = FALSE;
	            /* Achse ist bereits eingeschaltet */
				if( MC_Power_0.Enable == TRUE )
				{
					/* Auf weitere Kommandos warten */
					nAxisStep = STATE_READY;
				}
				/* Achse ist nicht eingeschaltet */
				else
				{
					/* In den Schritt WAIT springen */
					nAxisStep = STATE_WAIT;
				}
	        }
	        
	        /* check if error occured */
	        if (MC_BR_InitAxisSubjectPar_0.Error == TRUE)
	        {
	            /* Kommando zur�cksetzen */
				BasicControl.Command->InitAxisLimits = FALSE;
				
				BasicControl.Status->ErrorID = MC_BR_InitAxisSubjectPar_0.ErrorID;
	            MC_BR_InitAxisSubjectPar_0.Execute = 0;
	            nAxisStep = STATE_ERROR;
	        }
		}
	}
	switch(nAxisStep)
	{
	/******************** WAIT *************************/
	    case STATE_WAIT:  /* STATE: Wait */
	        if (BasicControl.Command->Power == 1)
	        {
	            nAxisStep = STATE_POWER_ON;
	        }
	        else
	        {
	            MC_Power_0.Enable = 0;
	        }
	
	        /* reset all FB execute inputs we use */
	        MC_Home_0.Execute = 0;
	        MC_Stop_0.Execute = 0;
	        MC_MoveAbsolute_0.Execute = 0;
	        MC_MoveAdditive_0.Execute = 0;
	        MC_MoveVelocity_0.Execute = 0;
	        MC_ReadAxisError_0.Acknowledge = 0;
	        MC_Reset_0.Execute = 0;
	
	        /* reset user commands */
	        BasicControl.Command->Halt = 0;
	        BasicControl.Command->Home = 0;
	        BasicControl.Command->MoveJogPos = 0;
	        BasicControl.Command->MoveJogNeg = 0;
	        BasicControl.Command->MoveVelocity = 0;
	        BasicControl.Command->MoveAbsolute = 0;
	        BasicControl.Command->MoveAdditive = 0;
			
	        BasicControl.Status->ErrorID = 0;
	    break;
	
	/******************** POWER ON **********************/
	    case STATE_POWER_ON:  /* STATE: Power on */
	        MC_Power_0.Enable = 1;
	        if (MC_Power_0.Status == 1)
	        {
	            nAxisStep = STATE_READY;
	        }
	        /* if a power error occured go to error state */
	        if (MC_Power_0.Error == 1)
	        {
	            BasicControl.Status->ErrorID = MC_Power_0.ErrorID;
	            nAxisStep = STATE_ERROR;
	        }
	    break;
	
	/******************** READY **********************/
	    case STATE_READY:  /* STATE: Waiting for commands */
	        if (BasicControl.Command->Home == 1)
	        {
	            BasicControl.Command->Home = 0;
	            BasicControl.Status->InPosition = FALSE;
				nAxisStep = STATE_HOME;
	        }
	        else if (BasicControl.Command->Stop == 1)
	        {
	            nAxisStep = STATE_STOP;
	        }
	        else if (BasicControl.Command->MoveJogPos == 1)
	        {
	            BasicControl.Status->InPosition = FALSE;
				nAxisStep = STATE_JOG_POSITIVE;
	        }
	        else if (BasicControl.Command->MoveJogNeg == 1)
	        {
	            BasicControl.Status->InPosition = FALSE;
				nAxisStep = STATE_JOG_NEGATIVE;
	        }
	        else if (BasicControl.Command->MoveAbsolute == 1)
	        {
	            BasicControl.Status->InPosition = FALSE;
				nAxisStep = STATE_MOVE_ABSOLUTE;
	        }
	        else if (BasicControl.Command->MoveAdditive == 1)
	        {
	            BasicControl.Command->MoveAdditive = 0;
	            BasicControl.Status->InPosition = FALSE;
				nAxisStep = STATE_MOVE_ADDITIVE;
	        }
	        else if (BasicControl.Command->MoveVelocity == 1)
	        {
	            BasicControl.Command->MoveVelocity = 0;
	            BasicControl.Status->InPosition = FALSE;
				nAxisStep = STATE_MOVE_VELOCITY;
	        }
	        else if (BasicControl.Command->Halt == 1)
	        {
	            BasicControl.Command->Halt = 0;
	            nAxisStep = STATE_HALT;   
	        }
	    break;
	
	/******************** HOME **********************/
	    case STATE_HOME:  /* STATE: start homing process */
	        MC_Home_0.Position = BasicControl.Parameter->HomePosition;
	        MC_Home_0.HomingMode = BasicControl.Parameter->HomeMode;
	        MC_Home_0.Execute = 1;
	        if (MC_Home_0.Done == 1)
	        {
	            MC_Home_0.Execute = 0;
	            nAxisStep = STATE_READY;
	        }
	        /* if a homing error occured go to error state */
	        if (MC_Home_0.Error == 1)
	        {
	            MC_Home_0.Execute = 0;
	            BasicControl.Status->ErrorID = MC_Home_0.ErrorID;
	            nAxisStep = STATE_ERROR;
	        }
	    break;
	    
	/************************HALT MOVEMENT**************************/    
	    case STATE_HALT:  /* STATE: Stop the active movement */
	        MC_Halt_0.Deceleration = BasicControl.Parameter->Deceleration;
	        MC_Halt_0.Execute = 1;
	        if (MC_Halt_0.Done == 1)
	        {
	            MC_Halt_0.Execute = 0;
	            nAxisStep = STATE_READY;  
	        }
	        /*check if error occured */
	        if (MC_Halt_0.Error == 1)
	        {
	            MC_Halt_0.Execute = 0;
	            BasicControl.Status->ErrorID = MC_Halt_0.ErrorID;
	            nAxisStep = STATE_ERROR;   
	        }
	    break;
	
	/*********************** STOP MOVEMENT *************************/
	    case STATE_STOP: /* STATE: Stop movement as long as command is set */
	        MC_Stop_0.Deceleration = BasicControl.Parameter->Deceleration;
	        MC_Stop_0.Execute = 1;
	        /* if axis is stopped go to ready state */
	        if ((MC_Stop_0.Done == 1) && (BasicControl.Command->Stop == 0))
	        {
	            MC_Stop_0.Execute = 0;
	            nAxisStep = STATE_READY;
	        }
	        /* check if error occured */
	        if (MC_Stop_0.Error == 1)
	        {
	            BasicControl.Status->ErrorID = MC_Stop_0.ErrorID;
	            MC_Stop_0.Execute = 0;
	            nAxisStep = STATE_ERROR;
	        }
	    break;
	
	
	/******************** START JOG MOVEMENT NEGATIVE **********************/
	    // Die Jog-Befehle werde nicht mehr mit einem MC_MoveVelocity() ausgef�hrt, um 
		// den Fehler "5003/5004: pos./neg. SW-Limit reached" zu vermeiden. Dieser
		// kann nicht auftreten wenn nun mit MC_MoveAbsolute() auf die jeweilige neg.
		// oder pos. SW-Endlage gefahren wird.
		case STATE_JOG_POSITIVE:  /* STATE: Start jog movement in positive direction */
		case STATE_JOG_NEGATIVE:  /* STATE: Start jog movement in negative direction */
			// SW-Endlage wurden noch nicht ermittelt
			if( SwLimitAvailable == FALSE )
			{			
				// Positive SW-Endlage ermittlen
				if( nAxisStep == STATE_JOG_POSITIVE )
				{
					// Mit Hilfe der Funktion wird die positive SW-Endlage auf die Parameter-Struktur geschrieben
					StatusGetSwLimit = GetSwLimitOfAxis( nAxisAdr, POS_SW_LIMIT, &BasicControl.Parameter->Position );
				}
				// Negative SW-Endlage ermittlen
				else if( nAxisStep == STATE_JOG_NEGATIVE )
				{
					// Mit Hilfe der Funktion wird die negative SW-Endlage auf die Parameter-Struktur geschrieben
					StatusGetSwLimit = GetSwLimitOfAxis( nAxisAdr, NEG_SW_LIMIT, &BasicControl.Parameter->Position );
				}
				// SW-Endlage wurde nicht erfolgreich ermittelt -> Fehler!
				if( StatusGetSwLimit != ERR_OK )
				{
					// Fehler auf Status-Struktur schreiben
					BasicControl.Status->ErrorID = StatusGetSwLimit;
		            nAxisStep = STATE_ERROR;
					SwLimitAvailable = FALSE;
					// Abarbeitung des Tasks hier beenden;
					// Nachfolgernder Code wird f�r diesen Zyklus nicht mehr bearbeitet!!!
					return;
				}
				// SW-Endlage wurde ermittelt
				SwLimitAvailable = TRUE;
			}
			MC_MoveAbsolute_0.Velocity      = BasicControl.Parameter->JogVelocity;
	        MC_MoveAbsolute_0.Acceleration  = BasicControl.Parameter->Acceleration;
	        MC_MoveAbsolute_0.Deceleration  = BasicControl.Parameter->Deceleration;
	        MC_MoveAbsolute_0.Position      = BasicControl.Parameter->Position;
	        MC_MoveAbsolute_0.Execute = 1;        
	        /* check if jog movement should be stopped */
	        if( ((BasicControl.Command->MoveJogNeg == FALSE) && (nAxisStep == STATE_JOG_NEGATIVE)) || 
				((BasicControl.Command->MoveJogPos == FALSE) && (nAxisStep == STATE_JOG_POSITIVE)) )
	        {
	            SwLimitAvailable = FALSE;
				MC_MoveAbsolute_0.Execute = FALSE;
	            nAxisStep = STATE_HALT;
	        }
	        /* check if error occured */
	        if (MC_MoveAbsolute_0.Error == TRUE)
	        {
	            SwLimitAvailable = FALSE;
				BasicControl.Status->ErrorID = MC_MoveAbsolute_0.ErrorID;
	            MC_MoveAbsolute_0.Execute = FALSE;
	            nAxisStep = STATE_ERROR;
	        }
	    break;
	
	/******************** START ABSOLUTE MOVEMENT **********************/
	    case STATE_MOVE_ABSOLUTE:  /* STATE: Start absolute movement */
	        MC_MoveAbsolute_0.Position      = BasicControl.Parameter->Position;
	        MC_MoveAbsolute_0.Velocity      = BasicControl.Parameter->Velocity;
	        MC_MoveAbsolute_0.Acceleration  = BasicControl.Parameter->Acceleration;
	        MC_MoveAbsolute_0.Deceleration  = BasicControl.Parameter->Deceleration;
	        MC_MoveAbsolute_0.Direction     = BasicControl.Parameter->Direction;
	        MC_MoveAbsolute_0.Execute = 1;
	        
	        if (BasicControl.Command->Halt == 1)
	        {
	            BasicControl.Command->Halt = 0;
	            MC_MoveAbsolute_0.Execute = 0;
	            nAxisStep = STATE_HALT;
	        }
	        /* Erneuten Absolutverfahrbefehl ausf�hren */
			else if( (BasicControl.Command->MoveAbsolute == TRUE) &&
					 (MC_MoveAbsolute_0.Busy == TRUE) )
			{
				/* Execute zur�cksetzen um beim n�chsten Durchlauf die neuen Verfahrparameter zu �bernehmen */
				MC_MoveAbsolute_0.Execute = FALSE;
				nAxisStep = STATE_MOVE_ABSOLUTE;
			}
			/* check if commanded position is reached */
			else if (MC_MoveAbsolute_0.Done == 1)
	        {
	            BasicControl.Status->InPosition = TRUE;
				MC_MoveAbsolute_0.Execute = 0;
	            nAxisStep = STATE_READY;
	        }
	        /* check if error occured */
	        if (MC_MoveAbsolute_0.Error == 1)
	        {
	            BasicControl.Status->ErrorID = MC_MoveAbsolute_0.ErrorID;
	            MC_MoveAbsolute_0.Execute = 0;
	            nAxisStep = STATE_ERROR;
	        }
			/* Kommando zur�cksetzen */
			BasicControl.Command->MoveAbsolute = FALSE;
	    break;
	
	/******************** START ADDITIVE MOVEMENT **********************/
	    case STATE_MOVE_ADDITIVE:  /* STATE: Start additive movement */
		    
			if( SwLimitAvailable == FALSE )
			{
			    // Additive Bewegung in negative Richtung
				if( BasicControl.Parameter->Distance < 0 )
				{
					// Negative Sw-Endlage ermitteln
					StatusGetSwLimit = GetSwLimitOfAxis( nAxisAdr, NEG_SW_LIMIT, &SwLimitValue );
				}
				// Additive Bewegung in positive Richtung
				else
				{
					// Positive SW-Endlage ermitteln
					StatusGetSwLimit = GetSwLimitOfAxis( nAxisAdr, POS_SW_LIMIT, &SwLimitValue );
				}
				// SW-Endlagen konnte Fehlerfrei ermittelt werden
				if( StatusGetSwLimit == ERR_OK )	
				{		
					// Hilfsvariablen f�r die Berechnung mit Werten bef�llen
					ActPosition = BasicControl.Status->ActPosition;
					Distance = BasicControl.Parameter->Distance;
					
					// Additive Bewegung in negative Richtung
					if( BasicControl.Parameter->Distance < 0 )
					{
						// Additive Bewegung w�rde die negative SW-Endlage �berfahren
						if( ActPosition + Distance < SwLimitValue )
						{
							// Errechnen des noch m�glichen Verfahrweges bis zur SW-Endlage
							BasicControl.Parameter->Distance = SwLimitValue - ActPosition;
						}
					}
					// Additive Bewegung in positive Richtung
					else
					{
						// Additive Bewegung w�rde die positive SW-Endlage �berfahren
						if( ActPosition + Distance > SwLimitValue )
						{
							// Errechnen des noch m�glichen Verfahrweges bis zur SW-Endlage
							BasicControl.Parameter->Distance = SwLimitValue - ActPosition;
						}
					}
				}
				else
				{
					// Flag zur�cksetzen; Beim n�chsten betreten dieses
					// Schrittes werden die SW-Endlagen erneut ermittelt
					SwLimitAvailable = FALSE;
					nAxisStep = STATE_ERROR;
					// Abarbeitung des Tasks hier beenden;
					// Nachfolgernder Code wird f�r diesen Zyklus nicht mehr bearbeitet!!!
					return;
				}
				// SW-Endlage wurde ermittelt
				SwLimitAvailable = TRUE;
			}
			
			MC_MoveAdditive_0.Distance      = BasicControl.Parameter->Distance;
			MC_MoveAdditive_0.Velocity      = BasicControl.Parameter->Velocity;
	        MC_MoveAdditive_0.Acceleration  = BasicControl.Parameter->Acceleration;
	        MC_MoveAdditive_0.Deceleration  = BasicControl.Parameter->Deceleration;
	        MC_MoveAdditive_0.Execute = 1;
	        /* check if commanded distance is reached */
	        if (BasicControl.Command->Halt == 1)
	        {
	            BasicControl.Command->Halt = 0;
	            MC_MoveAdditive_0.Execute = 0;
	            // Flag zur�cksetzen; Beim n�chsten betreten dieses
				// Schrittes werden die SW-Endlagen erneut ermittelt
				SwLimitAvailable = FALSE;
				nAxisStep = STATE_HALT;   
	        }
	        else if (MC_MoveAdditive_0.Done == 1)
	        {
	            MC_MoveAdditive_0.Execute = 0;
	            // Flag zur�cksetzen; Beim n�chsten betreten dieses
				// Schrittes werden die SW-Endlagen erneut ermittelt
				SwLimitAvailable = FALSE;
				nAxisStep = STATE_READY;
	        }
	        /* check if error occured */
	        if (MC_MoveAdditive_0.Error == 1)
	        {
	            BasicControl.Status->ErrorID = MC_MoveAdditive_0.ErrorID;
	            MC_MoveAdditive_0.Execute = 0;
	            // Flag zur�cksetzen; Beim n�chsten betreten dieses
				// Schrittes werden die SW-Endlagen erneut ermittelt
				SwLimitAvailable = FALSE;
				nAxisStep = STATE_ERROR;
	        }
	    break;
	
	/******************** START VELOCITY MOVEMENT **********************/
	    case STATE_MOVE_VELOCITY:  /* STATE: Start velocity movement */
	        MC_MoveVelocity_0.Velocity      = BasicControl.Parameter->Velocity;
	        MC_MoveVelocity_0.Acceleration  = BasicControl.Parameter->Acceleration;
	        MC_MoveVelocity_0.Deceleration  = BasicControl.Parameter->Deceleration;
	        MC_MoveVelocity_0.Direction     = BasicControl.Parameter->Direction;
	        MC_MoveVelocity_0.Execute = 1;
	        /* check if commanded velocity is reached */
	        if (BasicControl.Command->Halt == 1)
	        {
	            BasicControl.Command->Halt = 0;
	            MC_MoveVelocity_0.Execute = 0;
	            nAxisStep = STATE_HALT;    
	        }
	        else if (MC_MoveVelocity_0.InVelocity == 1)
	        {
	            MC_MoveVelocity_0.Execute = 0;
	            nAxisStep = STATE_READY;
	        }
	        /* check if error occured */
	        if (MC_MoveVelocity_0.Error == 1)
	        {
	            BasicControl.Status->ErrorID = MC_MoveVelocity_0.ErrorID;
	            MC_MoveVelocity_0.Execute = 0;
	            nAxisStep = STATE_ERROR;
	        }
	    break;
		
	/******************** FB-ERROR OCCURED *************************/
	    case STATE_ERROR:  /* STATE: Error */
	        /* check if FB indicates an axis error */
	        if (MC_ReadAxisError_0.AxisErrorCount != 0)
	        {
	            nAxisStep = STATE_ERROR_AXIS;
	        }
	        else
	        {
	            if (BasicControl.Command->ErrorAcknowledge == 1)
	            {
	                BasicControl.Command->ErrorAcknowledge = 0;
	                BasicControl.Status->ErrorID = 0;
	                /* reset axis if it is in axis state ErrorStop */
	                if ((MC_ReadStatus_0.Errorstop == 1) && (MC_ReadStatus_0.Valid == 1))
	                {
	                    nAxisStep = STATE_ERROR_RESET;
	                }
	                else
	                {
	                    nAxisStep = STATE_WAIT;
	                }
	            }
	        }
	    break;
	
	/******************** AXIS-ERROR OCCURED *************************/
	    case STATE_ERROR_AXIS:  /* STATE: Axis Error */
	        if (MC_ReadAxisError_0.Valid == 1)
	        {
	            if (MC_ReadAxisError_0.AxisErrorID != 0)
	            {
	                BasicControl.Status->ErrorID = MC_ReadAxisError_0.AxisErrorID;
	            }
	            MC_ReadAxisError_0.Acknowledge = 0;
	            if (BasicControl.Command->ErrorAcknowledge == 1)
	            {
	                BasicControl.Command->ErrorAcknowledge = 0;
	                /* acknowledge axis error */
	                if (MC_ReadAxisError_0.AxisErrorID != 0)
	                {
	                    MC_ReadAxisError_0.Acknowledge = 1;
	                }
	            }
	            if (MC_ReadAxisError_0.AxisErrorCount == 0)
	            {
	                BasicControl.Status->ErrorID = 0;
	                /* reset axis if it is in axis state ErrorStop */
	                if ((MC_ReadStatus_0.Errorstop == 1) && (MC_ReadStatus_0.Valid == 1))
	                {
	                    nAxisStep = STATE_ERROR_RESET;
	                }
	                else
	                {
	                    nAxisStep = STATE_WAIT;
	                }
	            }
	        }
	    break;
	
	/******************** RESET DONE *************************/
	    case STATE_ERROR_RESET:  /* STATE: Wait for reset done */
	        MC_Reset_0.Execute = 1;
	        /* reset MC_Power.Enable if this FB is in Error*/
	        if(MC_Power_0.Error == 1)
	        {
	            MC_Power_0.Enable = 0;
	        }
	        if(MC_Reset_0.Done == 1)
	        {
	            MC_Reset_0.Execute = 0;
	            nAxisStep = STATE_WAIT;
	        }
	        else if (MC_Reset_0.Error == 1)
	        {
	            MC_Reset_0.Execute = 0;
	            nAxisStep = STATE_ERROR;   
	        }
	    break;
	    
	/******************** SEQUENCE END *************************/
	}
	
	
	/* Nullpointer abfangen */
	if( pAxisStrukt != 0x00 )
	{
		/* ACOPOS-Simulationsmodus dieser Achse ermitteln */
		BasicControl.Status->DriveSimulationStatus = pAxisStrukt->simulation.acp_sim;
	}
	
	/***************************************************************
	        Function Block Calls
	***************************************************************/
	
	/************************** MC_POWER ****************************/
	MC_Power_0.Axis = nAxisAdr;  /* pointer to axis */
	MC_Power(&MC_Power_0);
	
	/************************** MC_HOME *****************************/
	MC_Home_0.Axis = nAxisAdr;
	MC_Home(&MC_Home_0);
	
	/********************** MC_MOVEABSOLUTE *************************/
	MC_MoveAbsolute_0.Axis = nAxisAdr;
	MC_MoveAbsolute(&MC_MoveAbsolute_0);
	/********************** MC_MOVEADDITIVE *************************/
	MC_MoveAdditive_0.Axis = nAxisAdr;
	MC_MoveAdditive(&MC_MoveAdditive_0);
	
	/********************** MC_MOVEVELOCITY *************************/
	MC_MoveVelocity_0.Axis = nAxisAdr;
	MC_MoveVelocity(&MC_MoveVelocity_0);
	
	/************************** MC_STOP *****************************/
	MC_Stop_0.Axis = nAxisAdr;
	MC_Stop(&MC_Stop_0);
	
	/**************************MC_HALT*******************************/
	MC_Halt_0.Axis = nAxisAdr;
	MC_Halt(&MC_Halt_0);
	
	/************************** MC_RESET ****************************/
	MC_Reset_0.Axis = nAxisAdr;
	MC_Reset(&MC_Reset_0);
	
	/****************** MC_BR_InitAxisSubjectPar ********************/
	MC_BR_InitAxisSubjectPar_0.Axis = nAxisAdr;
	MC_BR_InitAxisSubjectPar(&MC_BR_InitAxisSubjectPar_0);

}


