/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Basic
 * File: basicInit.c
 * Author: kusculart
 * Created: November 24, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif


_INIT void BasicControl_init(void)
{

/* get axis object */
nAxisAdr = (UDINT)&gAxis01;
BasicControl.Command	= &gAxisControl.AxisCmd[0].Horizontal;
BasicControl.Parameter	= &gAxisControl.Par.AxisPar[0].Horizontal;
BasicControl.AxisState	= &gAxisControl.State.AxisState[0].Horizontal;
BasicControl.Status		= &gAxisControl.State.AxisStatus[0].Horizontal;

brsstrcpy((UDINT)&sErrTxt, (UDINT)&"acp10etxde"); 

nAxisStep = STATE_WAIT;  /* start step */
}
