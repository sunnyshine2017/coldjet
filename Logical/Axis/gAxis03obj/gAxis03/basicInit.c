/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Basic
 * File: basicInit.c
 * Author: kusculart
 * Created: November 24, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif


_INIT void BasicControl_init(void)
{

/* get axis object */
nAxisAdr = (UDINT)&gAxis03;
BasicControl.Command	= &gAxisControl.AxisCmd[1].Horizontal;
BasicControl.Parameter	= &gAxisControl.Par.AxisPar[1].Horizontal;
BasicControl.AxisState	= &gAxisControl.State.AxisState[1].Horizontal;
BasicControl.Status		= &gAxisControl.State.AxisStatus[1].Horizontal;

brsstrcpy((UDINT)&sErrTxt, (UDINT)&"acp10etxde"); 

nAxisStep = STATE_WAIT;  /* start step */
}
