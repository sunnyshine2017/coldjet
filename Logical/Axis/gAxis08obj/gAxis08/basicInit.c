/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Basic
 * File: basicInit.c
 * Author: kusculart
 * Created: November 24, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif


_INIT void BasicControl_init(void)
{

/* get axis object */
nAxisAdr = (UDINT)&gAxis08;
BasicControl.Command	= &gAxisControl.AxisCmd[3].Vertical;
BasicControl.Parameter	= &gAxisControl.Par.AxisPar[3].Vertical;
BasicControl.AxisState	= &gAxisControl.State.AxisState[3].Vertical;
BasicControl.Status		= &gAxisControl.State.AxisStatus[3].Vertical;

brsstrcpy((UDINT)&sErrTxt, (UDINT)&"acp10etxde"); 

nAxisStep = STATE_WAIT;  /* start step */
}
