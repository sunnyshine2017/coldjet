/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Basic
 * File: basicInit.c
 * Author: kusculart
 * Created: November 24, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif


_INIT void BasicControl_init(void)
{

/* get axis object */
nAxisAdr = (UDINT)&gAxis05;
BasicControl.Command	= &gAxisControl.AxisCmd[2].Horizontal;
BasicControl.Parameter	= &gAxisControl.Par.AxisPar[2].Horizontal;
BasicControl.AxisState	= &gAxisControl.State.AxisState[2].Horizontal;
BasicControl.Status		= &gAxisControl.State.AxisStatus[2].Horizontal;

brsstrcpy((UDINT)&sErrTxt, (UDINT)&"acp10etxde"); 

nAxisStep = STATE_WAIT;  /* start step */
}
