/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: Basic
 * File: basicInit.c
 * Author: kusculart
 * Created: November 24, 2014
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif


_INIT void BasicControl_init(void)
{

/* get axis object */
nAxisAdr = (UDINT)&gAxis04;
BasicControl.Command	= &gAxisControl.AxisCmd[1].Vertical;
BasicControl.Parameter	= &gAxisControl.Par.AxisPar[1].Vertical;
BasicControl.AxisState	= &gAxisControl.State.AxisState[1].Vertical;
BasicControl.Status		= &gAxisControl.State.AxisStatus[1].Vertical;

brsstrcpy((UDINT)&sErrTxt, (UDINT)&"acp10etxde"); 

nAxisStep = STATE_WAIT;  /* start step */
}
