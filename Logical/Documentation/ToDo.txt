
ZIR:
	- Eine Warnung/Info ausgeben wenn nicht geklebt werden kann, weil
		a) IO.Input.bBonDevEnable nicht vorhanden ist
		b) ExtCtrlIf: IO.Input.bBondingAllowed
	  nicht auf TRUE gesetzt sind!!! Dies spart viel viel Zeit bei der Fehlersuche!
		
NIE:
	-Verriegeln, dass in der Konfiguration Klebek�pfe nicht der gleiche Triggersensor bei zwei K�pfen mit unterschiedlichen
	 Gebern verwendet werden kann. Niedrige Priorit�t, ist mir nur aufgefallen.
	-Die Druckmarken- und Triggersensoren die einem Klebekopf mit der Funktionalit�t eSEN_TYPE_PRINT_MARK_TRIG_SENSOR zugeordnet sind,
	 d�rfen nicht gleichzeitig einem anderen Klebekopf zugeordnet werden. Dies muss verriegelt werden. Durch Dropdown-Felder m�glich.
	-Die Druckmarkensensoren die einem Klebekopf mit negativen Sensorabstand zugeordnet sind, d�rfen nicht gleichzeitig einem anderen
	 Klebekopf zugeordnet werden. Dies muss verriegelt werden. Durch Dropdown-Felder m�glich.
