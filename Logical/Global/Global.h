/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Package: Global
 * File: Global.h
 * Author: niedermeierr
 * Created: December 18, 2012
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif
#include <string.h>
#include <AnsiCFunc.h>

#define TRUE	1
#define FALSE	0
