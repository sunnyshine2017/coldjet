(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * File: Global.typ
 * Author: niedermeierr
 * Created: September 14, 2012
 ********************************************************************
 * Global data types of project PlanatolDs
 ********************************************************************)

TYPE
	EncoderType_ENUM : 
		(
		eENCODER_TYPE_NONE := -1, (*Kein Geber*)
		eENCODER_TYPE_ENCODER := 0, (*normaler Drehgeber*)
		eENCODER_TYPE_VIRTUAL := 1, (*Virtueller Geber*)
		eENCODER_TYPE_TRIGGER := 2 (*Triggersensor zur Geschwindigkeits- und Positionsbestimmung*)
		);
	CallerIds_ENUM : 
		(
		eCALLERID_NONE,
		eCALLERID_MAIN_LOGIC,
		eCALLERID_ENCODER,
		eCALLERID_BONDING_DEVICE,
		eCALLERID_TRIGGER_SENSOR,
		eCALLERID_PRINT_MARK_SENSOR,
		eCALLERID_AXIS_CONTROL,
		eCALLERID_EVENTHANDLING,
		eCALLERID_PAR_HANDLING,
		eCALLERID_CALC_POS,
		eCALLERID_SYSTEM,
		eCALLERID_VISU,
		eCALLERID_EXT_CTRL_IF,
		eCALLERID_TEMP_CTRL
		);
	SensorType_ENUM : 
		(
		eSEN_TYPE_TRIGGER_SENSOR,
		eSEN_TYPE_ENCODER,
		eSEN_TYPE_PRINT_MARK_SENSOR,
		eSEN_TYPE_PRINT_MARK_TRIG_SENSOR
		);
	MotAdjustAlignment_ENUM : 
		(
		eVERTICAL := 0,
		eHORIZONTAL
		);
	TempZone_ENUM : 
		(
		eTEMP_ZONE_TUBE := 0, (*Temperaturzone: Schlauch zum Klebekopf*)
		eTEMP_ZONE_BON_DEV, (*Temperaturzone: Kopf des Klebekopfes*)
		eTEMP_ZONE_RESERVE (*Teperaturzone: Reservezone am Klebekopf*)
		);
	Par_TYP : 	STRUCT 
		MachinePar : MachinePar_TYP; (*Maschinen Parameter*)
		ProductPar : ProductPar_TYP; (*Produkt Parameter*)
	END_STRUCT;
	DcsPar_TYP : 	STRUCT 
		rPosComp : REAL; (*Positionskompensation*)
		rOnComp : REAL; (*Aktor Totzeitkompensation: Ausgang fr�her Einschalten*)
		rOffComp : REAL; (*Aktor Totzeitkompensation: Ausgang fr�her Ausschalten*)
		rHysteresis : REAL; (*Hysteres Fenster*)
		bDisNegDir : BOOL; (*Negative Z�hlrichtung der Position ignorieren*)
	END_STRUCT;
	TempCtrlModulePar_TYP : 	STRUCT 
		Tank : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF TempCtrlTankPar_TYP; (*In dieser Struktur befinden sich die Parameter des Tanks der Modulseite "Heizung"*)
		BonDev : ARRAY[0..nDEV_INDEX_MAX]OF TempCtrlBonDevPar_TYP; (*In dieser Struktur befinden sich die Parameter der Modulseite "Heizung" f�r alle Klebek�pfe*)
	END_STRUCT;
	TempCtrlTankPar_TYP : 	STRUCT 
		rSetTemp : REAL := 50.0; (*Soll-Temperatur des ausgew�hlten Tanks in [�C]*)
		rTempStandby : REAL := 30.0; (*Standby-Temperatur des ausgew�hlten Tanks in [�C]*)
	END_STRUCT;
	TempCtrlBonDevPar_TYP : 	STRUCT 
		bEnable : BOOL; (*Die Heizzonen des ausgew�hlten Klebekopfes beheizen, wenn die Heizung des dazugeh�rige Tanks eingeschaltet wird.*)
		rSetTemp : ARRAY[0..nIDX_TEMP_CTRL_ZONE_MAX]OF REAL := [3(50.0)]; (*Soll-Temperatur der jeweiligen Heizzonen des ausgew�hlten Klebekopfes.*)
	END_STRUCT;
	TempCtrlConfigPar_TYP : 	STRUCT 
		Tank : ARRAY[0..nIDX_TEMP_CTRL_TANK_MAX]OF TempCtrlTankCfgPar_TYP; (*In dieser Struktur befinden sich die Konfigurations-Parameter des Tanks der Funktionalit�t "Heizung".*)
		BonDev : ARRAY[0..nDEV_INDEX_MAX]OF TempCtrlBonDevCfgPar_TYP; (*In dieser Struktur befinden sich die Konfigurations-Parameter der Klebek�pfe f�r die Funktionalit�t "Heizung".*)
	END_STRUCT;
	TempCtrlBonDevCfgPar_TYP : 	STRUCT 
		bExist : BOOL; (*Der ausgew�hlte Klebekopf ist mit einer Heizung best�ckt.*)
		nTankIdx : SINT; (*Index des beheizten Tanks, zu dem der ausgew�hlte Klebekopf geh�rt (verschlaucht ist).*)
		rToleranzMin : REAL; (*MinimaleToleranz in [�C]. Sinkt die Ist-Temperatur unter diese Toleranz wird ein Fehler ausgel�st.*)
		rToleranzMax : REAL; (*Maximale Toleranz in [�C]. �bersteigt die Ist-Temperatur diese Toleranz wird ein Fehler ausgel�st.*)
		rTempStandby : REAL; (*Standby-Temperatur der aktiven Heizzonen des ausgew�hlten Kopfes.*)
		TempZone : ARRAY[0..nIDX_TEMP_CTRL_ZONE_MAX]OF TempCtrlTempZoneCfgPar_TYP; (*Parameter der jeweiligen Heizzone (Idx: 0 = Schlauch; 1 = Kopf; 2 = Reserve;). Enthalten sind Grenzwerte und die f�r die Temperaturregelung ben�tigt Parameter.*)
	END_STRUCT;
	TempCtrlTankCfgPar_TYP : 	STRUCT 
		bExist : BOOL; (*Der ausgew�hlte Tank ist mit einer Heizung best�ckt.*)
		rToleranzMin : REAL; (*MinimaleToleranz in [�C]. Sinkt die Ist-Temperatur unter diese Toleranz wird ein Fehler ausgel�st.*)
		rToleranzMax : REAL; (*Maximale Toleranz in [�C]. �bersteigt die Ist-Temperatur diese Toleranz wird ein Fehler ausgel�st.*)
		rTempDiffWarning : REAL; (*Prozentwert in [%] wann innerhalb des Toleranzbereiches aller Heizzonen (Tank und die Heizzonen der K�pfe) eine Warnung ausgel�st wird.*)
		TempZonePar : TempCtrlTempZoneCfgPar_TYP; (*In dieser Struktur sind Grenzwerte und die f�r die Temperaturregelung ben�tigten Parameter enthalten.*)
	END_STRUCT;
	TempCtrlTempZoneCfgPar_TYP : 	STRUCT 
		bZoneExist : BOOL; (*Nur f�r Teilheizzonen der K�pfe g�ltig. Bei Tanks wird dieses Exist nicht beachtet.*)
		rSetTempMax : REAL; (*Maximal eingebbare Temperatur in [�C].*)
		rGain : REAL; (*PID-Regler: Proportionalverst�rkung.*)
		rIntegrationTime : REAL; (*PID-Regler: Integrationszeit (Nachstellzeit) des Integralanteils. Einheit: [s]. *)
		rDerivativeTime : REAL; (*PID-Regler: Differentiationszeit (Vorhaltezeit) des Differentialanteils. Einheit: [s].*)
		rFilterTime : REAL; (*PID-Regler: Filterzeitkonstante des Differentialanteils. Einheit: [s].*)
		rMinOut : REAL; (*PID-Regler: untere Stellgr��enbegrenzung.*)
		rMaxOut : REAL; (*PID-Regler: Obere Stellgr��enbegrenzung.*)
	END_STRUCT;
	MachinePar_TYP : 	STRUCT 
		SystemCleaning : SystemCleaning_TYP;
		EncoderPar : ARRAY[0..nIDX_ENCODER_MAX]OF EncPar_TYP; (*Encoder Paramter*)
		MachineModule : MachineModule_TYP; (*Maschinenmodule*)
		PrintMarkPar : PrintMarkPar_TYP; (*Druckmarken Parameter*)
		TankType : Tank_TYP; (*Tanktypen*)
		MotorisedAdjustment : MotorisedAdjustment_TYP; (*Axiales Verfahren*)
		PressureControl : PressureControl_TYP; (*Geschwindigkeitsabh�ngige Druckregelung*)
		StitchmodeDistance : StitchmodeDistance_TYP;
		TempCtrlConfig : TempCtrlConfigPar_TYP; (*Konfigurationsparameter der Funktionalit�t "Heizung"*)
		nScreensaverTime : UDINT; (*Zeit f�r Bildschirmschoner [s]*)
		nBonDevIndex : SINT; (*Index Auftragek�pfe (von 0 bis 11)*)
		nTriggerSenIndex : SINT; (*Index Triggersensoren (von 0 bis 11)*)
		nEncoderIndex : SINT; (*Index Geber (von 0 bis 4; Idx 0: simulierter Geber; ab Idx 1 bis 4: 4 reale Geber)*)
		nProductDetectIndex : SINT; (*Index Produkterkennung  (von 0 bis 11)*)
		nPrintMarkSensorIndex : SINT; (*Index Druckmarkensensor (von 0 bis 11)*)
		rEnVelocity : REAL; (*Freigabegeschwindigkeit in mm/s*)
		rOpenTime : REAL; (*Reinigung: �ffnungszeit in ms (von 50ms bis 250ms)*)
		nNrOfRounds : USINT; (*Reinigung: Schussanzahl (von 1 bis 25)*)
		nHopperFillingPeriod : UDINT; (*Bef�llungsdauer in ms  des Trichterkopfes ausgel�st �ber die Schnittstelle zur Digitaldruckmaschine (manroland Dubai)*)
		nHopperCleaningPeriod : UDINT; (*Reinigungsdauer in ms des Trichterkopfes ausgel�st �ber die Schnittstelle zur Digitaldruckmaschine (manroland Dubai)*)
		nCollectorFillingPeriod : UDINT; (*Bef�llungsdauer in ms  des Klebekopfes vor dem Sammelzylinder ausgel�st �ber die Schnittstelle zur Digitaldruckmaschine (manroland Dubai)*)
		nCollectorCleaningPeriod : UDINT; (*Reinigungsdauer in ms des Klebekopfes vor dem Sammelzylinder ausgel�st �ber die Schnittstelle zur Digitaldruckmaschine (manroland Dubai)*)
	END_STRUCT;
	SystemCleaning_TYP : 	STRUCT 
		bEnable : BOOL; (*Systemreinigung (Sp�lung der Schleuche) ist vorhanden*)
		nGlueTankCleaningPeriod : UINT; (*Dauer der Systemreinigung mittels Klebstofftank-Pumpe*)
		nDetergentTankCleaningPeriod : UINT; (*Dauer der Systemreinigung mittels Reinigungstank-Pumpe*)
	END_STRUCT;
	ProductPar_TYP : 	STRUCT 
		BonDevPar : ARRAY[0..nDEV_INDEX_MAX]OF BonDevPar_TYP; (*Parameter Auftragek�pfe*)
		TriggerSenPar : TriggerSenPar_TYP; (*Triggersensor Parameter*)
		PmSenPar : PmSenPar_TYP; (*Druckmarkensensor Parameter*)
		TempCtrlPar : TempCtrlModulePar_TYP; (*Parameter Heizung*)
	END_STRUCT;
	StitchSize_ENUM : 
		(
		eSTITCH_SIZE_SMALL,
		eSTITCH_SIZE_MEDIUM,
		eSTITCH_SIZE_LARGE
		);
	BonDevPar_TYP : 	STRUCT 
		Switches : BondingIdx_TYP; (*Nocken*)
		ProductDetection : ProductDet_TYP; (*Produkterkennung*)
		nSwitchIndexMax : ARRAY[0..nIDX_PRINT_MARK_MAX]OF SINT; (*Index f�r Nocken*)
		rProductLen : REAL; (*Produktl�nge in [mm]*)
		rProductLenAdapted : REAL; (*Angepasste Produktl�nge in [mm]; In diesem Parameter wurde die Produktl�nge um die prozentuale Produktl�ngenanpassung (rReduceProductLenPercent) verk�rzt.*)
		rSenOffset : REAL; (*Sensorabstand in [mm]*)
		rCylinderPerimeter : REAL; (*Zylinderumfang in [mm]*)
		rPmProductOffset : REAL; (*Abstand Produktbeginn zu Druckmarke in [mm]*)
		eIdxSenType : DINT; (*Index f�r Sensortyp*)
		nIdxSenSource : SINT; (*Index f�r Sensorquelle*)
		nIdxPrintMarkSensorSource : SINT; (*Nur Sensortyp PrintMark_Trigger: Index des Druckmarkensensors*)
		nIdxEncoder : SINT; (*Index f�r Geber ( Idx 0: simulierter Geber; ab Idx 1 bis 4: 4 reelle Geber  )*)
		nIdxPrintMarkEncoder : SINT; (*Nur Sensortyp PrintMark_Trigger: Index des Gebers des Druckmarkensensors*)
		rDistancePrintMarkTriggerSensor : REAL; (*Bei PrintMark_Trigger: Distanz in [mm] vom Druckmarken-Sensor bis zum Triggersensor*)
		rDistancePrintMark_VelocityJump : REAL; (*Bei PrintMark_Trigger: Distanz in [mm] vom Druckmarken-Sensor bis zu der Stelle, an der sich die Geschwindigkeit ver�ndert (wird nur ben�tigt, wenn der Geber-Index des Druckmarkensensors und der des Triggersensors nicht gleich sind!)*)
		bStitchmode : BOOL; (*Stitchmode aus/an*)
		nStitchSize : DINT; (*Angew�hlte Stitchgr��e (0=Small, 1=Medium, 2=Large)*)
		rStitchCloseDistance : REAL := 0.2; (*Strecke die das Ventil geschlossen bleiben soll in [mm] (Min: 0,2 mm)*)
		bBonDevEn : BOOL; (*Enable Auftragekopf*)
		AxesPosition : AxesPosition_TYP; (*Diese Struktur beinhaltet die m�glichen Anfahrpositionen der motorisch verstellbaren Klebek�pfe*)
		rReduceProductLenPercent : REAL; (*Um diesen Prozentbetrag wird die Produktl�nge verk�rzt/angepasst, um das Kleben von Endlosprodukten zu erm�glichen.*)
		rValveCompensationOpenTime : REAL; (*Klebekopf Leimaustritt-Kompensationszeit in [ms]. Dieser Wert gibt die Zeitdauer an, vom Zeitpunkt des Ausgangs des Klebesignals aus dem X20DC-Modul bis Klebstoff aus dem Klebeventil austritt.*)
		rValveCompensationCloseTime : REAL; (*Klebekopf Leimstopp-Kompensationszeit in [ms]. Dieser Wert gibit die Zeitdauer an, vom Zeitpunkt des Abfallens des Klebesinals aus dem X20DC-Modul bis kein Klebstoff mehr aus dem Klebeventil austritt.*)
	END_STRUCT;
	PmSenPar_TYP : 	STRUCT 
		bPosNegDetection : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Positive oder Negative Auswertung der Flanken*)
	END_STRUCT;
	TriggerSenPar_TYP : 	STRUCT 
		bPosNegDetection : ARRAY[0..nDEV_INDEX_MAX]OF BOOL; (*Positive oder Negative Auswertung der Flanken*)
	END_STRUCT;
	ProductDet_TYP : 	STRUCT 
		nIdxProductDetect : SINT; (*Index des zugeh�rigen Produkterkennungs-Sensor*)
		bProductDetExist : BOOL; (*Produkterkennung ist aktiviert*)
		nOffset : UDINT; (*Abstand vom Produkterkennungs-Sensor bis zum Auftragkopf in 1/100 mm*)
	END_STRUCT;
	ParPermanentTempCtrlTank_TYP : 	STRUCT 
		bHeatingOn : BOOL;
	END_STRUCT;
	ParPermanent_TYP : 	STRUCT 
		nVisLanguage : UINT;
		nOperatingSecondsTotal : UDINT; (*Aktuelle Betriebssekunden*)
		nOperatingSecondsService : UDINT; (*Aktuelle Betriebssekunden zum R�cksetzen*)
		sCurrentProductName : STRING[nBRB_FILE_NAME_CHAR_MAX]; (*Filename Produktparameter*)
		nImgProcessingErrCnt : UDINT; (*St�rungsz�hler Bildverarbeitungssystem*)
		rSimulatedEncoderSpeed : REAL; (*Geschwindigkeit des simulierten Gebers in 0,01 m/s*)
	END_STRUCT;
	EncPar_TYP : 	STRUCT 
		eEncoderType : DINT; (*Betriebsart/Typ des Gebers; 0 = normaler Drehgeber, 1 = Virtueller Geber, 2 = Triggersensor zur Geschwindigkeits- und Positionsbestimmung*)
		bSimulationEnable : BOOL; (*Simulierter Geber aktiv. WICHTIG: Hat nur bei Geber-Index 0 (Simulierter Geber) Auswirkung.*)
		nIncPerRev : UDINT := 1024; (*GeberTyp Simuliert/Encoder: Inkremente pro Umdrehung*)
		rDistancePerRev : REAL := 500.0; (*GeberTyp Simuliert/Encoder: Strecke pro Umdrehung in mm*)
		nSourceEncoderIdx : USINT; (*GeberTyp Virtuell: Bezug des virtuellen Gebers auf einen reelen/simulierten Geber*)
		rFactor : REAL := 850.0; (*GeberTyp Virtuell: Faktor zur Berechnung der Geschwindigkeit/Positionen des virtuellen Gebers in Abh�ngigkeit zu der Geberquelle (Bezug)*)
		nTriggerSensorIdx : SINT := -1; (*GeberTyp Trigger: Index des Triggersensors der dieser Gebernummer zugeordnet ist*)
		rTriggerSignalDistance : REAL := 10; (*GeberTyp Trigger: Distanz zwischen zwei Trigger-Signalen in [mm] (Wertebereich 0.1 bis 1000 mm)*)
		rTriggerSignalTimeOut : REAL := 1; (*GeberTyp Trigger: maxmiale Zeit zwischen zwei Trigger-Signalen in [s] bevor die Geschwindigkeit auf Null m/s gesetzt wird (Wertebereich 0.1 bis 10 s)*)
		rVelocityFactor : REAL := 1.0;
	END_STRUCT;
	MachineModule_TYP : 	STRUCT  (*Maschinenmodule*)
		bOverviewEn : BOOL; (*�bersichts-Modul vorhanden*)
		bCleaningEn : BOOL; (*Reinigungsfunktionalit�t vorhanden*)
		bImgProcessingEn : BOOL; (*Bildverarbeitungssystem vorhanden*)
		bMotorisedAdjustmentEn : BOOL; (*Motorische Verstellung vorhanden*)
		bPressureCtrlEn : BOOL; (*Geschwindigkeitsabh�ngige Druckregelung vorhanden*)
		bStitchmodeEn : BOOL; (*Stitchmode vorhanden*)
		bTempCtrlEn : BOOL; (*Heizsystem vorhanden*)
		bAnalogFuellStandEn : BOOL;
	END_STRUCT;
	BondingIdx_TYP : 	STRUCT 
		BondingParIdx : ARRAY[0..nIDX_PRINT_MARK_MAX]OF BondingPar_Typ;
	END_STRUCT;
	BondingPar_Typ : 	STRUCT 
		rPause : ARRAY[0..nIDX_SWITCHES_MAX]OF REAL; (*Pause in mm*)
		rStitchLen : ARRAY[0..nIDX_SWITCHES_MAX]OF REAL; (*Strichl�nge in mm*)
	END_STRUCT;
	PosToBond_TYP : 	STRUCT 
		nPosToBond : DINT; (*Schieberegister f�r Positionen f�r den Klebeauftrag*)
		nDiffPos : DINT; (*Strecke zwischen zwei Produkten*)
		bEditProdukt : BOOL; (*Produkt bearbeiten*)
		bProductDetected : BOOL;
	END_STRUCT;
	PmPos_TYP : 	STRUCT 
		nStartPosFromTs : DINT; (*Errechnete Startposition aus Zeitstempel (Druckmarkenbeginn)*)
		nPmIdx : SINT; (*Druckmarkenindex*)
		bEditProdukt : BOOL; (*Produkt bearbeiten*)
		bProductDetected : BOOL; (*Produkt wurde erkannt (Produkterkennung)*)
		bProductPositionSetByTrigger : BOOL; (*Bei PrintMark_Trigger: Die Position des Produktanfangs wurde vom TriggerSensor f�r diesen Eintrag gesetzt*)
		nPositionProductBegins : DINT; (*Bei PrintMark_Trigger: Errechnete Position (aus Zeitstempel) des Produktanfangs welche vom Trigger-Sensor (Produktvorderkanten-Sensor) erkannt wurde*)
	END_STRUCT;
	Tank_TYP : 	STRUCT 
		bTankGlueEn : BOOL; (*Tank Klebstoff vorhanden*)
		nTankGlueStopDelay : SINT; (*Abschalt-Verz�gerung bei Tank leer [min] (0=keine Verz�gerung, sofortiges Abschalten)*)
		bTankCleanEn : BOOL; (*Tank Reinigungsmittel vorhanden*)
		nTankCleanStopDelay : SINT; (*Abschalt-Verz�gerung bei Tank leer [min] (0=keine Verz�gerung, sofortiges Abschalten)*)
		bTankSoftEn : BOOL; (*Tank Soft vorhanden*)
		nTankSoftStopDelay : SINT; (*Abschalt-Verz�gerung bei Tank leer [min] (0=keine Verz�gerung, sofortiges Abschalten)*)
		bAutoValveCtrl : BOOL; (*Automatische Ventilsteuerung (kann nur aktiviert werden, wenn Klebstofftank vorhanden ist)*)
	END_STRUCT;
	PrintMarkPar_TYP : 	STRUCT 
		nPrintMarkLen : ARRAY[0..nIDX_PRINT_MARK_MAX]OF DINT; (*Druckmarkenl�nge in 1/100 mm*)
		nTolerance : DINT; (*Toleranz der Druckmarkenl�nge in 1/100 mm*)
	END_STRUCT;
	SwitchesIdx_TYP : 	STRUCT 
		SwitchesIdx : ARRAY[0..nIDX_PRINT_MARK_MAX]OF SwitchesPar_TYP;
	END_STRUCT;
	SwitchesPar_TYP : 	STRUCT 
		rFirstOnPos : ARRAY[0..nIDX_SWITCHES_MAX]OF REAL; (*Untere Positionsgrenze an welcher der Ausgang eingeschaltet ist  in 1/100 mm*)
		rLastOnPos : ARRAY[0..nIDX_SWITCHES_MAX]OF REAL; (*Untere Positionsgrenze an welcher der Ausgang eingeschaltet ist  in 1/100 mm*)
	END_STRUCT;
	MotorisedAdjustment_TYP : 	STRUCT 
		rVelocityHorizontal : REAL; (*Geschwindigkteit Horizontal in mm/s*)
		rVelocityVertical : REAL; (*Geschwindigkteit Vertical in mm/s*)
		RefPos : ARRAY[0..nDEV_INDEX_MAX]OF RefPos_TYP; (*Referenzierpositionen*)
		MotorisedAdjExists : ARRAY[0..nDEV_INDEX_MAX]OF MotorisedAdjExists_TYP; (*Achsverstellungen vorhanden*)
		AxesLimits : ARRAY[0..nDEV_INDEX_MAX]OF AxesLimits_TYP;
	END_STRUCT;
	AxesLimits_TYP : 	STRUCT 
		Alignment : ARRAY[0..eHORIZONTAL]OF MParAxesLimitsAlignment_TYP;
	END_STRUCT;
	MParAxesLimitsAlignment_TYP : 	STRUCT 
		rNegativeSwLimit : REAL;
		rPositiveSwLimit : REAL;
	END_STRUCT;
	AxesPosition_TYP : 	STRUCT 
		Alignment : ARRAY[0..eHORIZONTAL]OF ProdParMotAdjustAlignment_TYP; (*Dieses Struktur-Array unterscheidet die Ausrichtung der der motorischen Verstellung ( [0] = vertikal, [1] = horizontal )*)
	END_STRUCT;
	ProdParMotAdjustAlignment_TYP : 	STRUCT 
		rParkPosition : REAL; (*Parkposition in [mm] des motorisch verstellbaren Klebekopfes*)
		rWorkPosition : REAL; (*Arbeitsposition in [mm] des motorisch verstellbaren Klebekopfes*)
	END_STRUCT;
	MotorisedAdjExists_TYP : 	STRUCT 
		bVertical : BOOL; (*Vertikale Achsverstellung vorhanden*)
		bHorizontal : BOOL; (*Horizontale Achsverstellung vorhanden*)
	END_STRUCT;
	RefPos_TYP : 	STRUCT 
		rVertical : REAL; (*Referenzierposition Vertikal in mm*)
		rHorizontal : REAL; (*Referenzierposition Horizontal in mm*)
	END_STRUCT;
	PressureControl_TYP : 	STRUCT 
		PreCtrlBasis : PressureCtrlBasis_TYP; (*Basis Druckparameter*)
	END_STRUCT;
	PressureCtrlBasis_TYP : 	STRUCT 
		rMaxPressure : REAL; (*Maximaldruck in bar*)
		rBasePressure : REAL; (*Basisdruck in bar*)
		rHysteresis : REAL; (*Hysterese in bar*)
		rMaxVelocitiy : REAL; (*Max. Geschwindigket f�r Druckregelung in m/s*)
		nEncoderIdx : SINT; (*Geber Index f�r die Druckregelung*)
		rBypassOpenTime : REAL; (*Zeit in [s] wie lange der Bypass f�r die Druckreduzierung ge�ffnet werden soll.*)
	END_STRUCT;
	StitchmodeDistance_TYP : 	STRUCT 
		rOpenDistanceSmall : REAL; (*Strecke die das Ventil ge�ffnet bleiben soll (Klein) in mm (Min: 0,2 mm)*)
		rOpenDistanceMedium : REAL; (*Strecke die das Ventil ge�ffnet bleiben soll (Mittel) in mm (Min: 0,2 mm)*)
		rOpenDistanceLarge : REAL; (*Strecke die das Ventil ge�ffnet bleiben soll (Gro�) in mm (Min: 0,2 mm)*)
		rOpenTimeSmall : REAL; (*Zeit die das Ventil ge�ffnet bleiben soll (Klein) in ms*)
		rOpenTimeMedium : REAL; (*Zeit die das Ventil ge�ffnet bleiben soll (Mittel) in ms*)
		rOpenTimeLarge : REAL; (*Zeit die das Ventil ge�ffnet bleiben soll (Gro�) in ms*)
	END_STRUCT;
	NegativeSensorOffset_TYP : 	STRUCT 
		nNegativeSensorOffset : DINT;
		nProductsBetweenBonDev_Sensor : DINT;
		nProductRestBetweenBonDev_Sensor : DINT;
		nProductRest : DINT;
		nNegativeOffsetToBonDev : DINT;
		nNegativePrintMarkPositionOffset : DINT;
	END_STRUCT;
END_TYPE
