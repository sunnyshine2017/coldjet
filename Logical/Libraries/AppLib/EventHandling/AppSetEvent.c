/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: AppLib
 * File: AppSetEvent.c
 * Author: niedermeierr
 * Created: October 06, 2014
 ********************************************************************
 * Implementation of library AppLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "AppLib.h"
#include <string.h>

#ifdef __cplusplus
	};
#endif

/* Setzt ein Event */
plcbit AppSetEvent(signed long nEventId, signed long nErrorNumber, plcstring* pDetail, signed long nDetail, struct AppEventManagement_TYP* pEventManagement)
{
	BOOL bResult = 0;
	if(nEventId <= nAPP_EVENT_INDEX_MAX)
	{
		pEventManagement->Events[nEventId].bSet = 1;
		pEventManagement->Events[nEventId].tClockSet = clock_ms();
		pEventManagement->Events[nEventId].bReset = 0;
		pEventManagement->Events[nEventId].bWaitForAcknowledge = 0;
		pEventManagement->Events[nEventId].bAcknowledged = 0;
		pEventManagement->Events[nEventId].nErrorNumber = nErrorNumber;
		memset(pEventManagement->Events[nEventId].sDetail, 0, sizeof(pEventManagement->Events[nEventId].sDetail));
		strncpy(pEventManagement->Events[nEventId].sDetail, pDetail, sizeof(pEventManagement->Events[nEventId].sDetail));
		pEventManagement->Events[nEventId].nDetail = nDetail;
		bResult = 1;
	}
	return bResult;
}
