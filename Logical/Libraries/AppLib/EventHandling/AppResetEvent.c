/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: AppLib
 * File: AppResetEvent.c
 * Author: niedermeierr
 * Created: October 06, 2014
 ********************************************************************
 * Implementation of library AppLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "AppLib.h"

#ifdef __cplusplus
	};
#endif

/* Setzt ein Event zur�ck */
plcbit AppResetEvent(signed long nEventId, struct AppEventManagement_TYP* pEventManagement)
{
	BOOL bResult = 0;
	if(nEventId <= nAPP_EVENT_INDEX_MAX)
	{
		if(pEventManagement->Events[nEventId].bSet == 1 || pEventManagement->AlarmImage[nEventId] == 1)
		{
			pEventManagement->Events[nEventId].bReset = 1;
			bResult = 1;
		}
	}
	return bResult;
}
