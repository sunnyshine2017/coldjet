/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: AppLib
 * File: Round.c
 * Author: kusculart
 * Created: November 01, 2014
 ********************************************************************
 * Implementation of library AppLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "AppLib.h"
	#include "AsBrMath.h"

#ifdef __cplusplus
	};
#endif

/* Round Function */
float Round(float rInput)
{
	REAL rZ1;
	REAL rZ2;
	rZ1 = brmfloor(rInput);
	rZ2 = brmfloor(rInput + 0.5);
	if (rZ2 > rZ1)
	{
		return rZ2;
	}
	else
	{
		return rZ1;
	}
}
