/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: AppLib
 * File: AppIsEventAcknowledged.c
 * Author: niedermeierr
 * Created: October 07, 2014
 ********************************************************************
 * Implementation of library AppLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "AppLib.h"

#ifdef __cplusplus
	};
#endif

/* Gibt zur�ck, ob ein Ereignis quittiert wurde */
plcbit AppIsEventAcknowledged(signed long nEventId, struct AppEventManagement_TYP* pEventManagement)
{
	BOOL bResult = 0;
	if(pEventManagement->Events[nEventId].bAcknowledged == 1)
	{
		pEventManagement->Events[nEventId].bAcknowledged = 0;
		bResult = 1;
	}
	return bResult;
}
