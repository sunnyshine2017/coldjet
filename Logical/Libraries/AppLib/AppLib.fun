(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: AppLib
 * File: AppLib.fun
 * Author: niedermeierr
 * Created: October 06, 2014
 ********************************************************************
 * Functions and function blocks of library AppLib
 ********************************************************************)

FUNCTION AppSetEvent : BOOL (*Setzt ein Event*) (*$GROUP=User*)
	VAR_INPUT
		nEventId : DINT;
		nErrorNumber : DINT;
		pDetail : STRING[0];
		nDetail : DINT;
		pEventManagement : AppEventManagement_TYP;
	END_VAR
END_FUNCTION

FUNCTION AppResetEvent : BOOL (*Setzt ein Event zur�ck*) (*$GROUP=User*)
	VAR_INPUT
		nEventId : DINT;
		pEventManagement : AppEventManagement_TYP;
	END_VAR
END_FUNCTION

FUNCTION AppIsEventAcknowledged : BOOL (*Gibt zur�ck, ob ein Ereignis quittiert wurde*) (*$GROUP=User*)
	VAR_INPUT
		nEventId : DINT;
		pEventManagement : AppEventManagement_TYP;
	END_VAR
END_FUNCTION

FUNCTION Round : REAL (*Round Function*) (*$GROUP=User*)
	VAR_INPUT
		rInput : REAL;
	END_VAR
END_FUNCTION
