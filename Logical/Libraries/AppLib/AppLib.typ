(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: AppLib
 * File: AppLib.typ
 * Author: niedermeierr
 * Created: October 06, 2014
 ********************************************************************
 * Data types of library AppLib
 ********************************************************************)

TYPE
	AppEvent_TYP : 	STRUCT  (*Eintrag in der Ereignis-Verwaltung*)
		bSet : BOOL; (*Intern: 1=Ereignis muss gesetzt werden*)
		tClockSet : TIME; (*Intern: Zeitstempel f�r R�cksetz-Verz�gerung*)
		bReset : BOOL; (*Intern: 1=Ereignis muss r�ckgesetzt werden*)
		bWaitForAcknowledge : BOOL; (*Intern: 1=Ereignis wartet auf Quittierung an Visu*)
		bAcknowledged : BOOL; (*F�r R�ckmeldung an Task: 1=Ereignis wurde an Visu quittiert*)
		nErrorNumber : DINT; (*Fehler-Nummer bei System-Funktionen*)
		sDetail : STRING[nAPP_EVENT_DETAIL_CHAR_MAX]; (*Detail-Text zur n�heren Beschreibung des Ereignisses*)
		nDetail : DINT; (*Detail-Nummer zur n�heren Beschreibung des Ereignisses*)
	END_STRUCT;
	AppEventManagement_TYP : 	STRUCT  (*Ereignis-Verwaltung*)
		Events : ARRAY[0..nAPP_EVENT_INDEX_MAX]OF AppEvent_TYP; (*Zur internen Verwaltung*)
		AlarmImage : ARRAY[0..nAPP_EVENT_INDEX_MAX]OF BOOL; (*Zur Anbindung an das Steuerelement*)
		AcknowledgeImage : ARRAY[0..nAPP_EVENT_INDEX_MAX]OF BOOL; (*Zur Anbindung an das Steuerelement*)
	END_STRUCT;
END_TYPE
