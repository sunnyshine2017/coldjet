/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbNormalizeAngleRad.c
 * Author: niedermeierr
 * Created: September 05, 2014
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Normalisiert einen Bogenmass-Winkel in den Bereich 0..2Pi */
float BrbNormalizeAngleRad(float rAngleRad)
{
	REAL rResult = 0.0;
	DINT nMod = (DINT)(rAngleRad / 6.2831853);
	rResult = rAngleRad - (nMod * 6.2831853);
	return rResult;
}
