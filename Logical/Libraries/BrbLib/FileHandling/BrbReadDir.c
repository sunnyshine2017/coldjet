/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbReadDir.c
 * Author: niedermeierr
 * Created: June 17, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <string.h>
#include <AnsiCFunc.h>

#ifdef __cplusplus
	};
#endif

// Eigene Enums und Typen
enum Steps_ENUM
{
	eSTEP_INIT,
	eSTEP_GET_FILTER,
	eSTEP_READ_DIR,
	eSTEP_SORT,
};

// Prototypen
void SortList(struct BrbReadDir* inst);
SINT SortTwoEntries(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2, BrbFileSorting_ENUM eSorting);
SINT FileComparerNone(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2);
SINT FileComparerAlphUp(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2);
SINT FileComparerAlphDown(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2);
SINT FileComparerOldest(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2);
SINT FileComparerYoungest(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2);
SINT FileComparerBiggest(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2);
SINT FileComparerSmallest(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2);

/* Gibt eine Liste der Dateien und/oder der Verzeichnisse eines Laufwerks zur�ck */
void BrbReadDir(struct BrbReadDir* inst)
{
	switch(inst->eStep)
	{
		case eSTEP_INIT:
			// Fub initialisieren
			inst->nStatus = 65535;
			inst->nDirCount = 0;
			inst->nFileCount = 0;
			inst->nTotalCount = 0;
			memset(&inst->FilterListMan, 0, sizeof(inst->FilterListMan));
			memset(&inst->fbDirRead, 0, sizeof(inst->fbDirRead));
			memset(&inst->FileInfo, 0, sizeof(inst->FileInfo));
			memset(&inst->FileListMan, 0, sizeof(inst->FileListMan));
			// Der Einfachkeit halber wird das File-Array in einer MemList verwaltet
			inst->FileListMan.pList = inst->pList;
			inst->FileListMan.nIndexMax = inst->nListIndexMax;
			inst->FileListMan.nEntryLength = sizeof(inst->ReadListEntry);
			BrbMemListClear(&inst->FileListMan);
			inst->eStep = eSTEP_GET_FILTER;
			break;

		case eSTEP_GET_FILTER:
			// Filtertext in Liste umsetzen
			// Der Einfachkeit halber wird die Filter-Liste in einer MemList verwaltet
			inst->FilterListMan.pList = (UDINT)&inst->FilterList;
			inst->FilterListMan.nIndexMax = nBRB_FILTER_SINGLE_INDEX_MAX;
			inst->FilterListMan.nEntryLength = nBRB_FILTER_SINGLE_CHAR_MAX+1;
			BrbMemListClear(&inst->FilterListMan);
			if(strlen(inst->pFileFilter))
			{
				inst->FilterListMan.nEntryCount = BrbStringSplit(inst->pFileFilter, ";", inst->FilterListMan.pList, inst->FilterListMan.nIndexMax, inst->FilterListMan.nEntryLength);
			}
			inst->eStep = eSTEP_READ_DIR;
			break;

		case eSTEP_READ_DIR:
			// Laufwerk auslesen
			inst->nStatus = 65535;
			inst->fbDirRead.enable = 1;
			inst->fbDirRead.pDevice = (UDINT)inst->pDevice;
			inst->fbDirRead.pPath = (UDINT)inst->pPath;
			inst->fbDirRead.option = fiBOTH;
			inst->fbDirRead.pData = (UDINT)&inst->FileInfo;
			inst->fbDirRead.data_len = sizeof(inst->FileInfo);
			DirRead(&inst->fbDirRead);
			if(inst->fbDirRead.status == 0)
			{
				// Filterpr�fung
				BOOL bInFilter = 0;
				if((strcmp((STRING*)&inst->FileInfo.Filename, "..") == 0 && inst->bWithParentDir == 0) || strcmp((STRING*)&inst->FileInfo.Filename, ".") == 0)
				{
					// ".." bezieht sich auf das �bergeordnete Verzeichnis und wird nur optional zur�ckgegeben
					// "." bezieht sich auf das eigene Verzeichnis und wird grunds�tzlich nicht zur�ckgegeben
				}
				else if(inst->eFilter != eBRB_DIR_INFO_ONLY_FILES && inst->FileInfo.Filelength == 0)
				{
					// Verzeichnis
					bInFilter = 1;
					inst->nDirCount += 1;
				}
				else if(inst->eFilter != eBRB_DIR_INFO_ONLY_DIRS && inst->FileInfo.Filelength > 0)
				{
					if(inst->FilterListMan.nEntryCount > 0)
					{
						UINT nFilterListIndex = 0;
						for(nFilterListIndex=0; nFilterListIndex<inst->FilterListMan.nEntryCount; nFilterListIndex++)
						{
							if(BrbStringEndsWith((STRING*)&inst->FileInfo.Filename, inst->FilterList[nFilterListIndex]) == 1)
							{
								// Datei
								bInFilter = 1;
								inst->nFileCount += 1;
								break;
							}
						}
					}
					else
					{
						// Datei
						bInFilter = 1;
						inst->nFileCount += 1;
					}
				}
				if(bInFilter == 1)
				{
					// Im Filter
					memset(&inst->ReadListEntry, 0, sizeof(inst->ReadListEntry));
					memcpy(&inst->ReadListEntry, &inst->FileInfo, sizeof(inst->ReadListEntry));
					if(inst->FileListMan.nEntryCount <= inst->FileListMan.nIndexMax)
					{
						// Liste ist nicht voll -> in Liste eintragen
						BrbMemListIn(&inst->FileListMan, inst->FileListMan.nEntryCount, (UDINT)&inst->ReadListEntry);
					}
					else
					{
						// Liste ist voll
						// Sortieren
						SortList(inst);
						// Den letzten Eintrag mit dem aktuellen vergleichen
						fiDIR_READ_DATA* pFileInfoLast = (fiDIR_READ_DATA*)(inst->FileListMan.pList + ((inst->FileListMan.nEntryCount-1) * inst->FileListMan.nEntryLength));
						SINT nSort = SortTwoEntries(pFileInfoLast, &inst->FileInfo, inst->eSorting);
						if(nSort > 0)
						{
							// Neue Datei ist n�her am Filter als der letzte Eintrag -> ersetzen
							inst->FileListMan.nEntryCount -= 1;
							BrbMemListIn(&inst->FileListMan, inst->FileListMan.nEntryCount, (UDINT)&inst->ReadListEntry);
						}
					}
				}
				memset(&inst->FileInfo, 0, sizeof(inst->FileInfo));
				inst->fbDirRead.entry += 1;
			}
			else if(inst->fbDirRead.status == fiERR_NO_MORE_ENTRIES)
			{
				// Alle Dateien gelesen
				inst->nTotalCount = inst->nDirCount + inst->nFileCount;
				inst->eStep = eSTEP_SORT;
			}
			else if(inst->fbDirRead.status != 65535)
			{
				inst->nStatus = inst->fbDirRead.status;
				inst->eStep = eSTEP_INIT;
			}
			break;

		case eSTEP_SORT:
			inst->nStatus = 65535;
			SortList(inst);
			inst->nStatus = 0;
			inst->eStep = eSTEP_INIT;
			break;

	}
}

void SortList(struct BrbReadDir* inst)
{
	switch(inst->eSorting)
	{
		case eBRB_FILE_SORTING_NONE:
			qsort((fiDIR_READ_DATA*)inst->pList, inst->nListIndexMax+1, sizeof(fiDIR_READ_DATA), (void*)&FileComparerNone);
			break;

		case eBRB_FILE_SORTING_ALPH_UP:
			qsort((fiDIR_READ_DATA*)inst->pList, inst->nListIndexMax+1, sizeof(fiDIR_READ_DATA), (void*)&FileComparerAlphUp);
			break;

		case eBRB_FILE_SORTING_ALPH_DOWN:
			qsort((fiDIR_READ_DATA*)inst->pList, inst->nListIndexMax+1, sizeof(fiDIR_READ_DATA), (void*)&FileComparerAlphDown);
			break;

		case eBRB_FILE_SORTING_OLDEST:
			qsort((fiDIR_READ_DATA*)inst->pList, inst->nListIndexMax+1, sizeof(fiDIR_READ_DATA), (void*)&FileComparerOldest);
			break;
	
		case eBRB_FILE_SORTING_YOUNGEST:
			qsort((fiDIR_READ_DATA*)inst->pList, inst->nListIndexMax+1, sizeof(fiDIR_READ_DATA), (void*)&FileComparerYoungest);
			break;

		case eBRB_FILE_SORTING_BIGGEST:
			qsort((fiDIR_READ_DATA*)inst->pList, inst->nListIndexMax+1, sizeof(fiDIR_READ_DATA), (void*)&FileComparerBiggest);
			break;

		case eBRB_FILE_SORTING_SMALLEST:
			qsort((fiDIR_READ_DATA*)inst->pList, inst->nListIndexMax+1, sizeof(fiDIR_READ_DATA), (void*)&FileComparerSmallest);
			break;
	}
}

SINT SortTwoEntries(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2, BrbFileSorting_ENUM eSorting)
{
	SINT nResult = 0;
	switch(eSorting)
	{
		case eBRB_FILE_SORTING_NONE:
			nResult = FileComparerNone(pEntry1, pEntry2);
			break;

		case eBRB_FILE_SORTING_ALPH_UP:
			nResult = FileComparerAlphUp(pEntry1, pEntry2);
			break;

		case eBRB_FILE_SORTING_ALPH_DOWN:
			nResult = FileComparerAlphDown(pEntry1, pEntry2);
			break;

		case eBRB_FILE_SORTING_OLDEST:
			nResult = FileComparerOldest(pEntry1, pEntry2);
			break;
	
		case eBRB_FILE_SORTING_YOUNGEST:
			nResult = FileComparerYoungest(pEntry1, pEntry2);
			break;

		case eBRB_FILE_SORTING_BIGGEST:
			nResult = FileComparerBiggest(pEntry1, pEntry2);
			break;

		case eBRB_FILE_SORTING_SMALLEST:
			nResult = FileComparerSmallest(pEntry1, pEntry2);
			break;
	}
	return nResult;
}

SINT FileComparerNone(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2)
{
	// Die Verzeichnisse m�ssen oben sein
	SINT nResult = 0;
	UDINT nNameLen1 = strlen((STRING*)&pEntry1->Filename);
	UDINT nNameLen2 = strlen((STRING*)&pEntry2->Filename);
	if(nNameLen1 == 0 && nNameLen2 == 0)
	{
		nResult = 0;		
	}
	else if(nNameLen1 == 0)
	{
		nResult = 1;		
	}
	else if(nNameLen2 == 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength == 0 && pEntry2->Filelength > 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength > 0 && pEntry2->Filelength == 0)
	{
		nResult = 1;		
	}
	else if(strcmp((STRING*)&pEntry1->Filename, "..") == 0)
	{
		nResult = -1;		
	}
	else if(strcmp((STRING*)&pEntry2->Filename, "..") == 0)
	{
		nResult = 1;		
	}
	else
	{
		nResult = 0;		
	}
	return nResult;
}

SINT FileComparerAlphUp(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2)
{
	SINT nResult = 0;
	UDINT nNameLen1 = strlen((STRING*)&pEntry1->Filename);
	UDINT nNameLen2 = strlen((STRING*)&pEntry2->Filename);
	if(nNameLen1 == 0 && nNameLen2 == 0)
	{
		nResult = 0;		
	}
	else if(nNameLen1 == 0)
	{
		nResult = 1;		
	}
	else if(nNameLen2 == 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength == 0 && pEntry2->Filelength > 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength > 0 && pEntry2->Filelength == 0)
	{
		nResult = 1;		
	}
	else if(strcmp((STRING*)&pEntry1->Filename, "..") == 0)
	{
		nResult = -1;		
	}
	else if(strcmp((STRING*)&pEntry2->Filename, "..") == 0)
	{
		nResult = 1;		
	}
	else
	{
		nResult = strcmp((STRING*)&pEntry1->Filename, (STRING*)&pEntry2->Filename);
	}
	return nResult;
}

SINT FileComparerAlphDown(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2)
{
	SINT nResult = 0;
	UDINT nNameLen1 = strlen((STRING*)&pEntry1->Filename);
	UDINT nNameLen2 = strlen((STRING*)&pEntry2->Filename);
	if(nNameLen1 == 0 && nNameLen2 == 0)
	{
		nResult = 0;		
	}
	else if(nNameLen1 == 0)
	{
		nResult = 1;		
	}
	else if(nNameLen2 == 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength == 0 && pEntry2->Filelength > 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength > 0 && pEntry2->Filelength == 0)
	{
		nResult = 1;		
	}
	else if(strcmp((STRING*)&pEntry1->Filename, "..") == 0)
	{
		nResult = -1;		
	}
	else if(strcmp((STRING*)&pEntry2->Filename, "..") == 0)
	{
		nResult = 1;		
	}
	else
	{
		nResult = strcmp((STRING*)&pEntry2->Filename, (STRING*)&pEntry1->Filename);
	}
	return nResult;
}

SINT FileComparerOldest(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2)
{
	SINT nResult = 0;
	UDINT nNameLen1 = strlen((STRING*)&pEntry1->Filename);
	UDINT nNameLen2 = strlen((STRING*)&pEntry2->Filename);
	if(nNameLen1 == 0 && nNameLen2 == 0)
	{
		nResult = 0;		
	}
	else if(nNameLen1 == 0)
	{
		nResult = 1;		
	}
	else if(nNameLen2 == 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength == 0 && pEntry2->Filelength > 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength > 0 && pEntry2->Filelength == 0)
	{
		nResult = 1;		
	}
	else if(strcmp((STRING*)&pEntry1->Filename, "..") == 0)
	{
		nResult = -1;		
	}
	else if(strcmp((STRING*)&pEntry2->Filename, "..") == 0)
	{
		nResult = 1;		
	}
	else
	{
		if(pEntry1->Date < pEntry2->Date)
		{
			nResult = -1;
		}
		else if(pEntry1->Date > pEntry2->Date)
		{
			nResult = 1;
		}
		else
		{
			nResult = FileComparerAlphUp(pEntry1, pEntry2);
		}
	}
	return nResult;
}

SINT FileComparerYoungest(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2)
{
	SINT nResult = 0;
	UDINT nNameLen1 = strlen((STRING*)&pEntry1->Filename);
	UDINT nNameLen2 = strlen((STRING*)&pEntry2->Filename);
	if(nNameLen1 == 0 && nNameLen2 == 0)
	{
		nResult = 0;		
	}
	else if(nNameLen1 == 0)
	{
		nResult = 1;		
	}
	else if(nNameLen2 == 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength == 0 && pEntry2->Filelength > 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength > 0 && pEntry2->Filelength == 0)
	{
		nResult = 1;		
	}
	else if(strcmp((STRING*)&pEntry1->Filename, "..") == 0)
	{
		nResult = -1;		
	}
	else if(strcmp((STRING*)&pEntry2->Filename, "..") == 0)
	{
		nResult = 1;		
	}
	else
	{
		if(pEntry1->Date > pEntry2->Date)
		{
			nResult = -1;
		}
		else if(pEntry1->Date < pEntry2->Date)
		{
			nResult = 1;
		}
		else
		{
			nResult = FileComparerAlphUp(pEntry1, pEntry2);
		}
	}
	return nResult;
}

SINT FileComparerBiggest(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2)
{
	SINT nResult = 0;
	UDINT nNameLen1 = strlen((STRING*)&pEntry1->Filename);
	UDINT nNameLen2 = strlen((STRING*)&pEntry2->Filename);
	if(nNameLen1 == 0 && nNameLen2 == 0)
	{
		nResult = 0;		
	}
	else if(nNameLen1 == 0)
	{
		nResult = 1;		
	}
	else if(nNameLen2 == 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength == 0 && pEntry2->Filelength > 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength > 0 && pEntry2->Filelength == 0)
	{
		nResult = 1;		
	}
	else if(strcmp((STRING*)&pEntry1->Filename, "..") == 0)
	{
		nResult = -1;		
	}
	else if(strcmp((STRING*)&pEntry2->Filename, "..") == 0)
	{
		nResult = 1;		
	}
	else
	{
		if(pEntry1->Filelength > pEntry2->Filelength)
		{
			nResult = -1;
		}
		else if(pEntry1->Filelength < pEntry2->Filelength)
		{
			nResult = 1;
		}
		else
		{
			nResult = FileComparerAlphUp(pEntry1, pEntry2);
		}
	}
	return nResult;
}

SINT FileComparerSmallest(fiDIR_READ_DATA* pEntry1, fiDIR_READ_DATA* pEntry2)
{
	SINT nResult = 0;
	UDINT nNameLen1 = strlen((STRING*)&pEntry1->Filename);
	UDINT nNameLen2 = strlen((STRING*)&pEntry2->Filename);
	if(nNameLen1 == 0 && nNameLen2 == 0)
	{
		nResult = 0;		
	}
	else if(nNameLen1 == 0)
	{
		nResult = 1;		
	}
	else if(nNameLen2 == 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength == 0 && pEntry2->Filelength > 0)
	{
		nResult = -1;		
	}
	else if(pEntry1->Filelength > 0 && pEntry2->Filelength == 0)
	{
		nResult = 1;		
	}
	else if(strcmp((STRING*)&pEntry1->Filename, "..") == 0)
	{
		nResult = -1;		
	}
	else if(strcmp((STRING*)&pEntry2->Filename, "..") == 0)
	{
		nResult = 1;		
	}
	else
	{
		if(pEntry1->Filelength < pEntry2->Filelength)
		{
			nResult = -1;
		}
		else if(pEntry1->Filelength > pEntry2->Filelength)
		{
			nResult = 1;
		}
		else
		{
			nResult = FileComparerAlphUp(pEntry1, pEntry2);
		}
	}
	return nResult;
}

