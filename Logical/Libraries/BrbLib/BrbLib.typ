(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbLib.typ
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Data types of library BrbLib
 ********************************************************************)
(*StepHandling*)

TYPE
	BrbStepHandlingCurrent_TYP : 	STRUCT 
		nStepNr : DINT; (*Aktuelle Schrittnummer*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX]; (*Aktueller Schritttext*)
		bTimeoutElapsed : BOOL; (*1=Timeout abgelaufen*)
		nTimeoutContinueStep : DINT; (*Schrittnummer nach Timeout*)
	END_STRUCT;
	BrbStepHandlingStep_TYP : 	STRUCT 
		nStepNr : DINT; (*Schrittnummer*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX]; (*Schritttext*)
		nCycleCount : UDINT; (*Zyklen*)
	END_STRUCT;
	BrbStepHandlingLog_TYP : 	STRUCT 
		bClear : BOOL; (*Kommando zum L�schen der Protokollierung*)
		bStop : BOOL; (*Kommando zum Stoppen der Protokollierung*)
		Steps : ARRAY[0..nBRB_STEPLOG_STEPS_MAX]OF BrbStepHandlingStep_TYP; (*Protokollierung*)
	END_STRUCT;
	BrbStepHandlingIntern_TYP : 	STRUCT 
		nStepNrOld : DINT; (*Alte Schrittnummer*)
		nCycleCount : UDINT; (*Aktueller Zyklusz�hler*)
		bLogOnNextCycle : BOOL; (*Flag bei Schrittwechsel*)
		fbTimeout : TON; (*Timeout-Fub*)
	END_STRUCT;
	BrbStepHandling_TYP : 	STRUCT 
		Current : BrbStepHandlingCurrent_TYP; (*Daten zum aktuellen Schritt*)
		Log : BrbStepHandlingLog_TYP; (*Protokollierung*)
		Intern : BrbStepHandlingIntern_TYP; (*Interne Daten*)
	END_STRUCT;
	BrbStopWatch_TYP : 	STRUCT 
		tStartTime : TIME; (*Start-Zeitstempel*)
		tStopTime : TIME; (*End-Zeitstempel*)
		nTimeDiff : UDINT; (*Berechnete Differenz*)
		sTimeDiff : STRING[24]; (*Differenz als Text*)
	END_STRUCT;
END_TYPE

(*TaskCommunication*)

TYPE
	BrbCallerStates_ENUM : 
		(
		eBRB_CALLER_STATE_NOT_READY := -1, (*-1=Nicht bereit*)
		eBRB_CALLER_STATE_OK := 0, (*0=Bereit*)
		eBRB_CALLER_STATE_BUSY := 1 (*1=Besetzt*)
		);
	BrbCaller_TYP : 	STRUCT 
		nCallerId : DINT; (*Task-ID*)
		bLock : BOOL; (*Semaphor-Flag*)
	END_STRUCT;
END_TYPE

(*VarHandling*)

TYPE
	BrbPvInfo_TYP : 	STRUCT 
		sName : STRING[nBRB_PVNAME_CHAR_MAX]; (*Variablen-Name*)
		nAdr : UDINT; (*Variablen-Adresse*)
		nDataType : UDINT; (*Variablen-Datentyp*)
		nLen : UDINT; (*Variablen-Gr��e*)
		nDimension : UINT; (*Max. Array-Index*)
		nItemIndex : UINT; (*Index des Array- oder Struktur-Items*)
	END_STRUCT;
END_TYPE

(*FileHandling*)

TYPE
	BrbUsbDeviceListEntry_TYP : 	STRUCT 
		sInterfaceName : STRING[nBRB_DEVICE_NAME_CHAR_MAX]; (*Schnittstellen-Name*)
		sDeviceName : STRING[nBRB_DEVICE_NAME_CHAR_MAX]; (*Schnittstellen-Name*)
		nNode : UDINT; (*Interne Node-Nummer*)
		nHandle : UDINT; (*Internes Handle*)
	END_STRUCT;
	BrbDirInfoFilter_ENUM : 
		(
		eBRB_DIR_INFO_ONLY_FILES, (*0=Nur Dateien*)
		eBRB_DIR_INFO_ONLY_DIRS, (*1=Nur Verzeichnisse*)
		eBRB_DIR_INFO_FILES_AND_DIRS (*2=Dateien und Verzeichnisse*)
		);
	BrbFileSorting_ENUM : 
		(
		eBRB_FILE_SORTING_NONE, (*0=Keine Sortierung*)
		eBRB_FILE_SORTING_ALPH_UP, (*1=Sortierung nach aufsteigendem Alphabet*)
		eBRB_FILE_SORTING_ALPH_DOWN, (*2=Sortierung nach aufsteigendem Alphabet*)
		eBRB_FILE_SORTING_OLDEST, (*3=Sortierung nach �ltesten Dateien*)
		eBRB_FILE_SORTING_YOUNGEST, (*4=Sortierung nach j�ngsten Dateien*)
		eBRB_FILE_SORTING_BIGGEST, (*5=Sortierung nach gr��ten Dateien*)
		eBRB_FILE_SORTING_SMALLEST (*6=Sortierung nach kleinsten Dateien*)
		);
	BrbReadDirListEntry_TYP : 	STRUCT 
		sName : STRING[nBRB_FILE_NAME_CHAR_MAX]; (*Verzeichnis- oder Datei-Name*)
		dtDate : DATE_AND_TIME; (*Zeitstempel*)
		nSize : UDINT; (*Gr��e (0=Verzeichnis)*)
	END_STRUCT;
END_TYPE

(*VisVc4*)

TYPE
	BrbVc4TouchStates_ENUM : 
		(
		eBRB_TOUCH_STATE_UNPUSHED := 0, (*0=Nicht gedr�ckt*)
		eBRB_TOUCH_STATE_UNPUSHED_EDGE := 1, (*1=Gerade losgelassen*)
		eBRB_TOUCH_STATE_PUSHED_EDGE := 2, (*2=Gerade gedr�ckt*)
		eBRB_TOUCH_STATE_PUSHED := 3, (*3=Gedr�ckt*)
		eBRB_TOUCH_STATE_DOUBLECLICK := 4 (*4=Doppelklick (nur f�r einen Zyklus)*)
		);
	BrbVc4GeneralTouch_TYP : 	STRUCT 
		nX : UDINT; (*Ausgang: X-Koordinate in [Pixel]*)
		nY : UDINT; (*Ausgang: Y-Koordinate in [Pixel]*)
		eState : BrbVc4TouchStates_ENUM; (*Ausgang: Momentaner Status des Touchs*)
		eStateSync : BrbVc4TouchStates_ENUM; (*Ausgang: Mit Redraw-Counter synchronisierter Status des Touchs*)
		fbDoubleClickDelay : TON; (*Interne Variable*)
		TouchAction : TouchAction; (*Interne Variable*)
		TouchActionOld : TouchAction; (*Interne Variable*)
	END_STRUCT;
	BrbVc4General_TYP : 	STRUCT 
		sVisName : STRING[nBRB_VIS_NAME_CHAR_MAX]; (*Eingang: Name des Vc-Objekts*)
		tDoubleclickDelay : TIME; (*Eingang: Zeit zwischen Loslassen und erneutem Dr�cken in [ms] zum Erkennen eines Doppelklicks*)
		nDoubleclickDrift : UDINT; (*Eingang: Max. Abstand zweier Klicks in [Pixel] zum Erkennen eines Doppelklicks*)
		nRedrawCycles : UINT; (*Eingang: Anzahl der Zyklen f�r das Zeichnen*)
		bDisableRedraw : BOOL; (*Eingang: 1=L�schen von eigenen Zeichnungen unterdr�cken*)
		nLanguageCurrent : UINT; (*Zur Anbindung an den Datenpunkt*)
		nLanguageChange : UINT; (*Zur Anbindung an den Datenpunkt*)
		nLifeSign : UDINT; (*Zur Anbindung an den Datenpunkt*)
		nVcHandle : UDINT; (*Ausgang: VcHandle der Visualisierung*)
		Touch : BrbVc4GeneralTouch_TYP; (*Aktuelle Touch-Daten*)
		nRedrawCounter : UINT; (*Ausgang: Z�hler f�rs Zeichnen (bei = 0 darf gezeichnet werden)*)
	END_STRUCT;
	BrbVc4ScreenSaver_TYP : 	STRUCT 
		bEnable : BOOL; (*Eingang: 1=Eingeschaltet*)
		hsScreen : BrbVc4Hotspot_TYP; (*Control: Optionaler Hotspot f�r R�ckschaltung*)
		nScreenSaverPage : UINT; (*Eingang: Bildschirmschoner-Seite*)
		fbScreenSaver : TON; (*Interne Variable*)
		TouchOld : BrbVc4GeneralTouch_TYP; (*Interne Variable*)
		nPageBeforeScreenSaver : DINT; (*Interne Variable*)
	END_STRUCT;
	BrbVc4PageHandling_TYP : 	STRUCT 
		nPageDefault : DINT; (*Eingang: Start-/Standard-Seite*)
		bChangePageDirect : BOOL; (*Eingang: Auf jeden Fall Seite wechseln*)
		nPageChange : DINT; (*Zur Anbindung an den Datenpunkt*)
		nPageCurrent : DINT; (*Zur Anbindung an den Datenpunkt*)
		nPageNext : DINT; (*Interne Variable*)
		PagesPreviousLifo : BrbMemListManagement_Typ; (*Interne Variable*)
		nPagesPrevious : ARRAY[0..nBRB_PAGE_LAST_INDEX_MAX]OF DINT; (*Interne Variable*)
		bPageInit : BOOL; (*Ausgang: Flanke bei Einsprung eine Seite*)
		bPageExit : BOOL; (*Ausgang: Flanke beim Verlassen einer Seite*)
		bPageChangeInProgress : BOOL; (*Ausgang: Seitenumschaltung im Gange*)
	END_STRUCT;
	BrbVc4Colors_ENUM : 
		(
		eBRB_COLOR_ENABLED := 59, (*59+256*0*)
		eBRB_COLOR_DISABLED := 63547, (*59+256*248*)
		eBRB_COLOR_TRANSPARENT := 255 (*Transparent*)
		);
	BrbVc4Animation_TYP : 	STRUCT 
		bEnable : BOOL; (*Eingang: 1=Enabled*)
		nIndexMin : UINT; (*Eingang: Kleinster Bitmap-Index der Animation*)
		nIndexMax : UINT; (*Eingang: Gr��ter Bitmap-Index der Animation*)
		nIndexStanding : UINT; (*Eingang: Bitmap-Index bei Stillstand*)
		nIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		fbTimer : TON; (*Interne Variable (Zeit PT muss eingestellt werden)*)
	END_STRUCT;
	BrbVc4Bargraph_TYP : 	STRUCT 
		rValue : REAL; (*Zur Anbindung an den Datenpunkt*)
		nValue : DINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Bitmap_TYP : 	STRUCT 
		nIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nFillColor1 : UINT; (*Zur Anbindung an den Datenpunkt*)
		nFillColor2 : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Button_TYP : 	STRUCT 
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bClicked : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Checkbox_TYP : 	STRUCT 
		bClicked : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		bChecked : BOOL; (*Ausgang: 1=Angehakt*)
	END_STRUCT;
	BrbVc4DateTime_TYP : 	STRUCT 
		dtValue : DATE_AND_TIME; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4DropdownOptions_ENUM : 
		(
		eBRB_DD_OPTION_NORMAL := 0, (*0=Normal*)
		eBRB_DD_OPTION_DISABLED := 1, (*1=Ausgegraut*)
		eBRB_DD_OPTION_INVISIBLE := 2 (*2=Unsichtbar*)
		);
	BrbVc4Drawbox_TYP : 	STRUCT 
		nLeft : UDINT; (*Eingang: Linke Koordinate*)
		nTop : UDINT; (*Eingang: Obere Koordinate*)
		nWidth : UDINT; (*Eingang: Breite*)
		nHeight : UDINT; (*Eingang: H�he*)
		sFullName : STRING[nBRB_FILE_NAME_CHAR_MAX]; (*Eingang: Pfad zur Anbindung*)
		nBackgroundColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Dropdown_TYP : 	STRUCT 
		nIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nIndexMin : UINT; (*Zur Anbindung an den Datenpunkt*)
		nIndexMax : UINT; (*Zur Anbindung an den Datenpunkt*)
		nOptions : ARRAY[0..nBRB_DROPDOWN_OPTION_MAX]OF USINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4EditCtrl_TYP : 	STRUCT 
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		nBusy : UINT; (*Zur Anbindung an den Datenpunkt*)
		sUrl : STRING[nBRB_URL_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		sCmdRequest : STRING[nBRB_EDIT_CTRL_CMD_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		sCmdResponse : STRING[nBRB_EDIT_CTRL_CMD_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		nCmdStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		nCompletion : UINT; (*Zur Anbindung an den Datenpunkt*)
		nCursorLine : UDINT; (*Zur Anbindung an den Datenpunkt*)
		nCursorColumn : UDINT; (*Zur Anbindung an den Datenpunkt*)
		nInsertMode : USINT; (*Zur Anbindung an den Datenpunkt*)
		nModified : USINT; (*Zur Anbindung an den Datenpunkt*)
		nSelectionMode : USINT; (*Zur Anbindung an den Datenpunkt*)
		nLineCount : UDINT; (*Interne Variable*)
		nColumsMax : UDINT; (*Interne Variable*)
		sLastCmdRequest : STRING[nBRB_EDIT_CTRL_CMD_CHAR_MAX]; (*Interne Variable*)
	END_STRUCT;
	BrbVc4Gauge_TYP : 	STRUCT 
		nValue : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMin : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMax : DINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Hotspot_TYP : 	STRUCT 
		bClicked : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Html_TYP : 	STRUCT 
		sCurrentUrl : STRING[nBRB_URL_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		sChangeUrl : STRING[nBRB_URL_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		sCurrentTitle : STRING[nBRB_URL_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		bBusy : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nHttpErrorCode : UDINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		bBusyOld : BOOL; (*Interne Variable*)
	END_STRUCT;
	BrbVc4HwPosSwitchPositions_ENUM : 
		(
		eBRB_HPS_POS_0_EDGE, (*0=Gerade auf linke Stellung geschaltet*)
		eBRB_HPS_POS_0, (*1=Auf linke Stellung gestellt*)
		eBRB_HPS_POS_1_EDGE, (*2=Gerade auf mittlere Stellung geschaltet*)
		eBRB_HPS_POS_1, (*3=Auf mittlere Stellung gestellt*)
		eBRB_HPS_POS_2_EDGE, (*4=Gerade auf rechte Stellung geschaltet*)
		eBRB_HPS_POS_2, (*5=Auf rechte Stellung gestellt*)
		eBRB_HPS_POS_UNDEF (*6=Stellung aufgrund der Eing�nge nicht zu ermitteln*)
		);
	BrbVc4HwPosSwitch2_TYP : 	STRUCT 
		bPos0 : BOOL; (*Hw-Eingang: Schalter auf linker Stellung*)
		bPos2 : BOOL; (*Hw-Eingang: Schalter auf rechter Stellung*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		nSwitchStatus0 : UINT; (*Zur Anbindung an den Datenpunkt*)
		nSwitchStatus1 : UINT; (*Zur Anbindung an den Datenpunkt*)
		nSwitchStatus2 : UINT; (*Zur Anbindung an den Datenpunkt*)
		ePosOld : BrbVc4HwPosSwitchPositions_ENUM; (*Interne Variable*)
		ePos : BrbVc4HwPosSwitchPositions_ENUM; (*Ausgang: Momentaner Status des Schalters*)
	END_STRUCT;
	BrbVc4HwSafteyButtonStates_ENUM : 
		(
		eBRB_HSB_STATE_UNPUSHED, (*0=Nicht gedr�ckt*)
		eBRB_HSB_STATE_UNPUSHED_EDGE, (*1=Gerade losgelassen*)
		eBRB_HSB_STATE_PUSHED_EDGE, (*2=Gerade gedr�ckt*)
		eBRB_HSB_STATE_PUSHED (*3=Gedr�ckt*)
		);
	BrbVc4HwSafetyButton_TYP : 	STRUCT 
		bNormallyOpened : BOOL; (*Hw-Eingang: Schliesser*)
		bNormallyClosed : BOOL; (*Hw-Eingang: �ffner*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		bPushedOld : BOOL; (*Interne Variable*)
		bPushed : BOOL; (*Ausgang: 0=Nicht gedr�ckt, 1=Gedr�ckt*)
		eState : BrbVc4HwSafteyButtonStates_ENUM; (*Ausgang: Momentaner Status des Tasters*)
	END_STRUCT;
	BrbVc4IncButtonStates_ENUM : 
		(
		eBRB_INC_BTN_STATE_UNPUSHED, (*0=Nicht gedr�ckt*)
		eBRB_INC_BTN_STATE_UNPUSHED_EDGE, (*1=Gerade losgelassen*)
		eBRB_INC_BTN_STATE_PUSHED_EDGE, (*2=Gerade gedr�ckt*)
		eBRB_INC_BTN_STATE_PUSHED_DELAY, (*3=Verz�gerunszeit l�uft*)
		eBRB_INC_BTN_STATE_PUSHED_REPEAT, (*4=Wiederholzeit l�uft*)
		eBRB_INC_BTN_STATE_PUSHED_INC (*5=Wiederholender Impuls*)
		);
	BrbVc4IncButton_TYP : 	STRUCT 
		bEnabled : BOOL; (*Eingang: 1=Enabled*)
		bSuppressDelay : BOOL; (*Eingang: 1=Keine Verz�gerungszeit*)
		bSuppressRepeat : BOOL; (*Eingang: 1=Keine Wiederholzeit*)
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bPushed : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		eIncState : BrbVc4IncButtonStates_ENUM; (*Ausgang: Momentaner Status des Buttons*)
		bPushedOld : BOOL; (*Interne Variable*)
		bEnabledOld : BOOL; (*Interne Variable*)
		fbDelay : TON; (*Interne Variable (Zeit PT muss eingestellt werden)*)
		fbRepeat : TON; (*Interne Variable (Zeit PT muss eingestellt werden)*)
	END_STRUCT;
	BrbVc4JogButtonStates_ENUM : 
		(
		eBRB_JOG_BTN_STATE_UNPUSHED, (*0=Nicht gedr�ckt*)
		eBRB_JOG_BTN_STATE_UNPUSHED_EDGE, (*1=Gerade losgelassen*)
		eBRB_JOG_BTN_STATE_PUSHED_EDGE, (*2=Gerade gedr�ckt*)
		eBRB_JOG_BTN_STATE_PUSHED (*3=Gedr�ckt*)
		);
	BrbVc4JogButton_TYP : 	STRUCT 
		bEnabled : BOOL; (*Eingang: 1=Enabled*)
		bSuppressTimeout : BOOL; (*Eingang: 1=Keine Verz�gerungszeit*)
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bPushed : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		eJogState : BrbVc4JogButtonStates_ENUM; (*Ausgang: Momentaner Status des Buttons*)
		bPushedOld : BOOL; (*Interne Variable*)
		bEnabledOld : BOOL; (*Interne Variable*)
		fbUnpush : TON; (*Interne Variable*)
	END_STRUCT;
	BrbVc4Layer_TYP : 	STRUCT 
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Listbox_TYP : 	STRUCT 
		nIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nIndexMin : UINT; (*Zur Anbindung an den Datenpunkt*)
		nIndexMax : UINT; (*Zur Anbindung an den Datenpunkt*)
		nControlColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nDisabledColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nSelectedColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nSliderColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nOptions : ARRAY[0..nBRB_LISTBOX_OPTION_MAX]OF USINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Numeric_TYP : 	STRUCT 
		nValue : DINT; (*Zur Anbindung an den Datenpunkt*)
		nMin : DINT; (*Zur Anbindung an den Datenpunkt*)
		nMax : DINT; (*Zur Anbindung an den Datenpunkt*)
		rValue : REAL; (*Zur Anbindung an den Datenpunkt*)
		rMin : REAL; (*Zur Anbindung an den Datenpunkt*)
		rMax : REAL; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Optionbox_TYP : 	STRUCT 
		bClicked : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		bChecked : BOOL; (*Ausgang: 1=Angehakt*)
	END_STRUCT;
	BrbVc4Password_TYP : 	STRUCT 
		sPasswords : ARRAY[0..nBRB_PASSWORD_INDEX_MAX]OF STRING[nBRB_PASSWORD_CHAR_MAX]; (*Passw�rter*)
		nLevel : UINT; (*Aktuelle Benutzer-Ebene*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4PieChart_TYP : 	STRUCT 
		nValue : ARRAY[0..nBRB_PIECHART_VALUE_INDEX_MAX]OF DINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Scale_TYP : 	STRUCT 
		nValue : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMin : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMax : DINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nControlColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nRangeColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4ScrollbarHor_TYP : 	STRUCT 
		bDisabled : BOOL; (*Eingang: 1=Disabled*)
		nTotalIndexMin : DINT; (*Eingang: Kleinster Index des Quelle*)
		nTotalIndexMax : DINT; (*Eingang: Gr��ter Index des Quelle*)
		nCountShow : UDINT; (*Eingang: Anzahl der angezeigen Zeilen*)
		nEntryCountTotal : UDINT; (*Kann zur Anzeige der totalen Eintr�ge verwendet werden*)
		nScrollTotal : UDINT; (*Interne Variable*)
		btnLeft : BrbVc4Button_TYP; (*Control*)
		btnPageLeft : BrbVc4IncButton_TYP; (*Control*)
		btnLineLeft : BrbVc4IncButton_TYP; (*Control*)
		btnLineRight : BrbVc4IncButton_TYP; (*Control*)
		btnPageRight : BrbVc4IncButton_TYP; (*Control*)
		btnRight : BrbVc4Button_TYP; (*Control*)
		bScrollDone : BOOL; (*Ausgang: 1=Es wurde gescrollt*)
	END_STRUCT;
	BrbVc4ScrollbarVer_TYP : 	STRUCT 
		bDisabled : BOOL; (*Eingang: 1=Disabled*)
		nTotalIndexMin : DINT; (*Eingang: Kleinster Index des Quelle*)
		nTotalIndexMax : DINT; (*Eingang: Gr��ter Index des Quelle*)
		nCountShow : UDINT; (*Eingang: Anzahl der angezeigen Zeilen*)
		nEntryCountTotal : UDINT; (*Kann zur Anzeige der totalen Eintr�ge verwendet werden*)
		nScrollTotal : UDINT; (* Interne Variable*)
		btnTop : BrbVc4Button_TYP; (*Control*)
		btnPageUp : BrbVc4IncButton_TYP; (*Control*)
		btnLineUp : BrbVc4IncButton_TYP; (*Control*)
		btnLineDown : BrbVc4IncButton_TYP; (*Control*)
		btnPageDown : BrbVc4IncButton_TYP; (*Control*)
		btnBottom : BrbVc4Button_TYP; (*Control*)
		bScrollDone : BOOL; (*Ausgang: 1=Es wurde gescrollt*)
	END_STRUCT;
	BrbVc4Scrollbar_TYP : 	STRUCT 
		Horizontal : BrbVc4ScrollbarHor_TYP; (*Horizontale Scrollbar*)
		Vertical : BrbVc4ScrollbarVer_TYP; (*Vertikale Scrollbar*)
	END_STRUCT;
	BrbVc4ScrollList_TYP : 	STRUCT 
		bGetList : BOOL; (*1=Liste wurde gescrollt und muss neu angezeigt werden*)
		nScrollOffset : DINT; (*Momentaner Offset des angezeigten Auschnitts der Quelle*)
		nSelectedIndex : DINT; (*Index der momentan selektierten Zeile*)
	END_STRUCT;
	BrbVc4Shape_TYP : 	STRUCT 
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Slider_TYP : 	STRUCT 
		nValueShow : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueInput : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMin : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMax : DINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4String_TYP : 	STRUCT 
		sValue : STRING[nBRB_STRING_INPUT_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4TabPage_TYP : 	STRUCT 
		btnPage : BrbVc4Button_TYP; (*Control*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4TabCtrl_TYP : 	STRUCT 
		nSelectedTabPageIndex : UINT; (*Ausgang: Selektierter Tab-Reiter*)
		TabPage : ARRAY[0..nBRB_TAB_PAGE_INDEX_MAX]OF BrbVc4TabPage_TYP; (*Tab-Reiter*)
	END_STRUCT;
	BrbVc4Text_TYP : 	STRUCT 
		nIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4ToggleButtonStates_ENUM : 
		(
		eBRB_TOG_BTN_STATE_UNPUSHED, (*0=Nicht gedr�ckt*)
		eBRB_TOG_BTN_STATE_UNPUSHED_EDGE, (*1=Gerade losgelassen*)
		eBRB_TOG_BTN_STATE_PUSHED_EDGE, (*2=Gerade gedr�ckt*)
		eBRB_TOG_BTN_STATE_PUSHED (*3=Gedr�ckt*)
		);
	BrbVc4ToggleButton_TYP : 	STRUCT 
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bPushed : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		eToggleState : BrbVc4ToggleButtonStates_ENUM; (*Ausgang: Momentaner Status des Buttons*)
		bPushedOld : BOOL; (*Interne Variable*)
	END_STRUCT;
	BrbVc4ToggleButtonExt_TYP : 	STRUCT 
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bVisPushed : BOOL; (*Zur Anbindung an den Datenpunkt*)
		bPushed : BOOL; (*Zur Verwendung im Programm*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		eToggleState : BrbVc4ToggleButtonStates_ENUM; (*Ausgang: Momentaner Status des Buttons*)
		bVisPushedOld : BOOL; (*Interne Variable*)
		bPushedOld : BOOL; (*Interne Variable*)
	END_STRUCT;
	BrbVc4Touchgrid_TYP : 	STRUCT 
		bClickEnabled : BOOL; (*Eingang: 1=Klicks werden erkannt*)
		bUseSyncTouchState : BOOL; (*Eingang: 1=Auswertung des synchronisierten TouchStates*)
		bDrawGrid : BOOL; (*Eingang: 1=Gitter wird gezeichnet*)
		bDrawMarker : BOOL; (*Eingang: 1=Markierung wird gezeichnet*)
		nGridLeft : DINT; (*Eingang: Linke Koordinate*)
		nGridTop : DINT; (*Eingang: Obere Koordinate*)
		nCellWidth : UINT; (*Eingang: Breite einer Zelle*)
		nIndexMaxX : UINT; (*Eingang: Maximaler Index der Zellen in X-Richtung*)
		nCellHeight : UINT; (*Eingang: H�he einer Zelle*)
		nIndexMaxY : UINT; (*Eingang: Maximaler Index der Zellen in Y-Richtung*)
		nGridColor : UINT; (*Eingang: Farbe des Gitters*)
		eMarkerFigure : BrbVc4Figures_ENUM; (*Eingang: Figur der Markierung (z.B. Rechteck)*)
		MarkerLine : BrbVc4Line_TYP; (*Eingang: Markierung als Linie*)
		MarkerRectangle : BrbVc4Rectangle_TYP; (*Eingang: Markierung als Rechteck*)
		MarkerEllipse : BrbVc4Ellipse_TYP; (*Eingang: Markierung als Ellipse*)
		MarkerArc : BrbVc4Arc_TYP; (*Eingang: Markierung als Bogen*)
		MarkerText : BrbVc4DrawText_TYP; (*Eingang: Markierung als Text*)
		nSelectedIndexX : UINT; (*Ausgang: Selektierte Spalte*)
		nSelectedIndexY : UINT; (*Ausgang: Selektierte Zeile*)
		nSelectedIndex : UINT; (*Ein-/Ausgang: Selektierter Index*)
		nSelectedIndexMax : UDINT; (*Ausgang: Maximal Selektier-Index*)
		eState : BrbVc4TouchStates_ENUM; (*Ausgang: Momentaner Status des Touchs*)
	END_STRUCT;
END_TYPE

(*VisVc4Draw*)

TYPE
	BrbVc4Figures_ENUM : 
		(
		eBRB_FIGURE_LINE, (*0=Linie*)
		eBRB_FIGURE_RECTANGLE, (*1=Rechteck*)
		eBRB_FIGURE_ELLIPSE, (*2=Ellipse*)
		eBRB_FIGURE_ARC, (*3=Bogen*)
		eBRB_FIGURE_TEXT (*4=Text*)
		);
	BrbVc4Point_TYP : 	STRUCT 
		nX : DINT; (*X-Koordinate*)
		nY : DINT; (*Y-Koordinate*)
	END_STRUCT;
	BrbVc4Line_TYP : 	STRUCT 
		nLeft : DINT; (*Eingang: Linke Koordinate*)
		nTop : DINT; (*Eingang: Obere Koordinate*)
		nRight : DINT; (*Eingang: Rechte Koordinate*)
		nBottom : DINT; (*Eingang: Untere Koordinate*)
		nColor : UINT; (*Eingang: Farbe*)
		nDashWidth : UINT; (*Eingang: Breite f�r Strichelung (0=Solide)*)
	END_STRUCT;
	BrbVc4Rectangle_TYP : 	STRUCT 
		nLeft : DINT; (*Eingang: Linke Koordinate*)
		nTop : DINT; (*Eingang: Obere Koordinate*)
		nWidth : DINT; (*Eingang: Breite*)
		nHeight : DINT; (*Eingang: H�he*)
		nFillColor : UINT; (*Eingang: F�ll-Farbe*)
		nBorderColor : UINT; (*Eingang: Rand-Farbe*)
		nDashWidth : UINT; (*Eingang: Breite f�r Strichelung (0=Solide)*)
	END_STRUCT;
	BrbVc4Ellipse_TYP : 	STRUCT 
		nLeft : DINT; (*Eingang: Linke Koordinate*)
		nTop : DINT; (*Eingang: Obere Koordinate*)
		nWidth : DINT; (*Eingang: Breite*)
		nHeight : DINT; (*Eingang: H�he*)
		nFillColor : UINT; (*Eingang: F�ll-Farbe*)
		nBorderColor : UINT; (*Eingang: F�ll-Farbe*)
		nDashWidth : UINT; (*Eingang: Breite f�r Strichelung (0=Solide)*)
	END_STRUCT;
	BrbVc4Arc_TYP : 	STRUCT 
		nLeft : DINT; (*Eingang: Linke Koordinate*)
		nTop : DINT; (*Eingang: Obere Koordinate*)
		nWidth : DINT; (*Eingang: Breite*)
		nHeight : DINT; (*Eingang: H�he*)
		rStartAngle : REAL; (*Eingang: Start-Winkel (0..360�)*)
		rEndAngle : REAL; (*Eingang: End-Winkel (0..360�)*)
		nFillColor : UINT; (*Eingang: F�ll-Farbe (momentan nicht unterst�tzt)*)
		nBorderColor : UINT; (*Eingang: Rand-Farbe*)
		nDashWidth : UINT; (*Eingang: Breite f�r Strichelung (0=Solide)*)
	END_STRUCT;
	BrbVc4DrawText_TYP : 	STRUCT 
		nLeft : DINT; (*Eingang: Linke Koordinate*)
		nTop : DINT; (*Eingang: Obere Koordinate*)
		nFontIndex : UINT; (*Eingang: Index der Schrift*)
		nTextColor : UINT; (*Eingang: Text-Farbe*)
		nBackgroundColor : UINT; (*Eingang: Hintergrund-Farbe*)
		sText : STRING[nBRB_DRAW_TEXT_CHAR_MAX]; (*Eingang: Text*)
	END_STRUCT;
	BrbVc4Font_TYP : 	STRUCT 
		nIndex : UINT; (*Parameter: Index der Schrift*)
		rCharWidth : REAL; (*Parameter: Durchschnittliche Breite eines Zeichens in [Pixel]*)
		rCharHeight : REAL; (*Parameter: H�he eines Zeichens in [Pixel]*)
	END_STRUCT;
END_TYPE

(*VisVc4DrawExt*)

TYPE
	BrbVc4DrawPadding_TYP : 	STRUCT  (*Einr�ckung*)
		nTop : DINT; (*Eingang: Einr�ckung Oben*)
		nBottom : DINT; (*Eingang: Einr�ckung Unten*)
		nLeft : DINT; (*Eingang: Einr�ckung Links*)
		nRight : DINT; (*Eingang: Einr�ckung Rechts*)
	END_STRUCT;
	BrbVc4DrawTouchBorderCorr_TYP : 	STRUCT  (*Touch-Korrektur des Rahmens*)
		nX : SINT; (*Eingang: Offset X f�r die Touch-Korrektur des Rahmens*)
		nY : SINT; (*Eingang: Offset Y f�r die Touch-Korrektur des Rahmens*)
	END_STRUCT;
	BrbVc4DrawTrendCfgGrid_TYP : 	STRUCT  (*Konfiguration eines Trend-Gitters*)
		bShow : BOOL; (*Eingang: 1=Gitter anzeigen*)
		nColor : UINT; (*Eingang: Farbe des Gitters*)
	END_STRUCT;
	BrbVc4DrawTrendCfgScaleY_TYP : 	STRUCT  (*Konfiguration einer Trend-Wert-Skala*)
		bShow : BOOL; (*Eingang: 1=Anzeigen*)
		nColor : UINT; (*Eingang: Farbe der Skala*)
		nLinesCount : UINT; (*Eingang: Anzahl der Skalenstriche*)
		nLineLength : DINT; (*Eingang: L�nge der Skalenstriche*)
		sUnit : STRING[nBRB_DRAW_TEXT_CHAR_MAX]; (*Eingang; Angezeigter Einheiten-Text*)
		Grid : BrbVc4DrawTrendCfgGrid_TYP; (*Konfiguration des Gitters*)
		rMin : REAL; (*Eingang: Unterer Skalenwert*)
		rMax : REAL; (*Eingang: Oberer Skalenwert*)
		nFractionDigits : UINT; (*Eingang: Anzahl der anzuzeigenden Nachkommastellen*)
	END_STRUCT;
	BrbVc4DrawTrendCfgScaleX_TYP : 	STRUCT  (*Konfiguration der Trend-Zeit-Skala*)
		bShow : BOOL; (*Eingang: 1=Anzeigen*)
		nColor : DINT; (*Eingang: Farbe*)
		nLinesCount : UINT; (*Eingang: Anzahl der Skalenstriche*)
		nLineLength : DINT; (*Eingang: L�nge der Skalenstriche*)
		Grid : BrbVc4DrawTrendCfgGrid_TYP; (*Konfiguration des Gitters*)
		sFormat : STRING[nBRB_TIME_TEXT_CHAR_MAX]; (*Eingang: Format der Zeit-Angabe (yyyy.mm.dd hh:MM:ss:mil)*)
		dtsStartTime : DTStructure; (*Eingang: Zeitstempel zum Beginn des Trends*)
		nInterval : UDINT; (*Eingang: Intervall in [ms]*)
		nZoomValueCount : UDINT; (*Eingang: Anzahl der anzuzeigenden Samplewerte auf die ganze Trendbreite*)
		nScrollOffset : DINT; (*Eingang: Scroll-Angabe bei Zoom*)
		bLimitScrollOffset : BOOL; (*Eingang: 1=Scroll-Offset wird begrenzt*)
	END_STRUCT;
	BrbVc4DrawTrendCfgTouchAct_TYP : 	STRUCT  (*Konfiguration der Trend-Touch-Aktion*)
		BorderCorrection : BrbVc4DrawTouchBorderCorr_TYP; (*Touch-Korrektur des Rahmens*)
		bSetCursor0 : BOOL; (*Eingang: 1=Cursor0 setzen*)
		bSetCursor1 : BOOL; (*Eingang: 1=Cursor1 setzen*)
		bZoomX : BOOL; (*Eingang: 1=ZoomX aktivieren*)
		bZoomY : BOOL; (*Eingang: 1=ZoomY aktivieren*)
		bScrollX : BOOL; (*Eingang: 1=ScrollingX aktivieren*)
		bScrollY : BOOL; (*Eingang: 1=ScrollingY aktivieren*)
	END_STRUCT;
	BrbVc4DrawTrendCfgCurve_TYP : 	STRUCT  (*Konfiguration einer Trend-Kurve*)
		bShow : BOOL; (*Eingang: 1=Anzeigen*)
		nColor : DINT; (*Eingang: Farbe*)
		eScaleY : BrbVc4TrendScaleYIndex_ENUM; (*Eingang: Skalen-Zugeh�rigkeit (0=Links, 1=Rechts)*)
		eMode : BrbVc4TrendCurveMode_ENUM; (*Eingang: Kurven-Zeichen-Modus*)
		eValueSource : BrbVc4TrendSource_ENUM; (*Eingang: Quelle*)
		eValueDatatype : BrbVc4TrendValueDatatype_ENUM; (*Eingang: Datentyp*)
		pValueSource : UDINT; (*Eingang: Zeiger auf den Anfang der Quelle*)
		nStructSize : UDINT; (*Eingang: Gr��e der Struktur bei Struktur-Array-Quelle*)
		nStructMemberOffset : UDINT; (*Eingang: Offset des Wertes in der Struktur bei Struktur-Array-Quelle*)
		rConversionFactor : REAL; (*Eingang: Faktor zur Umrechung der Quell-Daten auf die eingestellte Skala*)
		bCalculateStatistics : BOOL; (*Eingang: 1=Statistik-Werte berechnen*)
	END_STRUCT;
	BrbVc4DrawTrendCfgCursor_TYP : 	STRUCT  (*Konfiguration eines Trend-Cursors*)
		bShow : BOOL; (*Eingang: 1=Anzeigen*)
		nColor : DINT; (*Eingang: Farbe*)
		nSampleIndex : DINT; (*Eingang: Position des Cursors*)
	END_STRUCT;
	BrbVc4DrawTrendCfgCallbacks_TYP : 	STRUCT  (*Konfiguration der Trend-Aufrufe*)
		pCallbackAfterClear : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach L�schen der Drawbox*)
		pCallbackAfterCurveArea : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen des Kurvenbereichs*)
		pCallbackAfterScaleLineY : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen eines Werte-Skalierungs-Strichs*)
		pCallbackAfterScaleY : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen einer Werte-Skala*)
		pCallbackAfterScaleLineX : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen eines Zeit-Skalierungs-Strichs*)
		pCallbackAfterScaleX : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen der Zeit-Skala*)
		pCallbackAfterCurve : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen einer Kurve*)
		pCallbackAfterCursor : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen eines Cursors*)
		pCallbackAfterZoomWindow : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen des Zoom-Fensters*)
	END_STRUCT;
	BrbVc4DrawTrendCfg_TYP : 	STRUCT  (*Konfiguration des Trends*)
		Drawbox : BrbVc4Drawbox_TYP; (*Angaben zur Drawbox*)
		Padding : BrbVc4DrawPadding_TYP; (*Einr�ckung des Kurvenbereichs*)
		nCurveAreaColor : UINT; (*Farbe des Kurvenbereichs*)
		ScaleFont : BrbVc4Font_TYP; (*Eingang: Font der Skalierung*)
		ScaleY : ARRAY[0..nBRB_TREND_SCALE_Y_INDEX_MAX]OF BrbVc4DrawTrendCfgScaleY_TYP; (*Konfiguration der Werte-Skalen*)
		nSourceArrayIndexMax : DINT; (*Eingang: Maximaler Index der Quell-Arrays*)
		ScaleX : BrbVc4DrawTrendCfgScaleX_TYP; (*Konfiguration der Zeit-Skala*)
		TouchAction : BrbVc4DrawTrendCfgTouchAct_TYP; (*Konfiguration der Touch-Aktion*)
		Curve : ARRAY[0..nBRB_TREND_CURVE_INDEX_MAX]OF BrbVc4DrawTrendCfgCurve_TYP; (*Konfiguration der Kurven*)
		Cursor : ARRAY[0..1]OF BrbVc4DrawTrendCfgCursor_TYP; (*Konfiguration der Cursor*)
		Callbacks : BrbVc4DrawTrendCfgCallbacks_TYP; (*Konfiguration der Aufrufe*)
		pTag : UDINT; (*Eingang: Zeiger auf Benutzer-Daten*)
	END_STRUCT;
	BrbVc4DrawTrendStateCurveCur_TYP : 	STRUCT  (*Status des Cursors unter der Kurve*)
		rValue : REAL; (*Ausgang: Kurven-Wert unter dem Cursor*)
		dtsTimeStamp : DTStructure; (*Ausgang: Zeitstempel unter dem Cursor*)
	END_STRUCT;
	BrbVc4DrawTrendStateCurve_TYP : 	STRUCT  (*Status der Kurve*)
		rValueMax : REAL; (*Ausgang: Maximaler Wert der Kurve*)
		rValueMin : REAL; (*Ausgang: Minimaler Wert der Kurve*)
		rValueAverage : REAL; (*Ausgang: Wert-Durchschnitt der Kurve*)
		Cursor : ARRAY[0..1]OF BrbVc4DrawTrendStateCurveCur_TYP; (*Status eines Cursors unter der Kurve*)
	END_STRUCT;
	BrbVc4DrawTrendState_TYP : 	STRUCT  (*Status des Trends*)
		eTouchAction : BrbVc4TrendTouchAction_ENUM; (*Status einer Touch-Aktion*)
		Curve : ARRAY[0..nBRB_TREND_CURVE_INDEX_MAX]OF BrbVc4DrawTrendStateCurve_TYP; (*Status einer Kurve*)
	END_STRUCT;
	BrbVc4DrawTrendInternScaleY_TYP : 	STRUCT  (*Interne Variablen f�r die Wert-Skalen*)
		rValueScaleFactor : REAL; (*Intern: Pixel-Skalier-Faktor*)
		rScaleDistance : REAL; (*Intern: Abstand der Skalierungsstriche*)
		ScaleLine : BrbVc4Line_TYP; (*Intern: Hauptlinie der Skala*)
	END_STRUCT;
	BrbVc4DrawTrendInternTouchAc_TYP : 	STRUCT  (*Interne Variablen f�r Touch-Aktion*)
		TouchPointDrawbox : BrbVc4Point_TYP; (*Intern: Koordinaten des letzten Klicks bezogen auf Drawbox*)
		TouchPointCurveArea : BrbVc4Point_TYP; (*Intern: Koordinaten des letzten Klicks bezogen auf Kurven-Bereich*)
		LastScrollPosition : BrbVc4Point_TYP; (*Intern: Koordinaten beim letzten Aufruf f�r Scrolling*)
		LastScrollDifference : BrbVc4Point_TYP; (*Intern: Differenz beim letzten Aufruf f�r Scrolling*)
		fbZoomDragDelay : TON; (*Intern: Verz�gerung der Zoom-Funktion*)
	END_STRUCT;
	BrbVc4DrawTrendInternScaleX_TYP : 	STRUCT  (*Interne Variablen f�r die Zeit-Skala*)
		rZoomScaleFactor : REAL; (*Intern: Pixel-Skalier-Faktor*)
		rScaleDistance : REAL; (*Intern: Pixel-Abstand der Skalierungsstriche*)
		ScaleLine : BrbVc4Line_TYP; (*Intern: Hauptlinie der Skala*)
		nScrollOffsetMax : DINT; (*Intern: Begrenzung des Scroll-Offsets*)
	END_STRUCT;
	BrbVc4DrawTrendInternCurve_TYP : 	STRUCT  (*Interne Variablen f�r die Kurven*)
		rValueSum : REAL; (*Intern: Summe aller Kurven-Werte*)
	END_STRUCT;
	BrbVc4DrawTrendInternCursor_TYP : 	STRUCT  (*Interne Variablen f�r die Cursor*)
		CursorLine : BrbVc4Line_TYP; (*Intern: Linie des Cursors*)
	END_STRUCT;
	BrbVc4DrawTrendInternZoomWin_TYP : 	STRUCT  (*Interne Variablen f�r das Zoom-Fenster*)
		bShow : BOOL; (*Intern: 1=Zoom-Fenster anzeigen*)
		RectangleTouch : BrbVc4Rectangle_TYP; (*Intern: Rechteck des gezogenen Zoom-Fensters*)
		RectangleDraw : BrbVc4Rectangle_TYP; (*Intern: Rechteck des gezeichneten Zoom-Fensters*)
	END_STRUCT;
	BrbVc4DrawTrendIntern_TYP : 	STRUCT  (*Interne Variablen f�r den Trend*)
		nAccessStatus : UINT; (*Intern: Status der Access-Funktion*)
		nAttachStatus : UINT; (*Intern: Status der Attach-Funktion*)
		CurveArea : BrbVc4Rectangle_TYP; (*Intern: Rechteck des Kurvenbereichs*)
		ScaleY : ARRAY[0..nBRB_TREND_SCALE_Y_INDEX_MAX]OF BrbVc4DrawTrendInternScaleY_TYP; (*Interne Variablen f�r die Wert-Skalen*)
		TouchAction : BrbVc4DrawTrendInternTouchAc_TYP; (*Interne Variablen f�r Touch-Aktion*)
		ScaleX : BrbVc4DrawTrendInternScaleX_TYP; (*Interne Variablen f�r die Zeit-Skala*)
		Curve : ARRAY[0..nBRB_TREND_CURVE_INDEX_MAX]OF BrbVc4DrawTrendInternCurve_TYP; (*Interne Variablen f�r die Kurven*)
		Cursor : ARRAY[0..1]OF BrbVc4DrawTrendInternCursor_TYP; (*Interne Variablen f�r die Cursor*)
		ZoomWindow : BrbVc4DrawTrendInternZoomWin_TYP; (*Interne Variablen f�r das Zoom-Fenster*)
	END_STRUCT;
	BrbVc4DrawTrend_TYP : 	STRUCT 
		bEnable : BOOL; (*1=Aktiv*)
		nRedrawCounterMatch : UINT; (*Redraw-Cycle, bei dem gezeichnet wird*)
		Cfg : BrbVc4DrawTrendCfg_TYP; (*Konfiguration des Trends*)
		State : BrbVc4DrawTrendState_TYP; (*Status des Trends*)
		Intern : BrbVc4DrawTrendIntern_TYP; (*Interne berechnete Daten*)
	END_STRUCT;
	BrbVc4TrendScaleYIndex_ENUM : 
		( (*Index der Skala 0..1*)
		eBRB_TREND_SCALE_Y_INDEX_LEFT, (*Links*)
		eBRB_TREND_SCALE_Y_INDEX_RIGHT (*Rechts*)
		);
	BrbVc4TrendCurveMode_ENUM : 
		( (*Kurven-Zeichen-Modus*)
		eBRB_TREND_CURVE_MODE_LINED, (*Linear*)
		eBRB_TREND_CURVE_MODE_STEPPED (*Gestuft*)
		);
	BrbVc4TrendSource_ENUM : 
		( (*Quellen-Angabe 0..1*)
		eBRB_TREND_SOURCE_SINGLE_ARRAY, (*Einfaches Array*)
		eBRB_TREND_SOURCE_STRUCT_ARRAY (*Struktur-Array*)
		);
	BrbVc4TrendValueDatatype_ENUM : 
		( (*Datentyp der Kurven-Werte*)
		eBRB_TREND_VALUE_DATATYPE_REAL, (*REAL*)
		eBRB_TREND_VALUE_DATATYPE_DINT, (*DINT*)
		eBRB_TREND_VALUE_DATATYPE_INT (*INT*)
		);
	BrbVc4TrendTouchAction_ENUM : 
		( (*Touch-Aktionen des Trends*)
		eBRB_TREND_TOUCHACTION_NONE, (*Keine Touch-Aktion des Trends 0..4*)
		eBRB_TREND_TOUCHACTION_CURS_SET, (*Cursor wurde gesetzt*)
		eBRB_TREND_TOUCHACTION_SCROLL, (*Momentan wird gescrollt*)
		eBRB_TREND_TOUCHACTION_ZOOM_DRAG, (*Momentan wird das Zoom-Fenster gezogen*)
		eBRB_TREND_TOUCHACTION_ZOOM_SET (*Das Zoom-Fenster wurde gesetzt*)
		);
	BrbVc4DrawTrendLinkCfgTrend_TYP : 	STRUCT  (*Zeiger auf zu linkende Trends*)
		pTrend : REFERENCE TO BrbVc4DrawTrend_TYP; (*Eingang; Zeiger auf zu linkenden Trend, 0=deaktiviert*)
	END_STRUCT;
	BrbVc4DrawTrendLinkCfg_TYP : 	STRUCT  (*Konfiguration des TrendLinks*)
		Trend : ARRAY[0..nBRB_TRENDLINK_TREND_INDEX_MAX]OF BrbVc4DrawTrendLinkCfgTrend_TYP; (*Zeiger auf zu linkende Trends*)
		bLinkSetCursor0 : BOOL; (*Eingang: 1=Cursor0 setzen ist gelinkt*)
		bLinkSetCursor1 : BOOL; (*Eingang: 1=Cursor1 setzen ist gelinkt*)
		bLinkZoomX : BOOL; (*Eingang: 1=ZoomX ist gelinkt*)
		bLinkZoomY : BOOL; (*Eingang: 1=ZoomY ist gelinkt*)
		bLinkScrollX : BOOL; (*Eingang: 1=ScrollingX ist gelinkt*)
		bLinkScrollY : BOOL; (*Eingang: 1=ScrollingY ist gelinkt*)
	END_STRUCT;
	BrbVc4LinkTrends_TYP : 	STRUCT 
		bEnable : BOOL; (*1=Aktiv*)
		Cfg : BrbVc4DrawTrendLinkCfg_TYP; (*Konfiguration des TrendLinks*)
	END_STRUCT;
	BrbVc4DrawPlotCfgScaleY_TYP : 	STRUCT  (*Konfiguration der Y-Skala*)
		bShow : BOOL; (*Eingang: 1=Skalierung der Y-Achse darstellen*)
		nColor : UINT; (*Eingang: Farbe der Skala*)
		nLinesCount : UINT; (*Eingang: Anzahl der Skalenstriche*)
		nLineLength : DINT; (*Eingang: L�nge der Skalenstriche*)
		Grid : BrbVc4DrawTrendCfgGrid_TYP; (*Konfiguration des Gitters*)
		rMin : REAL; (*Eingang: Kleinster Wert der Y-Achse*)
		rMax : REAL; (*Eingang: Gr��ter Wert der Y-Achse*)
		nFractionDigits : UINT; (*Eingang: Anzahl der anzuzeigenden Nachkommastellen*)
	END_STRUCT;
	BrbVc4DrawPlotCfgScaleX_TYP : 	STRUCT  (*Konfiguration der X-Skala*)
		bShow : BOOL; (*Eingang: 1=Skalierung der Y-Achse darstellen*)
		nColor : UINT; (*Eingang: Farbe der Skala*)
		nLinesCount : UINT; (*Eingang: Anzahl der Skalenstriche*)
		nLineLength : DINT; (*Eingang: L�nge der Skalenstriche*)
		Grid : BrbVc4DrawTrendCfgGrid_TYP; (*Konfiguration des Gitters*)
		rMin : REAL; (*Eingang: Kleinster Wert der Y-Achse*)
		rMax : REAL; (*Eingang: Gr��ter Wert der Y-Achse*)
		nFractionDigits : UINT; (*Eingang: Anzahl der anzuzeigenden Nachkommastellen*)
	END_STRUCT;
	BrbVc4DrawPlotCfgSource_TYP : 	STRUCT  (*Konfiguration der Plot-Quelle*)
		nSourceArrayIndexMax : UDINT; (*Eingang: Max. Index des Werte-Arrays*)
		eValueSource : BrbVc4PlotSource_ENUM; (*Eingang: Quelle*)
		pArrayX : UDINT; (*Eingang: Zeiger auf den ersten Eintrag im X-Werte-Array*)
		pArrayY : UDINT; (*Eingang: Zeiger auf den ersten Eintrag im Y-Werte-Array*)
		pArrayStruct : UDINT; (*Eingang: Zeiger auf den ersten Eintrag im Struktur-Array*)
		nStructSize : UDINT; (*Eingang: Gr��e der Struktur bei Struktur-Array-Quelle*)
		nStructMemberOffsetX : UDINT; (*Eingang: Offset des X-Wertes in der Struktur bei Struktur-Array-Quelle*)
		nStructMemberOffsetY : UDINT; (*Eingang: Offset des Y-Wertes in der Struktur bei Struktur-Array-Quelle*)
	END_STRUCT;
	BrbVc4DrawPlotCfgTouchAct_TYP : 	STRUCT  (*Konfiguration der Plot-Touch-Aktion*)
		BorderCorrection : BrbVc4DrawTouchBorderCorr_TYP; (*Touch-Korrektur des Rahmens*)
		bSetCursor : BOOL; (*Eingang: 1=Cursor setzen*)
		bZoomX : BOOL; (*Eingang: 1=ZoomX aktivieren*)
		bZoomY : BOOL; (*Eingang: 1=ZoomY aktivieren*)
		bScrollX : BOOL; (*Eingang: 1=ScrollingX aktivieren*)
		bScrollY : BOOL; (*Eingang: 1=ScrollingY aktivieren*)
	END_STRUCT;
	BrbVc4DrawPlotCfgCursor_TYP : 	STRUCT  (*Konfiguration des Plot-Cursors*)
		bShow : USINT; (*Eingang: 1=Cursor darstellen*)
		nColor : UINT; (*Eingang: Farbe des Cursors*)
		rCursorX : REAL; (*Eingang: Aktueller X-Cursor-Wert*)
		rCursorY : REAL; (*Eingang: Aktueller X-CursorWert*)
	END_STRUCT;
	BrbVc4DrawPlotCfgCallbacks_TYP : 	STRUCT  (*Konfiguration der Plot-Aufrufe*)
		pCallbackAfterClear : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach L�schen der Drawbox*)
		pCallbackAfterCurveArea : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen des Kurvenbereichs*)
		pCallbackAfterZeroLines : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen der Null-Linien*)
		pCallbackAfterScaleLineY : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen eines Werte-Skalierungs-Strichs*)
		pCallbackAfterScaleY : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen einer Y-Skala*)
		pCallbackAfterScaleLineX : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen eines Zeit-Skalierungs-Strichs*)
		pCallbackAfterScaleX : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen der X-Skala*)
		pCallbackAfterCurve : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen der Kurve*)
		pCallbackAfterCursor : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen des Cursors*)
		pCallbackAfterZoomWindow : UDINT; (*Eingang: Funktions-Zeiger f�r Aufruf nach Zeichnen des Zoom-Fensters*)
	END_STRUCT;
	BrbVc4DrawPlotCfg_TYP : 	STRUCT  (*Konfiguration des Plots*)
		Drawbox : BrbVc4Drawbox_TYP; (*Angaben zur Drawbox*)
		Padding : BrbVc4DrawPadding_TYP; (*Einr�ckung des Kurvenbereichs*)
		nCurveAreaColor : UINT; (*Farbe des Kurvenbereichs*)
		bShowZeroLines : BOOL; (*Eingang: 1=Null-Linien darstellen*)
		nZeroLinesColor : UINT; (*Eingang: Farbe der Null-Linien*)
		ScaleFont : BrbVc4Font_TYP; (*Eingang: Font der Skalierung*)
		ScaleY : BrbVc4DrawPlotCfgScaleY_TYP; (*Konfiguration der Y-Skala*)
		ScaleX : BrbVc4DrawPlotCfgScaleX_TYP; (*Konfiguration der X-Skala*)
		Source : BrbVc4DrawPlotCfgSource_TYP; (*Konfiguration der Plot-Quelle*)
		nCurveColor : UINT; (*Eingang: Farbe der Kurve*)
		TouchAction : BrbVc4DrawPlotCfgTouchAct_TYP; (*Konfiguration der Plot-Touch-Aktion*)
		Cursor : BrbVc4DrawPlotCfgCursor_TYP; (*Konfiguration des Plot-Cursors*)
		Callbacks : BrbVc4DrawPlotCfgCallbacks_TYP; (*Konfiguration der Aufrufe*)
		pTag : UDINT; (*Eingang: Zeiger auf Benutzer-Daten*)
	END_STRUCT;
	BrbVc4DrawPlotStateStatistic_TYP : 	STRUCT  (*Statistik des Plots*)
		rMinX : REAL; (*Ausgang: Kleinster Wert der X-Achse*)
		rMaxX : REAL; (*Ausgang: Gr��ter Wert der X-Achse*)
		rMinY : REAL; (*Ausgang: Kleinster Wert der Y-Achse*)
		rMaxY : REAL; (*Ausgang: Gr��ter Wert der Y-Achse*)
	END_STRUCT;
	BrbVc4DrawPlotState_TYP : 	STRUCT  (*Status des Plots*)
		eTouchAction : BrbVc4PlotTouchAction_ENUM; (*Status einer Touch-Aktion*)
		nCursorSampleIndex : DINT; (*Ausgang: Sample-Index des geklickten Cursors*)
		Statistic : BrbVc4DrawPlotStateStatistic_TYP; (*Statistische Werte*)
	END_STRUCT;
	BrbVc4DrawPlotInternScaleY_TYP : 	STRUCT  (*Interne Variablen f�r die Y-Skala*)
		rValueScaleFactor : REAL; (*Intern: Pixel-Skalier-Faktor*)
		rScaleDistance : REAL; (*Intern: Abstand der Skalierungsstriche*)
		ScaleLine : BrbVc4Line_TYP; (*Intern: Hauptlinie der Skala*)
	END_STRUCT;
	BrbVc4DrawPlotInternTouchAc_TYP : 	STRUCT  (*Interne Variablen f�r Touch-Aktion*)
		TouchPointDrawbox : BrbVc4Point_TYP; (*Intern: Koordinaten des letzten Klicks bezogen auf Drawbox*)
		TouchPointCurveArea : BrbVc4Point_TYP; (*Intern: Koordinaten des letzten Klicks bezogen auf Kurven-Bereich*)
		LastScrollPosition : BrbVc4Point_TYP; (*Intern: Koordinaten beim letzten Aufruf f�r Scrolling*)
		LastScrollDifference : BrbVc4Point_TYP; (*Intern: Differenz beim letzten Aufruf f�r Scrolling*)
		fbZoomDragDelay : TON; (*Intern: Verz�gerung der Zoom-Funktion*)
	END_STRUCT;
	BrbVc4DrawPlotInternScaleX_TYP : 	STRUCT  (*Interne Variablen f�r die X-Skala*)
		rValueScaleFactor : REAL; (*Intern: Pixel-Skalier-Faktor*)
		rScaleDistance : REAL; (*Intern: Abstand der Skalierungsstriche*)
		ScaleLine : BrbVc4Line_TYP; (*Intern: Hauptlinie der Skala*)
	END_STRUCT;
	BrbVc4DrawPlotInternCursor_TYP : 	STRUCT  (*Interne Variablen f�r den Cursor*)
		CursorLineY : BrbVc4Line_TYP; (*Intern: Linie des Cursors*)
		CursorLineX : BrbVc4Line_TYP; (*Intern: Linie des Cursors*)
	END_STRUCT;
	BrbVc4DrawPlotInternZoomWin_TYP : 	STRUCT  (*Interne Variablen f�r das Zoom-Fenster*)
		bShow : BOOL; (*Intern: 1=Zoom-Fenster anzeigen*)
		RectangleTouch : BrbVc4Rectangle_TYP; (*Intern: Rechteck des gezogenen Zoom-Fensters*)
		RectangleDraw : BrbVc4Rectangle_TYP; (*Intern: Rechteck des gezeichneten Zoom-Fensters*)
	END_STRUCT;
	BrbVc4DrawPlotIntern_TYP : 	STRUCT  (*Interne Variablen f�r den Plot*)
		nAccessStatus : UINT; (*Intern: Status der Access-Funktion*)
		nAttachStatus : UINT; (*Intern: Status der Attach-Funktion*)
		CurveArea : BrbVc4Rectangle_TYP; (*Intern: Rechteck des Kurvenbereichs*)
		nCurveAreaRight : DINT; (*Intern: Rechte Koordinate des Kurvenbereichs*)
		nCurveAreaBottom : DINT; (*Intern: Untere Koordinate des Kurvenbereichs*)
		ZeroLineY : BrbVc4Line_TYP; (*Intern: Null-Linie Y*)
		ZeroLineX : BrbVc4Line_TYP; (*Intern: Null-Linie X*)
		ScaleY : BrbVc4DrawPlotInternScaleY_TYP; (*Intern: Variablen f�r die Y-Skala*)
		ScaleX : BrbVc4DrawPlotInternScaleX_TYP; (*Interne Variablen f�r die X-Skala*)
		TouchAction : BrbVc4DrawPlotInternTouchAc_TYP; (*Interne Variablen f�r Touch-Aktion*)
		Cursor : BrbVc4DrawPlotInternCursor_TYP; (*Interne Variablen f�r den Cursor*)
		ZoomWindow : BrbVc4DrawPlotInternZoomWin_TYP; (*Interne Variablen f�r das Zoom-Fenster*)
	END_STRUCT;
	BrbVc4DrawPlot_TYP : 	STRUCT 
		bEnable : BOOL; (*1=Aktiv*)
		nRedrawCounterMatch : UINT; (*Redraw-Cycle, bei dem gezeichnet wird*)
		Cfg : BrbVc4DrawPlotCfg_TYP; (*Konfiguration des Plots*)
		State : BrbVc4DrawPlotState_TYP; (*Status des Plots*)
		Intern : BrbVc4DrawPlotIntern_TYP; (*Interne Variablen f�r den Plot*)
	END_STRUCT;
	BrbVc4PlotSource_ENUM : 
		( (*Quellen-Angabe 0..1*)
		eBRB_PLOT_SOURCE_SINGLE_ARRAY, (*Einfaches Array*)
		eBRB_PLOT_SOURCE_STRUCT_ARRAY (*Struktur-Array*)
		);
	BrbVc4PlotTouchAction_ENUM : 
		( (*Keine Touch-Aktion des Plots 0..4*)
		eBRB_PLOT_TOUCHACTION_NONE, (*Keine Touch-Aktion*)
		eBRB_PLOT_TOUCHACTION_CURS_SET, (*Cursor wurde gesetzt*)
		eBRB_PLOT_TOUCHACTION_SCROLL, (*Momentan wird gescrollt*)
		eBRB_PLOT_TOUCHACTION_ZOOM_DRAG, (*Momentan wird das Zoom-Fenster gezogen*)
		eBRB_PLOT_TOUCHACTION_ZOOM_SET (*Das Zoom-Fenster wurde gesetzt*)
		);
	BrbVc4DrawAxisCaptionOrder_ENUM : 
		(
		eBRB_DRAW_AXIS_NAME_POS_VEL, (*0=Name-Position-Geschwindigkeit*)
		eBRB_DRAW_AXIS_NAME_VEL_POS, (*1=Name-Geschwindigkeit-Position*)
		eBRB_DRAW_AXIS_POS_NAME_VEL, (*2=Position-Name-Geschwindigkeit*)
		eBRB_DRAW_AXIS_POS_VEL_NAME, (*3=Position-Geschwindigkeit-Name*)
		eBRB_DRAW_AXIS_VEL_NAME_POS, (*4=Geschwindigkeit-Name-Position*)
		eBRB_DRAW_AXIS_VEL_POS_NAME (*5=Geschwindigkeit-Position-Name*)
		);
	BrbVc4DrawAxisLinear_TYP : 	STRUCT 
		bVertical : BOOL; (*Eingang: 0=Horizontal, 1=Vertikal*)
		bShowDrawArea : BOOL; (*Eingang: 1=Zeichenbereich darstellen*)
		nDrawAreaLeft : UDINT; (*Eingang: Linke Koordinate des Zeichenbereichs in [Pixel]*)
		nDrawAreaTop : UDINT; (*Eingang: Obere Koordinate des Zeichenbereichs in [Pixel]*)
		nDrawAreaWidth : UDINT; (*Eingang: Breite des Zeichenbereichs in [Pixel]*)
		nDrawAreaHeight : UDINT; (*Eingang: H�he des Zeichenbereichs in [Pixel]*)
		nDrawAreaColor : UINT; (*Eingang: Farbe des Zeichenbereichs*)
		nDrawIndent : UDINT; (*Eingang: Einzug links + rechts bzw. oben + unten in [Pixel]*)
		nAxisLimitMin : DINT; (*Eingang: Kleinste Achsposition in [Achseinheiten]*)
		nAxisLimitMax : DINT; (*Eingang: Gr��te Achsposition in [Achseinheiten]*)
		bShowAxisScale : BOOL; (*Eingang: 1=Skalierung darstellen*)
		nAxisScaleCount : UINT; (*Eingang: Anzahl der Skalierungs-Striche*)
		nAxisScaleColor : UINT; (*Eingang: Farbe der Skalierung*)
		AxisScaleFont : BrbVc4Font_TYP; (*Eingang: Font der Skalierung*)
		bHighlightActPosition : BOOL; (*Eingang: 1=Skalierung hervorheben, wenn Achse auf dieser Position*)
		nAxisScaleHighlightColor : UINT; (*Eingang: Farbe der Hervorhebung*)
		bInverted : BOOL; (*Eingang: 1=Achs-Richtung umdrehen*)
		bClip : BOOL; (*Eingang: 1=Ausschnitt anzeigen*)
		nAxisClipRange : UDINT; (*Eingang: Gr��e des Ausschnitts in [Achseinheiten]*)
		bShowAxisPosLine : BOOL; (*Eingang: Achs-Positions-Strich darstellen*)
		bShowAxisBorder : BOOL; (*Eingang: Achs-Beschriftungs-Umrandung darstellen*)
		nAxisColor : UINT; (*Eingang: Farbe des Positions-Strichs und der Umrandung*)
		eAxisCaptionOrder : BrbVc4DrawAxisCaptionOrder_ENUM; (*Eingang: Reihenfolge der Achs-Beschriftung*)
		bShowAxisName : BOOL; (*Eingang: 1=Achsname darstellen*)
		sAxisName : STRING[nBRB_DRAW_TEXT_CHAR_MAX]; (*Eingang: Achsname*)
		nAxisNameColor : UINT; (*Eingang: Farbe des Achsnamens*)
		AxisNameFont : BrbVc4Font_TYP; (*Eingang: Font des Achsnamens*)
		bShowAxisActPosition : BOOL; (*Eingang: 1=Aktuelle Achs-Position darstellen*)
		rAxisActPosition : REAL; (*Eingang: Aktuelle Achs-Position*)
		nAxisActPositionColor : UINT; (*Eingang: Farbe der Achs-Position*)
		bShowAxisActVelocity : BOOL; (*Eingang: 1=Aktuelle Achs-Geschwindigkeit darstellen*)
		rAxisActVelocity : REAL; (*Eingang: Aktuelle Achs-Geschwindigkeit*)
		nAxisActVelocityColor : UINT; (*Eingang: Farbe der Achs-Geschwindigkeit*)
		AxisValueFont : BrbVc4Font_TYP; (*Eingang: Font der Achs-Position und Geschwindigkeit*)
		bShowAxisSetPosition : BOOL; (*Eingang: 1=Soll-Position darstellen*)
		rAxisSetPosition : REAL; (*Eingang: Soll-Achs-Position*)
		nAxisSetPositionColor : UINT; (*Eingang: Farbe der Soll-Achs-Position*)
	END_STRUCT;
	BrbVc4DrawAxisRadial_TYP : 	STRUCT 
		bShowDrawArea : BOOL; (*Eingang: 1=Zeichenbereich darstellen*)
		nDrawAreaLeft : UDINT; (*Eingang: Linke Koordinate des Zeichenbereichs in [Pixel]*)
		nDrawAreaTop : UDINT; (*Eingang: Obere Koordinate des Zeichenbereichs in [Pixel]*)
		nDrawAreaWidth : UDINT; (*Eingang: Breite des Zeichenbereichs in [Pixel]*)
		nDrawAreaHeight : UDINT; (*Eingang: H�he des Zeichenbereichs in [Pixel]*)
		nDrawAreaColor : UINT; (*Eingang: Farbe des Zeichenbereichs*)
		nRadius : UDINT; (*Eingang: Radius der Skalierung in [Pixel]*)
		nAxisLimitMin : DINT; (*Eingang: Kleinste Achsposition in [Achseinheiten]*)
		nAxisLimitMax : DINT; (*Eingang: Gr��te Achsposition in [Achseinheiten]*)
		bShowAxisScale : BOOL; (*Eingang: 1=Skalierung darstellen*)
		nAxisScaleCount : UINT; (*Eingang: Anzahl der Skalierungs-Striche*)
		nAxisScaleColor : UINT; (*Eingang: Farbe der Skalierung*)
		AxisScaleFont : BrbVc4Font_TYP; (*Eingang: Font der Skalierung*)
		bHighlightActPosition : BOOL; (*Eingang: 1=Skalierung hervorheben, wenn Achse auf dieser Position*)
		nAxisScaleHighlightColor : UINT; (*Eingang: Farbe der Hervorhebung*)
		bInverted : BOOL; (*Eingang: 1=Achs-Richtung umdrehen*)
		nOffset : UINT; (*Eingang: 1=Versatz der Achs-Position von 0 bis 360 in[�]*)
		bClip : BOOL; (*Eingang: 1=Ausschnitt anzeigen*)
		nAxisClipRange : UDINT; (*Eingang: Gr��e des Ausschnitts in [Achseinheiten]*)
		bShowAxisPosLine : BOOL; (*Eingang: Achs-Positions-Strich darstellen*)
		nAxisColor : UINT; (*Eingang: Farbe des Positions-Strichs und der Umrandung*)
		eAxisCaptionOrder : BrbVc4DrawAxisCaptionOrder_ENUM; (*Eingang: Reihenfolge der Achs-Beschriftung*)
		bShowAxisName : BOOL; (*Eingang: 1=Achsname darstellen*)
		sAxisName : STRING[nBRB_DRAW_TEXT_CHAR_MAX]; (*Eingang: Achsname*)
		nAxisNameColor : UINT; (*Eingang: Farbe des Achsnamens*)
		AxisNameFont : BrbVc4Font_TYP; (*Eingang: Font des Achsnamens*)
		bShowAxisActPosition : BOOL; (*Eingang: 1=Aktuelle Achs-Position darstellen*)
		rAxisActPosition : REAL; (*Eingang: Aktuelle Achs-Position*)
		nAxisActPositionColor : UINT; (*Eingang: Farbe der Achs-Position*)
		bShowAxisActVelocity : BOOL; (*Eingang: 1=Aktuelle Achs-Geschwindigkeit darstellen*)
		rAxisActVelocity : REAL; (*Eingang: Aktuelle Achs-Geschwindigkeit*)
		nAxisActVelocityColor : UINT; (*Eingang: Farbe der Achs-Geschwindigkeit*)
		AxisValueFont : BrbVc4Font_TYP; (*Eingang: Font der Achs-Position und Geschwindigkeit*)
		bShowAxisSetPosition : BOOL; (*Eingang: 1=Soll-Position darstellen*)
		rAxisSetPosition : REAL; (*Eingang: Soll-Achs-Position*)
		nAxisSetPositionColor : UINT; (*Eingang: Farbe der Soll-Achs-Position*)
	END_STRUCT;
END_TYPE

(*TimeAndDate*)

TYPE
	BrbTimeAndDateCompare_ENUM : 
		(
		eBRB_TAD_COMPARE_YOUNGER,
		eBRB_TAD_COMPARE_YOUNGEREQUAL,
		eBRB_TAD_COMPARE_EQUAL,
		eBRB_TAD_COMPARE_OLDEREQUAL,
		eBRB_TAD_COMPARE_OLDER
		);
	BrbTimerSwitchParTimePoint_TYP : 	STRUCT  (*Schaltpunkt*)
		bActive : BOOL; (*1=Zeitpunkt ist aktiv*)
		dtsTimePoint : DTStructure; (*Zeitpunkt*)
		eSwitchType : BrbTimerSwitchType_ENUM; (*Typ der Schaltung (Aus, An, Umschalten, Puls)*)
	END_STRUCT;
	BrbTimerSwitchPar_TYP : 	STRUCT  (*Parameter der Zeitschaltuhr*)
		eMode : BrbTimerSwitchMode_ENUM; (*Modus der Schaltung (Sekunde, Minute, Stunde...)*)
		TimePoint : ARRAY[0..nBRB_TIMERSWITCH_POINT_INDEX_MAX]OF BrbTimerSwitchParTimePoint_TYP; (*Liste der Schaltpunkte*)
	END_STRUCT;
	BrbTimerSwitchType_ENUM : 
		( (*Typ der Schaltung*)
		eBRB_TIMERSWITCH_TYPE_OFF, (*0 = Aus*)
		eBRB_TIMERSWITCH_TYPE_ON, (*1 = An*)
		eBRB_TIMERSWITCH_TYPE_TOGGLE, (*2 = Umschalten*)
		eBRB_TIMERSWITCH_TYPE_IMPULSE (*3 = Puls*)
		);
	BrbTimerSwitchMode_ENUM : 
		( (*Modus der Schaltung*)
		eBRB_TIMERSWITCH_MODE_MONTH, (*0=Monat*)
		eBRB_TIMERSWITCH_MODE_WEEK, (*1=Woche*)
		eBRB_TIMERSWITCH_MODE_DAY, (*2=Tag*)
		eBRB_TIMERSWITCH_MODE_HOUR, (*3=Stunde*)
		eBRB_TIMERSWITCH_MODE_MINUTE, (*4=Minute*)
		eBRB_TIMERSWITCH_MODE_SECOND (*5=Sekunde*)
		);
	BrbTimerSwitchWeekdays_ENUM : 
		( (*Wochentage*)
		eBRB_TIMERSWITCH_WD_SUNDAY, (*0=Sonntag*)
		eBRB_TIMERSWITCH_WD_MONDAY, (*1=Montag*)
		eBRB_TIMERSWITCH_WD_TUESDAY, (*2=Dienstag*)
		eBRB_TIMERSWITCH_WD_WEDNESDAY, (*3=Mittwoch*)
		eBRB_TIMERSWITCH_WD_THURSDAY, (*4=Donnerstag*)
		eBRB_TIMERSWITCH_WD_FRIDAY, (*5=Freitag*)
		eBRB_TIMERSWITCH_WD_SATURDAY (*6=Samstag*)
		);
END_TYPE

(*Memory*)

TYPE
	BrbMemListManagement_Typ : 	STRUCT 
		pList : UDINT; (*Adresse des Array-Anfangs*)
		nEntryLength : UDINT; (*Gr��e eines Eintrags*)
		nIndexMax : UINT; (*Maximaler Index des Arrays*)
		nEntryCount : UINT; (*Momentane Anzahl der g�ltigen Eintrage*)
	END_STRUCT;
END_TYPE

(*Additional*)

TYPE
	BrbError_ENUM : 
		(
		eBRB_ERR_OK := 0,
		eBRB_ERR_NULL_POINTER := 50000,
		eBRB_ERR_INVALID_PARAMETER := 50001,
		eBRB_ERR_WRONG_VERSION := 50700,
		eBRB_ERR_TOO_LESS_FIGURES := 50701,
		eBRB_ERR_TOO_LESS_TRIANGLES := 50702,
		eBRB_ERR_TOO_LESS_TEXTS := 50703,
		eBRB_ERR_BUSY := 65535
		);
	BrbRound_ENUM : 
		(
		eBRB_ROUND_APPROP, (*Rundung gem�ss letzter Stelle(n), Grenze bei 5*)
		eBRB_ROUND_UP, (*Aufrundung*)
		eBRB_ROUND_DOWN (*Abrundung*)
		);
END_TYPE
