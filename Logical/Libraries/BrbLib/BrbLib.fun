(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbLib.fun
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Functions and function blocks of library BrbLib
 ********************************************************************)
(*StepHandling*)

FUNCTION BrbStepHandler : DINT (*Behandelt Schrittketten (Protokollierung, Timeout usw.)*)
	VAR_INPUT
		pStepHandling : BrbStepHandling_TYP; (*Zeiger auf eine lokale Struktur "BrbStepHandling_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbStartStepTimeout : UINT (*Startet den Timeout-Z�hler f�r eine Schrittkette*)
	VAR_INPUT
		pStepHandling : BrbStepHandling_TYP; (*Zeiger auf eine lokale Struktur "BrbStepHandling_TYP"*)
		nTimeout : UDINT; (*Zeitangabe in [ms]*)
		nContinueStep : DINT; (*Schrittnummer bei abgelaufenem Timeout*)
	END_VAR
END_FUNCTION

FUNCTION BrbStopStepTimeout : UINT (*Stoppt den Timeout-Z�hler f�r eine Schrittkette*)
	VAR_INPUT
		pStepHandling : BrbStepHandling_TYP; (*Zeiger auf eine lokale Struktur "BrbStepHandling_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbStartStopWatch : BOOL (*Startet eine Stoppuhr*)
	VAR_INPUT
		pStopWatch : BrbStopWatch_TYP; (*Zeiger auf eine lokale Struktur "BrbStopWatch_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbStopStopWatch : UDINT (*Stoppt eine Stoppuhr*)
	VAR_INPUT
		pStopWatch : BrbStopWatch_TYP; (*Zeiger auf eine lokale Struktur "BrbStopWatch_TYP"*)
	END_VAR
END_FUNCTION
(*TaskCommunication*)

FUNCTION BrbSetCaller : BrbCallerStates_ENUM (*Versucht, die Reservierung f�r ein Task-Kommando zu erhalten*)
	VAR_INPUT
		pCaller : BrbCaller_TYP; (*Zeiger auf eine Struktur "BrbCaller_TYP"*)
		nCallerId : DINT; (*Identifier f�r den aufrufenden Task*)
	END_VAR
END_FUNCTION

FUNCTION BrbClearDirectBox : UINT (*Setzt eine DirectBox komplett auf 0*)
	VAR_INPUT
		pDirectBox : UDINT; (*Zeiger auf eine DirectBox-Struktur*)
		nSize : UDINT; (*Gr��e der Struktur*)
	END_VAR
END_FUNCTION

FUNCTION BrbClearCallerBox : UINT (*Setzt eine CallerBox komplett auf 0*)
	VAR_INPUT
		pCallerBox : UDINT; (*Zeiger auf eine CallerBox-Struktur*)
		nSize : UDINT; (*Gr��e der Struktur*)
	END_VAR
END_FUNCTION
(*VarHandling*)

FUNCTION_BLOCK BrbSaveVarAscii (*Speichert eine Variable mit allen Elementen in einer Ascii-Datei*)
	VAR_INPUT
		pDevice : REFERENCE TO STRING[0]; (*Zeiger auf den Laufwerks-Namen*)
		pFile : REFERENCE TO STRING[0]; (*Zeiger auf den Datei-Namen*)
		pVarName : REFERENCE TO STRING[0]; (*Zeiger auf den Variablen-Namen*)
		nLinesToWriteAtOneStep : UINT; (*Anzahl der Zeilen, die in einem Aufruf geschrieben werden*)
	END_VAR
	VAR_OUTPUT
		nCharCountMaxPerWrite : UDINT; (*Gr��te Anzahl der Zeichen, welche in einer Zeile geschrieben wurden*)
		nStatus : UINT; (*Fub-Status*)
	END_VAR
	VAR
		eStep : DINT; (*Step-Nummer f�r die interne Schrittkette*)
		nStatusIntern : UINT; (*Interner Fub-Status*)
		fbFileDelete : FileDelete; (*Fub zum L�schen der Datei*)
		fbFileCreate : FileCreate; (*Fub zum Erzeugen der Datei*)
		sHelp : STRING[nBRB_PVNAME_CHAR_MAX]; (*Hilfsstring*)
		nOffset : UDINT; (*Schreib-Offset*)
		nLineCountWrite : UDINT; (*Zeilen-Z�hler*)
		bVarFinished : BOOL; (*1= Variable komplett durchgetickert*)
		PvInfo : ARRAY[0..16] OF BrbPvInfo_TYP; (*Struktur f�r Variablen-Daten*)
		nPvLevel : UINT; (*Hierarchie-Level f�r Strukturen*)
		sLines : STRING[nBRB_FILE_LINES_CHAR_MAX]; (*Text-Zeilen*)
		fbFileWrite : FileWrite; (*Fub zum Schreiben der Datei*)
		fbFileClose : FileClose; (*Fub zum Schliessen der Datei*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BrbLoadVarAscii (*L�dt eine Ascii-Datei mit Variablen-Werten*)
	VAR_INPUT
		pDevice : REFERENCE TO STRING[0]; (*Zeiger auf den Laufwerks-Namen*)
		pFile : REFERENCE TO STRING[0]; (*Zeiger auf den Datei-Namen*)
		nLinesToReadAtOneStep : UINT; (*Anzahl der Zeilen, die in einem Aufruf gelesen werden*)
	END_VAR
	VAR_OUTPUT
		nStatus : UINT; (*Fub-Status*)
	END_VAR
	VAR
		eStep : DINT; (*Step-Nummer f�r die interne Schrittkette*)
		nStatusIntern : UINT; (*Interner Fub-Status*)
		fbDatObjInfo : DatObjInfo; (*Fub zum Holen der Datenobjekt-Info*)
		fbFileOpen : FileOpen; (*Fub zum �ffnen der Datei*)
		fbDatObjCreate : DatObjCreate; (*Fub zum Erzeugen des Datenobjekts*)
		pTextStart : REFERENCE TO STRING[0]; (*Zeiger auf den Anfang des eingelesenen Texts*)
		pTextEnd : REFERENCE TO STRING[0]; (*Zeiger auf das Ende des eingelesenen Texts*)
		pText : REFERENCE TO STRING[0]; (*Zeiger auf den aktuellen Text*)
		nLineCountRead : UDINT; (*Anzahl der eingelesenen Zeilen*)
		fbFileRead : FileRead; (*Fub zum Lesen der Datei*)
		fbFileClose : FileClose; (*Fub zum Schliessen der Datei*)
		fbDatObjDelete : DatObjDelete; (*Fub zum L�schen des Datenobjekts*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BrbSaveVarBin (*Speichert eine Variable mit allen Elementen in einer Bin�r-Datei*)
	VAR_INPUT
		pDevice : REFERENCE TO STRING[0]; (*Zeiger auf den Laufwerks-Namen*)
		pFile : REFERENCE TO STRING[0]; (*Zeiger auf den Datei-Namen*)
		pVarName : REFERENCE TO STRING[0]; (*Zeiger auf den Variablen-Namen*)
	END_VAR
	VAR_OUTPUT
		nStatus : UINT; (*Fub-Status*)
	END_VAR
	VAR
		eStep : DINT; (*Step-Nummer f�r die interne Schrittkette*)
		nStatusIntern : UINT; (*Interner Fub-Status*)
		PvInfo : BrbPvInfo_TYP; (*Struktur f�r Variablen-Daten*)
		fbFileDelete : FileDelete; (*Fub zum L�schen der Datei*)
		fbFileCreate : FileCreate; (*Fub zum Erzeugen der Datei*)
		fbFileWrite : FileWrite; (*Fub zum Schreiben der Datei*)
		fbFileClose : FileClose; (*Fub zum Schliessen der Datei*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BrbLoadVarBin (*L�dt eine Bin�r-Datei mit Variablen-Werten*)
	VAR_INPUT
		pDevice : REFERENCE TO STRING[0]; (*Zeiger auf den Laufwerks-Namen*)
		pFile : REFERENCE TO STRING[0]; (*Zeiger auf den Datei-Namen*)
		pVarName : REFERENCE TO STRING[0]; (*Zeiger auf den Variablen-Namen*)
		bAllowBiggerVar : BOOL; (*1=Der Speicher der Variablen darf gr��er als die Datei sein*)
	END_VAR
	VAR_OUTPUT
		nStatus : UINT; (*Fub-Status*)
	END_VAR
	VAR
		eStep : DINT; (*Step-Nummer f�r die interne Schrittkette*)
		nStatusIntern : UINT; (*Interner Fub-Status*)
		fbFileOpen : FileOpen; (*Fub zum �ffnen der Datei*)
		PvInfo : BrbPvInfo_TYP; (*Struktur f�r Variablen-Daten*)
		fbFileRead : FileRead; (*Fub zum Lesen der Datei*)
		fbFileClose : FileClose; (*Fub zum Schliessen der Datei*)
	END_VAR
END_FUNCTION_BLOCK
(*FileHandling*)

FUNCTION_BLOCK BrbCheckUsbSticks (*Pr�ft, ob ein Usb-Stick gesteckt wurde*)
	VAR_INPUT
		bAutoLink : BOOL; (*1=Usb-Device wird beim Stecken automatisch gelinkt*)
	END_VAR
	VAR_OUTPUT
		nUsbDeviceCount : UINT; (*Anzahl der gesteckten Usb-Sticks*)
		bUsbDeviceCountChanged : BOOL; (*Anzahl hat sich ge�ndert (nur 1 f�r einen Zyklus)*)
		UsbDeviceList : ARRAY[0..nBRB_USB_DEVICE_LIST_INDEX_MAX] OF BrbUsbDeviceListEntry_TYP; (*Liste der Usb-Devices*)
		nStatus : UINT; (*Fub-Status*)
	END_VAR
	VAR
		eStep : DINT; (*Step-Nummer f�r die interne Schrittkette*)
		DeviceListMan : BrbMemListManagement_Typ; (*Verwaltung der Device-Liste*)
		nUsbDeviceIndex : UINT; (*Aktueller Index in der Device-Liste*)
		UsbDeviceListEntry : BrbUsbDeviceListEntry_TYP; (*Aktueller Eintrag in der Device-Liste*)
		fbUsbNodeListGet : UsbNodeListGet; (*Fub zum Lesen der Node-Liste*)
		nUsbNodeIndex : UINT; (*Aktueller Index in der Node-Liste*)
		UsbNodeList : ARRAY[0..nBRB_USB_DEVICE_LIST_INDEX_MAX] OF UDINT; (*Node-Liste*)
		nAttachDetachCountOld : UDINT; (*Vergleichs-Z�hler*)
		fbDevUnlink : DevUnlink; (*Fub zum Unlinken*)
		fbUsbNodeGet : UsbNodeGet; (*Fub zum Lesen der Node-Info*)
		UsbNodeInfo : usbNode_typ; (*Node-Info*)
		sLinkParameter : STRING[100]; (*Parameter zum Linken*)
		fbDevLink : DevLink; (*Fub zum Linken*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BrbReadDir (*Gibt eine Liste der Dateien und/oder der Verzeichnisse eines Laufwerks zur�ck*)
	VAR_INPUT
		pDevice : REFERENCE TO STRING[0]; (*Zeiger auf den Laufwerks-Namen*)
		pPath : REFERENCE TO STRING[0]; (*Zeiger auf den Pfad*)
		eFilter : BrbDirInfoFilter_ENUM; (*Datei- und Verzeichnis-Filter*)
		bWithParentDir : BOOL; (*1=Mit �bergeordnetem Verzeichnis*)
		pFileFilter : REFERENCE TO STRING[0]; (*Zeiger auf den Filtertext f�r Datei-Endungen*)
		eSorting : BrbFileSorting_ENUM; (*Sortierung*)
		pList : UDINT; (*Zeiger auf das zu f�llende Array*)
		nListIndexMax : UDINT; (*Maximaler Anzahl der Eintr�ge im Array*)
	END_VAR
	VAR_OUTPUT
		nDirCount : UDINT; (*Anzahl der Verzeichnisse in der Liste*)
		nFileCount : UDINT; (*Anzahl der Dateien in der Liste*)
		nTotalCount : UDINT; (*Anzahl der Verzeichnisse und Dateien in der Liste*)
		nStatus : UINT; (*Fub-Status*)
	END_VAR
	VAR
		eStep : DINT; (*Step-Nummer f�r die interne Schrittkette*)
		FilterListMan : BrbMemListManagement_Typ; (*Verwaltung der Filter-Liste*)
		FilterList : ARRAY[0..nBRB_FILTER_SINGLE_INDEX_MAX] OF STRING[nBRB_FILTER_SINGLE_CHAR_MAX]; (*Liste der Filter*)
		fbDirRead : DirRead; (*Fub zum Auslesen des Laufwerks*)
		FileInfo : fiDIR_READ_DATA; (*Daten der aktueller Datei*)
		ReadListEntry : BrbReadDirListEntry_TYP; (*Daten der aktueller Datei*)
		FileListMan : BrbMemListManagement_Typ; (*Verwaltung der Datei-Liste*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BrbDeleteFiles (*L�scht Dateien, die �ber Filter festgelegt sind*)
	VAR_INPUT
		pDevice : REFERENCE TO STRING[0]; (*Zeiger auf den Laufwerks-Namen*)
		pPath : REFERENCE TO STRING[0]; (*Zeiger auf den Pfad*)
		pFileFilter : REFERENCE TO STRING[0]; (*Zeiger auf den Filtertext f�r Datei-Endungen*)
		dtDateStart : DATE_AND_TIME; (*Angabe des Start-Datums*)
		dtDateEnd : DATE_AND_TIME; (*Angabe des Ende-Datums*)
	END_VAR
	VAR_OUTPUT
		nDeletedFileCount : UDINT; (*Anzahl der gel�schten Dateien*)
		nKeptFileCount : UDINT; (*Anzahl der nicht gel�schten Dateien*)
		nStatus : UINT; (*Fub-Status*)
	END_VAR
	VAR
		eStep : DINT; (*Step-Nummer f�r die interne Schrittkette*)
		FilterListMan : BrbMemListManagement_Typ; (*Verwaltung der Filter-Liste*)
		FilterList : ARRAY[0..nBRB_FILTER_SINGLE_INDEX_MAX] OF STRING[nBRB_FILTER_SINGLE_CHAR_MAX]; (*Liste der Filter*)
		fbDirRead : DirRead; (*Fub zum Auslesen des Laufwerks*)
		FileInfo : fiDIR_READ_DATA; (*Daten der aktueller Datei*)
		sFileWithPath : STRING[nBRB_FILE_NAME_CHAR_MAX];
		fbFileDelete : FileDelete; (*Fub zum Auslesen des Laufwerks*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BrbLoadFileDataObj (*L�dt eine Datei in ein Datenobjekt*)
	VAR_INPUT
		pDevice : REFERENCE TO STRING[0]; (*Zeiger auf den Laufwerks-Namen*)
		pFile : REFERENCE TO STRING[0]; (*Zeiger auf den Datei-Namen*)
		pDataObjName : REFERENCE TO STRING[0]; (*Zeiger auf den Datenobjekt-Namen*)
		nDataObjMemType : USINT; (*Angabe �ber den Speicher-Typ des Datenobjekts*)
		nDataObjOption : UDINT; (*Angabe �ber die Option des Datenobjekts*)
	END_VAR
	VAR_OUTPUT
		nStatus : UINT; (*Fub-Status*)
		nDataObjIdent : UDINT; (*Ident des Datenobjekts*)
		pDataObjMem : UDINT; (*Zeiger auf die Daten des Datenobjekts*)
		nDataObjLen : UDINT; (*L�nge des Datenobjekts (= L�nge der Datei)*)
	END_VAR
	VAR
		eStep : DINT; (*Step-Nummer f�r die interne Schrittkette*)
		nStatusIntern : UINT; (*Interner Fub-Status*)
		fbDatObjInfo : DatObjInfo; (*Interner Fub*)
		fbDatObjDelete : DatObjDelete; (*Interner Fub*)
		fbFileOpen : FileOpen; (*Interner Fub*)
		fbDatObjCreate : DatObjCreate; (*Interner Fub*)
		fbFileRead : FileRead; (*Interner Fub*)
		fbFileClose : FileClose; (*Interner Fub*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK BrbSaveFileDataObj (*Speichert ein Datenobjekt in eine Datei*)
	VAR_INPUT
		pDevice : REFERENCE TO STRING[0]; (*Zeiger auf den Laufwerks-Namen*)
		pFile : REFERENCE TO STRING[0]; (*Zeiger auf den Datei-Namen*)
		pDataObjName : REFERENCE TO STRING[0]; (*Zeiger auf den Datenobjekt-Namen*)
	END_VAR
	VAR_OUTPUT
		nStatus : UINT; (*Fub-Status*)
	END_VAR
	VAR
		eStep : DINT; (*Step-Nummer f�r die interne Schrittkette*)
		nStatusIntern : UINT; (*Interner Fub-Status*)
		fbDatObjInfo : DatObjInfo; (*Interner Fub*)
		fbFileInfo : FileInfo; (*Interner Fub*)
		FileInfo : fiFILE_INFO; (*Interne Daten*)
		fbFileDelete : FileDelete; (*Interner Fub*)
		fbFileCreate : FileCreate; (*Interner Fub*)
		fbFileWrite : FileWrite; (*Interner Fub*)
		fbFileClose : FileClose; (*Interner Fub*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION BrbCheckFileName : UINT (*Ersetzt Sonderzeichen in einem Datei-Namen mit Unterstrichen*)
	VAR_INPUT
		pFileName : STRING[0]; (*Zeiger auf den Datei-Namen*)
	END_VAR
END_FUNCTION

FUNCTION BrbCheckFileEnding : BOOL (*Pr�ft die Endung eines Datei-Namens und berichtigt sie gegebenenfalls*)
	VAR_INPUT
		pFileName : STRING[0]; (*Zeiger auf den Datei-Namen*)
		pEnding : STRING[0]; (*Zeiger auf die Datei-Endung (mit oder ohne Punkt)*)
	END_VAR
END_FUNCTION

FUNCTION BrbCombinePath : UINT (*F�gt zwei Pfade oder einen Pfad und eine Datei-Namen zusammen*)
	VAR_INPUT
		pPath : STRING[0]; (*Zeiger auf Pfad*)
		pFilename : STRING[0]; (*Zeiger auf Pfad oder Datei-Namen*)
		pFilenameWithPath : STRING[0]; (*Zeiger auf den String, der das Ergebnis aufnimmt*)
	END_VAR
END_FUNCTION
(*VisVc4*)

FUNCTION BrbVc4HandleGeneral : BOOL (*Logik f�r allgemeine Vis-Funktionalit�ten*)
	VAR_INPUT
		pVisGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleChangePage : UINT (*Behandelt die Seitenumschaltung*)
	VAR_INPUT
		pPageHandling : BrbVc4PageHandling_TYP; (*Zeiger auf die Seiten-Verwaltung*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4ChangePage : BOOL (*Schaltet die Visu-Seite um*)
	VAR_INPUT
		pPageHandling : BrbVc4PageHandling_TYP; (*Zeiger auf die Seiten-Verwaltung*)
		nIndex : DINT; (*Index der Seite*)
		bStoreLastPage : BOOL; (*1=Vor der Umschaltung wird die aktuelle Seite im Previous-Speicher gemerkt*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4ChangePageBack : BOOL (*Schaltet auf die zuletzt gemerkte Seite zur�ck*)
	VAR_INPUT
		pPageHandling : BrbVc4PageHandling_TYP; (*Zeiger auf die Seiten-Verwaltung*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleScreenSaver : BOOL (*Behandelt den Bildschirmschoner*) (*$GROUP=User*)
	VAR_INPUT
		pScreenSaver : BrbVc4ScreenSaver_TYP;
		pGeneral : BrbVc4General_TYP;
		pPageHandling : BrbVc4PageHandling_TYP;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4SetControlEnability : BOOL (*Setzt den Status eines Vis-Controls auf Enabled/Disabled*)
	VAR_INPUT
		pStatus : REFERENCE TO UINT; (*Zeiger auf den Status*)
		bEnable : BOOL; (*1=Enable*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4IsControlEnabled : BOOL (*Gibt zur�ck, ob ein Vis-Control enabled/disabled ist*)
	VAR_INPUT
		nStatus : UINT; (*Status-Wert*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4SetControlVisibility : BOOL (*Setzt ein Vis-Control auf sichtbar/unsichtbar*)
	VAR_INPUT
		pStatus : REFERENCE TO UINT; (*Zeiger auf den Status*)
		bVisible : BOOL; (*1=Sichtbar*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4IsControlVisible : BOOL (*Gibt zur�ck, ob ein Vis-Control sichtbar/unsichtbar ist*)
	VAR_INPUT
		nStatus : UINT; (*Status-Wert*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4SetControlFocus : BOOL (*Setzt oder l�scht den Focus auf ein Vis-Control*)
	VAR_INPUT
		pStatus : REFERENCE TO UINT; (*Zeiger auf den Status*)
		bFocus : BOOL; (*1=Focus setzen*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HasControlFocus : BOOL (*Gibt zur�ck, ob ein Vis-Control den Focus hat*)
	VAR_INPUT
		nStatus : UINT; (*Status-Wert*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4IsControlInputActive : BOOL (*Gibt zur�ck, ob bei einem Vis-Control die Eingabe aktiv ist*)
	VAR_INPUT
		nStatus : UINT;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4OpenTouchpad : BOOL (*�ffnet das Touchpad an einem Eingabe-Control*)
	VAR_INPUT
		pStatus : REFERENCE TO UINT; (*Zeiger auf den Status*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4CloseTouchpad : BOOL (*Schliesst das Touchpad an einem Eingabe-Control*)
	VAR_INPUT
		pStatus : REFERENCE TO UINT; (*Zeiger auf den Status*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4IsTouchpadOpen : BOOL (*Gibt zur�ck, ob das Touchpad an einem Control ge�ffnet ist*)
	VAR_INPUT
		nStatus : UINT; (*Status-Wert*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4SetControlColor : UINT (*Setzt eine von zwei Farben eines Controls aufgrund einer Bedingung*)
	VAR_INPUT
		pColorDatapoint : REFERENCE TO UINT; (*Zeiger auf den Farb-Datenpunkt*)
		bCondition : BOOL; (*Bool'sche Bedingung*)
		nColorTrue : UINT; (*Farbe f�r Bedingung = TRUE*)
		nColorFalse : UINT; (*Farbe f�r Bedingung = FALSE*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleAnimation : UINT (*Logik f�r eine Bitmap-Animation*)
	VAR_INPUT
		pAnimation : BrbVc4Animation_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleButton : BOOL (*Logik f�r einen normalen Button*)
	VAR_INPUT
		pButton : BrbVc4Button_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleCheckbox : BOOL (*Logik f�r eine Checkbox*)
	VAR_INPUT
		pCheckbox : BrbVc4Checkbox_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleDropdown : BOOL (*Logik f�r ein Dropdown*)
	VAR_INPUT
		pDropdown : BrbVc4Dropdown_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleHwPosSwitch2 : BrbVc4HwPosSwitchPositions_ENUM (*Logik f�r einen Hardware-2-Positions-Schalter*)
	VAR_INPUT
		pPosSwitch : BrbVc4HwPosSwitch2_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleHwSafetyButton : BOOL (*Logik f�r einen Hardware-Freigabe-Taster*)
	VAR_INPUT
		pSafetyButton : BrbVc4HwSafetyButton_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleIncButton : BOOL (*Logik f�r einen Inkrement-Button*)
	VAR_INPUT
		pButton : BrbVc4IncButton_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleJogButton : BOOL (*Logik f�r einen  Jog-Button*)
	VAR_INPUT
		pButton : BrbVc4JogButton_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleOptionbox : BOOL (*Logik f�r eine Optionbox*)
	VAR_INPUT
		pOptionbox : BrbVc4Optionbox_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleScrollbarHorizontal : BOOL (*Logik f�r eine horizontale Scrollbar*)
	VAR_INPUT
		pScrollbar : BrbVc4ScrollbarHor_TYP; (*Zeiger auf Struktur-Instanz*)
		pScrollOffset : REFERENCE TO DINT; (*Zeiger auf den Scroll-Offset der verwalteten Liste*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleScrollbarVertical : BOOL (*Logik f�r eine vertikale Scrollbar*)
	VAR_INPUT
		pScrollbar : BrbVc4ScrollbarVer_TYP; (*Zeiger auf Struktur-Instanz*)
		pScrollOffset : REFERENCE TO DINT; (*Zeiger auf den Scroll-Offset der verwalteten Liste*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleTabCtrl : BOOL (*Logik f�r eine TabCtrl*)
	VAR_INPUT
		pTabCtrl : BrbVc4TabCtrl_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleToggleButton : BOOL (*Logik f�r einen Toggle-Button*)
	VAR_INPUT
		pButton : BrbVc4ToggleButton_TYP; (*Zeiger auf Struktur-Instanz*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleToggleButtonExt : BOOL (*Logik f�r einen erweiterten ToggleButton*)
	VAR_INPUT
		pButton : BrbVc4ToggleButtonExt_TYP;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4HandleTouchGrid : UINT (*Logik f�r ein Touchgrid*)
	VAR_INPUT
		pTouchgrid : BrbVc4Touchgrid_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION
(*VisVc4Draw*)

FUNCTION BrbVc4DrawLine : UINT (*Zeichnet eine Linie*)
	VAR_INPUT
		pLine : BrbVc4Line_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawLineCorr : UINT (*Zeichnet eine korrigierte Linie*)
	VAR_INPUT
		pLine : BrbVc4Line_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawLineClip : UINT (*Zeichnet eine Linie in einen Ausschnitt*)
	VAR_INPUT
		pLine : BrbVc4Line_TYP; (*Zeiger auf Struktur-Instanz*)
		pClip : BrbVc4Rectangle_TYP; (*Zeiger auf das Ausschnitts-Rechteck*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawRectangle : UINT (*Zeichnet ein Rechteck*)
	VAR_INPUT
		pRectangle : BrbVc4Rectangle_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawRectangleCorr : UINT (*Zeichnet ein korrigiertes Rechteck*)
	VAR_INPUT
		pRectangle : BrbVc4Rectangle_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawRectangleClip : UINT (*Zeichnet ein Rechteck in einen Ausschnitt*)
	VAR_INPUT
		pRectangle : BrbVc4Rectangle_TYP; (*Zeiger auf Struktur-Instanz*)
		pClip : BrbVc4Rectangle_TYP; (*Zeiger auf das Ausschnitts-Rechteck*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawEllipse : UINT (*Zeichnet eine Ellipse*)
	VAR_INPUT
		pEllipse : BrbVc4Ellipse_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawArc : UINT (*Zeichnet einen Bogen*)
	VAR_INPUT
		pArc : BrbVc4Arc_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawText : UINT (*Zeichnet einen Text*)
	VAR_INPUT
		pText : BrbVc4DrawText_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawTrend : UINT (*Zeichnet einen Trend*)
	VAR_INPUT
		pTrend : BrbVc4DrawTrend_TYP;
		pGeneral : BrbVc4General_TYP;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4GetTrendDisplayCoordinateY : DINT (*Gibt die Y-Pixel-Koordinate eines Trend-Wertes zur�ck*)
	VAR_INPUT
		pTrend : BrbVc4DrawTrend_TYP; (*Zeiger auf die Trend-Daten*)
		eScaleY : BrbVc4TrendScaleYIndex_ENUM; (*Index der Skala*)
		rValue : REAL; (*Trend-Wert*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4GetTrendDisplayCoordinateX : DINT (*Gibt die X-Pixel-Koordinate eines Trend-Wertes zur�ck*)
	VAR_INPUT
		pTrend : BrbVc4DrawTrend_TYP;
		nSampleIndex : DINT;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4TrendCallbackAfterClear : UINT (*Muster f�r Callback-Funktion AfterClear*)
	VAR_INPUT
		pTrend : BrbVc4DrawTrend_TYP;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4TrendCallbackAfterCrveArea : UINT (*Muster f�r Callback-Funktion AfterCurveArea*)
	VAR_INPUT
		pTrend : BrbVc4DrawTrend_TYP;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4TrendCallbackAfterScaleY : UINT (*Muster f�r Callback-Funktion AfterScaleY*)
	VAR_INPUT
		pTrend : BrbVc4DrawTrend_TYP;
		eScaleYIndex : BrbVc4TrendScaleYIndex_ENUM;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4TrendCallbackAfterScaleX : UINT (*Muster f�r Callback-Funktion AfterScaleX*)
	VAR_INPUT
		pTrend : BrbVc4DrawTrend_TYP;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4TrendCallbackAfterCurve : UINT (*Muster f�r Callback-Funktion AfterCurve*)
	VAR_INPUT
		pTrend : BrbVc4DrawTrend_TYP;
		nCurveIndex : UINT;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4TrendCallbackAfterCursor : UINT (*Muster f�r Callback-Funktion AfterCursor*)
	VAR_INPUT
		pTrend : BrbVc4DrawTrend_TYP;
		nCursorIndex : UINT;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4TrendCallbackAfterZoomWind : UINT (*Muster f�r Callback-Funktion AfterZoomWindow*)
	VAR_INPUT
		pTrend : BrbVc4DrawTrend_TYP;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4LinkTrends : UINT (*Linkt die Touch-Bedienung zwischen mehreren Trends*)
	VAR_INPUT
		pLinkTrends : BrbVc4LinkTrends_TYP;
		pGeneral : BrbVc4General_TYP;
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawPlot : UINT (*Zeichnet einen XY-Graphen*)
	VAR_INPUT
		pPlot : BrbVc4DrawPlot_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4GetPlotDisplayCoordinateY : DINT (*Gibt die Y-Pixel-Koordinate eines Plot-Wertes zur�ck*)
	VAR_INPUT
		pPlot : BrbVc4DrawPlot_TYP; (*Zeiger auf die Plot-Daten*)
		rValue : REAL; (*Plot-Wert*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4GetPlotDisplayCoordinateX : DINT (*Gibt die X-Pixel-Koordinate eines Plot-Wertes zur�ck*)
	VAR_INPUT
		pPlot : BrbVc4DrawPlot_TYP; (*Zeiger auf die Plot-Daten*)
		rValue : REAL; (*Plot-Wert*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawAxisLinear : REAL (*Zeichnet eine lineare Achse*)
	VAR_INPUT
		pAxis : BrbVc4DrawAxisLinear_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4DrawAxisRadial : REAL (*Zeichnet eine radiale Achse*)
	VAR_INPUT
		pAxis : BrbVc4DrawAxisRadial_TYP; (*Zeiger auf Struktur-Instanz*)
		pGeneral : BrbVc4General_TYP; (*Zeiger auf die Instanz vom Typ "BrbVc4General_TYP"*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4CorrectLine : UINT (*Korrigiert eine Linie mit negativen Koordinaten*)
	VAR_INPUT
		pLine : BrbVc4Line_TYP; (*Zeiger auf die Linie*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4ClipLine : UINT (*Korrigiert die Koordinaten einer Linie, um in einen Ausschnitt zu passen*)
	VAR_INPUT
		pLine : BrbVc4Line_TYP; (*Zeiger auf die Linie*)
		pClip : BrbVc4Rectangle_TYP; (*Zeiger auf das Ausschnitts-Rechteck*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4CorrectRectangle : UINT (*Korrigiert ein Rechteck mit negativen Koordinaten*)
	VAR_INPUT
		pRectangle : BrbVc4Rectangle_TYP; (*Zeiger auf das Rechteck*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4ClipRectangle : UINT (*Korrigiert die Koordinaten eines Rechtecks, um in einen Ausschnitt zu passen*)
	VAR_INPUT
		pRectangle : BrbVc4Rectangle_TYP; (*Zeiger auf das Rechteck*)
		pClip : BrbVc4Rectangle_TYP; (*Zeiger auf das Ausschnitts-Rechteck*)
	END_VAR
END_FUNCTION

FUNCTION BrbVc4IsPointWithinRectangle : BOOL (*Gibt zur�ck, ob ein Punkt innerhalb eines Rechtecks liegt*)
	VAR_INPUT
		nPointX : DINT; (*X-Koordinate des Punkts*)
		nPointY : DINT; (*Y-Koordinate des Punkts*)
		pRectangle : BrbVc4Rectangle_TYP; (*Zeiger auf das Rechteck*)
	END_VAR
END_FUNCTION
(*TimeAndDate*)

FUNCTION BrbGetTimeText : UINT (*Gibt die angegebene RTC-Zeit als Text zur�ck*)
	VAR_INPUT
		pTime : RTCtime_typ; (*Zeiger auf eine Zeit-Struktur*)
		pText : STRING[0]; (*Zeiger auf den String*)
		nTextSize : UDINT; (*Gr��e der String-Variablen*)
		pFormat : STRING[0]; (*Zeiger auf den Formatierungstext*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetCurrentTimeText : UINT (*Gibt die aktuelle Zeit als Text zur�ck*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den String*)
		nTextSize : UDINT; (*Gr��e der String-Variablen*)
		pFormat : STRING[0]; (*Zeiger auf den Formatierungstext*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetTimeTextDtStruct : UINT (*Gibt die angegebene DtStruct-Zeit als Text zur�ck*)
	VAR_INPUT
		pTime : DTStructure; (*Zeiger auf eine Zeit-Struktur*)
		pText : STRING[0]; (*Zeiger auf den String*)
		nTextSize : UDINT; (*Gr��e der String-Variablen*)
		pFormat : STRING[0]; (*Zeiger auf den Formatierungstext*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetTimeTextDt : UINT (*Gibt die angegebene DT-Zeit als Text zur�ck*)
	VAR_INPUT
		dtTime : DATE_AND_TIME; (*Zeiger auf eine DT-Zeit*)
		pText : STRING[0]; (*Zeiger auf den String*)
		nTextSize : UDINT; (*Gr��e der String-Variablen*)
		pFormat : STRING[0]; (*Zeiger auf den Formatierungstext*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetDtFromTimeText : DATE_AND_TIME (*Wandelt einen Zeit-Text in eine Zeit um*)
	VAR_INPUT
		pTimeText : STRING[0]; (*Zeiger auf den Zeit-Text im Format*)
		pFormat : STRING[0]; (*Zeiger auf den Formatierungstext*)
	END_VAR
END_FUNCTION

FUNCTION BrbRtcTimeToDtStruct : UINT (*Wandelt eine RTC-Zeit in eine DtStruct-Zeit*)
	VAR_INPUT
		pRtcTime : RTCtime_typ; (*Zeiger auf eine Struktur "RTCtime_typ"*)
		pDtStruct : DTStructure; (*Zeiger auf eine Struktur "DTStructure"*)
	END_VAR
END_FUNCTION

FUNCTION BrbDtStructCompare : BOOL (*Vergleicht zwei DtStructures*)
	VAR_INPUT
		pDtStruct1 : DTStructure; (*Zeiger auf eine Struktur "DTStructure"*)
		eCompare : BrbTimeAndDateCompare_ENUM;
		pDtStruct2 : DTStructure; (*Zeiger auf eine Struktur "DTStructure"*)
	END_VAR
END_FUNCTION

FUNCTION BrbDtStructAddSeconds : DATE_AND_TIME (*Addiert zu einer DTStructure die angegebenen Sekunden*)
	VAR_INPUT
		pDtStruct : DTStructure; (*Zeiger auf eine Struktur "DTStructure"*)
		nSeconds : DINT; (*Zu addierende Sekunden*)
	END_VAR
END_FUNCTION

FUNCTION BrbDtStructAddMilliseconds : DATE_AND_TIME (*Addiert zu einer DTStructure die angegebenen Millisekunden*)
	VAR_INPUT
		pDtStruct : DTStructure;
		nMilliseconds : DINT;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK BrbTimerSwitch (*Zeitschaltuhr f�r einen Kanal*)
	VAR_INPUT
		bEnable : BOOL; (*1=Baustein wird ausgef�hrt*)
		bCmdSwitchOff : BOOL; (*1=Ausgangs-Kanal wird ausgeschaltet*)
		bCmdSwitchOn : BOOL; (*1=Ausgangs-Kanal wird eingeschaltet*)
		bCmdToggle : BOOL; (*1=Ausgangs-Kanal wird umgeschaltet*)
		pUserTime : REFERENCE TO DTStructure; (*Optionaler Zeiger auf Benutzer-Zeitstempel*)
		Parameter : BrbTimerSwitchPar_TYP; (*Parameter-Struktur*)
	END_VAR
	VAR_OUTPUT
		bOut : BOOL; (*Ausgangs-Kanal*)
		dtsUsedTime : DTStructure; (*Verwendete Zeit*)
		nSwitchCount : UDINT; (*Anzahl der Schaltungen*)
	END_VAR
	VAR
		fbDTStructureGetTime : DTStructureGetTime;
		bLocked : BOOL;
		fbLock : TON;
		bClearOutOnNextCycle : BOOL;
	END_VAR
END_FUNCTION_BLOCK
(*Strings*)

FUNCTION BrbUdintToAscii : UINT (*Wandelt einen UDINT-Wert zu einem String*)
	VAR_INPUT
		nValue : UDINT; (*UDINT-Wert*)
		pText : STRING[0]; (*Zeiger auf den String*)
	END_VAR
END_FUNCTION

FUNCTION BrbAsciiToUdint : UDINT (*Wandelt einen String zu einem UDINT-Wert*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den String*)
	END_VAR
END_FUNCTION

FUNCTION BrbUdintToHex : UINT (*Wandelt einen Udint in einen Hex-String*)
	VAR_INPUT
		nValue : UDINT; (*UDINT-Wert*)
		pHex : STRING[0]; (*Zeiger auf den String*)
		nHexSize : UDINT; (*Gr��e der String-Variablen*)
		bWithPraefix : BOOL; (*1=Mit Pr�fix '0x'*)
	END_VAR
END_FUNCTION

FUNCTION BrbHexToUdint : UDINT (*Wandelt einen Hex-String in einen Udint*)
	VAR_INPUT
		pHex : STRING[0]; (*Zeiger auf den String*)
	END_VAR
END_FUNCTION

FUNCTION BrbAsciiFieldToString : BOOL (*Wandelt ein Ascii-Feld in einen lesbaren Text*)
	VAR_INPUT
		pAsciiField : REFERENCE TO USINT; (*Zeiger auf das Ascii-Feld*)
		nAsciiFieldLen : UDINT; (*L�nge des Ascii-Felds*)
		nFinalAsciiCharCount : UDINT; (*Anzahl der letzten Ascii-Zeichen, die auf jeden Fall enthalten sein sollten*)
		pText : STRING[0]; (*Zeiger auf den String*)
		nTextSize : UDINT; (*Gr��e der String-Variablen*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringGetIndexOf : DINT (*Gibt den ersten Index eines zu suchenden Textes zur�ck*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		pFind : STRING[0]; (*Zeiger auf den zu suchenden Text*)
		nTextLen : UDINT; (*L�nge des ganzen Textes*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringGetLastIndexOf : DINT (*Gibt den letzten Index eines zu suchenden Textes zur�ck*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		pFind : STRING[0]; (*Zeiger auf den zu suchenden Text*)
		nTextLen : UDINT; (*L�nge des ganzen Textes*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringGetAdrOf : STRING[0] (*Gibt die erste Adresse eines zu suchenden Textes zur�ck*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		pFind : STRING[0]; (*Zeiger auf den zu suchenden Text*)
		nTextLen : UDINT; (*L�nge des ganzen Textes*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringGetLastAdrOf : STRING[0] (*Gibt die letzte Adresse eines zu suchenden Textes zur�ck*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		pFind : STRING[0]; (*Zeiger auf den zu suchenden Text*)
		nTextLen : UDINT; (*L�nge des ganzen Textes*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringStartsWith : BOOL (*Gibt zur�ck, ob ein Text mit einer bestimmten Zeichenfolge beginnt*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		pFind : STRING[0]; (*Zeiger auf den zu suchenden Text*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringEndsWith : BOOL (*Gibt zur�ck, ob ein Text mit einer bestimmten Zeichenfolge endet*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		pFind : STRING[0]; (*Zeiger auf den zu suchenden Text*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringGetSubText : STRING[0] (*Gibt eine Zeichenfolge innerhalb eines Textes zur�ck*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		nIndex : UDINT; (*Start-Index*)
		nLen : UDINT; (*L�nge*)
		pSubText : STRING[0]; (*Zeiger auf eine String-Variable, die das Ergebnis aufnimmt*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringGetSubTextByLen : STRING[0] (*Gibt eine Zeichenfolge innerhalb eines Textes zur�ck*)
	VAR_INPUT
		pStart : STRING[0]; (*Zeiger auf den Start der Zeichenfolge*)
		nLen : UDINT; (*L�nge der Zeichenfolge*)
		pSubText : STRING[0]; (*Zeiger auf eine String-Variable, die das Ergebnis aufnimmt*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringGetSubTextByAdr : STRING[0] (*Gibt eine Zeichenfolge innerhalb eines Textes zur�ck*)
	VAR_INPUT
		pStart : STRING[0]; (*Zeiger auf den Start der Zeichenfolge*)
		pEnd : STRING[0]; (*Zeiger auf das Ende der Zeichenfolge*)
		pSubText : STRING[0]; (*Zeiger auf eine String-Variable, die das Ergebnis aufnimmt*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringCut : STRING[0] (*Schneidet eine Zeichenfolge aus einem Text heraus*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		nCutIndex : UDINT; (*Start-Index der Schneidung*)
		nCutLen : UDINT; (*L�nge der Schneidung*)
		pCut : STRING[0]; (*Zeiger auf eine String-Variable, die die herausgeschnittene Zeichenfolge aufnimmt*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringCutFromLastSeparator : UDINT (*Schneidet die Zeichenfolge ab dem letzten Trennzeichen in einem Text ab*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		pSeparator : STRING[0]; (*Zeiger auf den zu suchenden Text*)
		pCut : STRING[0]; (*Zeiger auf eine String-Variable, die die herausgeschnittene Zeichenfolge aufnimmt*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringInsert : STRING[0] (*F�gt eine Zeichenfolge in einen Text ein*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		nInsertIndex : UDINT; (*Start-Index der Einf�gung*)
		pInsert : STRING[0]; (*Zeiger auf die einzuf�gende Zeichenfolge*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringReplace : UDINT (*Ersetzt eine Zeichenfolge in einem Text durch eine andere*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den ganzen Text*)
		pFind : STRING[0]; (*Zeiger auf die zu ersetzende Zeichenfolge*)
		pReplace : STRING[0]; (*Zeiger auf die ersetzende Zeichenfolge*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringPadLeft : BOOL (*F�llt einen String auf der linken Seite mit einem F�llzeichen auf eine bestimme L�nge*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den Text*)
		pFillChar : STRING[0]; (*Zeiger auf das F�llzeichen*)
		nLen : UDINT; (*L�nge, auf die gef�llt werden soll*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringPadRight : BOOL (*F�llt einen String auf der rechten Seite mit einem F�llzeichen auf eine bestimme L�nge*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den Text*)
		pFillChar : STRING[0]; (*Zeiger auf das F�llzeichen*)
		nLen : UDINT; (*L�nge, auf die gef�llt werden soll*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringTrimLeft : BOOL (*Schneidet eine Zeichenfolge auf der linken Seite eines Textes heraus*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den Text*)
		pTrim : STRING[0]; (*Zeiger auf die herauszuschneidene Zeichenfolge*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringTrimRight : BOOL (*Schneidet eine Zeichenfolge auf der rechten Seite eines Textes heraus*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den Text*)
		pTrim : STRING[0]; (*Zeiger auf die herauszuschneidene Zeichenfolge*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringSplit : UDINT (*Spaltet einen Text in mehrere Strings aufgrund eines Trennzeichens*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den Text*)
		pSep : STRING[0]; (*Zeiger auf das Trennzeichen*)
		pSplitArray : UDINT; (*Zeiger auf ein String-Array*)
		nArrayIndexMax : UDINT; (*Maximaler Index des String-Arrays*)
		nEntrySize : UDINT; (*Gr��e eines Strings im String-Array*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringConvertRealFromExp : BOOL (*Konvertiert eine exponentielle Notation in einen Real-Wert*)
	VAR_INPUT
		pValue : STRING[0]; (*Zeiger auf den Wert-Text*)
		pResult : STRING[0]; (*Zeiger auf eine String-Variable, die das Ergebnis aufnimmt*)
		nResultSize : UDINT; (*Gr��e der Ergebnis-Variable*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringConvertRealToExp : BOOL (*Konvertiert einen Real-Wert in eine exponentielle Notation*)
	VAR_INPUT
		pValue : STRING[0]; (*Zeiger auf den Wert-Text*)
		pResult : STRING[0]; (*Zeiger auf eine String-Variable, die das Ergebnis aufnimmt*)
		nResultSize : UDINT; (*Gr��e der Ergebnis-Variable*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringFormatFractionDigits : BOOL (*Formatiert einen Real-String-Wert auf Nachkommastellen*)
	VAR_INPUT
		pValue : STRING[0]; (*Zeiger auf den Wert-Text*)
		nValueSize : UDINT; (*Gr��e der Variable*)
		nFractionsDigits : UINT; (*Anzahl der Nachkomma-Stellen*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringSwap : BOOL (*Dreht die Reihenfolge der Zeichen in einem Text*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den zu drehenden Text*)
		pSwapped : STRING[0]; (*Zeiger auf eine String-Variable, die das Ergebnis aufnimmt*)
		nSwappedSize : UDINT; (*Gr��e der String-Variablen*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringToUpper : BOOL (*Konvertiert einen String zu Gro�buchstaben*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den Text*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringToLower : BOOL (*Konvertiert einen String zu Kleinbuchstaben*)
	VAR_INPUT
		pText : STRING[0]; (*Zeiger auf den Text*)
	END_VAR
END_FUNCTION

FUNCTION BrbStringIsNumerical : BOOL (*Gibt zur�ck, ob ein Text eine Nummer enth�lt*)
	VAR_INPUT
		pText : STRING[0];
	END_VAR
END_FUNCTION
(*Xml*)

FUNCTION BrbXmlGetTagText : UDINT (*Gibt den Text zwischen zwei Xml-Tags zur�ck*)
	VAR_INPUT
		pStartTag : STRING[0]; (*Zeiger auf den Start-Tag*)
		pEndTag : STRING[0]; (*Zeiger auf den End-Tag*)
		pStart : UDINT; (*Zeiger auf den Such-Start*)
		pEnd : UDINT; (*Zeiger auf das Such-Ende*)
		pText : STRING[0]; (*Zeiger auf eine String-Variable, die das Ergebnis aufnimmt*)
		nTextSize : UDINT; (*Gr��e der String-Variablen*)
	END_VAR
END_FUNCTION

FUNCTION BrbXmlGetNextTag : UDINT (*Gibt die Adresse nach einem Xml-Tag zur�ck*)
	VAR_INPUT
		pTag : STRING[0]; (*Zeiger auf den Tag*)
		pStart : UDINT; (*Zeiger auf den Such-Start*)
		pEnd : UDINT; (*Zeiger auf das Such-Ende*)
	END_VAR
END_FUNCTION
(*Memory*)

FUNCTION BrbMemListClear : DINT (*L�scht die gesamte Liste*)
	VAR_INPUT
		pListManagement : BrbMemListManagement_Typ; (*Zeiger auf die Verwaltungs-Struktur*)
	END_VAR
END_FUNCTION

FUNCTION BrbMemListIn : DINT (*F�gt einen Eintrag an bestimmter Stelle in der Liste ein*)
	VAR_INPUT
		pListManagement : BrbMemListManagement_Typ; (*Zeiger auf die Verwaltungs-Struktur*)
		nIndex : UDINT; (*Index des Eintrags*)
		pNewEntry : UDINT; (*Zeiger auf die Daten des Eintrags*)
	END_VAR
END_FUNCTION

FUNCTION BrbMemListOut : DINT (*Holt einen bestimmten Eintrag und l�scht ihn aus der Liste*)
	VAR_INPUT
		pListManagement : BrbMemListManagement_Typ; (*Zeiger auf die Verwaltungs-Struktur*)
		nIndex : UDINT; (*Index des Eintrags*)
		pListEntry : UDINT; (*Zeiger auf die Daten des Eintrags*)
	END_VAR
END_FUNCTION

FUNCTION BrbMemListGetEntry : DINT (*Holt einen bestimmten Eintrag aus der Liste*)
	VAR_INPUT
		pListManagement : BrbMemListManagement_Typ; (*Zeiger auf die Verwaltungs-Struktur*)
		nIndex : UDINT; (*Index des Eintrags*)
		pListEntry : UDINT; (*Zeiger auf die Daten des Eintrags*)
	END_VAR
END_FUNCTION

FUNCTION BrbFifoIn : DINT (*F�gt einen neuen Eintrag an der letzten Stelle hinzu*)
	VAR_INPUT
		pListManagement : BrbMemListManagement_Typ; (*Zeiger auf die Verwaltungs-Struktur*)
		pNewEntry : UDINT; (*Zeiger auf die Daten des Eintrags*)
	END_VAR
END_FUNCTION

FUNCTION BrbFifoOut : DINT (*Gibt den Eintrag an erster Stelle zur�ck und l�scht ihn aus der Liste*)
	VAR_INPUT
		pListManagement : BrbMemListManagement_Typ; (*Zeiger auf die Verwaltungs-Struktur*)
		pListEntry : UDINT; (*Zeiger auf die Daten des Eintrags*)
	END_VAR
END_FUNCTION

FUNCTION BrbLifoIn : DINT (*F�gt einen neuen Eintrag an der letzten Stelle hinzu*)
	VAR_INPUT
		pListManagement : BrbMemListManagement_Typ; (*Zeiger auf die Verwaltungs-Struktur*)
		pNewEntry : UDINT; (*Zeiger auf die Daten des Eintrags*)
	END_VAR
END_FUNCTION

FUNCTION BrbLifoOut : DINT (*Gibt den Eintrag an letzter Stelle zur�ck und l�scht ihn aus der Liste*)
	VAR_INPUT
		pListManagement : BrbMemListManagement_Typ; (*Zeiger auf die Verwaltungs-Struktur*)
		pListEntry : UDINT; (*Zeiger auf die Daten des Eintrags*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetBitUdint : BOOL (*Liest ein Bit aus einem Udint*)
	VAR_INPUT
		nValue : UDINT; (*Udint-Wert*)
		nBitNumber : UINT; (*Nummer des Bits (0..31)*)
	END_VAR
END_FUNCTION

FUNCTION BrbSetBitUdint : BOOL (*Setzt oder l�scht ein Bit in einem Udint*)
	VAR_INPUT
		pValue : REFERENCE TO UDINT; (*Zeiger auf einen Udint-Wert*)
		nBitNumber : UINT; (*Nummer des Bits (0..31)*)
		bBit : BOOL; (*0=L�schen, 1=Setzen*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetBitMaskUdint : BOOL (*Liest eine Bitmaske aus einem Udint*)
	VAR_INPUT
		nValue : UDINT;
		nBitMask : UDINT;
	END_VAR
END_FUNCTION

FUNCTION BrbSetBitMaskUdint : BOOL (*Setzt oder l�scht eine Bitmaske in einem Udint*)
	VAR_INPUT
		pValue : REFERENCE TO UDINT;
		nBitMask : UDINT;
		bSet : BOOL;
	END_VAR
END_FUNCTION

FUNCTION BrbGetBitUint : BOOL (*Liest ein Bit aus einem Uint*)
	VAR_INPUT
		nValue : UINT; (*Uint-Wert*)
		nBitNumber : UINT; (*Nummer des Bits (0..15)*)
	END_VAR
END_FUNCTION

FUNCTION BrbSetBitUint : BOOL (*Setzt oder l�scht ein Bit in einem Uint*)
	VAR_INPUT
		pValue : REFERENCE TO UINT; (*Zeiger auf einen Uint-Wert*)
		nBitNumber : UINT; (*Nummer des Bits (0..15)*)
		bBit : BOOL; (*0=L�schen, 1=Setzen*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetBitMaskUint : BOOL (*Liest eine Bitmaske aus einem Uint*)
	VAR_INPUT
		nValue : UINT;
		nBitMask : UINT;
	END_VAR
END_FUNCTION

FUNCTION BrbSetBitMaskUint : BOOL (*Setzt oder l�scht eine Bitmaske in einem Uint*)
	VAR_INPUT
		pValue : REFERENCE TO UINT;
		nBitMask : UINT;
		bSet : BOOL;
	END_VAR
END_FUNCTION

FUNCTION BrbGetBitUsint : BOOL (*Liest ein Bit aus einem Usint*)
	VAR_INPUT
		nValue : USINT; (*Usint-Wert*)
		nBitNumber : UINT; (*Nummer des Bits (0..7)*)
	END_VAR
END_FUNCTION

FUNCTION BrbSetBitUsint : BOOL (*Setzt ein Bit in einem Usint*)
	VAR_INPUT
		pValue : REFERENCE TO USINT; (*Zeiger auf einen Usint-Wert*)
		nBitNumber : UINT; (*Nummer des Bits (0..7)*)
		bBit : BOOL; (*0=L�schen, 1=Setzen*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetBitMaskUsint : BOOL (*Liest eine Bitmaske aus einem Usint*)
	VAR_INPUT
		nValue : USINT;
		nBitMask : USINT;
	END_VAR
END_FUNCTION

FUNCTION BrbSetBitMaskUsint : BOOL (*Setzt oder l�scht eine Bitmaske in einem Usint*)
	VAR_INPUT
		pValue : REFERENCE TO USINT;
		nBitMask : USINT;
		bSet : BOOL;
	END_VAR
END_FUNCTION
(*Additional*)

FUNCTION BrbGetRandomPercent : REAL (*Erzeugt eine Zufallszahl zwischen 0.0 und 1.0*)
END_FUNCTION

FUNCTION BrbGetRandomBool : BOOL (*Erzeugt einen boolschen Zufallswert*)
END_FUNCTION

FUNCTION BrbGetRandomUdint : UDINT (*Erzeugt eine Zufallszahl zwischen nMin und nMax*)
	VAR_INPUT
		nMin : UDINT; (*Minimaler Zahlenwert*)
		nMax : UDINT; (*Maximaler Zahlenwert*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetRandomDint : DINT (*Erzeugt eine Zufallszahl zwischen nMin und nMax*)
	VAR_INPUT
		nMin : DINT; (*Minimaler Zahlenwert*)
		nMax : DINT; (*Maximaler Zahlenwert*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetAngleRad : REAL (*Rechnet einen Winkel von Grad ins Bogenmass um*)
	VAR_INPUT
		rAngleDeg : REAL; (*Winkel in Grad*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetAngleDeg : REAL (*Rechnet einen Winkel vom Bogenmass in Grad um*)
	VAR_INPUT
		rAngleRad : REAL; (*Winkel im Bogenmass*)
	END_VAR
END_FUNCTION

FUNCTION BrbNormalizeAngleRad : REAL (*Normalisiert einen Bogenmass-Winkel in den Bereich 0..2Pi*)
	VAR_INPUT
		rAngleRad : REAL; (*Winkel im Bogenmass*)
	END_VAR
END_FUNCTION

FUNCTION BrbNormalizeAngleDeg : REAL (*Normalisiert einen Grad-Winkel in den Bereich 0..360�*)
	VAR_INPUT
		rAngleDeg : REAL; (*Winkel in Grad*)
		bKeep360 : USINT; (*Genau 360.0� werden nicht zu 0.0�*)
	END_VAR
END_FUNCTION

FUNCTION BrbCheckIpAddress : BOOL (*Pr�ft eine Ip-Adresse und berichtigt sie gegebenenfalls*)
	VAR_INPUT
		pIpAddress : STRING[0];
	END_VAR
END_FUNCTION

FUNCTION_BLOCK BrbDebounceInput (*Entprellt einen Eingang*)
	VAR_INPUT
		bInput : BOOL;
		nDebounceTime : UDINT;
	END_VAR
	VAR_OUTPUT
		bOutput : BOOL;
	END_VAR
	VAR
		fbDebounce : TON;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION BrbRoundDint : DINT (*Rundet einen Dint*)
	VAR_INPUT
		nValue : DINT; (*Zu rundendet Wert*)
		eRound : BrbRound_ENUM; (*Rundungstyp*)
		nDigits : USINT; (*Stelle, auf die gerundet wird*)
	END_VAR
END_FUNCTION

FUNCTION BrbGetStructMemberOffset : UINT (*Gibt den Offset eines Struktur-Members zur�ck*)
	VAR_INPUT
		pStructName : STRING[0];
		pMemberName : STRING[0];
		pOffset : REFERENCE TO UDINT;
	END_VAR
END_FUNCTION
