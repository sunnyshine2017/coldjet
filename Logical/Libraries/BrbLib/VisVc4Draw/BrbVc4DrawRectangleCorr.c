/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawRectangleCorr.c
 * Author: niedermeierr
 * Created: September 11, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>

#ifdef __cplusplus
	};
#endif

/* Zeichnet ein korrigiertes Rechteck */
unsigned short BrbVc4DrawRectangleCorr(struct BrbVc4Rectangle_TYP* pRectangle, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	BrbVc4Line_TYP Lines[4];
	memset(&Lines, 0, sizeof(Lines));
	Lines[0].nLeft = pRectangle->nLeft;
	Lines[0].nTop = pRectangle->nTop;
	Lines[0].nRight = pRectangle->nLeft + pRectangle->nWidth;
	Lines[0].nBottom = pRectangle->nTop;
	Lines[0].nColor = pRectangle->nBorderColor;
	Lines[0].nDashWidth = pRectangle->nDashWidth;
	BrbVc4CorrectLine(&Lines[0]);
	Lines[1].nLeft = pRectangle->nLeft + pRectangle->nWidth;
	Lines[1].nTop = pRectangle->nTop;
	Lines[1].nRight = pRectangle->nLeft + pRectangle->nWidth;
	Lines[1].nBottom = pRectangle->nTop + pRectangle->nHeight;
	Lines[1].nColor = pRectangle->nBorderColor;
	Lines[1].nDashWidth = pRectangle->nDashWidth;
	BrbVc4CorrectLine(&Lines[1]);
	Lines[2].nLeft = pRectangle->nLeft + pRectangle->nWidth;
	Lines[2].nTop = pRectangle->nTop + pRectangle->nHeight;
	Lines[2].nRight = pRectangle->nLeft;
	Lines[2].nBottom = pRectangle->nTop + pRectangle->nHeight;
	Lines[2].nColor = pRectangle->nBorderColor;
	Lines[2].nDashWidth = pRectangle->nDashWidth;
	BrbVc4CorrectLine(&Lines[2]);
	Lines[3].nLeft = pRectangle->nLeft;
	Lines[3].nTop = pRectangle->nTop + pRectangle->nHeight;
	Lines[3].nRight = pRectangle->nLeft;
	Lines[3].nBottom = pRectangle->nTop;
	Lines[3].nColor = pRectangle->nBorderColor;
	Lines[3].nDashWidth = pRectangle->nDashWidth;
	BrbVc4CorrectLine(&Lines[3]);

	if(pRectangle->nFillColor != 255)
	{
		BrbVc4Rectangle_TYP RectCorr;
		memcpy(&RectCorr, pRectangle, sizeof(BrbVc4Rectangle_TYP));
		BrbVc4CorrectRectangle(&RectCorr);
		nResult = VA_Rect(1, pGeneral->nVcHandle, RectCorr.nLeft, RectCorr.nTop, RectCorr.nWidth, RectCorr.nHeight, RectCorr.nFillColor, RectCorr.nFillColor);
	}
	BrbVc4DrawLine(&Lines[0], pGeneral);		
	BrbVc4DrawLine(&Lines[1], pGeneral);		
	BrbVc4DrawLine(&Lines[2], pGeneral);		
	BrbVc4DrawLine(&Lines[3], pGeneral);		
	return nResult;
}
