/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4ClipRectangle.c
 * Author: niedermeierr
 * Created: September 16, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Korrigiert die Koordinaten eines Rechtecks, um in einen Ausschnitt zu passen */
unsigned short BrbVc4ClipRectangle(struct BrbVc4Rectangle_TYP* pRectangle, struct BrbVc4Rectangle_TYP* pClip)
{
	DINT nRight = pRectangle->nLeft + pRectangle->nWidth;
	DINT nBottom = pRectangle->nTop + pRectangle->nHeight;
	DINT nClipRight = pClip->nLeft + pClip->nWidth;
	DINT nClipBottom = pClip->nTop + pClip->nHeight;
	if(pRectangle->nLeft < pClip->nLeft && nRight < pClip->nLeft)
	{
		// Wenn Rechteck ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pRectangle->nLeft = 12345678;
	}
	else if(pRectangle->nTop < pClip->nTop && nBottom < pClip->nTop)
	{
		// Wenn Rechteck ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pRectangle->nTop = 12345678;
	}
	else if(pRectangle->nLeft > nClipRight && nRight > nClipRight)
	{
		// Wenn Rechteck ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pRectangle->nLeft = 12345678;
	}
	else if(pRectangle->nTop > nClipBottom && nBottom > nClipBottom)
	{
		// Wenn Rechteck ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pRectangle->nTop = 12345678;
	}
	else
	{
		if(pRectangle->nLeft < pClip->nLeft)
		{
			pRectangle->nWidth -= pClip->nLeft - pRectangle->nLeft;
			pRectangle->nLeft = pClip->nLeft;
		}
		if(pRectangle->nTop < pClip->nTop)
		{
			pRectangle->nHeight -= pClip->nTop - pRectangle->nTop;
			pRectangle->nTop = pClip->nTop;
		}
		nRight = pRectangle->nLeft + pRectangle->nWidth;
		nBottom = pRectangle->nTop + pRectangle->nHeight;
		if(nRight > nClipRight)
		{
			pRectangle->nWidth -= nRight - nClipRight;
		}
		if(nBottom > nClipBottom)
		{
			pRectangle->nHeight -= nBottom - nClipBottom;
		}
	}
	return 0;
}
