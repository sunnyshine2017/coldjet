/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4ClipLine.c
 * Author: niedermeierr
 * Created: September 16, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Korrigiert die Koordinaten einer Linie, um in einen Ausschnitt zu passen */
unsigned short BrbVc4ClipLine(struct BrbVc4Line_TYP* pLine, struct BrbVc4Rectangle_TYP* pClip)
{
	DINT nClipRight = pClip->nLeft + pClip->nWidth;
	DINT nClipBottom = pClip->nTop + pClip->nHeight;
	if(pLine->nLeft < pClip->nLeft && pLine->nRight < pClip->nLeft)
	{
		// Wenn Linie ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pLine->nLeft = 12345678;
		pLine->nRight = 12345678;
	}
	else if(pLine->nLeft > nClipRight && pLine->nRight > nClipRight)
	{
		// Wenn Linie ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pLine->nLeft = 12345678;
		pLine->nRight = 12345678;
	}
	else if(pLine->nTop < pClip->nTop && pLine->nBottom < pClip->nTop)
	{
		// Wenn Linie ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pLine->nTop = 12345678;
		pLine->nBottom = 12345678;
	}
	else if(pLine->nTop > nClipBottom && pLine->nBottom > nClipBottom)
	{
		// Wenn Linie ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pLine->nTop = 12345678;
		pLine->nBottom = 12345678;
	}
	else
	{
		if(pLine->nLeft < pClip->nLeft)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffX != 0.0)
			{
				REAL rGradient = rDiffY / rDiffX; // Steigung
				REAL rOffsetX = (REAL)(pClip->nLeft - pLine->nLeft); // Verschiebung
				pLine->nLeft = pClip->nLeft;
				pLine->nTop = pLine->nTop + rGradient * rOffsetX;
			}
		}
		else if(pLine->nRight < pClip->nLeft)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffX != 0.0)
			{
				REAL rGradient = rDiffY / rDiffX; // Steigung
				REAL rOffsetX = (REAL)(pClip->nLeft - pLine->nRight); // Verschiebung
				pLine->nRight = pClip->nLeft;
				pLine->nBottom = pLine->nBottom + rGradient * rOffsetX;
			}
		}
		if(pLine->nTop < pClip->nTop)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffY != 0.0)
			{
				REAL rGradient = rDiffX / rDiffY; // Steigung
				REAL rOffsetY = (REAL)(pClip->nTop - pLine->nTop); // Verschiebung
				pLine->nTop = pClip->nTop;
				pLine->nLeft = pLine->nLeft + rGradient * rOffsetY;
			}
		}
		else if(pLine->nBottom < pClip->nTop)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffY != 0.0)
			{
				REAL rGradient = rDiffX / rDiffY; // Steigung
				REAL rOffsetY = (REAL)(pClip->nTop - pLine->nBottom); // Verschiebung
				pLine->nBottom = pClip->nTop;
				pLine->nRight = pLine->nRight + rGradient * rOffsetY;
			}
		}

		if(pLine->nLeft > nClipRight)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffX != 0.0)
			{
				REAL rGradient = rDiffY / rDiffX; // Steigung
				REAL rOffsetX = (REAL)(nClipRight - pLine->nLeft); // Verschiebung
				pLine->nLeft = nClipRight;
				pLine->nTop = pLine->nTop + rGradient * rOffsetX;
			}
		}
		else if(pLine->nRight > nClipRight)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffX != 0.0)
			{
				REAL rGradient = rDiffY / rDiffX; // Steigung
				REAL rOffsetX = (REAL)(nClipRight - pLine->nRight); // Verschiebung
				pLine->nRight = nClipRight;
				pLine->nBottom = pLine->nBottom + rGradient * rOffsetX;
			}
		}
		if(pLine->nTop > nClipBottom)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffY != 0.0)
			{
				REAL rGradient = rDiffX / rDiffY; // Steigung
				REAL rOffsetY = (REAL)(nClipBottom - pLine->nTop); // Verschiebung
				pLine->nTop = nClipBottom;
				pLine->nLeft = pLine->nLeft + rGradient * rOffsetY;
			}
		}
		else if(pLine->nBottom > nClipBottom)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffY != 0.0)
			{
				REAL rGradient = rDiffX / rDiffY; // Steigung
				REAL rOffsetY = (REAL)(nClipBottom - pLine->nBottom); // Verschiebung
				pLine->nBottom = nClipBottom;
				pLine->nRight = pLine->nRight + rGradient * rOffsetY;
			}
		}
	}
	return 0;
}
