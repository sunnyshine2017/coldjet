/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4CorrectLine.c
 * Author: niedermeierr
 * Created: August 01, 2014
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Korrigiert eine Linie mit negativen Koordinaten */
unsigned short BrbVc4CorrectLine(struct BrbVc4Line_TYP* pLine)
{
	// Minuswerte bei Koordinaten werden von der Funktion "VA_Line" falsch gezeichnet.
	// Hier werden die Koordinaten auf >= 0 begrenzt
	if(pLine->nLeft < 0 && pLine->nRight < 0)
	{
		// Wenn Linie ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pLine->nLeft = 12345678;
		pLine->nRight = 12345678;
	}
	else if(pLine->nTop < 0 && pLine->nBottom < 0)
	{
		// Wenn Linie ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pLine->nTop = 12345678;
		pLine->nBottom = 12345678;
	}
	else
	{
		if(pLine->nLeft < 0)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffX != 0.0)
			{
				REAL rGradient = rDiffY / rDiffX; // Steigung
				REAL rOffsetX = (REAL)(pLine->nLeft); // Verschiebung
				pLine->nLeft = 0;
				pLine->nTop = pLine->nTop - rGradient * rOffsetX;
			}
		}
		else if(pLine->nRight < 0)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffX != 0.0)
			{
				REAL rGradient = rDiffY / rDiffX; // Steigung
				REAL rOffsetX = (REAL)(pLine->nRight); // Verschiebung
				pLine->nRight = 0;
				pLine->nBottom = pLine->nBottom - rGradient * rOffsetX;
			}
		}
		if(pLine->nTop < 0)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffY != 0.0)
			{
				REAL rGradient = rDiffX / rDiffY; // Steigung
				REAL rOffsetY = (REAL)(pLine->nTop); // Verschiebung
				pLine->nTop = 0;
				pLine->nLeft = pLine->nLeft - rGradient * rOffsetY;
			}
		}
		else if(pLine->nBottom < 0)
		{
			REAL rDiffX = (REAL)(pLine->nRight - pLine->nLeft);
			REAL rDiffY = (REAL)(pLine->nBottom - pLine->nTop);
			if(rDiffY != 0.0)
			{
				REAL rGradient = rDiffX / rDiffY; // Steigung
				REAL rOffsetY = (REAL)(pLine->nBottom); // Verschiebung
				pLine->nBottom = 0;
				pLine->nRight = pLine->nRight - rGradient * rOffsetY;
			}
		}
	}
	return 0;
}
