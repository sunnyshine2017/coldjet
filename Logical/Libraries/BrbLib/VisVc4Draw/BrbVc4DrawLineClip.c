/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawLineClip.c
 * Author: niedermeierr
 * Created: September 16, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>

#ifdef __cplusplus
	};
#endif

/* Zeichnet eine Linie in einen Ausschnitt */
unsigned short BrbVc4DrawLineClip(struct BrbVc4Line_TYP* pLine, struct BrbVc4Rectangle_TYP* pClip, struct BrbVc4General_TYP* pGeneral)
{
	BrbVc4Line_TYP Line;
	memcpy(&Line, pLine, sizeof(BrbVc4Line_TYP));
	BrbVc4ClipLine(&Line, pClip);
	return BrbVc4DrawLine(&Line, pGeneral);
}
