/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4LinkTrends.c
 * Author: niedermeierr
 * Created: January 10, 2016
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Linkt die Touch-Bedienung zwischen mehreren Trends */
unsigned short BrbVc4LinkTrends(struct BrbVc4LinkTrends_TYP* pLinkTrends, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	if(pLinkTrends->bEnable == 1)
	{
		UINT nTrendIndexSource = 0;
		UINT nTrendIndexTarget = 0;
		for(nTrendIndexSource=0; nTrendIndexSource<=nBRB_TRENDLINK_TREND_INDEX_MAX; nTrendIndexSource++)
		{
			if(pLinkTrends->Cfg.Trend[nTrendIndexSource].pTrend != 0)
			{
				BrbVc4DrawTrend_TYP* pTrendSource = pLinkTrends->Cfg.Trend[nTrendIndexSource].pTrend;
				if(pTrendSource->bEnable == 1)
				{
					// SetCursor0
					if(pLinkTrends->Cfg.bLinkSetCursor0 == 1 && pTrendSource->Cfg.TouchAction.bSetCursor0 == 1)
					{
						if(pTrendSource->State.eTouchAction == eBRB_TREND_TOUCHACTION_CURS_SET)
						{
							for(nTrendIndexTarget=0; nTrendIndexTarget<=nBRB_TRENDLINK_TREND_INDEX_MAX; nTrendIndexTarget++)
							{
								if(nTrendIndexSource != nTrendIndexTarget)
								{
									if(pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend != 0)
									{
										BrbVc4DrawTrend_TYP* pTrendTarget = pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend;
										if(pTrendTarget->bEnable == 1 && pTrendTarget->Cfg.TouchAction.bSetCursor0 == 1)
										{
											pTrendTarget->Cfg.Cursor[0].nSampleIndex = pTrendSource->Cfg.Cursor[0].nSampleIndex;
										}
									}
								}
							}
						}
					}
					// SetCursor1
					if(pLinkTrends->Cfg.bLinkSetCursor1 == 1 && pTrendSource->Cfg.TouchAction.bSetCursor1 == 1)
					{
						if(pTrendSource->State.eTouchAction == eBRB_TREND_TOUCHACTION_CURS_SET)
						{
							for(nTrendIndexTarget=0; nTrendIndexTarget<=nBRB_TRENDLINK_TREND_INDEX_MAX; nTrendIndexTarget++)
							{
								if(nTrendIndexSource != nTrendIndexTarget)
								{
									if(pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend != 0)
									{
										BrbVc4DrawTrend_TYP* pTrendTarget = pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend;
										if(pTrendTarget->bEnable == 1 && pTrendTarget->Cfg.TouchAction.bSetCursor1 == 1)
										{
											pTrendTarget->Cfg.Cursor[1].nSampleIndex = pTrendSource->Cfg.Cursor[1].nSampleIndex;
										}
									}
								}
							}
						}
					}
					// ZoomX + ZoomY
					if((pLinkTrends->Cfg.bLinkZoomX == 1 && pTrendSource->Cfg.TouchAction.bZoomX == 1) || (pLinkTrends->Cfg.bLinkZoomY == 1 && pTrendSource->Cfg.TouchAction.bZoomY == 1) )
					{
						if(pTrendSource->State.eTouchAction == eBRB_TREND_TOUCHACTION_ZOOM_DRAG)
						{
							for(nTrendIndexTarget=0; nTrendIndexTarget<=nBRB_TRENDLINK_TREND_INDEX_MAX; nTrendIndexTarget++)
							{
								if(nTrendIndexSource != nTrendIndexTarget)
								{
									if(pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend != 0)
									{
										BrbVc4DrawTrend_TYP* pTrendTarget = pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend;
										if(pTrendTarget->bEnable == 1)
										{
											if(pLinkTrends->Cfg.bLinkZoomX == 1 && pLinkTrends->Cfg.bLinkZoomY == 1)
											{
												if(pTrendSource->Cfg.TouchAction.bZoomX == 1 && pTrendTarget->Cfg.TouchAction.bZoomX == 1 && pTrendSource->Cfg.TouchAction.bZoomY == 1 && pTrendTarget->Cfg.TouchAction.bZoomY == 1)
												{
													pTrendTarget->Intern.ZoomWindow.bShow = pTrendSource->Intern.ZoomWindow.bShow;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nLeft = pTrendSource->Intern.ZoomWindow.RectangleTouch.nLeft;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nWidth = pTrendSource->Intern.ZoomWindow.RectangleTouch.nWidth;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nTop = pTrendSource->Intern.ZoomWindow.RectangleTouch.nTop;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nHeight = pTrendSource->Intern.ZoomWindow.RectangleTouch.nHeight;
												}
												else if(pTrendSource->Cfg.TouchAction.bZoomX == 1 && pTrendTarget->Cfg.TouchAction.bZoomX == 1)
												{
													pTrendTarget->Intern.ZoomWindow.bShow = pTrendSource->Intern.ZoomWindow.bShow;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nLeft = pTrendSource->Intern.ZoomWindow.RectangleTouch.nLeft;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nWidth = pTrendSource->Intern.ZoomWindow.RectangleTouch.nWidth;
													if(pTrendTarget->Cfg.TouchAction.bZoomY == 1)
													{
														pTrendTarget->Intern.ZoomWindow.RectangleTouch.nTop = pTrendTarget->Intern.CurveArea.nTop;
														pTrendTarget->Intern.ZoomWindow.RectangleTouch.nHeight = pTrendTarget->Intern.CurveArea.nHeight;
													}
												}
												else if(pTrendSource->Cfg.TouchAction.bZoomY == 1 && pTrendTarget->Cfg.TouchAction.bZoomY == 1)
												{
													pTrendTarget->Intern.ZoomWindow.bShow = pTrendSource->Intern.ZoomWindow.bShow;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nTop = pTrendSource->Intern.ZoomWindow.RectangleTouch.nTop;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nHeight = pTrendSource->Intern.ZoomWindow.RectangleTouch.nHeight;
													if(pTrendTarget->Cfg.TouchAction.bZoomX == 1)
													{
														pTrendTarget->Intern.ZoomWindow.RectangleTouch.nLeft = pTrendTarget->Intern.CurveArea.nLeft;
														pTrendTarget->Intern.ZoomWindow.RectangleTouch.nWidth = pTrendTarget->Intern.CurveArea.nWidth;
													}
												}
											}
											else if(pLinkTrends->Cfg.bLinkZoomX == 1)
											{
												if(pTrendSource->Cfg.TouchAction.bZoomX == 1 && pTrendTarget->Cfg.TouchAction.bZoomX == 1)
												{
													pTrendTarget->Intern.ZoomWindow.bShow = pTrendSource->Intern.ZoomWindow.bShow;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nLeft = pTrendSource->Intern.ZoomWindow.RectangleTouch.nLeft;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nTop = pTrendTarget->Intern.CurveArea.nTop;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nWidth = pTrendSource->Intern.ZoomWindow.RectangleTouch.nWidth;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nHeight = pTrendTarget->Intern.CurveArea.nHeight;
												}
											}
											else if(pLinkTrends->Cfg.bLinkZoomY == 1)
											{
												if(pTrendSource->Cfg.TouchAction.bZoomY == 1 && pTrendTarget->Cfg.TouchAction.bZoomY == 1)
												{
													pTrendTarget->Intern.ZoomWindow.bShow = pTrendSource->Intern.ZoomWindow.bShow;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nLeft = pTrendTarget->Intern.CurveArea.nLeft;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nTop = pTrendSource->Intern.ZoomWindow.RectangleTouch.nTop;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nWidth = pTrendTarget->Intern.CurveArea.nWidth;
													pTrendTarget->Intern.ZoomWindow.RectangleTouch.nHeight = pTrendSource->Intern.ZoomWindow.RectangleTouch.nHeight;
												}
											}
										}
									}
								}
							}
						}
						if(pTrendSource->State.eTouchAction == eBRB_TREND_TOUCHACTION_ZOOM_SET)
						{
							for(nTrendIndexTarget=0; nTrendIndexTarget<=nBRB_TRENDLINK_TREND_INDEX_MAX; nTrendIndexTarget++)
							{
								if(nTrendIndexSource != nTrendIndexTarget)
								{
									if(pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend != 0)
									{
										BrbVc4DrawTrend_TYP* pTrendTarget = pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend;
										if(pLinkTrends->Cfg.bLinkZoomX == 1 && pTrendTarget->bEnable == 1 && pTrendTarget->Cfg.TouchAction.bZoomX == 1)
										{
											pTrendTarget->Intern.ZoomWindow.bShow = pTrendSource->Intern.ZoomWindow.bShow;
											pTrendTarget->Cfg.ScaleX.nScrollOffset = pTrendSource->Cfg.ScaleX.nScrollOffset;
											pTrendTarget->Cfg.ScaleX.nZoomValueCount = pTrendSource->Cfg.ScaleX.nZoomValueCount;
										}
										if(pLinkTrends->Cfg.bLinkZoomY == 1 && pTrendTarget->bEnable == 1 && pTrendTarget->Cfg.TouchAction.bZoomY == 1)
										{
											pTrendTarget->Intern.ZoomWindow.bShow = pTrendSource->Intern.ZoomWindow.bShow;
											DINT eScaleIndex = 0;
											for(eScaleIndex=eBRB_TREND_SCALE_Y_INDEX_LEFT; eScaleIndex<=eBRB_TREND_SCALE_Y_INDEX_RIGHT; eScaleIndex++)
											{
												DINT nTop = pTrendTarget->Intern.ZoomWindow.RectangleTouch.nTop;
												DINT nBottom = pTrendTarget->Intern.ZoomWindow.RectangleTouch.nTop + pTrendTarget->Intern.ZoomWindow.RectangleTouch.nHeight;
												if(nBottom < nTop)
												{
													DINT nTemp = nBottom;
													nBottom = nTop;
													nTop = nTemp;
												}
												REAL rScaleMin = ((pTrendTarget->Intern.CurveArea.nTop + pTrendTarget->Intern.CurveArea.nHeight - nBottom) / pTrendTarget->Intern.ScaleY[eScaleIndex].rValueScaleFactor) + pTrendTarget->Cfg.ScaleY[eScaleIndex].rMin;
												REAL rScaleMax = ((pTrendTarget->Intern.CurveArea.nTop + pTrendTarget->Intern.CurveArea.nHeight - nTop) / pTrendTarget->Intern.ScaleY[eScaleIndex].rValueScaleFactor) + pTrendTarget->Cfg.ScaleY[eScaleIndex].rMin;
												pTrendTarget->Cfg.ScaleY[eScaleIndex].rMin = rScaleMin;
												pTrendTarget->Cfg.ScaleY[eScaleIndex].rMax = rScaleMax;
											}
										}
									}
								}
							}
						}
					}
					// ScrollX
					if(pLinkTrends->Cfg.bLinkScrollX == 1 && pTrendSource->Cfg.TouchAction.bScrollX == 1)
					{
						if(pTrendSource->State.eTouchAction == eBRB_TREND_TOUCHACTION_SCROLL)
						{
							for(nTrendIndexTarget=0; nTrendIndexTarget<=nBRB_TRENDLINK_TREND_INDEX_MAX; nTrendIndexTarget++)
							{
								if(nTrendIndexSource != nTrendIndexTarget)
								{
									if(pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend != 0)
									{
										BrbVc4DrawTrend_TYP* pTrendTarget = pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend;
										if(pTrendTarget->bEnable == 1 && pTrendTarget->Cfg.TouchAction.bScrollX == 1)
										{
											pTrendTarget->Cfg.ScaleX.nScrollOffset = pTrendSource->Cfg.ScaleX.nScrollOffset;
										}
									}
								}
							}
						}
					}
					// ScrollY
					if(pLinkTrends->Cfg.bLinkScrollY == 1 && pTrendSource->Cfg.TouchAction.bScrollY == 1)
					{
						if(pTrendSource->State.eTouchAction == eBRB_TREND_TOUCHACTION_SCROLL)
						{
							DINT nCursorY = pTrendSource->Intern.TouchAction.TouchPointCurveArea.nY; // Auf CurveArea begrenzt
							if(nCursorY < 0)
							{
								nCursorY = 0;
							}
							else if(nCursorY > pTrendSource->Intern.CurveArea.nHeight)
							{
								nCursorY = pTrendSource->Intern.CurveArea.nHeight;
							}
							for(nTrendIndexTarget=0; nTrendIndexTarget<=nBRB_TRENDLINK_TREND_INDEX_MAX; nTrendIndexTarget++)
							{
								if(nTrendIndexSource != nTrendIndexTarget)
								{
									if(pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend != 0)
									{
										BrbVc4DrawTrend_TYP* pTrendTarget = pLinkTrends->Cfg.Trend[nTrendIndexTarget].pTrend;
										if(pTrendTarget->bEnable == 1 && pTrendTarget->Cfg.TouchAction.bScrollY == 1)
										{
											DINT eScaleIndex = 0;
											for(eScaleIndex=eBRB_TREND_SCALE_Y_INDEX_LEFT; eScaleIndex<=eBRB_TREND_SCALE_Y_INDEX_RIGHT; eScaleIndex++)
											{
												REAL rScrollY = (REAL)(pTrendSource->Intern.TouchAction.LastScrollDifference.nY) / pTrendTarget->Intern.ScaleY[eScaleIndex].rValueScaleFactor;
												pTrendTarget->Cfg.ScaleY[eScaleIndex].rMin -= rScrollY;
												pTrendTarget->Cfg.ScaleY[eScaleIndex].rMax -= rScrollY;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return nResult;
}
