/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawEllipse.c
 * Author: niedermeierr
 * Created: July 02, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>

#ifdef __cplusplus
	};
#endif

/* Zeichnet eine Ellipse */
unsigned short BrbVc4DrawEllipse(struct BrbVc4Ellipse_TYP* pEllipse, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	UINT nMiddleX = (UINT)(pEllipse->nLeft + pEllipse->nWidth / 2);
	UINT nMiddleY = (UINT)(pEllipse->nTop + pEllipse->nHeight / 2);
	if(pEllipse->nDashWidth > 1)
	{
		// Gestrichelt
		BrbVc4Arc_TYP Arc;
		memset(&Arc, 0, sizeof(Arc));
		Arc.nLeft = pEllipse->nLeft;
		Arc.nTop = pEllipse->nTop;
		Arc.nWidth = pEllipse->nWidth;
		Arc.nHeight = pEllipse->nHeight;
		Arc.rStartAngle = 0.0;
		Arc.rEndAngle = 360.0;
		Arc.nBorderColor = pEllipse->nBorderColor;
		Arc.nFillColor = pEllipse->nFillColor;
		Arc.nDashWidth = pEllipse->nDashWidth;
		if(pEllipse->nFillColor != 255)
		{
			nResult = VA_Ellipse(1, pGeneral->nVcHandle, nMiddleX, nMiddleY, pEllipse->nHeight, pEllipse->nWidth, pEllipse->nFillColor, pEllipse->nFillColor);
		}
		nResult = BrbVc4DrawArc(&Arc, pGeneral);
	}
	else
	{
		// Solide
		nResult = VA_Ellipse(1, pGeneral->nVcHandle, nMiddleX, nMiddleY, pEllipse->nHeight, pEllipse->nWidth, pEllipse->nFillColor, pEllipse->nBorderColor);
	}
	return nResult;
}
