/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawPlot.c
 * Author: niedermeierr
 * Created: June 25, 2014
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>
#include <math.h>

#ifdef __cplusplus
	};
#endif

REAL GetPlotValueX(BrbVc4DrawPlot_TYP* pPlot, DINT nValueIndex);
REAL GetPlotValueY(BrbVc4DrawPlot_TYP* pPlot, DINT nValueIndex);
REAL GetDistance(DINT nX1, DINT nY1, DINT nX2, DINT nY2);

/* Zeichnet einen XY-Graphen */
unsigned short BrbVc4DrawPlot(struct BrbVc4DrawPlot_TYP* pPlot, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	if(pPlot->bEnable == 1)
	{
		if(pGeneral->nVcHandle != 0)
		{
			if(pGeneral->nRedrawCounter == pPlot->nRedrawCounterMatch)
			{
				// Zugriff reservieren
				pPlot->Intern.nAccessStatus = VA_Saccess(1, pGeneral->nVcHandle);
				if(pPlot->Intern.nAccessStatus == 0)
				{
					// Drawbox verbinden
					pPlot->Intern.nAttachStatus = VA_Attach(1, pGeneral->nVcHandle, 0, (UDINT)&pPlot->Cfg.Drawbox.sFullName);
					if(pPlot->Intern.nAttachStatus == 0)
					{
						// L�schen + Kurven-Bereich
						if(1 == 1)
						{
							// Bisherige Zeichnung l�schen
							BrbVc4Rectangle_TYP RectBackground;
							RectBackground.nLeft = 0;
							RectBackground.nTop = 0;
							RectBackground.nWidth = pPlot->Cfg.Drawbox.nWidth;
							RectBackground.nHeight = pPlot->Cfg.Drawbox.nHeight;
							RectBackground.nFillColor= pPlot->Cfg.Drawbox.nBackgroundColor;
							RectBackground.nBorderColor = pPlot->Cfg.Drawbox.nBackgroundColor;
							RectBackground.nDashWidth = 0;
							BrbVc4DrawRectangle(&RectBackground, pGeneral);
							if(pPlot->Cfg.Callbacks.pCallbackAfterClear != 0)
							{
								// Funktion hinterlegt -> Funktionspointer casten
								UINT (*pFpFunction) (BrbVc4DrawPlot_TYP*) = (void*)pPlot->Cfg.Callbacks.pCallbackAfterClear;
									// Funktion aufrufen
									(*pFpFunction)(pPlot);
							}
							// Kurvenbereich
							pPlot->Intern.CurveArea.nLeft = pPlot->Cfg.Padding.nLeft;
							pPlot->Intern.CurveArea.nTop = pPlot->Cfg.Padding.nTop;
							pPlot->Intern.CurveArea.nWidth = pPlot->Cfg.Drawbox.nWidth - pPlot->Cfg.Padding.nLeft - pPlot->Cfg.Padding.nRight;
							if(pPlot->Intern.CurveArea.nWidth < 10)
							{
								pPlot->Intern.CurveArea.nWidth = 10;
							}
							pPlot->Intern.CurveArea.nHeight = pPlot->Cfg.Drawbox.nHeight - pPlot->Cfg.Padding.nTop - pPlot->Cfg.Padding.nBottom;
							if(pPlot->Intern.CurveArea.nHeight < 10)
							{
								pPlot->Intern.CurveArea.nHeight = 10;
							}
							pPlot->Intern.CurveArea.nFillColor = pPlot->Cfg.nCurveAreaColor;
							pPlot->Intern.CurveArea.nBorderColor = pPlot->Cfg.nCurveAreaColor;
							pPlot->Intern.CurveArea.nDashWidth = 0;
							pPlot->Intern.nCurveAreaRight = pPlot->Intern.CurveArea.nLeft + pPlot->Intern.CurveArea.nWidth;
							pPlot->Intern.nCurveAreaBottom = pPlot->Intern.CurveArea.nTop + pPlot->Intern.CurveArea.nHeight;
							BrbVc4DrawRectangle(&pPlot->Intern.CurveArea, pGeneral);
							if(pPlot->Cfg.Callbacks.pCallbackAfterCurveArea != 0)
							{
								// Funktion hinterlegt -> Funktionspointer casten
								UINT (*pFpFunction) (BrbVc4DrawPlot_TYP*) = (void*)pPlot->Cfg.Callbacks.pCallbackAfterCurveArea;
									// Funktion aufrufen
									(*pFpFunction)(pPlot);
							}
						}
						// Statistik vorbesetzen
						pPlot->State.Statistic.rMinX = 3.4e38;
						pPlot->State.Statistic.rMaxX = -3.4e38;
						pPlot->State.Statistic.rMinY = 3.4e38;
						pPlot->State.Statistic.rMaxY = -3.4e38;
						// Null-Linien
						if(1 == 1)
						{
							if(pPlot->Cfg.bShowZeroLines == 1)
							{
								// Y
								pPlot->Intern.ZeroLineY.nLeft = pPlot->Intern.CurveArea.nLeft;
								pPlot->Intern.ZeroLineY.nRight = pPlot->Intern.nCurveAreaRight;
								pPlot->Intern.ZeroLineY.nTop = BrbVc4GetPlotDisplayCoordinateY(pPlot, 0.0);
								pPlot->Intern.ZeroLineY.nBottom = pPlot->Intern.ZeroLineY.nTop;
								pPlot->Intern.ZeroLineY.nColor = pPlot->Cfg.nZeroLinesColor;
								pPlot->Intern.ZeroLineY.nDashWidth = 0;
								BrbVc4DrawLine(&pPlot->Intern.ZeroLineY, pGeneral);
								// X
								pPlot->Intern.ZeroLineX.nLeft = BrbVc4GetPlotDisplayCoordinateX(pPlot, 0.0);
								pPlot->Intern.ZeroLineX.nRight = pPlot->Intern.ZeroLineX.nLeft;
								pPlot->Intern.ZeroLineX.nTop = pPlot->Intern.CurveArea.nTop;
								pPlot->Intern.ZeroLineX.nBottom = pPlot->Intern.nCurveAreaBottom;
								pPlot->Intern.ZeroLineX.nColor = pPlot->Cfg.nZeroLinesColor;
								pPlot->Intern.ZeroLineX.nDashWidth = 0;
								BrbVc4DrawLine(&pPlot->Intern.ZeroLineX, pGeneral);
								if(pPlot->Cfg.Callbacks.pCallbackAfterZeroLines != 0)
								{
									// Funktion hinterlegt -> Funktionspointer casten
									UINT (*pFpFunction) (BrbVc4DrawPlot_TYP*) = (void*)pPlot->Cfg.Callbacks.pCallbackAfterZeroLines;
										// Funktion aufrufen
										(*pFpFunction)(pPlot);
								}
							}
						}
						// Skalierung
						BrbVc4DrawText_TYP ScaleText;
						// Skalierung Y
						if(1 == 1)
						{
							if(pPlot->Cfg.ScaleY.rMax <= pPlot->Cfg.ScaleY.rMin)
							{
								pPlot->Cfg.ScaleY.rMax = pPlot->Cfg.ScaleY.rMin + 1.0;
							}
							pPlot->Intern.ScaleY.rValueScaleFactor = (REAL)pPlot->Intern.CurveArea.nHeight / (pPlot->Cfg.ScaleY.rMax - pPlot->Cfg.ScaleY.rMin);
							if(pPlot->Cfg.ScaleY.bShow == 1)
							{
								// Basis-Linie
								pPlot->Intern.ScaleY.ScaleLine.nLeft = BrbVc4GetPlotDisplayCoordinateX(pPlot, pPlot->Cfg.ScaleX.rMin);
								pPlot->Intern.ScaleY.ScaleLine.nRight = pPlot->Intern.ScaleY.ScaleLine.nLeft;
								pPlot->Intern.ScaleY.ScaleLine.nTop = BrbVc4GetPlotDisplayCoordinateY(pPlot, pPlot->Cfg.ScaleY.rMax);
								pPlot->Intern.ScaleY.ScaleLine.nBottom = BrbVc4GetPlotDisplayCoordinateY(pPlot, pPlot->Cfg.ScaleY.rMin);
								pPlot->Intern.ScaleY.ScaleLine.nColor = pPlot->Cfg.ScaleY.nColor;
								pPlot->Intern.ScaleY.ScaleLine.nDashWidth = 0;
								BrbVc4DrawLine(&pPlot->Intern.ScaleY.ScaleLine, pGeneral);
								// Skalierungsschritte
								ScaleText.nFontIndex = pPlot->Cfg.ScaleFont.nIndex;
								ScaleText.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
								BrbVc4Line_TYP ScaleLine;
								ScaleLine.nLeft = pPlot->Intern.CurveArea.nLeft - (DINT)(pPlot->Cfg.ScaleY.nLineLength/2.0);
								ScaleLine.nRight = ScaleLine.nLeft + pPlot->Cfg.ScaleY.nLineLength;
								ScaleLine.nColor = pPlot->Cfg.ScaleY.nColor;
								ScaleLine.nDashWidth = 0;
								if(pPlot->Cfg.ScaleY.nLinesCount > 0)
								{
									UINT nLineIndex = 0;
									pPlot->Intern.ScaleY.rScaleDistance = (pPlot->Cfg.ScaleY.rMax - pPlot->Cfg.ScaleY.rMin) / pPlot->Cfg.ScaleY.nLinesCount;
									for(nLineIndex=0; nLineIndex<=pPlot->Cfg.ScaleY.nLinesCount; nLineIndex++)
									{
										REAL rScaleValue = pPlot->Cfg.ScaleY.rMin + (nLineIndex * pPlot->Intern.ScaleY.rScaleDistance);
										ScaleLine.nTop = BrbVc4GetPlotDisplayCoordinateY(pPlot, rScaleValue);
										ScaleLine.nBottom = ScaleLine.nTop;
										if(pPlot->Cfg.ScaleY.Grid.bShow == 1)
										{
											BrbVc4Line_TYP GridLine;
											GridLine.nLeft = pPlot->Intern.CurveArea.nLeft;
											GridLine.nTop = ScaleLine.nTop;
											GridLine.nRight = pPlot->Intern.CurveArea.nLeft + pPlot->Intern.CurveArea.nWidth;
											GridLine.nBottom = ScaleLine.nTop;
											GridLine.nColor = pPlot->Cfg.ScaleY.Grid.nColor;
											GridLine.nDashWidth = 0;
											BrbVc4DrawLine(&GridLine, pGeneral);
										}								
										BrbVc4DrawLine(&ScaleLine, pGeneral);
										brsftoa(rScaleValue, (UDINT)&ScaleText.sText);
										BrbStringFormatFractionDigits(ScaleText.sText, sizeof(ScaleText.sText), pPlot->Cfg.ScaleY.nFractionDigits);
										if(rScaleValue >= 0.0)
										{
											BrbStringInsert(ScaleText.sText, 0, "+");
										}
										UDINT nTextLen = strlen(ScaleText.sText);
										ScaleText.nLeft = ScaleLine.nLeft - (DINT)((REAL)nTextLen * pPlot->Cfg.ScaleFont.rCharWidth) - 4;
										ScaleText.nTop = ScaleLine.nTop - (DINT)(pPlot->Cfg.ScaleFont.rCharHeight / 2.0);
										ScaleText.nTextColor = pPlot->Cfg.ScaleY.nColor;
										BrbVc4DrawText(&ScaleText, pGeneral);
										if(pPlot->Cfg.Callbacks.pCallbackAfterScaleLineY != 0)
										{
											// Funktion hinterlegt -> Funktionspointer casten
											UINT (*pFpFunction) (BrbVc4DrawPlot_TYP*, UINT, BrbVc4Line_TYP*, BrbVc4DrawText_TYP*, REAL) = (void*)pPlot->Cfg.Callbacks.pCallbackAfterScaleLineY;
											// Funktion aufrufen
											(*pFpFunction)(pPlot, nLineIndex, &ScaleLine, &ScaleText, rScaleValue);
										}
									}
								}
								if(pPlot->Cfg.Callbacks.pCallbackAfterScaleY != 0)
								{
									// Funktion hinterlegt -> Funktionspointer casten
									UINT (*pFpFunction) (BrbVc4DrawPlot_TYP*) = (void*)pPlot->Cfg.Callbacks.pCallbackAfterScaleY;
										// Funktion aufrufen
										(*pFpFunction)(pPlot);
								}
							}
						}
						// Skalierung X
						if(1 == 1)
						{
							if(pPlot->Cfg.ScaleX.rMax <= pPlot->Cfg.ScaleX.rMin)
							{
								pPlot->Cfg.ScaleX.rMax = pPlot->Cfg.ScaleX.rMin + 1.0;
							}
							pPlot->Intern.ScaleX.rValueScaleFactor = (REAL)pPlot->Intern.CurveArea.nWidth / (pPlot->Cfg.ScaleX.rMax - pPlot->Cfg.ScaleX.rMin);
							if(pPlot->Cfg.ScaleX.bShow == 1)
							{
								// Basis-Linie
								pPlot->Intern.ScaleX.ScaleLine.nLeft = BrbVc4GetPlotDisplayCoordinateX(pPlot, pPlot->Cfg.ScaleX.rMin);
								pPlot->Intern.ScaleX.ScaleLine.nRight = BrbVc4GetPlotDisplayCoordinateX(pPlot, pPlot->Cfg.ScaleX.rMax);
								pPlot->Intern.ScaleX.ScaleLine.nTop = BrbVc4GetPlotDisplayCoordinateY(pPlot, pPlot->Cfg.ScaleY.rMin);
								pPlot->Intern.ScaleX.ScaleLine.nBottom = pPlot->Intern.ScaleX.ScaleLine.nTop;
								pPlot->Intern.ScaleX.ScaleLine.nColor = pPlot->Cfg.ScaleX.nColor;
								pPlot->Intern.ScaleX.ScaleLine.nDashWidth = 0;
								BrbVc4DrawLine(&pPlot->Intern.ScaleX.ScaleLine, pGeneral);
								// Skalierungsschritte
								BrbVc4Line_TYP ScaleLine;
								ScaleLine.nTop = pPlot->Intern.ScaleX.ScaleLine.nTop - (DINT)(pPlot->Cfg.ScaleX.nLineLength / 2.0);
								ScaleLine.nBottom = ScaleLine.nTop + pPlot->Cfg.ScaleX.nLineLength;
								ScaleLine.nColor = pPlot->Cfg.ScaleX.nColor;
								ScaleLine.nDashWidth = 0;
								ScaleText.nTop = ScaleLine.nBottom;
								ScaleText.nFontIndex = pPlot->Cfg.ScaleFont.nIndex;
								ScaleText.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
								if(pPlot->Cfg.ScaleX.nLinesCount > 0)
								{
									UINT nLineIndex = 0;
									pPlot->Intern.ScaleX.rScaleDistance = (pPlot->Cfg.ScaleX.rMax - pPlot->Cfg.ScaleX.rMin) / pPlot->Cfg.ScaleX.nLinesCount;
									for(nLineIndex=0; nLineIndex<=pPlot->Cfg.ScaleX.nLinesCount; nLineIndex++)
									{
										REAL rScaleValue = pPlot->Cfg.ScaleX.rMin + (nLineIndex * pPlot->Intern.ScaleX.rScaleDistance);
										ScaleLine.nLeft = BrbVc4GetPlotDisplayCoordinateX(pPlot, rScaleValue);
										ScaleLine.nRight = ScaleLine.nLeft;
										if(pPlot->Cfg.ScaleX.Grid.bShow == 1 && nLineIndex != 0 && nLineIndex != pPlot->Cfg.ScaleX.nLinesCount)
										{
											BrbVc4Line_TYP GridLine;
											GridLine.nLeft = ScaleLine.nLeft;
											GridLine.nTop = 	pPlot->Intern.CurveArea.nTop;
											GridLine.nRight = ScaleLine.nLeft;
											GridLine.nBottom = pPlot->Intern.CurveArea.nTop + pPlot->Intern.CurveArea.nHeight - 1;
											GridLine.nColor = pPlot->Cfg.ScaleX.Grid.nColor;
											GridLine.nDashWidth = 0;
											BrbVc4DrawLine(&GridLine, pGeneral);
										}
										BrbVc4DrawLine(&ScaleLine, pGeneral);
										brsftoa(rScaleValue, (UDINT)&ScaleText.sText);
										BrbStringFormatFractionDigits(ScaleText.sText, sizeof(ScaleText.sText), pPlot->Cfg.ScaleX.nFractionDigits);
										if(rScaleValue >= 0.0)
										{
											BrbStringInsert(ScaleText.sText, 0, "+");
										}
										UDINT nTextLen = strlen(ScaleText.sText);
										ScaleText.nLeft = ScaleLine.nLeft;
										// Zwischen-Beschriftungen zentrieren
										ScaleText.nLeft -= (DINT)((nTextLen * pPlot->Cfg.ScaleFont.rCharWidth) / 2.0);
										ScaleText.nTextColor = pPlot->Cfg.ScaleX.nColor;
										BrbVc4DrawText(&ScaleText, pGeneral);
										if(pPlot->Cfg.Callbacks.pCallbackAfterScaleLineX != 0)
										{
											// Funktion hinterlegt -> Funktionspointer casten
											UINT (*pFpFunction) (BrbVc4DrawPlot_TYP*, UINT, BrbVc4Line_TYP*, BrbVc4DrawText_TYP*, REAL) = (void*)pPlot->Cfg.Callbacks.pCallbackAfterScaleLineX;
											// Funktion aufrufen
											(*pFpFunction)(pPlot, nLineIndex, &ScaleLine, &ScaleText, rScaleValue);
										}
									}
								}
								if(pPlot->Cfg.Callbacks.pCallbackAfterScaleX != 0)
								{
									// Funktion hinterlegt -> Funktionspointer casten
									UINT (*pFpFunction) (BrbVc4DrawPlot_TYP*) = (void*)pPlot->Cfg.Callbacks.pCallbackAfterScaleX;
										// Funktion aufrufen
										(*pFpFunction)(pPlot);
								}
							}
						}
						// Plot zeichnen
						if(1 == 1)
						{
							if((pPlot->Cfg.Source.eValueSource == eBRB_PLOT_SOURCE_SINGLE_ARRAY && pPlot->Cfg.Source.pArrayX != 0 && pPlot->Cfg.Source.pArrayY != 0)
								|| (pPlot->Cfg.Source.eValueSource == eBRB_PLOT_SOURCE_STRUCT_ARRAY && pPlot->Cfg.Source.pArrayStruct != 0))
							{
								if(pPlot->Cfg.Source.nSourceArrayIndexMax > 0)
								{
									REAL rValueOldX = GetPlotValueX(pPlot, 0);
									if(rValueOldX < pPlot->State.Statistic.rMinX)
									{
										pPlot->State.Statistic.rMinX = rValueOldX;
									}
									if(rValueOldX > pPlot->State.Statistic.rMaxX)
									{
										pPlot->State.Statistic.rMaxX = rValueOldX;
									}
									REAL rValueOldY = GetPlotValueY(pPlot, 0);
									if(rValueOldY < pPlot->State.Statistic.rMinY)
									{
										pPlot->State.Statistic.rMinY = rValueOldY;
									}
									if(rValueOldY > pPlot->State.Statistic.rMaxY)
									{
										pPlot->State.Statistic.rMaxY = rValueOldY;
									}
									UDINT nValueIndex = 0;
									REAL rCursorDistanceOld = (REAL)pPlot->Intern.CurveArea.nWidth;
									for(nValueIndex=0; nValueIndex<=pPlot->Cfg.Source.nSourceArrayIndexMax; nValueIndex++)
									{
										REAL rValueX = GetPlotValueX(pPlot, nValueIndex);
										REAL rValueY = GetPlotValueY(pPlot, nValueIndex);
										BrbVc4Line_TYP CurveLine;
										CurveLine.nLeft = BrbVc4GetPlotDisplayCoordinateX(pPlot, rValueOldX);
										CurveLine.nRight = BrbVc4GetPlotDisplayCoordinateX(pPlot, rValueX);
										CurveLine.nTop = BrbVc4GetPlotDisplayCoordinateY(pPlot, rValueOldY);
										CurveLine.nBottom = BrbVc4GetPlotDisplayCoordinateY(pPlot, rValueY);
										CurveLine.nColor = pPlot->Cfg.nCurveColor;
										CurveLine.nDashWidth = 0;
										BrbVc4DrawLineClip(&CurveLine, &pPlot->Intern.CurveArea, pGeneral);
										rValueOldX = rValueX;
										rValueOldY = rValueY;
										// Statistik
										if(rValueX < pPlot->State.Statistic.rMinX)
										{
											pPlot->State.Statistic.rMinX = rValueX;
										}
										if(rValueX > pPlot->State.Statistic.rMaxX)
										{
											pPlot->State.Statistic.rMaxX = rValueX;
										}
										if(rValueY < pPlot->State.Statistic.rMinY)
										{
											pPlot->State.Statistic.rMinY = rValueY;
										}
										if(rValueY > pPlot->State.Statistic.rMaxY)
										{
											pPlot->State.Statistic.rMaxY = rValueY;
										}
										// Cursor setzen
										if(pPlot->Cfg.TouchAction.bSetCursor == 1)
										{
											if(pGeneral->Touch.eState == eBRB_TOUCH_STATE_PUSHED)
											{
												DINT nMouseDrawboxX = pGeneral->Touch.nX - pPlot->Cfg.Drawbox.nLeft - pPlot->Cfg.TouchAction.BorderCorrection.nX;
												DINT nMouseDrawboxY = pGeneral->Touch.nY - pPlot->Cfg.Drawbox.nTop - pPlot->Cfg.TouchAction.BorderCorrection.nY;
												DINT nMouseCurveAreaX = nMouseDrawboxX - pPlot->Intern.CurveArea.nLeft;
												DINT nMouseCurveAreaY = nMouseDrawboxY - pPlot->Intern.CurveArea.nTop;
												BOOL bMouseWithinCurveAreaX = (nMouseCurveAreaX >= 0 && nMouseCurveAreaX <= pPlot->Intern.CurveArea.nWidth);
												BOOL bMouseWithinCurveAreaY = (nMouseCurveAreaY >= 0 && nMouseCurveAreaY <= pPlot->Intern.CurveArea.nHeight);
												if(bMouseWithinCurveAreaX == 1 && bMouseWithinCurveAreaY == 1)
												{
													// Abstand des Klicks zum aktuellen Kurvenpunkt
													REAL rCursorDistance = GetDistance(nMouseDrawboxX, nMouseDrawboxY, CurveLine.nRight, CurveLine.nBottom);
													if(rCursorDistance < rCursorDistanceOld)
													{
														// Aktueller Abstand ist k�rzer als zuletzt ermittelter
														rCursorDistanceOld = rCursorDistance;
														pPlot->Cfg.Cursor.rCursorY = rValueY;
														pPlot->Cfg.Cursor.rCursorX = rValueX;
														pPlot->State.nCursorSampleIndex = nValueIndex;
													}
												}
											}
										}
									}
									if(pPlot->Cfg.Callbacks.pCallbackAfterCurve != 0)
									{
										// Funktion hinterlegt -> Funktionspointer casten
										UINT (*pFpFunction) (BrbVc4DrawPlot_TYP*) = (void*)pPlot->Cfg.Callbacks.pCallbackAfterCurve;
											// Funktion aufrufen
											(*pFpFunction)(pPlot);
									}
								}
							}
						}
						// Cursor zeichnen
						if(1 == 1)
						{
							if(pPlot->Cfg.Cursor.bShow == 1)
							{
								// Horizontal
								pPlot->Intern.Cursor.CursorLineY.nColor = pPlot->Cfg.Cursor.nColor;
								pPlot->Intern.Cursor.CursorLineY.nDashWidth = 0;
								pPlot->Intern.Cursor.CursorLineY.nLeft = BrbVc4GetPlotDisplayCoordinateX(pPlot, pPlot->Cfg.ScaleX.rMin);
								pPlot->Intern.Cursor.CursorLineY.nRight = BrbVc4GetPlotDisplayCoordinateX(pPlot, pPlot->Cfg.ScaleX.rMax);
								pPlot->Intern.Cursor.CursorLineY.nTop = BrbVc4GetPlotDisplayCoordinateY(pPlot, pPlot->Cfg.Cursor.rCursorY);
								pPlot->Intern.Cursor.CursorLineY.nBottom = pPlot->Intern.Cursor.CursorLineY.nTop;
								BrbVc4DrawLineClip(&pPlot->Intern.Cursor.CursorLineY, &pPlot->Intern.CurveArea, pGeneral);
								// Vertikal
								pPlot->Intern.Cursor.CursorLineX.nColor = pPlot->Cfg.Cursor.nColor;
								pPlot->Intern.Cursor.CursorLineX.nDashWidth = 0;
								pPlot->Intern.Cursor.CursorLineX.nLeft = BrbVc4GetPlotDisplayCoordinateX(pPlot, pPlot->Cfg.Cursor.rCursorX);
								pPlot->Intern.Cursor.CursorLineX.nRight = pPlot->Intern.Cursor.CursorLineX.nLeft;
								pPlot->Intern.Cursor.CursorLineX.nTop = BrbVc4GetPlotDisplayCoordinateY(pPlot, pPlot->Cfg.ScaleY.rMin);
								pPlot->Intern.Cursor.CursorLineX.nBottom = BrbVc4GetPlotDisplayCoordinateY(pPlot, pPlot->Cfg.ScaleY.rMax);
								BrbVc4DrawLineClip(&pPlot->Intern.Cursor.CursorLineX, &pPlot->Intern.CurveArea, pGeneral);
								BrbVc4DrawText_TYP CursorText;
								memset(&CursorText.sText, 0, sizeof(CursorText.sText));
								STRING sFtoa[100];
								memset(&sFtoa, 0, sizeof(sFtoa));
								brsftoa(pPlot->Cfg.Cursor.rCursorX, (UDINT)&sFtoa);
								BrbStringFormatFractionDigits(sFtoa, sizeof(sFtoa), pPlot->Cfg.ScaleX.nFractionDigits);
								strcat(CursorText.sText, sFtoa);
								strcat(CursorText.sText, " ; ");
								brsftoa(pPlot->Cfg.Cursor.rCursorY, (UDINT)&sFtoa);
								BrbStringFormatFractionDigits(sFtoa, sizeof(sFtoa), pPlot->Cfg.ScaleY.nFractionDigits);
								strcat(CursorText.sText, sFtoa);
								DINT nTextLength = strlen(CursorText.sText) * pPlot->Cfg.ScaleFont.rCharWidth;
								CursorText.nFontIndex = pPlot->Cfg.ScaleFont.nIndex;
								CursorText.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
								CursorText.nLeft = BrbVc4GetPlotDisplayCoordinateX(pPlot, pPlot->Cfg.Cursor.rCursorX) + 2;
								if(CursorText.nLeft >= pPlot->Intern.nCurveAreaRight - nTextLength)
								{
									CursorText.nLeft -= nTextLength;
								}
								CursorText.nTop = BrbVc4GetPlotDisplayCoordinateY(pPlot, pPlot->Cfg.Cursor.rCursorY);
								if(CursorText.nTop >= pPlot->Intern.nCurveAreaBottom - (DINT)pPlot->Cfg.ScaleFont.rCharHeight)
								{
									CursorText.nTop -= (DINT)pPlot->Cfg.ScaleFont.rCharHeight;
								}
								CursorText.nTextColor = pPlot->Cfg.Cursor.nColor;
								BrbVc4DrawText(&CursorText, pGeneral);
								if(pPlot->Cfg.Callbacks.pCallbackAfterCursor != 0)
								{
									// Funktion hinterlegt -> Funktionspointer casten
									UINT (*pFpFunction) (BrbVc4DrawPlot_TYP*) = (void*)pPlot->Cfg.Callbacks.pCallbackAfterCursor;
										// Funktion aufrufen
										(*pFpFunction)(pPlot);
								}
							}	
						}
						// Zoom-Fenster zeichnen
						if(pPlot->Intern.ZoomWindow.bShow == 1)
						{
							pPlot->Intern.ZoomWindow.RectangleDraw.nBorderColor = 164;
							pPlot->Intern.ZoomWindow.RectangleDraw.nFillColor = eBRB_COLOR_TRANSPARENT;
							pPlot->Intern.ZoomWindow.RectangleDraw.nDashWidth = 4;
							if(pPlot->Cfg.TouchAction.bZoomX == 1)
							{
								pPlot->Intern.ZoomWindow.RectangleDraw.nLeft = pPlot->Intern.ZoomWindow.RectangleTouch.nLeft;
								pPlot->Intern.ZoomWindow.RectangleDraw.nWidth = pPlot->Intern.ZoomWindow.RectangleTouch.nWidth;
							}
							else
							{
								pPlot->Intern.ZoomWindow.RectangleDraw.nLeft = pPlot->Intern.CurveArea.nLeft;
								pPlot->Intern.ZoomWindow.RectangleDraw.nWidth = pPlot->Intern.CurveArea.nWidth - 1;
							}
							if(pPlot->Cfg.TouchAction.bZoomY == 1)
							{
								pPlot->Intern.ZoomWindow.RectangleDraw.nTop = pPlot->Intern.ZoomWindow.RectangleTouch.nTop;
								pPlot->Intern.ZoomWindow.RectangleDraw.nHeight = pPlot->Intern.ZoomWindow.RectangleTouch.nHeight;
							}
							else
							{
								pPlot->Intern.ZoomWindow.RectangleDraw.nTop = pPlot->Intern.CurveArea.nTop;
								pPlot->Intern.ZoomWindow.RectangleDraw.nHeight = pPlot->Intern.CurveArea.nHeight - 1;
							}
							if(pPlot->Cfg.TouchAction.bZoomX == 1 && pPlot->Cfg.TouchAction.bZoomY == 1 && (abs(pPlot->Intern.ZoomWindow.RectangleTouch.nWidth) < 10 || abs(pPlot->Intern.ZoomWindow.RectangleTouch.nHeight) < 10))
							{
								BrbVc4Ellipse_TYP ZoomPoint;
								ZoomPoint.nLeft = pPlot->Intern.ZoomWindow.RectangleDraw.nLeft - 2;
								ZoomPoint.nTop = pPlot->Intern.ZoomWindow.RectangleDraw.nTop - 2;
								ZoomPoint.nWidth = 4;
								ZoomPoint.nHeight = 4;
								ZoomPoint.nBorderColor = 0;
								ZoomPoint.nFillColor = 0;
								ZoomPoint.nDashWidth = 0;
								BrbVc4DrawEllipse(&ZoomPoint, pGeneral);
							}
							BrbVc4DrawRectangle(&pPlot->Intern.ZoomWindow.RectangleDraw, pGeneral);
							if(pPlot->Cfg.Callbacks.pCallbackAfterZoomWindow != 0)
							{
								// Funktion hinterlegt -> Funktionspointer casten
								UINT (*pFpFunction) (BrbVc4DrawPlot_TYP*) = (void*)pPlot->Cfg.Callbacks.pCallbackAfterZoomWindow;
									// Funktion aufrufen
									(*pFpFunction)(pPlot);
							}
						}
						// Drawbox trennen
						VA_Detach(1, pGeneral->nVcHandle);
					}
					// Zugriff freigeben
					VA_Srelease(1, pGeneral->nVcHandle);
				}
			}
			// TouchAction
			if(1 == 1)
			{
				pPlot->State.eTouchAction = eBRB_PLOT_TOUCHACTION_NONE;
				pPlot->Intern.TouchAction.TouchPointDrawbox.nX = pGeneral->Touch.nX - pPlot->Cfg.Drawbox.nLeft;
				pPlot->Intern.TouchAction.TouchPointDrawbox.nY = pGeneral->Touch.nY - pPlot->Cfg.Drawbox.nTop;
				BOOL bMouseWithinDrawboxY = (pPlot->Intern.TouchAction.TouchPointDrawbox.nY >= 0 && pPlot->Intern.TouchAction.TouchPointDrawbox.nY <= pPlot->Cfg.Drawbox.nHeight);
				pPlot->Intern.TouchAction.TouchPointCurveArea.nX = pPlot->Intern.TouchAction.TouchPointDrawbox.nX - pPlot->Intern.CurveArea.nLeft;
				pPlot->Intern.TouchAction.TouchPointCurveArea.nY = pPlot->Intern.TouchAction.TouchPointDrawbox.nY - pPlot->Intern.CurveArea.nTop;
				//BOOL bMouseWithinCurveAreaX = (pPlot->Intern.TouchAction.TouchPointCurveArea.nX >= 0 && pPlot->Intern.TouchAction.TouchPointCurveArea.nX <= pPlot->Intern.CurveArea.nWidth);
				BOOL bMouseWithinCurveAreaY = (pPlot->Intern.TouchAction.TouchPointCurveArea.nY >= 0 && pPlot->Intern.TouchAction.TouchPointCurveArea.nY <= pPlot->Intern.CurveArea.nHeight);
				DINT nCursorX = pPlot->Intern.TouchAction.TouchPointCurveArea.nX; // Auf CurveArea begrenzt
				if(nCursorX < 0)
				{
					nCursorX = 0;
				}
				else if(nCursorX > pPlot->Intern.CurveArea.nWidth)
				{
					nCursorX = pPlot->Intern.CurveArea.nWidth;
				}
				DINT nCursorY = pPlot->Intern.TouchAction.TouchPointCurveArea.nY; // Auf CurveArea begrenzt
				if(nCursorY < 0)
				{
					nCursorY = 0;
				}
				else if(nCursorY > pPlot->Intern.CurveArea.nHeight)
				{
					nCursorY = pPlot->Intern.CurveArea.nHeight;
				}
				// "pPlot->Cfg.TouchAction.bSetCursor" wird schon beim Zeichnen des Plot ausgewertet
				if(pPlot->Cfg.TouchAction.bZoomX == 1 || pPlot->Cfg.TouchAction.bZoomY == 1 || pPlot->Cfg.TouchAction.bScrollX == 1  || pPlot->Cfg.TouchAction.bScrollY == 1)
				{
					if(pGeneral->Touch.eStateSync == eBRB_TOUCH_STATE_PUSHED_EDGE)
					{
						// Gedr�ckt
						if(bMouseWithinCurveAreaY == 1)
						{
							// Innerhalb des Kurvenbereichs
							if(pPlot->Cfg.TouchAction.bZoomX == 1 || pPlot->Cfg.TouchAction.bZoomY == 1)
							{
								pPlot->Intern.TouchAction.fbZoomDragDelay.IN = 1; // Verz�gerung starten
								pPlot->Intern.ZoomWindow.RectangleTouch.nLeft = nCursorX + pPlot->Intern.CurveArea.nLeft;
								pPlot->Intern.ZoomWindow.RectangleTouch.nWidth = 0;
								pPlot->Intern.ZoomWindow.RectangleTouch.nTop = nCursorY + pPlot->Intern.CurveArea.nTop;
								pPlot->Intern.ZoomWindow.RectangleTouch.nHeight = 0;									
							}
							pPlot->Intern.TouchAction.LastScrollPosition.nX = nCursorX;
							pPlot->Intern.TouchAction.LastScrollPosition.nY = nCursorY;
						}
					}
					else if(pGeneral->Touch.eStateSync == eBRB_TOUCH_STATE_PUSHED)
					{
						// Gehalten
						if(bMouseWithinDrawboxY == 1)
						{
							// Innerhalb des Kurvenbereichs
							if((pPlot->Cfg.TouchAction.bScrollX == 1 || pPlot->Cfg.TouchAction.bScrollY == 1) && (pPlot->Intern.TouchAction.fbZoomDragDelay.Q == 0 || (pPlot->Cfg.TouchAction.bZoomX == 0 && pPlot->Cfg.TouchAction.bZoomY == 0)))
							{
								// Scroll
								if(pPlot->Cfg.TouchAction.bScrollX == 1 && pPlot->Intern.TouchAction.LastScrollPosition.nX != nCursorX)
								{
									// X-Position wurde ge�ndert -> ScrollX
									pPlot->Intern.TouchAction.LastScrollDifference.nX = pPlot->Intern.TouchAction.LastScrollPosition.nX - nCursorX;
									REAL rScrollX = (REAL)pPlot->Intern.TouchAction.LastScrollDifference.nX / pPlot->Intern.ScaleX.rValueScaleFactor;
									if(rScrollX > 0.0 && rScrollX < 1.0)
									{
										rScrollX = 1.0;
									}
									else if(rScrollX < 0.0 && rScrollX > -1.0)
									{
										rScrollX = -1.0;
									}
									pPlot->Cfg.ScaleX.rMin += rScrollX;
									pPlot->Cfg.ScaleX.rMax += rScrollX;
									if(abs(pPlot->Intern.TouchAction.LastScrollDifference.nX) > 2)
									{
										pPlot->Intern.TouchAction.fbZoomDragDelay.IN = 0; // Verz�gerung stoppen und damit Zoom nicht starten
									}
									pPlot->State.eTouchAction = eBRB_PLOT_TOUCHACTION_SCROLL;
								}
								if(pPlot->Cfg.TouchAction.bScrollY == 1 && pPlot->Intern.TouchAction.LastScrollPosition.nY != nCursorY)
								{
									// Y-Position wurde ge�ndert -> ScrollY
									pPlot->Intern.TouchAction.LastScrollDifference.nY = pPlot->Intern.TouchAction.LastScrollPosition.nY - nCursorY;
									REAL rScrollY = (REAL)pPlot->Intern.TouchAction.LastScrollDifference.nY / pPlot->Intern.ScaleY.rValueScaleFactor;
									pPlot->Cfg.ScaleY.rMin -= rScrollY;
									pPlot->Cfg.ScaleY.rMax -= rScrollY;
									if(abs(pPlot->Intern.TouchAction.LastScrollDifference.nY) > 2.0)
									{
										pPlot->Intern.TouchAction.fbZoomDragDelay.IN = 0; // Verz�gerung stoppen und damit Zoom nicht starten
									}
									pPlot->State.eTouchAction = eBRB_PLOT_TOUCHACTION_SCROLL;
								}
								pPlot->Intern.ZoomWindow.bShow = 0;
							}
							else if((pPlot->Cfg.TouchAction.bZoomX == 1 || pPlot->Cfg.TouchAction.bZoomY == 1) && (pPlot->Intern.TouchAction.fbZoomDragDelay.IN == 1 || (pPlot->Cfg.TouchAction.bScrollX == 0 && pPlot->Cfg.TouchAction.bScrollY == 0)))
							{
								// Zoom
								if(pPlot->Intern.TouchAction.fbZoomDragDelay.Q == 1 || (pPlot->Cfg.TouchAction.bScrollX == 0 && pPlot->Cfg.TouchAction.bScrollY == 0))
								{
									// Zoom-Fenster anzeigen
									pPlot->Intern.ZoomWindow.bShow = 1;
									pPlot->Intern.ZoomWindow.RectangleTouch.nWidth = nCursorX + pPlot->Intern.CurveArea.nLeft - pPlot->Intern.ZoomWindow.RectangleTouch.nLeft;
									pPlot->Intern.ZoomWindow.RectangleTouch.nHeight = nCursorY + pPlot->Intern.CurveArea.nTop - pPlot->Intern.ZoomWindow.RectangleTouch.nTop;
									pPlot->State.eTouchAction = eBRB_PLOT_TOUCHACTION_ZOOM_DRAG;
								}
							}
							pPlot->Intern.TouchAction.LastScrollPosition.nX = nCursorX;
							pPlot->Intern.TouchAction.LastScrollPosition.nY = nCursorY;
						}
					}
					else if(pGeneral->Touch.eStateSync == eBRB_TOUCH_STATE_UNPUSHED_EDGE)
					{
						// Losgelassen
						if(bMouseWithinDrawboxY == 1)
						{
							// Innerhalb des Kurvenbereichs
							if(pPlot->Intern.TouchAction.fbZoomDragDelay.Q == 1)
							{
								// X-Drag p�fen
								if(pPlot->Cfg.TouchAction.bZoomX == 1)
								{
									if(abs(pPlot->Intern.ZoomWindow.RectangleTouch.nWidth) > 10)
									{
										DINT nLeft = pPlot->Intern.ZoomWindow.RectangleTouch.nLeft;
										DINT nRight = pPlot->Intern.ZoomWindow.RectangleTouch.nLeft + pPlot->Intern.ZoomWindow.RectangleTouch.nWidth;
										if(nRight < nLeft)
										{
											DINT nTemp = nRight;
											nRight = nLeft;
											nLeft = nTemp;
										}
										REAL rScaleMin = ((nLeft - pPlot->Intern.CurveArea.nLeft) / pPlot->Intern.ScaleX.rValueScaleFactor) + pPlot->Cfg.ScaleX.rMin;
										REAL rScaleMax = ((nRight - pPlot->Intern.CurveArea.nLeft) / pPlot->Intern.ScaleX.rValueScaleFactor) + pPlot->Cfg.ScaleX.rMin;
										pPlot->Cfg.ScaleX.rMin = rScaleMin;
										pPlot->Cfg.ScaleX.rMax = rScaleMax;
									}
								}
								// Y-Drag p�fen
								if(pPlot->Cfg.TouchAction.bZoomY == 1)
								{
									if(abs(pPlot->Intern.ZoomWindow.RectangleTouch.nHeight) > 10)
									{
										DINT nTop = pPlot->Intern.ZoomWindow.RectangleTouch.nTop;
										DINT nBottom = pPlot->Intern.ZoomWindow.RectangleTouch.nTop + pPlot->Intern.ZoomWindow.RectangleTouch.nHeight;
										if(nBottom < nTop)
										{
											DINT nTemp = nBottom;
											nBottom = nTop;
											nTop = nTemp;
										}
										REAL rScaleMin = ((pPlot->Intern.CurveArea.nTop + pPlot->Intern.CurveArea.nHeight - nBottom) / pPlot->Intern.ScaleY.rValueScaleFactor) + pPlot->Cfg.ScaleY.rMin;
										REAL rScaleMax = ((pPlot->Intern.CurveArea.nTop + pPlot->Intern.CurveArea.nHeight - nTop) / pPlot->Intern.ScaleY.rValueScaleFactor) + pPlot->Cfg.ScaleY.rMin;
										pPlot->Cfg.ScaleY.rMin = rScaleMin;
										pPlot->Cfg.ScaleY.rMax = rScaleMax;
									}
								}
							}
						}
						pPlot->Intern.TouchAction.fbZoomDragDelay.IN = 0;
						pPlot->Intern.ZoomWindow.bShow = 0;
					}
				}
			}
		}
	}
	pPlot->Intern.TouchAction.fbZoomDragDelay.PT = 1000;
	TON(&pPlot->Intern.TouchAction.fbZoomDragDelay);
	return nResult; 
}

/* Gibt einen X-Quell-Wert zur�ck */
REAL GetPlotValueX(BrbVc4DrawPlot_TYP* pPlot, DINT nValueIndex)
{
	REAL rValue = 0.0;
	if(pPlot->Cfg.Source.eValueSource == eBRB_PLOT_SOURCE_SINGLE_ARRAY)
	{
		REAL* pValue = (REAL*)pPlot->Cfg.Source.pArrayX + nValueIndex;
		rValue = *pValue;
	}
	else if(pPlot->Cfg.Source.eValueSource == eBRB_PLOT_SOURCE_STRUCT_ARRAY)
	{
		UDINT pArrayMember = pPlot->Cfg.Source.pArrayStruct + (pPlot->Cfg.Source.nStructSize * nValueIndex);
		UDINT pStructMember = pArrayMember + pPlot->Cfg.Source.nStructMemberOffsetX;
		REAL* pValue = (REAL*)pStructMember;
		rValue = *pValue;
	}
	return rValue;
}

/* Gibt einen Y-Quell-Wert zur�ck */
REAL GetPlotValueY(BrbVc4DrawPlot_TYP* pPlot, DINT nValueIndex)
{
	REAL rValue = 0.0;
	if(pPlot->Cfg.Source.eValueSource == eBRB_PLOT_SOURCE_SINGLE_ARRAY)
	{
		REAL* pValue = (REAL*)pPlot->Cfg.Source.pArrayY + nValueIndex;
		rValue = *pValue;
	}
	else if(pPlot->Cfg.Source.eValueSource == eBRB_PLOT_SOURCE_STRUCT_ARRAY)
	{
		UDINT pArrayMember = pPlot->Cfg.Source.pArrayStruct + (pPlot->Cfg.Source.nStructSize * nValueIndex);
		UDINT pStructMember = pArrayMember + pPlot->Cfg.Source.nStructMemberOffsetY;
		REAL* pValue = (REAL*)pStructMember;
		rValue = *pValue;
	}
	return rValue;
}

/* Gibt die Distanz zwischen zwei Punkten zur�ck */
REAL GetDistance(DINT nX1, DINT nY1, DINT nX2, DINT nY2)
{
	return sqrt(pow((REAL)(nX1-nX2), 2) + pow((REAL)(nY1-nY2), 2) );
}
