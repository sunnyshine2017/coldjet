/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawTrend.c
 * Author: niedermeierr
 * Created: December 11, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>
#include <math.h>

#ifdef __cplusplus
	};
#endif

void LimitScrollOffset(struct BrbVc4DrawTrend_TYP* pTrend);
REAL GetTrendValue(BrbVc4DrawTrendCfgCurve_TYP* pCfgCurve, DINT nValueIndex);

/* Zeichnet einen Trend */
unsigned short BrbVc4DrawTrend(struct BrbVc4DrawTrend_TYP* pTrend, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	// Das Zeichnen muss synchronisiert sein
	if(pTrend->bEnable == 1)
	{
		if(pGeneral->nVcHandle != 0)
		{
			LimitScrollOffset(pTrend); // Begrenzen des ScrollOffsets
			if(pGeneral->nRedrawCounter == pTrend->nRedrawCounterMatch)
			{
				// Zugriff reservieren
				pTrend->Intern.nAccessStatus = VA_Saccess(1, pGeneral->nVcHandle);
				if(pTrend->Intern.nAccessStatus == 0)
				{
					// Drawbox verbinden
					pTrend->Intern.nAttachStatus = VA_Attach(1, pGeneral->nVcHandle, 0, (UDINT)&pTrend->Cfg.Drawbox.sFullName);
					if(pTrend->Intern.nAttachStatus == 0)
					{
						// L�schen + Kurven-Bereich
						if(1 == 1)
						{
							// Bisherige Zeichnung l�schen
							BrbVc4Rectangle_TYP RectBackground;
							RectBackground.nLeft = 0;
							RectBackground.nTop = 0;
							RectBackground.nWidth = pTrend->Cfg.Drawbox.nWidth;
							RectBackground.nHeight = pTrend->Cfg.Drawbox.nHeight;
							RectBackground.nFillColor= pTrend->Cfg.Drawbox.nBackgroundColor;
							RectBackground.nBorderColor = pTrend->Cfg.Drawbox.nBackgroundColor;
							RectBackground.nDashWidth = 0;
							BrbVc4DrawRectangle(&RectBackground, pGeneral);
							if(pTrend->Cfg.Callbacks.pCallbackAfterClear != 0)
							{
								// Funktion hinterlegt -> Funktionspointer casten
								UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterClear;
									// Funktion aufrufen
									(*pFpFunction)(pTrend);
							}
							// Kurvenbereich
							pTrend->Intern.CurveArea.nLeft = pTrend->Cfg.Padding.nLeft;
							pTrend->Intern.CurveArea.nTop = pTrend->Cfg.Padding.nTop;
							pTrend->Intern.CurveArea.nWidth = pTrend->Cfg.Drawbox.nWidth - pTrend->Cfg.Padding.nLeft - pTrend->Cfg.Padding.nRight;
							if(pTrend->Intern.CurveArea.nWidth < 10)
							{
								pTrend->Intern.CurveArea.nWidth = 10;
							}
							pTrend->Intern.CurveArea.nHeight = pTrend->Cfg.Drawbox.nHeight - pTrend->Cfg.Padding.nTop - pTrend->Cfg.Padding.nBottom;
							if(pTrend->Intern.CurveArea.nHeight < 10)
							{
								pTrend->Intern.CurveArea.nHeight = 10;
							}
							pTrend->Intern.CurveArea.nFillColor = pTrend->Cfg.nCurveAreaColor;
							pTrend->Intern.CurveArea.nBorderColor = pTrend->Cfg.nCurveAreaColor;
							pTrend->Intern.CurveArea.nDashWidth = 0;
							BrbVc4DrawRectangle(&pTrend->Intern.CurveArea, pGeneral);
							if(pTrend->Cfg.Callbacks.pCallbackAfterCurveArea != 0)
							{
								// Funktion hinterlegt -> Funktionspointer casten
								UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterCurveArea;
									// Funktion aufrufen
									(*pFpFunction)(pTrend);
							}
						}
						// Skalierung Y links
						if(1 == 1)
						{
							BrbVc4TrendScaleYIndex_ENUM eScaleIndex = eBRB_TREND_SCALE_Y_INDEX_LEFT;
							BrbVc4DrawTrendCfgScaleY_TYP* pCfgScale = &pTrend->Cfg.ScaleY[eScaleIndex];
							BrbVc4DrawTrendInternScaleY_TYP* pInternScale = &pTrend->Intern.ScaleY[eScaleIndex];
							if(pCfgScale->rMax <= pCfgScale->rMin)
							{
								pCfgScale->rMax = pCfgScale->rMin + 1.0;
							}
							pInternScale->rValueScaleFactor = (REAL)pTrend->Intern.CurveArea.nHeight / (pCfgScale->rMax - pCfgScale->rMin);
							if(pCfgScale->bShow == 1)
							{
								// Skalierung zeichnen
								pInternScale->ScaleLine.nLeft = pTrend->Intern.CurveArea.nLeft;
								pInternScale->ScaleLine.nTop = pTrend->Intern.CurveArea.nTop;
								pInternScale->ScaleLine.nRight = pInternScale->ScaleLine.nLeft;
								pInternScale->ScaleLine.nBottom = pTrend->Intern.CurveArea.nTop + pTrend->Intern.CurveArea.nHeight;
								pInternScale->ScaleLine.nColor = pCfgScale->nColor;
								pInternScale->ScaleLine.nDashWidth = 0;
								BrbVc4DrawLine(&pInternScale->ScaleLine, pGeneral);
								// Einheiten-Text zeichnen
								BrbVc4DrawText_TYP UnitText;
								strcpy(UnitText.sText, pCfgScale->sUnit);
								UnitText.nLeft = pInternScale->ScaleLine.nLeft - (DINT)((REAL)strlen(UnitText.sText) / 2.0 * pTrend->Cfg.ScaleFont.rCharWidth);
								UnitText.nTop = pInternScale->ScaleLine.nTop - (DINT)(pTrend->Cfg.ScaleFont.rCharHeight * 1.0) - 4;
								UnitText.nFontIndex = pTrend->Cfg.ScaleFont.nIndex;
								UnitText.nTextColor = pCfgScale->nColor;
								UnitText.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
								BrbVc4DrawText(&UnitText, pGeneral);
								// Skalierungs-Striche
								UINT nLineIndexY = 0;
								pTrend->Intern.ScaleY[eScaleIndex].rScaleDistance = (pCfgScale->rMax - pCfgScale->rMin) / pCfgScale->nLinesCount;
								for(nLineIndexY=0; nLineIndexY<=pCfgScale->nLinesCount; nLineIndexY++)
								{
									REAL rScaleValue = pCfgScale->rMin + pTrend->Intern.ScaleY[eScaleIndex].rScaleDistance * nLineIndexY;
									BrbVc4Line_TYP ScaleLine;
									ScaleLine.nLeft = pTrend->Intern.CurveArea.nLeft - pCfgScale->nLineLength / 2.0;
									ScaleLine.nTop = BrbVc4GetTrendDisplayCoordinateY(pTrend, eScaleIndex, rScaleValue);
									ScaleLine.nRight = ScaleLine.nLeft + pCfgScale->nLineLength;
									ScaleLine.nBottom = ScaleLine.nTop;
									ScaleLine.nColor = pCfgScale->nColor;
									ScaleLine.nDashWidth = 0;
									if(pCfgScale->Grid.bShow == 1)
									{
										BrbVc4Line_TYP GridLine;
										GridLine.nLeft = pTrend->Intern.CurveArea.nLeft;
										GridLine.nTop = ScaleLine.nTop;
										GridLine.nRight = pTrend->Intern.CurveArea.nLeft + pTrend->Intern.CurveArea.nWidth - 1;
										GridLine.nBottom = ScaleLine.nTop;
										GridLine.nColor = pCfgScale->Grid.nColor;
										GridLine.nDashWidth = 0;
										BrbVc4DrawLine(&GridLine, pGeneral);
									}								
									BrbVc4DrawLine(&ScaleLine, pGeneral);
									BrbVc4DrawText_TYP ScaleTextY;
									brsftoa(rScaleValue, (UDINT)&ScaleTextY.sText);
									BrbStringFormatFractionDigits(ScaleTextY.sText, sizeof(ScaleTextY.sText), pCfgScale->nFractionDigits);
									if(rScaleValue >= 0.0)
									{
										BrbStringInsert(ScaleTextY.sText, 0, "+");
									}
									ScaleTextY.nLeft = ScaleLine.nLeft - 8 - (DINT)((REAL)strlen(ScaleTextY.sText) * pTrend->Cfg.ScaleFont.rCharWidth);
									ScaleTextY.nTop = ScaleLine.nTop - (DINT)(pTrend->Cfg.ScaleFont.rCharHeight / 2.0);
									ScaleTextY.nFontIndex = pTrend->Cfg.ScaleFont.nIndex;
									ScaleTextY.nTextColor = pCfgScale->nColor;
									ScaleTextY.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
									BrbVc4DrawText(&ScaleTextY, pGeneral);
									if(pTrend->Cfg.Callbacks.pCallbackAfterScaleLineY != 0)
									{
										// Funktion hinterlegt -> Funktionspointer casten
										UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*, BrbVc4TrendScaleYIndex_ENUM, UINT, BrbVc4Line_TYP*, BrbVc4DrawText_TYP*, REAL) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterScaleLineY;
										// Funktion aufrufen
										(*pFpFunction)(pTrend, eBRB_TREND_SCALE_Y_INDEX_LEFT, nLineIndexY, &ScaleLine, &ScaleTextY, rScaleValue);
									}
								}
								if(pTrend->Cfg.Callbacks.pCallbackAfterScaleY != 0)
								{
									// Funktion hinterlegt -> Funktionspointer casten
									UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*, BrbVc4TrendScaleYIndex_ENUM) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterScaleY;
										// Funktion aufrufen
										(*pFpFunction)(pTrend, eBRB_TREND_SCALE_Y_INDEX_LEFT);
								}
							}
						}
						// Skalierung Y rechts
						if(1 == 1)
						{
							BrbVc4TrendScaleYIndex_ENUM eScaleIndex = eBRB_TREND_SCALE_Y_INDEX_RIGHT;
							BrbVc4DrawTrendCfgScaleY_TYP* pCfgScale = &pTrend->Cfg.ScaleY[eScaleIndex];
							BrbVc4DrawTrendInternScaleY_TYP* pInternScale = &pTrend->Intern.ScaleY[eScaleIndex];
							if(pCfgScale->rMax <= pCfgScale->rMin)
							{
								pCfgScale->rMax = pCfgScale->rMin + 1.0;
							}
							pInternScale->rValueScaleFactor = (REAL)pTrend->Intern.CurveArea.nHeight / (pCfgScale->rMax - pCfgScale->rMin);
							if(pCfgScale->bShow == 1)
							{
								// Skalierung zeichnen
								pInternScale->ScaleLine.nLeft = pTrend->Intern.CurveArea.nLeft + pTrend->Intern.CurveArea.nWidth;
								pInternScale->ScaleLine.nTop = pTrend->Intern.CurveArea.nTop;
								pInternScale->ScaleLine.nRight = pInternScale->ScaleLine.nLeft;
								pInternScale->ScaleLine.nBottom = pTrend->Intern.CurveArea.nTop + pTrend->Intern.CurveArea.nHeight;
								pInternScale->ScaleLine.nColor = pCfgScale->nColor;
								pInternScale->ScaleLine.nDashWidth = 0;
								BrbVc4DrawLine(&pInternScale->ScaleLine, pGeneral);
								// Einheiten-Text zeichnen
								BrbVc4DrawText_TYP UnitText;
								strcpy(UnitText.sText, pCfgScale->sUnit);
								UnitText.nLeft = pInternScale->ScaleLine.nLeft - (DINT)((REAL)strlen(UnitText.sText) / 2.0 * pTrend->Cfg.ScaleFont.rCharWidth);
								UnitText.nTop = pInternScale->ScaleLine.nTop - (DINT)(pTrend->Cfg.ScaleFont.rCharHeight * 1.0) - 4;
								UnitText.nFontIndex = pTrend->Cfg.ScaleFont.nIndex;
								UnitText.nTextColor = pCfgScale->nColor;
								UnitText.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
								BrbVc4DrawText(&UnitText, pGeneral);
								// Skalierungs-Striche
								UINT nLineIndexY = 0;
								pTrend->Intern.ScaleY[eScaleIndex].rScaleDistance = (pCfgScale->rMax - pCfgScale->rMin) / pCfgScale->nLinesCount;
								for(nLineIndexY=0; nLineIndexY<=pCfgScale->nLinesCount; nLineIndexY++)
								{
									REAL rScaleValue = pCfgScale->rMin + pTrend->Intern.ScaleY[eScaleIndex].rScaleDistance * nLineIndexY;
									BrbVc4Line_TYP ScaleLine;
									ScaleLine.nLeft = pTrend->Intern.CurveArea.nLeft + pTrend->Intern.CurveArea.nWidth - pCfgScale->nLineLength / 2.0;
									ScaleLine.nTop = BrbVc4GetTrendDisplayCoordinateY(pTrend, eScaleIndex, rScaleValue);
									ScaleLine.nRight = ScaleLine.nLeft + pCfgScale->nLineLength;
									ScaleLine.nBottom = ScaleLine.nTop;
									ScaleLine.nColor = pCfgScale->nColor;
									ScaleLine.nDashWidth = 0;
									if(pCfgScale->Grid.bShow == 1 && nLineIndexY != 0)
									{
										BrbVc4Line_TYP GridLine;
										GridLine.nLeft = pTrend->Intern.CurveArea.nLeft;
										GridLine.nTop = ScaleLine.nTop;
										GridLine.nRight = pTrend->Intern.CurveArea.nLeft + pTrend->Intern.CurveArea.nWidth - 1;
										GridLine.nBottom = ScaleLine.nTop;
										GridLine.nColor = pCfgScale->Grid.nColor;
										GridLine.nDashWidth = 0;
										BrbVc4DrawLine(&GridLine, pGeneral);
									}								
									BrbVc4DrawLine(&ScaleLine, pGeneral);
									BrbVc4DrawText_TYP ScaleTextY;
									brsftoa(rScaleValue, (UDINT)&ScaleTextY.sText);
									BrbStringFormatFractionDigits(ScaleTextY.sText, sizeof(ScaleTextY.sText), pCfgScale->nFractionDigits);
									if(rScaleValue >= 0.0)
									{
										BrbStringInsert(ScaleTextY.sText, 0, "+");
									}
									ScaleTextY.nLeft = ScaleLine.nRight + 4;
									ScaleTextY.nTop = ScaleLine.nTop - (DINT)(pTrend->Cfg.ScaleFont.rCharHeight / 2.0);
									ScaleTextY.nFontIndex = pTrend->Cfg.ScaleFont.nIndex;
									ScaleTextY.nTextColor = pCfgScale->nColor;
									ScaleTextY.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
									BrbVc4DrawText(&ScaleTextY, pGeneral);
									if(pTrend->Cfg.Callbacks.pCallbackAfterScaleLineY != 0)
									{
										// Funktion hinterlegt -> Funktionspointer casten
										UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*, BrbVc4TrendScaleYIndex_ENUM, UINT, BrbVc4Line_TYP*, BrbVc4DrawText_TYP*, REAL) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterScaleLineY;
										// Funktion aufrufen
										(*pFpFunction)(pTrend, eBRB_TREND_SCALE_Y_INDEX_RIGHT, nLineIndexY, &ScaleLine, &ScaleTextY, rScaleValue);
									}
								}
								if(pTrend->Cfg.Callbacks.pCallbackAfterScaleY != 0)
								{
									// Funktion hinterlegt -> Funktionspointer casten
									UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*, BrbVc4TrendScaleYIndex_ENUM) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterScaleY;
										// Funktion aufrufen
										(*pFpFunction)(pTrend, eBRB_TREND_SCALE_Y_INDEX_RIGHT);
								}
							}
						}
						// Skalierung X-Achse
						if(1 == 1)
						{
							// Der Zoom kann durch den Eingang nZoomValueCount (= Anzahl der anzuzeigenden Samplewerte auf die ganze Trendbreite) ver�ndert werden	
							if(pTrend->Cfg.ScaleX.nZoomValueCount < 1)
							{
								// Anzahl der anzuzeigenden Werte begrenzen
								pTrend->Cfg.ScaleX.nZoomValueCount = 1;
							}
							LimitScrollOffset(pTrend); // Begrenzen des ScrollOffsets
							pTrend->Intern.ScaleX.rZoomScaleFactor = (REAL)pTrend->Intern.CurveArea.nWidth / (REAL)pTrend->Cfg.ScaleX.nZoomValueCount;
							if(pTrend->Cfg.ScaleX.bShow == 1)
							{
								// Skalierung zeichnen
								pTrend->Intern.ScaleX.ScaleLine.nLeft = pTrend->Intern.CurveArea.nLeft;
								pTrend->Intern.ScaleX.ScaleLine.nTop = pTrend->Intern.CurveArea.nTop + pTrend->Intern.CurveArea.nHeight;
								pTrend->Intern.ScaleX.ScaleLine.nRight = pTrend->Intern.CurveArea.nLeft + pTrend->Intern.CurveArea.nWidth;
								pTrend->Intern.ScaleX.ScaleLine.nBottom = pTrend->Intern.ScaleX.ScaleLine.nTop;
								pTrend->Intern.ScaleX.ScaleLine.nColor = pTrend->Cfg.ScaleX.nColor;
								pTrend->Intern.ScaleX.ScaleLine.nDashWidth = 0;
								BrbVc4DrawLine(&pTrend->Intern.ScaleX.ScaleLine, pGeneral);
								// Skalierungs-Striche
								UINT nLineIndexX = 0;
								pTrend->Intern.ScaleX.rScaleDistance = (pTrend->Intern.CurveArea.nWidth) / (REAL)pTrend->Cfg.ScaleX.nLinesCount;
								for(nLineIndexX=0; nLineIndexX<=pTrend->Cfg.ScaleX.nLinesCount; nLineIndexX++)
								{
									REAL rScaleValue = pTrend->Intern.ScaleX.rScaleDistance * nLineIndexX;
									BrbVc4Line_TYP ScaleLine;
									ScaleLine.nLeft = pTrend->Intern.CurveArea.nLeft + (DINT)rScaleValue;
									ScaleLine.nTop = 	pTrend->Intern.ScaleX.ScaleLine.nTop - pTrend->Cfg.ScaleX.nLineLength / 2.0;
									ScaleLine.nRight = ScaleLine.nLeft;
									ScaleLine.nBottom = ScaleLine.nTop + pTrend->Cfg.ScaleX.nLineLength;
									ScaleLine.nColor = pTrend->Cfg.ScaleX.nColor;
									ScaleLine.nDashWidth = 0;
									if(pTrend->Cfg.ScaleX.Grid.bShow == 1 && nLineIndexX != 0 && nLineIndexX != pTrend->Cfg.ScaleX.nLinesCount)
									{
										BrbVc4Line_TYP GridLine;
										GridLine.nLeft = ScaleLine.nLeft;
										GridLine.nTop = 	pTrend->Intern.CurveArea.nTop;
										GridLine.nRight = ScaleLine.nLeft;
										GridLine.nBottom = pTrend->Intern.CurveArea.nTop + pTrend->Intern.CurveArea.nHeight - 1;
										GridLine.nColor = pTrend->Cfg.ScaleX.Grid.nColor;
										GridLine.nDashWidth = 0;
										BrbVc4DrawLine(&GridLine, pGeneral);
									}
									BrbVc4DrawLine(&ScaleLine, pGeneral);
									BrbVc4DrawText_TYP ScaleTextX;
									DTStructure dtsTimeStamp;
									memcpy(&dtsTimeStamp, &pTrend->Cfg.ScaleX.dtsStartTime, sizeof(DTStructure));
									DINT nSampleIndex = ((UDINT)((REAL)pTrend->Cfg.ScaleX.nZoomValueCount / (REAL)pTrend->Cfg.ScaleX.nLinesCount * (REAL)nLineIndexX) + pTrend->Cfg.ScaleX.nScrollOffset);
									UDINT nMilliSeconds = nSampleIndex * pTrend->Cfg.ScaleX.nInterval;
									BrbDtStructAddMilliseconds(&dtsTimeStamp, nMilliSeconds);
									BrbGetTimeTextDtStruct(&dtsTimeStamp, ScaleTextX.sText, sizeof(ScaleTextX.sText), pTrend->Cfg.ScaleX.sFormat);
									ScaleTextX.nLeft = ScaleLine.nLeft - (DINT)((REAL)strlen(ScaleTextX.sText) * pTrend->Cfg.ScaleFont.rCharWidth / 2.0);
									ScaleTextX.nTop = ScaleLine.nBottom;
									ScaleTextX.nFontIndex = pTrend->Cfg.ScaleFont.nIndex;
									ScaleTextX.nTextColor = pTrend->Cfg.ScaleX.nColor;
									ScaleTextX.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
									BrbVc4DrawText(&ScaleTextX, pGeneral);
									if(pTrend->Cfg.Callbacks.pCallbackAfterScaleLineX != 0)
									{
										// Funktion hinterlegt -> Funktionspointer casten
										UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*, UINT, BrbVc4Line_TYP*, BrbVc4DrawText_TYP*, DINT) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterScaleLineX;
										// Funktion aufrufen
										(*pFpFunction)(pTrend, nLineIndexX, &ScaleLine, &ScaleTextX, nSampleIndex);
									}
								}
								if(pTrend->Cfg.Callbacks.pCallbackAfterScaleX != 0)
								{
									// Funktion hinterlegt -> Funktionspointer casten
									UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterScaleX;
										// Funktion aufrufen
										(*pFpFunction)(pTrend);
								}
							}
						}
						// Kurven zeichnen
						if(1 == 1)
						{
							UINT nCurveIndex = 0;
							for(nCurveIndex=0; nCurveIndex<=nBRB_TREND_CURVE_INDEX_MAX; nCurveIndex++)
							{
								if(pTrend->Cfg.Curve[nCurveIndex].rConversionFactor == 0.0)
								{
									pTrend->Cfg.Curve[nCurveIndex].rConversionFactor = 1.0;
								}
								BrbVc4DrawTrendCfgCurve_TYP* pCfgCurve = &pTrend->Cfg.Curve[nCurveIndex];
								if(pCfgCurve->bShow == 1)
								{
									if(pCfgCurve->pValueSource != 0)
									{
										if(pTrend->Cfg.nSourceArrayIndexMax >= 0)
										{
										}
										if(pTrend->Cfg.Curve[nCurveIndex].eMode == eBRB_TREND_CURVE_MODE_LINED)
										{
											BrbVc4Line_TYP CurveLine;
											CurveLine.nColor = pCfgCurve->nColor;
											CurveLine.nDashWidth = 0;
											BrbVc4Line_TYP LastCurveLine;
											LastCurveLine.nRight = BrbVc4GetTrendDisplayCoordinateX(pTrend, 0);
											LastCurveLine.nBottom = BrbVc4GetTrendDisplayCoordinateY(pTrend, pCfgCurve->eScaleY, GetTrendValue(pCfgCurve, 0));
											DINT nEqualLineCount = 0;
											DINT nSampleIndex = 0;
											for(nSampleIndex=pTrend->Cfg.ScaleX.nScrollOffset; nSampleIndex<=(pTrend->Cfg.ScaleX.nScrollOffset + (DINT)pTrend->Cfg.ScaleX.nZoomValueCount); nSampleIndex++)
											{
												if(nSampleIndex >= 0 && nSampleIndex <= pTrend->Cfg.nSourceArrayIndexMax)
												{
													REAL rValue = GetTrendValue(pCfgCurve, nSampleIndex);
													CurveLine.nLeft = LastCurveLine.nRight;
													CurveLine.nTop = LastCurveLine.nBottom;
													CurveLine.nRight = BrbVc4GetTrendDisplayCoordinateX(pTrend, nSampleIndex);
													CurveLine.nBottom = BrbVc4GetTrendDisplayCoordinateY(pTrend, pCfgCurve->eScaleY, rValue);
													if(memcmp(&LastCurveLine, &CurveLine, sizeof(BrbVc4Line_TYP)) == 0)
													{
														nEqualLineCount += 1;
													}
													else
													{
														nEqualLineCount = 0;
													}
													if(nEqualLineCount == 0)
													{
														BrbVc4DrawLineClip(&CurveLine, &pTrend->Intern.CurveArea, pGeneral);
													}
													memcpy(&LastCurveLine, &CurveLine, sizeof(BrbVc4Line_TYP));
												}	
											}
										}
										else
										{
											// eBRB_TREND_CURVE_MODE_STEPPED
											BrbVc4Line_TYP CurveLineV;
											memset(&CurveLineV, 0, sizeof(BrbVc4Line_TYP));
											CurveLineV.nColor = pCfgCurve->nColor;
											CurveLineV.nDashWidth = 0;
											BrbVc4Line_TYP CurveLineH;
											memset(&CurveLineH, 0, sizeof(BrbVc4Line_TYP));
											CurveLineH.nColor = pCfgCurve->nColor;
											CurveLineH.nDashWidth = 0;
											BrbVc4Line_TYP LastCurveLineH;
											memset(&LastCurveLineH, 0, sizeof(BrbVc4Line_TYP));
											LastCurveLineH.nLeft = BrbVc4GetTrendDisplayCoordinateX(pTrend, 0);
											LastCurveLineH.nRight = BrbVc4GetTrendDisplayCoordinateY(pTrend, pCfgCurve->eScaleY, GetTrendValue(pCfgCurve, 0));
											DINT nSampleIndex = 0;
											for(nSampleIndex=pTrend->Cfg.ScaleX.nScrollOffset; nSampleIndex<=(pTrend->Cfg.ScaleX.nScrollOffset + (DINT)pTrend->Cfg.ScaleX.nZoomValueCount - 1); nSampleIndex++)
											{
												if(nSampleIndex >= 0 && nSampleIndex <= pTrend->Cfg.nSourceArrayIndexMax)
												{
													REAL rValue = GetTrendValue(pCfgCurve, nSampleIndex);
													DINT nY = BrbVc4GetTrendDisplayCoordinateY(pTrend, pCfgCurve->eScaleY, rValue);
													CurveLineV.nLeft = CurveLineH.nRight;
													CurveLineV.nTop = CurveLineH.nBottom;
													CurveLineV.nRight = CurveLineV.nLeft;
													CurveLineV.nBottom = nY;
													if(nSampleIndex != pTrend->Cfg.ScaleX.nScrollOffset)
													{
														BrbVc4DrawLineClip(&CurveLineV, &pTrend->Intern.CurveArea, pGeneral);
													}
													CurveLineH.nLeft = BrbVc4GetTrendDisplayCoordinateX(pTrend, nSampleIndex);
													CurveLineH.nTop = nY;
													CurveLineH.nRight = BrbVc4GetTrendDisplayCoordinateX(pTrend, nSampleIndex+1);
													CurveLineH.nBottom = nY;
													BrbVc4DrawLineClip(&CurveLineH, &pTrend->Intern.CurveArea, pGeneral);
													memcpy(&LastCurveLineH, &CurveLineH, sizeof(BrbVc4Line_TYP));
												}	
											}
										}
										if(pTrend->Cfg.Callbacks.pCallbackAfterCurve != 0)
										{
											// Funktion hinterlegt -> Funktionspointer casten
											UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*, UINT) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterCurve;
											// Funktion aufrufen
											(*pFpFunction)(pTrend, nCurveIndex);
										}
									}
								}
							}
						}
						// Cursor zeichnen
						if(1 == 1)
						{
							SINT nCursorIndex = 0;
							for(nCursorIndex=1; nCursorIndex>=0; nCursorIndex--)
							{
								if(pTrend->Cfg.Cursor[nCursorIndex].bShow == 1)
								{
									pTrend->Intern.Cursor[nCursorIndex].CursorLine.nLeft = BrbVc4GetTrendDisplayCoordinateX(pTrend, pTrend->Cfg.Cursor[nCursorIndex].nSampleIndex);
									pTrend->Intern.Cursor[nCursorIndex].CursorLine.nTop = pTrend->Intern.CurveArea.nTop;
									pTrend->Intern.Cursor[nCursorIndex].CursorLine.nRight = pTrend->Intern.Cursor[nCursorIndex].CursorLine.nLeft;
									pTrend->Intern.Cursor[nCursorIndex].CursorLine.nBottom = pTrend->Intern.CurveArea.nTop + pTrend->Intern.CurveArea.nHeight;
									pTrend->Intern.Cursor[nCursorIndex].CursorLine.nColor = pTrend->Cfg.Cursor[nCursorIndex].nColor;
									pTrend->Intern.Cursor[nCursorIndex].CursorLine.nDashWidth = 0;
									BrbVc4DrawLineClip(&pTrend->Intern.Cursor[nCursorIndex].CursorLine, &pTrend->Intern.CurveArea, pGeneral);
									if(pTrend->Cfg.Callbacks.pCallbackAfterCursor != 0)
									{
										// Funktion hinterlegt -> Funktionspointer casten
										UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*, UINT) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterCursor;
										// Funktion aufrufen
										(*pFpFunction)(pTrend, (UINT)nCursorIndex);
									}
								}
							}
						}
						// Zoom-Fenster zeichnen
						if(pTrend->Intern.ZoomWindow.bShow == 1)
						{
							pTrend->Intern.ZoomWindow.RectangleDraw.nBorderColor = 164;
							pTrend->Intern.ZoomWindow.RectangleDraw.nFillColor = eBRB_COLOR_TRANSPARENT;
							pTrend->Intern.ZoomWindow.RectangleDraw.nDashWidth = 4;
							if(pTrend->Cfg.TouchAction.bZoomX == 1)
							{
								pTrend->Intern.ZoomWindow.RectangleDraw.nLeft = pTrend->Intern.ZoomWindow.RectangleTouch.nLeft;
								pTrend->Intern.ZoomWindow.RectangleDraw.nWidth = pTrend->Intern.ZoomWindow.RectangleTouch.nWidth;
							}
							else
							{
								pTrend->Intern.ZoomWindow.RectangleDraw.nLeft = pTrend->Intern.CurveArea.nLeft;
								pTrend->Intern.ZoomWindow.RectangleDraw.nWidth = pTrend->Intern.CurveArea.nWidth - 1;
							}
							if(pTrend->Cfg.TouchAction.bZoomY == 1)
							{
								pTrend->Intern.ZoomWindow.RectangleDraw.nTop = pTrend->Intern.ZoomWindow.RectangleTouch.nTop;
								pTrend->Intern.ZoomWindow.RectangleDraw.nHeight = pTrend->Intern.ZoomWindow.RectangleTouch.nHeight;
							}
							else
							{
								pTrend->Intern.ZoomWindow.RectangleDraw.nTop = pTrend->Intern.CurveArea.nTop;
								pTrend->Intern.ZoomWindow.RectangleDraw.nHeight = pTrend->Intern.CurveArea.nHeight - 1;
							}
							if(pTrend->Cfg.TouchAction.bZoomX == 1 && pTrend->Cfg.TouchAction.bZoomY == 1 && (abs(pTrend->Intern.ZoomWindow.RectangleTouch.nWidth) < 10 || abs(pTrend->Intern.ZoomWindow.RectangleTouch.nHeight) < 10))
							{
								BrbVc4Ellipse_TYP ZoomPoint;
								ZoomPoint.nLeft = pTrend->Intern.ZoomWindow.RectangleDraw.nLeft - 2;
								ZoomPoint.nTop = pTrend->Intern.ZoomWindow.RectangleDraw.nTop - 2;
								ZoomPoint.nWidth = 4;
								ZoomPoint.nHeight = 4;
								ZoomPoint.nBorderColor = 0;
								ZoomPoint.nFillColor = 0;
								ZoomPoint.nDashWidth = 0;
								BrbVc4DrawEllipse(&ZoomPoint, pGeneral);
							}
							BrbVc4DrawRectangle(&pTrend->Intern.ZoomWindow.RectangleDraw, pGeneral);
							if(pTrend->Cfg.Callbacks.pCallbackAfterZoomWindow != 0)
							{
								// Funktion hinterlegt -> Funktionspointer casten
								UINT (*pFpFunction) (BrbVc4DrawTrend_TYP*) = (void*)pTrend->Cfg.Callbacks.pCallbackAfterZoomWindow;
									// Funktion aufrufen
									(*pFpFunction)(pTrend);
							}
						}
						// Status
						if(1 == 1)
						{
							UINT nCurveIndex = 0;
							for(nCurveIndex=0; nCurveIndex<=nBRB_TREND_CURVE_INDEX_MAX; nCurveIndex++)
							{
								if(pTrend->Cfg.Curve[nCurveIndex].pValueSource != 0)
								{
									pTrend->State.Curve[nCurveIndex].rValueMin = +1234567890.0;
									pTrend->State.Curve[nCurveIndex].rValueMax = -123456789.0;
									pTrend->State.Curve[nCurveIndex].rValueAverage = 0.0;
									pTrend->Intern.Curve[nCurveIndex].rValueSum = 0.0;
									SINT nCursorIndex = 0;
									for(nCursorIndex=0; nCursorIndex<=1; nCursorIndex++)
									{
										if(pTrend->Cfg.Cursor[nCursorIndex].nSampleIndex >= 0 && pTrend->Cfg.Cursor[nCursorIndex].nSampleIndex <= pTrend->Cfg.nSourceArrayIndexMax)
										{
											pTrend->State.Curve[nCurveIndex].Cursor[nCursorIndex].rValue = GetTrendValue(&pTrend->Cfg.Curve[nCurveIndex], pTrend->Cfg.Cursor[nCursorIndex].nSampleIndex);
											DTStructure dtsTimeStamp;
											memcpy(&dtsTimeStamp, &pTrend->Cfg.ScaleX.dtsStartTime, sizeof(DTStructure));
											UDINT nMilliSeconds = pTrend->Cfg.Cursor[nCursorIndex].nSampleIndex * pTrend->Cfg.ScaleX.nInterval;
											BrbDtStructAddMilliseconds(&dtsTimeStamp, nMilliSeconds);
											memcpy(&pTrend->State.Curve[nCurveIndex].Cursor[nCursorIndex].dtsTimeStamp, &dtsTimeStamp, sizeof(DTStructure));
										}
										else
										{
											pTrend->State.Curve[nCurveIndex].Cursor[nCursorIndex].rValue = 0.0;
											memcpy(&pTrend->State.Curve[nCurveIndex].Cursor[nCursorIndex].dtsTimeStamp, &pTrend->Cfg.ScaleX.dtsStartTime, sizeof(DTStructure));
										}
									}
									if(pTrend->Cfg.Curve[nCurveIndex].bCalculateStatistics == 1)
									{
										DINT nSampleIndex = 0;
										for(nSampleIndex=0; nSampleIndex<=pTrend->Cfg.nSourceArrayIndexMax; nSampleIndex++)
										{
											REAL rValue = GetTrendValue(&pTrend->Cfg.Curve[nCurveIndex], nSampleIndex);
											if(rValue < pTrend->State.Curve[nCurveIndex].rValueMin)
											{
												pTrend->State.Curve[nCurveIndex].rValueMin = rValue;
											}
											if(rValue > pTrend->State.Curve[nCurveIndex].rValueMax)
											{
												pTrend->State.Curve[nCurveIndex].rValueMax = rValue;
											}
											pTrend->Intern.Curve[nCurveIndex].rValueSum += rValue;
										}
										pTrend->State.Curve[nCurveIndex].rValueAverage = pTrend->Intern.Curve[nCurveIndex].rValueSum / nSampleIndex;
									}
									else
									{
										pTrend->State.Curve[nCurveIndex].rValueMin = 0.0;
										pTrend->State.Curve[nCurveIndex].rValueMax = 0.0;
									}
								}
							}
						}
						// Drawbox trennen
						VA_Detach(1, pGeneral->nVcHandle);
					}
					// Zugriff freigeben
					VA_Srelease(1, pGeneral->nVcHandle);
				}
			}
			// TouchAction
			if(1 == 1)
			{
				pTrend->State.eTouchAction = eBRB_TREND_TOUCHACTION_NONE;
				pTrend->Intern.TouchAction.TouchPointDrawbox.nX = pGeneral->Touch.nX - pTrend->Cfg.Drawbox.nLeft - pTrend->Cfg.TouchAction.BorderCorrection.nX;
				pTrend->Intern.TouchAction.TouchPointDrawbox.nY = pGeneral->Touch.nY - pTrend->Cfg.Drawbox.nTop - pTrend->Cfg.TouchAction.BorderCorrection.nY;
				BOOL bMouseWithinDrawboxX = (pTrend->Intern.TouchAction.TouchPointDrawbox.nX >= 0 && pTrend->Intern.TouchAction.TouchPointDrawbox.nX <= pTrend->Cfg.Drawbox.nWidth);
				BOOL bMouseWithinDrawboxY = (pTrend->Intern.TouchAction.TouchPointDrawbox.nY >= 0 && pTrend->Intern.TouchAction.TouchPointDrawbox.nY <= pTrend->Cfg.Drawbox.nHeight);
				pTrend->Intern.TouchAction.TouchPointCurveArea.nX = pTrend->Intern.TouchAction.TouchPointDrawbox.nX - pTrend->Intern.CurveArea.nLeft;
				pTrend->Intern.TouchAction.TouchPointCurveArea.nY = pTrend->Intern.TouchAction.TouchPointDrawbox.nY - pTrend->Intern.CurveArea.nTop;
				BOOL bMouseWithinCurveAreaX = (pTrend->Intern.TouchAction.TouchPointCurveArea.nX >= 0 && pTrend->Intern.TouchAction.TouchPointCurveArea.nX <= pTrend->Intern.CurveArea.nWidth);
				BOOL bMouseWithinCurveAreaY = (pTrend->Intern.TouchAction.TouchPointCurveArea.nY >= 0 && pTrend->Intern.TouchAction.TouchPointCurveArea.nY <= pTrend->Intern.CurveArea.nHeight);
				DINT nCursorX = pTrend->Intern.TouchAction.TouchPointCurveArea.nX; // Auf CurveArea begrenzt
				if(nCursorX < 0)
				{
					nCursorX = 0;
				}
				else if(nCursorX > pTrend->Intern.CurveArea.nWidth)
				{
					nCursorX = pTrend->Intern.CurveArea.nWidth;
				}
				DINT nCursorY = pTrend->Intern.TouchAction.TouchPointCurveArea.nY; // Auf CurveArea begrenzt
				if(nCursorY < 0)
				{
					nCursorY = 0;
				}
				else if(nCursorY > pTrend->Intern.CurveArea.nHeight)
				{
					nCursorY = pTrend->Intern.CurveArea.nHeight;
				}
				if(pTrend->Cfg.TouchAction.bSetCursor0 == 1)
				{
					if(pGeneral->Touch.eStateSync == eBRB_TOUCH_STATE_PUSHED_EDGE || pGeneral->Touch.eStateSync == eBRB_TOUCH_STATE_PUSHED)
					{
						if(bMouseWithinDrawboxX == 1 && bMouseWithinDrawboxY == 1)
						{
							if(bMouseWithinCurveAreaX == 1)
							{
								// Innerhalb des Kurvenbereichs
								pTrend->Cfg.Cursor[0].nSampleIndex = (DINT)((REAL)(pTrend->Intern.TouchAction.TouchPointCurveArea.nX * pTrend->Cfg.ScaleX.nZoomValueCount) / pTrend->Intern.CurveArea.nWidth) + pTrend->Cfg.ScaleX.nScrollOffset;
							}
							else if(pTrend->Intern.TouchAction.TouchPointCurveArea.nX < 0)
							{
								// Links vom Kurvenbereich
								pTrend->Cfg.Cursor[0].nSampleIndex = pTrend->Cfg.ScaleX.nScrollOffset;
							}
							else if(pTrend->Intern.TouchAction.TouchPointCurveArea.nX > pTrend->Intern.CurveArea.nWidth)
							{
								// Rechts vom Kurvenbereich
								pTrend->Cfg.Cursor[0].nSampleIndex = pTrend->Cfg.ScaleX.nScrollOffset + pTrend->Cfg.ScaleX.nZoomValueCount - 1;
							}
							pTrend->State.eTouchAction = eBRB_TREND_TOUCHACTION_CURS_SET;
						}
					}
				}
				if(pTrend->Cfg.TouchAction.bSetCursor1 == 1)
				{
					if(pGeneral->Touch.eStateSync == eBRB_TOUCH_STATE_PUSHED_EDGE || pGeneral->Touch.eStateSync == eBRB_TOUCH_STATE_PUSHED)
					{
						if(bMouseWithinDrawboxX == 1 && bMouseWithinDrawboxY == 1)
						{
							if(bMouseWithinCurveAreaX == 1)
							{
								// Innerhalb des Kurvenbereichs
								pTrend->Cfg.Cursor[1].nSampleIndex = (DINT)((REAL)(pTrend->Intern.TouchAction.TouchPointCurveArea.nX * pTrend->Cfg.ScaleX.nZoomValueCount) / pTrend->Intern.CurveArea.nWidth) + pTrend->Cfg.ScaleX.nScrollOffset;
							}
							else if(pTrend->Intern.TouchAction.TouchPointCurveArea.nX < 0)
							{
								// Links vom Kurvenbereich
								pTrend->Cfg.Cursor[1].nSampleIndex = pTrend->Cfg.ScaleX.nScrollOffset;
							}
							else if(pTrend->Intern.TouchAction.TouchPointCurveArea.nX > pTrend->Intern.CurveArea.nWidth)
							{
								// Rechts vom Kurvenbereich
								pTrend->Cfg.Cursor[1].nSampleIndex = pTrend->Cfg.ScaleX.nScrollOffset + pTrend->Cfg.ScaleX.nZoomValueCount - 1;
							}
							pTrend->State.eTouchAction = eBRB_TREND_TOUCHACTION_CURS_SET;								
						}
					}
				}
				if(pTrend->Cfg.TouchAction.bZoomX == 1 || pTrend->Cfg.TouchAction.bZoomY == 1 || pTrend->Cfg.TouchAction.bScrollX == 1  || pTrend->Cfg.TouchAction.bScrollY == 1)
				{
					if(pGeneral->Touch.eStateSync == eBRB_TOUCH_STATE_PUSHED_EDGE)
					{
						// Gedr�ckt
						if(bMouseWithinCurveAreaY == 1)
						{
							// Innerhalb des Kurvenbereichs
							if(pTrend->Cfg.TouchAction.bZoomX == 1 || pTrend->Cfg.TouchAction.bZoomY == 1)
							{
								pTrend->Intern.TouchAction.fbZoomDragDelay.IN = 1; // Verz�gerung starten
								pTrend->Intern.ZoomWindow.RectangleTouch.nLeft = nCursorX + pTrend->Intern.CurveArea.nLeft;
								pTrend->Intern.ZoomWindow.RectangleTouch.nWidth = 0;
								pTrend->Intern.ZoomWindow.RectangleTouch.nTop = nCursorY + pTrend->Intern.CurveArea.nTop;
								pTrend->Intern.ZoomWindow.RectangleTouch.nHeight = 0;			
							}
							pTrend->Intern.TouchAction.LastScrollPosition.nX = nCursorX;
							pTrend->Intern.TouchAction.LastScrollPosition.nY = nCursorY;
						}
					}
					else if(pGeneral->Touch.eStateSync == eBRB_TOUCH_STATE_PUSHED)
					{
						// Gehalten
						if(bMouseWithinDrawboxX == 1 && bMouseWithinDrawboxY == 1)
						{
							// Innerhalb des Kurvenbereichs
							if((pTrend->Cfg.TouchAction.bScrollX == 1 || pTrend->Cfg.TouchAction.bScrollY == 1) && (pTrend->Intern.TouchAction.fbZoomDragDelay.Q == 0 || (pTrend->Cfg.TouchAction.bZoomX == 0 && pTrend->Cfg.TouchAction.bZoomY == 0)))
							{
								// Scroll
								if(pTrend->Cfg.TouchAction.bScrollX == 1 && pTrend->Intern.TouchAction.LastScrollPosition.nX != nCursorX)
								{
									// X-Position wurde ge�ndert -> ScrollX
									pTrend->Intern.TouchAction.LastScrollDifference.nX = pTrend->Intern.TouchAction.LastScrollPosition.nX - nCursorX;
									REAL rScrollX = (REAL)pTrend->Intern.TouchAction.LastScrollDifference.nX / pTrend->Intern.ScaleX.rZoomScaleFactor;
									if(rScrollX > 0.0 && rScrollX < 1.0)
									{
										rScrollX = 1.0;
									}
									else if(rScrollX < 0.0 && rScrollX > -1.0)
									{
										rScrollX = -1.0;
									}
									pTrend->Cfg.ScaleX.nScrollOffset += (DINT)rScrollX;
									if(abs(pTrend->Intern.TouchAction.LastScrollDifference.nX) > 2)
									{
										pTrend->Intern.TouchAction.fbZoomDragDelay.IN = 0; // Verz�gerung stoppen und damit Zoom nicht starten
									}
									pTrend->State.eTouchAction = eBRB_TREND_TOUCHACTION_SCROLL;
								}
								if(pTrend->Cfg.TouchAction.bScrollY == 1 && pTrend->Intern.TouchAction.LastScrollPosition.nY != nCursorY)
								{
									// Y-Position wurde ge�ndert -> ScrollY
									pTrend->Intern.TouchAction.LastScrollDifference.nY = pTrend->Intern.TouchAction.LastScrollPosition.nY - nCursorY;
									DINT eScaleIndex = 0;
									for(eScaleIndex=eBRB_TREND_SCALE_Y_INDEX_LEFT; eScaleIndex<=eBRB_TREND_SCALE_Y_INDEX_RIGHT; eScaleIndex++)
									{
										REAL rScrollY = (REAL)pTrend->Intern.TouchAction.LastScrollDifference.nY / pTrend->Intern.ScaleY[eScaleIndex].rValueScaleFactor;
										pTrend->Cfg.ScaleY[eScaleIndex].rMin -= rScrollY;
										pTrend->Cfg.ScaleY[eScaleIndex].rMax -= rScrollY;
									}
									if(abs(pTrend->Intern.TouchAction.LastScrollDifference.nY) > 2.0)
									{
										pTrend->Intern.TouchAction.fbZoomDragDelay.IN = 0; // Verz�gerung stoppen und damit Zoom nicht starten
									}
									pTrend->State.eTouchAction = eBRB_TREND_TOUCHACTION_SCROLL;
								}
								pTrend->Intern.ZoomWindow.bShow = 0;
							}
							else if((pTrend->Cfg.TouchAction.bZoomX == 1 || pTrend->Cfg.TouchAction.bZoomY == 1) && (pTrend->Intern.TouchAction.fbZoomDragDelay.IN == 1 || (pTrend->Cfg.TouchAction.bScrollX == 0 && pTrend->Cfg.TouchAction.bScrollY == 0)))
							{
								// Zoom
								if(pTrend->Intern.TouchAction.fbZoomDragDelay.Q == 1 || (pTrend->Cfg.TouchAction.bScrollX == 0 && pTrend->Cfg.TouchAction.bScrollY == 0))
								{
									// Zoom-Fenster anzeigen
									pTrend->Intern.ZoomWindow.bShow = 1;
									pTrend->Intern.ZoomWindow.RectangleTouch.nWidth = nCursorX + pTrend->Intern.CurveArea.nLeft - pTrend->Intern.ZoomWindow.RectangleTouch.nLeft;
									pTrend->Intern.ZoomWindow.RectangleTouch.nHeight = nCursorY + pTrend->Intern.CurveArea.nTop - pTrend->Intern.ZoomWindow.RectangleTouch.nTop;
									pTrend->State.eTouchAction = eBRB_TREND_TOUCHACTION_ZOOM_DRAG;
								}
							}
							pTrend->Intern.TouchAction.LastScrollPosition.nX = nCursorX;
							pTrend->Intern.TouchAction.LastScrollPosition.nY = nCursorY;
						}
					}
					else if(pGeneral->Touch.eStateSync == eBRB_TOUCH_STATE_UNPUSHED_EDGE)
					{
						// Losgelassen
						if(bMouseWithinDrawboxX == 1 && bMouseWithinDrawboxY == 1)
						{
							// Innerhalb des Kurvenbereichs
							if(pTrend->Intern.TouchAction.fbZoomDragDelay.Q == 1)
							{
								// X-Drag p�fen
								if(pTrend->Cfg.TouchAction.bZoomX == 1)
								{
									if(abs(pTrend->Intern.ZoomWindow.RectangleTouch.nWidth) > 10)
									{
										DINT nStartX = pTrend->Intern.ZoomWindow.RectangleDraw.nLeft;
										DINT nEndX = pTrend->Intern.ZoomWindow.RectangleDraw.nLeft + pTrend->Intern.ZoomWindow.RectangleDraw.nWidth;
										DINT nSampleIndexStart = ((nStartX - pTrend->Intern.CurveArea.nLeft) * pTrend->Cfg.ScaleX.nZoomValueCount / pTrend->Intern.CurveArea.nWidth) + pTrend->Cfg.ScaleX.nScrollOffset;
										DINT nSampleIndexEnd = ((nEndX - pTrend->Intern.CurveArea.nLeft) * pTrend->Cfg.ScaleX.nZoomValueCount / pTrend->Intern.CurveArea.nWidth) + pTrend->Cfg.ScaleX.nScrollOffset;
										DINT nZoomWidth = nSampleIndexEnd - nSampleIndexStart;
										DINT nScrollOffset = nSampleIndexStart;
										if(nSampleIndexEnd <= nSampleIndexStart)
										{
											nZoomWidth = nSampleIndexStart - nSampleIndexEnd;
											nScrollOffset = nSampleIndexEnd;
										}
										if(nZoomWidth > 0)
										{
											pTrend->Cfg.ScaleX.nScrollOffset = nScrollOffset;
											pTrend->Cfg.ScaleX.nZoomValueCount = nZoomWidth;
											pTrend->State.eTouchAction = eBRB_TREND_TOUCHACTION_ZOOM_SET;
										}
									}
								}
								// Y-Drag p�fen
								if(pTrend->Cfg.TouchAction.bZoomY == 1)
								{
									if(abs(pTrend->Intern.ZoomWindow.RectangleTouch.nHeight) > 10)
									{
										DINT eScaleIndex = 0;
										for(eScaleIndex=eBRB_TREND_SCALE_Y_INDEX_LEFT; eScaleIndex<=eBRB_TREND_SCALE_Y_INDEX_RIGHT; eScaleIndex++)
										{
											DINT nTop = pTrend->Intern.ZoomWindow.RectangleTouch.nTop;
											DINT nBottom = pTrend->Intern.ZoomWindow.RectangleTouch.nTop + pTrend->Intern.ZoomWindow.RectangleTouch.nHeight;
											if(nBottom < nTop)
											{
												DINT nTemp = nBottom;
												nBottom = nTop;
												nTop = nTemp;
											}
											REAL rScaleMin = ((pTrend->Intern.CurveArea.nTop + pTrend->Intern.CurveArea.nHeight - nBottom) / pTrend->Intern.ScaleY[eScaleIndex].rValueScaleFactor) + pTrend->Cfg.ScaleY[eScaleIndex].rMin;
											REAL rScaleMax = ((pTrend->Intern.CurveArea.nTop + pTrend->Intern.CurveArea.nHeight - nTop) / pTrend->Intern.ScaleY[eScaleIndex].rValueScaleFactor) + pTrend->Cfg.ScaleY[eScaleIndex].rMin;
											pTrend->Cfg.ScaleY[eScaleIndex].rMin = rScaleMin;
											pTrend->Cfg.ScaleY[eScaleIndex].rMax = rScaleMax;
											pTrend->State.eTouchAction = eBRB_TREND_TOUCHACTION_ZOOM_SET;
										}
									}
								}
							}
						}
						pTrend->Intern.TouchAction.fbZoomDragDelay.IN = 0;
						pTrend->Intern.ZoomWindow.bShow = 0;
					}
				}
				LimitScrollOffset(pTrend); // Begrenzen des ScrollOffsets wegen Ausgang
			}
		}
	}
	pTrend->Intern.TouchAction.fbZoomDragDelay.PT = 1000;
	TON(&pTrend->Intern.TouchAction.fbZoomDragDelay);
	return nResult; 
}

/* Begrenzt den ScrollOffset */
void LimitScrollOffset(struct BrbVc4DrawTrend_TYP* pTrend)
{
	pTrend->Intern.ScaleX.nScrollOffsetMax = pTrend->Cfg.nSourceArrayIndexMax + 1 - pTrend->Cfg.ScaleX.nZoomValueCount;
	if(pTrend->Intern.ScaleX.nScrollOffsetMax < 0)
	{
		pTrend->Intern.ScaleX.nScrollOffsetMax = 0;
	}
	if(pTrend->Cfg.ScaleX.bLimitScrollOffset == 1)
	{
		if(pTrend->Cfg.ScaleX.nScrollOffset < 0)
		{
			pTrend->Cfg.ScaleX.nScrollOffset = 0;
		}
		else if(pTrend->Cfg.ScaleX.nScrollOffset > pTrend->Intern.ScaleX.nScrollOffsetMax)
		{
			pTrend->Cfg.ScaleX.nScrollOffset = pTrend->Intern.ScaleX.nScrollOffsetMax;
		}
	}
}

REAL GetTrendValue(BrbVc4DrawTrendCfgCurve_TYP* pCfgCurve, DINT nValueIndex)
{
	REAL rValue = 0.0;
	if(pCfgCurve->eValueSource == eBRB_TREND_SOURCE_SINGLE_ARRAY)
	{
		switch(pCfgCurve->eValueDatatype)
		{
			case eBRB_TREND_VALUE_DATATYPE_REAL:
				{
					REAL* pValue = (REAL*)pCfgCurve->pValueSource + nValueIndex;
					rValue = *pValue;
					rValue *= pCfgCurve->rConversionFactor;
				}
				break;

			case eBRB_TREND_VALUE_DATATYPE_DINT:
				{
					DINT* pValue = (DINT*)pCfgCurve->pValueSource + nValueIndex;
					rValue = (REAL)*pValue;
					rValue *= pCfgCurve->rConversionFactor;
				}
				break;

			case eBRB_TREND_VALUE_DATATYPE_INT:
				{
					INT* pValue = (INT*)pCfgCurve->pValueSource + nValueIndex;
					rValue = (REAL)*pValue;
					rValue *= pCfgCurve->rConversionFactor;
				}
				break;

		}
	}
	else if(pCfgCurve->eValueSource == eBRB_TREND_SOURCE_STRUCT_ARRAY)
	{
		switch(pCfgCurve->eValueDatatype)
		{
			case eBRB_TREND_VALUE_DATATYPE_REAL:
				{
					UDINT pArrayMember = pCfgCurve->pValueSource + (pCfgCurve->nStructSize * nValueIndex);
					UDINT pStructMember = pArrayMember + pCfgCurve->nStructMemberOffset;
					REAL* pValue = (REAL*)pStructMember;
					rValue = *pValue;
					rValue *= pCfgCurve->rConversionFactor;
				}
				break;

			case eBRB_TREND_VALUE_DATATYPE_DINT:
				{
					UDINT pArrayMember = pCfgCurve->pValueSource + (pCfgCurve->nStructSize * nValueIndex);
					UDINT pStructMember = pArrayMember + pCfgCurve->nStructMemberOffset;
					DINT* pValue = (DINT*)pStructMember;
					rValue = (REAL)*pValue;
					rValue *= pCfgCurve->rConversionFactor;
				}
				break;

			case eBRB_TREND_VALUE_DATATYPE_INT:
				{
					UDINT pArrayMember = pCfgCurve->pValueSource + (pCfgCurve->nStructSize * nValueIndex);
					UDINT pStructMember = pArrayMember + pCfgCurve->nStructMemberOffset;
					INT* pValue = (INT*)pStructMember;
					rValue = (REAL)*pValue;
					rValue *= pCfgCurve->rConversionFactor;
				}
				break;

		}
	}
	return rValue;
}
