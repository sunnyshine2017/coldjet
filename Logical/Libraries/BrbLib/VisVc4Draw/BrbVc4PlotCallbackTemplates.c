/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4PlotCallbackTemplates.c
 * Author: niedermeierr
 * Created: December 28, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Muster f�r Callback-Funktion AfterClear */
unsigned short BrbVc4PlotCallbackAfterClear(struct BrbVc4DrawPlot_TYP* pPlot)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterCurveArea */
unsigned short BrbVc4PlotCallbackAfterCrveArea(struct BrbVc4DrawPlot_TYP* pPlot)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterZeroLines */
unsigned short BrbVc4PlotCallbackAfterZeroLines(struct BrbVc4DrawPlot_TYP* pPlot)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterScaleLineY */
unsigned short BrbVc4PlotCallbackAfterScaleLineY(struct BrbVc4DrawPlot_TYP* pPlot, UINT nLineIndex, BrbVc4Line_TYP* pScaleLine, BrbVc4DrawText_TYP* pScaleText, REAL rScaleValue)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterScaleY */
unsigned short BrbVc4PlotCallbackAfterScaleY(struct BrbVc4DrawPlot_TYP* pPlot)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterScaleLineY */
unsigned short BrbVc4PlotCallbackAfterScaleLineX(struct BrbVc4DrawPlot_TYP* pPlot, UINT nLineIndex, BrbVc4Line_TYP* pScaleLine, BrbVc4DrawText_TYP* pScaleText, REAL rScaleValue)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterScaleX */
unsigned short BrbVc4PlotCallbackAfterScaleX(struct BrbVc4DrawPlot_TYP* pPlot)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterCurve */
unsigned short BrbVc4PlotCallbackAfterCurve(struct BrbVc4DrawPlot_TYP* pPlot)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterCursor */
unsigned short BrbVc4PlotCallbackAfterCursor(struct BrbVc4DrawPlot_TYP* pPlot)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterZoomWindow */
unsigned short BrbVc4PlotCallbackAfterZoomWind(struct BrbVc4DrawPlot_TYP* pPlot)
{
	return 0;
}
