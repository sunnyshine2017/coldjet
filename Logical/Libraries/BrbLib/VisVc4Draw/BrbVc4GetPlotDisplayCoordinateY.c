/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4GetPlotDisplayCoordinateY.c
 * Author: niedermeierr
 * Created: December 29, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Gibt die Y-Pixel-Koordinate eines Plot-Wertes zur�ck */
signed long BrbVc4GetPlotDisplayCoordinateY(struct BrbVc4DrawPlot_TYP* pPlot, float rValue)
{
	DINT nResult = pPlot->Intern.CurveArea.nTop + pPlot->Intern.CurveArea.nHeight - (DINT)((rValue - pPlot->Cfg.ScaleY.rMin) * pPlot->Intern.ScaleY.rValueScaleFactor);
	return nResult;
}
