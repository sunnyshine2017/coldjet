/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawAxisRadial.c
 * Author: niedermeierr
 * Created: May 11, 2014
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>
#include <math.h>
REAL const rPI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;
REAL const rPI_HALF = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679 / 2.0;
REAL const rPI_DOUBLE = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679 * 2.0;

#ifdef __cplusplus
	};
#endif

REAL GetScaledLine(REAL rAxPos, REAL rAxMin, REAL rAxMax, DINT nMidX, DINT nMidY, DINT nRadius1, DINT nRadius2, BOOL bInverted, REAL rOffset, BrbVc4Line_TYP* pLine);
void GetScaleTextPosition(BrbVc4Line_TYP* pLine, REAL rDegree, BrbVc4DrawText_TYP* pText, BrbVc4Font_TYP* pFont);
UDINT GetDintAsText(DINT nValue, STRING* pText, UDINT nTextSize);
UDINT GetRealAsText(REAL rValue, STRING* pText, UDINT nTextSize);
DINT GetGreatestValue(DINT nValue1, DINT nValue2, DINT nValue3, DINT nValue4);

/* Zeichnet eine radiale Achse */
float BrbVc4DrawAxisRadial(struct BrbVc4DrawAxisRadial_TYP* pAxis, struct BrbVc4General_TYP* pGeneral)
{
	if(pAxis->nOffset > 360)
	{
		pAxis->nOffset = 360;
	}
	REAL rOffset = (rPI_DOUBLE / 360.0) * (REAL)pAxis->nOffset; // Offset im Bogenma�
	if(pAxis->bInverted == 1)
	{
		rOffset += (rPI_DOUBLE / 360.0) * 180;
	}
	BrbVc4Rectangle_TYP DrawArea;
	DrawArea.nLeft = pAxis->nDrawAreaLeft;
	DrawArea.nTop = pAxis->nDrawAreaTop;
	DrawArea.nWidth = pAxis->nDrawAreaWidth;
	DrawArea.nHeight = pAxis->nDrawAreaHeight;
	DrawArea.nFillColor = pAxis->nDrawAreaColor;
	DrawArea.nBorderColor = 0;
	DrawArea.nDashWidth = 0;
	if(pAxis->bShowDrawArea == 1)
	{
		BrbVc4DrawRectangle(&DrawArea, pGeneral);
	}
	DINT nMidX = DrawArea.nLeft + (DINT)(DrawArea.nWidth / 2.0);
	DINT nMidY = DrawArea.nTop + (DINT)(DrawArea.nHeight / 2.0);
	DINT nRadiusHalf = (DINT)(pAxis->nRadius / 2.0);
	// Ausschnitt
	REAL rAxisClipMin = (REAL)pAxis->nAxisLimitMin;
	REAL rAxisClipMax = (REAL)pAxis->nAxisLimitMax;
	REAL rAxisClipRange = rAxisClipMax - rAxisClipMin;
	REAL rAxisClipRangeMid = rAxisClipMin + (rAxisClipRange / 2.0);
	if(pAxis->bClip == 1)
	{
		if(pAxis->nAxisClipRange == 0)
		{
			pAxis->nAxisClipRange = 1000;
		}
		// Wegen DINT-�berlauf mu� hier mit REAL gerechnet werden
		LREAL rClipRangeHalf = pAxis->nAxisClipRange / 2.0;
		// Modulo funktioniert nur korrekt im positiven Bereich
		// Daher wird die Position auf >= 0 normiert
		LREAL rActNorm = (LREAL)((LREAL)pAxis->rAxisActPosition - (LREAL)pAxis->nAxisLimitMin + (LREAL)pAxis->nAxisClipRange);
		LREAL rClipDiff = rActNorm - rClipRangeHalf;
		DINT nClipDiff = (DINT)rClipDiff;
		DINT nClipMod = nClipDiff % pAxis->nAxisClipRange;
		// Die Normnierung wird r�ckg�ngig gemacht
		LREAL rClipScale = rActNorm - (LREAL)nClipMod - (LREAL)pAxis->nAxisClipRange + (LREAL)pAxis->nAxisLimitMin;
		// und der berechnete Bereich gesetzt
		rAxisClipMin = (REAL)rClipScale;
		rAxisClipMax = (REAL)rAxisClipMin + (REAL)pAxis->nAxisClipRange;
	}
	// Skalierung
	if(pAxis->bShowAxisScale == 1)
	{
		// Skalierungsschritte
		BrbVc4DrawText_TYP AxisScaleCaption;
		AxisScaleCaption.nTop = 0;
		AxisScaleCaption.nFontIndex = pAxis->AxisScaleFont.nIndex;
		AxisScaleCaption.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
		BrbVc4Line_TYP ScaleLine;
		ScaleLine.nTop = AxisScaleCaption.nTop + (DINT)pAxis->AxisScaleFont.rCharHeight + 2;
		ScaleLine.nBottom = ScaleLine.nTop + 4;
		ScaleLine.nColor = pAxis->nAxisScaleColor;
		ScaleLine.nDashWidth = 0;
		if(pAxis->nAxisScaleCount > 0)
		{
			UINT nScaleIndex = 0;
			REAL rScaleDistance = (rAxisClipMax - rAxisClipMin) / pAxis->nAxisScaleCount;
			UINT nStart = 0;
			UINT nEnd = pAxis->nAxisScaleCount-1;
			if(pAxis->rAxisActPosition >= rAxisClipRangeMid)
			{
				nStart += 1;
				nEnd += 1;
			}
			for(nScaleIndex=nStart; nScaleIndex<=nEnd; nScaleIndex++)
			{
				REAL rValue = rAxisClipMin + (nScaleIndex * rScaleDistance);
				REAL rDegree = GetScaledLine(rValue, rAxisClipMin, rAxisClipMax, nMidX, nMidY, pAxis->nRadius - 6, pAxis->nRadius + 6, pAxis->bInverted, rOffset, &ScaleLine);
				BrbVc4DrawLine(&ScaleLine, pGeneral);
				DINT nValue = (DINT)rValue;
				if(rValue > 2147483647.0)
				{
					nValue = 2147483647.0;
				}
				GetDintAsText(nValue, AxisScaleCaption.sText, sizeof(AxisScaleCaption.sText));
				GetScaleTextPosition(&ScaleLine, rDegree, &AxisScaleCaption, &pAxis->AxisScaleFont);
				if(pAxis->bHighlightActPosition == 1 && (pAxis->rAxisActPosition == rValue))
				{
					AxisScaleCaption.nTextColor = pAxis->nAxisScaleHighlightColor;
				}
				else
				{
					AxisScaleCaption.nTextColor = pAxis->nAxisScaleColor;
				}
				BrbVc4DrawText(&AxisScaleCaption, pGeneral);
			}
		}
	}
	// Skalierung-Basis
	BrbVc4Ellipse_TYP AxisScaleCircle;
	AxisScaleCircle.nLeft = nMidX - nRadiusHalf;
	AxisScaleCircle.nWidth = pAxis->nRadius;
	AxisScaleCircle.nTop = nMidY - nRadiusHalf;
	AxisScaleCircle.nHeight = pAxis->nRadius;
	AxisScaleCircle.nFillColor = eBRB_COLOR_TRANSPARENT;
	AxisScaleCircle.nBorderColor = pAxis->nAxisScaleColor;
	AxisScaleCircle.nDashWidth = 0;
	BrbVc4DrawEllipse(&AxisScaleCircle, pGeneral);
	// Achs-Soll-Linie
	BrbVc4Line_TYP AxisSetLine;
	GetScaledLine(pAxis->rAxisSetPosition, rAxisClipMin, rAxisClipMax, nMidX, nMidY, pAxis->nRadius - 16, pAxis->nRadius, pAxis->bInverted, rOffset, &AxisSetLine);
	AxisSetLine.nColor = pAxis->nAxisSetPositionColor;
	AxisSetLine.nDashWidth = 0;
	if(pAxis->bShowAxisSetPosition == 1 && (pAxis->rAxisSetPosition >= rAxisClipMin) && (pAxis->rAxisSetPosition <= rAxisClipMax))
	{
		BrbVc4DrawLine(&AxisSetLine, pGeneral);
	}
	// Achs-Linie
	BrbVc4Line_TYP AxisLine;
	GetScaledLine(pAxis->rAxisActPosition, rAxisClipMin, rAxisClipMax, nMidX, nMidY, 0, pAxis->nRadius, pAxis->bInverted, rOffset, &AxisLine);
	AxisLine.nColor = pAxis->nAxisColor;
	AxisLine.nDashWidth = 0;
	if(pAxis->bShowAxisPosLine == 1)
	{
		BrbVc4DrawLine(&AxisLine, pGeneral);
	}
	// Beschriftung
	UINT nCaptionOrderName = 0;
	UINT nCaptionOrderPosition = 1;
	UINT nCaptionOrderVelocity = 1;
	if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_NAME_POS_VEL)
	{
		nCaptionOrderName = 0;
		nCaptionOrderPosition = 1;
		nCaptionOrderVelocity = 2;
	}
	else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_NAME_VEL_POS)
	{
		nCaptionOrderName = 0;
		nCaptionOrderPosition = 2;
		nCaptionOrderVelocity = 1;
	}
	else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_POS_NAME_VEL)
	{
		nCaptionOrderName = 1;
		nCaptionOrderPosition = 0;
		nCaptionOrderVelocity = 2;
	}
	else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_POS_VEL_NAME)
	{
		nCaptionOrderName = 2;
		nCaptionOrderPosition = 0;
		nCaptionOrderVelocity = 1;
	}
	else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_VEL_NAME_POS)
	{
		nCaptionOrderName = 1;
		nCaptionOrderPosition = 2;
		nCaptionOrderVelocity = 0;
	}
	else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_VEL_POS_NAME)
	{
		nCaptionOrderName = 2;
		nCaptionOrderPosition = 1;
		nCaptionOrderVelocity = 0;
	}
	DINT nCaptionHeight = 0;
	BrbVc4DrawText_TYP AxisPosition;
	UINT nAxisPositionLen = GetRealAsText(pAxis->rAxisActPosition, AxisPosition.sText, sizeof(AxisPosition.sText));
	DINT nAxisPositionWidth = 0;
	if(pAxis->bShowAxisActPosition == 1)
	{
		nAxisPositionWidth = (DINT)(nAxisPositionLen * pAxis->AxisValueFont.rCharWidth);
		nCaptionHeight += (DINT)pAxis->AxisValueFont.rCharHeight;
	}
	BrbVc4DrawText_TYP AxisName;
	strcpy(AxisName.sText, pAxis->sAxisName);
	UINT nAxisNameLen = strlen(AxisName.sText);
	DINT nAxisNameWidth = 0;
	if(pAxis->bShowAxisName == 1)
	{
		nAxisNameWidth = (DINT)(nAxisNameLen * pAxis->AxisNameFont.rCharWidth);
		nCaptionHeight += (DINT)pAxis->AxisNameFont.rCharHeight;
	}
	BrbVc4DrawText_TYP AxisVelocity;
	UINT nAxisVelocityLen = GetRealAsText(pAxis->rAxisActVelocity, AxisVelocity.sText, sizeof(AxisVelocity.sText));
	DINT nAxisVelocityWidth = 0;
	if(pAxis->bShowAxisActVelocity == 1)
	{
		nAxisVelocityWidth = (DINT)(nAxisVelocityLen * pAxis->AxisValueFont.rCharWidth);
		nCaptionHeight += (DINT)pAxis->AxisValueFont.rCharHeight;
	}
	DINT nY = nMidY - (DINT)(nCaptionHeight / 2.0) - 6;
	UINT nCaptionIndex = 0;
	if(pAxis->bShowAxisName == 1)
	{
		nAxisNameWidth = (DINT)(nAxisNameLen * pAxis->AxisNameFont.rCharWidth);
	}
	for(nCaptionIndex=0; nCaptionIndex<=3; nCaptionIndex++)
	{
		if(nCaptionIndex == nCaptionOrderName)
		{
			if(pAxis->bShowAxisName == 1)
			{
				AxisName.nLeft = nMidX - (DINT)(nAxisNameWidth / 2.0);
				AxisName.nTop = nY;
				AxisName.nFontIndex = pAxis->AxisNameFont.nIndex;
				AxisName.nTextColor = pAxis->nAxisNameColor;
				AxisName.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
				BrbVc4DrawText(&AxisName, pGeneral);
				nY += (DINT)pAxis->AxisNameFont.rCharHeight;
			}
		
		}
		if(nCaptionIndex == nCaptionOrderPosition)
		{
			if(pAxis->bShowAxisActPosition == 1)
			{
				AxisPosition.nLeft = nMidX - (DINT)(nAxisPositionWidth / 2.0);
				AxisPosition.nTop = nY;
				AxisPosition.nFontIndex = pAxis->AxisValueFont.nIndex;
				AxisPosition.nTextColor = pAxis->nAxisActPositionColor;
				AxisPosition.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
				BrbVc4DrawText(&AxisPosition, pGeneral);
				nY += (DINT)pAxis->AxisValueFont.rCharHeight;
			}

		}
		if(nCaptionIndex == nCaptionOrderVelocity)
		{
			if(pAxis->bShowAxisActVelocity == 1)
			{
				AxisVelocity.nLeft = nMidX - (DINT)(nAxisVelocityWidth / 2.0);
				AxisVelocity.nTop = nY;
				AxisVelocity.nFontIndex = pAxis->AxisValueFont.nIndex;
				AxisVelocity.nTextColor = pAxis->nAxisActVelocityColor;
				AxisVelocity.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
				BrbVc4DrawText(&AxisVelocity, pGeneral);
				nY += (DINT)pAxis->AxisValueFont.rCharHeight;
			}
		}
	}
	return rAxisClipMin;
}

REAL GetScaledLine(REAL rAxPos, REAL rAxMin, REAL rAxMax, DINT nMidX, DINT nMidY, DINT nRadius1, DINT nRadius2, BOOL bInverted, REAL rOffset, BrbVc4Line_TYP* pLine)
{
	REAL rResult = 0.0;
	REAL rAxRange = rAxMax - rAxMin;
	LREAL rDegreePerUnit = 0;
	if(rAxRange != 0)
	{
		rDegreePerUnit = (LREAL)rPI_DOUBLE / (LREAL)rAxRange;
	}
	LREAL rDegree = (rAxPos - rAxMin) * rDegreePerUnit + rPI_HALF;
	if(bInverted == 1)
	{
		rDegree = rPI_DOUBLE - rDegree;
	}
	rDegree += rOffset;
	LREAL nRadiusHalf1 = (REAL)nRadius1 / 2.0;
	LREAL rCos = cos(rDegree);
	LREAL rSin = sin(rDegree);
	pLine->nLeft = (DINT)(nMidX + nRadiusHalf1 * rCos);
	pLine->nTop = (DINT)(nMidY + nRadiusHalf1 * rSin);
	LREAL nRadiusHalf2 = (REAL)nRadius2 / 2.0;
	pLine->nRight = (DINT)(nMidX + nRadiusHalf2 * rCos);
	pLine->nBottom = (DINT)(nMidY + nRadiusHalf2 * rSin);
	rResult = rDegree + rPI_HALF;
	// Erster �berlauf muss korrigiert werden
	if(rResult >= rPI_DOUBLE)
	{
		rResult -= rPI_DOUBLE;
	}
	// Zweiter �berlauf muss korrigiert werden
	if(rResult >= rPI_DOUBLE)
	{
		rResult -= rPI_DOUBLE;
	}
	return rResult;
}

void GetScaleTextPosition(BrbVc4Line_TYP* pLine, REAL rDegree, BrbVc4DrawText_TYP* pText, BrbVc4Font_TYP* pFont)
{
	UDINT nTextLen = strlen(pText->sText);
	DINT nX = 0;
	if((rDegree > -0.1 && rDegree < 0.1) || (rDegree > (rPI - 0.1) && rDegree < (rPI + 0.1)))
	{
		// Im Bereich von 0� oder 180� zentrieren
		nX =  -(DINT)((nTextLen * pFont->rCharWidth) / 2.0);
	}
	else if(rDegree > 0 && rDegree < rPI)
	{
		// Zwischen 0� und 180� linksb�ndig
		nX = 2;
	}
	else if(rDegree > rPI && rDegree < rPI_DOUBLE)
	{
		// Zwischen 180� und 360� rechtsb�ndig
		nX = -(nTextLen * pFont->rCharWidth) - 2;
	}
	pText->nLeft = pLine->nRight + nX;
	DINT nY = 0;
	if(rDegree <= rPI || rDegree >= rPI_DOUBLE)
	{
		nY = (DINT)((rDegree / rPI) * (pFont->rCharHeight)) - pFont->rCharHeight;
	}
	else
	{
		nY = (DINT)(((rPI_DOUBLE - rDegree) / rPI) * (pFont->rCharHeight)) - pFont->rCharHeight;
	}
	pText->nTop = pLine->nBottom + nY;
}
