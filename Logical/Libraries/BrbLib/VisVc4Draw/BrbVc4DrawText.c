/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawText.c
 * Author: niedermeierr
 * Created: July 02, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>

#ifdef __cplusplus
	};
#endif

/* Zeichnet einen Text */
unsigned short BrbVc4DrawText(struct BrbVc4DrawText_TYP* pText, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	nResult = VA_Textout(1, pGeneral->nVcHandle, pText->nFontIndex, pText->nLeft, pText->nTop, pText->nTextColor, pText->nBackgroundColor, pText->sText);
	return nResult;
}
