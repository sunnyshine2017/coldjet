/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4TrendCallbackTemplates.c
 * Author: niedermeierr
 * Created: December 28, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Muster f�r Callback-Funktion AfterClear */
unsigned short BrbVc4TrendCallbackAfterClear(struct BrbVc4DrawTrend_TYP* pTrend)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterCurveArea */
unsigned short BrbVc4TrendCallbackAfterCrveArea(struct BrbVc4DrawTrend_TYP* pTrend)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterScaleLineY */
unsigned short BrbVc4TrendCallbackAfterScaleLineY(struct BrbVc4DrawTrend_TYP* pTrend, BrbVc4TrendScaleYIndex_ENUM eScaleYIndex, UINT nLineIndex, BrbVc4Line_TYP* pScaleLine, BrbVc4DrawText_TYP* pScaleText, REAL rScaleValue)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterScaleY */
unsigned short BrbVc4TrendCallbackAfterScaleY(struct BrbVc4DrawTrend_TYP* pTrend, BrbVc4TrendScaleYIndex_ENUM eScaleYIndex)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterScaleLineX */
unsigned short BrbVc4TrendCallbackAfterScaleLineX(struct BrbVc4DrawTrend_TYP* pTrend, UINT nLineIndex, BrbVc4Line_TYP* pScaleLine, BrbVc4DrawText_TYP* pScaleText, DINT nSampleIndex)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterScaleX */
unsigned short BrbVc4TrendCallbackAfterScaleX(struct BrbVc4DrawTrend_TYP* pTrend)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterCurve */
unsigned short BrbVc4TrendCallbackAfterCurve(struct BrbVc4DrawTrend_TYP* pTrend, UINT nCurveIndex)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterCursor */
unsigned short BrbVc4TrendCallbackAfterCursor(struct BrbVc4DrawTrend_TYP* pTrend, UINT nCursorIndex)
{
	return 0;
}

/* Muster f�r Callback-Funktion AfterZoomWindow */
unsigned short BrbVc4TrendCallbackAfterZoomWind(struct BrbVc4DrawTrend_TYP* pTrend)
{
	return 0;
}
