/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawArc.c
 * Author: niedermeierr
 * Created: July 04, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>
#include <math.h>

#ifdef __cplusplus
	};
#endif

// Prototypen
REAL GetAngleRad90(REAL rAngleDegree);

/* Zeichnet einen Bogen */
unsigned short BrbVc4DrawArc(struct BrbVc4Arc_TYP* pArc, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	UINT nMiddleX = (UINT)(pArc->nLeft + pArc->nWidth / 2);
	UINT nMiddleY = (UINT)(pArc->nTop + pArc->nHeight / 2);
	REAL rRadiusX = (REAL)pArc->nWidth / 2.0;
	REAL rRadiusY = (REAL)pArc->nHeight / 2.0;
	REAL rStep = 1.0;
	REAL rStartAngle = pArc->rStartAngle;
	REAL rEndAngle = pArc->rEndAngle;
	if(pArc->rStartAngle > pArc->rEndAngle)
	{
		rEndAngle = pArc->rEndAngle + 360.0;
	}
	
	if(pArc->nDashWidth > 1)
	{
		// Gestrichelt
		REAL rX = rRadiusX * cos(GetAngleRad90(rStartAngle));
		REAL rY = rRadiusY * sin(GetAngleRad90(rStartAngle));
		BrbVc4Line_TYP SolidLine;
		memset(&SolidLine, 0, sizeof(SolidLine));
		SolidLine.nLeft = SolidLine.nRight = (DINT)((REAL)nMiddleX + rX);
		SolidLine.nTop = SolidLine.nBottom = (DINT)((REAL)nMiddleY + rY);
		DINT nLastChangeSolidX = SolidLine.nLeft;
		DINT nLastChangeSolidY = SolidLine.nTop;
		BOOL bSolid = 1;
		REAL rAngleDegree = 0.0;
		for(rAngleDegree=rStartAngle; rAngleDegree<=rEndAngle; rAngleDegree+=rStep)
		{
			REAL rAngleBog = GetAngleRad90(rAngleDegree); 
			rX = rRadiusX * cos(rAngleBog);
			rY = rRadiusY * sin(rAngleBog);
			SolidLine.nLeft =	SolidLine.nRight;
			SolidLine.nTop = SolidLine.nBottom;
			SolidLine.nRight = (DINT)((REAL)nMiddleX + rX);
			SolidLine.nBottom = (DINT)((REAL)nMiddleY+ rY);
			SolidLine.nColor = pArc->nBorderColor;
			DINT nDiffX = abs(nLastChangeSolidX - SolidLine.nLeft);
			DINT nDiffY = abs(nLastChangeSolidY - SolidLine.nTop);
			if(nDiffX >= pArc->nDashWidth || nDiffY >= pArc->nDashWidth)
			{
				bSolid = !bSolid;
				nLastChangeSolidX = SolidLine.nLeft;
				nLastChangeSolidY = SolidLine.nTop;
			}
			if(bSolid == 1)
			{
				nResult = BrbVc4DrawLine(&SolidLine, pGeneral);
			}
			UINT nDummy = 0; nDummy = 1;
		}
		// Der letzte Punkt wird immer gezeichnet
		nResult = BrbVc4DrawLine(&SolidLine, pGeneral);
	}
	else
	{
		// Solide
		REAL rX = rRadiusX * cos(GetAngleRad90(rStartAngle));
		REAL rY = rRadiusY * sin(GetAngleRad90(rStartAngle));
		BrbVc4Line_TYP SolidLine;
		memset(&SolidLine, 0, sizeof(SolidLine));
		SolidLine.nRight = (DINT)((REAL)nMiddleX + rX);
		SolidLine.nBottom = (DINT)((REAL)nMiddleY+ rY);
		SolidLine.nColor = pArc->nBorderColor;
		REAL rAngleDegree = 0.0;
		for(rAngleDegree=rStartAngle; rAngleDegree<=rEndAngle; rAngleDegree+=rStep)
		{
			REAL rAngleBog = GetAngleRad90(rAngleDegree); 
			rX = rRadiusX * cos(rAngleBog);
			rY = rRadiusY * sin(rAngleBog);
			SolidLine.nLeft =	SolidLine.nRight;
			SolidLine.nTop = SolidLine.nBottom;
			SolidLine.nRight = (DINT)((REAL)nMiddleX + rX);
			SolidLine.nBottom = (DINT)((REAL)nMiddleY+ rY);
			nResult = BrbVc4DrawLine(&SolidLine, pGeneral);
			UINT nDummy = 0; nDummy = 1;
		}
	}
	return nResult;
}

// Umrechung von Grad auf Bogenma� (AnsiC-Math-Funktionen rechnen nur in Bogenma�)
// Zus�tzlich wird der Winkel um 90� nach hinten versetzt, damit 0� oben ist statt rechts (cos + sin rechnen mit 0� = rechts)
REAL GetAngleRad90(REAL rAngleDegree)
{
	REAL nResult = BrbGetAngleRad(rAngleDegree - 90);
	return nResult;
}
