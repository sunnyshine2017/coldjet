/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4GetPlotDisplayCoordinateX.c
 * Author: niedermeierr
 * Created: December 29, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Gibt die X-Pixel-Koordinate eines Plot-Wertes zur�ck */
signed long BrbVc4GetPlotDisplayCoordinateX(struct BrbVc4DrawPlot_TYP* pPlot, float rValue)
{
	DINT nResult = (DINT)((rValue - pPlot->Cfg.ScaleX.rMin) * pPlot->Intern.ScaleX.rValueScaleFactor) + pPlot->Intern.CurveArea.nLeft;
	
	nResult = ((rValue - pPlot->Cfg.ScaleX.rMin) * pPlot->Intern.ScaleX.rValueScaleFactor) + pPlot->Intern.CurveArea.nLeft;
	return nResult;
}
