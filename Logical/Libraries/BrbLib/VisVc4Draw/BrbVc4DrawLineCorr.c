/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawLineCorr.c
 * Author: niedermeierr
 * Created: September 12, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>

#ifdef __cplusplus
	};
#endif

/* Zeichnet eine korrigierte Linie */
unsigned short BrbVc4DrawLineCorr(struct BrbVc4Line_TYP* pLine, struct BrbVc4General_TYP* pGeneral)
{
	BrbVc4Line_TYP Line;
	memcpy(&Line, pLine, sizeof(BrbVc4Line_TYP));
	BrbVc4CorrectLine(&Line);
	return BrbVc4DrawLine(&Line, pGeneral);
}
