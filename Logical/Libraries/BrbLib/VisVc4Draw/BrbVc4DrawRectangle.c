/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawRectangle.c
 * Author: niedermeierr
 * Created: July 02, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>

#ifdef __cplusplus
	};
#endif

/* Zeichnet ein Rechteck */
unsigned short BrbVc4DrawRectangle(struct BrbVc4Rectangle_TYP* pRectangle, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	if(pRectangle->nDashWidth > 1)
	{
		// Gestrichelt (wird aus Linien zusammengesetzt)
		BrbVc4Line_TYP Lines[4];
		memset(&Lines, 0, sizeof(Lines));
		Lines[0].nLeft = pRectangle->nLeft;
		Lines[0].nTop = pRectangle->nTop;
		Lines[0].nRight = pRectangle->nLeft + pRectangle->nWidth;
		Lines[0].nBottom = pRectangle->nTop;
		Lines[0].nColor = pRectangle->nBorderColor;
		Lines[0].nDashWidth = pRectangle->nDashWidth;
		Lines[1].nLeft = pRectangle->nLeft + pRectangle->nWidth;
		Lines[1].nTop = pRectangle->nTop;
		Lines[1].nRight = pRectangle->nLeft + pRectangle->nWidth;
		Lines[1].nBottom = pRectangle->nTop + pRectangle->nHeight;
		Lines[1].nColor = pRectangle->nBorderColor;
		Lines[1].nDashWidth = pRectangle->nDashWidth;
		Lines[2].nLeft = pRectangle->nLeft + pRectangle->nWidth;
		Lines[2].nTop = pRectangle->nTop + pRectangle->nHeight;
		Lines[2].nRight = pRectangle->nLeft;
		Lines[2].nBottom = pRectangle->nTop + pRectangle->nHeight;
		Lines[2].nColor = pRectangle->nBorderColor;
		Lines[2].nDashWidth = pRectangle->nDashWidth;
		Lines[3].nLeft = pRectangle->nLeft;
		Lines[3].nTop = pRectangle->nTop + pRectangle->nHeight;
		Lines[3].nRight = pRectangle->nLeft;
		Lines[3].nBottom = pRectangle->nTop;
		Lines[3].nColor = pRectangle->nBorderColor;
		Lines[3].nDashWidth = pRectangle->nDashWidth;

		if(pRectangle->nFillColor != 255)
		{
			nResult = VA_Rect(1, pGeneral->nVcHandle, pRectangle->nLeft, pRectangle->nTop, pRectangle->nWidth, pRectangle->nHeight, pRectangle->nFillColor, pRectangle->nFillColor);
		}
		BrbVc4DrawLine(&Lines[0], pGeneral);		
		BrbVc4DrawLine(&Lines[1], pGeneral);		
		BrbVc4DrawLine(&Lines[2], pGeneral);		
		BrbVc4DrawLine(&Lines[3], pGeneral);		
	}
	else
	{
		// Solide
		nResult = VA_Rect(1, pGeneral->nVcHandle, pRectangle->nLeft, pRectangle->nTop, pRectangle->nWidth, pRectangle->nHeight, pRectangle->nFillColor, pRectangle->nBorderColor);
	}
	return nResult;
}
