/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4GetTrendDisplayCoordinateY.c
 * Author: niedermeierr
 * Created: December 11, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Gibt die Y-Pixel-Koordinate eines Trend-Wertes zur�ck */
signed long BrbVc4GetTrendDisplayCoordinateY(struct BrbVc4DrawTrend_TYP* pTrend, BrbVc4TrendScaleYIndex_ENUM eScaleY, float rValue)
{
	DINT nResult = (DINT)(pTrend->Intern.CurveArea.nTop + pTrend->Intern.CurveArea.nHeight - ((rValue - pTrend->Cfg.ScaleY[eScaleY].rMin) * pTrend->Intern.ScaleY[eScaleY].rValueScaleFactor));
	return nResult;
}
