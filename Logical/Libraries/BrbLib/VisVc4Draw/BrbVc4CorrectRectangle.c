/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4CorrectRectangle.c
 * Author: niedermeierr
 * Created: September 12, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Korrigiert ein Rechteck mit negativen Koordinaten */
unsigned short BrbVc4CorrectRectangle(struct BrbVc4Rectangle_TYP* pRectangle)
{
	DINT nRight = pRectangle->nLeft + pRectangle->nWidth;
	DINT nBottom = pRectangle->nTop + pRectangle->nHeight;
	if(pRectangle->nLeft < 0 && nRight < 0)
		{
		// Wenn Rechteck ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pRectangle->nLeft = 12345678;
	}
	else if(pRectangle->nTop < 0 && nBottom < 0)
	{
		// Wenn Rechteck ausserhalb des Zeichenbereichs, dann ins Positive ausserhalb des Bereichs korrigieren
		pRectangle->nTop = 12345678;
	}
	else
	{
		if(pRectangle->nLeft < 0)
		{
			pRectangle->nWidth += pRectangle->nLeft;
			pRectangle->nLeft = 0;
		}
		if(pRectangle->nTop < 0)
		{
			pRectangle->nHeight += pRectangle->nTop;
			pRectangle->nTop = 0;
		}
	}
	return 0;
}
