/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4GetTrendDisplayCoordinateX.c
 * Author: niedermeierr
 * Created: January 03, 2016
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Gibt die X-Pixel-Koordinate eines Trend-Wertes zur�ck */
signed long BrbVc4GetTrendDisplayCoordinateX(struct BrbVc4DrawTrend_TYP* pTrend, signed long nSampleIndex)
{
	DINT nResult = (pTrend->Intern.CurveArea.nWidth * (REAL)(nSampleIndex - pTrend->Cfg.ScaleX.nScrollOffset) / pTrend->Cfg.ScaleX.nZoomValueCount) + pTrend->Intern.CurveArea.nLeft;
	return nResult;
}
