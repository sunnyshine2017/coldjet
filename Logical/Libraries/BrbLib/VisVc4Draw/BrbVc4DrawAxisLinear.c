/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawAxisLinear.c
 * Author: niedermeierr
 * Created: May 09, 2014
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>

#ifdef __cplusplus
	};
#endif

DINT ScaleAxisPosition(REAL rAxPos, REAL rAxMin, REAL rAxMax, DINT nDrawMin, DINT nDrawMax, BOOL bInverted);
UDINT GetDintAsText(DINT nValue, STRING* pText, UDINT nTextSize);
UDINT GetRealAsText(REAL rValue, STRING* pText, UDINT nTextSize);
DINT GetGreatestValue(DINT nValue1, DINT nValue2, DINT nValue3, DINT nValue4);

/* Zeichnet eine lineare Achse */
float BrbVc4DrawAxisLinear(struct BrbVc4DrawAxisLinear_TYP* pAxis, struct BrbVc4General_TYP* pGeneral)
{
	REAL rResult = 0.0;
	UDINT nTextLen = 0;
	BrbVc4Rectangle_TYP DrawArea;
	DrawArea.nLeft = pAxis->nDrawAreaLeft;
	DrawArea.nTop = pAxis->nDrawAreaTop;
	DrawArea.nWidth = pAxis->nDrawAreaWidth;
	DrawArea.nHeight = pAxis->nDrawAreaHeight;
	DrawArea.nFillColor = pAxis->nDrawAreaColor;
	DrawArea.nBorderColor = 0;
	DrawArea.nDashWidth = 0;
	if(pAxis->bShowDrawArea == 1)
	{
		BrbVc4DrawRectangle(&DrawArea, pGeneral);
	}
	if(pAxis->bVertical == 0)
	{
		// Horizontal
		BrbVc4Rectangle_TYP DrawAreaAx;
		DrawAreaAx.nLeft = DrawArea.nLeft + pAxis->nDrawIndent;
		DrawAreaAx.nTop = DrawArea.nTop;
		DrawAreaAx.nWidth = DrawArea.nWidth - 2 * pAxis->nDrawIndent;
		DrawAreaAx.nHeight = DrawArea.nHeight;
		DrawAreaAx.nFillColor = DrawArea.nFillColor;
		DrawAreaAx.nBorderColor = 0;
		DrawAreaAx.nDashWidth = 0;
		DINT nY = DrawArea.nTop;
		// Ausschnitt
		REAL rAxisClipMin = (REAL)pAxis->nAxisLimitMin;
		REAL rAxisClipMax = (REAL)pAxis->nAxisLimitMax;
		if(pAxis->bClip == 1)
		{
			if(pAxis->nAxisClipRange == 0)
			{
				pAxis->nAxisClipRange = 1000;
			}
			// Wegen DINT-�berlauf mu� hier mit REAL gerechnet werden
			LREAL rClipRangeHalf = pAxis->nAxisClipRange / 2.0;
			// Modulo funktioniert nur korrekt im positiven Bereich
			// Daher wird die Position auf >= 0 normiert
			LREAL rActNorm = (LREAL)((LREAL)pAxis->rAxisActPosition - (LREAL)pAxis->nAxisLimitMin + (LREAL)pAxis->nAxisClipRange);
			LREAL rClipDiff = rActNorm - rClipRangeHalf;
			DINT nClipDiff = (DINT)rClipDiff;
			DINT nClipMod = nClipDiff % pAxis->nAxisClipRange;
			// Die Normnierung wird r�ckg�ngig gemacht
			LREAL rClipScale = rActNorm - (LREAL)nClipMod - (LREAL)pAxis->nAxisClipRange + (LREAL)pAxis->nAxisLimitMin;
			// und der berechnete Bereich gesetzt
			rAxisClipMin = (REAL)rClipScale;
			rAxisClipMax = (REAL)rAxisClipMin + (REAL)pAxis->nAxisClipRange;
		}
		// Skalierung
		if(pAxis->bShowAxisScale == 1)
		{
			// Skalierungsschritte
			BrbVc4DrawText_TYP AxisScaleCaption;
			AxisScaleCaption.nTop = nY;
			AxisScaleCaption.nFontIndex = pAxis->AxisScaleFont.nIndex;
			AxisScaleCaption.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
			BrbVc4Line_TYP ScaleLine;
			ScaleLine.nTop = AxisScaleCaption.nTop + (DINT)pAxis->AxisScaleFont.rCharHeight;
			ScaleLine.nBottom = ScaleLine.nTop + 4;
			ScaleLine.nColor = pAxis->nAxisScaleColor;
			ScaleLine.nDashWidth = 0;
			if(pAxis->nAxisScaleCount > 0)
			{
				UINT nScaleIndex = 0;
				REAL rScaleDistance = (rAxisClipMax - rAxisClipMin) / pAxis->nAxisScaleCount;
				for(nScaleIndex=0; nScaleIndex<=pAxis->nAxisScaleCount; nScaleIndex++)
				{
					REAL rValue = rAxisClipMin + (nScaleIndex * rScaleDistance);
					ScaleLine.nLeft = ScaleAxisPosition(rValue, rAxisClipMin, rAxisClipMax, DrawAreaAx.nLeft, DrawAreaAx.nLeft + DrawAreaAx.nWidth, pAxis->bInverted);
					ScaleLine.nRight = ScaleLine.nLeft;
					BrbVc4DrawLine(&ScaleLine, pGeneral);
					DINT nValue = (DINT)rValue;
					if(rValue > 2147483647.0)
					{
						nValue = 2147483647.0;
					}
					nTextLen = GetDintAsText(nValue, AxisScaleCaption.sText, sizeof(AxisScaleCaption.sText));
					AxisScaleCaption.nLeft = ScaleLine.nLeft;
					// Zwischen-Beschriftungen zentrieren
					AxisScaleCaption.nLeft -= (DINT)((nTextLen * pAxis->AxisScaleFont.rCharWidth) / 2.0);
					if(pAxis->bHighlightActPosition == 1 && (pAxis->rAxisActPosition == rValue))
					{
						AxisScaleCaption.nTextColor = pAxis->nAxisScaleHighlightColor;
					}
					else
					{
						AxisScaleCaption.nTextColor = pAxis->nAxisScaleColor;
					}
					BrbVc4DrawText(&AxisScaleCaption, pGeneral);
				}
			}
			// Skalierung-Basis
			BrbVc4Line_TYP AxisScaleBase;
			AxisScaleBase.nLeft = DrawAreaAx.nLeft;
			AxisScaleBase.nRight = DrawAreaAx.nLeft + DrawAreaAx.nWidth;
			AxisScaleBase.nTop = ScaleLine.nTop + 2;
			AxisScaleBase.nBottom = AxisScaleBase.nTop;
			AxisScaleBase.nColor = pAxis->nAxisScaleColor;
			AxisScaleBase.nDashWidth = 0;
			BrbVc4DrawLine(&AxisScaleBase, pGeneral);
			nY = AxisScaleBase.nTop;
		}
		// Achs-Soll-Position
		BrbVc4Line_TYP AxisSetPosLine;
		AxisSetPosLine.nLeft = ScaleAxisPosition(pAxis->rAxisSetPosition, rAxisClipMin, rAxisClipMax, DrawAreaAx.nLeft, DrawAreaAx.nLeft + DrawAreaAx.nWidth, pAxis->bInverted);
		AxisSetPosLine.nRight = AxisSetPosLine.nLeft;
		AxisSetPosLine.nTop = nY;
		AxisSetPosLine.nBottom = AxisSetPosLine.nTop + 16;
		AxisSetPosLine.nColor = pAxis->nAxisSetPositionColor;
		AxisSetPosLine.nDashWidth = 0;
		if(pAxis->bShowAxisSetPosition == 1 && (pAxis->rAxisSetPosition >= rAxisClipMin) && (pAxis->rAxisSetPosition <= rAxisClipMax))
		{
			BrbVc4DrawLine(&AxisSetPosLine, pGeneral);
		}
		// Achs-Positions-Linie
		BrbVc4Line_TYP AxisPosLine;
		AxisPosLine.nLeft = ScaleAxisPosition(pAxis->rAxisActPosition, rAxisClipMin, rAxisClipMax, DrawAreaAx.nLeft, DrawAreaAx.nLeft + DrawAreaAx.nWidth, pAxis->bInverted);
		AxisPosLine.nRight = AxisPosLine.nLeft;
		AxisPosLine.nTop = nY;
		AxisPosLine.nBottom = AxisPosLine.nTop + 8;
		AxisPosLine.nColor = pAxis->nAxisColor;
		AxisPosLine.nDashWidth = 0;
		if(pAxis->bShowAxisPosLine == 1)
		{
			BrbVc4DrawLine(&AxisPosLine, pGeneral);
		}
		// Beschriftung
		UINT nCaptionOrderName = 0;
		UINT nCaptionOrderPosition = 1;
		UINT nCaptionOrderVelocity = 2;
		if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_NAME_POS_VEL)
		{
			nCaptionOrderName = 0;
			nCaptionOrderPosition = 1;
			nCaptionOrderVelocity = 2;
		}
		else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_NAME_VEL_POS)
		{
			nCaptionOrderName = 0;
			nCaptionOrderPosition = 2;
			nCaptionOrderVelocity = 1;
		}
		else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_POS_NAME_VEL)
		{
			nCaptionOrderName = 1;
			nCaptionOrderPosition = 0;
			nCaptionOrderVelocity = 2;
		}
		else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_POS_VEL_NAME)
		{
			nCaptionOrderName = 2;
			nCaptionOrderPosition = 0;
			nCaptionOrderVelocity = 1;
		}
		else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_VEL_NAME_POS)
		{
			nCaptionOrderName = 1;
			nCaptionOrderPosition = 2;
			nCaptionOrderVelocity = 0;
		}
		else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_VEL_POS_NAME)
		{
			nCaptionOrderName = 2;
			nCaptionOrderPosition = 1;
			nCaptionOrderVelocity = 0;
		}
		BrbVc4DrawText_TYP AxisPosition;
		UINT nAxisPositionLen = GetRealAsText(pAxis->rAxisActPosition, AxisPosition.sText, sizeof(AxisPosition.sText));
		DINT nAxisPositionWidth = (DINT)(nAxisPositionLen * pAxis->AxisValueFont.rCharWidth);
		BrbVc4DrawText_TYP AxisName;
		strcpy(AxisName.sText, pAxis->sAxisName);
		UINT nAxisNameLen = strlen(AxisName.sText);
		DINT nAxisNameWidth = (DINT)(nAxisNameLen * pAxis->AxisNameFont.rCharWidth);
		BrbVc4DrawText_TYP AxisVelocity;
		UINT nAxisVelocityLen = GetRealAsText(pAxis->rAxisActVelocity, AxisVelocity.sText, sizeof(AxisVelocity.sText));
		DINT nAxisVelocityWidth = (DINT)(nAxisVelocityLen * pAxis->AxisValueFont.rCharWidth);
		nY = AxisPosLine.nBottom;
		UINT nCaptionIndex = 0;
		for(nCaptionIndex=0; nCaptionIndex<=3; nCaptionIndex++)
		{
			if(nCaptionIndex == nCaptionOrderName)
			{
				if(pAxis->bShowAxisName == 1)
				{
					AxisName.nLeft = AxisPosLine.nLeft - (DINT)(nAxisNameWidth / 2.0);
					AxisName.nTop = nY;
					AxisName.nFontIndex = pAxis->AxisNameFont.nIndex;
					AxisName.nTextColor = pAxis->nAxisNameColor;
					AxisName.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
					BrbVc4DrawText(&AxisName, pGeneral);
					nY += (DINT)pAxis->AxisNameFont.rCharHeight;
				}
		
			}
			if(nCaptionIndex == nCaptionOrderPosition)
			{
				if(pAxis->bShowAxisActPosition == 1)
				{
					AxisPosition.nLeft = AxisPosLine.nLeft - (DINT)((nAxisPositionLen * pAxis->AxisValueFont.rCharWidth) / 2.0);
					AxisPosition.nTop = nY;
					AxisPosition.nFontIndex = pAxis->AxisValueFont.nIndex;
					AxisPosition.nTextColor = pAxis->nAxisActPositionColor;
					AxisPosition.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
					BrbVc4DrawText(&AxisPosition, pGeneral);
					nY += (DINT)pAxis->AxisValueFont.rCharHeight;
				}

			}
			if(nCaptionIndex == nCaptionOrderVelocity)
			{
				if(pAxis->bShowAxisActVelocity == 1)
				{
					AxisVelocity.nLeft = AxisPosLine.nLeft - (DINT)((nAxisVelocityLen * pAxis->AxisValueFont.rCharWidth) / 2.0);
					AxisVelocity.nTop = nY;
					AxisVelocity.nFontIndex = pAxis->AxisValueFont.nIndex;
					AxisVelocity.nTextColor = pAxis->nAxisActVelocityColor;
					AxisVelocity.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
					BrbVc4DrawText(&AxisVelocity, pGeneral);
					nY += (DINT)pAxis->AxisValueFont.rCharHeight;
				}
			}
		}
		// Rahmen
		BrbVc4Rectangle_TYP AxisRect;
		DINT nWidestWidth = GetGreatestValue(nAxisPositionWidth, nAxisNameWidth, nAxisVelocityWidth, 40);
		AxisRect.nLeft = AxisPosLine.nLeft - (DINT)(nWidestWidth / 2.0) - 4;
		AxisRect.nTop = AxisPosLine.nBottom;
		AxisRect.nWidth = nWidestWidth + 8;
		AxisRect.nHeight = 8;
		if(pAxis->bShowAxisActPosition == 1)
		{
			AxisRect.nHeight += (DINT)pAxis->AxisValueFont.rCharHeight;
		}
		if(pAxis->bShowAxisName == 1)
		{
			AxisRect.nHeight += (DINT)pAxis->AxisNameFont.rCharHeight;
		}
		if(pAxis->bShowAxisActVelocity == 1)
		{
			AxisRect.nHeight += (DINT)pAxis->AxisValueFont.rCharHeight;
		}
		if(AxisRect.nHeight == 8)
		{
			AxisRect.nHeight = 10;
		}
		AxisRect.nFillColor = eBRB_COLOR_TRANSPARENT;
		AxisRect.nBorderColor = pAxis->nAxisColor;
		AxisRect.nDashWidth = 0;
		if(pAxis->bShowAxisBorder == 1)
		{
			BrbVc4DrawRectangle(&AxisRect, pGeneral);
		}
		rResult = rAxisClipMin;
	}
	else
	{
		// Vertical
		BrbVc4Rectangle_TYP DrawAreaAx;
		DrawAreaAx.nLeft = DrawArea.nLeft;
		DrawAreaAx.nTop = DrawArea.nTop + pAxis->nDrawIndent;
		DrawAreaAx.nWidth = DrawArea.nWidth;
		DrawAreaAx.nHeight = DrawArea.nHeight - 2 * pAxis->nDrawIndent;
		DrawAreaAx.nFillColor = DrawArea.nFillColor;
		DrawAreaAx.nBorderColor = 0;
		DrawAreaAx.nDashWidth = 0;
		DINT nX = DrawArea.nLeft;
		// Ausschnitt
		REAL rAxisClipMin = (REAL)pAxis->nAxisLimitMin;
		REAL rAxisClipMax = (REAL)pAxis->nAxisLimitMax;
		if(pAxis->bClip == 1)
		{
			if(pAxis->nAxisClipRange == 0)
			{
				pAxis->nAxisClipRange = 1000;
			}
			// Wegen DINT-�berlauf mu� hier mit REAL gerechnet werden
			LREAL rClipRangeHalf = pAxis->nAxisClipRange / 2.0;
			// Modulo funktioniert nur korrekt im positiven Bereich
			// Daher wird die Position auf >= 0 normiert
			LREAL rActNorm = (LREAL)((LREAL)pAxis->rAxisActPosition - (LREAL)pAxis->nAxisLimitMin + (LREAL)pAxis->nAxisClipRange);
			LREAL rClipDiff = rActNorm - rClipRangeHalf;
			DINT nClipDiff = (DINT)rClipDiff;
			DINT nClipMod = nClipDiff % pAxis->nAxisClipRange;
			// Die Normnierung wird r�ckg�ngig gemacht
			LREAL rClipScale = rActNorm - (LREAL)nClipMod - (LREAL)pAxis->nAxisClipRange + (LREAL)pAxis->nAxisLimitMin;
			// und der berechnete Bereich gesetzt
			rAxisClipMin = (REAL)rClipScale;
			rAxisClipMax = (REAL)rAxisClipMin + (REAL)pAxis->nAxisClipRange;
		}
		// Skalierung
		BrbVc4DrawText_TYP AxisScaleCaption;
		UDINT nMinLen = GetDintAsText((DINT)rAxisClipMin, AxisScaleCaption.sText, sizeof(AxisScaleCaption.sText));
		UDINT nMaxLen = GetDintAsText((DINT)rAxisClipMax, AxisScaleCaption.sText, sizeof(AxisScaleCaption.sText));
		UDINT nMinMaxWidestLen = GetGreatestValue(nMinLen, nMaxLen, 0, 0);
		if(pAxis->bShowAxisScale == 1)
		{
			// Skalierungsschritte
			AxisScaleCaption.nLeft = nX;
			AxisScaleCaption.nFontIndex = pAxis->AxisScaleFont.nIndex;
			AxisScaleCaption.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
			BrbVc4Line_TYP ScaleLine;
			ScaleLine.nLeft = AxisScaleCaption.nLeft;
			ScaleLine.nRight = ScaleLine.nLeft + 4;
			ScaleLine.nColor = pAxis->nAxisScaleColor;
			ScaleLine.nDashWidth = 0;
			if(pAxis->nAxisScaleCount > 0)
			{
				UDINT nTextWidth = (DINT)(nMinMaxWidestLen * pAxis->AxisScaleFont.rCharWidth);
				ScaleLine.nLeft += nTextWidth + 4;
				ScaleLine.nRight = ScaleLine.nLeft + 4;
				UINT nScaleIndex = 0;
				REAL rScaleDistance = (rAxisClipMax - rAxisClipMin) / pAxis->nAxisScaleCount;
				for(nScaleIndex=0; nScaleIndex<=pAxis->nAxisScaleCount; nScaleIndex++)
				{
					REAL rValue = rAxisClipMin + (nScaleIndex * rScaleDistance);
					ScaleLine.nTop = ScaleAxisPosition(rValue, rAxisClipMin, rAxisClipMax, DrawAreaAx.nTop, DrawAreaAx.nTop + DrawAreaAx.nHeight, pAxis->bInverted);
					ScaleLine.nBottom = ScaleLine.nTop;
					BrbVc4DrawLine(&ScaleLine, pGeneral);
					DINT nValue = (DINT)rValue;
					if(rValue > 2147483647.0)
					{
						nValue = 2147483647.0;
					}
					nTextLen = GetDintAsText(nValue, AxisScaleCaption.sText, sizeof(AxisScaleCaption.sText));
					AxisScaleCaption.nLeft = ScaleLine.nLeft - (DINT)(nTextLen * pAxis->AxisScaleFont.rCharWidth) - 4;
					AxisScaleCaption.nTop = ScaleLine.nTop - (DINT)(pAxis->AxisScaleFont.rCharHeight / 2.0);
					if(pAxis->bHighlightActPosition == 1 && (pAxis->rAxisActPosition == rValue))
					{
						AxisScaleCaption.nTextColor = pAxis->nAxisScaleHighlightColor;
					}
					else
					{
						AxisScaleCaption.nTextColor = pAxis->nAxisScaleColor;
					}
					BrbVc4DrawText(&AxisScaleCaption, pGeneral);
				}
			}
			// Skalierung-Basis
			BrbVc4Line_TYP AxisScaleBase;
			AxisScaleBase.nLeft = ScaleLine.nLeft + 2;
			AxisScaleBase.nRight = AxisScaleBase.nLeft;
			AxisScaleBase.nTop = DrawAreaAx.nTop;
			AxisScaleBase.nBottom = DrawAreaAx.nTop + DrawAreaAx.nHeight;
			AxisScaleBase.nColor = pAxis->nAxisScaleColor;
			AxisScaleBase.nDashWidth = 0;
			BrbVc4DrawLine(&AxisScaleBase, pGeneral);
			nX = AxisScaleBase.nLeft;
		}
		// Achs-Soll-Position
		BrbVc4Line_TYP AxisSetPosLine;
		AxisSetPosLine.nLeft = nX;
		AxisSetPosLine.nRight = AxisSetPosLine.nLeft + 16;
		AxisSetPosLine.nTop = ScaleAxisPosition(pAxis->rAxisSetPosition, rAxisClipMin, rAxisClipMax, DrawAreaAx.nTop, DrawAreaAx.nTop + DrawAreaAx.nHeight, pAxis->bInverted);
		AxisSetPosLine.nBottom = AxisSetPosLine.nTop;
		AxisSetPosLine.nColor = pAxis->nAxisSetPositionColor;
		AxisSetPosLine.nDashWidth = 0;
		if(pAxis->bShowAxisSetPosition == 1 && (pAxis->rAxisSetPosition >= rAxisClipMin) && (pAxis->rAxisSetPosition <= rAxisClipMax))
		{
			BrbVc4DrawLine(&AxisSetPosLine, pGeneral);
		}
		// Achs-Positions-Linie
		BrbVc4Line_TYP AxisPosLine;
		AxisPosLine.nLeft = nX;
		AxisPosLine.nRight = AxisPosLine.nLeft + 8;
		AxisPosLine.nTop = ScaleAxisPosition(pAxis->rAxisActPosition, rAxisClipMin, rAxisClipMax, DrawAreaAx.nTop, DrawAreaAx.nTop + DrawAreaAx.nHeight, pAxis->bInverted);
		AxisPosLine.nBottom = AxisPosLine.nTop;
		AxisPosLine.nColor = pAxis->nAxisColor;
		AxisPosLine.nDashWidth = 0;
		if(pAxis->bShowAxisPosLine == 1)
		{
			BrbVc4DrawLine(&AxisPosLine, pGeneral);
		}
		// Beschriftung
		UINT nCaptionOrderName = 0;
		UINT nCaptionOrderPosition = 1;
		UINT nCaptionOrderVelocity = 1;
		if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_NAME_POS_VEL)
		{
			nCaptionOrderName = 0;
			nCaptionOrderPosition = 1;
			nCaptionOrderVelocity = 2;
		}
		else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_NAME_VEL_POS)
		{
			nCaptionOrderName = 0;
			nCaptionOrderPosition = 2;
			nCaptionOrderVelocity = 1;
		}
		else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_POS_NAME_VEL)
		{
			nCaptionOrderName = 1;
			nCaptionOrderPosition = 0;
			nCaptionOrderVelocity = 2;
		}
		else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_POS_VEL_NAME)
		{
			nCaptionOrderName = 2;
			nCaptionOrderPosition = 0;
			nCaptionOrderVelocity = 1;
		}
		else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_VEL_NAME_POS)
		{
			nCaptionOrderName = 1;
			nCaptionOrderPosition = 2;
			nCaptionOrderVelocity = 0;
		}
		else if(pAxis->eAxisCaptionOrder == eBRB_DRAW_AXIS_VEL_POS_NAME)
		{
			nCaptionOrderName = 2;
			nCaptionOrderPosition = 1;
			nCaptionOrderVelocity = 0;
		}
		DINT nCaptionHeight = 0;
		BrbVc4DrawText_TYP AxisPosition;
		UINT nAxisPositionLen = GetRealAsText(pAxis->rAxisActPosition, AxisPosition.sText, sizeof(AxisPosition.sText));
		DINT nAxisPositionWidth = 0;
		if(pAxis->bShowAxisActPosition == 1)
		{
			nAxisPositionWidth = (DINT)(nAxisPositionLen * pAxis->AxisValueFont.rCharWidth);
			nCaptionHeight += (DINT)pAxis->AxisValueFont.rCharHeight;
		}
		BrbVc4DrawText_TYP AxisName;
		strcpy(AxisName.sText, pAxis->sAxisName);
		UINT nAxisNameLen = strlen(AxisName.sText);
		DINT nAxisNameWidth = 0;
		if(pAxis->bShowAxisName == 1)
		{
			nAxisNameWidth = (DINT)(nAxisNameLen * pAxis->AxisNameFont.rCharWidth);
			nCaptionHeight += (DINT)pAxis->AxisNameFont.rCharHeight;
		}
		BrbVc4DrawText_TYP AxisVelocity;
		UINT nAxisVelocityLen = GetRealAsText(pAxis->rAxisActVelocity, AxisVelocity.sText, sizeof(AxisVelocity.sText));
		DINT nAxisVelocityWidth = 0;
		if(pAxis->bShowAxisActVelocity == 1)
		{
			nAxisVelocityWidth = (DINT)(nAxisVelocityLen * pAxis->AxisValueFont.rCharWidth);
			nCaptionHeight += (DINT)pAxis->AxisValueFont.rCharHeight;
		}
		DINT nWidestWidth = GetGreatestValue(nAxisPositionWidth, nAxisNameWidth, nAxisVelocityWidth, 60);
		DINT nY = AxisPosLine.nTop - (DINT)(nCaptionHeight / 2.0) - 3;
		UINT nCaptionIndex = 0;
		if(pAxis->bShowAxisName == 1)
		{
			nAxisNameWidth = (DINT)(nAxisNameLen * pAxis->AxisNameFont.rCharWidth);
		}
		for(nCaptionIndex=0; nCaptionIndex<=3; nCaptionIndex++)
		{
			if(nCaptionIndex == nCaptionOrderName)
			{
				if(pAxis->bShowAxisName == 1)
				{
					AxisName.nLeft = AxisPosLine.nRight + (DINT)(nWidestWidth / 2.0) - (DINT)(nAxisNameWidth / 2.0) + 2;
					AxisName.nTop = nY;
					AxisName.nFontIndex = pAxis->AxisNameFont.nIndex;
					AxisName.nTextColor = pAxis->nAxisNameColor;
					AxisName.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
					BrbVc4DrawText(&AxisName, pGeneral);
					nY += (DINT)pAxis->AxisNameFont.rCharHeight;
				}
		
			}
			if(nCaptionIndex == nCaptionOrderPosition)
			{
				if(pAxis->bShowAxisActPosition == 1)
				{
					AxisPosition.nLeft = AxisPosLine.nRight + (DINT)(nWidestWidth / 2.0) - (DINT)(nAxisPositionWidth / 2.0) + 2;
					AxisPosition.nTop = nY;
					AxisPosition.nFontIndex = pAxis->AxisValueFont.nIndex;
					AxisPosition.nTextColor = pAxis->nAxisActPositionColor;
					AxisPosition.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
					BrbVc4DrawText(&AxisPosition, pGeneral);
					nY += (DINT)pAxis->AxisValueFont.rCharHeight;
				}

			}
			if(nCaptionIndex == nCaptionOrderVelocity)
			{
				if(pAxis->bShowAxisActVelocity == 1)
				{
					AxisVelocity.nLeft = AxisPosLine.nRight + (DINT)(nWidestWidth / 2.0) - (DINT)(nAxisVelocityWidth / 2.0) + 2;
					AxisVelocity.nTop = nY;
					AxisVelocity.nFontIndex = pAxis->AxisValueFont.nIndex;
					AxisVelocity.nTextColor = pAxis->nAxisActVelocityColor;
					AxisVelocity.nBackgroundColor = eBRB_COLOR_TRANSPARENT;
					BrbVc4DrawText(&AxisVelocity, pGeneral);
					nY += (DINT)pAxis->AxisValueFont.rCharHeight;
				}
			}
		}
		// Rahmen
		BrbVc4Rectangle_TYP AxisRect;
		AxisRect.nLeft = AxisPosLine.nRight;
		AxisRect.nTop = AxisPosLine.nTop - 3;
		AxisRect.nWidth = nWidestWidth + 4;
		AxisRect.nHeight = 6;
		if(pAxis->bShowAxisActPosition == 1)
		{
			AxisRect.nTop -= (DINT)(pAxis->AxisValueFont.rCharHeight / 2.0);
			AxisRect.nHeight += (DINT)pAxis->AxisValueFont.rCharHeight;
		}
		if(pAxis->bShowAxisName == 1)
		{
			AxisRect.nTop -= (DINT)(pAxis->AxisNameFont.rCharHeight / 2.0);
			AxisRect.nHeight += (DINT)pAxis->AxisNameFont.rCharHeight;
		}
		if(pAxis->bShowAxisActVelocity == 1)
		{
			AxisRect.nTop -= (DINT)(pAxis->AxisValueFont.rCharHeight / 2.0);
			AxisRect.nHeight += (DINT)pAxis->AxisValueFont.rCharHeight;
		}
		if(AxisRect.nHeight == 8)
		{
			AxisRect.nHeight = 10;
		}
		AxisRect.nFillColor = eBRB_COLOR_TRANSPARENT;
		AxisRect.nBorderColor = pAxis->nAxisColor;
		AxisRect.nDashWidth = 0;
		if(pAxis->bShowAxisBorder == 1)
		{
			BrbVc4DrawRectangle(&AxisRect, pGeneral);
		}
		rResult = rAxisClipMin;
	}
	return rResult;
}

DINT ScaleAxisPosition(REAL rAxPos, REAL rAxMin, REAL rAxMax, DINT nDrawMin, DINT nDrawMax, BOOL bInverted)
{
	DINT nResult = 0;
	REAL rAxRange = rAxMax - rAxMin;
	DINT nDrawRange = nDrawMax - nDrawMin;
	REAL rFactor = 1;
	if(rAxRange != 0)
	{
		rFactor = (REAL)nDrawRange / rAxRange;
	}
	nResult = (DINT)((rAxPos - rAxMin) * rFactor);
	if(bInverted == 1)
	{
		nResult = nDrawMax - nResult;
	}
	else
	{
		nResult += nDrawMin;
	}
	return nResult;
}

UDINT GetDintAsText(DINT nValue, STRING* pText, UDINT nTextSize)
{
	UDINT nResult = 0;
	memset(pText, 0, sizeof(nTextSize));
	brsitoa(nValue, (UDINT)pText);
	if(nValue > 0)
	{
		BrbStringInsert(pText, 0, "+");
	}
	nResult = strlen(pText);
	return nResult;
}

UDINT GetRealAsText(REAL rValue, STRING* pText, UDINT nTextSize)
{
	UDINT nResult = 0;
	memset(pText, 0, sizeof(nTextSize));
	brsftoa(rValue, (UDINT)pText);
	if(rValue > 0.0)
	{
		BrbStringInsert(pText, 0, "+");
	}
	nResult = strlen(pText);
	return nResult;
}

DINT GetGreatestValue(DINT nValue1, DINT nValue2, DINT nValue3, DINT nValue4)
{
	DINT nResult = nValue1;
	if(nValue2 > nResult)
	{
		nResult = nValue2;
	}
	if(nValue3 > nResult)
	{
		nResult = nValue3;
	}
	if(nValue4 > nResult)
	{
		nResult = nValue4;
	}
	return nResult;
}
