/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleJogButton.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r einen  Jog-Button */
plcbit BrbVc4HandleJogButton(struct BrbVc4JogButton_TYP* pButton)
{
	if(pButton->fbUnpush.PT == 0)
	{
		pButton->fbUnpush.PT = 1000;
	}
	if(pButton->bSuppressTimeout == 0)
	{
		TON(&pButton->fbUnpush);
	}
	if(pButton->fbUnpush.Q == 1)
	{
		// Loslassen emulieren
		pButton->bPushed = 0;
		pButton->bPushedOld = 1;
	}
	pButton->bEnabled = BrbVc4IsControlEnabled(pButton->nStatus);
	if(pButton->bEnabled == 0 && pButton->bEnabledOld == 1)
	{
		// Button wurde gerade disabled
		pButton->bPushed = 0;
		pButton->bPushedOld = 0;
		pButton->eJogState = eBRB_JOG_BTN_STATE_UNPUSHED;
		pButton->fbUnpush.IN = 0;
		pButton->nBmpIndex = 0;
	}
	else if(pButton->bEnabled == 0)
	{
		// Button ist disabled
		pButton->bPushed = 0;
		pButton->bPushedOld = 0;
		pButton->eJogState = eBRB_JOG_BTN_STATE_UNPUSHED;
		pButton->fbUnpush.IN = 0;
		pButton->nBmpIndex = 0;
	}
	else
	{
		// Button ist enabled
		pButton->nBmpIndex = 1;
		if(pButton->bPushed == 0 && pButton->bPushedOld == 0)
		{
			pButton->eJogState = eBRB_JOG_BTN_STATE_UNPUSHED;
			pButton->fbUnpush.IN = 0;
		}
		else if(pButton->bPushed == 0 && pButton->bPushedOld == 1)
		{
			pButton->eJogState = eBRB_JOG_BTN_STATE_UNPUSHED_EDGE;
			pButton->fbUnpush.IN = 0;
		}
		else if(pButton->bPushed == 1 && pButton->bPushedOld == 1)
		{
			pButton->eJogState = eBRB_JOG_BTN_STATE_PUSHED;
			pButton->fbUnpush.IN = 1;
		}
		else if(pButton->bPushed == 1 && pButton->bPushedOld == 0)
		{
			pButton->eJogState = eBRB_JOG_BTN_STATE_PUSHED_EDGE;
			pButton->fbUnpush.IN = 1;
		}
		pButton->bPushedOld = pButton->bPushed;
	}
	pButton->bEnabledOld = pButton->bEnabled;
	return pButton->bPushed;
}
