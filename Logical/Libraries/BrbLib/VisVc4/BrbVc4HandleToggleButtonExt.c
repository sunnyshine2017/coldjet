/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleToggleButtonExt.c
 * Author: niedermeierr
 * Created: September 02, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r einen erweiterten ToggleButton */
plcbit BrbVc4HandleToggleButtonExt(struct BrbVc4ToggleButtonExt_TYP* pButton)
{
	BOOL bPushedChanged = 0;
	if(BrbVc4IsControlEnabled(pButton->nStatus) == 0)
	{
		pButton->bVisPushed = 0;
		pButton->bVisPushedOld = 0;
		pButton->bPushedOld = pButton->bPushed;
		pButton->nBmpIndex = 0;
	}
	else
	{
		pButton->nBmpIndex = 1;
		if(pButton->bPushed != pButton->bPushedOld)
		{
			// Umschaltung von Programm
			pButton->bVisPushed = pButton->bPushed;
		}
		else if(pButton->bVisPushed != pButton->bVisPushedOld)
		{
			// Umschaltung von Visu
			if(pButton->bVisPushed == 0 && pButton->bVisPushedOld == 1)
			{
				pButton->bPushed = pButton->bVisPushed;
				bPushedChanged = 1;
			}
			else if(pButton->bVisPushed == 1 && pButton->bVisPushedOld == 0)
			{
				pButton->bPushed = pButton->bVisPushed;
				bPushedChanged = 1;
			}
		}
		else
		{
			pButton->bVisPushed = pButton->bPushed;
		}
		if(pButton->bVisPushed == 0 && pButton->bVisPushedOld == 0)
		{
			pButton->eToggleState = eBRB_TOG_BTN_STATE_UNPUSHED;
		}
		else if(pButton->bVisPushed == 0 && pButton->bVisPushedOld == 1)
		{
			pButton->eToggleState = eBRB_TOG_BTN_STATE_UNPUSHED_EDGE;
		}
		else if(pButton->bVisPushed == 1 && pButton->bVisPushedOld == 1)
		{
			pButton->eToggleState = eBRB_TOG_BTN_STATE_PUSHED;
		}
		else if(pButton->bVisPushed == 1 && pButton->bVisPushedOld == 0)
		{
			pButton->eToggleState = eBRB_TOG_BTN_STATE_PUSHED_EDGE;
		}
	}
	pButton->bPushedOld = pButton->bPushed;
	pButton->bVisPushedOld = pButton->bVisPushed;
	return bPushedChanged;
}
