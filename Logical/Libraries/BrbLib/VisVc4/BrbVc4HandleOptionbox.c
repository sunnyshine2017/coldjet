/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleOptionbox.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r eine Optionbox */
plcbit BrbVc4HandleOptionbox(struct BrbVc4Optionbox_TYP* pOptionbox)
{
	BOOL	bClicked;
	bClicked = 0;
	if(BrbVc4IsControlEnabled(pOptionbox->nStatus) == 0)
	{
		pOptionbox->bClicked = 0;
		pOptionbox->nBmpIndex = pOptionbox->bChecked;
		pOptionbox->nTextColor = eBRB_COLOR_DISABLED;
	}
	else
	{
		if(pOptionbox->bClicked == 1)
		{
			pOptionbox->bClicked = 0;
			pOptionbox->bChecked = 1;
			bClicked = 1;
		}
		pOptionbox->nBmpIndex = 2 + pOptionbox->bChecked;
		pOptionbox->nTextColor = eBRB_COLOR_ENABLED;
	}
	return bClicked;
}
