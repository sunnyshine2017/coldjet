/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleButton.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r einen normalen Button */
plcbit BrbVc4HandleButton(struct BrbVc4Button_TYP* pButton)
{
	if(BrbVc4IsControlEnabled(pButton->nStatus) == 0)
	{
		pButton->nBmpIndex = 0;
	}
	else
	{
		pButton->nBmpIndex = 1;
	}
	return pButton->bClicked;
}
