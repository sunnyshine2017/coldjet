
/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleChangePage.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Behandelt die Seitenumschaltung */
unsigned short BrbVc4HandleChangePage(struct BrbVc4PageHandling_TYP* pPageHandling)
{
	// Pr�fen, ob die Seite schon umgeschaltet wurde
	if(pPageHandling->nPageCurrent == pPageHandling->nPageNext)
	{
		pPageHandling->bPageChangeInProgress = 0;
	}
	else
	{
		// Seite umschalten
		pPageHandling->nPageChange = pPageHandling->nPageNext;
		pPageHandling->bPageInit = 1;
	}
	return 0;
}
