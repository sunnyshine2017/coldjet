/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleScrollbarVertical.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r eine vertikale Scrollbar */
plcbit BrbVc4HandleScrollbarVertical(struct BrbVc4ScrollbarVer_TYP* pScrollbar, signed long* pScrollOffset)
{
	pScrollbar->nScrollTotal = pScrollbar->nTotalIndexMax - pScrollbar->nTotalIndexMin + 1;
	UDINT nScrollOffsetMax = 0;
	BOOL bScrollDone = 0;

	// Maximalen Scrolloffset berechnen
	if(pScrollbar->nScrollTotal <= pScrollbar->nCountShow)
	{
		nScrollOffsetMax = pScrollbar->nTotalIndexMin;
	}
	else
	{
		nScrollOffsetMax = pScrollbar->nTotalIndexMax - pScrollbar->nCountShow + 1;
	}

	if(pScrollbar->bDisabled == 0)
	{
		BOOL bIsOnTop;
		BOOL bIsOnBottom;
		BOOL bNoScroll = (pScrollbar->nScrollTotal <= pScrollbar->nCountShow);
		if(bNoScroll == 1)
		{
			bIsOnTop = 1;
			bIsOnBottom = 1;
		}
		else
		{
			bIsOnTop = (*pScrollOffset <= pScrollbar->nTotalIndexMin);
			bIsOnBottom = (*pScrollOffset >= nScrollOffsetMax);
		}
		BrbVc4SetControlEnability(&pScrollbar->btnTop.nStatus, !bIsOnTop);
		BrbVc4SetControlEnability(&pScrollbar->btnPageUp.nStatus, !bIsOnTop);
		BrbVc4SetControlEnability(&pScrollbar->btnLineUp.nStatus, !bIsOnTop);
		BrbVc4SetControlEnability(&pScrollbar->btnLineDown.nStatus, !bIsOnBottom);
		BrbVc4SetControlEnability(&pScrollbar->btnPageDown.nStatus, !bIsOnBottom);
		BrbVc4SetControlEnability(&pScrollbar->btnBottom.nStatus, !bIsOnBottom);
		if(BrbVc4HandleButton(&pScrollbar->btnTop) == 1)
		{
			pScrollbar->btnTop.bClicked = 0;
			*pScrollOffset = pScrollbar->nTotalIndexMin;
			bScrollDone = 1;
		}
		BrbVc4HandleIncButton(&pScrollbar->btnPageUp);
		if(pScrollbar->btnPageUp.eIncState == eBRB_INC_BTN_STATE_PUSHED_EDGE || pScrollbar->btnPageUp.eIncState == eBRB_INC_BTN_STATE_PUSHED_INC)
		{
			*pScrollOffset -= pScrollbar->nCountShow;
			bScrollDone = 1;
		}
		BrbVc4HandleIncButton(&pScrollbar->btnLineUp);
		if(pScrollbar->btnLineUp.eIncState == eBRB_INC_BTN_STATE_PUSHED_EDGE || pScrollbar->btnLineUp.eIncState == eBRB_INC_BTN_STATE_PUSHED_INC)
		{
			*pScrollOffset -= 1;
			bScrollDone = 1;
		}
		BrbVc4HandleIncButton(&pScrollbar->btnLineDown);
		if(pScrollbar->btnLineDown.eIncState == eBRB_INC_BTN_STATE_PUSHED_EDGE || pScrollbar->btnLineDown.eIncState == eBRB_INC_BTN_STATE_PUSHED_INC)
		{
			*pScrollOffset += 1;
			bScrollDone = 1;
		}
		BrbVc4HandleIncButton(&pScrollbar->btnPageDown);
		if(pScrollbar->btnPageDown.eIncState == eBRB_INC_BTN_STATE_PUSHED_EDGE || pScrollbar->btnPageDown.eIncState == eBRB_INC_BTN_STATE_PUSHED_INC)
		{
			*pScrollOffset += pScrollbar->nCountShow;
			bScrollDone = 1;
		}
		if(BrbVc4HandleButton(&pScrollbar->btnBottom) == 1)
		{
			pScrollbar->btnBottom.bClicked = 0;
			*pScrollOffset = nScrollOffsetMax;
			bScrollDone = 1;
		}
	}
	else
	{
		BrbVc4SetControlEnability(&pScrollbar->btnTop.nStatus, 0);
		BrbVc4SetControlEnability(&pScrollbar->btnPageUp.nStatus, 0);
		BrbVc4SetControlEnability(&pScrollbar->btnLineUp.nStatus, 0);
		BrbVc4SetControlEnability(&pScrollbar->btnLineDown.nStatus, 0);
		BrbVc4SetControlEnability(&pScrollbar->btnPageDown.nStatus, 0);
		BrbVc4SetControlEnability(&pScrollbar->btnBottom.nStatus, 0);
	}
	pScrollbar->btnTop.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnTop.nStatus);
	pScrollbar->btnPageUp.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnPageUp.nStatus);
	pScrollbar->btnLineUp.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnLineUp.nStatus);
	pScrollbar->btnLineDown.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnLineDown.nStatus);
	pScrollbar->btnPageDown.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnPageDown.nStatus);
	pScrollbar->btnBottom.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnBottom.nStatus);
	// Scrolloffset begrenzen
	if(*pScrollOffset < pScrollbar->nTotalIndexMin)
	{
		*pScrollOffset = pScrollbar->nTotalIndexMin;
		bScrollDone = 1;
	}
	if(*pScrollOffset > nScrollOffsetMax)
	{
		*pScrollOffset = nScrollOffsetMax;
		bScrollDone = 1;
	}
	// Durch die Defaultbesetzung am Schluss kann sie vor dem Aufruf auch ge�ndert werden
	pScrollbar->btnPageUp.fbDelay.PT = 300;
	pScrollbar->btnPageUp.fbRepeat.PT = 200;
	pScrollbar->btnLineUp.fbDelay.PT = 300;
	pScrollbar->btnLineUp.fbRepeat.PT = 200;
	pScrollbar->btnLineDown.fbDelay.PT = 300;
	pScrollbar->btnLineDown.fbRepeat.PT = 200;
	pScrollbar->btnPageDown.fbDelay.PT = 300;
	pScrollbar->btnPageDown.fbRepeat.PT = 200;
	pScrollbar->bScrollDone = bScrollDone;
	return bScrollDone;
}
