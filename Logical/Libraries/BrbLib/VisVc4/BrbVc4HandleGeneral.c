/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleGeneral.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <string.h>
#include <AnsiCFunc.h>

#ifdef __cplusplus
	};
#endif

/* Logik f�r allgemeine Vis-Funktionalit�ten */
plcbit BrbVc4HandleGeneral(struct BrbVc4General_TYP* pVisGeneral)
{
	if(pVisGeneral->tDoubleclickDelay == 0)
	{
		pVisGeneral->tDoubleclickDelay = 100;
	}
	pVisGeneral->Touch.fbDoubleClickDelay.PT = pVisGeneral->tDoubleclickDelay;
	if(pVisGeneral->nDoubleclickDrift == 0)
	{
		pVisGeneral->nDoubleclickDrift = 8;
	}
	pVisGeneral->Touch.eState = eBRB_TOUCH_STATE_UNPUSHED;
	if(pVisGeneral->nVcHandle == 0)
	{
		pVisGeneral->nVcHandle	= VA_Setup(1 , pVisGeneral->sVisName);
	}
	if(pVisGeneral->nVcHandle != 0)
	{
		if(!VA_Saccess(1, pVisGeneral->nVcHandle))
		{
			VA_GetTouchAction (1, pVisGeneral->nVcHandle, 0, (UDINT)&pVisGeneral->Touch.TouchAction);
			pVisGeneral->Touch.nX = pVisGeneral->Touch.TouchAction.x;
			pVisGeneral->Touch.nY = pVisGeneral->Touch.TouchAction.y;
			if(pVisGeneral->Touch.TouchAction.status == 1 && pVisGeneral->Touch.TouchActionOld.status == 2)
			{
				pVisGeneral->Touch.eState = eBRB_TOUCH_STATE_PUSHED_EDGE;
				pVisGeneral->Touch.eStateSync = eBRB_TOUCH_STATE_PUSHED_EDGE;
				if(pVisGeneral->Touch.fbDoubleClickDelay.Q == 0)
				{
					UDINT nDriftX = (UDINT)abs(pVisGeneral->Touch.TouchActionOld.x - pVisGeneral->Touch.TouchAction.x);
					UDINT nDriftY = (UDINT)abs(pVisGeneral->Touch.TouchActionOld.y - pVisGeneral->Touch.TouchAction.y);
					if(nDriftX <= pVisGeneral->nDoubleclickDrift && nDriftY <= pVisGeneral->nDoubleclickDrift)
					{
						pVisGeneral->Touch.eState = eBRB_TOUCH_STATE_DOUBLECLICK;
						pVisGeneral->Touch.eStateSync = eBRB_TOUCH_STATE_DOUBLECLICK;
					}
				}
				pVisGeneral->Touch.fbDoubleClickDelay.IN = 0;
			}
			else if(pVisGeneral->Touch.TouchAction.status == 1 && pVisGeneral->Touch.TouchActionOld.status == 1)
			{
				pVisGeneral->Touch.eState = eBRB_TOUCH_STATE_PUSHED;
				pVisGeneral->Touch.fbDoubleClickDelay.IN = 0;
			}
			else if(pVisGeneral->Touch.TouchAction.status == 2 && pVisGeneral->Touch.TouchActionOld.status == 1)
			{
				pVisGeneral->Touch.eState = eBRB_TOUCH_STATE_UNPUSHED_EDGE;
				pVisGeneral->Touch.eStateSync = eBRB_TOUCH_STATE_UNPUSHED_EDGE;
				pVisGeneral->Touch.fbDoubleClickDelay.IN = 1;
			}
			else
			{
				pVisGeneral->Touch.eState = eBRB_TOUCH_STATE_UNPUSHED;
				pVisGeneral->Touch.fbDoubleClickDelay.IN = 1;
			}
			memcpy(&pVisGeneral->Touch.TouchActionOld, &pVisGeneral->Touch.TouchAction, sizeof(pVisGeneral->Touch.TouchActionOld));
			VA_Srelease(1, pVisGeneral->nVcHandle);
		}
	}
	TON(&pVisGeneral->Touch.fbDoubleClickDelay);
	if(pVisGeneral->nRedrawCycles > 0)
	{
		if(pVisGeneral->nRedrawCounter == 0)
		{
			pVisGeneral->Touch.eStateSync = pVisGeneral->Touch.eState;
		}
		if(pVisGeneral->nRedrawCounter >= pVisGeneral->nRedrawCycles)
		{
			pVisGeneral->nRedrawCounter = 0;
			if(pVisGeneral->nVcHandle != 0 && pVisGeneral->bDisableRedraw == 0)
			{
				// Zugriff reservieren
				UINT nAccessStatus = VA_Saccess(1, pVisGeneral->nVcHandle);
				if(nAccessStatus == 0)
				{
					// Bisherige Zeichnung l�schen
					VA_Redraw(1, pVisGeneral->nVcHandle);
					// Zugriff freigeben
					VA_Srelease(1, pVisGeneral->nVcHandle);
				}
			}
		}
		else
		{
			pVisGeneral->nRedrawCounter += 1;
		}
	}
	else
	{
		pVisGeneral->Touch.eStateSync = pVisGeneral->Touch.eState;
	}
	return 0;
}
