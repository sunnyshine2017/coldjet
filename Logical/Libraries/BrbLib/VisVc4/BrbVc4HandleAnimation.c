/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleAnimation.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r eine Bitmap-Animation */
unsigned short BrbVc4HandleAnimation(struct BrbVc4Animation_TYP* pAnimation)
{
	if(pAnimation->bEnable == 1)
	{
		TON(&pAnimation->fbTimer);
		pAnimation->fbTimer.IN = 1;
		if(pAnimation->fbTimer.Q == 1)
		{
			pAnimation->fbTimer.IN = 0;
			pAnimation->nIndex++;
			if(pAnimation->nIndex > pAnimation->nIndexMax)
			{
				pAnimation->nIndex = pAnimation->nIndexMin;
			}
		}
	}
	else
	{
		pAnimation->nIndex = pAnimation->nIndexStanding;
	}
	return 0;
}
