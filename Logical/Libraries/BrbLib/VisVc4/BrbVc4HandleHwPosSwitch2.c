/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleHwPosSwitch2.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r einen Hardware-2-Positions-Schalter */
enum BrbVc4HwPosSwitchPositions_ENUM BrbVc4HandleHwPosSwitch2(struct BrbVc4HwPosSwitch2_TYP* pPosSwitch)
{
	if(pPosSwitch->bPos0 == 1 && pPosSwitch->bPos2 == 0)
	{
		pPosSwitch->ePos = eBRB_HPS_POS_0;
		if(pPosSwitch->ePosOld != pPosSwitch->ePos && pPosSwitch->ePosOld != eBRB_HPS_POS_0_EDGE)
		{
			pPosSwitch->ePos = eBRB_HPS_POS_0_EDGE;
		}
	}
	else if(pPosSwitch->bPos0 == 0 && pPosSwitch->bPos2 == 0)
	{
		pPosSwitch->ePos = eBRB_HPS_POS_1;
		if(pPosSwitch->ePosOld != pPosSwitch->ePos && pPosSwitch->ePosOld != eBRB_HPS_POS_1_EDGE)
		{
			pPosSwitch->ePos = eBRB_HPS_POS_1_EDGE;
		}
	}
	else if(pPosSwitch->bPos0 == 0 && pPosSwitch->bPos2 == 1)
	{
		pPosSwitch->ePos = eBRB_HPS_POS_2;
		if(pPosSwitch->ePosOld != pPosSwitch->ePos && pPosSwitch->ePosOld != eBRB_HPS_POS_2_EDGE)
		{
			pPosSwitch->ePos = eBRB_HPS_POS_2_EDGE;
		}
	}
	else
	{
		pPosSwitch->ePos = eBRB_HPS_POS_UNDEF;
	}
	BrbVc4SetControlVisibility(&pPosSwitch->nSwitchStatus0, (pPosSwitch->bPos0 == 1 && pPosSwitch->bPos2 == 0));
	BrbVc4SetControlVisibility(&pPosSwitch->nSwitchStatus1, (pPosSwitch->bPos0 == 0 && pPosSwitch->bPos2 == 0));
	BrbVc4SetControlVisibility(&pPosSwitch->nSwitchStatus2, (pPosSwitch->bPos0 == 0 && pPosSwitch->bPos2 == 1));
	pPosSwitch->ePosOld = pPosSwitch->ePos;
	return pPosSwitch->ePos;
}
