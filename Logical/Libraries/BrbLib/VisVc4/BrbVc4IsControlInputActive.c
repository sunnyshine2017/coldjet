/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4IsControlInputActive.c
 * Author: niedermeierr
 * Created: September 09, 2015
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Gibt zur�ck, ob bei einem Vis-Control die Eingabe aktiv ist */
plcbit BrbVc4IsControlInputActive(unsigned short nStatus)
{
	return ((nStatus & 32768) > 0);
}
