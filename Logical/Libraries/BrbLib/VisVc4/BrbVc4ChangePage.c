/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4ChangePage.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Schaltet die Visu-Seite um */
plcbit BrbVc4ChangePage(struct BrbVc4PageHandling_TYP* pPageHandling, signed long nPageIndex, plcbit bStoreLastPage)
{
	BOOL bResult = 0;
	// Pr�fen, ob Seite umgeschaltet werden mu�
	if((nPageIndex != pPageHandling->nPageCurrent) || (pPageHandling->bChangePageDirect == 1))
	{
		if(nPageIndex == pPageHandling->nPageDefault)
		{
			// Startseite -> Lifo l�schen
			BrbMemListClear(&pPageHandling->PagesPreviousLifo);
		}
		else if(bStoreLastPage == 1)
		{
			// Aktuelle Seite in Lifo eintragen
			pPageHandling->PagesPreviousLifo.pList = (UDINT)&pPageHandling->nPagesPrevious;
			pPageHandling->PagesPreviousLifo.nEntryLength = sizeof(pPageHandling->nPagesPrevious[0]);
			pPageHandling->PagesPreviousLifo.nIndexMax = nBRB_PAGE_LAST_INDEX_MAX;
			BrbLifoIn(&pPageHandling->PagesPreviousLifo, (UDINT)&pPageHandling->nPageCurrent);
		}
		// Seite umschalten
		pPageHandling->nPageChange = nPageIndex;
		pPageHandling->nPageNext = nPageIndex;
		pPageHandling->bPageChangeInProgress = 1;
		pPageHandling->bPageInit = 1;
		pPageHandling->bPageExit = 1;
		bResult = 1;
	}
	pPageHandling->bChangePageDirect = 0;
	return bResult;
}
