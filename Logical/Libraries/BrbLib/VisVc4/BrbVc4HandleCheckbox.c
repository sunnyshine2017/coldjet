/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleCheckbox.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r eine Checkbox */
plcbit BrbVc4HandleCheckbox(struct BrbVc4Checkbox_TYP* pCheckbox)
{
	BOOL	bClicked;
	bClicked = 0;
	if(BrbVc4IsControlEnabled(pCheckbox->nStatus) == 0)
	{
		pCheckbox->bClicked = 0;
		pCheckbox->nBmpIndex = pCheckbox->bChecked;
		pCheckbox->nTextColor = eBRB_COLOR_DISABLED;
	}
	else
	{
		if(pCheckbox->bClicked == 1)
		{
			pCheckbox->bClicked = 0;
			pCheckbox->bChecked = !pCheckbox->bChecked;
			bClicked = 1;
		}
		pCheckbox->nBmpIndex = 2 + pCheckbox->bChecked;
		pCheckbox->nTextColor = eBRB_COLOR_ENABLED;
	}
	return bClicked;
}
