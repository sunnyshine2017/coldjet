/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4IsControlVisible.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Gibt zur�ck, ob ein Vis-Control sichtbar/unsichtbar ist */
plcbit BrbVc4IsControlVisible(unsigned short nStatus)
{
	return !(nStatus & 1);
}
