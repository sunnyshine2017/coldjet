/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleTouchGrid.c
 * Author: niedermeierr
 * Created: July 03, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <string.h>

#ifdef __cplusplus
	};
#endif

/* Logik f�r ein Touchgrid */
unsigned short BrbVc4HandleTouchGrid(struct BrbVc4Touchgrid_TYP* pTouchgrid, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	pTouchgrid->nSelectedIndexMax = pTouchgrid->nIndexMaxX + pTouchgrid->nIndexMaxY * (pTouchgrid->nIndexMaxX+1);
	if(pTouchgrid->nSelectedIndex > pTouchgrid->nSelectedIndexMax)
	{
		pTouchgrid->nSelectedIndex = pTouchgrid->nSelectedIndexMax;
	}
	BrbVc4Rectangle_TYP GridBound;
	memset(&GridBound, 0, sizeof(GridBound));
	GridBound.nLeft = pTouchgrid->nGridLeft;
	GridBound.nTop = pTouchgrid->nGridTop;
	GridBound.nWidth = (pTouchgrid->nIndexMaxX+1) * pTouchgrid->nCellWidth;
	GridBound.nHeight = (pTouchgrid->nIndexMaxY+1) * pTouchgrid->nCellHeight;
	if(pTouchgrid->bClickEnabled == 1)
	{
		if(			pGeneral->Touch.nX >= GridBound.nLeft 
			&&	pGeneral->Touch.nX <= GridBound.nLeft + GridBound.nWidth
			&&	pGeneral->Touch.nY >= GridBound.nTop
			&&	pGeneral->Touch.nY <= GridBound.nTop + GridBound.nHeight
			)
		{
			// Zeiger innerhalb des Grids
			BrbVc4TouchStates_ENUM eTouchState = pGeneral->Touch.eState;
			if(pTouchgrid->bUseSyncTouchState == 1)
			{
				eTouchState = pGeneral->Touch.eStateSync;
			}
			if(eTouchState == eBRB_TOUCH_STATE_PUSHED_EDGE)
			{
				// Klick
				UINT nClickX = pGeneral->Touch.nX - pTouchgrid->nGridLeft;
				pTouchgrid->nSelectedIndexX = nClickX / pTouchgrid->nCellWidth;
				UINT nClickY = pGeneral->Touch.nY - pTouchgrid->nGridTop;
				pTouchgrid->nSelectedIndexY = nClickY / pTouchgrid->nCellHeight;
				pTouchgrid->nSelectedIndex = pTouchgrid->nSelectedIndexX + pTouchgrid->nSelectedIndexY * (pTouchgrid->nIndexMaxX+1);
			}
			else
			{
				pTouchgrid->nSelectedIndexY = pTouchgrid->nSelectedIndex / (pTouchgrid->nIndexMaxX+1);
				pTouchgrid->nSelectedIndexX = pTouchgrid->nSelectedIndex - pTouchgrid->nSelectedIndexY * (pTouchgrid->nIndexMaxX+1);
			}
			pTouchgrid->eState = eTouchState;
		}
		else
		{
			pTouchgrid->nSelectedIndexY = pTouchgrid->nSelectedIndex / (pTouchgrid->nIndexMaxX+1);
			pTouchgrid->nSelectedIndexX = pTouchgrid->nSelectedIndex - pTouchgrid->nSelectedIndexY * (pTouchgrid->nIndexMaxX+1);
			pTouchgrid->eState =eBRB_TOUCH_STATE_UNPUSHED;
		}
	}
	else
	{
		pTouchgrid->nSelectedIndexY = pTouchgrid->nSelectedIndex / (pTouchgrid->nIndexMaxX+1);
		pTouchgrid->nSelectedIndexX = pTouchgrid->nSelectedIndex - pTouchgrid->nSelectedIndexY * (pTouchgrid->nIndexMaxX+1);
		pTouchgrid->eState = eBRB_TOUCH_STATE_UNPUSHED;
	}
	// Zeichnen
	if(pTouchgrid->bDrawGrid == 1 || pTouchgrid->bDrawMarker == 1)
	{
		// Das Zeichnen wird synchronisiert
		if(pGeneral->nRedrawCounter == 0)
		{
			if(pGeneral->nVcHandle != 0)
			{
				// Zugriff reservieren
				nResult = VA_Saccess(1, pGeneral->nVcHandle);
				if(nResult == 0)
				{
					if(pTouchgrid->bDrawGrid == 1)
					{
						UINT nCellIndexX = 0;
						UINT nCellIndexY = 0;
						BrbVc4Rectangle_TYP CellRect;
						memset(&CellRect, 0, sizeof(CellRect));
						CellRect.nWidth = pTouchgrid->nCellWidth;
						CellRect.nHeight = pTouchgrid->nCellHeight;
						CellRect.nFillColor = 255; // Nur Rahmen
						CellRect.nBorderColor = pTouchgrid->nGridColor;
						CellRect.nDashWidth = 0;
						for(nCellIndexX=0; nCellIndexX<=pTouchgrid->nIndexMaxX; nCellIndexX++)
						{
							for(nCellIndexY=0; nCellIndexY<=pTouchgrid->nIndexMaxY; nCellIndexY++)
							{
								CellRect.nLeft = pTouchgrid->nGridLeft + nCellIndexX * pTouchgrid->nCellWidth;
								CellRect.nTop = pTouchgrid->nGridTop + nCellIndexY * pTouchgrid->nCellHeight;
								nResult = BrbVc4DrawRectangle(&CellRect, pGeneral);
							}
						}
					}
					if(pTouchgrid->bDrawMarker == 1)
					{
						if(pTouchgrid->eMarkerFigure == eBRB_FIGURE_LINE)
						{
							BrbVc4Line_TYP Marker;
							memcpy(&Marker, (USINT*)&pTouchgrid->MarkerLine, sizeof(Marker));
							Marker.nLeft = GridBound.nLeft + pTouchgrid->nSelectedIndexX * pTouchgrid->nCellWidth + pTouchgrid->MarkerLine.nLeft;
							Marker.nTop = GridBound.nTop + pTouchgrid->nSelectedIndexY * pTouchgrid->nCellHeight + pTouchgrid->MarkerLine.nTop;
							Marker.nRight = GridBound.nLeft + pTouchgrid->nSelectedIndexX * pTouchgrid->nCellWidth + pTouchgrid->MarkerLine.nRight;
							Marker.nBottom = GridBound.nTop + pTouchgrid->nSelectedIndexY * pTouchgrid->nCellHeight + pTouchgrid->MarkerLine.nBottom;
							nResult = BrbVc4DrawLine(&Marker, pGeneral);
						}
						else if(pTouchgrid->eMarkerFigure == eBRB_FIGURE_ELLIPSE)
						{
							BrbVc4Ellipse_TYP Marker;
							memcpy(&Marker, (USINT*)&pTouchgrid->MarkerEllipse, sizeof(Marker));
							Marker.nLeft = GridBound.nLeft + pTouchgrid->nSelectedIndexX * pTouchgrid->nCellWidth + pTouchgrid->MarkerEllipse.nLeft;
							Marker.nTop = GridBound.nTop + pTouchgrid->nSelectedIndexY * pTouchgrid->nCellHeight + pTouchgrid->MarkerEllipse.nTop;
							nResult = BrbVc4DrawEllipse(&Marker, pGeneral);
						}
						else if(pTouchgrid->eMarkerFigure == eBRB_FIGURE_ARC)
						{
							BrbVc4Arc_TYP Marker;
							memcpy(&Marker, (USINT*)&pTouchgrid->MarkerArc, sizeof(Marker));
							Marker.nLeft = GridBound.nLeft + pTouchgrid->nSelectedIndexX * pTouchgrid->nCellWidth + pTouchgrid->MarkerArc.nLeft;
							Marker.nTop = GridBound.nTop + pTouchgrid->nSelectedIndexY * pTouchgrid->nCellHeight + pTouchgrid->MarkerArc.nTop;
							nResult = BrbVc4DrawArc(&Marker, pGeneral);
						}
						else if(pTouchgrid->eMarkerFigure == eBRB_FIGURE_TEXT)
						{
							BrbVc4DrawText_TYP Marker;
							memcpy(&Marker, (USINT*)&pTouchgrid->MarkerText, sizeof(Marker));
							Marker.nLeft = GridBound.nLeft + pTouchgrid->nSelectedIndexX * pTouchgrid->nCellWidth + pTouchgrid->MarkerText.nLeft;
							Marker.nTop = GridBound.nTop + pTouchgrid->nSelectedIndexY * pTouchgrid->nCellHeight + pTouchgrid->MarkerText.nTop;
							nResult = BrbVc4DrawText(&Marker, pGeneral);
						}
						else
						{
							BrbVc4Rectangle_TYP Marker;
							memcpy(&Marker, (USINT*)&pTouchgrid->MarkerRectangle, sizeof(Marker));
							Marker.nLeft = GridBound.nLeft + pTouchgrid->nSelectedIndexX * pTouchgrid->nCellWidth + pTouchgrid->MarkerRectangle.nLeft;
							Marker.nTop = GridBound.nTop + pTouchgrid->nSelectedIndexY * pTouchgrid->nCellHeight + pTouchgrid->MarkerRectangle.nTop;
							nResult = BrbVc4DrawRectangle(&Marker, pGeneral);
						}
					}
					// Zugriff freigeben
					nResult = VA_Srelease(1, pGeneral->nVcHandle);
				}
			}
		}
	}
	return nResult;
}
