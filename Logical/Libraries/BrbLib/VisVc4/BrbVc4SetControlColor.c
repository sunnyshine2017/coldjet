/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4SetControlColor.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Setzt eine von zwei Farben eines Controls aufgrund einer Bedingung */
unsigned short BrbVc4SetControlColor(unsigned short* pColorDatapoint, plcbit bCondition, unsigned short nColorTrue, unsigned short nColorFalse)
{
	if(bCondition == 1)
	{
		*pColorDatapoint = nColorTrue;
	}
	else
	{
		*pColorDatapoint = nColorFalse;
	}
	return 0;
}
