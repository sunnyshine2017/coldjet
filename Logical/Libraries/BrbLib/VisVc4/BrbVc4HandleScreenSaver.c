/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleScreenSaver.c
 * Author: niedermeierr
 * Created: September 20, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <string.h>

#ifdef __cplusplus
	};
#endif

/* Behandelt den Bildschirmschoner */
plcbit BrbVc4HandleScreenSaver(struct BrbVc4ScreenSaver_TYP* pScreenSaver, struct BrbVc4General_TYP* pGeneral, struct BrbVc4PageHandling_TYP* pPageHandling)
{
	BOOL bResult = 0;
	if(pScreenSaver->nPageBeforeScreenSaver == 0)
	{
		pScreenSaver->nPageBeforeScreenSaver = pPageHandling->nPageDefault;
	}
	if(pScreenSaver->bEnable == 1 && pScreenSaver->fbScreenSaver.PT != 0 && pPageHandling->bPageChangeInProgress == 0)
	{
		if(pScreenSaver->TouchOld.nX == pGeneral->Touch.nX && pScreenSaver->TouchOld.nY == pGeneral->Touch.nY && pScreenSaver->TouchOld.eState == pGeneral->Touch.eState)
		{
			// Keine Bewegung der Maus
			if(pPageHandling->nPageCurrent != pScreenSaver->nScreenSaverPage)
			{
				// Die Bildschirmschoner-Seite wird nicht angezeigt
				if(pScreenSaver->fbScreenSaver.PT != 0)
				{
					pScreenSaver->fbScreenSaver.IN = 1;
				}
			}
			else
			{
				// Die Bildschirmschoner-Seite wird angezeigt
				pScreenSaver->fbScreenSaver.IN = 0;
			}
		}
		else
		{
			// Bewegung der Maus
			if(pPageHandling->nPageCurrent != pScreenSaver->nScreenSaverPage)
			{
				// Die Bildschirmschoner-Seite wird nicht angezeigt
				pScreenSaver->fbScreenSaver.IN = 0;
			}
			else
			{
				// Die Bildschirmschoner-Seite wird angezeigt
				pScreenSaver->fbScreenSaver.IN = 0;
				BrbVc4ChangePage(pPageHandling, pScreenSaver->nPageBeforeScreenSaver, 0);
			}
		}
		if(pScreenSaver->hsScreen.bClicked == 1)
		{
			pScreenSaver->hsScreen.bClicked = 0;
			// Zurückschalten auf vorherige Seite
			pScreenSaver->fbScreenSaver.IN = 0;
			BrbVc4ChangePage(pPageHandling, pScreenSaver->nPageBeforeScreenSaver, 0);
		}
	}
	else
	{
		pScreenSaver->fbScreenSaver.IN = 0;
	}
	if(pScreenSaver->fbScreenSaver.Q == 1)
	{
		pScreenSaver->fbScreenSaver.IN = 0;
		pScreenSaver->nPageBeforeScreenSaver = pPageHandling->nPageCurrent;
		BrbVc4ChangePage(pPageHandling, pScreenSaver->nScreenSaverPage, 0);
		bResult = 1;
	}
	TON(&pScreenSaver->fbScreenSaver);
	memcpy(&pScreenSaver->TouchOld, &pGeneral->Touch, sizeof(BrbVc4GeneralTouch_TYP));
	return bResult;
}
