/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4SetControlVisibility.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Setzt ein Vis-Control auf sichtbar/unsichtbar */
plcbit BrbVc4SetControlVisibility(unsigned short* pStatus, plcbit bVisible)
{
	if(bVisible == 0)
	{
		*pStatus = *pStatus | 1;
		return 0;
	}
	else
	{
		*pStatus = *pStatus & 0xFFFE;
		return 1;
	}
}
