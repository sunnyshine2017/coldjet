﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.1.9.44 SP?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="Encoder" Source="Tasks.Encoder.Encoder.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="TriggerSen" Source="Tasks.TriggerSensor.TriggerSensor.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="PrintMarkS" Source="Tasks.PrintMarkSensor.PrintMarkSensor.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="BondingDev" Source="Tasks.BondingDev.BondingDev.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="ncsdcctrl" Source="ncsdcctrl.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#2">
    <Task Name="PressureCt" Source="Tasks.PressureCtrl.PressureCtrl.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#3">
    <Task Name="MainLogic" Source="Tasks.MainLogic.MainLogic.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#4">
    <Task Name="AxisContro" Source="Tasks.AxisControl.AxisControl.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="gAxis01" Source="Axis.gAxis01obj.gAxis01.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="gAxis02" Source="Axis.gAxis02obj.gAxis02.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="gAxis03" Source="Axis.gAxis03obj.gAxis03.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="gAxis04" Source="Axis.gAxis04obj.gAxis04.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="gAxis05" Source="Axis.gAxis05obj.gAxis05.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="gAxis06" Source="Axis.gAxis06obj.gAxis06.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="gAxis07" Source="Axis.gAxis07obj.gAxis07.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="gAxis08" Source="Axis.gAxis08obj.gAxis08.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#5" />
  <TaskClass Name="Cyclic#6">
    <Task Name="EventHandl" Source="Tasks.EventHandling.EventHandling.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="CalcPos" Source="Tasks.CalcPos.CalcPos.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#7" />
  <TaskClass Name="Cyclic#8">
    <Task Name="Template" Source="Tasks.Template.Template.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="ParHandlin" Source="Tasks.ParHandling.ParHandling.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Machine" Source="Tasks.Machine.Machine.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="System" Source="Tasks.System.System.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Stitchmode" Source="Tasks.Stitchmode.Stitchmode.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="Visu" Source="Tasks.Visu.Visu.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
  </TaskClass>
  <DataObjects>
    <DataObject Name="Acp10sys" Source="" Memory="UserROM" Language="Binary" />
  </DataObjects>
  <NcDataObjects>
    <NcDataObject Name="gAxis01a" Source="Axis.gAxis01obj.gAxis01a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxis01i" Source="Axis.gAxis01obj.gAxis01i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxis02a" Source="Axis.gAxis02obj.gAxis02a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxis02i" Source="Axis.gAxis02obj.gAxis02i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxis03a" Source="Axis.gAxis03obj.gAxis03a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxis03i" Source="Axis.gAxis03obj.gAxis03i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxis04a" Source="Axis.gAxis04obj.gAxis04a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxis04i" Source="Axis.gAxis04obj.gAxis04i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxis05a" Source="Axis.gAxis05obj.gAxis05a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxis05i" Source="Axis.gAxis05obj.gAxis05i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxis06a" Source="Axis.gAxis06obj.gAxis06a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxis06i" Source="Axis.gAxis06obj.gAxis06i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxis07a" Source="Axis.gAxis07obj.gAxis07a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxis07i" Source="Axis.gAxis07obj.gAxis07i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="gAxis08a" Source="Axis.gAxis08obj.gAxis08a.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="gAxis08i" Source="Axis.gAxis08obj.gAxis08i.dob" Memory="UserROM" Language="Ax" />
    <NcDataObject Name="acp10etxde" Source="acp10etxde.dob" Memory="UserROM" Language="Ett" />
  </NcDataObjects>
  <VcDataObjects>
    <VcDataObject Name="VisWvg" Source="Tasks.Visu.VisWvga.dob" Memory="UserROM" Language="Vc" WarningLevel="2" Compress="false" />
  </VcDataObjects>
  <Binaries>
    <BinaryObject Name="vcctext" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcgclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="VisWvg03" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccshape" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcnet" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsloc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfntttf" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccgauge" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccpwd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcshared" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcrt" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpkat" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbmp" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcbclass" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcfile" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="VisWvg02" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbtn" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcmgr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vctcal" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="visvc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccnum" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcchspot" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdsw" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcdsint" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="arialxsr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccovl" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccalarm" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccline" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdvnc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccstr" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="VisWvg01" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcresman" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccbar" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccpopup" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccddbox" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vccdbox" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdi815" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcptelo" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdihd" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpdi855" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfppc7" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpk" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="Acp10map" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="acp10cfg" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="vcpfx20" Source="" Memory="UserROM" Language="Binary" />
  </Binaries>
  <ReActionTechnologyObjects>
    <ReActionTechnologyObject Name="Stitchmod2" Source="Tasks.Stitchmode.StitchmodeCtrlX2X.prg" Memory="UserROM" Language="ReAction" />
  </ReActionTechnologyObjects>
  <Libraries>
    <LibraryObject Name="AppLib" Source="Libraries.AppLib.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="AsARCfg" Source="Libraries.AsARCfg.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsBrStr" Source="Libraries.AsBrStr.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsIecCon" Source="Libraries.AsIecCon.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsIOTime" Source="Libraries.AsIOTime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="ASMcDcs" Source="Libraries.ASMcDcs.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="astime" Source="Libraries.astime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsUSB" Source="Libraries.AsUSB.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="BrbLib" Source="Libraries.BrbLib.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="brsystem" Source="Libraries.brsystem.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="DataObj" Source="Libraries.DataObj.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="FileIO" Source="Libraries.FileIO.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Operator" Source="Libraries.Operator.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Runtime" Source="Libraries.Runtime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="standard" Source="Libraries.standard.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="visapi" Source="Libraries.visapi.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="asbrmath" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="asstring" Source="Libraries.asstring.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="powerlnk" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10man" Source="Libraries.Acp10man.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10par" Source="Libraries.Acp10par.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="NcGlobal" Source="Libraries.NcGlobal.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="acp10_mc" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10sdc" Source="Libraries.Acp10sdc.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MTBasics" Source="Libraries.MTBasics.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MTTypes" Source="Libraries.MTTypes.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsRfbExt" Source="Libraries.AsRfbExt.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="ashw" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="asioacc" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>