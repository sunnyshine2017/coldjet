UnmarkedObjectFolder := C:/Users/Resmi/Desktop/ColdjetGit/Logical/Libraries/AppLib
MarkedObjectFolder := C:/Users/Resmi/Desktop/ColdjetGit/Logical/Libraries/AppLib

$(AS_CPU_PATH)/AppLib.br: \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/ANSIC.lby \
	$(AS_CPU_PATH)/AppLib/AppLib.ox
	@"$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe" "$(AS_CPU_PATH)/AppLib/AppLib.ox" -o "$(AS_CPU_PATH)/AppLib.br" -v V1.00.0 -f "$(AS_CPU_PATH)/NT.ofs" -offsetLT "$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs" -T SG4  -M IA32  -B M4.10 -extConstants  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -d "astime: V* - V*" -r Library -R "Applikations-spezifische Funktionen"  -s "Libraries.AppLib" -L "Acp10_MC: V2.45.1, Acp10man: V2.45.1, Acp10par: V2.45.1, Acp10sdc: V2.45.1, Acp10sim: V2.45.1, AppLib: V*, AsARCfg: V*, AsBrMath: V*, AsBrStr: V*, AsIecCon: V*, AsIOAcc: V*, AsIORTI: V*, AsIOTime: V*, ASMcDcs: V*, AsRfbExt: V*, asstring: V*, astime: V*, AsUSB: V*, AsWeigh: V*, BrbLib: V2.03.0, brsystem: V*, DataObj: V*, FileIO: V*, MTBasics: V2.10.0, MTFilter: V2.10.0, MTTypes: V1.03.0, NcGlobal: V2.45.1, Operator: V*, Runtime: V*, standard: V*, sys_lib: V*, visapi: V*" -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.taskbuilder.exe"

$(AS_CPU_PATH)/AppLib/AppLib.ox: \
	$(AS_CPU_PATH)/AppLib/a.out
	@"$(AS_BIN_PATH)/BR.AS.Backend.exe" "$(AS_CPU_PATH)/AppLib/a.out" -o "$(AS_CPU_PATH)/AppLib/AppLib.ox" -T SG4 -r Library  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES  -G V4.1.2  -secret "$(AS_PROJECT_PATH)_br.as.backend.exe"

$(AS_CPU_PATH)/AppLib/a.out: \
	$(AS_CPU_PATH)/AppLib/AppSetEvent.c.o \
	$(AS_CPU_PATH)/AppLib/AppResetEvent.c.o \
	$(AS_CPU_PATH)/AppLib/AppIsEventAcknowledged.c.o \
	$(AS_CPU_PATH)/AppLib/Round.c.o
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" -link  -O "$(AS_CPU_PATH)//AppLib/AppLib.out.opt" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/AppLib/AppSetEvent.c.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/EventHandling/AppSetEvent.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsBrMath/AsBrMath.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_TEMP_PATH)/Includes/AppLib.h \
	$(AS_TEMP_PATH)/Includes/astime.h \
	$(AS_TEMP_PATH)/Includes/runtime.h
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/EventHandling/AppSetEvent.c" -o "$(AS_CPU_PATH)/AppLib/AppSetEvent.c.o"  -T SG4  -M IA32  -B M4.10 -G V4.1.2  -s "Libraries.AppLib" -t "$(AS_TEMP_PATH)" -r Library -I "$(AS_PROJECT_PATH)/Logical/Libraries/AppLib" "$(AS_TEMP_PATH)/Includes/Libraries/AppLib" "$(AS_TEMP_PATH)/Includes" "$(AS_PROJECT_PATH)/Logical/Global" -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include "$(AS_CPU_PATH)/Libraries.h" -D _APPLIB_EXPORT  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -x c -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/AppLib/AppResetEvent.c.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/EventHandling/AppResetEvent.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsBrMath/AsBrMath.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_TEMP_PATH)/Includes/AppLib.h \
	$(AS_TEMP_PATH)/Includes/astime.h \
	$(AS_TEMP_PATH)/Includes/runtime.h
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/EventHandling/AppResetEvent.c" -o "$(AS_CPU_PATH)/AppLib/AppResetEvent.c.o"  -T SG4  -M IA32  -B M4.10 -G V4.1.2  -s "Libraries.AppLib" -t "$(AS_TEMP_PATH)" -r Library -I "$(AS_PROJECT_PATH)/Logical/Libraries/AppLib" "$(AS_TEMP_PATH)/Includes/Libraries/AppLib" "$(AS_TEMP_PATH)/Includes" "$(AS_PROJECT_PATH)/Logical/Global" -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include "$(AS_CPU_PATH)/Libraries.h" -D _APPLIB_EXPORT  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -x c -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/AppLib/AppIsEventAcknowledged.c.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/EventHandling/AppIsEventAcknowledged.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsBrMath/AsBrMath.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_TEMP_PATH)/Includes/AppLib.h \
	$(AS_TEMP_PATH)/Includes/astime.h \
	$(AS_TEMP_PATH)/Includes/runtime.h
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/EventHandling/AppIsEventAcknowledged.c" -o "$(AS_CPU_PATH)/AppLib/AppIsEventAcknowledged.c.o"  -T SG4  -M IA32  -B M4.10 -G V4.1.2  -s "Libraries.AppLib" -t "$(AS_TEMP_PATH)" -r Library -I "$(AS_PROJECT_PATH)/Logical/Libraries/AppLib" "$(AS_TEMP_PATH)/Includes/Libraries/AppLib" "$(AS_TEMP_PATH)/Includes" "$(AS_PROJECT_PATH)/Logical/Global" -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include "$(AS_CPU_PATH)/Libraries.h" -D _APPLIB_EXPORT  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -x c -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/AppLib/Round.c.o: \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/EventHandling/Round.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsBrMath/AsBrMath.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_TEMP_PATH)/Includes/AppLib.h \
	$(AS_TEMP_PATH)/Includes/astime.h \
	$(AS_TEMP_PATH)/Includes/runtime.h \
	$(AS_TEMP_PATH)/Includes/AsBrMath.h
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/EventHandling/Round.c" -o "$(AS_CPU_PATH)/AppLib/Round.c.o"  -T SG4  -M IA32  -B M4.10 -G V4.1.2  -s "Libraries.AppLib" -t "$(AS_TEMP_PATH)" -r Library -I "$(AS_PROJECT_PATH)/Logical/Libraries/AppLib" "$(AS_TEMP_PATH)/Includes/Libraries/AppLib" "$(AS_TEMP_PATH)/Includes" "$(AS_PROJECT_PATH)/Logical/Global" -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include "$(AS_CPU_PATH)/Libraries.h" -D _APPLIB_EXPORT  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -x c -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

-include $(AS_CPU_PATH)/Force.mak 

