######################################################
#                                                    #
# Automatic generated Makefile for Visual Components #
#                                                    #
#                  Do NOT edit!                      #
#                                                    #
######################################################

VCC:=@"$(AS_BIN_PATH)/br.vc.pc.exe"
LINK:=@"$(AS_BIN_PATH)/BR.VC.Link.exe"
MODGEN:=@"$(AS_BIN_PATH)/BR.VC.ModGen.exe"
VCPL:=@"$(AS_BIN_PATH)/BR.VC.PL.exe"
VCHWPP:=@"$(AS_BIN_PATH)/BR.VC.HWPP.exe"
VCDEP:=@"$(AS_BIN_PATH)/BR.VC.Depend.exe"
VCFLGEN:=@"$(AS_BIN_PATH)/BR.VC.lfgen.exe"
VCREFHANDLER:=@"$(AS_BIN_PATH)/BR.VC.CrossReferenceHandler.exe"
VCXREFEXTENDER:=@"$(AS_BIN_PATH)/BR.AS.CrossRefVCExtender.exe"
RM=CMD /C DEL
PALFILE_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Palette.vcr
VCCFLAGS_VisWvg=-server -proj VisWvg -vc "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/VCObject.vc" -prj_path "$(AS_PROJECT_PATH)" -temp_path "$(AS_TEMP_PATH)" -cfg $(AS_CONFIGURATION) -plc $(AS_PLC) -plctemp $(AS_TEMP_PLC) -cpu_path "$(AS_CPU_PATH)"
VCFIRMWARE=4.25.0
VCFIRMWAREPATH=$(AS_VC_PATH)/Firmware/V4.25.0/SG4
VCOBJECT_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/VCObject.vc
VCSTARTUP="vcstart.br"
VCLOD="vclod.br"
VCSTPOST="vcstpost.br"
TARGET_FILE_VisWvg=$(AS_CPU_PATH)/VisWvg.br
OBJ_SCOPE_VisWvg=Tasks/Visu
PRJ_PATH_VisWvg=$(AS_PROJECT_PATH)
SRC_PATH_VisWvg=$(AS_PROJECT_PATH)/Logical/$(OBJ_SCOPE_VisWvg)/VisWvga
TEMP_PATH_VisWvg=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VisWvg
TEMP_PATH_Shared=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared
TEMP_PATH_ROOT_VisWvg=$(AS_TEMP_PATH)
VC_LIBRARY_LIST_VisWvg=$(TEMP_PATH_VisWvg)/libraries.vci
VC_XREF_BUILDFILE_VisWvg=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.build
VC_XREF_CLEANFILE=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.clean
VC_LANGUAGES_VisWvg=$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr
CPUHWC="$(TEMP_PATH_VisWvg)/cpuhwc.vci"
VC_STATIC_OPTIONS_VisWvg="$(TEMP_PATH_VisWvg)/vcStaticOptions.xml"
VC_STATIC_OPTIONS_Shared="$(TEMP_PATH_Shared)/vcStaticOptions.xml"
TTFFLAGS_VisWvg= -P "$(AS_PROJECT_PATH)" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo"

#
# Font arialxsr
#
ifneq ($(VC_FONT_arialxsr),1)
VC_FONT_arialxsr=1
$(AS_CPU_PATH)/VcFntDat/arialxsr.vco:$(AS_CPU_PATH)/VcFntDat/arialxsr.vci
	 $(VCC) -f "$<" -o "$@" $(TTFFLAGS_VisWvg) $(VCCFLAGS_VisWvg) -sfas

$(AS_CPU_PATH)/VcFntDat/arialxsr.ccf:$(AS_CPU_PATH)/VcFntDat/arialxsr.vco
	 $(LINK) "$^" -o "$@" -warningLevel2 -m arialxsr.ttf -name VisWvg -profile "False" -vcr 4250 -sfas

$(AS_CPU_PATH)/arialxsr.br:$(AS_CPU_PATH)/VcFntDat/arialxsr.ccf
	 $(MODGEN) -so $(VC_STATIC_OPTIONS_Shared) -fw "$(VCFIRMWAREPATH)" -m $(VCLOD) -f "$<" -o "$@" -d vcgclass -v V1.00.0 -profile False -vc "$(VCOBJECT_VisWvg)" -b
endif

FONT_MODULES_VisWvg=$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/arialxsr.br 
