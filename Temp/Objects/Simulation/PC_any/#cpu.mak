SHELL = cmd.exe
export AS_PLC := PC_any
export AS_TEMP_PLC := PC_any
export AS_CPU_PATH := $(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_PLC)
export AS_CPU_PATH_2 := C:/Users/Resmi/Desktop/ColdjetGit/Temp//Objects/$(AS_CONFIGURATION)/$(AS_PLC)
export AS_PROJECT_CONFIG_PATH := $(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)
export AS_PROJECT_CPU_PATH := $(AS_PROJECT_CONFIG_PATH)/$(AS_PLC)
export AS_STATIC_ARCHIVES_PATH := $(AS_TEMP_PATH)/Archives/$(AS_CONFIGURATION)/$(AS_PLC)
export WIN32_AS_CPU_PATH := $(WIN32_AS_TEMP_PATH)\Objects\$(AS_CONFIGURATION)\$(AS_PLC)
export WIN32_AS_ACTIVE_CONFIG_PATH := $(WIN32_AS_PROJECT_PATH)\Physical\$(AS_CONFIGURATION)\$(AS_PLC)


CpuMakeFile: \
$(AS_CPU_PATH)/ashwd.br \
$(AS_CPU_PATH)/asfw.br \
$(AS_CPU_PATH)/sysconf.br \
$(AS_CPU_PATH)/arconfig.br \
$(AS_CPU_PATH)/acp10cfg.br \
$(AS_CPU_PATH)/acp10map.br \
$(AS_CPU_PATH)/steppersim.br \
$(AS_CPU_PATH)/AppLib.br \
$(AS_CPU_PATH)/BrbLib.br \
$(AS_CPU_PATH)/Encoder.br \
$(AS_CPU_PATH)/MainLogic.br \
$(AS_CPU_PATH)/BondingDev.br \
$(AS_CPU_PATH)/PrintMarkS.br \
$(AS_CPU_PATH)/TriggerSen.br \
$(AS_CPU_PATH)/TestTrigge.br \
$(AS_CPU_PATH)/AxisContro.br \
$(AS_CPU_PATH)/gAxis01.br \
$(AS_CPU_PATH)/gAxis02.br \
$(AS_CPU_PATH)/gAxis03.br \
$(AS_CPU_PATH)/gAxis04.br \
$(AS_CPU_PATH)/PressureCt.br \
$(AS_CPU_PATH)/CalcPos.br \
$(AS_CPU_PATH)/EventHandl.br \
$(AS_CPU_PATH)/ExtCtrlIf.br \
$(AS_CPU_PATH)/TempCtrl.br \
$(AS_CPU_PATH)/Template.br \
$(AS_CPU_PATH)/ParHandlin.br \
$(AS_CPU_PATH)/Machine.br \
$(AS_CPU_PATH)/System.br \
$(AS_CPU_PATH)/Stitchmode.br \
$(AS_CPU_PATH)/Visu.br \
$(AS_CPU_PATH)/gAxis01a.br \
$(AS_CPU_PATH)/gAxis01i.br \
$(AS_CPU_PATH)/gAxis02a.br \
$(AS_CPU_PATH)/gAxis02i.br \
$(AS_CPU_PATH)/gAxis03a.br \
$(AS_CPU_PATH)/gAxis03i.br \
$(AS_CPU_PATH)/gAxis04a.br \
$(AS_CPU_PATH)/gAxis04i.br \
$(AS_CPU_PATH)/acp10etxde.br \
$(AS_CPU_PATH)/ExtCtr.br \
vcPostBuild_ExtCtr \
$(AS_CPU_PATH)/VisWvg.br \
vcPostBuild_VisWvg \
$(AS_CPU_PATH)/Stitchmod1.br \
$(AS_CPU_PATH)/iomap.br \
$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/Transfer.lst


$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/Transfer.lst: \
	$(AS_CPU_PATH)/ashwd.br \
	$(AS_CPU_PATH)/asfw.br \
	$(AS_CPU_PATH)/sysconf.br \
	$(AS_CPU_PATH)/arconfig.br \
	$(AS_CPU_PATH)/acp10cfg.br \
	$(AS_CPU_PATH)/acp10map.br \
	$(AS_CPU_PATH)/steppersim.br \
	$(AS_CPU_PATH)/AppLib.br \
	$(AS_CPU_PATH)/BrbLib.br \
	$(AS_CPU_PATH)/Encoder.br \
	$(AS_CPU_PATH)/MainLogic.br \
	$(AS_CPU_PATH)/BondingDev.br \
	$(AS_CPU_PATH)/PrintMarkS.br \
	$(AS_CPU_PATH)/TriggerSen.br \
	$(AS_CPU_PATH)/TestTrigge.br \
	$(AS_CPU_PATH)/AxisContro.br \
	$(AS_CPU_PATH)/gAxis01.br \
	$(AS_CPU_PATH)/gAxis02.br \
	$(AS_CPU_PATH)/gAxis03.br \
	$(AS_CPU_PATH)/gAxis04.br \
	$(AS_CPU_PATH)/PressureCt.br \
	$(AS_CPU_PATH)/CalcPos.br \
	$(AS_CPU_PATH)/EventHandl.br \
	$(AS_CPU_PATH)/ExtCtrlIf.br \
	$(AS_CPU_PATH)/TempCtrl.br \
	$(AS_CPU_PATH)/Template.br \
	$(AS_CPU_PATH)/ParHandlin.br \
	$(AS_CPU_PATH)/Machine.br \
	$(AS_CPU_PATH)/System.br \
	$(AS_CPU_PATH)/Stitchmode.br \
	$(AS_CPU_PATH)/Visu.br \
	$(AS_CPU_PATH)/gAxis01a.br \
	$(AS_CPU_PATH)/gAxis01i.br \
	$(AS_CPU_PATH)/gAxis02a.br \
	$(AS_CPU_PATH)/gAxis02i.br \
	$(AS_CPU_PATH)/gAxis03a.br \
	$(AS_CPU_PATH)/gAxis03i.br \
	$(AS_CPU_PATH)/gAxis04a.br \
	$(AS_CPU_PATH)/gAxis04i.br \
	$(AS_CPU_PATH)/acp10etxde.br \
	$(AS_CPU_PATH)/ExtCtr.br \
	$(AS_CPU_PATH)/VisWvg.br \
	$(AS_CPU_PATH)/Stitchmod1.br \
	$(AS_CPU_PATH)/iomap.br \
	$(AS_PROJECT_CPU_PATH)/Cpu.sw
	@"$(AS_BIN_PATH)/BR.AS.FinalizeBuild.exe" "$(AS_PROJECT_PATH)/Coldjetorg.apj" -t "$(AS_TEMP_PATH)" -o "$(AS_BINARIES_PATH)" -c "$(AS_CONFIGURATION)" -i "C:/BrAutomation/AS42" -S "PC_any"   -A "AR000" -pil   -swFiles "$(AS_PROJECT_PATH)/Physical/Simulation/PC_any/Cpu.sw" -Z "mapp: 1.30.0, GMC: 0.67.0, TextSystem: n.d, Connectivity: n.d, AAS: n.d" -C "/RT=1000 /AM=* /DAIP=127.0.0.1 /REPO=11160 /ANSL=1" -D "/IF=tcpip /LOPO=11159 /SA=1" -M IA32 -T SG4

#nothing to do (just call module make files)

include $(AS_CPU_PATH)/iomap/iomap.mak
include $(AS_CPU_PATH)/Stitchmod1/Stitchmod1.mak
include $(AS_CPU_PATH)/VisWvg/VisWvg.mak
include $(AS_CPU_PATH)/ExtCtr/ExtCtr.mak
include $(AS_CPU_PATH)/acp10etxde/acp10etxde.mak
include $(AS_CPU_PATH)/gAxis04i/gAxis04i.mak
include $(AS_CPU_PATH)/gAxis04a/gAxis04a.mak
include $(AS_CPU_PATH)/gAxis03i/gAxis03i.mak
include $(AS_CPU_PATH)/gAxis03a/gAxis03a.mak
include $(AS_CPU_PATH)/gAxis02i/gAxis02i.mak
include $(AS_CPU_PATH)/gAxis02a/gAxis02a.mak
include $(AS_CPU_PATH)/gAxis01i/gAxis01i.mak
include $(AS_CPU_PATH)/gAxis01a/gAxis01a.mak
include $(AS_CPU_PATH)/Visu/Visu.mak
include $(AS_CPU_PATH)/Stitchmode/Stitchmode.mak
include $(AS_CPU_PATH)/System/System.mak
include $(AS_CPU_PATH)/Machine/Machine.mak
include $(AS_CPU_PATH)/ParHandlin/ParHandlin.mak
include $(AS_CPU_PATH)/Template/Template.mak
include $(AS_CPU_PATH)/TempCtrl/TempCtrl.mak
include $(AS_CPU_PATH)/ExtCtrlIf/ExtCtrlIf.mak
include $(AS_CPU_PATH)/EventHandl/EventHandl.mak
include $(AS_CPU_PATH)/CalcPos/CalcPos.mak
include $(AS_CPU_PATH)/PressureCt/PressureCt.mak
include $(AS_CPU_PATH)/gAxis04/gAxis04.mak
include $(AS_CPU_PATH)/gAxis03/gAxis03.mak
include $(AS_CPU_PATH)/gAxis02/gAxis02.mak
include $(AS_CPU_PATH)/gAxis01/gAxis01.mak
include $(AS_CPU_PATH)/AxisContro/AxisContro.mak
include $(AS_CPU_PATH)/TestTrigge/TestTrigge.mak
include $(AS_CPU_PATH)/TriggerSen/TriggerSen.mak
include $(AS_CPU_PATH)/PrintMarkS/PrintMarkS.mak
include $(AS_CPU_PATH)/BondingDev/BondingDev.mak
include $(AS_CPU_PATH)/MainLogic/MainLogic.mak
include $(AS_CPU_PATH)/Encoder/Encoder.mak
include $(AS_CPU_PATH)/BrbLib/BrbLib.mak
include $(AS_CPU_PATH)/AppLib/AppLib.mak
include $(AS_CPU_PATH)/steppersim/steppersim.mak
include $(AS_CPU_PATH)/acp10map/acp10map.mak
include $(AS_CPU_PATH)/acp10cfg/acp10cfg.mak
include $(AS_CPU_PATH)/arconfig/arconfig.mak
include $(AS_CPU_PATH)/sysconf/sysconf.mak
include $(AS_CPU_PATH)/asfw/asfw.mak
include $(AS_CPU_PATH)/ashwd/ashwd.mak

.DEFAULT:
#nothing to do (need this target for prerequisite files that don't exit)

FORCE:
