UnmarkedObjectFolder := C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/TestTrigger
MarkedObjectFolder := C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/TestTrigger

$(AS_CPU_PATH)/TestTrigge.br: \
	$(AS_PROJECT_CPU_PATH)/Cpu.per \
	$(AS_CPU_PATH)/TestTrigge/TestTrigge.ox
	@"$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe" "$(AS_CPU_PATH)/TestTrigge/TestTrigge.ox" -o "$(AS_CPU_PATH)/TestTrigge.br" -v V1.00.0 -f "$(AS_CPU_PATH)/NT.ofs" -offsetLT "$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs" -T SG4  -M IA32  -B M4.10 -extConstants  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -r Cyclic1 -p 7 -s "Tasks.TestTrigger" -L "Acp10_MC: V2.45.1, Acp10man: V2.45.1, Acp10par: V2.45.1, Acp10sdc: V2.45.1, Acp10sim: V2.45.1, AppLib: V*, AsARCfg: V*, AsBrMath: V*, AsBrStr: V*, AsIecCon: V*, AsIOAcc: V*, AsIORTI: V*, AsIOTime: V*, ASMcDcs: V*, AsRfbExt: V*, asstring: V*, astime: V*, AsUSB: V*, AsWeigh: V*, BrbLib: V2.03.0, brsystem: V*, DataObj: V*, FileIO: V*, MTBasics: V2.10.0, MTFilter: V2.10.0, MTTypes: V1.03.0, NcGlobal: V2.45.1, Operator: V*, Runtime: V*, standard: V*, sys_lib: V*, visapi: V*" -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.taskbuilder.exe"

$(AS_CPU_PATH)/TestTrigge/TestTrigge.ox: \
	$(AS_CPU_PATH)/TestTrigge/a.out
	@"$(AS_BIN_PATH)/BR.AS.Backend.exe" "$(AS_CPU_PATH)/TestTrigge/a.out" -o "$(AS_CPU_PATH)/TestTrigge/TestTrigge.ox" -T SG4 -r Cyclic1  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES  -G V4.1.2  -secret "$(AS_PROJECT_PATH)_br.as.backend.exe"

$(AS_CPU_PATH)/TestTrigge/a.out: \
	$(AS_CPU_PATH)/TestTrigge/TestTrigger.c.o
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" -link  -O "$(AS_CPU_PATH)//TestTrigge/TestTrigge.out.opt" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/TestTrigge/TestTrigger.c.o: \
	$(AS_PROJECT_PATH)/Logical/Tasks/TestTrigger/TestTrigger.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/standard/standard.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_PROJECT_PATH)/Logical/Tasks/TestTrigger/TestTrigger.var \
	$(AS_PROJECT_PATH)/Logical/Global/Global.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/System/G_System.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/TriggerSensor/G_TriggerSensor.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/TriggerSensor/G_TriggerSensor.typ \
	$(AS_PROJECT_PATH)/Logical/Global/Global.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/G_Encoder.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/G_Encoder.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/TestTrigger/TestTrigger.typ \
	$(AS_PROJECT_PATH)/Logical/Global/Global.h \
	$(AS_PROJECT_PATH)/Logical/Global/AnsiCFunc.h
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Tasks/TestTrigger/TestTrigger.c" -o "$(AS_CPU_PATH)/TestTrigge/TestTrigger.c.o"  -T SG4  -M IA32  -B M4.10 -G V4.1.2  -s "Tasks.TestTrigger" -t "$(AS_TEMP_PATH)" -I "$(AS_PROJECT_PATH)/Logical/Tasks/TestTrigger" "$(AS_TEMP_PATH)/Includes/Tasks/TestTrigger"  "$(AS_TEMP_PATH)/Includes/Tasks" "$(AS_TEMP_PATH)/Includes" "$(AS_PROJECT_PATH)/Logical/Global" -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include "$(AS_CPU_PATH)/Libraries.h"  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -x c -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

-include $(AS_CPU_PATH)/Force.mak 

