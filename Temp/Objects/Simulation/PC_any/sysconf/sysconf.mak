$(AS_CPU_PATH)/sysconf.br: \
	$(AS_PROJECT_CONFIG_PATH)/Hardware.hw 
	@"$(AS_BIN_PATH)/BR.AS.ConfigurationBuilder.exe" "$(AS_PROJECT_CONFIG_PATH)/Hardware.hw"  -c Simulation -sysconf -S "PC_any" -o "$(AS_CPU_PATH)/sysconf.br" -T SG4  -B M4.10 -P "$(AS_PROJECT_PATH)" -s "Simulation"  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -secret "$(AS_PROJECT_PATH)_br.as.configurationbuilder.exe"

-include $(AS_CPU_PATH)/Force.mak 
