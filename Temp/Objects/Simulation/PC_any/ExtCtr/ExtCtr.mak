######################################################
#                                                    #
# Automatic generated Makefile for Visual Components #
#                                                    #
#                  Do NOT edit!                      #
#                                                    #
######################################################

VCC:=@"$(AS_BIN_PATH)/br.vc.pc.exe"
LINK:=@"$(AS_BIN_PATH)/BR.VC.Link.exe"
MODGEN:=@"$(AS_BIN_PATH)/BR.VC.ModGen.exe"
VCPL:=@"$(AS_BIN_PATH)/BR.VC.PL.exe"
VCHWPP:=@"$(AS_BIN_PATH)/BR.VC.HWPP.exe"
VCDEP:=@"$(AS_BIN_PATH)/BR.VC.Depend.exe"
VCFLGEN:=@"$(AS_BIN_PATH)/BR.VC.lfgen.exe"
VCREFHANDLER:=@"$(AS_BIN_PATH)/BR.VC.CrossReferenceHandler.exe"
VCXREFEXTENDER:=@"$(AS_BIN_PATH)/BR.AS.CrossRefVCExtender.exe"
RM=CMD /C DEL
PALFILE_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Palette.vcr
VCCFLAGS_ExtCtr=-server -proj ExtCtr -vc "$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/VCObject.vc" -prj_path "$(AS_PROJECT_PATH)" -temp_path "$(AS_TEMP_PATH)" -cfg $(AS_CONFIGURATION) -plc $(AS_PLC) -plctemp $(AS_TEMP_PLC) -cpu_path "$(AS_CPU_PATH)"
VCFIRMWARE=4.25.0
VCFIRMWAREPATH=$(AS_VC_PATH)/Firmware/V4.25.0/SG4
VCOBJECT_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/VCObject.vc
VCSTARTUP="vcstart.br"
VCLOD="vclod.br"
VCSTPOST="vcstpost.br"
TARGET_FILE_ExtCtr=$(AS_CPU_PATH)/ExtCtr.br
OBJ_SCOPE_ExtCtr=Tasks/ExtCtrlIf
PRJ_PATH_ExtCtr=$(AS_PROJECT_PATH)
SRC_PATH_ExtCtr=$(AS_PROJECT_PATH)/Logical/$(OBJ_SCOPE_ExtCtr)/ExtCtrlIf_visu
TEMP_PATH_ExtCtr=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/ExtCtr
TEMP_PATH_Shared=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared
TEMP_PATH_ROOT_ExtCtr=$(AS_TEMP_PATH)
VC_LIBRARY_LIST_ExtCtr=$(TEMP_PATH_ExtCtr)/libraries.vci
VC_XREF_BUILDFILE_ExtCtr=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.build
VC_XREF_CLEANFILE=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.clean
VC_LANGUAGES_ExtCtr=$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr
CPUHWC="$(TEMP_PATH_ExtCtr)/cpuhwc.vci"
VC_STATIC_OPTIONS_ExtCtr="$(TEMP_PATH_ExtCtr)/vcStaticOptions.xml"
VC_STATIC_OPTIONS_Shared="$(TEMP_PATH_Shared)/vcStaticOptions.xml"
# include Shared and Font Makefile (only once)
	include $(AS_TEMP_PATH)/objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCFntDat/Font_ExtCtr.mak
ifneq ($(VCINC),1)
	VCINC=1
	include $(AS_TEMP_PATH)/objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/VCShared.mak
endif

DEPENDENCIES_ExtCtr=-d vcgclass -profile "False"
DEFAULT_STYLE_SHEET_ExtCtr=Source[local].StyleSheet[Color]
SHARED_MODULE=$(TEMP_PATH_ROOT_ExtCtr)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcshared.br
LFNTFLAGS_ExtCtr=-P "$(AS_PROJECT_PATH)" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)"
BDRFLAGS_ExtCtr=-P "$(AS_PROJECT_PATH)" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)"

# Local Libs
LIB_LOCAL_OBJ_ExtCtr=$(TEMP_PATH_ExtCtr)/localobj.vca

# Hardware sources
PANEL_HW_OBJECT_ExtCtr=$(TEMP_PATH_ROOT_ExtCtr)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/ExtCtr/dis.Hardware.vco
PANEL_HW_VCI_ExtCtr=$(TEMP_PATH_ROOT_ExtCtr)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/ExtCtr/dis.Hardware.vci
PANEL_HW_SOURCE_ExtCtr=C:/Users/Resmi/Desktop/ColdjetGit/Physical/Simulation/Hardware.hw 
DIS_OBJECTS_ExtCtr=$(PANEL_HW_OBJECT_ExtCtr) $(KEYMAP_OBJECTS_ExtCtr)

# KeyMapping flags
$(TEMP_PATH_ExtCtr)/dis.PS2-Keyboard1.vco: $(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)/$(AS_PLC)/VC/PS2-Keyboard1.dis
	$(VCHWPP) -f "$(PANEL_HW_SOURCE_ExtCtr)" -o "$(subst .vco,.vci,$(TEMP_PATH_ExtCtr)/dis.PS2-Keyboard1.vco)" -n ExtCtrlIf_visu -d ExtCtr -pal "$(PALFILE_ExtCtr)" -c "$(AS_CONFIGURATION)" -p "$(AS_PLC)" -ptemp "$(AS_TEMP_PLC)" -B "M4.10" -L "AsRfbExt: V*, visapi: V*" -hw "$(CPUHWC)" -warninglevel 2 -so $(VC_STATIC_OPTIONS_ExtCtr) -sos $(VC_STATIC_OPTIONS_Shared) -keyboard "$(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)/$(AS_PLC)/VC/PS2-Keyboard1.dis" -fp "$(AS_VC_PATH)/Firmware/V4.25.0/SG4" -prj "$(AS_PROJECT_PATH)" -apj "Coldjetorg" -sfas -vcob "$(VCOBJECT_ExtCtr)"
	$(VCC) -f "$(subst .vco,.vci,$@)" -o "$@" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -k "$(VCVK_SOURCES_ExtCtr)" $(VCCFLAGS_ExtCtr) -p ExtCtr -sfas

KEYMAP_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)/$(AS_PLC)/VC/PS2-Keyboard1.dis 
KEYMAP_OBJECTS_ExtCtr=$(TEMP_PATH_ExtCtr)/dis.PS2-Keyboard1.vco 

# All Source Objects
FNINFO_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Fonts/DefaultFont.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Fonts/Arial9px.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Fonts/Arial9pxBold.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Fonts/Arial10pxBold.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Fonts/Arial12px.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Fonts/Arial9pxValue.fninfo 

BMINFO_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA3.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadVGA_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA2_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA3_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA1_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadVGA.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/NumPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/NumPad.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AcknowledgeReset.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmActive.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmBypassOFF.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmBypassON.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmInactive.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmLatched.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmNotQuit.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmQuit.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/Reset.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ResetAcknowledge.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/Triggered.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadHor.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadHor_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadVer.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadVer_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/backward_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/backward_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/forward_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/forward_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/control_button_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/control_button_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/right_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/right_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/left_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/left_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_up_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_down_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_down_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ProgressBorder.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_active_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_pressed_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_active_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_gradient_upside.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_gradient_downside.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/frame_header.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_pressed_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/EditPadVGA.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/EditPadVGA_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneNumPad_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadVer_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPad_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA1_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA1_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA2_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA3_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditpadQVGA2_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditPadQVGA3_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditPadVga_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditPadVga_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadHor_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadHor_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadVer_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneNumPad_pressed.bminfo 

BMGRP_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/AlphaPadQVGA.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/NumPad.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/AlphaPad.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/AlarmEvent.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/AlarmState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/BypassState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/AcknowledgeState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/NavigationPad.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/Borders.bmgrp 

PAGE_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Pages/Init_Page.page 

VCS_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/StyleSheets/Gray.vcs \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/StyleSheets/Color.vcs 

BDR_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Raised.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Sunken.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Etched.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Bump.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/SunkenOuter.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/RaisedInner.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Flat_black.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Flat_grey.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/BackwardActive.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/BackwardPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ControlActive.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ControlPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/DownActiveControl.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/DownPressedControl.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ForwardActive.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ForwardPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/GlobalAreaActive.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/GlobalAreaPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/MultiScrollDownActive.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/MultiScrollDownPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/MultiScrollUpActive.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/MultiScrollUpPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ProgressBarBorder.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollDownActive.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollDownPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollUpActive.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollUpPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollLeftActive.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollLeftPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollRightActive.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollRightPressed.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/UpActiveControl.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/UpPressedControl.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/FrameHeader.bdr 

TPR_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/NumPad.tpr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/AlphaPadQVGA.tpr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/AlphaPad.tpr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/NavigationPad_ver.tpr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/NavigationPad_hor.tpr \
	$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/EditPad.tpr 

TDC_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Trends/TrendData.tdc 

VCVK_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/VirtualKeys.vcvk 

VCR_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Palette.vcr 

# Runtime Object sources
VCR_OBJECT_ExtCtr=$(TEMP_PATH_ExtCtr)/vcrt.vco
VCR_SOURCE_ExtCtr=$(SRC_PATH_ExtCtr)/package.vcp
# All Source Objects END

#Panel Hardware
$(PANEL_HW_VCI_ExtCtr): $(PANEL_HW_SOURCE_ExtCtr)
	$(VCHWPP) -f "$<" -o "$@" -n ExtCtrlIf_visu -d ExtCtr -pal "$(PALFILE_ExtCtr)" -c "$(AS_CONFIGURATION)" -p "$(AS_PLC)" -ptemp "$(AS_TEMP_PLC)" -B "M4.10" -L "AsRfbExt: V*, visapi: V*" -verbose "False" -profile "False" -hw "$(CPUHWC)" -warninglevel 2 -so $(VC_STATIC_OPTIONS_ExtCtr) -sos $(VC_STATIC_OPTIONS_Shared) -fp "$(AS_VC_PATH)/Firmware/V4.25.0/SG4" -sfas -prj "$(AS_PROJECT_PATH)" -apj "Coldjetorg" -vcob "$(VCOBJECT_ExtCtr)"

$(PANEL_HW_OBJECT_ExtCtr): $(PANEL_HW_VCI_ExtCtr) $(PALFILE_ExtCtr) $(VC_LIBRARY_LIST_ExtCtr) $(KEYMAP_SOURCES_ExtCtr)
	$(VCC) -f "$(subst .vco,.vci,$@)" -o "$@" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -k "$(VCVK_SOURCES_ExtCtr)" $(VCCFLAGS_ExtCtr) -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


# Pages
PAGE_OBJECTS_ExtCtr = $(addprefix $(TEMP_PATH_ExtCtr)/page., $(notdir $(PAGE_SOURCES_ExtCtr:.page=.vco)))

$(TEMP_PATH_ExtCtr)/page.Init_Page.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Pages/Init_Page.page $(VC_LANGUAGES_ExtCtr)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_ExtCtr)/StyleSheets/Color.vcs" -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


#Pages END




# Stylesheets
VCS_OBJECTS_ExtCtr = $(addprefix $(TEMP_PATH_ExtCtr)/vcs., $(notdir $(VCS_SOURCES_ExtCtr:.vcs=.vco)))

$(TEMP_PATH_ExtCtr)/vcs.Gray.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/StyleSheets/Gray.vcs
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr) -P "$(AS_PROJECT_PATH)" -ds $(DEFAULT_STYLE_SHEET_ExtCtr) -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/vcs.Color.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/StyleSheets/Color.vcs
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr) -P "$(AS_PROJECT_PATH)" -ds $(DEFAULT_STYLE_SHEET_ExtCtr) -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


#Stylesheets END




# Virtual Keys
VCVK_OBJECTS_ExtCtr = $(addprefix $(TEMP_PATH_ExtCtr)/vcvk., $(notdir $(VCVK_SOURCES_ExtCtr:.vcvk=.vco)))

$(TEMP_PATH_ExtCtr)/vcvk.VirtualKeys.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/VirtualKeys.vcvk
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas

$(VCVK_OBJECTS_ExtCtr): $(VC_LANGUAGES_ExtCtr)

#Virtual Keys END




# Touch Pads
TPR_OBJECTS_ExtCtr = $(addprefix $(TEMP_PATH_ExtCtr)/tpr., $(notdir $(TPR_SOURCES_ExtCtr:.tpr=.vco)))

$(TEMP_PATH_ExtCtr)/tpr.NumPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/NumPad.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu" -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/tpr.AlphaPadQVGA.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/AlphaPadQVGA.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu" -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/tpr.AlphaPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/AlphaPad.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu" -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/tpr.NavigationPad_ver.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/NavigationPad_ver.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu" -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/tpr.NavigationPad_hor.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/NavigationPad_hor.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu" -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/tpr.EditPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/TouchPads/EditPad.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu" -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


#Touch Pads END




# BitmapGroups
BMGRP_OBJECTS_ExtCtr = $(addprefix $(TEMP_PATH_ExtCtr)/bmgrp., $(notdir $(BMGRP_SOURCES_ExtCtr:.bmgrp=.vco)))

$(TEMP_PATH_ExtCtr)/bmgrp.AlphaPadQVGA.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/AlphaPadQVGA.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bmgrp.NumPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/NumPad.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bmgrp.AlphaPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/AlphaPad.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bmgrp.AlarmEvent.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/AlarmEvent.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bmgrp.AlarmState.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/AlarmState.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bmgrp.BypassState.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/BypassState.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bmgrp.AcknowledgeState.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/AcknowledgeState.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bmgrp.NavigationPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/NavigationPad.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bmgrp.Borders.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/BitmapGroups/Borders.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


#BitmapGroups END




# Bitmaps
BMINFO_OBJECTS_ExtCtr = $(addprefix $(TEMP_PATH_ExtCtr)/bminfo., $(notdir $(BMINFO_SOURCES_ExtCtr:.bminfo=.vco)))

$(TEMP_PATH_ExtCtr)/bminfo.AlphaPadQVGA1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA1.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlphaPadQVGA2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA2.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlphaPadQVGA3.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA3.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA3.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlphaPadVGA_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadVGA_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadVGA_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlphaPadQVGA2_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA2_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA2_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlphaPadQVGA3_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA3_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA3_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlphaPadQVGA1_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA1_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadQVGA1_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlphaPadVGA.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadVGA.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlphaPadVGA.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.NumPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/NumPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/NumPad_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.NumPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/NumPad.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/NumPad.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AcknowledgeReset.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AcknowledgeReset.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AcknowledgeReset.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlarmActive.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmActive.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmActive.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlarmBypassOFF.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmBypassOFF.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmBypassOFF.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlarmBypassON.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmBypassON.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmBypassON.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlarmInactive.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmInactive.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmInactive.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlarmLatched.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmLatched.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmLatched.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlarmNotQuit.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmNotQuit.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmNotQuit.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.AlarmQuit.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmQuit.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/AlarmQuit.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.Reset.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/Reset.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/Reset.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.ResetAcknowledge.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ResetAcknowledge.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ResetAcknowledge.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.Triggered.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/Triggered.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/Triggered.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.ListPadHor.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadHor.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadHor.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.ListPadHor_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadHor_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadHor_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.ListPadVer.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadVer.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadVer.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.ListPadVer_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadVer_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ListPadVer_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.backward_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/backward_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/backward_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.backward_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/backward_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/backward_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.global_area_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.global_area_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.forward_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/forward_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/forward_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.forward_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/forward_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/forward_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.control_button_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/control_button_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/control_button_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.control_button_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/control_button_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/control_button_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.right_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/right_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/right_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.right_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/right_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/right_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.left_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/left_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/left_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.left_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/left_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/left_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.up_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.up_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.down_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.down_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.multi_up_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_up_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_up_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.multi_up_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_up_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.multi_down_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_down_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_down_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.multi_down_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_down_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/multi_down_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.ProgressBorder.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ProgressBorder.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/ProgressBorder.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.down_active_control.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_active_control.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_active_control.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.down_pressed_control.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_pressed_control.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/down_pressed_control.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.up_active_control.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_active_control.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_active_control.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.global_area_gradient_upside.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_gradient_upside.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_gradient_upside.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.global_area_gradient_downside.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_gradient_downside.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/global_area_gradient_downside.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.frame_header.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/frame_header.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/frame_header.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.up_pressed_control.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_pressed_control.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/up_pressed_control.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.EditPadVGA.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/EditPadVGA.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/EditPadVGA.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.EditPadVGA_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/EditPadVGA_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/EditPadVGA_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneNumPad_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneNumPad_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneNumPad_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneListPadVer_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadVer_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadVer_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneAlphaPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPad_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneAlphaPad_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPad_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPad_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneAlphaPadQVGA1_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA1_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA1_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneAlphaPadQVGA1_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA1_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA1_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneAlphaPadQVGA2_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA2_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA2_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneAlphaPadQVGA3_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA3_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneAlphaPadQVGA3_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneEditpadQVGA2_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditpadQVGA2_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditpadQVGA2_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneEditPadQVGA3_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditPadQVGA3_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditPadQVGA3_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneEditPadVga_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditPadVga_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditPadVga_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneEditPadVga_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditPadVga_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneEditPadVga_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneListPadHor_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadHor_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadHor_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneListPadHor_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadHor_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadHor_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneListPadVer_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadVer_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneListPadVer_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


$(TEMP_PATH_ExtCtr)/bminfo.zuneNumPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneNumPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Bitmaps/zuneNumPad_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


#Bitmaps END




# Trend Data Configuration
TDC_OBJECTS_ExtCtr = $(addprefix $(TEMP_PATH_ExtCtr)/tdc., $(notdir $(TDC_SOURCES_ExtCtr:.tdc=.vco)))

$(TEMP_PATH_ExtCtr)/tdc.TrendData.vco: $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Trends/TrendData.tdc
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_ExtCtr)" $(VCCFLAGS_ExtCtr)  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas


#Trend Data Configuration END


#
# Borders
#
BDR_SOURCES_ExtCtr=$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Raised.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Sunken.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Etched.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Bump.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/SunkenOuter.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/RaisedInner.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Flat_black.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/Flat_grey.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/BackwardActive.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/BackwardPressed.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ControlActive.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ControlPressed.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/DownActiveControl.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/DownPressedControl.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ForwardActive.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ForwardPressed.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/GlobalAreaActive.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/GlobalAreaPressed.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/MultiScrollDownActive.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/MultiScrollDownPressed.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/MultiScrollUpActive.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/MultiScrollUpPressed.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ProgressBarBorder.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollDownActive.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollDownPressed.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollUpActive.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollUpPressed.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollLeftActive.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollLeftPressed.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollRightActive.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/ScrollRightPressed.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/UpActiveControl.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/UpPressedControl.bdr $(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Borders/FrameHeader.bdr 
BDR_OBJECTS_ExtCtr=$(TEMP_PATH_ExtCtr)/bdr.Bordermanager.vco
$(TEMP_PATH_ExtCtr)/bdr.Bordermanager.vco: $(BDR_SOURCES_ExtCtr)
	$(VCC) -f "$<" -o "$@" -pkg "$(SRC_PATH_ExtCtr)" $(BDRFLAGS_ExtCtr) $(VCCFLAGS_ExtCtr) -p ExtCtr$(SRC_PATH_ExtCtr)
#
# Logical fonts
#
$(TEMP_PATH_ExtCtr)/lfnt.en.vco: $(TEMP_PATH_ExtCtr)/en.lfnt $(VC_LANGUAGES_ExtCtr)
	 $(VCC) -f "$<" -o "$@" $(LFNTFLAGS_ExtCtr) $(VCCFLAGS_ExtCtr) -p ExtCtr -sfas
$(TEMP_PATH_ExtCtr)/lfnt.de.vco: $(TEMP_PATH_ExtCtr)/de.lfnt $(VC_LANGUAGES_ExtCtr)
	 $(VCC) -f "$<" -o "$@" $(LFNTFLAGS_ExtCtr) $(VCCFLAGS_ExtCtr) -p ExtCtr -sfas
LFNT_OBJECTS_ExtCtr=$(TEMP_PATH_ExtCtr)/lfnt.en.vco $(TEMP_PATH_ExtCtr)/lfnt.de.vco 

#Runtime Object
$(VCR_OBJECT_ExtCtr) : $(VCR_SOURCE_ExtCtr)
	$(VCC) -f "$<" -o "$@" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -sl de $(VCCFLAGS_ExtCtr) -rt  -p ExtCtr -so $(VC_STATIC_OPTIONS_ExtCtr) -vcr 4250 -sfas
# Local resources Library rules
LIB_LOCAL_RES_ExtCtr=$(TEMP_PATH_ExtCtr)/localres.vca
$(LIB_LOCAL_RES_ExtCtr) : $(TEMP_PATH_ExtCtr)/ExtCtr02.ccf

# Bitmap Library rules
LIB_BMP_RES_ExtCtr=$(TEMP_PATH_ExtCtr)/bmpres.vca
$(LIB_BMP_RES_ExtCtr) : $(TEMP_PATH_ExtCtr)/ExtCtr03.ccf
$(BMGRP_OBJECTS_ExtCtr) : $(PALFILE_ExtCtr) $(VC_LANGUAGES_ExtCtr)
$(BMINFO_OBJECTS_ExtCtr) : $(PALFILE_ExtCtr)

BUILD_FILE_ExtCtr=$(TEMP_PATH_ExtCtr)/BuildFiles.arg
$(BUILD_FILE_ExtCtr) : BUILD_FILE_CLEAN_ExtCtr $(BUILD_SOURCES_ExtCtr)
BUILD_FILE_CLEAN_ExtCtr:
	$(RM) /F /Q "$(BUILD_FILE_ExtCtr)" 2>nul
#All Modules depending to this project
PROJECT_MODULES_ExtCtr=$(AS_CPU_PATH)/ExtCtr01.br $(AS_CPU_PATH)/ExtCtr02.br $(AS_CPU_PATH)/ExtCtr03.br $(FONT_MODULES_ExtCtr) $(SHARED_MODULE)

# General Build rules

$(TARGET_FILE_ExtCtr): $(PROJECT_MODULES_ExtCtr) $(TEMP_PATH_ExtCtr)/ExtCtr.prj
	$(MODGEN) -so $(VC_STATIC_OPTIONS_ExtCtr) -fw "$(VCFIRMWAREPATH)" -m $(VCSTPOST) -v V1.00.0 -f "$(TEMP_PATH_ExtCtr)/ExtCtr.prj" -o "$@" -vc "$(VCOBJECT_ExtCtr)" $(DEPENDENCIES_ExtCtr) $(addprefix -d ,$(notdir $(PROJECT_MODULES_ExtCtr:.br=)))

$(AS_CPU_PATH)/ExtCtr01.br: $(TEMP_PATH_ExtCtr)/ExtCtr01.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_ExtCtr) -fw "$(VCFIRMWAREPATH)" -m $(VCLOD) -v V1.00.0 -b -vc "$(VCOBJECT_ExtCtr)" -f "$<" -o "$@" $(DEPENDENCIES_ExtCtr)

$(AS_CPU_PATH)/ExtCtr02.br: $(TEMP_PATH_ExtCtr)/ExtCtr02.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_ExtCtr) -fw "$(VCFIRMWAREPATH)" -m $(VCLOD) -v V1.00.0 -b -vc "$(VCOBJECT_ExtCtr)" -f "$<" -o "$@" $(DEPENDENCIES_ExtCtr)

$(AS_CPU_PATH)/ExtCtr03.br: $(TEMP_PATH_ExtCtr)/ExtCtr03.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_ExtCtr) -fw "$(VCFIRMWAREPATH)" -m $(VCLOD) -v V1.00.0 -b -vc "$(VCOBJECT_ExtCtr)" -f "$<" -o "$@" $(DEPENDENCIES_ExtCtr)

# General Build rules END
$(LIB_LOCAL_OBJ_ExtCtr) : $(TEMP_PATH_ExtCtr)/ExtCtr01.ccf

# Main Module
$(TEMP_PATH_ROOT_ExtCtr)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/ExtCtr.vcm:
$(TEMP_PATH_ExtCtr)/ExtCtr.prj: $(TEMP_PATH_ROOT_ExtCtr)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/ExtCtr.vcm
	$(VCDEP) -m "$(TEMP_PATH_ROOT_ExtCtr)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/ExtCtr.vcm" -s "$(AS_CPU_PATH)/VCShared/Shared.vcm" -p "$(AS_PATH)/AS/VC/Firmware" -c "$(AS_CPU_PATH)" -fw "$(VCFIRMWAREPATH)" -hw "$(CPUHWC)" -so $(VC_STATIC_OPTIONS_ExtCtr) -o ExtCtr -proj ExtCtr
	$(VCPL) $(notdir $(PROJECT_MODULES_ExtCtr:.br=,4)) ExtCtr,2 -o "$@" -p ExtCtr -vc "ExtCtr" -verbose "False" -fl "$(TEMP_PATH_ROOT_ExtCtr)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/ExtCtr.vcm" -vcr "$(VCR_SOURCE_ExtCtr)" -prj "$(AS_PROJECT_PATH)" -warningLevel2 -sfas

# 01 Module

DEL_TARGET01_LFL_ExtCtr=$(TEMP_PATH_ExtCtr)\ExtCtr01.ccf.lfl
$(TEMP_PATH_ExtCtr)/ExtCtr01.ccf: $(LIB_SHARED) $(SHARED_CCF) $(LIB_BMP_RES_ExtCtr) $(TEMP_PATH_ExtCtr)/ExtCtr03.ccf $(LIB_LOCAL_RES_ExtCtr) $(TEMP_PATH_ExtCtr)/ExtCtr02.ccf $(DIS_OBJECTS_ExtCtr) $(PAGE_OBJECTS_ExtCtr) $(VCS_OBJECTS_ExtCtr) $(VCVK_OBJECTS_ExtCtr) $(VCRT_OBJECTS_ExtCtr) $(TPR_OBJECTS_ExtCtr) $(TXTGRP_OBJECTS_ExtCtr) $(LAYER_OBJECTS_ExtCtr) $(VCR_OBJECT_ExtCtr) $(TDC_OBJECTS_ExtCtr) $(TRD_OBJECTS_ExtCtr) $(TRE_OBJECTS_ExtCtr) $(PRC_OBJECTS_ExtCtr) $(SCR_OBJECTS_ExtCtr)
	-@CMD /Q /C if exist "$(DEL_TARGET01_LFL_ExtCtr)" DEL /F /Q "$(DEL_TARGET01_LFL_ExtCtr)" 2>nul
	@$(VCFLGEN) "$@.lfl" "$(LIB_SHARED)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(LIB_BMP_RES_ExtCtr)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(LIB_LOCAL_RES_ExtCtr)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(DIS_OBJECTS_ExtCtr:.vco=.vco|)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .page -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Package.vcp" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(VCS_OBJECTS_ExtCtr:.vco=.vco|)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .vcvk -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Package.vcp" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(VCRT_OBJECTS_ExtCtr:.vco=.vco|)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(TPR_OBJECTS_ExtCtr:.vco=.vco|)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .txtgrp -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Package.vcp" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .layer -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Package.vcp" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(VCR_OBJECT_ExtCtr:.vco=.vco|)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .tdc -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Package.vcp" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .trd -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Package.vcp" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(SCR_OBJECTS_ExtCtr:.vco=.vco|)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	$(LINK) "$@.lfl" -o "$@" -p ExtCtr -lib "$(LIB_LOCAL_OBJ_ExtCtr)" -P "$(AS_PROJECT_PATH)" -m "local objects" -profile "False" -warningLevel2 -vcr 4250 -sfas
# 01 Module END

# 02 Module

DEL_TARGET02_LFL_ExtCtr=$(TEMP_PATH_ExtCtr)\ExtCtr02.ccf.lfl
$(TEMP_PATH_ExtCtr)/ExtCtr02.ccf: $(LIB_SHARED) $(SHARED_CCF) $(LIB_BMP_RES_ExtCtr) $(TEMP_PATH_ExtCtr)/ExtCtr03.ccf $(BDR_OBJECTS_ExtCtr) $(LFNT_OBJECTS_ExtCtr) $(CLM_OBJECTS_ExtCtr)
	-@CMD /Q /C if exist "$(DEL_TARGET02_LFL_ExtCtr)" DEL /F /Q "$(DEL_TARGET02_LFL_ExtCtr)" 2>nul
	@$(VCFLGEN) "$@.lfl" "$(LIB_SHARED)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(LIB_BMP_RES_ExtCtr)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(BDR_OBJECTS_ExtCtr:.vco=.vco|)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(LFNT_OBJECTS_ExtCtr:.vco=.vco|)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(CLM_OBJECTS_ExtCtr:.vco=.vco|)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	$(LINK) "$@.lfl" -o "$@" -p ExtCtr -lib "$(LIB_LOCAL_RES_ExtCtr)" -P "$(AS_PROJECT_PATH)" -m "local resources" -profile "False" -warningLevel2 -vcr 4250 -sfas
# 02 Module END

# 03 Module

DEL_TARGET03_LFL_ExtCtr=$(TEMP_PATH_ExtCtr)\ExtCtr03.ccf.lfl
$(TEMP_PATH_ExtCtr)/ExtCtr03.ccf: $(LIB_SHARED) $(SHARED_CCF) $(BMGRP_OBJECTS_ExtCtr) $(BMINFO_OBJECTS_ExtCtr) $(PALFILE_ExtCtr)
	-@CMD /Q /C if exist "$(DEL_TARGET03_LFL_ExtCtr)" DEL /F /Q "$(DEL_TARGET03_LFL_ExtCtr)" 2>nul
	@$(VCFLGEN) "$@.lfl" "$(LIB_SHARED)" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .bmgrp -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Package.vcp" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .bminfo -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/ExtCtrlIf/ExtCtrlIf_visu/Package.vcp" -temp "$(TEMP_PATH_ExtCtr)" -prj "$(PRJ_PATH_ExtCtr)" -sfas
	$(LINK) "$@.lfl" -o "$@" -p ExtCtr -lib "$(LIB_BMP_RES_ExtCtr)" -P "$(AS_PROJECT_PATH)" -m "bitmap resources" -profile "False" -warningLevel2 -vcr 4250 -sfas
# 03 Module END

# Post Build Steps

.PHONY : vcPostBuild_ExtCtr

vcPostBuild_ExtCtr :
	$(VCC) -pb -vcm "$(TEMP_PATH_ExtCtr)/MODULEFILES.vcm" -fw "$(VCFIRMWAREPATH)" $(VCCFLAGS_ExtCtr) -p ExtCtr -vcr 4250 -sfas

# Post Build Steps END
