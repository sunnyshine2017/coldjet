UnmarkedObjectFolder := C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/Visu/Visu
MarkedObjectFolder := C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/Visu/Visu

$(AS_CPU_PATH)/Visu.br: \
	$(AS_PROJECT_CPU_PATH)/Cpu.per \
	$(AS_CPU_PATH)/Visu/Visu.ox
	@"$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe" "$(AS_CPU_PATH)/Visu/Visu.ox" -o "$(AS_CPU_PATH)/Visu.br" -v V1.00.0 -f "$(AS_CPU_PATH)/NT.ofs" -offsetLT "$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs" -T SG4  -M IA32  -B M4.10 -extConstants  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -r Cyclic8 -p 7 -s "Tasks.Visu.Visu" -L "Acp10_MC: V2.45.1, Acp10man: V2.45.1, Acp10par: V2.45.1, Acp10sdc: V2.45.1, Acp10sim: V2.45.1, AppLib: V*, AsARCfg: V*, AsBrMath: V*, AsBrStr: V*, AsIecCon: V*, AsIOAcc: V*, AsIORTI: V*, AsIOTime: V*, ASMcDcs: V*, AsRfbExt: V*, asstring: V*, astime: V*, AsUSB: V*, AsWeigh: V*, BrbLib: V2.03.0, brsystem: V*, DataObj: V*, FileIO: V*, MTBasics: V2.10.0, MTFilter: V2.10.0, MTTypes: V1.03.0, NcGlobal: V2.45.1, Operator: V*, Runtime: V*, standard: V*, sys_lib: V*, visapi: V*" -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.taskbuilder.exe"

$(AS_CPU_PATH)/Visu/Visu.ox: \
	$(AS_CPU_PATH)/Visu/a.out
	@"$(AS_BIN_PATH)/BR.AS.Backend.exe" "$(AS_CPU_PATH)/Visu/a.out" -o "$(AS_CPU_PATH)/Visu/Visu.ox" -T SG4 -r Cyclic8  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES  -G V4.1.2  -secret "$(AS_PROJECT_PATH)_br.as.backend.exe"

$(AS_CPU_PATH)/Visu/a.out: \
	$(AS_CPU_PATH)/Visu/Visu.c.o \
	$(AS_CPU_PATH)/Visu/VisuFunc.c.o
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" -link  -O "$(AS_CPU_PATH)//Visu/Visu.out.opt" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/Visu/Visu.c.o: \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/Visu/Visu.c \
	FORCE 
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/Visu/Visu.c" -o "$(AS_CPU_PATH)/Visu/Visu.c.o"  -T SG4  -M IA32  -B M4.10 -G V4.1.2  -s "Tasks.Visu.Visu" -t "$(AS_TEMP_PATH)" -I "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/Visu" "$(AS_TEMP_PATH)/Includes/Tasks/Visu/Visu"  "$(AS_TEMP_PATH)/Includes/Tasks/Visu"  "$(AS_TEMP_PATH)/Includes/Tasks" "$(AS_TEMP_PATH)/Includes" "$(AS_PROJECT_PATH)/Logical/Global" -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include "$(AS_CPU_PATH)/Libraries.h"  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -x c -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/Visu/VisuFunc.c.o: \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/Visu/VisuFunc.c \
	FORCE 
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/Visu/VisuFunc.c" -o "$(AS_CPU_PATH)/Visu/VisuFunc.c.o"  -T SG4  -M IA32  -B M4.10 -G V4.1.2  -s "Tasks.Visu.Visu" -t "$(AS_TEMP_PATH)" -I "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/Visu" "$(AS_TEMP_PATH)/Includes/Tasks/Visu/Visu"  "$(AS_TEMP_PATH)/Includes/Tasks/Visu"  "$(AS_TEMP_PATH)/Includes/Tasks" "$(AS_TEMP_PATH)/Includes" "$(AS_PROJECT_PATH)/Logical/Global" -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include "$(AS_CPU_PATH)/Libraries.h"  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -x c -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

-include $(AS_CPU_PATH)/Force.mak 



FORCE: