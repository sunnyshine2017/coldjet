$(AS_CPU_PATH)/Stitchmod1.br: \
	$(AS_PROJECT_PATH)/Logical/Tasks/Stitchmode/StitchmodeCtrl/StitchmodeCtrlCyclic.rti \
	$(AS_PROJECT_PATH)/Temp/Objects/$(AS_CONFIGURATION)/$(AS_PLC)/ReActionTechnologyConfiguration.xml \
	$(AS_PROJECT_PATH)/Logical/Tasks/Stitchmode/StitchmodeCtrl/StitchmodeCtrl.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsIORTI/AsIORTI.fun
	@"$(AS_BIN_PATH)/BR.AS.ReActionTechnologyBuilder.exe"   "$(AS_PROJECT_PATH)/Logical/Tasks/Stitchmode/StitchmodeCtrl/StitchmodeCtrlCyclic.rti"   -x "$(AS_CPU_PATH)/ReActionTechnologyConfiguration.xml" -B M4.10 -o "$(AS_CPU_PATH)/Stitchmod1.br" -P "$(AS_PROJECT_PATH)" -c "$(AS_CONFIGURATION)" -S "$(AS_PLC)" -s "Tasks.Stitchmode.StitchmodeCtrl"  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -secret "$(AS_PROJECT_PATH)_br.as.reactiontechnologybuilder.exe"

-include $(AS_CPU_PATH)/Force.mak 
