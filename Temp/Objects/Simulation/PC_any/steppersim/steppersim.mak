$(AS_CPU_PATH)/steppersim.br: \
	$(AS_PROJECT_CPU_PATH)/Motion/StepperSim.ncm \
	$(AS_PROJECT_PATH)/physical/simulation/pc_any/motion/steppersim.ncm \
	$(AS_PROJECT_PATH)/physical/simulation/hardware.hw
	@"$(AS_BIN_PATH)/BR.MC.Builder.exe" "$(AS_PROJECT_CPU_PATH)/Motion/StepperSim.ncm"  -v V1.00.0 -a "$(AS_PROJECT_CONFIG_PATH)/Hardware.hw" -L "Acp10_MC: V2.45.1, Acp10man: V2.45.1, Acp10par: V2.45.1, NcGlobal: V2.45.1" -P "$(AS_PROJECT_PATH)" -o "$(AS_CPU_PATH)/StepperSim.br" -T SG4  -s "Simulation.PC_any.Motion"  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -secret "$(AS_PROJECT_PATH)_br.mc.builder.exe"

-include $(AS_CPU_PATH)/Force.mak 
