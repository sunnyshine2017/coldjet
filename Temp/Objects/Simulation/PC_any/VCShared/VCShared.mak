######################################################
#                                                    #
# Automatic generated Makefile for Visual Components #
#                                                    #
#                  Do NOT edit!                      #
#                                                    #
######################################################

VCC:=@"$(AS_BIN_PATH)/br.vc.pc.exe"
LINK:=@"$(AS_BIN_PATH)/BR.VC.Link.exe"
MODGEN:=@"$(AS_BIN_PATH)/BR.VC.ModGen.exe"
VCPL:=@"$(AS_BIN_PATH)/BR.VC.PL.exe"
VCHWPP:=@"$(AS_BIN_PATH)/BR.VC.HWPP.exe"
VCDEP:=@"$(AS_BIN_PATH)/BR.VC.Depend.exe"
VCFLGEN:=@"$(AS_BIN_PATH)/BR.VC.lfgen.exe"
VCREFHANDLER:=@"$(AS_BIN_PATH)/BR.VC.CrossReferenceHandler.exe"
VCXREFEXTENDER:=@"$(AS_BIN_PATH)/BR.AS.CrossRefVCExtender.exe"
RM=CMD /C DEL
PALFILE_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Palette.vcr
VCCFLAGS_VisWvg=-server -proj VisWvg -vc "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/VCObject.vc" -prj_path "$(AS_PROJECT_PATH)" -temp_path "$(AS_TEMP_PATH)" -cfg $(AS_CONFIGURATION) -plc $(AS_PLC) -plctemp $(AS_TEMP_PLC) -cpu_path "$(AS_CPU_PATH)"
VCFIRMWARE=4.25.0
VCFIRMWAREPATH=$(AS_VC_PATH)/Firmware/V4.25.0/SG4
VCOBJECT_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/VCObject.vc
VCSTARTUP="vcstart.br"
VCLOD="vclod.br"
VCSTPOST="vcstpost.br"
TARGET_FILE_VisWvg=$(AS_CPU_PATH)/VisWvg.br
OBJ_SCOPE_VisWvg=Tasks/Visu
PRJ_PATH_VisWvg=$(AS_PROJECT_PATH)
SRC_PATH_VisWvg=$(AS_PROJECT_PATH)/Logical/$(OBJ_SCOPE_VisWvg)/VisWvga
TEMP_PATH_VisWvg=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VisWvg
TEMP_PATH_Shared=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared
TEMP_PATH_ROOT_VisWvg=$(AS_TEMP_PATH)
VC_LIBRARY_LIST_VisWvg=$(TEMP_PATH_VisWvg)/libraries.vci
VC_XREF_BUILDFILE_VisWvg=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.build
VC_XREF_CLEANFILE=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.clean
VC_LANGUAGES_VisWvg=$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr
CPUHWC="$(TEMP_PATH_VisWvg)/cpuhwc.vci"
VC_STATIC_OPTIONS_VisWvg="$(TEMP_PATH_VisWvg)/vcStaticOptions.xml"
VC_STATIC_OPTIONS_Shared="$(TEMP_PATH_Shared)/vcStaticOptions.xml"

DSOFLAGS=-P "$(AS_PROJECT_PATH)" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)"
LIB_SHARED=$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/vcshared.vca

#
# Shared Runtime Options
#
VCRS_OBJECT=$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/vcrt_s.vco
VCRS_SOURCE=$(AS_PROJECT_PATH)/Logical/VCShared/Package.vcp

# All Shared Source Objects
VCR_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr 

TXTGRP_SHARED_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/AlarmAcknowledgeState.txtgrp \
	$(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/AlarmBypassState.txtgrp \
	$(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/AlarmEvent.txtgrp \
	$(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/AlarmState.txtgrp \
	$(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/DateTimeFormats.txtgrp \
	$(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/httpURL_SDM.txtgrp 

VCUG_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_LengthMillimeterToMillimeter.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Mass.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Volume.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Power.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Memory.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Pressure.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Temperatures.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_TimeMilliSecondsToMinutes.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_TimeMilliSecondsToSeconds.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_IndexToCount.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_TimeMilliSeconds.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_Percent.vcug \
	$(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_LengthMillimeter100ToMillim.vcug 

ALCFG_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/VCShared/AlarmGroups/AlarmSystem.alcfg 

ALGRP_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/VCShared/AlarmGroups/SystemAlarms.algrp \
	$(AS_PROJECT_PATH)/Logical/VCShared/AlarmGroups/AG_Errors.algrp \
	$(AS_PROJECT_PATH)/Logical/VCShared/AlarmGroups/AG_Warnings.algrp \
	$(AS_PROJECT_PATH)/Logical/VCShared/AlarmGroups/AG_Infos.algrp 

DSO_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/VCShared/DataSources/Internal.dso \
	$(AS_PROJECT_PATH)/Logical/VCShared/DataSources/DS_Plc.dso 

CVINFO_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo 



# UnitGroups
VCUG_OBJECTS_VisWvg = $(addprefix $(AS_CPU_PATH)/VCShared/vcug., $(notdir $(VCUG_SOURCES_VisWvg:.vcug=.vco)))

$(AS_CPU_PATH)/VCShared/vcug.UG_LengthMillimeterToMillimeter.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_LengthMillimeterToMillimeter.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.Mass.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Mass.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.Volume.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Volume.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.Power.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Power.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.Memory.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Memory.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.Pressure.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Pressure.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.Temperatures.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/Temperatures.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.UG_TimeMilliSecondsToMinutes.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_TimeMilliSecondsToMinutes.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.UG_TimeMilliSecondsToSeconds.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_TimeMilliSecondsToSeconds.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.UG_IndexToCount.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_IndexToCount.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.UG_TimeMilliSeconds.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_TimeMilliSeconds.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.UG_Percent.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_Percent.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/vcug.UG_LengthMillimeter100ToMillim.vco: $(AS_PROJECT_PATH)/Logical/VCShared/UnitGroups/UG_LengthMillimeter100ToMillim.vcug
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#UnitGroups END




# AlarmGroups
ALGRP_OBJECTS_VisWvg = $(addprefix $(AS_CPU_PATH)/VCShared/algrp., $(notdir $(ALGRP_SOURCES_VisWvg:.algrp=.vco)))

$(AS_CPU_PATH)/VCShared/algrp.SystemAlarms.vco: $(AS_PROJECT_PATH)/Logical/VCShared/AlarmGroups/SystemAlarms.algrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/algrp.AG_Errors.vco: $(AS_PROJECT_PATH)/Logical/VCShared/AlarmGroups/AG_Errors.algrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/algrp.AG_Warnings.vco: $(AS_PROJECT_PATH)/Logical/VCShared/AlarmGroups/AG_Warnings.algrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/algrp.AG_Infos.vco: $(AS_PROJECT_PATH)/Logical/VCShared/AlarmGroups/AG_Infos.algrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#AlarmGroups END




# AlarmSystem
ALCFG_OBJECTS_VisWvg = $(addprefix $(AS_CPU_PATH)/VCShared/alcfg., $(notdir $(ALCFG_SOURCES_VisWvg:.alcfg=.vco)))

$(AS_CPU_PATH)/VCShared/alcfg.AlarmSystem.vco: $(AS_PROJECT_PATH)/Logical/VCShared/AlarmGroups/AlarmSystem.alcfg
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#AlarmSystem END




# Text Groups
TXTGRP_SHARED_OBJECTS_VisWvg = $(addprefix $(AS_CPU_PATH)/VCShared/txtgrp., $(notdir $(TXTGRP_SHARED_SOURCES_VisWvg:.txtgrp=.vco)))

$(AS_CPU_PATH)/VCShared/txtgrp.AlarmAcknowledgeState.vco: $(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/AlarmAcknowledgeState.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/txtgrp.AlarmBypassState.vco: $(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/AlarmBypassState.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/txtgrp.AlarmEvent.vco: $(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/AlarmEvent.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/txtgrp.AlarmState.vco: $(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/AlarmState.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/txtgrp.DateTimeFormats.vco: $(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/DateTimeFormats.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(AS_CPU_PATH)/VCShared/txtgrp.httpURL_SDM.vco: $(AS_PROJECT_PATH)/Logical/VCShared/TextGroups/httpURL_SDM.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#Text Groups END


#
# Datapoint Objects
#
$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/dso.Internal.vco: $(AS_PROJECT_PATH)/Logical/VCShared/DataSources/Internal.dso 
	 $(VCC) -f "$<" -o "$@" $(DSOFLAGS) $(VCCFLAGS_VisWvg) -p VisWvg -vcr 4250 -sfas

$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/dso.DS_Plc.vco: $(AS_PROJECT_PATH)/Logical/VCShared/DataSources/DS_Plc.dso 
	 $(VCC) -f "$<" -o "$@" $(DSOFLAGS) $(VCCFLAGS_VisWvg) -p VisWvg -vcr 4250 -sfas

DPT_OBJECTS = $(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/dpt.DataPointList.vco
DSO_OBJECTS_VisWvg=$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/dso.Internal.vco $(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/dso.DS_Plc.vco 
$(DSO_OBJECTS_VisWvg): $(DSO_SOURCES_VisWvg)


#
# Building the Shared Runtime Options
#
$(VCRS_OBJECT) : $(VCRS_SOURCE)
	$(VCC) -f "$<" -o "$@" -ct shared -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -sl  $(VCCFLAGS_VisWvg) -p VisWvg -vcr 4250 -sfas

#
# The Shared Module
#
SHARED_MODULE=$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcshared.br
SHARED_CCF=$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/vcshared.ccf
DEL_SHARED_CCF=$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/vcshared.ccf.lfl

$(SHARED_MODULE) : $(SHARED_CCF)
	 $(MODGEN) -so $(VC_STATIC_OPTIONS_Shared) -fw "$(VCFIRMWAREPATH)" -m $(VCSTARTUP) -v V1.00.0 -vc "$(VCOBJECT_VisWvg)" -f "$<" -o "$@" -d vcgclass -profile "False"

$(VCUG_OBJECTS_VisWvg): $(VC_LANGUAGES_VisWvg)
$(TXTGRP_SHARED_OBJECTS_VisWvg): $(VC_LANGUAGES_VisWvg)
$(ALGRP_OBJECTS_VisWvg): $(VC_LANGUAGES_VisWvg)
$(ALCFG_OBJECTS_VisWvg): $(VC_LANGUAGES_VisWvg)

$(SHARED_CCF): $(VCRS_OBJECT) $(VCR_OBJECTS_VisWvg) $(VCUG_OBJECTS_VisWvg) $(ALGRP_OBJECTS_VisWvg) $(ALCFG_OBJECTS_VisWvg) $(DSO_OBJECTS_VisWvg) $(TXTGRP_SHARED_OBJECTS_VisWvg) $(CVINFO_OBJECTS_VisWvg)
	-@CMD /Q /C if exist "$(DEL_SHARED_CCF)" DEL /F /Q "$(DEL_SHARED_CCF)" 2>nul
	 @$(VCFLGEN) "$@.lfl" "$(VCR_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)"
	 @$(VCFLGEN) "$@.lfl" -mask .vcug -vcp "$(AS_PROJECT_PATH)/Logical/VCShared/Package.vcp" -temp "$(TEMP_PATH_Shared)" -prj "$(PRJ_PATH_VisWvg)"
	 @$(VCFLGEN) "$@.lfl" -mask .algrp -vcp "$(AS_PROJECT_PATH)/Logical/VCShared/Package.vcp" -temp "$(TEMP_PATH_Shared)" -prj "$(PRJ_PATH_VisWvg)"
	 @$(VCFLGEN) "$@.lfl" "$(ALCFG_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)"
	 @$(VCFLGEN) "$@.lfl" -mask .txtgrp -vcp "$(AS_PROJECT_PATH)/Logical/VCShared/Package.vcp" -temp "$(TEMP_PATH_Shared)" -prj "$(PRJ_PATH_VisWvg)"
	 @$(VCFLGEN) "$@.lfl" "$(DSO_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)"
	 @$(VCFLGEN) "$@.lfl" "$(DPT_OBJECTS:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)"
	 @$(VCFLGEN) "$@.lfl" "$(VCRS_OBJECT)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)"
	 $(LINK) "$@.lfl" -o "$@" -lib "$(LIB_SHARED)" -P "$(AS_PROJECT_PATH)" -m "shared resources" -profile "False" -warningLevel2 -name VisWvg -vcr 4250 -sfas


$(LIB_SHARED): $(SHARED_CCF)
