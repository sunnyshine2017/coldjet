UnmarkedObjectFolder := C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/Encoder/Encoder
MarkedObjectFolder := C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/Encoder/Encoder

$(AS_CPU_PATH)/Encoder.br: \
	$(AS_PROJECT_CPU_PATH)/Cpu.per \
	$(AS_CPU_PATH)/Encoder/Encoder.ox
	@"$(AS_BIN_PATH)/BR.AS.TaskBuilder.exe" "$(AS_CPU_PATH)/Encoder/Encoder.ox" -o "$(AS_CPU_PATH)/Encoder.br" -v V1.00.0 -f "$(AS_CPU_PATH)/NT.ofs" -offsetLT "$(AS_BINARIES_PATH)/$(AS_CONFIGURATION)/$(AS_PLC)/LT.ofs" -T SG4  -M IA32  -B M4.10 -extConstants  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -r Cyclic1 -p 2 -s "Tasks.Encoder.Encoder" -L "Acp10_MC: V2.45.1, Acp10man: V2.45.1, Acp10par: V2.45.1, Acp10sdc: V2.45.1, Acp10sim: V2.45.1, AppLib: V*, AsARCfg: V*, AsBrMath: V*, AsBrStr: V*, AsIecCon: V*, AsIOAcc: V*, AsIORTI: V*, AsIOTime: V*, ASMcDcs: V*, AsRfbExt: V*, asstring: V*, astime: V*, AsUSB: V*, AsWeigh: V*, BrbLib: V2.03.0, brsystem: V*, DataObj: V*, FileIO: V*, MTBasics: V2.10.0, MTFilter: V2.10.0, MTTypes: V1.03.0, NcGlobal: V2.45.1, Operator: V*, Runtime: V*, standard: V*, sys_lib: V*, visapi: V*" -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.taskbuilder.exe"

$(AS_CPU_PATH)/Encoder/Encoder.ox: \
	$(AS_CPU_PATH)/Encoder/a.out
	@"$(AS_BIN_PATH)/BR.AS.Backend.exe" "$(AS_CPU_PATH)/Encoder/a.out" -o "$(AS_CPU_PATH)/Encoder/Encoder.ox" -T SG4 -r Cyclic1  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES  -G V4.1.2  -secret "$(AS_PROJECT_PATH)_br.as.backend.exe"

$(AS_CPU_PATH)/Encoder/a.out: \
	$(AS_CPU_PATH)/Encoder/Encoder.c.o \
	$(AS_CPU_PATH)/Encoder/EncoderFunc.c.o \
	$(AS_CPU_PATH)/Encoder/Global.c.o
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" -link  -O "$(AS_CPU_PATH)//Encoder/Encoder.out.opt" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/Encoder/Encoder.c.o: \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/Encoder.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/ASMcDcs/ASMcDcs.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/ASMcDcs/ASMcDcs.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsIOTime/AsIOTime.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/standard/standard.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsBrStr/AsBrStr.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_PROJECT_PATH)/Logical/Global/Global.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/System/G_System.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/TriggerSensor/G_TriggerSensor.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/Encoder.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/TriggerSensor/G_TriggerSensor.typ \
	$(AS_PROJECT_PATH)/Logical/Global/Global.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/System/G_System.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.typ \
	$(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)/$(AS_PLC)/Cpu.per \
	$(AS_PROJECT_PATH)/Logical/Tasks/ParHandling/G_ParHandling.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/ParHandling/G_ParHandling.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/MainLogic/G_MainLogic.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/MainLogic/G_MainLogic.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/G_Encoder.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/EventHandling/G_EventHandling.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/G_Encoder.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/BondingDev/G_BondingDev.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/BondingDev/G_BondingDev.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/Encoder.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/EncoderFunc.h \
	$(AS_PROJECT_PATH)/Logical/Global/Global.h \
	$(AS_PROJECT_PATH)/Logical/Global/AnsiCFunc.h
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/Encoder.c" -o "$(AS_CPU_PATH)/Encoder/Encoder.c.o"  -T SG4  -M IA32  -B M4.10 -G V4.1.2  -s "Tasks.Encoder.Encoder" -t "$(AS_TEMP_PATH)" -I "$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder" "$(AS_TEMP_PATH)/Includes/Tasks/Encoder/Encoder"  "$(AS_TEMP_PATH)/Includes/Tasks/Encoder"  "$(AS_TEMP_PATH)/Includes/Tasks" "$(AS_TEMP_PATH)/Includes" "$(AS_PROJECT_PATH)/Logical/Global" -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include "$(AS_CPU_PATH)/Libraries.h"  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -x c -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/Encoder/EncoderFunc.c.o: \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/EncoderFunc.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/ASMcDcs/ASMcDcs.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/ASMcDcs/ASMcDcs.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsIOTime/AsIOTime.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/standard/standard.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsBrStr/AsBrStr.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_PROJECT_PATH)/Logical/Global/Global.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/System/G_System.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/TriggerSensor/G_TriggerSensor.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/Encoder.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/TriggerSensor/G_TriggerSensor.typ \
	$(AS_PROJECT_PATH)/Logical/Global/Global.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/System/G_System.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.typ \
	$(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)/$(AS_PLC)/Cpu.per \
	$(AS_PROJECT_PATH)/Logical/Tasks/ParHandling/G_ParHandling.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/ParHandling/G_ParHandling.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/MainLogic/G_MainLogic.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/MainLogic/G_MainLogic.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/G_Encoder.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/EventHandling/G_EventHandling.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/G_Encoder.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/BondingDev/G_BondingDev.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/BondingDev/G_BondingDev.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/Encoder.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/EncoderFunc.h \
	$(AS_PROJECT_PATH)/Logical/Global/Global.h \
	$(AS_PROJECT_PATH)/Logical/Global/AnsiCFunc.h
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/EncoderFunc.c" -o "$(AS_CPU_PATH)/Encoder/EncoderFunc.c.o"  -T SG4  -M IA32  -B M4.10 -G V4.1.2  -s "Tasks.Encoder.Encoder" -t "$(AS_TEMP_PATH)" -I "$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder" "$(AS_TEMP_PATH)/Includes/Tasks/Encoder/Encoder"  "$(AS_TEMP_PATH)/Includes/Tasks/Encoder"  "$(AS_TEMP_PATH)/Includes/Tasks" "$(AS_TEMP_PATH)/Includes" "$(AS_PROJECT_PATH)/Logical/Global" -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include "$(AS_CPU_PATH)/Libraries.h"  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -x c -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

$(AS_CPU_PATH)/Encoder/Global.c.o: \
	$(AS_PROJECT_PATH)/Logical/Global/Global.c \
	$(AS_PROJECT_PATH)/Logical/Libraries/ASMcDcs/ASMcDcs.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/ASMcDcs/ASMcDcs.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsIOTime/AsIOTime.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/standard/standard.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/AsBrStr/AsBrStr.fun \
	$(AS_PROJECT_PATH)/Logical/Libraries/asstring/asstring.fun \
	$(AS_PROJECT_PATH)/Logical/Global/Global.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/System/G_System.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/TriggerSensor/G_TriggerSensor.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/Encoder.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/BrbLib/BrbLib.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/TriggerSensor/G_TriggerSensor.typ \
	$(AS_PROJECT_PATH)/Logical/Global/Global.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/System/G_System.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/astime/astime.typ \
	$(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)/$(AS_PLC)/Cpu.per \
	$(AS_PROJECT_PATH)/Logical/Tasks/ParHandling/G_ParHandling.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/ParHandling/G_ParHandling.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/MainLogic/G_MainLogic.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/MainLogic/G_MainLogic.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/G_Encoder.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/EventHandling/G_EventHandling.var \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.typ \
	$(AS_PROJECT_PATH)/Logical/Libraries/AppLib/AppLib.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/G_Encoder.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/BondingDev/G_BondingDev.var \
	$(AS_PROJECT_PATH)/Logical/Tasks/BondingDev/G_BondingDev.typ \
	$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder/Encoder.typ \
	$(AS_PROJECT_PATH)/Logical/Global/Global.h \
	$(AS_PROJECT_PATH)/Logical/Global/AnsiCFunc.h
	@"$(AS_BIN_PATH)/BR.AS.CCompiler.exe" "$(AS_PROJECT_PATH)/Logical/Global/Global.c" -o "$(AS_CPU_PATH)/Encoder/Global.c.o"  -T SG4  -M IA32  -B M4.10 -G V4.1.2  -s "Tasks.Encoder.Encoder" -t "$(AS_TEMP_PATH)" -I "$(AS_PROJECT_PATH)/Logical/Tasks/Encoder/Encoder" "$(AS_TEMP_PATH)/Includes/Tasks/Encoder/Encoder"  "$(AS_TEMP_PATH)/Includes/Tasks/Encoder"  "$(AS_TEMP_PATH)/Includes/Tasks" "$(AS_TEMP_PATH)/Includes" "$(AS_PROJECT_PATH)/Logical/Global" -trigraphs -fno-asm -D _DEFAULT_INCLUDES -D _SG4 -fPIC -O0 -g -Wall -include "$(AS_CPU_PATH)/Libraries.h"  -extOptions -D HW_ARSIM -D SIM_STEPPER -D SIM_TEMP_ZONES -x c -P "$(AS_PROJECT_PATH)" -secret "$(AS_PROJECT_PATH)_br.as.ccompiler.exe"

-include $(AS_CPU_PATH)/Force.mak 

