######################################################
#                                                    #
# Automatic generated Makefile for Visual Components #
#                                                    #
#                  Do NOT edit!                      #
#                                                    #
######################################################

VCC:=@"$(AS_BIN_PATH)/br.vc.pc.exe"
LINK:=@"$(AS_BIN_PATH)/BR.VC.Link.exe"
MODGEN:=@"$(AS_BIN_PATH)/BR.VC.ModGen.exe"
VCPL:=@"$(AS_BIN_PATH)/BR.VC.PL.exe"
VCHWPP:=@"$(AS_BIN_PATH)/BR.VC.HWPP.exe"
VCDEP:=@"$(AS_BIN_PATH)/BR.VC.Depend.exe"
VCFLGEN:=@"$(AS_BIN_PATH)/BR.VC.lfgen.exe"
VCREFHANDLER:=@"$(AS_BIN_PATH)/BR.VC.CrossReferenceHandler.exe"
VCXREFEXTENDER:=@"$(AS_BIN_PATH)/BR.AS.CrossRefVCExtender.exe"
RM=CMD /C DEL
PALFILE_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Palette.vcr
VCCFLAGS_VisWvg=-server -proj VisWvg -vc "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/VCObject.vc" -prj_path "$(AS_PROJECT_PATH)" -temp_path "$(AS_TEMP_PATH)" -cfg $(AS_CONFIGURATION) -plc $(AS_PLC) -plctemp $(AS_TEMP_PLC) -cpu_path "$(AS_CPU_PATH)"
VCFIRMWARE=4.25.0
VCFIRMWAREPATH=$(AS_VC_PATH)/Firmware/V4.25.0/SG4
VCOBJECT_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/VCObject.vc
VCSTARTUP="vcstart.br"
VCLOD="vclod.br"
VCSTPOST="vcstpost.br"
TARGET_FILE_VisWvg=$(AS_CPU_PATH)/VisWvg.br
OBJ_SCOPE_VisWvg=Tasks/Visu
PRJ_PATH_VisWvg=$(AS_PROJECT_PATH)
SRC_PATH_VisWvg=$(AS_PROJECT_PATH)/Logical/$(OBJ_SCOPE_VisWvg)/VisWvga
TEMP_PATH_VisWvg=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VisWvg
TEMP_PATH_Shared=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared
TEMP_PATH_ROOT_VisWvg=$(AS_TEMP_PATH)
VC_LIBRARY_LIST_VisWvg=$(TEMP_PATH_VisWvg)/libraries.vci
VC_XREF_BUILDFILE_VisWvg=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.build
VC_XREF_CLEANFILE=$(AS_TEMP_PATH)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcxref.clean
VC_LANGUAGES_VisWvg=$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr
CPUHWC="$(TEMP_PATH_VisWvg)/cpuhwc.vci"
VC_STATIC_OPTIONS_VisWvg="$(TEMP_PATH_VisWvg)/vcStaticOptions.xml"
VC_STATIC_OPTIONS_Shared="$(TEMP_PATH_Shared)/vcStaticOptions.xml"
# include Shared and Font Makefile (only once)
	include $(AS_TEMP_PATH)/objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCFntDat/Font_VisWvg.mak
ifneq ($(VCINC),1)
	VCINC=1
	include $(AS_TEMP_PATH)/objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/VCShared.mak
endif

DEPENDENCIES_VisWvg=-d vcgclass -profile "False"
DEFAULT_STYLE_SHEET_VisWvg=Source[local].StyleSheet[Color]
SHARED_MODULE=$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/vcshared.br
LFNTFLAGS_VisWvg=-P "$(AS_PROJECT_PATH)" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)"
BDRFLAGS_VisWvg=-P "$(AS_PROJECT_PATH)" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)"

# Local Libs
LIB_LOCAL_OBJ_VisWvg=$(TEMP_PATH_VisWvg)/localobj.vca

# Hardware sources
PANEL_HW_OBJECT_VisWvg=$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VisWvg/dis.Hardware.vco
PANEL_HW_VCI_VisWvg=$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VisWvg/dis.Hardware.vci
PANEL_HW_SOURCE_VisWvg=C:/Users/Resmi/Desktop/ColdjetGit/Physical/Simulation/Hardware.hw 
DIS_OBJECTS_VisWvg=$(PANEL_HW_OBJECT_VisWvg) $(KEYMAP_OBJECTS_VisWvg)

# KeyMapping flags
$(TEMP_PATH_VisWvg)/dis.PS2-Keyboard.vco: $(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)/$(AS_PLC)/VC/PS2-Keyboard.dis
	$(VCHWPP) -f "$(PANEL_HW_SOURCE_VisWvg)" -o "$(subst .vco,.vci,$(TEMP_PATH_VisWvg)/dis.PS2-Keyboard.vco)" -n VisWvga -d VisWvg -pal "$(PALFILE_VisWvg)" -c "$(AS_CONFIGURATION)" -p "$(AS_PLC)" -ptemp "$(AS_TEMP_PLC)" -B "M4.10" -L "AsRfbExt: V*, visapi: V*" -hw "$(CPUHWC)" -warninglevel 2 -so $(VC_STATIC_OPTIONS_VisWvg) -sos $(VC_STATIC_OPTIONS_Shared) -keyboard "$(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)/$(AS_PLC)/VC/PS2-Keyboard.dis" -fp "$(AS_VC_PATH)/Firmware/V4.25.0/SG4" -prj "$(AS_PROJECT_PATH)" -apj "Coldjetorg" -sfas -vcob "$(VCOBJECT_VisWvg)"
	$(VCC) -f "$(subst .vco,.vci,$@)" -o "$@" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -k "$(VCVK_SOURCES_VisWvg)" $(VCCFLAGS_VisWvg) -p VisWvg -sfas

KEYMAP_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Physical/$(AS_CONFIGURATION)/$(AS_PLC)/VC/PS2-Keyboard.dis 
KEYMAP_OBJECTS_VisWvg=$(TEMP_PATH_VisWvg)/dis.PS2-Keyboard.vco 

# All Source Objects
TXTGRP_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Footer.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Menu.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Events.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Configuration.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_User.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Password.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Main.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Contact.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Service.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Production.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Head.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Cleaning.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Camera.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_PressureControl.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_MotorizedAdjustment.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Units.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Screensaver.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_PageTitle.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Overview.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Dialogs.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Header.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_ModuleConfig.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_SystemCleaning.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_TempCtrl.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_HeadPar.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Loadcell.txtgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_MedianVerbrauch.txtgrp 

FNINFO_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_Label.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_Touchpad.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_Header.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_FooterLabel.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_InOutput.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_Alarm.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_Html.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_Trend.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_Tab.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_PageTitle.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_HeadBig.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_HeadSmall.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_StartButton.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_Screensaver.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_Menu.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_HeadnumberTitle.fninfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Fonts/F_DialogText.fninfo 

BMINFO_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA3.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadVGA_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA2_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA3_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA1_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadVGA.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/NumPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/NumPad.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AcknowledgeReset.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmActive.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmBypassOFF.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmBypassON.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmInactive.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmLatched.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmNotQuit.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmQuit.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Reset.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ResetAcknowledge.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Triggered.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadHor.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadHor_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadVer.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadVer_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/backward_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/backward_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/forward_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/forward_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/control_button_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/control_button_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/right_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/right_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/left_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/left_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_up_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_up_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_down_active.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_down_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ProgressBorder.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_active_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_pressed_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_active_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_gradient_upside.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_gradient_downside.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/frame_header.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_pressed_control.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/EditPadVGA.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/EditPadVGA_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneNumPad_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadVer_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPad_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA1_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA1_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA2_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA3_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditpadQVGA2_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditPadQVGA3_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditPadVga_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditPadVga_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadHor_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadHor_released.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadVer_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneNumPad_pressed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox00_DisabledOff.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox01_DisabledOn.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox02_EnabledOff.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox03_EnabledOn.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox00_DisabledOff.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox01_DisabledOn.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox02_EnabledOff.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox03_EnabledOn.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar00_TopDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar01_TopEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar02_PageUpDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar03_PageUpEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar04_LineUpDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar05_LineUpEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar06_LineDownDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar07_LineDownEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar08_PageDownDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar09_PageDownEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar10_BottomDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar11_BottomEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar12_LeftDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar13_LeftEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar14_PageLeftDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar15_PageLeftEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar16_LineLeftDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar17_LineLeftEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar18_LineRightDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar19_LineRightEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar20_PageRightDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar21_PageRightEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar22_RightDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar23_RightEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Tab.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlagGermany.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlagUK.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedBlack1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedBlack1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGrey2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGrey2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Valve.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningGreen.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Camera.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallGrey.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallBlue.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallGreen.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigGrey.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigBlue.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigGreen.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Glue.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Soft.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/RoundGrey2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Cleaning.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/StitchLenTotal.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/PressureControl.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/MotorizedAdjustment.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGreyDouble.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGreyDouble.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ProductLen.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/StitchIndexMax.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGrey3.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGrey3.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGrey1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGrey1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallRed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigRed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ModuleEmpty.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRunningGreen.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRunningYellow.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRunningRed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Overview.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallGrey.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallRed.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallBlue.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallGreen.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TempCtrl.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Einstellungen74.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Hilfe74.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home74.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_40px.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_48px.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_40px.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_48px.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Einstellungen74_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Hilfe74_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home74_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_40px_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_48px_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_40px_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_48px_0.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Einstellungen74_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Hilfe74_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home74_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_40px_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_48px_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_40px_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_48px_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/LT1300B.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Bitmap_1.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ASP251.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ASp251Valve.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Bitmap_2.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Modconfig.bminfo \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/alarmd.bminfo 

BMGRP_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/AlphaPadQVGA.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/NumPad.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/AlphaPad.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/AlarmEvent.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/AlarmState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BypassState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/AcknowledgeState.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/NavigationPad.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/Borders.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Controls.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Logos.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Drops.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Header.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Borders.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Languages.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Includes.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_DropRunning.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Toolbar.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Heads.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Arrows.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Miscellaneous.bmgrp \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Tanks.bmgrp 

PAGE_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P01000_Main.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P00000_Template.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P00010_ScreenSaver.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02000_Production.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02101_Configuration1.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02201_Service1.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02300_User.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02400_Events.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02500_Contact.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03100_Cleaning.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03200_PressureControl.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03300_MotorizedAdjustment0.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03400_Camera.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02102_Configuration2.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P01100_Head.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02010_ProductionSave.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02020_ProductionDelete.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02105_Configuration5.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02108_Configuration8.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02103_Configuration3.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P01101_HeadCopy.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02106_Configuration6.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02109_Configuration9.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02110_Configuration10.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02112_Configuration12.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02202_Service2.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03000_Overview.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02107_Configuration7.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P10000_DialogMoveAxes.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02111_Configuration11.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03301_MotorizedAdjustment1.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02203_Service3.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02116_Configuration16.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02117_Configuration17.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02310_ModuleConfig.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03101_SystemCleaning.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02104_Configuration4.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03500_TempCtrl.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02113_Configuration13.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02114_Configuration14.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02150_ConfigParTempCtrlZone.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P10010_DialogStartHeating.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P01200_HeadPar.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02115_Configuration15.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P04000_AnalogFuellStand.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P10011_MedienVerbrauch.page \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P04100_AnalogFuellStand1.page 

LAYER_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_Header.layer \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_Footer.layer \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_Menu.layer \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_Toolbar.layer \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_HeaderWithoutMenu.layer \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_LoadCellSidebar.layer \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_MsgPopup.layer 

VCS_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/StyleSheets/Color.vcs 

BDR_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_Raised.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_Sunken.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatBlack.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatGrey.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_RoundGrey2.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatPressedGrey2.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_Tab.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatPressedBlack1.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatUnpressedBlack1.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatUnpressedGrey2.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatPressedGreyDouble.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatUnpressedGreyDouble.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatPressedGrey3.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatUnpressedGrey3.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatPressedGrey1.bdr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatUnpressedGrey1.bdr 

TPR_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/NumPad.tpr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/AlphaPadQVGA.tpr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/AlphaPad.tpr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/NavigationPad_ver.tpr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/NavigationPad_hor.tpr \
	$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/EditPad.tpr 

TDC_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Trends/TrendData.tdc 

VCVK_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/VirtualKeys.vcvk 

VCR_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Palette.vcr 

# Runtime Object sources
VCR_OBJECT_VisWvg=$(TEMP_PATH_VisWvg)/vcrt.vco
VCR_SOURCE_VisWvg=$(SRC_PATH_VisWvg)/package.vcp
# All Source Objects END

#Panel Hardware
$(PANEL_HW_VCI_VisWvg): $(PANEL_HW_SOURCE_VisWvg)
	$(VCHWPP) -f "$<" -o "$@" -n VisWvga -d VisWvg -pal "$(PALFILE_VisWvg)" -c "$(AS_CONFIGURATION)" -p "$(AS_PLC)" -ptemp "$(AS_TEMP_PLC)" -B "M4.10" -L "AsRfbExt: V*, visapi: V*" -verbose "False" -profile "False" -hw "$(CPUHWC)" -warninglevel 2 -so $(VC_STATIC_OPTIONS_VisWvg) -sos $(VC_STATIC_OPTIONS_Shared) -fp "$(AS_VC_PATH)/Firmware/V4.25.0/SG4" -sfas -prj "$(AS_PROJECT_PATH)" -apj "Coldjetorg" -vcob "$(VCOBJECT_VisWvg)"

$(PANEL_HW_OBJECT_VisWvg): $(PANEL_HW_VCI_VisWvg) $(PALFILE_VisWvg) $(VC_LIBRARY_LIST_VisWvg) $(KEYMAP_SOURCES_VisWvg)
	$(VCC) -f "$(subst .vco,.vci,$@)" -o "$@" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -k "$(VCVK_SOURCES_VisWvg)" $(VCCFLAGS_VisWvg) -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


# Pages
PAGE_OBJECTS_VisWvg = $(addprefix $(TEMP_PATH_VisWvg)/page., $(notdir $(PAGE_SOURCES_VisWvg:.page=.vco)))

$(TEMP_PATH_VisWvg)/page.P01000_Main.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P01000_Main.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P00000_Template.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P00000_Template.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P00010_ScreenSaver.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P00010_ScreenSaver.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02000_Production.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02000_Production.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02101_Configuration1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02101_Configuration1.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02201_Service1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02201_Service1.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02300_User.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02300_User.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02400_Events.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02400_Events.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02500_Contact.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02500_Contact.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P03100_Cleaning.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03100_Cleaning.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P03200_PressureControl.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03200_PressureControl.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P03300_MotorizedAdjustment0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03300_MotorizedAdjustment0.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P03400_Camera.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03400_Camera.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02102_Configuration2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02102_Configuration2.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P01100_Head.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P01100_Head.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02010_ProductionSave.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02010_ProductionSave.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02020_ProductionDelete.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02020_ProductionDelete.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02105_Configuration5.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02105_Configuration5.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02108_Configuration8.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02108_Configuration8.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02103_Configuration3.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02103_Configuration3.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P01101_HeadCopy.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P01101_HeadCopy.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02106_Configuration6.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02106_Configuration6.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02109_Configuration9.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02109_Configuration9.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02110_Configuration10.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02110_Configuration10.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02112_Configuration12.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02112_Configuration12.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02202_Service2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02202_Service2.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P03000_Overview.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03000_Overview.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02107_Configuration7.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02107_Configuration7.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P10000_DialogMoveAxes.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P10000_DialogMoveAxes.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02111_Configuration11.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02111_Configuration11.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P03301_MotorizedAdjustment1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03301_MotorizedAdjustment1.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02203_Service3.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02203_Service3.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02116_Configuration16.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02116_Configuration16.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02117_Configuration17.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02117_Configuration17.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02310_ModuleConfig.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02310_ModuleConfig.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P03101_SystemCleaning.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03101_SystemCleaning.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02104_Configuration4.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02104_Configuration4.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P03500_TempCtrl.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P03500_TempCtrl.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02113_Configuration13.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02113_Configuration13.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02114_Configuration14.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02114_Configuration14.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02150_ConfigParTempCtrlZone.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02150_ConfigParTempCtrlZone.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P10010_DialogStartHeating.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P10010_DialogStartHeating.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P01200_HeadPar.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P01200_HeadPar.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P02115_Configuration15.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P02115_Configuration15.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P04000_AnalogFuellStand.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P04000_AnalogFuellStand.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P10011_MedienVerbrauch.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P10011_MedienVerbrauch.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/page.P04100_AnalogFuellStand1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Pages/P04100_AnalogFuellStand1.page $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds "$(SRC_PATH_VisWvg)/StyleSheets/Color.vcs" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#Pages END




# Stylesheets
VCS_OBJECTS_VisWvg = $(addprefix $(TEMP_PATH_VisWvg)/vcs., $(notdir $(VCS_SOURCES_VisWvg:.vcs=.vco)))

$(TEMP_PATH_VisWvg)/vcs.Color.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/StyleSheets/Color.vcs
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -P "$(AS_PROJECT_PATH)" -ds $(DEFAULT_STYLE_SHEET_VisWvg) -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#Stylesheets END




# Layers
LAYER_OBJECTS_VisWvg = $(addprefix $(TEMP_PATH_VisWvg)/layer., $(notdir $(LAYER_SOURCES_VisWvg:.layer=.vco)))

$(TEMP_PATH_VisWvg)/layer.CL_Header.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_Header.layer $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -ds $(DEFAULT_STYLE_SHEET_VisWvg) -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/layer.CL_Footer.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_Footer.layer $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -ds $(DEFAULT_STYLE_SHEET_VisWvg) -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/layer.CL_Menu.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_Menu.layer $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -ds $(DEFAULT_STYLE_SHEET_VisWvg) -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/layer.CL_Toolbar.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_Toolbar.layer $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -ds $(DEFAULT_STYLE_SHEET_VisWvg) -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/layer.CL_HeaderWithoutMenu.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_HeaderWithoutMenu.layer $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -ds $(DEFAULT_STYLE_SHEET_VisWvg) -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/layer.CL_LoadCellSidebar.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_LoadCellSidebar.layer $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -ds $(DEFAULT_STYLE_SHEET_VisWvg) -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/layer.CL_MsgPopup.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Layers/CL_MsgPopup.layer $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -ds $(DEFAULT_STYLE_SHEET_VisWvg) -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#Layers END




# Virtual Keys
VCVK_OBJECTS_VisWvg = $(addprefix $(TEMP_PATH_VisWvg)/vcvk., $(notdir $(VCVK_SOURCES_VisWvg:.vcvk=.vco)))

$(TEMP_PATH_VisWvg)/vcvk.VirtualKeys.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/VirtualKeys.vcvk
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas

$(VCVK_OBJECTS_VisWvg): $(VC_LANGUAGES_VisWvg)

#Virtual Keys END




# Touch Pads
TPR_OBJECTS_VisWvg = $(addprefix $(TEMP_PATH_VisWvg)/tpr., $(notdir $(TPR_SOURCES_VisWvg:.tpr=.vco)))

$(TEMP_PATH_VisWvg)/tpr.NumPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/NumPad.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/Visu/VisWvga" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/tpr.AlphaPadQVGA.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/AlphaPadQVGA.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/Visu/VisWvga" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/tpr.AlphaPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/AlphaPad.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/Visu/VisWvga" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/tpr.NavigationPad_ver.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/NavigationPad_ver.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/Visu/VisWvga" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/tpr.NavigationPad_hor.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/NavigationPad_hor.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/Visu/VisWvga" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/tpr.EditPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TouchPads/EditPad.tpr
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg) -prj "C:/Users/Resmi/Desktop/ColdjetGit/Logical/Tasks/Visu/VisWvga" -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#Touch Pads END




# Text Groups
TXTGRP_OBJECTS_VisWvg = $(addprefix $(TEMP_PATH_VisWvg)/txtgrp., $(notdir $(TXTGRP_SOURCES_VisWvg:.txtgrp=.vco)))

$(TEMP_PATH_VisWvg)/txtgrp.TG_Footer.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Footer.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Menu.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Menu.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Events.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Events.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Configuration.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Configuration.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_User.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_User.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Password.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Password.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Main.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Main.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Contact.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Contact.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Service.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Service.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Production.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Production.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Head.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Head.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Cleaning.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Cleaning.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Camera.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Camera.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_PressureControl.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_PressureControl.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_MotorizedAdjustment.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_MotorizedAdjustment.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Units.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Units.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Screensaver.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Screensaver.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_PageTitle.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_PageTitle.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Overview.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Overview.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Dialogs.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Dialogs.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Header.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Header.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_ModuleConfig.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_ModuleConfig.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_SystemCleaning.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_SystemCleaning.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_TempCtrl.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_TempCtrl.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_HeadPar.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_HeadPar.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_Loadcell.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_Loadcell.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/txtgrp.TG_MedianVerbrauch.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/TextGroups/TG_MedianVerbrauch.txtgrp $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#Text Groups END




# BitmapGroups
BMGRP_OBJECTS_VisWvg = $(addprefix $(TEMP_PATH_VisWvg)/bmgrp., $(notdir $(BMGRP_SOURCES_VisWvg:.bmgrp=.vco)))

$(TEMP_PATH_VisWvg)/bmgrp.AlphaPadQVGA.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/AlphaPadQVGA.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.NumPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/NumPad.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.AlphaPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/AlphaPad.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.AlarmEvent.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/AlarmEvent.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.AlarmState.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/AlarmState.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BypassState.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BypassState.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.AcknowledgeState.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/AcknowledgeState.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.NavigationPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/NavigationPad.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.Borders.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/Borders.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Controls.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Controls.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Logos.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Logos.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Drops.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Drops.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Header.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Header.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Borders.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Borders.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Languages.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Languages.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Includes.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Includes.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_DropRunning.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_DropRunning.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Toolbar.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Toolbar.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Heads.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Heads.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Arrows.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Arrows.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Miscellaneous.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Miscellaneous.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bmgrp.BG_Tanks.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/BitmapGroups/BG_Tanks.bmgrp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#BitmapGroups END




# Bitmaps
BMINFO_OBJECTS_VisWvg = $(addprefix $(TEMP_PATH_VisWvg)/bminfo., $(notdir $(BMINFO_SOURCES_VisWvg:.bminfo=.vco)))

$(TEMP_PATH_VisWvg)/bminfo.AlphaPadQVGA1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA1.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlphaPadQVGA2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA2.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlphaPadQVGA3.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA3.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA3.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlphaPadVGA_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadVGA_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadVGA_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlphaPadQVGA2_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA2_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA2_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlphaPadQVGA3_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA3_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA3_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlphaPadQVGA1_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA1_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadQVGA1_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlphaPadVGA.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadVGA.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlphaPadVGA.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.NumPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/NumPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/NumPad_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.NumPad.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/NumPad.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/NumPad.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AcknowledgeReset.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AcknowledgeReset.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AcknowledgeReset.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlarmActive.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmActive.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmActive.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlarmBypassOFF.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmBypassOFF.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmBypassOFF.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlarmBypassON.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmBypassON.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmBypassON.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlarmInactive.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmInactive.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmInactive.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlarmLatched.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmLatched.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmLatched.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlarmNotQuit.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmNotQuit.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmNotQuit.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.AlarmQuit.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmQuit.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/AlarmQuit.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Reset.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Reset.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Reset.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ResetAcknowledge.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ResetAcknowledge.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ResetAcknowledge.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Triggered.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Triggered.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Triggered.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ListPadHor.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadHor.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadHor.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ListPadHor_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadHor_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadHor_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ListPadVer.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadVer.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadVer.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ListPadVer_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadVer_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ListPadVer_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.backward_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/backward_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/backward_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.backward_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/backward_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/backward_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.global_area_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.global_area_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.forward_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/forward_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/forward_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.forward_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/forward_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/forward_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.control_button_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/control_button_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/control_button_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.control_button_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/control_button_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/control_button_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.right_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/right_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/right_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.right_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/right_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/right_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.left_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/left_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/left_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.left_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/left_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/left_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.up_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.up_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.down_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.down_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.multi_up_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_up_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_up_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.multi_up_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_up_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_up_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.multi_down_active.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_down_active.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_down_active.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.multi_down_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_down_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/multi_down_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ProgressBorder.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ProgressBorder.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ProgressBorder.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.down_active_control.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_active_control.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_active_control.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.down_pressed_control.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_pressed_control.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/down_pressed_control.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.up_active_control.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_active_control.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_active_control.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.global_area_gradient_upside.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_gradient_upside.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_gradient_upside.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.global_area_gradient_downside.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_gradient_downside.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/global_area_gradient_downside.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.frame_header.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/frame_header.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/frame_header.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.up_pressed_control.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_pressed_control.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/up_pressed_control.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.EditPadVGA.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/EditPadVGA.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/EditPadVGA.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.EditPadVGA_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/EditPadVGA_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/EditPadVGA_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneNumPad_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneNumPad_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneNumPad_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneListPadVer_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadVer_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadVer_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneAlphaPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPad_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneAlphaPad_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPad_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPad_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneAlphaPadQVGA1_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA1_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA1_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneAlphaPadQVGA1_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA1_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA1_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneAlphaPadQVGA2_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA2_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA2_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneAlphaPadQVGA3_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA3_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneAlphaPadQVGA3_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneEditpadQVGA2_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditpadQVGA2_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditpadQVGA2_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneEditPadQVGA3_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditPadQVGA3_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditPadQVGA3_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneEditPadVga_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditPadVga_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditPadVga_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneEditPadVga_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditPadVga_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneEditPadVga_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneListPadHor_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadHor_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadHor_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneListPadHor_released.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadHor_released.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadHor_released.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneListPadVer_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadVer_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneListPadVer_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.zuneNumPad_pressed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneNumPad_pressed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/zuneNumPad_pressed.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Checkbox00_DisabledOff.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox00_DisabledOff.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox00_DisabledOff.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Checkbox01_DisabledOn.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox01_DisabledOn.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox01_DisabledOn.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Checkbox02_EnabledOff.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox02_EnabledOff.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox02_EnabledOff.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Checkbox03_EnabledOn.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox03_EnabledOn.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Checkbox03_EnabledOn.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Optionbox00_DisabledOff.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox00_DisabledOff.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox00_DisabledOff.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Optionbox01_DisabledOn.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox01_DisabledOn.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox01_DisabledOn.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Optionbox02_EnabledOff.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox02_EnabledOff.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox02_EnabledOff.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Optionbox03_EnabledOn.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox03_EnabledOn.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Optionbox03_EnabledOn.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar00_TopDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar00_TopDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar00_TopDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar01_TopEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar01_TopEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar01_TopEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar02_PageUpDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar02_PageUpDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar02_PageUpDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar03_PageUpEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar03_PageUpEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar03_PageUpEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar04_LineUpDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar04_LineUpDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar04_LineUpDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar05_LineUpEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar05_LineUpEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar05_LineUpEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar06_LineDownDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar06_LineDownDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar06_LineDownDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar07_LineDownEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar07_LineDownEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar07_LineDownEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar08_PageDownDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar08_PageDownDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar08_PageDownDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar09_PageDownEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar09_PageDownEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar09_PageDownEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar10_BottomDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar10_BottomDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar10_BottomDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar11_BottomEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar11_BottomEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar11_BottomEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar12_LeftDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar12_LeftDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar12_LeftDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar13_LeftEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar13_LeftEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar13_LeftEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar14_PageLeftDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar14_PageLeftDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar14_PageLeftDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar15_PageLeftEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar15_PageLeftEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar15_PageLeftEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar16_LineLeftDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar16_LineLeftDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar16_LineLeftDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar17_LineLeftEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar17_LineLeftEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar17_LineLeftEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar18_LineRightDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar18_LineRightDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar18_LineRightDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar19_LineRightEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar19_LineRightEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar19_LineRightEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar20_PageRightDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar20_PageRightDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar20_PageRightDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar21_PageRightEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar21_PageRightEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar21_PageRightEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar22_RightDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar22_RightDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar22_RightDisabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Scrollbar23_RightEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar23_RightEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Scrollbar23_RightEnabled.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeaderLogo.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBorder.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Settings.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Help.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Home.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Tab.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Tab.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Tab.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlagGermany.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlagGermany.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlagGermany.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlagUK.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlagUK.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlagUK.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlatUnpressedBlack1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedBlack1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedBlack1.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlatPressedBlack1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedBlack1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedBlack1.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlatUnpressedGrey2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGrey2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGrey2.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlatPressedGrey2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGrey2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGrey2.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Valve.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Valve.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Valve.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRunningGreen.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningGreen.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningGreen.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Camera.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Camera.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Camera.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeadSmallGrey.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallGrey.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallGrey.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeadSmallBlue.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallBlue.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallBlue.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeadSmallGreen.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallGreen.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallGreen.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeadBigGrey.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigGrey.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigGrey.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeadBigBlue.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigBlue.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigBlue.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeadBigGreen.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigGreen.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigGreen.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowLeftDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowLeftEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowRightDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowRightEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Glue.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Glue.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Glue.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Soft.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Soft.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Soft.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.RoundGrey2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/RoundGrey2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/RoundGrey2.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Cleaning.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Cleaning.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Cleaning.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.StitchLenTotal.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/StitchLenTotal.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/StitchLenTotal.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.PressureControl.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/PressureControl.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/PressureControl.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.MotorizedAdjustment.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/MotorizedAdjustment.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/MotorizedAdjustment.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlatUnpressedGreyDouble.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGreyDouble.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGreyDouble.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlatPressedGreyDouble.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGreyDouble.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGreyDouble.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ProductLen.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ProductLen.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ProductLen.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.StitchIndexMax.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/StitchIndexMax.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/StitchIndexMax.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRunningYellow.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRunningRed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlatUnpressedGrey3.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGrey3.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGrey3.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlatPressedGrey3.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGrey3.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGrey3.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlatUnpressedGrey1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGrey1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatUnpressedGrey1.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.FlatPressedGrey1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGrey1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/FlatPressedGrey1.bmp
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeadSmallRed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallRed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadSmallRed.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeadBigRed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigRed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeadBigRed.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ModuleEmpty.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ModuleEmpty.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ModuleEmpty.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBlue.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropGreen.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropYellow.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigBorder.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigBlue.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigGreen.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigYellow.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigRed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigRunningGreen.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRunningGreen.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRunningGreen.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigRunningYellow.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRunningYellow.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRunningYellow.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigRunningRed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRunningRed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRunningRed.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ScreensaverLogo.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowModuleLeftEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowModuleRightEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Overview.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Overview.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Overview.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowUpDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowUpEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowDownDisabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowDownEnabled.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.TankSmallGrey.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallGrey.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallGrey.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.TankSmallRed.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallRed.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallRed.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.TankSmallBlue.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallBlue.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallBlue.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.TankSmallGreen.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallGreen.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TankSmallGreen.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.TempCtrl.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TempCtrl.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/TempCtrl.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowDownDisabled_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowDownEnabled_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowLeftDisabled_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowLeftEnabled_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowModuleLeftEnabled_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowModuleRightEnabled_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowRightDisabled_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowRightEnabled_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowUpDisabled_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowUpEnabled_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigBlue_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigBorder_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigGreen_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigRed_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigYellow_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBlue_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBorder_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropGreen_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRed_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRunningRed_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRunningYellow_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropYellow_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Einstellungen74.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Einstellungen74.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Einstellungen74.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeaderLogo_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Help_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Hilfe74.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Hilfe74.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Hilfe74.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Home74.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home74.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home74.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ScreensaverLogo_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Settings_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_li_40px.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_40px.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_40px.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_li_48px.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_48px.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_48px.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_re_40px.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_40px.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_40px.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_re_48px.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_48px.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_48px.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowDownDisabled_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowDownEnabled_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowLeftDisabled_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowLeftEnabled_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowModuleLeftEnabled_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowModuleRightEnabled_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowRightDisabled_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowRightEnabled_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowUpDisabled_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowUpEnabled_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigBlue_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigBorder_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigGreen_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigRed_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigYellow_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBlue_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBorder_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropGreen_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRed_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRunningRed_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRunningYellow_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropYellow_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Einstellungen74_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Einstellungen74_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Einstellungen74_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeaderLogo_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Help_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Hilfe74_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Hilfe74_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Hilfe74_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Home74_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home74_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home74_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ScreensaverLogo_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Settings_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_li_40px_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_40px_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_40px_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_li_48px_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_48px_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_48px_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_re_40px_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_40px_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_40px_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_re_48px_0.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_48px_0.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_48px_0.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowDownDisabled_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownDisabled_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowDownEnabled_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowDownEnabled_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowLeftDisabled_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftDisabled_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowLeftEnabled_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowLeftEnabled_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowModuleLeftEnabled_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleLeftEnabled_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowModuleRightEnabled_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowModuleRightEnabled_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowRightDisabled_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightDisabled_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowRightEnabled_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowRightEnabled_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowUpDisabled_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpDisabled_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ArrowUpEnabled_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ArrowUpEnabled_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigBlue_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBlue_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigBorder_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigBorder_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigGreen_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigGreen_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigRed_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigRed_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBigYellow_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBigYellow_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBlue_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBlue_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropBorder_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropBorder_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropGreen_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropGreen_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRed_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRed_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRunningRed_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningRed_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropRunningYellow_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropRunningYellow_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.DropYellow_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/DropYellow_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Einstellungen74_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Einstellungen74_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Einstellungen74_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.HeaderLogo_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/HeaderLogo_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Help_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Help_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Hilfe74_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Hilfe74_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Hilfe74_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Home74_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home74_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Home74_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ScreensaverLogo_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ScreensaverLogo_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Settings_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Settings_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_li_40px_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_40px_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_40px_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_li_48px_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_48px_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_li_48px_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_re_40px_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_40px_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_40px_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.pfeil_re_48px_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_48px_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/pfeil_re_48px_1.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.LT1300B.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/LT1300B.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/LT1300B.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Bitmap_1.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Bitmap_1.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Bitmap_1.jpg
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ASP251.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ASP251.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ASP251.jpg
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.ASp251Valve.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ASp251Valve.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/ASp251Valve.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Bitmap_2.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Bitmap_2.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Bitmap_2.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.Modconfig.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Modconfig.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/Modconfig.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


$(TEMP_PATH_VisWvg)/bminfo.alarmd.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/alarmd.bminfo $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Bitmaps/alarmd.png
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#Bitmaps END




# Trend Data Configuration
TDC_OBJECTS_VisWvg = $(addprefix $(TEMP_PATH_VisWvg)/tdc., $(notdir $(TDC_SOURCES_VisWvg:.tdc=.vco)))

$(TEMP_PATH_VisWvg)/tdc.TrendData.vco: $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Trends/TrendData.tdc
	 $(VCC) -f "$<" -o "$@" -l "$(AS_PROJECT_PATH)/Logical/VCShared/Languages.vcr" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -pal "$(PALFILE_VisWvg)" $(VCCFLAGS_VisWvg)  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas


#Trend Data Configuration END


#
# Borders
#
BDR_SOURCES_VisWvg=$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_Raised.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_Sunken.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatBlack.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatGrey.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_RoundGrey2.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatPressedGrey2.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_Tab.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatPressedBlack1.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatUnpressedBlack1.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatUnpressedGrey2.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatPressedGreyDouble.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatUnpressedGreyDouble.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatPressedGrey3.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatUnpressedGrey3.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatPressedGrey1.bdr $(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Borders/B_FlatUnpressedGrey1.bdr 
BDR_OBJECTS_VisWvg=$(TEMP_PATH_VisWvg)/bdr.Bordermanager.vco
$(TEMP_PATH_VisWvg)/bdr.Bordermanager.vco: $(BDR_SOURCES_VisWvg)
	$(VCC) -f "$<" -o "$@" -pkg "$(SRC_PATH_VisWvg)" $(BDRFLAGS_VisWvg) $(VCCFLAGS_VisWvg) -p VisWvg$(SRC_PATH_VisWvg)
#
# Logical fonts
#
$(TEMP_PATH_VisWvg)/lfnt.en.vco: $(TEMP_PATH_VisWvg)/en.lfnt $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" $(LFNTFLAGS_VisWvg) $(VCCFLAGS_VisWvg) -p VisWvg -sfas
$(TEMP_PATH_VisWvg)/lfnt.de.vco: $(TEMP_PATH_VisWvg)/de.lfnt $(VC_LANGUAGES_VisWvg)
	 $(VCC) -f "$<" -o "$@" $(LFNTFLAGS_VisWvg) $(VCCFLAGS_VisWvg) -p VisWvg -sfas
LFNT_OBJECTS_VisWvg=$(TEMP_PATH_VisWvg)/lfnt.en.vco $(TEMP_PATH_VisWvg)/lfnt.de.vco 

#Runtime Object
$(VCR_OBJECT_VisWvg) : $(VCR_SOURCE_VisWvg)
	$(VCC) -f "$<" -o "$@" -cv "$(AS_PROJECT_PATH)/Logical/VCShared/ControlVersion.cvinfo" -sl en $(VCCFLAGS_VisWvg) -rt  -p VisWvg -so $(VC_STATIC_OPTIONS_VisWvg) -vcr 4250 -sfas
# Local resources Library rules
LIB_LOCAL_RES_VisWvg=$(TEMP_PATH_VisWvg)/localres.vca
$(LIB_LOCAL_RES_VisWvg) : $(TEMP_PATH_VisWvg)/VisWvg02.ccf

# Bitmap Library rules
LIB_BMP_RES_VisWvg=$(TEMP_PATH_VisWvg)/bmpres.vca
$(LIB_BMP_RES_VisWvg) : $(TEMP_PATH_VisWvg)/VisWvg03.ccf
$(BMGRP_OBJECTS_VisWvg) : $(PALFILE_VisWvg) $(VC_LANGUAGES_VisWvg)
$(BMINFO_OBJECTS_VisWvg) : $(PALFILE_VisWvg)

BUILD_FILE_VisWvg=$(TEMP_PATH_VisWvg)/BuildFiles.arg
$(BUILD_FILE_VisWvg) : BUILD_FILE_CLEAN_VisWvg $(BUILD_SOURCES_VisWvg)
BUILD_FILE_CLEAN_VisWvg:
	$(RM) /F /Q "$(BUILD_FILE_VisWvg)" 2>nul
#All Modules depending to this project
PROJECT_MODULES_VisWvg=$(AS_CPU_PATH)/VisWvg01.br $(AS_CPU_PATH)/VisWvg02.br $(AS_CPU_PATH)/VisWvg03.br $(FONT_MODULES_VisWvg) $(SHARED_MODULE)

# General Build rules

$(TARGET_FILE_VisWvg): $(PROJECT_MODULES_VisWvg) $(TEMP_PATH_VisWvg)/VisWvg.prj
	$(MODGEN) -so $(VC_STATIC_OPTIONS_VisWvg) -fw "$(VCFIRMWAREPATH)" -m $(VCSTPOST) -v V1.00.0 -f "$(TEMP_PATH_VisWvg)/VisWvg.prj" -o "$@" -vc "$(VCOBJECT_VisWvg)" $(DEPENDENCIES_VisWvg) $(addprefix -d ,$(notdir $(PROJECT_MODULES_VisWvg:.br=)))

$(AS_CPU_PATH)/VisWvg01.br: $(TEMP_PATH_VisWvg)/VisWvg01.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_VisWvg) -fw "$(VCFIRMWAREPATH)" -m $(VCLOD) -v V1.00.0 -b -vc "$(VCOBJECT_VisWvg)" -f "$<" -o "$@" $(DEPENDENCIES_VisWvg)

$(AS_CPU_PATH)/VisWvg02.br: $(TEMP_PATH_VisWvg)/VisWvg02.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_VisWvg) -fw "$(VCFIRMWAREPATH)" -m $(VCLOD) -v V1.00.0 -b -vc "$(VCOBJECT_VisWvg)" -f "$<" -o "$@" $(DEPENDENCIES_VisWvg)

$(AS_CPU_PATH)/VisWvg03.br: $(TEMP_PATH_VisWvg)/VisWvg03.ccf
	$(MODGEN) -so $(VC_STATIC_OPTIONS_VisWvg) -fw "$(VCFIRMWAREPATH)" -m $(VCLOD) -v V1.00.0 -b -vc "$(VCOBJECT_VisWvg)" -f "$<" -o "$@" $(DEPENDENCIES_VisWvg)

# General Build rules END
$(LIB_LOCAL_OBJ_VisWvg) : $(TEMP_PATH_VisWvg)/VisWvg01.ccf

# Main Module
$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/VisWvg.vcm:
$(TEMP_PATH_VisWvg)/VisWvg.prj: $(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/VisWvg.vcm
	$(VCDEP) -m "$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/VisWvg.vcm" -s "$(AS_CPU_PATH)/VCShared/Shared.vcm" -p "$(AS_PATH)/AS/VC/Firmware" -c "$(AS_CPU_PATH)" -fw "$(VCFIRMWAREPATH)" -hw "$(CPUHWC)" -so $(VC_STATIC_OPTIONS_VisWvg) -o VisWvg -proj VisWvg
	$(VCPL) $(notdir $(PROJECT_MODULES_VisWvg:.br=,4)) VisWvg,2 -o "$@" -p VisWvg -vc "VisWvg" -verbose "False" -fl "$(TEMP_PATH_ROOT_VisWvg)/Objects/$(AS_CONFIGURATION)/$(AS_TEMP_PLC)/VCShared/VisWvg.vcm" -vcr "$(VCR_SOURCE_VisWvg)" -prj "$(AS_PROJECT_PATH)" -warningLevel2 -sfas

# 01 Module

DEL_TARGET01_LFL_VisWvg=$(TEMP_PATH_VisWvg)\VisWvg01.ccf.lfl
$(TEMP_PATH_VisWvg)/VisWvg01.ccf: $(LIB_SHARED) $(SHARED_CCF) $(LIB_BMP_RES_VisWvg) $(TEMP_PATH_VisWvg)/VisWvg03.ccf $(LIB_LOCAL_RES_VisWvg) $(TEMP_PATH_VisWvg)/VisWvg02.ccf $(DIS_OBJECTS_VisWvg) $(PAGE_OBJECTS_VisWvg) $(VCS_OBJECTS_VisWvg) $(VCVK_OBJECTS_VisWvg) $(VCRT_OBJECTS_VisWvg) $(TPR_OBJECTS_VisWvg) $(TXTGRP_OBJECTS_VisWvg) $(LAYER_OBJECTS_VisWvg) $(VCR_OBJECT_VisWvg) $(TDC_OBJECTS_VisWvg) $(TRD_OBJECTS_VisWvg) $(TRE_OBJECTS_VisWvg) $(PRC_OBJECTS_VisWvg) $(SCR_OBJECTS_VisWvg)
	-@CMD /Q /C if exist "$(DEL_TARGET01_LFL_VisWvg)" DEL /F /Q "$(DEL_TARGET01_LFL_VisWvg)" 2>nul
	@$(VCFLGEN) "$@.lfl" "$(LIB_SHARED)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(LIB_BMP_RES_VisWvg)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(LIB_LOCAL_RES_VisWvg)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(DIS_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .page -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Package.vcp" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(VCS_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .vcvk -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Package.vcp" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(VCRT_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(TPR_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .txtgrp -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Package.vcp" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .layer -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Package.vcp" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(VCR_OBJECT_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .tdc -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Package.vcp" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .trd -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Package.vcp" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(SCR_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	$(LINK) "$@.lfl" -o "$@" -p VisWvg -lib "$(LIB_LOCAL_OBJ_VisWvg)" -P "$(AS_PROJECT_PATH)" -m "local objects" -profile "False" -warningLevel2 -vcr 4250 -sfas
# 01 Module END

# 02 Module

DEL_TARGET02_LFL_VisWvg=$(TEMP_PATH_VisWvg)\VisWvg02.ccf.lfl
$(TEMP_PATH_VisWvg)/VisWvg02.ccf: $(LIB_SHARED) $(SHARED_CCF) $(LIB_BMP_RES_VisWvg) $(TEMP_PATH_VisWvg)/VisWvg03.ccf $(BDR_OBJECTS_VisWvg) $(LFNT_OBJECTS_VisWvg) $(CLM_OBJECTS_VisWvg)
	-@CMD /Q /C if exist "$(DEL_TARGET02_LFL_VisWvg)" DEL /F /Q "$(DEL_TARGET02_LFL_VisWvg)" 2>nul
	@$(VCFLGEN) "$@.lfl" "$(LIB_SHARED)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(LIB_BMP_RES_VisWvg)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(BDR_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(LFNT_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" "$(CLM_OBJECTS_VisWvg:.vco=.vco|)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	$(LINK) "$@.lfl" -o "$@" -p VisWvg -lib "$(LIB_LOCAL_RES_VisWvg)" -P "$(AS_PROJECT_PATH)" -m "local resources" -profile "False" -warningLevel2 -vcr 4250 -sfas
# 02 Module END

# 03 Module

DEL_TARGET03_LFL_VisWvg=$(TEMP_PATH_VisWvg)\VisWvg03.ccf.lfl
$(TEMP_PATH_VisWvg)/VisWvg03.ccf: $(LIB_SHARED) $(SHARED_CCF) $(BMGRP_OBJECTS_VisWvg) $(BMINFO_OBJECTS_VisWvg) $(PALFILE_VisWvg)
	-@CMD /Q /C if exist "$(DEL_TARGET03_LFL_VisWvg)" DEL /F /Q "$(DEL_TARGET03_LFL_VisWvg)" 2>nul
	@$(VCFLGEN) "$@.lfl" "$(LIB_SHARED)" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .bmgrp -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Package.vcp" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	@$(VCFLGEN) "$@.lfl" -mask .bminfo -vcp "$(AS_PROJECT_PATH)/Logical/Tasks/Visu/VisWvga/Package.vcp" -temp "$(TEMP_PATH_VisWvg)" -prj "$(PRJ_PATH_VisWvg)" -sfas
	$(LINK) "$@.lfl" -o "$@" -p VisWvg -lib "$(LIB_BMP_RES_VisWvg)" -P "$(AS_PROJECT_PATH)" -m "bitmap resources" -profile "False" -warningLevel2 -vcr 4250 -sfas
# 03 Module END

# Post Build Steps

.PHONY : vcPostBuild_VisWvg

vcPostBuild_VisWvg :
	$(VCC) -pb -vcm "$(TEMP_PATH_VisWvg)/MODULEFILES.vcm" -fw "$(VCFIRMWAREPATH)" $(VCCFLAGS_VisWvg) -p VisWvg -vcr 4250 -sfas

# Post Build Steps END
