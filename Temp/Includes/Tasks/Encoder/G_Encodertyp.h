/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_7_
#define _BUR_1500273701_7_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct EncoderCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bSimEncReset;
	plcbit bInitEncoder;
	plcbit bGetBonDevPar;
} EncoderCallerBox_TYP;

typedef struct EncoderDirectBox_TYP
{	plcbit bSimEncOn;
	plcbit bSimulatedEncoderStart;
	plcbit bSimulatedEncoderStop;
} EncoderDirectBox_TYP;

typedef struct EncFifoPos_TYP
{	struct PosToBond_TYP FifoPos[301];
} EncFifoPos_TYP;

typedef struct EncoderPar_TYP
{	struct EncFifoPos_TYP EncFifo[5];
	float rSimSpeed;
	float rSimulatedEncoderSpeed;
} EncoderPar_TYP;

typedef struct EncoderState_TYP
{	float rVelocity[5];
	signed long nAxisPos[5];
	signed long nSimAxisPos;
	plcbit bSimEnc;
	plcbit bEncReady;
	plcbit bSimulatedEncoderStarted;
} EncoderState_TYP;

typedef struct Encoder_TYP
{	struct EncoderCallerBox_TYP CallerBox;
	struct EncoderDirectBox_TYP DirectBox;
	struct EncoderPar_TYP Par;
	struct EncoderState_TYP State;
} Encoder_TYP;

typedef struct FifoTriggerSignal_TYP
{	signed long nTimeStamp;
	signed long nTimeDifference;
	signed char nCount;
} FifoTriggerSignal_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Encoder/G_Encoder.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Tasks/TriggerSensor/G_TriggerSensor.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_7_ */

