/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_41_
#define _BUR_1500273701_41_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_ENC_RESET,
	eSTEP_ENC_INIT,
	eSTEP_GET_BONDEV_PAR
} Steps_ENUM;

typedef struct EncoderStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} EncoderStep_TYP;

typedef struct IoBrokenWire_TYP
{	plcbit bBrokenWireA;
	plcbit bBrokenWireB;
	plcbit bBrokenWireR;
} IoBrokenWire_TYP;

typedef struct IoInput_TYP
{	signed long nEncoderIncrements[5];
	signed long nEncLatchInc[5];
	signed char nEncLatchCount[5];
	plcbit bEncLatchEn[5];
	struct IoBrokenWire_TYP BrokenWire[5];
} IoInput_TYP;

typedef struct IoOutput_TYP
{	plcbit bAckBrokenWire;
} IoOutput_TYP;

typedef struct IO_TYP
{	struct IoInput_TYP Input;
	struct IoOutput_TYP Output;
} IO_TYP;

typedef struct Encod_TYP
{	double rDiffScaled;
	double rAfterComma;
	float rAfterCommaStore;
	signed long nEncoderIncrementsOld;
	signed long nDiff;
	signed long nBeforeComma;
} Encod_TYP;

typedef struct SimEncod_TYP
{	struct Encod_TYP EncodPar;
	double rSimInc;
	double rSimEncoderIn;
	float rReset;
	unsigned long nIncPerRev;
	float rDistancePerRev;
	signed long nEncoderIncrements;
} SimEncod_TYP;

typedef struct EncLatch_TYP
{	signed long nDiffIncrements;
	signed long nOldIncrements;
	plcbit bEncEnable;
} EncLatch_TYP;

typedef struct EncConfig_TYP
{	signed long nOldIncrements;
	signed long nDiffIncrements;
	signed long nAdaptedInc;
	signed long nAdaptedLatchInc;
	plcbit bEncReady;
	plcbit bEncDir;
} EncConfig_TYP;

typedef struct CalcedVar_TYP
{	unsigned long nTimeDifferenceTotal;
	unsigned short nEntryCount;
	double rAverrageTimeDifference;
} CalcedVar_TYP;

typedef struct TriggerEncoderPar_TYP
{	signed char nSensorIdx;
	signed char nRisingCnt_old;
	float rDistance;
	float rEncoderSpeed;
	struct CalcedVar_TYP CalcedVar;
	struct TON TON_TriggerSignalTimeOut;
} TriggerEncoderPar_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Encoder/Encoder/Encoder.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_41_ */

