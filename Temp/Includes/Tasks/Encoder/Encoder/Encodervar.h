/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_42_
#define _BUR_1500273701_42_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nIDX_FIFO_ENCODER_TRIGGER_MAX 4U
#else
 _LOCAL_CONST unsigned char nIDX_FIFO_ENCODER_TRIGGER_MAX;
#endif


/* Variablen */
_BUR_LOCAL float rVelocityFactor;
_BUR_LOCAL signed short nTempTimeDifference;
_BUR_LOCAL unsigned short i;
_BUR_LOCAL unsigned short nEntryCount;
_BUR_LOCAL unsigned long nTimeDifferenceTotal;
_BUR_LOCAL double rAverrageTimeDifference;
_BUR_LOCAL struct TriggerEncoderPar_TYP TriggerEncoderPar[5];
_BUR_LOCAL struct FifoTriggerSignal_TYP CurrentTriggerSignal;
_BUR_LOCAL struct FifoTriggerSignal_TYP LastTriggerSignal;
_BUR_LOCAL struct FifoTriggerSignal_TYP TriggerSignal;
_BUR_LOCAL struct FifoTriggerSignal_TYP FifoTriggerSignal[5][5];
_BUR_LOCAL plcbit bEdgePos;
_BUR_LOCAL plcbit bMsgDone[5];
_BUR_LOCAL signed char nForIdx;
_BUR_LOCAL signed char nSourceEncoderIdx;
_BUR_LOCAL signed long nSourceEncoderDifference;
_BUR_LOCAL signed long nSourceEncoderAxisPos_old[5];
_BUR_LOCAL signed char nEncoderIndex_old;
_BUR_LOCAL signed char nEncIndex;
_BUR_LOCAL signed char nOldLatchCount[5];
_BUR_LOCAL float rEncInc[5];
_BUR_LOCAL struct TON_10ms fbTimer;
_BUR_LOCAL struct R_TRIG fbEdgePosAuto;
_BUR_LOCAL struct ASMcDcsPrecisionDigitalCamSwitch fbPrecDCSVelocity[5];
_BUR_LOCAL struct IO_TYP IO;
_BUR_LOCAL struct PosToBond_TYP FirstPosToBond[12];
_BUR_LOCAL struct EncConfig_TYP EncConfig[5];
_BUR_LOCAL struct EncLatch_TYP EncLatch[5];
_BUR_LOCAL struct SimEncod_TYP SimEncodPar;
_BUR_LOCAL struct SimEncod_TYP SimulatedEncoderPar[5];
_BUR_LOCAL struct Encod_TYP EncodPar[5];
_BUR_LOCAL struct PosToBond_TYP CurrentFifoPos[5];
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct EncoderStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Encoder/Encoder/Encoder.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ASMcDcs/ASMcDcs.fun\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_42_ */

