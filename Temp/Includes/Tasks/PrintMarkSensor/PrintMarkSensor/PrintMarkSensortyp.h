/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_45_
#define _BUR_1500273701_45_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_INIT_PM_SENSOR = 200,
	eSTEP_GET_BONDEV_PAR
} Steps_ENUM;

typedef struct PrintMarkSensorStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} PrintMarkSensorStep_TYP;

typedef struct IoPrintMarkSen_TYP
{	signed char nRisingCnt;
	signed short nTsRising;
	signed char nFallingCnt;
	signed short nTsFalling;
} IoPrintMarkSen_TYP;

typedef struct IoInput_TYP
{	struct IoPrintMarkSen_TYP PrintMarkSen[12];
} IoInput_TYP;

typedef struct IO_TYP
{	struct IoInput_TYP Input;
} IO_TYP;

typedef struct BonDev_TYP
{	plcbit bEnPmSen;
	signed char nBonDevIdx;
	signed char nEncIdx;
	signed char nIdxTriggerEncoder;
	signed char nTriggerSensorIdx;
	plcbit bVirtualEncoderEnable;
	unsigned char nSourceEncoderIdx;
	SensorType_ENUM eIdxSenType;
	signed long nProductLen;
	signed long nPrintMarkProductOffset;
	signed long nSensorOffset;
	float nDistancePrintMark_VelocityJump;
} BonDev_TYP;

typedef struct PmDetection_TYP
{	signed long nPosFromTs;
	signed long nVirtualSourceEncoderPosFromTs;
	signed short nDiffTime;
	plcbit bCntOk;
} PmDetection_TYP;

typedef struct PmState_TYP
{	struct PmDetection_TYP RisingDetection[12];
	struct PmDetection_TYP FallingDetection[12];
	plcbit bError[12];
} PmState_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/PrintMarkSensor/PrintMarkSensor/PrintMarkSensor.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_45_ */

