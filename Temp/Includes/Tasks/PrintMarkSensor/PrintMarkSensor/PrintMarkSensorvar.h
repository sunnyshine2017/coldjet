/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_46_
#define _BUR_1500273701_46_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variablen */
_BUR_LOCAL unsigned char nProductCnt;
_BUR_LOCAL struct NegativeSensorOffset_TYP NegativeSensorOffset;
_BUR_LOCAL signed long nDistanceVelocityCompensation[12];
_BUR_LOCAL signed long nDistanceWithPrintMarkVelocity[12];
_BUR_LOCAL float rVelocityFactorTriggerPrintMark[12];
_BUR_LOCAL signed short nDiffTime;
_BUR_LOCAL unsigned char nPrintMarkIdx;
_BUR_LOCAL signed char nIdxPrintMarkSensorSource;
_BUR_LOCAL signed char nIdxSenSource;
_BUR_LOCAL plcbit bRequestInit;
_BUR_LOCAL unsigned char nBonDevIdx;
_BUR_LOCAL unsigned char nDevIdx;
_BUR_LOCAL signed char nOldFallingCnt[12];
_BUR_LOCAL signed char nOldRisingCnt[12];
_BUR_LOCAL struct R_TRIG fbEdgePosAuto;
_BUR_LOCAL struct PmState_TYP PmState;
_BUR_LOCAL struct IO_TYP IO;
_BUR_LOCAL struct BonDev_TYP BonDevPar[12];
_BUR_LOCAL struct PmPos_TYP FirstEntry[12];
_BUR_LOCAL struct PmPos_TYP FifoEntry[12];
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct PrintMarkSensorStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/PrintMarkSensor/PrintMarkSensor/PrintMarkSensor.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_46_ */

