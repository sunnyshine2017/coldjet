/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_11_
#define _BUR_1500273701_11_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct PrintMarkSensorCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bPmInit;
	plcbit bGetBonDevPar;
} PrintMarkSensorCallerBox_TYP;

typedef struct PrintMarkSensorDirectBox_TYP
{	plcbit bUpdateBonDevPar;
} PrintMarkSensorDirectBox_TYP;

typedef struct PmFifoEntry_TYP
{	struct PmPos_TYP FifoEntry[301];
} PmFifoEntry_TYP;

typedef struct PrintMarkSensorPar_TYP
{	struct PmFifoEntry_TYP PmFifo[12];
} PrintMarkSensorPar_TYP;

typedef struct PrintMarkSensorState_TYP
{	plcbit bPmSenReady;
	signed long nPmLen[12];
} PrintMarkSensorState_TYP;

typedef struct PrintMarkSensor_TYP
{	struct PrintMarkSensorCallerBox_TYP CallerBox;
	struct PrintMarkSensorDirectBox_TYP DirectBox;
	struct PrintMarkSensorPar_TYP Par;
	struct PrintMarkSensorState_TYP State;
} PrintMarkSensor_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/PrintMarkSensor/G_PrintMarkSensor.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Tasks/TriggerSensor/G_TriggerSensor.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_11_ */

