/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_25_
#define _BUR_1500273701_25_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct MachineCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bDummy;
} MachineCallerBox_TYP;

typedef struct MachineDirectBox_TYP
{	plcbit bResetCntImgProc;
} MachineDirectBox_TYP;

typedef struct MachineParameter_TYP
{	unsigned short nDummy;
} MachineParameter_TYP;

typedef struct TankLvl_TYP
{	float nFillLvlGlue;
	unsigned char nFillLvlSoft;
	unsigned char nFillLvlClean;
} TankLvl_TYP;

typedef struct MachineState_TYP
{	struct TankLvl_TYP TankLvl;
} MachineState_TYP;

typedef struct Machine_TYP
{	struct MachineCallerBox_TYP CallerBox;
	struct MachineDirectBox_TYP DirectBox;
	struct MachineParameter_TYP Par;
	struct MachineState_TYP State;
} Machine_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Machine/G_Machine.typ\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_25_ */

