/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_60_
#define _BUR_1500273701_60_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nMAX_CNT_TANK_IDX 2U
#else
 _LOCAL_CONST unsigned char nMAX_CNT_TANK_IDX;
#endif


/* Variablen */
_BUR_LOCAL float rTank_Weight;
_BUR_LOCAL struct WGHscalePara_typ pWGHscalePara;
_BUR_LOCAL struct WGHscale fb_WGHscale;
_BUR_LOCAL struct MTFilterMovingAverage fb_MTFilterMovAvg;
_BUR_LOCAL signed long Analog_Input;
_BUR_LOCAL unsigned char nShortestDelayTimeIdx;
_BUR_LOCAL unsigned char i;
_BUR_LOCAL plcbit bTankWithDelayedSystemStopEmpty;
_BUR_LOCAL plcdt dtCurrentTimeTankEmpty[3];
_BUR_LOCAL plcdt dtSystemStopTimeTankEmpty[3];
_BUR_LOCAL plcstring sSystemStopTimeTankEmpty[3][6];
_BUR_LOCAL struct TON TON_TankEmptyStopDelay[3];
_BUR_LOCAL struct DTGetTime GetCurrentTimeTankEmpty[3];
_BUR_LOCAL plcbit bEventSetTankIsEmpty[3];
_BUR_LOCAL plcbit bEventSetTankEmptyStopDelay[3];
_BUR_LOCAL plcbit bCalcPos;
_BUR_LOCAL struct R_TRIG fbPosImgProc;
_BUR_LOCAL struct IO_TYP IO;
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct MachineStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Machine/Machine/Machine.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsWeigh/AsWeigh.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MTFilter/MTFilter.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Tasks/Machine/Machine/Machine.typ\\\" scope \\\"local\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_60_ */

