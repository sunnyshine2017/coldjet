/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_59_
#define _BUR_1500273701_59_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_CMD1 = 200,
	eSTEP_CMD1_FINISHED
} Steps_ENUM;

typedef enum Tank_Idx_ENUM
{	eGLUE_TANK_IDX,
	eSOFTING_TANK_IDX,
	eCLEANING_TANK_IDX,
	eMAX_CNT_TANK_IDX
} Tank_Idx_ENUM;

typedef struct MachineStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} MachineStep_TYP;

typedef struct IoInput_TYP
{	plcbit bImgProcErr;
	plcbit bFillLvlSoft;
	plcbit bFillLvlClean;
	plcbit bFillLvlGlue;
} IoInput_TYP;

typedef struct IoOutput_TYP
{	plcbit bError;
	plcbit bTankEmpty;
} IoOutput_TYP;

typedef struct IO_TYP
{	struct IoInput_TYP Input;
	struct IoOutput_TYP Output;
} IO_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Machine/Machine/Machine.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_59_ */

