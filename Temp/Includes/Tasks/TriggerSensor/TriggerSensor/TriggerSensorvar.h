/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_44_
#define _BUR_1500273701_44_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variablen */
_BUR_LOCAL unsigned char nDevIndex;
_BUR_LOCAL unsigned char nProductCnt;
_BUR_LOCAL struct NegativeSensorOffset_TYP NegativeSensorOffset;
_BUR_LOCAL signed long nDistanceProductMoved;
_BUR_LOCAL signed long nDistanceProductHasToMove;
_BUR_LOCAL signed long nPositionProductBegins;
_BUR_LOCAL unsigned short i;
_BUR_LOCAL signed long nFirstTriggerPosition[12];
_BUR_LOCAL signed long nTriggerPosition[12];
_BUR_LOCAL struct PmPos_TYP FifoPrintMarkTriggerPos[12];
_BUR_LOCAL struct PmPos_TYP FirstPrintMarkTriggerPos[12];
_BUR_LOCAL unsigned short nTriggerSignalEntryCount[12];
_BUR_LOCAL plcbit bRequestInit;
_BUR_LOCAL unsigned char nForIdx;
_BUR_LOCAL signed char nIdxSenSource;
_BUR_LOCAL signed char nOldEdgeFallingCnt[12];
_BUR_LOCAL signed char nOldEdgeRisingCnt[12];
_BUR_LOCAL signed short nDiffTime[12];
_BUR_LOCAL struct R_TRIG fbEdgePosAuto;
_BUR_LOCAL struct IgnoreWrongTrigger_type IgnoreWrongTrigger[12];
_BUR_LOCAL struct PosToBond_TYP FirstPosToBond[12];
_BUR_LOCAL struct BonDevParForTriggerSen_TYP BonDevPar[12];
_BUR_LOCAL struct PosToBond_TYP FifoPos[12];
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct TriggerSensorStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/TriggerSensor/TriggerSensor/TriggerSensor.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_44_ */

