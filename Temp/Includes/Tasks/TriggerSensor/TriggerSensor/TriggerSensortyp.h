/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_43_
#define _BUR_1500273701_43_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_INIT_TRIGGER,
	eSTEP_GET_BONDEV_PAR
} Steps_ENUM;

typedef struct TriggerSensorStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} TriggerSensorStep_TYP;

typedef struct BonDevParForTriggerSen_TYP
{	plcbit bEnTrSen;
	signed char nBonDevIdx;
	signed char nEncoderIdx;
	signed char nIdxPrintMarkSensorSource;
	SensorType_ENUM eIdxSenType;
	signed long nProductLen;
	float nDistancePrintMarkTriggerSensor;
	signed long nSensorOffset;
} BonDevParForTriggerSen_TYP;

typedef struct IgnoreWrongTrigger_type
{	signed long nDiffLen;
	signed long nStartPosPro;
	plcbit bInit;
} IgnoreWrongTrigger_type;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/TriggerSensor/TriggerSensor/TriggerSensor.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_43_ */

