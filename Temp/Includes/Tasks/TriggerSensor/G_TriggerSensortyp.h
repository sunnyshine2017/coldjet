/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_9_
#define _BUR_1500273701_9_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct TriggerSensorCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bInit;
	plcbit bGetBonDevPar;
} TriggerSensorCallerBox_TYP;

typedef struct TriggerSensorDirectBox_TYP
{	plcbit bUpdateBonDevPar;
} TriggerSensorDirectBox_TYP;

typedef struct TriggerFifo_TYP
{	struct PosToBond_TYP FifoPos[301];
} TriggerFifo_TYP;

typedef struct Timestamp_TYP
{	signed short nTsRising;
	signed char nRisingCnt;
	signed short nTsFalling;
	signed char nFallingCnt;
} Timestamp_TYP;

typedef struct TriggerSensorPar_TYP
{	struct TriggerFifo_TYP PosBondDev[12];
	struct Timestamp_TYP SensorTS[12];
} TriggerSensorPar_TYP;

typedef struct TriggerSensorState_TYP
{	plcbit bTriggerSenReady;
} TriggerSensorState_TYP;

typedef struct TriggerSensor_TYP
{	struct TriggerSensorCallerBox_TYP CallerBox;
	struct TriggerSensorDirectBox_TYP DirectBox;
	struct TriggerSensorPar_TYP Par;
	struct TriggerSensorState_TYP State;
} TriggerSensor_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/TriggerSensor/G_TriggerSensor.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Tasks/TriggerSensor/G_TriggerSensor.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_9_ */

