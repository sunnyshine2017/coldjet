/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_10_
#define _BUR_1500273701_10_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nFIFO_SIZE_INDEX 300U
#else
 _GLOBAL_CONST unsigned short nFIFO_SIZE_INDEX;
#endif


/* Variablen */
_GLOBAL struct BrbMemListManagement_Typ gFifoManTrSen[12];
_GLOBAL struct TriggerSensor_TYP gTriggerSensor;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/TriggerSensor/G_TriggerSensor.var\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_10_ */

