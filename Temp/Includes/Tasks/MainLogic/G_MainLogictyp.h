/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_5_
#define _BUR_1500273701_5_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Cleaning_Type_ENUM
{	eCLEANING_TYPE_STANDARD,
	eCLEANING_TYPE_INTERVAL
} Cleaning_Type_ENUM;

typedef struct MainLogicCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bAutoOn;
	plcbit bCleaningOn;
	plcbit bSystemCleaningStart;
} MainLogicCallerBox_TYP;

typedef struct MainLogicDirectBox_TYP
{	plcbit bStop;
	plcbit bCleaningStop;
	plcbit bSystemCleaningCancel;
	plcbit bAutoCleaningOn;
	plcbit bCmdOpenBonDev[12];
	plcbit bContCleanBonDevWithGlue[12];
	plcbit bIntervalCleanBonDevWithGlue[12];
	plcbit bContCleanBonDevWithDetergent[12];
} MainLogicDirectBox_TYP;

typedef struct MainLogicPar_TYP
{	plcbit bBonDevCleaningEn[12];
	enum Cleaning_Type_ENUM eCleaningType;
	plcbit bDetergentActive;
} MainLogicPar_TYP;

typedef struct OpenBonDev_TYP
{	plcbit bOpenBonDev[12];
	plcbit bCleanInt[12];
	plcbit bCleanStandard[12];
	plcbit bOpenBonDevDirect[12];
	plcbit bAutoCleaningOn[12];
	plcbit bStartInitCleaning;
} OpenBonDev_TYP;

typedef struct MainLogicState_TYP
{	struct OpenBonDev_TYP CleaningBonDev;
	plcbit bAutomatic;
	plcbit bCleaningActive;
	plcbit bAutomaticPossible;
	plcbit bSealingSystem[12];
	plcbit bCleaningContinuous[12];
	plcbit bCleaningWithDetergentActive;
	plcbit bCleaningWithGlueActive;
	plcbit bMainValveOpen;
	plcbit bGlueValveOpen;
	plcbit bDetergentValveOpen;
	unsigned short nSystemCleaningProgress;
	unsigned short nSystemCleaningRestPeriod;
	plcbit bSystemCleaningActive;
	plcbit bAutoCleaningOn;
	plcbit bCleaningWithPressureCtrlActive;
} MainLogicState_TYP;

typedef struct MainLogic_TYP
{	struct MainLogicCallerBox_TYP CallerBox;
	struct MainLogicDirectBox_TYP DirectBox;
	struct MainLogicPar_TYP Par;
	struct MainLogicState_TYP State;
} MainLogic_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/MainLogic/G_MainLogic.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_5_ */

