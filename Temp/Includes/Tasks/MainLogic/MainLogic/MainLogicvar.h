/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_40_
#define _BUR_1500273701_40_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variablen */
_BUR_LOCAL struct TON TON_SystemCleaningTimeElapsed;
_BUR_LOCAL struct F_TRIG NegEdgeStateCleaningActive;
_BUR_LOCAL struct F_TRIG NegEdgeCleaningContinuous[12];
_BUR_LOCAL struct F_TRIG NegEdgeOpenBonDev[12];
_BUR_LOCAL plcbit bCleaningWithGlueActive;
_BUR_LOCAL plcbit bCleaningWithDetergentActive;
_BUR_LOCAL struct Cleaning_TYP CleaningType;
_BUR_LOCAL unsigned char nDevIdx;
_BUR_LOCAL unsigned char bStopCleaning;
_BUR_LOCAL struct R_TRIG fbEdgeStopCleaning;
_BUR_LOCAL struct IO_TYP IO;
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct MainLogicStep_TYP Step;
_BUR_LOCAL struct BrbStepHandling_TYP SystemCleaningStepHandling;
_BUR_LOCAL struct SystemCleaningStep_TYP SystemCleaningStep;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/MainLogic/MainLogic/MainLogic.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_40_ */

