/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_39_
#define _BUR_1500273701_39_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum SystemCleaningSteps_ENUM
{	eSTEP_WAIT_START_SYSTEM_CLEANING = 0,
	eSTEP_START_SYSTEM_CLEANING,
	eSTEP_OPEN_SYS_CLEAN_VALVES,
	eSTEP_START_GLUE_PUMP,
	eSTEP_STOP_GLUE_PUMP,
	eSTEP_START_DETERGENT_PUMP,
	eSTEP_STOP_DETERGENT_PUMP,
	eSTEP_CLOSE_SYS_CLEAN_VALVES,
	eSTEP_SYSTEM_CLEANING_DONE,
	eSTEP_CANCEL_SYSTEM_CLEANING
} SystemCleaningSteps_ENUM;

typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_AUTOMATIC_ON = 200,
	eSTEP_STOP = 201,
	eSTEP_CLEANING = 202,
	eSTEP_STOP_CLEANING = 203
} Steps_ENUM;

typedef struct SystemCleaningStep_TYP
{	plcstring sStepText[51];
	enum SystemCleaningSteps_ENUM eStepNr;
	plcbit bInitDone;
} SystemCleaningStep_TYP;

typedef struct MainLogicStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} MainLogicStep_TYP;

typedef struct IoOutput_TYP
{	plcbit bValveY1;
	plcbit bValveY2;
	plcbit bValveY3;
	plcbit bSealingSystemCommon;
	plcbit bSealingSystem[12];
	plcbit bValveSystemCleaning[12];
} IoOutput_TYP;

typedef struct IoInput_TYP
{	plcbit bOpenBonDev[12];
	plcbit bCleaningInt[12];
	plcbit bCleaningContinuous[12];
} IoInput_TYP;

typedef struct IO_TYP
{	struct IoOutput_TYP Output;
	struct IoInput_TYP Input;
} IO_TYP;

typedef struct Cleaning_TYP
{	plcbit bCleaningCmpVar[12];
	signed long nCmpResult;
	unsigned char nCyclicCnt;
} Cleaning_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/MainLogic/MainLogic/MainLogic.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_39_ */

