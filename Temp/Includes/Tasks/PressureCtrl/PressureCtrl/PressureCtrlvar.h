/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_52_
#define _BUR_1500273701_52_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nINT_FFFF 32767
 #define nMAX_PRESSURE 10
 #define nFACTOR_M_TO_MM 1000
#else
 _LOCAL_CONST signed short nINT_FFFF;
 _LOCAL_CONST signed short nMAX_PRESSURE;
 _LOCAL_CONST signed short nFACTOR_M_TO_MM;
#endif


/* Variablen */
_BUR_LOCAL struct TON TON_CloseBypassValve;
_BUR_LOCAL struct NegEdge_TYP NegEdge;
_BUR_LOCAL unsigned short nBonDevIdx;
_BUR_LOCAL plcbit bEncVelOfEnabledBonDevNotZero;
_BUR_LOCAL struct IO_type IO;
_BUR_LOCAL struct ActPressure_type ActPressure;
_BUR_LOCAL struct PressureCtrl_type PressureCtrl;
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct PressureCtrlStep_type Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/PressureCtrl/PressureCtrl/PressureCtrl.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_52_ */

