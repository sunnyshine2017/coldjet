/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_51_
#define _BUR_1500273701_51_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Steps_enum
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_CMD1 = 200,
	eSTEP_CMD1_FINISHED
} Steps_enum;

typedef struct PressureCtrlStep_type
{	plcstring sStepText[51];
	enum Steps_enum eStepNr;
	plcbit bInitDone;
} PressureCtrlStep_type;

typedef struct FubPar_type
{	struct PressureControl_TYP PressureParOld;
	signed long nCmpResult;
	float rFubPressureOld;
	float rDiffFubPressure;
	plcbit bUpdatePar;
} FubPar_type;

typedef struct PressureCtrl_type
{	struct MTBasicsLevelController fbLevelController;
	struct FubPar_type FubPar;
	double rGradient;
	float rAdaptedPressure;
	float rFubPressure;
	plcbit bPressureCtrlErr;
} PressureCtrl_type;

typedef struct Input_type
{	signed short nActPressure;
} Input_type;

typedef struct Output_type
{	plcbit bPressureDecrease;
	plcbit bPressureIncrease;
	plcbit bPressureCtrlMoveGlue;
	plcbit bPressureCtrlVent;
	plcbit bOpenBypassValve;
} Output_type;

typedef struct IO_type
{	struct Input_type Input;
	struct Output_type Output;
} IO_type;

typedef struct ActPressure_type
{	double rGradient;
	float rMaxPressure;
	float rMinPressure;
} ActPressure_type;

typedef struct NegEdge_TYP
{	struct F_TRIG EncVelOfEnabledBonDevNotZero;
} NegEdge_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/PressureCtrl/PressureCtrl/PressureCtrl.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_51_ */

