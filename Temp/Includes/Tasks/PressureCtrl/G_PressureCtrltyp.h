/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_17_
#define _BUR_1500273701_17_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct PressureCtrlCallerBox_TYP
{	struct BrbCaller_TYP Caller;
} PressureCtrlCallerBox_TYP;

typedef struct PressureCtrlDirectBox_TYP
{	plcbit bReducePressureByOpenBypass;
} PressureCtrlDirectBox_TYP;

typedef struct PressureCtrlPar_TYP
{	float rPressureCorr;
} PressureCtrlPar_TYP;

typedef struct PressureCtrlState_TYP
{	float rActPressure;
	plcbit bPressureCtrlActive;
} PressureCtrlState_TYP;

typedef struct PressureCtrl_TYP
{	struct PressureCtrlCallerBox_TYP CallerBox;
	struct PressureCtrlDirectBox_TYP DirectBox;
	struct PressureCtrlPar_TYP Par;
	struct PressureCtrlState_TYP State;
} PressureCtrl_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/PressureCtrl/G_PressureCtrl.typ\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_17_ */

