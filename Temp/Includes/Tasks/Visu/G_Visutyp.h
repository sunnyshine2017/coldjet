/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_27_
#define _BUR_1500273701_27_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum VisuEthDescriptionIndices_ENUM
{	VIS_ETH_DESC_INDEX_ONBOARD = 0,
	VIS_ETH_DESC_INDEX_PANEL = 1,
	VIS_ETH_DESC_INDEX_COMMUNICATION = 2,
	VIS_ETH_DESC_INDEX_REMOTE_ACCESS = 3
} VisuEthDescriptionIndices_ENUM;

typedef struct GlobalDataMsgPopup
{	signed short ChangePage;
	signed short CurrentPage;
} GlobalDataMsgPopup;

typedef struct VisuCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bDummy;
} VisuCallerBox_TYP;

typedef struct VisuDirectBox_TYP
{	plcbit bShowDialogMoveAxes;
	plcbit bShowDialogStartHeating;
} VisuDirectBox_TYP;

typedef struct VisuPar_TYP
{	plcbit bDummy;
} VisuPar_TYP;

typedef struct VisuState_TYP
{	plcbit bDialogMoveAxesActive;
	plcbit bDialogStartHeatingActive;
} VisuState_TYP;

typedef struct Visu_TYP
{	struct VisuCallerBox_TYP CallerBox;
	struct VisuDirectBox_TYP DirectBox;
	struct VisuPar_TYP Par;
	struct VisuState_TYP State;
} Visu_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Visu/G_Visu.typ\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_27_ */

