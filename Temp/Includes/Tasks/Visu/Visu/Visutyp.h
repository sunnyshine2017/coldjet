/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500276136_1_
#define _BUR_1500276136_1_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Pages_ENUM
{	ePAGE_TEMPLATE = 0,
	ePAGE_SCREENSAVER = 10,
	ePAGE_MAIN = 1000,
	ePAGE_HEAD = 1100,
	ePAGE_HEAD_COPY = 1101,
	ePAGE_HEAD_PAR = 1200,
	ePAGE_PRODUCTION = 2000,
	ePAGE_PRODUCTION_SAVE = 2010,
	ePAGE_PRODUCTION_DELETE = 2020,
	ePAGE_CONFIGURATION1 = 2101,
	ePAGE_CONFIGURATION2 = 2102,
	ePAGE_CONFIGURATION3 = 2103,
	ePAGE_CONFIGURATION4 = 2104,
	ePAGE_CONFIGURATION5 = 2105,
	ePAGE_CONFIGURATION6 = 2106,
	ePAGE_CONFIGURATION7 = 2107,
	ePAGE_CONFIGURATION8 = 2108,
	ePAGE_CONFIGURATION9 = 2109,
	ePAGE_CONFIGURATION10 = 2110,
	ePAGE_CONFIGURATION11 = 2111,
	ePAGE_CONFIGURATION12 = 2112,
	ePAGE_CONFIGURATION13 = 2113,
	ePAGE_CONFIGURATION14 = 2114,
	ePAGE_CONFIGURATION15 = 2115,
	ePAGE_CONFIGURATION16 = 2116,
	ePAGE_CONFIGURATION17 = 2117,
	ePAGE_CONFIG_PAR_TMPCTRL_ZONE = 2150,
	ePAGE_SERVICE1 = 2201,
	ePAGE_SERVICE2 = 2202,
	ePAGE_SERVICE3 = 2203,
	ePAGE_USER = 2300,
	ePAGE_MODULE_CONFIG = 2310,
	ePAGE_EVENTS = 2400,
	ePAGE_CONTACT = 2500,
	ePAGE_OVERVIEW = 3000,
	ePAGE_CLEANING = 3100,
	ePAGE_SYSTEM_CLEANING = 3101,
	ePAGE_PRESSURE_CONTROL = 3200,
	ePAGE_MOTORIZED_ADJUSTMENT0 = 3300,
	ePAGE_MOTORIZED_ADJUSTMENT1 = 3301,
	ePAGE_CAMERA = 3400,
	ePAGE_TEMPCTRL = 3500,
	ePAGE_ANALOGFUELLSTAND = 4000,
	ePAGE_DLG_MOVE_AXES = 10000,
	ePAGE_DLG_START_HEATING = 10010,
	ePAGE_MEDIENVERBRAUCH = 10011
} Pages_ENUM;

typedef enum Colors_ENUM
{	eBACKCOLOR_BLACK = 0,
	eBACKCOLOR_BLUE = 1,
	eBACKCOLOR_GREEN = 2,
	eBACKCOLOR_LIGHT_GREY = 7,
	eBACKCOLOR_LIGHT_BLUE = 9,
	eBACKCOLOR_LIGHT_GREEN = 10,
	eBACKCOLOR_WHITE = 15,
	eBACKCOLOR_RED = 45,
	eBACKCOLOR_YELLOW = 46,
	eBACKCOLOR_SILVER = 59,
	eBACKCOLOR_DARK_GREY = 248,
	eBACKCOLOR_MINT_GREEN = 106,
	eBACKCOLOR_PAGE = 249,
	eBACKCOLOR_INPUT = 96,
	eBACKCOLOR_EDIT = 100,
	eBACKCOLOR_PLANATOL_BLUE = 240,
	eBACKCOLOR_FOCUS = 133,
	eFORECOLOR_BLACK = 0,
	eFORECOLOR_BLUE = 256,
	eFORECOLOR_GREEN = 512,
	eFORECOLOR_LIGHT_GREY = 1792,
	eFORECOLOR_LIGHT_BLUE = 2304,
	eFORECOLOR_LIGHT_GREEN = 2560,
	eFORECOLOR_WHITE = 3840,
	eFORECOLOR_RED = 11520,
	eFORECOLOR_YELLOW = 11776,
	eFORECOLOR_SILVER = 15104,
	eFORECOLOR_DARK_GREY = 63488,
	eFORECOLOR_MINT_GREEN = 27136,
	eFORECOLOR_PAGE = 64512,
	eFORECOLOR_INPUT = 24576,
	eFORECOLOR_EDIT = 25600,
	eFORECOLOR_FOCUS = 25856,
	eFORECOLOR_PLANATOL_BLUE = 61440,
	eCOLOR_SELECTED = 96,
	eCOLOR_ENABLED = 15,
	eCOLOR_DISABLED = 63737,
	eCOLOR_MENU_UNSELECTED = 252,
	eCOLOR_MENU_SELECTED = 7,
	eCOLOR_INPUT = 15,
	eCOLOR_OUTPUT = 249
} Colors_ENUM;

typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_CMD1 = 200,
	eSTEP_CMD1_FINISHED
} Steps_ENUM;

typedef enum UserLevel_ENUM
{	eUSERLEVEL_OPERATOR,
	eUSERLEVEL_SERVICE,
	eUSERLEVEL_CONFIGURATOR
} UserLevel_ENUM;

typedef enum DropsBmpIndices_ENUM
{	eDROPS_BMP_INDEX_BORDER,
	eDROPS_BMP_INDEX_BLUE,
	eDROPS_BMP_INDEX_GREEN,
	eDROPS_BMP_INDEX_YELLOW,
	eDROPS_BMP_INDEX_RED,
	eDROPS_BMP_INDEX_BORDER_BIG,
	eDROPS_BMP_INDEX_BLUE_BIG,
	eDROPS_BMP_INDEX_GREEN_BIG,
	eDROPS_BMP_INDEX_YELLOW_BIG,
	eDROPS_BMP_INDEX_RED_BIG
} DropsBmpIndices_ENUM;

typedef enum HeadBmpIndices_ENUM
{	eHEAD_BMP_INDEX_SMALL_GREY,
	eHEAD_BMP_INDEX_SMALL_RED,
	eHEAD_BMP_INDEX_SMALL_BLUE,
	eHEAD_BMP_INDEX_SMALL_GREEN,
	eHEAD_BMP_INDEX_BIG_GREY,
	eHEAD_BMP_INDEX_BIG_RED,
	eHEAD_BMP_INDEX_BIG_BLUE,
	eHEAD_BMP_INDEX_BIG_GREEN
} HeadBmpIndices_ENUM;

typedef enum TankBmpIndices_ENUM
{	eTANK_BMP_INDEX_SMALL_GREY,
	eTANK_BMP_INDEX_SMALL_RED,
	eTANK_BMP_INDEX_SMALL_BLUE,
	eTANK_BMP_INDEX_SMALL_GREEN
} TankBmpIndices_ENUM;

typedef enum ModuleBmpIndices_ENUM
{	eMODUL_BMP_INDEX_EMPTY,
	eMODUL_BMP_INDEX_OVERVIEW,
	eMODUL_BMP_INDEX_CLEANING,
	eMODUL_BMP_INDEX_PRESSURE_CTRL,
	eMODUL_BMP_INDEX_MOT_ADJUSTMENT,
	eMODUL_BMP_INDEX_CAMERA,
	eMODUL_BMP_INDEX_TEMPCTRL,
	eMODUL_BMP_INDEX_ANALOGFUELL
} ModuleBmpIndices_ENUM;

typedef enum Vis_TempCtrlParType_ENUM
{	eVIS_TEMPCTRL_PAR_UNKNOWN,
	eVIS_TEMPCTRL_PAR_TANK,
	eVIS_TEMPCTRL_PAR_TUBE,
	eVIS_TEMPCTRL_PAR_HEAD,
	eVIS_TEMPCTRL_PAR_RESERVE
} Vis_TempCtrlParType_ENUM;

typedef struct VisuStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} VisuStep_TYP;

typedef struct VisGeneralOwn_TYP
{	unsigned short nScreenWidth;
	unsigned short nScreenHeight;
	unsigned short nUserLevel;
	unsigned short nUserLevelOld;
	plcbit bBlink400;
} VisGeneralOwn_TYP;

typedef struct VisHeader_TYP
{	struct BrbVc4Bitmap_TYP bmpLogo;
	struct BrbVc4Text_TYP txtStepperSimulated;
	plcstring sDateTime[25];
	struct BrbVc4Button_TYP btnMenu;
	struct BrbVc4Button_TYP btnHelp;
	struct BrbVc4Button_TYP btnHome;
} VisHeader_TYP;

typedef struct VisMenu_TYP
{	enum Pages_ENUM eLastMenuPage;
	struct BrbVc4Button_TYP btnProduction;
	struct BrbVc4Button_TYP btnConfiguration;
	struct BrbVc4Button_TYP btnService;
	struct BrbVc4Button_TYP btnUser;
	struct BrbVc4Button_TYP btnEvents;
	struct BrbVc4Button_TYP btnContact;
	unsigned short nPageCount;
	unsigned short nPageCountTotal;
} VisMenu_TYP;

typedef struct VisHead_TYP
{	struct BrbVc4Button_TYP btnHead;
	struct BrbVc4JogButton_TYP btnHeadJog;
	struct BrbVc4Numeric_TYP numHead;
	struct BrbVc4Checkbox_TYP chkHead;
	struct BrbVc4Hotspot_TYP hsHead;
	struct BrbVc4Numeric_TYP numEncoderVelocity;
} VisHead_TYP;

typedef struct VisHeads_TYP
{	signed short nHeadOffset;
	struct VisHead_TYP Head[12];
} VisHeads_TYP;

typedef struct VisTank_TYP
{	struct BrbVc4Button_TYP btnTank;
	struct BrbVc4Numeric_TYP numTank;
} VisTank_TYP;

typedef struct VisTanks_TYP
{	signed short nTankOffset;
	struct VisTank_TYP Tank[4];
} VisTanks_TYP;

typedef struct VisNavigation_TYP
{	struct BrbVc4Button_TYP btnLeft;
	struct BrbVc4Button_TYP btnRight;
	struct BrbVc4Button_TYP btnUp;
	struct BrbVc4Button_TYP btnDown;
} VisNavigation_TYP;

typedef struct VisToolbarModuleSource_TYP
{	enum ModuleBmpIndices_ENUM eModuleBmpIndex;
} VisToolbarModuleSource_TYP;

typedef struct VisToolbarModuleShow_TYP
{	struct BrbVc4Button_TYP btnModule;
} VisToolbarModuleShow_TYP;

typedef struct VisToolbar_TYP
{	struct VisToolbarModuleSource_TYP ModulesSource[11];
	struct BrbVc4Button_TYP btnLeft;
	struct BrbVc4Button_TYP btnRight;
	signed short nScrollOffsetMax;
	signed short nScrollOffset;
	unsigned short nEnabledModuleCount;
	struct VisToolbarModuleShow_TYP ModulesShow[6];
} VisToolbar_TYP;

typedef struct VisDropRunning_TYP
{	plcbit bRun;
	unsigned short nIndex;
	unsigned short nStatus;
} VisDropRunning_TYP;

typedef struct VisFooter_TYP
{	struct BrbVc4Bitmap_TYP bmpDrop;
	struct VisDropRunning_TYP DropRunningGreen;
	struct VisDropRunning_TYP DropRunningYellow;
	struct VisDropRunning_TYP DropRunningRed;
	struct BrbVc4Hotspot_TYP hsDrop;
	struct BrbVc4Text_TYP txtParInvalidWarning;
	struct BrbVc4Text_TYP txtParSaveWarning;
	struct BrbVc4String_TYP strCurrentProductName;
	plcbit bShowStopStart;
	struct BrbVc4Button_TYP btnStart;
	struct BrbVc4Button_TYP btnStop;
} VisFooter_TYP;

typedef struct VisPageScreensaver_TYP
{	plcbit bDummy;
} VisPageScreensaver_TYP;

typedef struct VisPageDialogMoveAxes_TYP
{	plcbit bAllAxesHomingOk;
	struct BrbVc4Text_TYP txtHoming;
	struct BrbVc4Button_TYP btnNo;
	struct BrbVc4Button_TYP btnYes;
} VisPageDialogMoveAxes_TYP;

typedef struct VisPageDialogStartHeating_TYP
{	struct BrbVc4Checkbox_TYP chkTank[2];
	struct BrbVc4Button_TYP btnOk;
	unsigned short nStep;
	unsigned short nTankIndexStart;
} VisPageDialogStartHeating_TYP;

typedef struct VisPageMain_TYP
{	struct BrbVc4Bargraph_TYP bgGlue;
	struct BrbVc4Bargraph_TYP bgSoft;
} VisPageMain_TYP;

typedef struct VisPageHeadSwitch_TYP
{	unsigned short nNumber;
	float rPause;
	float rLine;
	plcbit bInputCompleted;
	unsigned short nColor;
	unsigned short nStatus;
} VisPageHeadSwitch_TYP;

typedef struct VisPageHead_TYP
{	unsigned short nSelectedHeadIndex;
	struct BrbVc4Text_TYP txtSensorType;
	unsigned short nSelectedPrintmarkStatus;
	unsigned short nSelectedPrintMarkIndex;
	struct BrbVc4Button_TYP btnPrintmarkMinus;
	struct BrbVc4Button_TYP btnPrintmarkPlus;
	signed short nSwitchOffset;
	struct BrbVc4Button_TYP btnSwitchMinus;
	struct BrbVc4Numeric_TYP numSwitchIndexMax;
	struct BrbVc4Button_TYP btnSwitchPlus;
	struct BrbVc4Numeric_TYP numProductLen;
	struct BrbVc4Numeric_TYP numCylinderPerimeter;
	struct BrbVc4Checkbox_TYP chkStichModeOn;
	struct BrbVc4Numeric_TYP numTrackLenTotal;
	struct VisPageHeadSwitch_TYP SwitchesShow[3];
	struct BrbVc4Numeric_TYP numStitchCloseDistance;
	struct BrbVc4Dropdown_TYP ddStitchSize;
	struct BrbVc4Button_TYP btnSwitchesRight;
	struct BrbVc4Button_TYP btnSwitchesLeft;
	struct BrbVc4Button_TYP btnCopy;
	struct BrbVc4Button_TYP btnApply;
	struct BrbVc4Button_TYP btnHeadPar;
} VisPageHead_TYP;

typedef struct VisPageHeadCopy_TYP
{	struct BrbVc4Numeric_TYP numSourceHeadIndex;
	struct BrbVc4Numeric_TYP numDestHeadIndex;
	struct BrbVc4Button_TYP btnCancel;
	struct BrbVc4Button_TYP btnCopy;
} VisPageHeadCopy_TYP;

typedef struct VisPageHeadPar_TYP
{	unsigned short nSelectedHeadIndex;
	struct BrbVc4Text_TYP txtSensorType;
	struct BrbVc4Numeric_TYP numHeadProductOffset;
	struct BrbVc4Numeric_TYP numHopperCleaningPeriod;
	struct BrbVc4Numeric_TYP numHopperFillingPeriod;
	struct BrbVc4Numeric_TYP numCollectorCleaningPeriod;
	struct BrbVc4Numeric_TYP numCollectorFillingPeriod;
	struct BrbVc4Button_TYP btnBack;
} VisPageHeadPar_TYP;

typedef struct VisPageProdFileListShowEntry_TYP
{	plcstring sProductName[260];
	unsigned short nColor;
} VisPageProdFileListShowEntry_TYP;

typedef struct VisPageProductionFileList_TYP
{	plcbit bGetListSource;
	struct BrbReadDir fbBrbReadDir;
	struct BrbReadDirListEntry_TYP ListSource[101];
	plcbit bSelectCurrentProduct;
	signed long nCurrentProcuctIndex;
	plcstring sSelectedProductName[260];
	struct BrbVc4ScrollList_TYP ScrollList;
	struct VisPageProdFileListShowEntry_TYP ListShow[8];
	struct BrbVc4Touchgrid_TYP tgList;
} VisPageProductionFileList_TYP;

typedef struct VisPageProduction_TYP
{	struct VisPageProductionFileList_TYP FileList;
	struct BrbVc4Button_TYP btnLoad;
	struct BrbVc4Button_TYP btnSave;
	struct BrbVc4Button_TYP btnDelete;
} VisPageProduction_TYP;

typedef struct VisPageProductionSave_TYP
{	struct BrbVc4String_TYP strOldName;
	struct BrbVc4String_TYP strNewName;
	struct BrbVc4Button_TYP btnCancel;
	struct BrbVc4Button_TYP btnSave;
	plcbit bWaitForSave;
} VisPageProductionSave_TYP;

typedef struct VisPageProductionDelete_TYP
{	struct BrbVc4String_TYP strName;
	struct BrbVc4Button_TYP btnCancel;
	struct BrbVc4Button_TYP btnDelete;
	plcbit bWaitForDelete;
} VisPageProductionDelete_TYP;

typedef struct VisPageConfigParTempCtrlZone_TYP
{	enum Vis_TempCtrlParType_ENUM eParType;
	struct TempCtrlTempZoneCfgPar_TYP* pParSource;
	struct BrbVc4Numeric_TYP numSetTempMax;
	struct BrbVc4Numeric_TYP numGain;
	struct BrbVc4Numeric_TYP numIntegrationTime;
	struct BrbVc4Numeric_TYP numDerivativeTime;
	struct BrbVc4Numeric_TYP numFilterTime;
	struct BrbVc4Numeric_TYP numMinOut;
	struct BrbVc4Numeric_TYP numMaxOut;
} VisPageConfigParTempCtrlZone_TYP;

typedef struct VisPageConfiguration_TYP
{	plcbit bInputAllowed;
	unsigned short nInputStatus;
	plcbit bInputAllowedInAutomatic;
	enum Pages_ENUM eLastConfigurationPageIndex;
	struct BrbVc4Hotspot_TYP hsLanguageGerman;
	struct BrbVc4Shape_TYP shLanguageGerman;
	struct BrbVc4Hotspot_TYP hsLanguageEnglish;
	struct BrbVc4Shape_TYP shLanguageEnglish;
	struct BrbVc4Checkbox_TYP chkTankGlueEnable;
	struct BrbVc4Checkbox_TYP chkTankCleanEnable;
	struct BrbVc4Checkbox_TYP chkTankSoftEnable;
	signed short nSelectedHeadIndex;
	struct BrbVc4Button_TYP btnHeadRight;
	struct BrbVc4Button_TYP btnHeadLeft;
	struct BrbVc4Dropdown_TYP ddHeadSensorType;
	struct BrbVc4Layer_TYP layHeadTrigger;
	struct BrbVc4Layer_TYP layHeadEncoder;
	struct BrbVc4Layer_TYP layHeadPrintmark;
	struct BrbVc4Layer_TYP layHeadPrintmarkTrigger;
	struct BrbVc4Numeric_TYP numHeadTriggerSensorNumber;
	struct BrbVc4Numeric_TYP numHeadPrintMarkSensorNumber;
	struct BrbVc4Numeric_TYP numHeadDistanceHeadSensor;
	struct BrbVc4Numeric_TYP numHeadEncoderNumber;
	struct BrbVc4Numeric_TYP numHeadCylinderPerimeter;
	struct BrbVc4Numeric_TYP numHeadReduceProductLenPercent;
	struct BrbVc4Numeric_TYP numHeadPrintMarkEncoder;
	struct BrbVc4Numeric_TYP numHeadPrintMarkSource;
	struct BrbVc4Numeric_TYP numHeadDistancePrintTrigSensor;
	struct BrbVc4Numeric_TYP numHeadDistancePrintVelocityJump;
	struct BrbVc4Numeric_TYP numHeadValveCompOpenTime;
	struct BrbVc4Numeric_TYP numHeadValveCompCloseTime;
	struct BrbVc4Checkbox_TYP chkHeadProductDetectionOn;
	struct BrbVc4Numeric_TYP numHeadProductDetectSensorNumber;
	struct BrbVc4Numeric_TYP numHeadProductDetectSensOffset;
	struct BrbVc4Numeric_TYP numSelectedTriggerSensorIndex;
	struct BrbVc4Button_TYP btnTriggerSensorRight;
	struct BrbVc4Button_TYP btnTriggerSensorLeft;
	struct BrbVc4Numeric_TYP numSelectedEncoderIndex;
	struct BrbVc4Optionbox_TYP optTriggerSensorEdgeDetectPos;
	struct BrbVc4Optionbox_TYP optTriggerSensorEdgeDetectNeg;
	struct BrbVc4Button_TYP btnEncoderRight;
	struct BrbVc4Button_TYP btnEncoderLeft;
	struct BrbVc4Layer_TYP layEncoderSim;
	struct BrbVc4Layer_TYP layEncoderReal;
	struct BrbVc4Layer_TYP layEncoderVirtual;
	struct BrbVc4Layer_TYP layEncoderTrigger;
	struct BrbVc4Dropdown_TYP ddEncoderType;
	struct BrbVc4Checkbox_TYP chkEncoderSimEnable;
	struct BrbVc4Numeric_TYP numEncoderIncPerRev;
	struct BrbVc4Numeric_TYP numEncoderVelocityFactor;
	struct BrbVc4Numeric_TYP numEncoderDistPerRev;
	struct BrbVc4Dropdown_TYP ddEncoderSource;
	struct BrbVc4Numeric_TYP numEncoderFactor;
	struct BrbVc4Dropdown_TYP ddEncoderTriggerSource;
	struct BrbVc4Numeric_TYP numEncoderTriggerSignalDistance;
	struct BrbVc4Numeric_TYP numEncoderTriggerTimeOut;
	struct BrbVc4Numeric_TYP numSelectedPrintmarkSensorIndex;
	struct BrbVc4Button_TYP btnPrintmarkSensorRight;
	struct BrbVc4Button_TYP btnPrintmarkSensorLeft;
	struct BrbVc4Optionbox_TYP optPrintmarkSensorEdgeDetectPos;
	struct BrbVc4Optionbox_TYP optPrintmarkSensorEdgeDetectNeg;
	struct BrbVc4Numeric_TYP numSelectedPrintmarkIndex;
	struct BrbVc4Numeric_TYP numPrintmarkTolerance;
	struct BrbVc4Button_TYP btnPrintmarkRight;
	struct BrbVc4Button_TYP btnPrintmarkLeft;
	struct BrbVc4Numeric_TYP numPrintmarkLength;
	unsigned short nStitchingInputStatus;
	unsigned short nCleaningInputStatus;
	struct BrbVc4Text_TYP txtNoCleaningTank;
	struct BrbVc4Checkbox_TYP chkCleaningAutoValveControl;
	struct BrbVc4Checkbox_TYP chkSystemCleaningEnable;
	unsigned short nPressureControlInputStatus;
	unsigned short nMotorizedAdjustmentInputStatus;
	struct BrbVc4Checkbox_TYP chkMotorizedAdjustmentVertical;
	struct BrbVc4Numeric_TYP numMotAdjustRefOffsetVertical;
	struct BrbVc4Numeric_TYP numMotAdjustNegLimitVertical;
	struct BrbVc4Numeric_TYP numMotAdjustPosLimitVertical;
	struct BrbVc4Checkbox_TYP chkMotorizedAdjustmentHorizontal;
	struct BrbVc4Numeric_TYP numMotAdjustRefOffsetHorizontal;
	struct BrbVc4Numeric_TYP numMotAdjustNegLimitHorizontal;
	struct BrbVc4Numeric_TYP numMotAdjustPosLimitHorizontal;
	unsigned short nImageProcessingInputStatus;
	struct BrbVc4Button_TYP btnTankRight;
	struct BrbVc4Button_TYP btnTankLeft;
	signed short nSelectedTankIndex;
	unsigned short nTempCtrlInputStatus;
	struct BrbVc4Checkbox_TYP chkTempCtrlTankExists;
	struct BrbVc4Button_TYP btnParTempCtrlZoneTank;
	struct BrbVc4Numeric_TYP numTempCtrlToleranceMin;
	struct BrbVc4Numeric_TYP numTempCtrlToleranceMax;
	struct BrbVc4Numeric_TYP numTempCtrlTempWarning;
	struct VisPageConfigParTempCtrlZone_TYP ParTempCtrlZone;
	struct BrbVc4Checkbox_TYP chkTempCtrlHeadZonesExist;
	struct BrbVc4Checkbox_TYP chkTempCtrlHeadZoneHeadExist;
	struct BrbVc4Checkbox_TYP chkTempCtrlHeadZoneTubeExist;
	struct BrbVc4Checkbox_TYP chkTempCtrlHeadZoneReserveExist;
	struct BrbVc4Button_TYP btnParTempCtrlZoneTube;
	struct BrbVc4Button_TYP btnParTempCtrlZoneHead;
	struct BrbVc4Button_TYP btnParTempCtrlZoneReserve;
	struct BrbVc4Dropdown_TYP ddTempCtrlHeadTankNumber;
	struct BrbVc4Numeric_TYP numTempCtrlStandbyTemp;
	struct BrbVc4Button_TYP btnStrainGaugeProdReset;
	struct BrbVc4Button_TYP btnStrainGaugeProdSumReset;
	struct BrbVc4Button_TYP btnStrainGaugeSoftSumReset;
	struct BrbVc4Button_TYP btnStrainGaugeSoftReset;
	struct BrbVc4Button_TYP btnTabOfConsum;
} VisPageConfiguration_TYP;

typedef struct VisPageServiceEthInterface_TYP
{	plcbit bInputCompleted;
	unsigned short nStatus;
} VisPageServiceEthInterface_TYP;

typedef struct VisPageService_TYP
{	struct BrbVc4Numeric_TYP numOperatingHoursTotal;
	struct BrbVc4Numeric_TYP numOperatingHoursService;
	struct BrbVc4Button_TYP btnReset;
	struct BrbVc4Button_TYP btnCalibrateTouch;
	struct BrbVc4Numeric_TYP numVelocityEncoder[5];
	struct DTStructure dtSystemTime;
	unsigned short nSystemTimeStatus;
	plcbit bSystemTimeInputCompleted;
	struct BrbVc4Numeric_TYP numSelectedPrintmarkSensorIndex;
	struct BrbVc4Button_TYP btnPrintmarkSensorRight;
	struct BrbVc4Button_TYP btnPrintmarkSensorLeft;
	struct BrbVc4Numeric_TYP numPrintmarkLen;
	struct BrbVc4Button_TYP btnSimEncoderVelocityMinus;
	struct BrbVc4Numeric_TYP numSimEncoderVelocity;
	struct BrbVc4Button_TYP btnSimEncoderVelocityPlus;
	struct BrbVc4Button_TYP btnSimEncoderStart;
	struct BrbVc4Button_TYP btnSimEncoderStop;
	struct VisPageServiceEthInterface_TYP EthInterface[3];
} VisPageService_TYP;

typedef struct VisPageUser_TYP
{	struct BrbVc4Button_TYP btnPassword;
	struct BrbVc4Password_TYP pwPassword;
	struct BrbVc4Button_TYP btnLogout;
	struct BrbVc4Button_TYP btnModConfig;
	struct BrbVc4Button_TYP btnMsgPopupOk;
	struct BrbVc4Bitmap_TYP bmpMsgPopupBell;
	struct BrbVc4Text_TYP txtMsgPopup;
} VisPageUser_TYP;

typedef struct VisPageModuleConfig_TYP
{	struct BrbVc4Checkbox_TYP chkOverviewEnable;
	struct BrbVc4Checkbox_TYP chkCleaningEnable;
	struct BrbVc4Checkbox_TYP chkStitchingEnable;
	struct BrbVc4Checkbox_TYP chkMotorizedAdjustmentEnable;
	struct BrbVc4Checkbox_TYP chkPressureControlEnable;
	struct BrbVc4Checkbox_TYP chkImageProcessingEnable;
	struct BrbVc4Checkbox_TYP chkTempCtrlEnable;
	struct BrbVc4Checkbox_TYP chkAnalogFuellstand;
	struct BrbVc4Button_TYP btnOk;
} VisPageModuleConfig_TYP;

typedef struct VisPageEvents_TYP
{	struct BrbVc4Button_TYP btnDummy;
} VisPageEvents_TYP;

typedef struct VisPageContact_TYP
{	unsigned short nDummy;
} VisPageContact_TYP;

typedef struct VisPageOverview_TYP
{	struct BrbVc4Button_TYP btnCorrectionMinus;
	struct BrbVc4Numeric_TYP numCorrection;
	struct BrbVc4Button_TYP btnCorrectionPlus;
} VisPageOverview_TYP;

typedef struct VisPageCleaning_TYP
{	unsigned short nHeadsEnabledCount;
	struct BrbVc4Bargraph_TYP bgClean;
	struct BrbVc4Optionbox_TYP optCleanInterval;
	struct BrbVc4Optionbox_TYP optCleanStandard;
	struct BrbVc4Optionbox_TYP optBondInterval;
	struct BrbVc4Optionbox_TYP optBondStandard;
	struct BrbVc4Button_TYP btnStop;
	struct BrbVc4Button_TYP btnStart;
} VisPageCleaning_TYP;

typedef struct VisPageSystemCleaning_TYP
{	struct BrbVc4String_TYP strProgress;
	struct BrbVc4Numeric_TYP numGlueTankCleaningPeriod;
	struct BrbVc4Numeric_TYP numDetergentTankCleaningPeriod;
	struct BrbVc4Button_TYP btnCancel;
	struct BrbVc4Button_TYP btnStart;
} VisPageSystemCleaning_TYP;

typedef struct VisPagePressureControl_TYP
{	struct BrbVc4Button_TYP btnCorrectionMinus;
	struct BrbVc4Numeric_TYP numCorrection;
	struct BrbVc4Button_TYP btnCorrectionPlus;
} VisPagePressureControl_TYP;

typedef struct VisPageMotorizedAdjustmentAx_TYP
{	struct BrbVc4Numeric_TYP numAxPos;
	struct BrbVc4JogButton_TYP btnAxJogNeg;
	struct BrbVc4JogButton_TYP btnAxJogPos;
	struct BrbVc4Button_TYP btnStop;
	struct BrbVc4Button_TYP btnHome;
	struct BrbVc4Numeric_TYP numAxLimitNeg;
	struct BrbVc4Numeric_TYP numAxLimitPos;
	struct BrbVc4Numeric_TYP numParkPos;
	struct BrbVc4Button_TYP btnMoveToParkPos;
	struct BrbVc4Numeric_TYP numWorkPos;
	struct BrbVc4Button_TYP btnMoveToWorkPos;
} VisPageMotorizedAdjustmentAx_TYP;

typedef struct VisPageMotorizedAdjustment_TYP
{	unsigned short nSelectedHeadIndex;
	struct VisPageMotorizedAdjustmentAx_TYP Vertical;
	struct VisPageMotorizedAdjustmentAx_TYP Horizontal;
	struct BrbVc4Button_TYP btnHomeAllAxes;
	struct BrbVc4Button_TYP btnLeft;
	struct BrbVc4Button_TYP btnRight;
} VisPageMotorizedAdjustment_TYP;

typedef struct VisPageCamera_TYP
{	struct BrbVc4Button_TYP btnReset;
} VisPageCamera_TYP;

typedef struct VisPageMedienVerbrauch_TYP
{	struct BrbVc4Button_TYP btnSave;
	struct VisPageProductionFileList_TYP sProductNameMedien;
	float rSoftConcWeight[8];
	float rGlueWeight[8];
	plcdt dtDateTime[8];
} VisPageMedienVerbrauch_TYP;

typedef struct VisPageAnalogFuellstand_TYP
{
} VisPageAnalogFuellstand_TYP;

typedef struct VisPageTempCtrlZone_TYP
{	struct BrbVc4Numeric_TYP numActTemp;
	struct BrbVc4Numeric_TYP numSetTemp;
} VisPageTempCtrlZone_TYP;

typedef struct VisPageTempCtrl_TYP
{	unsigned short nSelectedTankIndex;
	struct BrbVc4Button_TYP btnHeatingStartStop;
	struct BrbVc4Numeric_TYP numActTempTank;
	struct BrbVc4Numeric_TYP numSetTempTank;
	struct BrbVc4Button_TYP btnStandbyStartStop;
	struct BrbVc4Numeric_TYP numStandbyTemp;
	unsigned short nSelectedHeadIndex;
	struct BrbVc4Checkbox_TYP chkHeatingZonesEnable;
	struct VisPageTempCtrlZone_TYP Zone[3];
	struct BrbVc4Numeric_TYP numRelatedTank;
} VisPageTempCtrl_TYP;

typedef struct Vis_TYP
{	struct BrbVc4General_TYP General;
	struct VisGeneralOwn_TYP GeneralOwn;
	struct BrbVc4PageHandling_TYP PageHandling;
	struct BrbVc4ScreenSaver_TYP ScreenSaver;
	struct BrbVc4Scrollbar_TYP Scrollbar;
	struct BrbVc4TabCtrl_TYP tcTabCtrl;
	struct VisHeads_TYP Heads;
	struct VisTanks_TYP Tanks;
	struct VisHeader_TYP Header;
	struct VisNavigation_TYP Navigation;
	struct VisToolbar_TYP Toolbar;
	struct VisFooter_TYP Footer;
	struct VisMenu_TYP Menu;
	struct VisPageScreensaver_TYP PageScreensaver;
	struct VisPageDialogMoveAxes_TYP PageDialogMoveAxes;
	struct VisPageDialogStartHeating_TYP PageDialogStartHeating;
	struct VisPageMain_TYP PageMain;
	struct VisPageHead_TYP PageHead;
	struct VisPageHeadCopy_TYP PageHeadCopy;
	struct VisPageHeadPar_TYP PageHeadPar;
	struct VisPageProduction_TYP PageProduction;
	struct VisPageProductionSave_TYP PageProductionSave;
	struct VisPageProductionDelete_TYP PageProductionDelete;
	struct VisPageConfiguration_TYP PageConfiguration;
	struct VisPageService_TYP PageService;
	struct VisPageUser_TYP PageUser;
	struct VisPageModuleConfig_TYP PageModuleConfig;
	struct VisPageEvents_TYP PageEvents;
	struct VisPageContact_TYP PageContact;
	struct VisPageOverview_TYP PageOverview;
	struct VisPageCleaning_TYP PageCleaning;
	struct VisPageSystemCleaning_TYP PageSystemCleaning;
	struct VisPagePressureControl_TYP PagePressureControl;
	struct VisPageMotorizedAdjustment_TYP PageMotorizedAdjustment;
	struct VisPageCamera_TYP PageCamera;
	struct VisPageTempCtrl_TYP PageTempCtrl;
	struct VisPageMedienVerbrauch_TYP PageMedienVerbrauch;
	struct VisPageAnalogFuellstand_TYP PageAnalogFuellstand;
} Vis_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Visu/Visu/Visu.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Tasks/Visu/Visu/Visu.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Tasks/System/G_System.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500276136_1_ */

