/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_62_
#define _BUR_1500273701_62_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nVIS_TANK_SHOW_INDEX_MAX 3U
 #define nVIS_HEAD_OVERV_SHOW_INDEX_MAX 5U
 #define nVIS_TOOLBAR_SHOW_INDEX_MAX 5U
 #define nVIS_TOOLBAR_SOURCE_INDEX_MAX 10U
 #define nVIS_TOOLBAR_SHOW_EMPTY_OFFSET 2U
 #define nVIS_HEAD_SWITCH_SHOW_INDEX_MAX 2U
 #define nVIS_FILE_LIST_SHOW_INDEX_MAX 7U
 #define nVIS_FILE_LIST_SOURCE_INDEX_MAX 100U
 #define nVIS_HEAD_MAIN_SHOW_INDEX_MAX 3U
 #define nVIS_MOT_ADJUST_JOG_TIMEOUT 10000U
#else
 _LOCAL_CONST unsigned short nVIS_TANK_SHOW_INDEX_MAX;
 _LOCAL_CONST unsigned short nVIS_HEAD_OVERV_SHOW_INDEX_MAX;
 _LOCAL_CONST unsigned short nVIS_TOOLBAR_SHOW_INDEX_MAX;
 _LOCAL_CONST unsigned short nVIS_TOOLBAR_SOURCE_INDEX_MAX;
 _LOCAL_CONST unsigned short nVIS_TOOLBAR_SHOW_EMPTY_OFFSET;
 _LOCAL_CONST unsigned short nVIS_HEAD_SWITCH_SHOW_INDEX_MAX;
 _LOCAL_CONST unsigned short nVIS_FILE_LIST_SHOW_INDEX_MAX;
 _LOCAL_CONST unsigned short nVIS_FILE_LIST_SOURCE_INDEX_MAX;
 _LOCAL_CONST unsigned short nVIS_HEAD_MAIN_SHOW_INDEX_MAX;
 _LOCAL_CONST unsigned short nVIS_MOT_ADJUST_JOG_TIMEOUT;
#endif


/* Variablen */
_BUR_LOCAL struct TON fbServiceLogout2Hours;
_BUR_LOCAL unsigned short nTankIndex;
_BUR_LOCAL unsigned short nAxisIndex;
_BUR_LOCAL unsigned short nPrintmarkIndex;
_BUR_LOCAL unsigned short nSwitchIndex;
_BUR_LOCAL unsigned short nEncoderIndex;
_BUR_LOCAL unsigned short nHeadIndex;
_BUR_LOCAL struct TON fbBlink400;
_BUR_LOCAL struct TON fbOperatingSeconds;
_BUR_LOCAL signed long nIndex;
_BUR_LOCAL struct Vis_TYP Vis;
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct VisuStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Visu/Visu/Visu.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_62_ */

