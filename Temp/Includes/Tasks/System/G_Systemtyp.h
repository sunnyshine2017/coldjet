/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_23_
#define _BUR_1500273701_23_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct SystemCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bGetEthSettings;
	plcbit bSetEthSettings;
} SystemCallerBox_TYP;

typedef struct SystemDirectBox_TYP
{	plcbit bGetVersions;
	plcbit bSetSystemTime;
} SystemDirectBox_TYP;

typedef struct SystemParEthInterface_TYP
{	plcstring sIpAdress[17];
	plcstring sSubnetMask[17];
} SystemParEthInterface_TYP;

typedef struct SystemPar_TYP
{	struct DTStructure dtSystemTime;
	struct SystemParEthInterface_TYP EthInterface[3];
} SystemPar_TYP;

typedef struct SystemStateEthInterface_TYP
{	plcbit bExists;
	plcstring sDevice[65];
	unsigned short nVisDescriptionIndex;
	plcbit bChangeable;
} SystemStateEthInterface_TYP;

typedef struct SystemState_TYP
{	signed long nSystemTime;
	struct DTStructure dtSystemTime;
	plcstring sProjectVersion[21];
	plcstring sProjectTimestamp[25];
	plcstring sArVersion[21];
	plcstring sVcVersion[21];
	plcstring sMcVersion[21];
	struct SystemStateEthInterface_TYP EthInterface[3];
	plcbit bGettingOrSettingEth;
} SystemState_TYP;

typedef struct System_TYP
{	struct SystemCallerBox_TYP CallerBox;
	struct SystemDirectBox_TYP DirectBox;
	struct SystemPar_TYP Par;
	struct SystemState_TYP State;
} System_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/System/G_System.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Tasks/System/G_System.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_23_ */

