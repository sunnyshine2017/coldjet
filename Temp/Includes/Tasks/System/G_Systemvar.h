/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_24_
#define _BUR_1500273701_24_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nSYSTEM_ETH_INTERFACE_INDEX_MAX 2U
 #define nSYSTEM_VERSION_CHAR_MAX 20U
#else
 _GLOBAL_CONST unsigned short nSYSTEM_ETH_INTERFACE_INDEX_MAX;
 _GLOBAL_CONST unsigned short nSYSTEM_VERSION_CHAR_MAX;
#endif


/* Variablen */
_GLOBAL struct System_TYP gSystem;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/System/G_System.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_24_ */

