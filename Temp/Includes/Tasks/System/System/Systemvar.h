/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_58_
#define _BUR_1500273701_58_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variablen */
_BUR_LOCAL plcbit bEventSetHmiPanelNotConnected;
_BUR_LOCAL struct AsRfbExt_typ RfbExtLib;
_BUR_LOCAL unsigned long nDataAge;
_BUR_LOCAL unsigned short nStatusRfbExtInit;
_BUR_LOCAL unsigned short nStatusRfbExtConnect;
_BUR_LOCAL unsigned short nStatusRfbExtDataAge;
_BUR_LOCAL struct CfgSetIPAddr fbCfgSetIPAddr;
_BUR_LOCAL struct CfgSetSubnetMask fbCfgSetSubnetMask;
_BUR_LOCAL unsigned short nStatusCfgClearNV;
_BUR_LOCAL struct CfgGetIPAddr fbCfgGetIPAddr;
_BUR_LOCAL struct CfgGetSubnetMask fbCfgGetSubnetMask;
_BUR_LOCAL signed short nEthInterfaceIndex;
_BUR_LOCAL struct DTStructureSetTime fbDTStructureSetTime;
_BUR_LOCAL struct DTStructureGetTime fbDTStructureGetTime;
_BUR_LOCAL unsigned short nGetVersionStatus;
_BUR_LOCAL struct MoVerStruc_typ McInfo;
_BUR_LOCAL struct MoVerStruc_typ VcInfo;
_BUR_LOCAL struct TARGETInfo fbTargetInfo;
_BUR_LOCAL struct MoVerStruc_typ ProjectInfo;
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct SystemStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/System/System/System.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/brsystem/brsystem.fun\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_58_ */

