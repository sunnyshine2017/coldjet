/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_57_
#define _BUR_1500273701_57_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_GET_ETH_SETTINGS = 200,
	eSTEP_GES_NEXT_INTERFACE,
	eSTEP_GES_SUBNET_MASK,
	eSTEP_GES_IP_ADRESS,
	eSTEP_GES_FINISHED,
	eSTEP_SET_ETH_SETTINGS = 300,
	eSTEP_SES_CLEAR_NV,
	eSTEP_SES_NEXT_INTERFACE,
	eSTEP_SES_IP_ADRESS,
	eSTEP_SES_SUBNET_MASK,
	eSTEP_SES_FINISHED
} Steps_ENUM;

typedef struct SystemStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} SystemStep_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/System/System/System.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_57_ */

