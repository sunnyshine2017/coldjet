/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_55_
#define _BUR_1500273701_55_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_CMD1 = 200,
	eSTEP_CMD1_FINISHED
} Steps_ENUM;

typedef struct EventHandlingStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} EventHandlingStep_TYP;

typedef struct EventTestErrors_TYP
{	plcbit bTestError;
	plcbit bEmergencyStop;
	plcbit bLoadingParameter;
} EventTestErrors_TYP;

typedef struct EventTestWarnings_TYP
{	plcbit bTestWarning;
} EventTestWarnings_TYP;

typedef struct EventTestInfos_TYP
{	plcbit bTestInfo;
	plcbit bParameterLoaded;
	plcbit bParameterSaved;
	plcbit bServiceLoggedIn;
	plcbit bServiceLoggedOut;
} EventTestInfos_TYP;

typedef struct EventTest_TYP
{	signed long nCount;
	struct EventTestErrors_TYP Errors;
	struct EventTestWarnings_TYP Warnings;
	struct EventTestInfos_TYP Infos;
} EventTest_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/EventHandling/EventHandling/EventHandling.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_55_ */

