/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_56_
#define _BUR_1500273701_56_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define tSTATE_DELAY_TIME 200
#else
 _LOCAL_CONST plctime tSTATE_DELAY_TIME;
#endif


/* Variablen */
_BUR_LOCAL unsigned long nTestCount;
_BUR_LOCAL unsigned short nInfoCount;
_BUR_LOCAL unsigned short nWarningCount;
_BUR_LOCAL unsigned short nErrorCount;
_BUR_LOCAL plctime tClock;
_BUR_LOCAL struct EventTest_TYP EventTest;
_BUR_LOCAL unsigned short nEventId;
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct EventHandlingStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/EventHandling/EventHandling/EventHandling.var\\\" scope \\\"local\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_56_ */

