/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_53_
#define _BUR_1500273701_53_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_CHECK_PAR_DIR = 101,
	eSTEP_CREATE_PAR_DIR = 102,
	eSTEP_CREATE_MPAR_DIR = 103,
	eSTEP_CREATE_PRODUCT_PAR_DIR = 104,
	eSTEP_CHECK_PRODUCT_PAR = 110,
	eSTEP_GET_PRODUCT_PAR = 111,
	eSTEP_LOAD_PRODUCT_PAR = 200,
	eSTEP_SAVE_PRODUCT_PAR = 201,
	eSTEP_LOAD_M_PAR = 202,
	eSTEP_SAVE_M_PAR = 203,
	eSTEP_DELETE_PRODUCT_PAR = 204,
	eSTEP_CHECK_AXES = 300,
	eSTEP_INIT_ALL_AX_LIMITS,
	eSTEP_CHECK_AXES_WAIT,
	eSTEP_CHECK_HEATING = 310,
	eSTEP_CHECK_HEATING_WAIT
} Steps_ENUM;

typedef struct ParHandlingStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} ParHandlingStep_TYP;

typedef struct DirCheck_TYP
{	struct DirInfo fbDirInfo;
	struct DirCreate fbDirCreate;
	struct DirRead fbDirRead;
	struct fiDIR_READ_DATA ParDirData;
} DirCheck_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/ParHandling/ParHandling/ParHandling.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_53_ */

