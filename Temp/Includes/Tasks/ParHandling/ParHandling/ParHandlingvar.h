/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_54_
#define _BUR_1500273701_54_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define sVAR_NAME_M_PAR "gPar.MachinePar"
 #define sVAR_NAME_PRODUCT_PAR "gPar"
 #define sDIR_PRODUCT_PAR "Parameter/Products"
 #define sDIR_MPAR "Parameter/MPar"
 #define sDIR_PARAMETER "Parameter"
 #define sDEV_USER "DevUser"
 #define sM_PAR_FILE_NAME "MPar.txt"
 #define sDEV_M_PAR "DevMPar"
#else
 _LOCAL_CONST plcstring sVAR_NAME_M_PAR[33];
 _LOCAL_CONST plcstring sVAR_NAME_PRODUCT_PAR[33];
 _LOCAL_CONST plcstring sDIR_PRODUCT_PAR[33];
 _LOCAL_CONST plcstring sDIR_MPAR[33];
 _LOCAL_CONST plcstring sDIR_PARAMETER[33];
 _LOCAL_CONST plcstring sDEV_USER[33];
 _LOCAL_CONST plcstring sM_PAR_FILE_NAME[33];
 _LOCAL_CONST plcstring sDEV_M_PAR[33];
#endif


/* Variablen */
_BUR_LOCAL unsigned char nCyclicCnt;
_BUR_LOCAL signed long nMemCmpResult;
_BUR_LOCAL struct FileDelete fbDelFile;
_BUR_LOCAL struct BrbSaveVarAscii fbSaveVarAscii;
_BUR_LOCAL struct BrbLoadVarAscii fbLoadVarAscii;
_BUR_LOCAL struct Par_TYP ParamterCopy;
_BUR_LOCAL struct DirCheck_TYP DirCheck;
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct ParHandlingStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/ParHandling/ParHandling/ParHandling.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/BrbLib/BrbLib.fun\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_54_ */

