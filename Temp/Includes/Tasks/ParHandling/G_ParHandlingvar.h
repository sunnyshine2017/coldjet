/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_20_
#define _BUR_1500273701_20_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define sDEV_PRODUCT_PAR "DevProductPar"
#else
 _GLOBAL_CONST plcstring sDEV_PRODUCT_PAR[33];
#endif


/* Variablen */
_GLOBAL struct ParHandling_TYP gParHandling;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/ParHandling/G_ParHandling.var\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_20_ */

