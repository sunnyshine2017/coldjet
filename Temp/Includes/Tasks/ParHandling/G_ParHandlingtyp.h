/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_19_
#define _BUR_1500273701_19_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct ParHandlingCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bCheckDir;
	plcbit bLoadProductPar;
	plcbit bLoadMPar;
	plcbit bSaveProductPar;
	plcbit bSaveMPar;
	plcbit bDeleteProductPar;
} ParHandlingCallerBox_TYP;

typedef struct ParHandlingDirectBox_TYP
{	plcbit bDummy;
} ParHandlingDirectBox_TYP;

typedef struct ParHandlingPar_TYP
{	plcstring sFileName[260];
} ParHandlingPar_TYP;

typedef struct ParHandlingState_TYP
{	plcbit bInitDone;
	plcbit bMParValid;
	plcbit bPParValid;
	plcbit bParChanged;
	plcbit bAction;
	plcbit bParFileLoading;
} ParHandlingState_TYP;

typedef struct ParHandling_TYP
{	struct ParHandlingCallerBox_TYP CallerBox;
	struct ParHandlingDirectBox_TYP DirectBox;
	struct ParHandlingPar_TYP Par;
	struct ParHandlingState_TYP State;
} ParHandling_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/ParHandling/G_ParHandling.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_19_ */

