/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_3_
#define _BUR_1500273701_3_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct TemplateCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bDummy;
} TemplateCallerBox_TYP;

typedef struct TemplateDirectBox_TYP
{	plcbit bDummy;
} TemplateDirectBox_TYP;

typedef struct TemplatePar_TYP
{	unsigned short nDummy;
} TemplatePar_TYP;

typedef struct TemplateState_TYP
{	signed long nDummy;
} TemplateState_TYP;

typedef struct Template_TYP
{	struct TemplateCallerBox_TYP CallerBox;
	struct TemplateDirectBox_TYP DirectBox;
	struct TemplatePar_TYP Par;
	struct TemplateState_TYP State;
} Template_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Template/G_Template.typ\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_3_ */

