/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_35_
#define _BUR_1500273701_35_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct TempCtrlCallerBoxHeatingSys_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bStartHeating;
	plcbit bStartSystemStandby;
	plcbit bStopSystemStandby;
} TempCtrlCallerBoxHeatingSys_TYP;

typedef struct TempCtrlCallerBox_TYP
{	struct TempCtrlCallerBoxHeatingSys_TYP HeatingSystem[2];
} TempCtrlCallerBox_TYP;

typedef struct TempCtrlDirectBoxHeatingSys_TYP
{	plcbit bStopHeating;
} TempCtrlDirectBoxHeatingSys_TYP;

typedef struct TempCtrlDirectBox_TYP
{	struct TempCtrlDirectBoxHeatingSys_TYP HeatingSystem[2];
	plcbit bReset;
} TempCtrlDirectBox_TYP;

typedef struct TempCtrlPar_TYP
{	unsigned short nDummy;
} TempCtrlPar_TYP;

typedef struct TempCtrlStateHeatingSystem_TYP
{	plcbit bHeatingOn;
	plcbit bStandbyOn;
	plcbit bTempInToleranceWindow;
	plcbit bTempInWarningWindow;
	plcbit bErrorHeatingSystem;
	plcbit bHeatingMainContactorClosed;
	plcbit bAutomaticPossible;
	plcbit bOneOrMoreBonDevProdutionEnabled;
} TempCtrlStateHeatingSystem_TYP;

typedef struct TempCtrlStateTempZone_TYP
{	float rActTemp;
	plcbit bHeatingOn;
	plcbit bTempInToleranceWindow;
	plcbit bTempInWarningWindow;
	plcbit bErrorTempZone;
	plcbit bOutputHeating;
	plcbit bTemperatureChanged;
	plcbit bStandbyOn;
} TempCtrlStateTempZone_TYP;

typedef struct TempCtrlStateBonDev_TYP
{	struct TempCtrlStateTempZone_TYP TempZone[3];
	plcbit bHeatingOn;
	plcbit bAutomaticPossible;
	plcbit bTempInToleranceWindow;
	plcbit bTempInWarningWindow;
	plcbit bErrorBonDev;
} TempCtrlStateBonDev_TYP;

typedef struct TempCtrlState_TYP
{	struct TempCtrlStateHeatingSystem_TYP HeatingSystem[2];
	struct TempCtrlStateTempZone_TYP Tank[2];
	struct TempCtrlStateBonDev_TYP BonDev[12];
	plcbit bInitDone;
	plcbit bError;
	plcbit bAutomaticPossible;
} TempCtrlState_TYP;

typedef struct TempCtrl_TYP
{	struct TempCtrlCallerBox_TYP CallerBox;
	struct TempCtrlDirectBox_TYP DirectBox;
	struct TempCtrlPar_TYP Par;
	struct TempCtrlState_TYP State;
} TempCtrl_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/TempCtrl/G_TempCtrl.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_35_ */

