/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273702_71_
#define _BUR_1500273702_71_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum HeatingSystemSteps_ENUM
{	eSTEP_HS_INIT = 0,
	eSTEP_HS_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_HS_INIT_FINISHED,
	eSTEP_HS_WAIT_FOR_COMMAND = 100,
	eSTEP_HS_START_HEATING = 200,
	eSTEP_HS_HEATING_TANK_STARTED,
	eSTEP_HS_START_HEATING_BON_DEV,
	eSTEP_HS_HEATING_BON_DEV_STARTED,
	eSTEP_HS_START_STAND_BY,
	eSTEP_HS_ERR_STEP = 300
} HeatingSystemSteps_ENUM;

typedef enum TempZoneSteps_ENUM
{	eSTEP_TZ_INIT = 0,
	eSTEP_TZ_INIT_WAIT_FOR_PAR_VALID = 1,
	eSTEP_TZ_INIT_FINISHED,
	eSTEP_TZ_WAITING_FOR_COMMAND = 100,
	eSTEP_TZ_START_HEATING = 200,
	eSTEP_TZ_STOP_HEATING,
	eSTEP_TZ_UPDATE_PARAMETER,
	eSTEP_TZ_START_STEP_TUNING,
	eSTEP_TZ_WAIT_HEATING_STARTED,
	eSTEP_TZ_WAIT_TUNING_DONE,
	eSTEP_TZ_TUNING_DONE,
	eSTEP_TZ_ERR_STEP = 300,
	eSTEP_TZ_ERR_RESET
} TempZoneSteps_ENUM;

typedef struct HeatingSystemStep_TYP
{	plcstring sStepText[51];
	enum HeatingSystemSteps_ENUM eStepNr;
	plcbit bInitDone;
} HeatingSystemStep_TYP;

typedef struct TempCtrlStep_TYP
{	plcstring sStepText[51];
	enum HeatingSystemSteps_ENUM eStepNr;
	plcbit bInitDone;
} TempCtrlStep_TYP;

typedef struct TempZoneStep_TYP
{	plcstring sStepText[51];
	enum TempZoneSteps_ENUM eStepNr;
	plcbit bInitDone;
} TempZoneStep_TYP;

typedef struct IO_InputBonDev_TYP
{	signed short nActTemp_Zone[3];
	plcbit bStatusHeatingDigitalOutput[3];
} IO_InputBonDev_TYP;

typedef struct IO_InputTank_TYP
{	signed short nActTemp;
	plcbit bStatusHeatingDigitalOutput;
} IO_InputTank_TYP;

typedef struct IO_Input_TYP
{	struct IO_InputTank_TYP Tank[2];
	struct IO_InputBonDev_TYP BonDev[12];
} IO_Input_TYP;

typedef struct IO_OutputHeatingSystem_TYP
{	plcbit bHeatingSystemReady;
	plcbit bHeatingMainContactorClosed;
} IO_OutputHeatingSystem_TYP;

typedef struct IO_OutputTank_TYP
{	plcbit bHeatingOn;
} IO_OutputTank_TYP;

typedef struct IO_OutputBonDev_TYP
{	plcbit bHeatingOn[3];
} IO_OutputBonDev_TYP;

typedef struct IO_Output_TYP
{	struct IO_OutputHeatingSystem_TYP HeatingSystem[2];
	struct IO_OutputTank_TYP Tank[2];
	struct IO_OutputBonDev_TYP BonDev[12];
} IO_Output_TYP;

typedef struct IO_TYP
{	struct IO_Input_TYP Input;
	struct IO_Output_TYP Output;
} IO_TYP;

typedef struct TempCtrlFubs_TYP
{	struct MTBasicsPID PID;
	struct MTBasicsPWM PWM;
} TempCtrlFubs_TYP;

typedef struct TempCtrlTempZoneHeatingCtrl_TYP
{	struct MTBasicsPID PID;
	struct MTBasicsPWM PWM;
	struct MTBasicsStepTuning StepTuning;
} TempCtrlTempZoneHeatingCtrl_TYP;

typedef struct TempCtrlBonDevCmd_TYP
{	plcbit bStartHeating;
	plcbit bStopHeating;
	plcbit bUpdateParameter;
	plcbit bReset;
} TempCtrlBonDevCmd_TYP;

typedef struct TempCtrlTempZoneCmd_TYP
{	plcbit bStartHeating;
	plcbit bStopHeating;
	plcbit bUpdateParameter;
	plcbit bReset;
	plcbit bStartStepTuning;
	plcbit bApplyPIDParameters;
} TempCtrlTempZoneCmd_TYP;

typedef struct TempCtrlTempZonePar_TYP
{	float rSetTemp;
	unsigned char rStandbyTemp;
} TempCtrlTempZonePar_TYP;

typedef struct TempCtrlTempZoneSimulation_TYP
{	struct MTBasicsPT2 PT2;
	struct MTBasicsDT2 DT2;
	struct MTBasicsIntegrator Integrator;
	struct MTBasicsTimeDelay DeadTime;
} TempCtrlTempZoneSimulation_TYP;

typedef struct SetEvent_TYP
{	plcbit bActive;
	plcbit bEventIsSet;
} SetEvent_TYP;

typedef struct TempCtrlTempZoneErrors_TYP
{	struct SetEvent_TYP ErrPIDParameterInvalid;
	struct SetEvent_TYP ErrTempSensorBrokenWire;
	struct SetEvent_TYP ErrTempSensorMinValueReached;
	struct SetEvent_TYP ErrTempSensorInvalidValue;
	struct SetEvent_TYP ErrExternalHeatingSignal;
	struct SetEvent_TYP ErrActTempLeftToleranceWindow;
	struct SetEvent_TYP ErrHeatingShorted;
	struct SetEvent_TYP ErrOverTemperature;
} TempCtrlTempZoneErrors_TYP;

typedef struct TempCtrlTempZoneWarnings_TYP
{	struct SetEvent_TYP WarnUpdateAbortedInvalidPIDPar;
	struct SetEvent_TYP WarnPIDParameterInvalid;
	struct SetEvent_TYP WarnPIDInteral;
} TempCtrlTempZoneWarnings_TYP;

typedef struct TempCtrlTempZoneStatusIDs_TYP
{	signed long nPID_StatusID;
	signed long nPWM_StatusID;
	signed long nStepTuning_StatusID;
} TempCtrlTempZoneStatusIDs_TYP;

typedef struct TempCtrlTempZoneNegEdge_TYP
{	struct F_TRIG TempSettled;
	struct F_TRIG TempInTolerance;
} TempCtrlTempZoneNegEdge_TYP;

typedef struct TempInTolAndSettledInternal_TYP
{	unsigned short nStep;
	struct F_TRIG NegEdgeTempInWarningWindow;
	struct TON TimeTempSettled;
} TempInTolAndSettledInternal_TYP;

typedef struct TempInToleranceAndSettled_TYP
{	plcbit bTempInToleranceWindow;
	plcbit bTempInWarningWindow;
	plctime nSettlingTime;
	plcbit bTempInToleranceAndSettled;
	struct TempInTolAndSettledInternal_TYP Internal;
} TempInToleranceAndSettled_TYP;

typedef struct TempCtrlTempZone_TYP
{	struct TempCtrlTempZoneCmd_TYP Cmd;
	struct TempCtrlTempZonePar_TYP Par;
	struct TempZoneStep_TYP Step;
	struct BrbStepHandling_TYP StepHandling;
	struct TempCtrlTempZoneHeatingCtrl_TYP HeatingCtrl;
	struct TempCtrlTempZoneSimulation_TYP Simulation;
	struct TempCtrlTempZoneWarnings_TYP Warnings;
	struct TempCtrlTempZoneErrors_TYP Errors;
	struct TempCtrlTempZoneStatusIDs_TYP StatusIDs;
	float rTemperatureHeatingSwitchedOff;
	struct TempInToleranceAndSettled_TYP TempInToleranceAndSettled;
	struct TempCtrlTempZoneNegEdge_TYP NegEdge;
} TempCtrlTempZone_TYP;

typedef struct TempCtrlBonDev_TYP
{	struct TempCtrlBonDevCmd_TYP Cmd;
	struct TempCtrlTempZone_TYP TempZone[3];
} TempCtrlBonDev_TYP;

typedef struct TempCtrlHeatingSystemWarn_TYP
{	struct SetEvent_TYP WarnHeatingSystemCriticalTemp;
} TempCtrlHeatingSystemWarn_TYP;

typedef struct TempCtrlHeatingSystemInfos_TYP
{	struct SetEvent_TYP InfoHeatingSystemWarmUp;
	struct SetEvent_TYP InfoHeatingSystemInStandby;
} TempCtrlHeatingSystemInfos_TYP;

typedef struct TempCtrlHeatingSystem_TYP
{	struct HeatingSystemStep_TYP Step;
	struct BrbStepHandling_TYP StepHandling;
	struct TempCtrlHeatingSystemInfos_TYP Infos;
	struct TempCtrlHeatingSystemWarn_TYP Warnings;
} TempCtrlHeatingSystem_TYP;

typedef struct NegEdge_TYP
{	struct F_TRIG MachineModuleTempCtrlEnabled;
	struct F_TRIG TempCtrlTankExist[2];
	struct F_TRIG TempCtrlBonDevExist[12];
	struct F_TRIG TempCtrlZoneExist[12][3];
	struct F_TRIG HeatingSystemError[2];
	struct F_TRIG BonDevEnable[12];
	struct F_TRIG TempCtrlTankTempSettled[2];
	struct F_TRIG TempCtrlTankTempInTolerance[2];
	struct F_TRIG TempCtrlZoneTempSettled[12][3];
	struct F_TRIG TempCtrlZoneTempInTolerance[12][3];
} NegEdge_TYP;

typedef struct PosEdge_TYP
{	struct R_TRIG HeatingSystemError[2];
	struct R_TRIG BonDevEnable[12];
} PosEdge_TYP;

typedef struct Infos_TYP
{	struct SetEvent_TYP InfoAutomaticNotPossible;
	struct SetEvent_TYP InfoTankSetTempToLow;
	struct SetEvent_TYP InfoBondDevSetTempToLow;
} Infos_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/TempCtrl/TempCtrl/TempCtrl.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273702_71_ */

