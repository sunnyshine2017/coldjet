/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273702_72_
#define _BUR_1500273702_72_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nERR_NULL_POINTER 2U
 #define nERR_X20AT_TEMP_SENSORS 1U
#else
 _LOCAL_CONST unsigned short nERR_NULL_POINTER;
 _LOCAL_CONST unsigned short nERR_X20AT_TEMP_SENSORS;
#endif


/* Variablen */
_BUR_LOCAL struct Infos_TYP Infos;
_BUR_LOCAL struct TempInToleranceAndSettled_TYP BonDevAutomaticPossible[12];
_BUR_LOCAL struct TempInToleranceAndSettled_TYP TankAutomaticPossible[2];
_BUR_LOCAL plcstring sTempZoneName[3][81];
_BUR_LOCAL plcbit bBonDevExistInThisHeatingSystem;
_BUR_LOCAL plcbit bAllBonDevHeatingOn;
_BUR_LOCAL struct PosEdge_TYP PosEdge;
_BUR_LOCAL struct NegEdge_TYP NegEdge;
_BUR_LOCAL unsigned char nTempZoneIdx;
_BUR_LOCAL unsigned char nBonDevIdx;
_BUR_LOCAL unsigned char nTankIdx;
_BUR_LOCAL unsigned short StatusCalcTempFromAT4222ErrCheck;
_BUR_LOCAL struct IO_TYP IO;
_BUR_LOCAL struct TempCtrlBonDev_TYP BonDev[12];
_BUR_LOCAL struct TempCtrlTempZone_TYP Tank[2];
_BUR_LOCAL struct TempCtrlHeatingSystem_TYP HeatingSystem[2];





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/TempCtrl/TempCtrl/TempCtrl.var\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273702_72_ */

