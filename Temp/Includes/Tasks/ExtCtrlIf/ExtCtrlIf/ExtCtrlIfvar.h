/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273702_68_
#define _BUR_1500273702_68_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variablen */
_BUR_LOCAL unsigned long nIntervarlCleaningTime;
_BUR_LOCAL struct TON TON_IntervalCleaningTime;
_BUR_LOCAL struct R_TRIG PosEdgeMakuTurnOutClosed;
_BUR_LOCAL struct IO_TYP IO;
_BUR_LOCAL unsigned char nDevIndex;
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct BrbStepHandling_TYP CleaningHopperStepHandling;
_BUR_LOCAL struct BrbStepHandling_TYP CleaningCollectorStepHandling;
_BUR_LOCAL struct ExtCtrlIfStep_TYP Step;
_BUR_LOCAL struct CleaningCollectorStep_TYP CleaningCollectorStep;
_BUR_LOCAL struct CleaningHopperStep_TYP CleaningHopperStep;
_BUR_LOCAL struct F_TRIG PosEdgeAutomaticOff;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/ExtCtrlIf/ExtCtrlIf/ExtCtrlIf.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273702_68_ */

