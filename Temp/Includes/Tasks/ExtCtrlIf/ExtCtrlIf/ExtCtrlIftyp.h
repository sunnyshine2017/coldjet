/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273702_67_
#define _BUR_1500273702_67_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum CleaningCollectorSteps_ENUM
{	eSTEP_WAIT_CMD_CLEAN_COLLECTOR = 100,
	eSTEP_MOVE_HEAD_TO_CLEAN_STATION,
	eSTEP_WAIT_HEAD_TO_CLEAN_STATION,
	eSTEP_START_CLEAN_COLLECTOR,
	eSTEP_CLEAN_COLLECTOR_DONE,
	eSTEP_WAIT_CMD_FILL_COLLECTOR,
	eSTEP_WAIT_HOPPER_CLEANED,
	eSTEP_START_FILL_COLLECTOR,
	eSTEP_FILL_COLLECTOR_DONE,
	eSTEP_MOVE_HEAD_TO_WORK_POSITION,
	eSTEP_WAIT_HEAD_IN_WORK_POSITION,
	eSTEP_WAIT_MAKU_TURN_OUT_CLOSED,
	eSTEP_RESET_COLLECTOR_CLEANING = 200
} CleaningCollectorSteps_ENUM;

typedef enum CleaningHopperSteps_ENUM
{	eSTEP_WAIT_CMD_CLEAN_HOPPER = 100,
	eSTEP_START_CLEAN_HOPPER,
	eSTEP_CLEAN_HOPPER_DONE,
	eSTEP_WAIT_CMD_FILL_HOPPER,
	eSTEP_WAIT_COLLECTOR_CLEANED,
	eSTEP_START_FILL_HOPPER,
	eSTEP_FILL_HOPPER_DONE,
	eSTEP_RESET_HOPPER_CLEANING = 200
} CleaningHopperSteps_ENUM;

typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_CMD1 = 200,
	eSTEP_CMD1_FINISHED
} Steps_ENUM;

typedef struct CleaningCollectorStep_TYP
{	plcstring sStepText[51];
	enum CleaningCollectorSteps_ENUM eStepNr;
	plcbit bInitDone;
} CleaningCollectorStep_TYP;

typedef struct CleaningHopperStep_TYP
{	plcstring sStepText[51];
	enum CleaningHopperSteps_ENUM eStepNr;
	plcbit bInitDone;
} CleaningHopperStep_TYP;

typedef struct ExtCtrlIfStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} ExtCtrlIfStep_TYP;

typedef struct IoInput_TYP
{	plcbit bBondingAllowed[12];
	plcbit bCleanBondingDevices;
	plcbit bFillHopperBondigDevice;
	plcbit bFillCollectorBondigDevice;
	plcbit bMakuTurnOutClosed;
} IoInput_TYP;

typedef struct IoOutput_TYP
{	plcbit bGeneralError;
} IoOutput_TYP;

typedef struct IO_TYP
{	struct IoOutput_TYP Output;
	struct IoInput_TYP Input;
} IO_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/ExtCtrlIf/ExtCtrlIf/ExtCtrlIf.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273702_67_ */

