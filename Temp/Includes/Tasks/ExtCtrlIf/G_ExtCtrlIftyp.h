/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_31_
#define _BUR_1500273701_31_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct ExtCtrlIfCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bDummy;
} ExtCtrlIfCallerBox_TYP;

typedef struct ExtCtrlIfDirectBox_TYP
{	plcbit bStartIntervalCleaning;
} ExtCtrlIfDirectBox_TYP;

typedef struct ExtCtrlIfPar_TYP
{	unsigned short nDummy;
} ExtCtrlIfPar_TYP;

typedef struct ExtCtrlIfState_TYP
{	plcbit bBondingNotAllowed[12];
	plcbit bCleanBondingDevices;
	plcbit bFillHopperBondigDevice;
	plcbit bFillCollectorBondigDevice;
	plcbit bMakuTurnOutClosed;
	plcbit bIntervalCleaningActive;
	plcbit bCleaningActiveHopperBonDev;
	plcbit bCleaningActiveCollectorBonDev;
	plcbit bBondingDeviceCleaned[12];
} ExtCtrlIfState_TYP;

typedef struct ExtCtrlIf_TYP
{	struct ExtCtrlIfCallerBox_TYP CallerBox;
	struct ExtCtrlIfDirectBox_TYP DirectBox;
	struct ExtCtrlIfPar_TYP Par;
	struct ExtCtrlIfState_TYP State;
} ExtCtrlIf_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/ExtCtrlIf/G_ExtCtrlIf.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_31_ */

