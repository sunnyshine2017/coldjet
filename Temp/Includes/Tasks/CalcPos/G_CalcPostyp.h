/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_33_
#define _BUR_1500273701_33_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct CalcPosCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bDummy;
} CalcPosCallerBox_TYP;

typedef struct CalcPosDirectBox_TYP
{	plcbit bDummy;
} CalcPosDirectBox_TYP;

typedef struct CalcPosPar_TYP
{	unsigned short nDummy;
} CalcPosPar_TYP;

typedef struct TrackState_TYP
{	float rCalcTrackLen;
	plcbit bInvalidTrackLen;
} TrackState_TYP;

typedef struct HeadState_TYP
{	struct TrackState_TYP TrackState[4];
	signed char nInvalidParIdx[4];
	plcbit bInvalidProductLen;
	plcbit bHeadError;
} HeadState_TYP;

typedef struct CalcPosState_TYP
{	struct HeadState_TYP HeadState[12];
	plcbit bInvalidParEntry;
} CalcPosState_TYP;

typedef struct CalcPos_TYP
{	struct CalcPosCallerBox_TYP CallerBox;
	struct CalcPosDirectBox_TYP DirectBox;
	struct CalcPosPar_TYP Par;
	struct CalcPosState_TYP State;
} CalcPos_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/CalcPos/G_CalcPos.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_33_ */

