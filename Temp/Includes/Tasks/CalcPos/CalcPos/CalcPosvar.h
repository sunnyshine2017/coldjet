/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273702_70_
#define _BUR_1500273702_70_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variablen */
_BUR_LOCAL plcbit bEdgePos;
_BUR_LOCAL plcbit bStartCalc;
_BUR_LOCAL unsigned char nDevIndex;
_BUR_LOCAL unsigned char nPmIndex;
_BUR_LOCAL unsigned char nSwitchIndex;
_BUR_LOCAL float rLastSwitchPos[12][4];
_BUR_LOCAL struct RF_TRIG fbEdge[12];
_BUR_LOCAL struct InvalidPar_TYP InvalidPar[12];
_BUR_LOCAL struct SwitchesIdx_TYP SwitchesBuffer[12];
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct CalcPosStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/CalcPos/CalcPos/CalcPos.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273702_70_ */

