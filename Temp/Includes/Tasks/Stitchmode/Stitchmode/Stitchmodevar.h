/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_64_
#define _BUR_1500273701_64_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nFACTOR_MM_TO_MIKRO 1000U
 #define nREACTION_CYCLE_TIME 5U
 #define nREACTION_CYCLE 1U
 #define nFACTOR_TIME_TO_CYCLE 100000U
#else
 _LOCAL_CONST unsigned long nFACTOR_MM_TO_MIKRO;
 _LOCAL_CONST unsigned long nREACTION_CYCLE_TIME;
 _LOCAL_CONST unsigned long nREACTION_CYCLE;
 _LOCAL_CONST unsigned long nFACTOR_TIME_TO_CYCLE;
#endif


/* Variablen */
_BUR_LOCAL unsigned char nWriteAsyncPVARtoX2X_ErrCnt[12];
_BUR_LOCAL signed long nStitchSize_old[12];
_BUR_LOCAL struct AsIOAccWrite WriteAsyncPVARtoRT_Module_X2X[12];
_BUR_LOCAL signed long nReactionCycleOnX2X_old[12];
_BUR_LOCAL float rOpenTimeLarge_old;
_BUR_LOCAL float rOpenTimeMedium_old;
_BUR_LOCAL float rOpenTimeSmall_old;
_BUR_LOCAL unsigned char nForIdx;
_BUR_LOCAL struct StitchTime_TYP StitchTime[12];
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct StitchmodeStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Stitchmode/Stitchmode/Stitchmode.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIOAcc/AsIOAcc.fun\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_64_ */

