/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_63_
#define _BUR_1500273701_63_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_CMD1 = 200,
	eSTEP_CMD1_FINISHED
} Steps_ENUM;

typedef struct StitchmodeStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} StitchmodeStep_TYP;

typedef struct StitchTime_TYP
{	signed long nReactionCycleOnX2X;
	signed long nReactionCycleOn;
	signed long nReactionCycleOff;
} StitchTime_TYP;

typedef struct WriteAsyncDataToX2XModule_TYP
{	unsigned char AsIO;
	unsigned char New_Member1;
	unsigned char New_Member;
} WriteAsyncDataToX2XModule_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Stitchmode/Stitchmode/Stitchmode.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_63_ */

