/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_29_
#define _BUR_1500273701_29_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct StitchmodeCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bDummy;
} StitchmodeCallerBox_TYP;

typedef struct StitchmodeDirectBox_TYP
{	plcbit bDummy;
} StitchmodeDirectBox_TYP;

typedef struct StitchmodePar_TYP
{	unsigned short nDummy;
} StitchmodePar_TYP;

typedef struct StitchmodeState_TYP
{	plcbit bEnableStitchmode;
	unsigned char nStitchingActiveX2X[12];
	signed long nStitchingActive[12];
} StitchmodeState_TYP;

typedef struct Stitchmode_TYP
{	struct StitchmodeCallerBox_TYP CallerBox;
	struct StitchmodeDirectBox_TYP DirectBox;
	struct StitchmodePar_TYP Par;
	struct StitchmodeState_TYP State;
} Stitchmode_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/Stitchmode/G_Stitchmode.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_29_ */

