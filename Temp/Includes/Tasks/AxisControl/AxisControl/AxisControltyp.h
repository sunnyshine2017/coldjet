/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_49_
#define _BUR_1500273701_49_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum AxisControl_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_NO_AXIS_EXISTS,
	eSTEP_POWER_ON = 200,
	eSTEP_POWER_OFF,
	eSTEP_HOMING_ALL,
	eSTEP_HOMING_ALL_ACTIVE,
	eSTEP_INIT_ALL_AXES_LIMITS,
	eSTEP_MOVE_ALL_AXES_TO_WORK_POS,
	eSTEP_HOMING,
	eSTEP_STOP,
	eSTEP_MOVE_ABS,
	eSTEP_MOVE_ADD_POS,
	eSTEP_MOVE_ADD_NEG,
	eSTEP_JOG_POS,
	eSTEP_JOG_NEG
} AxisControl_ENUM;

typedef struct AxisControlStep_TYP
{	plcstring sStepText[51];
	enum AxisControl_ENUM eStepNr;
	plcbit bInitDone;
} AxisControlStep_TYP;

typedef struct Error_type
{	unsigned short nHorizontalErrId;
	plcbit bHorizontalErr;
	plcbit bHorizontalCtrlErr;
	unsigned short nVerticalErrId;
	plcbit bVerticalErr;
	plcbit bVerticalCtrlErr;
	plcbit bMsgDoneControllerV;
	plcbit bMsgDoneControllerH;
	plcbit bMsgDoneH;
	plcbit bMsgDoneV;
	plcbit bInfoDoneH;
	plcbit bInfoDoneV;
} Error_type;

typedef struct Homing_type
{	plcbit bHomingVertical;
	plcbit bHomingHorizontal;
} Homing_type;

typedef struct Move_Add_Type
{	plcbit Horizontal;
	plcbit Vertical;
} Move_Add_Type;

typedef struct AuxVar_type
{	plcbit bControllerOff;
	plcbit bJogPos;
	plcbit bJogPosActive;
	plcbit bJogNeg;
	plcbit bJogNegActive;
	plcbit bStop;
	plcbit bHoming;
	plcbit bMoveAbs;
	struct Homing_type HomingDone[12];
	struct Move_Add_Type Move_Add_Pos[12];
	struct Move_Add_Type Move_Add_Neg[12];
} AuxVar_type;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/AxisControl/AxisControl/AxisControl.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_49_ */

