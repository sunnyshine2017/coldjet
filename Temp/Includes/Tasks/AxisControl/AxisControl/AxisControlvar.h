/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_50_
#define _BUR_1500273701_50_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nDECELERATION_VER 10000.0f
 #define nDECELERATION_HOR 300000.0f
 #define nACCELERATION_VER 10000.0f
 #define nACCELERATION_HOR 300000.0f
 #define nINC_VERTICAL 5U
 #define nINC_HORIZONTAL 10U
 #define nTIMER_DELAY 70U
 #define rJOG_V_REDUCE 0.1f
#else
 _LOCAL_CONST float nDECELERATION_VER;
 _LOCAL_CONST float nDECELERATION_HOR;
 _LOCAL_CONST float nACCELERATION_VER;
 _LOCAL_CONST float nACCELERATION_HOR;
 _LOCAL_CONST unsigned short nINC_VERTICAL;
 _LOCAL_CONST unsigned short nINC_HORIZONTAL;
 _LOCAL_CONST unsigned long nTIMER_DELAY;
 _LOCAL_CONST float rJOG_V_REDUCE;
#endif


/* Variablen */
_BUR_LOCAL plcbit bOneOrMoreAxisIsEnabled;
_BUR_LOCAL unsigned char nHomingMode;
_BUR_LOCAL plcbit bAxisErrAck;
_BUR_LOCAL plcbit bAxisErrControllerAck;
_BUR_LOCAL plcbit bJogNegActive;
_BUR_LOCAL plcbit bJogPosActive;
_BUR_LOCAL plcbit bMoveAddNegActive;
_BUR_LOCAL plcbit bMoveAddPosActive;
_BUR_LOCAL unsigned char nBonDevIdx;
_BUR_LOCAL struct TON_10ms fbTon;
_BUR_LOCAL struct AuxVar_type AuxVar;
_BUR_LOCAL struct Error_type ErrId[12];
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct AxisControlStep_TYP Step;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/AxisControl/AxisControl/AxisControl.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_50_ */

