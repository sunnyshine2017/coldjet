/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_15_
#define _BUR_1500273701_15_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct HmiCaller_TYP
{	plcbit bStartAbsMove;
	plcbit bRefAxis;
} HmiCaller_TYP;

typedef struct HmiCallerAxis_TYP
{	struct HmiCaller_TYP Vertical[12];
	struct HmiCaller_TYP Horizontal[12];
	plcbit bRefAllAxes;
} HmiCallerAxis_TYP;

typedef struct AxisControlCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	struct HmiCallerAxis_TYP HmiCaller;
	plcbit bMoveAllAxesToWorkPosition;
	plcbit bInitAllAxesLimits;
} AxisControlCallerBox_TYP;

typedef struct HmiDirect_TYP
{	plcbit bStop;
	plcbit bJogPos;
	plcbit bJogNeg;
} HmiDirect_TYP;

typedef struct HmiDirectAxis_TYP
{	struct HmiDirect_TYP Vertical;
	struct HmiDirect_TYP Horizontal;
} HmiDirectAxis_TYP;

typedef struct AxisControlDirectBox_TYP
{	struct HmiDirectAxis_TYP HmiCmd[12];
	plcbit bPowerOn;
	plcbit bPowerOff;
} AxisControlDirectBox_TYP;

typedef struct Basic_parameter_TYP
{	float Position;
	float Distance;
	float Velocity;
	unsigned char Direction;
	float Acceleration;
	float Deceleration;
	float HomePosition;
	unsigned char HomeMode;
	float JogVelocity;
	float PositiveSwLimit;
	float NegativeSwLimit;
} Basic_parameter_TYP;

typedef struct Basic_parameter_Axis_TYP
{	struct Basic_parameter_TYP Vertical;
	struct Basic_parameter_TYP Horizontal;
} Basic_parameter_Axis_TYP;

typedef struct AbsPosAxis_TYP
{	float Vertical;
	float Horizontal;
} AbsPosAxis_TYP;

typedef struct HmiPar_TYP
{	struct AbsPosAxis_TYP AbsPos[12];
	unsigned char nRefAxisIdx;
} HmiPar_TYP;

typedef struct AxisControlPar_TYP
{	struct Basic_parameter_Axis_TYP AxisPar[12];
	struct HmiPar_TYP HmiPar;
} AxisControlPar_TYP;

typedef struct Basic_axisState_TYP
{	plcbit Disabled;
	plcbit StandStill;
	plcbit Homing;
	plcbit Stopping;
	plcbit DiscreteMotion;
	plcbit ContinuousMotion;
	plcbit SynchronizedMotion;
	plcbit ErrorStop;
} Basic_axisState_TYP;

typedef struct Basic_axisState_Axis_TYP
{	struct Basic_axisState_TYP Vertical;
	struct Basic_axisState_TYP Horizontal;
} Basic_axisState_Axis_TYP;

typedef struct Basic_status_TYP
{	unsigned short ErrorID;
	plcstring ErrorText[4][80];
	float ActPosition;
	float ActVelocity;
	plcbit InPosition;
	unsigned char DriveSimulationStatus;
	struct MC_DRIVESTATUS_TYP DriveStatus;
} Basic_status_TYP;

typedef struct Basic_status_Axis_TYP
{	struct Basic_status_TYP Vertical;
	struct Basic_status_TYP Horizontal;
} Basic_status_Axis_TYP;

typedef struct HmiState_TYP
{	float rActPosition;
} HmiState_TYP;

typedef struct HmiStateAxis_TYP
{	struct HmiState_TYP Vertical;
	struct HmiState_TYP Horizontal;
} HmiStateAxis_TYP;

typedef struct AxisControlState_TYP
{	plcbit bStepperSimulated;
	struct Basic_axisState_Axis_TYP AxisState[12];
	struct Basic_status_Axis_TYP AxisStatus[12];
	struct HmiStateAxis_TYP HmiState[12];
} AxisControlState_TYP;

typedef struct Basic_command_TYP
{	plcbit Power;
	plcbit Home;
	plcbit MoveAbsolute;
	plcbit MoveAdditive;
	plcbit MoveVelocity;
	plcbit Halt;
	plcbit Stop;
	plcbit MoveJogPos;
	plcbit MoveJogNeg;
	plcbit InitAxisLimits;
	plcbit ErrorAcknowledge;
} Basic_command_TYP;

typedef struct Basic_command_Axis_TYP
{	struct Basic_command_TYP Vertical;
	struct Basic_command_TYP Horizontal;
} Basic_command_Axis_TYP;

typedef struct AxisControl_TYP
{	struct AxisControlCallerBox_TYP CallerBox;
	struct AxisControlDirectBox_TYP DirectBox;
	struct AxisControlPar_TYP Par;
	struct AxisControlState_TYP State;
	struct Basic_command_Axis_TYP AxisCmd[12];
} AxisControl_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/AxisControl/G_AxisControl.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_15_ */

