/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273702_66_
#define _BUR_1500273702_66_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nMAX_IDX_TEST_TRIGGER 3U
 #define nIDX_SIMULATED_PRINT_MARK_MAX 4U
#else
 _LOCAL_CONST unsigned char nMAX_IDX_TEST_TRIGGER;
 _LOCAL_CONST unsigned char nIDX_SIMULATED_PRINT_MARK_MAX;
#endif


/* Variablen */
_BUR_LOCAL struct TestTrigger_TYP TestTrigger[4];
_BUR_LOCAL struct TaskVar_TYP TaskVar[4];
_BUR_LOCAL unsigned char nTestTriggerIdx;
_BUR_LOCAL struct IO_TYP IO;
_BUR_LOCAL unsigned char i;
_BUR_LOCAL struct IoPrintMarkSen_TYP CopyTo_IO_PrintMarkSensor[12];
_BUR_LOCAL_RETAIN plcbit bArSimTriggerSensorIdxEnabled[12];





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/TestTrigger/TestTrigger.var\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273702_66_ */

