/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_65_
#define _BUR_1500273701_65_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct TaskVar_TYP
{	signed long nTriggerProduct;
	signed long nTriggerFalse;
	unsigned short nCycleCnt;
	signed long nPositionPrintMarkTriggerOn;
	signed long nPositionPrintMarkTriggerOff;
	signed long nPositionPrintMarkTriggerOn_old;
	signed long nDistanceToPrintMarkTriggerOn;
	plcbit bPrintMarkTrigger;
	unsigned char nPrintMarkCount;
	signed long nTimeToNegEdgePrintMarkTrigger;
	signed long nTimePrintMarkTriggerSignal;
	signed long nTimeToPosEdgePrintMarkTrigger;
	struct F_TRIG NegEdge;
	struct R_TRIG PosEdge;
	signed long nAdaptedDistancePmToTrigger;
	signed long nPositionToSetTrigger;
	signed long nDistanceToSetTrigger;
	float rEncoderVelocityFactor;
	signed long nDistanceWithPrintMarkVelocity;
	signed long nDistanceVelocityCompensation;
	signed long nDistanceProductToProDetSensor;
	signed long nPositionToSetProDetTrigger;
	signed char nTriggerEncoderIdx;
	signed char nPrintMarkEncoderIdx;
} TaskVar_TYP;

typedef struct IO_Input_TYP
{	unsigned char Dummy;
} IO_Input_TYP;

typedef struct IoPrintMarkSen_TYP
{	signed char nRisingCnt;
	signed short nTsRising;
	signed char nFallingCnt;
	signed short nTsFalling;
} IoPrintMarkSen_TYP;

typedef struct Edge_TYP
{	plcbit bEnable;
	signed char nSequence;
	signed long nTimeStamp[4];
	plcbit bQuitError;
	plcbit bQuitWarning;
} Edge_TYP;

typedef struct EdgeGenerationModule_TYP
{	struct Edge_TYP RisingEdge;
	struct Edge_TYP FallingEdge;
} EdgeGenerationModule_TYP;

typedef struct IO_OutputEdgeGeneration_TYP
{	struct EdgeGenerationModule_TYP PrintMark[4];
	struct EdgeGenerationModule_TYP Trigger[4];
} IO_OutputEdgeGeneration_TYP;

typedef struct IO_Output_TYP
{	struct IoPrintMarkSen_TYP CopyToARsim_IO_PrintMarkSensor[12];
	struct IO_OutputEdgeGeneration_TYP EdgeGeneration;
	plcbit bTrigger[4];
	plcbit bProductDetectionTrigger[4];
} IO_Output_TYP;

typedef struct IO_TYP
{	struct IO_Input_TYP Input;
	struct IO_Output_TYP Output;
} IO_TYP;

typedef struct TestTriggerCmd_TYP
{	plcbit bSetTriggerOnce;
	plcbit bSetTriggerCyclicOn;
	plcbit bSetTriggerCyclicOff;
	plcbit bTriggerContinousOn;
	plcbit bTriggerContinousOff;
	plcbit bSetPrintMarkOnce;
	plcbit bPrintMarkContinousOn;
	plcbit bPrintMarkContinousOff;
	plcbit bSetPrintMark_TriggerOnce;
	plcbit bPrintMark_TriggerContinousOn;
	plcbit bPrintMark_TriggerContinousOff;
	plcbit bEnableProductDetectionSensor;
} TestTriggerCmd_TYP;

typedef struct TestTriggerPar_TYP
{	unsigned char nBondingDeviceIdx;
	unsigned short nSetTriggerCyclicCnt;
	float rPrintMarkGapBetweenProducts;
	float rTriggerGapBetweenProducts;
	signed long nPrintMarkLength[5];
	float rTriggerCorrection;
	float rProDetTriggerCorrection;
} TestTriggerPar_TYP;

typedef struct TestTriggerState_TYP
{	plcbit bContinousTriggerIsActive;
	plcbit bCyclicTriggerIsActive;
	plcbit bOncePrintMarkIsSet;
	plcbit bContinousPrintMarkIsActive;
	plcbit bOncePrintMark_TriggerIsSet;
	plcbit bContinousPM_TriggerIsActive;
	signed long nActPrintMarkLength;
} TestTriggerState_TYP;

typedef struct TestTrigger_TYP
{	struct TestTriggerCmd_TYP Cmd;
	struct TestTriggerPar_TYP Par;
	struct TestTriggerState_TYP State;
} TestTrigger_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/TestTrigger/TestTrigger.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Tasks/TestTrigger/TestTrigger.var\\\" scope \\\"local\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_65_ */

