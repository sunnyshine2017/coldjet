/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_48_
#define _BUR_1500273701_48_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define rREAD_LATENCY 0.002f
 #define nPRODUCT_DET_BOTTOM_LIMIT 500
 #define nPRODUCT_DET_LIMIT 2000
 #define nDISABLE_NEG_DIR 1
 #define rFILTER_PAR 0.003f
 #define rNETWORK_COMPENSATION 0.003f
 #define nMIN_SENSOR_DISTANCE 6000U
 _WEAK const float nLAST_ON_POS_DUMMY[64] = {3001.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
 _WEAK const float nFIRST_ON_POS_DUMMY[64] = {3000.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
 _WEAK const float nLAST_ON_POS_DUMMY_START[64] = {11.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
 _WEAK const float nFIRST_ON_POS_DUMMY_START[64] = {10.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
 #define nPERIOD_LEN_CLEANING_STANDARD 400000000U
 #define nPERIOD_LEN_CLEANING_INTERVALL 35000U
#else
 _LOCAL_CONST float rREAD_LATENCY;
 _LOCAL_CONST signed short nPRODUCT_DET_BOTTOM_LIMIT;
 _LOCAL_CONST signed short nPRODUCT_DET_LIMIT;
 _LOCAL_CONST plcbit nDISABLE_NEG_DIR;
 _LOCAL_CONST float rFILTER_PAR;
 _LOCAL_CONST float rNETWORK_COMPENSATION;
 _LOCAL_CONST unsigned long nMIN_SENSOR_DISTANCE;
 _LOCAL_CONST float nLAST_ON_POS_DUMMY[64];
 _LOCAL_CONST float nFIRST_ON_POS_DUMMY[64];
 _LOCAL_CONST float nLAST_ON_POS_DUMMY_START[64];
 _LOCAL_CONST float nFIRST_ON_POS_DUMMY_START[64];
 _LOCAL_CONST unsigned long nPERIOD_LEN_CLEANING_STANDARD;
 _LOCAL_CONST unsigned long nPERIOD_LEN_CLEANING_INTERVALL;
#endif


/* Variablen */
_BUR_LOCAL plcbit bStopIntCleaning;
_BUR_LOCAL plcbit bRequestApplyData;
_BUR_LOCAL plcbit bRequestInitAuto;
_BUR_LOCAL plcbit bRequestInitClean;
_BUR_LOCAL plcbit bPeriodInit[12];
_BUR_LOCAL plcbit bEnableTrack[12];
_BUR_LOCAL plcbit bGetPeriod[12];
_BUR_LOCAL plcbit bGetStartPos[12];
_BUR_LOCAL plcbit bStartPosValid[12];
_BUR_LOCAL plcbit bCalcPeriod[12];
_BUR_LOCAL unsigned char nSwitchIndex;
_BUR_LOCAL unsigned char nDevIndex;
_BUR_LOCAL unsigned char nCntCleanFlag;
_BUR_LOCAL unsigned char nCntClean[12];
_BUR_LOCAL float rNwCompensation;
_BUR_LOCAL float rPosDcsClean[12];
_BUR_LOCAL float rPosDcs[12];
_BUR_LOCAL struct IO_TYP IO;
_BUR_LOCAL struct BonDevEn_TYP BonDevEn[12];
_BUR_LOCAL struct ProductDetection_TYP ProductDetect;
_BUR_LOCAL struct SwitchesPar_TYP AdaptedSwitches[12];
_BUR_LOCAL struct SwitchesIdx_TYP SwitchesBuffer[12];
_BUR_LOCAL struct ProductPar_TYP ProductParDcs;
_BUR_LOCAL struct BonDevDcs_TYP BonDev;
_BUR_LOCAL struct BrbStepHandling_TYP StepHandling;
_BUR_LOCAL struct BondingDevStep_TYP Step;
_BUR_LOCAL struct F_TRIG fbEdgeNegCleanMan;
_BUR_LOCAL struct F_TRIG fbEdgeNegClean;
_BUR_LOCAL struct R_TRIG fbEdgePosCleanMan;
_BUR_LOCAL struct R_TRIG fbEdgePosClean;
_BUR_LOCAL struct F_TRIG fbEdgeNegAuto;
_BUR_LOCAL struct R_TRIG fbEdgePosAuto;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/BondingDev/BondingDev/BondingDev.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_48_ */

