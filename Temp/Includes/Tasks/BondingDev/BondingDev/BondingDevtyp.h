/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_47_
#define _BUR_1500273701_47_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Steps_ENUM
{	eSTEP_INIT = 1,
	eSTEP_INIT_WAIT_FOR_PAR_VALID,
	eSTEP_INIT_FINISHED,
	eSTEP_WAIT_FOR_COMMAND = 100,
	eSTEP_DCS_INIT,
	eSTEP_SWITCHES_INIT,
	eSTEP_DCS_CLEAN_INIT
} Steps_ENUM;

typedef struct BondingDevStep_TYP
{	plcstring sStepText[51];
	enum Steps_ENUM eStepNr;
	plcbit bInitDone;
} BondingDevStep_TYP;

typedef struct FifoStruct_TYP
{	struct PosToBond_TYP FifoEntryStandard[12];
	struct PmPos_TYP FifoEntryPm[12];
} FifoStruct_TYP;

typedef struct BonDevDcs_TYP
{	struct ASMcDcsPrecisionDigitalCamSwitch fbBonDevDCS[12];
	struct FifoStruct_TYP FifoStruct;
	float rProductLen[12];
	signed long nFifoPos[12];
	signed long nStartPos[12];
	signed long nGap[12];
	unsigned long nRemainingDistance[12];
	unsigned long nNextPeriod[12];
	unsigned long nCurrentPeriod[12];
	unsigned char nIdxCurrentPeriod[12];
	unsigned char nIdxNextPeriod[12];
	signed char nSwitchIdx[12];
	plcbit bDcsInitOk[12];
	signed long nDiffLen[12];
	signed long nDiffLatchPos[12];
} BonDevDcs_TYP;

typedef struct ProductDetection_TYP
{	struct FifoStruct_TYP FifoType;
	unsigned long nCalcPosProDetect[12];
	signed char nProductCnt[12];
	plcbit bProductDetected[12];
	plcbit bNewPeriod[12];
	signed long nDistanceProductToProDetSensor[12];
	float rProductInWindow[12];
	float rProductOutOfWindow[12];
} ProductDetection_TYP;

typedef struct IoInput_TYP
{	plcbit bBonDevEnable;
	signed char nProDetRisingCnt[12];
	signed short nTsProduktDetect[12];
} IoInput_TYP;

typedef struct IO_TYP
{	struct IoInput_TYP Input;
} IO_TYP;

typedef struct BonDevEn_TYP
{	plcbit bBonDevEnFlag;
	plcbit bBonDevEnabled;
	plcbit bBonDevDisabled;
	plcbit bDcsInitOk;
	plcbit bEncInitOk;
	plcbit bTriggerInitOk;
	plcbit bPmSenInitOk;
	plcbit bInitializeCounter;
} BonDevEn_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/BondingDev/BondingDev/BondingDev.typ\\\" scope \\\"local\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_47_ */

