/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_13_
#define _BUR_1500273701_13_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct BondingDevCallerBox_TYP
{	struct BrbCaller_TYP Caller;
	plcbit bDcsInit;
	plcbit bApplyPar;
	plcbit bCleanInit;
} BondingDevCallerBox_TYP;

typedef struct BondingDevDirectBox_TYP
{	plcbit bDummy;
} BondingDevDirectBox_TYP;

typedef struct BondingDevPar_TYP
{	struct SwitchesIdx_TYP Switches[12];
} BondingDevPar_TYP;

typedef struct BondingDevState_TYP
{	float rVelocitiy[12];
	plcbit bVelocityOk[12];
	unsigned short nEntryCnt[12];
	unsigned short nCurrentFifoCnt[12];
	unsigned short nCurrentFifoCntFlag[12];
	unsigned short nProDetEntryCnt[12];
	plcbit bBonDevError[12];
	plcbit bIntCleanDone[12];
	plcbit bCleaningActive[12];
	plcbit bDcsCleanInitOk;
} BondingDevState_TYP;

typedef struct BondingDev_TYP
{	struct BondingDevCallerBox_TYP CallerBox;
	struct BondingDevDirectBox_TYP DirectBox;
	struct BondingDevPar_TYP Par;
	struct BondingDevState_TYP State;
} BondingDev_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Tasks/BondingDev/G_BondingDev.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_13_ */

