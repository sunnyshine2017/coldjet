/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */
/* AppLib  */

#ifndef _APPLIB_
#define _APPLIB_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG4
		#include "astime.h"
#endif
#ifdef _SG3
		#include "astime.h"
#endif
#ifdef _SGC
		#include "astime.h"
#endif

/* Constants */
#ifdef _REPLACE_CONST
 #define nAPP_EVENT_INDEX_MAX 299U
 #define nAPP_EVENT_DETAIL_CHAR_MAX 64U
#else
 #ifndef _GLOBAL_CONST
   #define _GLOBAL_CONST _WEAK const
 #endif
 _GLOBAL_CONST unsigned short nAPP_EVENT_INDEX_MAX;
 _GLOBAL_CONST unsigned short nAPP_EVENT_DETAIL_CHAR_MAX;
#endif




/* Datentypen und Datentypen von Funktionsblöcken */
typedef struct AppEvent_TYP
{	plcbit bSet;
	plctime tClockSet;
	plcbit bReset;
	plcbit bWaitForAcknowledge;
	plcbit bAcknowledged;
	signed long nErrorNumber;
	plcstring sDetail[65];
	signed long nDetail;
} AppEvent_TYP;

typedef struct AppEventManagement_TYP
{	struct AppEvent_TYP Events[300];
	plcbit AlarmImage[300];
	plcbit AcknowledgeImage[300];
} AppEventManagement_TYP;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC plcbit AppSetEvent(signed long nEventId, signed long nErrorNumber, plcstring* pDetail, signed long nDetail, struct AppEventManagement_TYP* pEventManagement);
_BUR_PUBLIC plcbit AppResetEvent(signed long nEventId, struct AppEventManagement_TYP* pEventManagement);
_BUR_PUBLIC plcbit AppIsEventAcknowledged(signed long nEventId, struct AppEventManagement_TYP* pEventManagement);
_BUR_PUBLIC float Round(float rInput);


#ifdef __cplusplus
};
#endif
#endif /* _APPLIB_ */

