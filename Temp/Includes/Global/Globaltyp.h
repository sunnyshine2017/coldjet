/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_1_
#define _BUR_1500273701_1_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum EncoderType_ENUM
{	eENCODER_TYPE_NONE = -1,
	eENCODER_TYPE_ENCODER = 0,
	eENCODER_TYPE_VIRTUAL = 1,
	eENCODER_TYPE_TRIGGER = 2
} EncoderType_ENUM;

typedef enum CallerIds_ENUM
{	eCALLERID_NONE,
	eCALLERID_MAIN_LOGIC,
	eCALLERID_ENCODER,
	eCALLERID_BONDING_DEVICE,
	eCALLERID_TRIGGER_SENSOR,
	eCALLERID_PRINT_MARK_SENSOR,
	eCALLERID_AXIS_CONTROL,
	eCALLERID_EVENTHANDLING,
	eCALLERID_PAR_HANDLING,
	eCALLERID_CALC_POS,
	eCALLERID_SYSTEM,
	eCALLERID_VISU,
	eCALLERID_EXT_CTRL_IF,
	eCALLERID_TEMP_CTRL
} CallerIds_ENUM;

typedef enum SensorType_ENUM
{	eSEN_TYPE_TRIGGER_SENSOR,
	eSEN_TYPE_ENCODER,
	eSEN_TYPE_PRINT_MARK_SENSOR,
	eSEN_TYPE_PRINT_MARK_TRIG_SENSOR
} SensorType_ENUM;

typedef enum MotAdjustAlignment_ENUM
{	eVERTICAL = 0,
	eHORIZONTAL
} MotAdjustAlignment_ENUM;

typedef enum TempZone_ENUM
{	eTEMP_ZONE_TUBE = 0,
	eTEMP_ZONE_BON_DEV,
	eTEMP_ZONE_RESERVE
} TempZone_ENUM;

typedef enum StitchSize_ENUM
{	eSTITCH_SIZE_SMALL,
	eSTITCH_SIZE_MEDIUM,
	eSTITCH_SIZE_LARGE
} StitchSize_ENUM;

typedef struct SystemCleaning_TYP
{	plcbit bEnable;
	unsigned short nGlueTankCleaningPeriod;
	unsigned short nDetergentTankCleaningPeriod;
} SystemCleaning_TYP;

typedef struct EncPar_TYP
{	signed long eEncoderType;
	plcbit bSimulationEnable;
	unsigned long nIncPerRev;
	float rDistancePerRev;
	unsigned char nSourceEncoderIdx;
	float rFactor;
	signed char nTriggerSensorIdx;
	float rTriggerSignalDistance;
	float rTriggerSignalTimeOut;
	float rVelocityFactor;
} EncPar_TYP;

typedef struct MachineModule_TYP
{	plcbit bOverviewEn;
	plcbit bCleaningEn;
	plcbit bImgProcessingEn;
	plcbit bMotorisedAdjustmentEn;
	plcbit bPressureCtrlEn;
	plcbit bStitchmodeEn;
	plcbit bTempCtrlEn;
	plcbit bAnalogFuellStandEn;
} MachineModule_TYP;

typedef struct PrintMarkPar_TYP
{	signed long nPrintMarkLen[4];
	signed long nTolerance;
} PrintMarkPar_TYP;

typedef struct Tank_TYP
{	plcbit bTankGlueEn;
	signed char nTankGlueStopDelay;
	plcbit bTankCleanEn;
	signed char nTankCleanStopDelay;
	plcbit bTankSoftEn;
	signed char nTankSoftStopDelay;
	plcbit bAutoValveCtrl;
} Tank_TYP;

typedef struct RefPos_TYP
{	float rVertical;
	float rHorizontal;
} RefPos_TYP;

typedef struct MotorisedAdjExists_TYP
{	plcbit bVertical;
	plcbit bHorizontal;
} MotorisedAdjExists_TYP;

typedef struct MParAxesLimitsAlignment_TYP
{	float rNegativeSwLimit;
	float rPositiveSwLimit;
} MParAxesLimitsAlignment_TYP;

typedef struct AxesLimits_TYP
{	struct MParAxesLimitsAlignment_TYP Alignment[2];
} AxesLimits_TYP;

typedef struct MotorisedAdjustment_TYP
{	float rVelocityHorizontal;
	float rVelocityVertical;
	struct RefPos_TYP RefPos[12];
	struct MotorisedAdjExists_TYP MotorisedAdjExists[12];
	struct AxesLimits_TYP AxesLimits[12];
} MotorisedAdjustment_TYP;

typedef struct PressureCtrlBasis_TYP
{	float rMaxPressure;
	float rBasePressure;
	float rHysteresis;
	float rMaxVelocitiy;
	signed char nEncoderIdx;
	float rBypassOpenTime;
} PressureCtrlBasis_TYP;

typedef struct PressureControl_TYP
{	struct PressureCtrlBasis_TYP PreCtrlBasis;
} PressureControl_TYP;

typedef struct StitchmodeDistance_TYP
{	float rOpenDistanceSmall;
	float rOpenDistanceMedium;
	float rOpenDistanceLarge;
	float rOpenTimeSmall;
	float rOpenTimeMedium;
	float rOpenTimeLarge;
} StitchmodeDistance_TYP;

typedef struct TempCtrlTempZoneCfgPar_TYP
{	plcbit bZoneExist;
	float rSetTempMax;
	float rGain;
	float rIntegrationTime;
	float rDerivativeTime;
	float rFilterTime;
	float rMinOut;
	float rMaxOut;
} TempCtrlTempZoneCfgPar_TYP;

typedef struct TempCtrlTankCfgPar_TYP
{	plcbit bExist;
	float rToleranzMin;
	float rToleranzMax;
	float rTempDiffWarning;
	struct TempCtrlTempZoneCfgPar_TYP TempZonePar;
} TempCtrlTankCfgPar_TYP;

typedef struct TempCtrlBonDevCfgPar_TYP
{	plcbit bExist;
	signed char nTankIdx;
	float rToleranzMin;
	float rToleranzMax;
	float rTempStandby;
	struct TempCtrlTempZoneCfgPar_TYP TempZone[3];
} TempCtrlBonDevCfgPar_TYP;

typedef struct TempCtrlConfigPar_TYP
{	struct TempCtrlTankCfgPar_TYP Tank[2];
	struct TempCtrlBonDevCfgPar_TYP BonDev[12];
} TempCtrlConfigPar_TYP;

typedef struct MachinePar_TYP
{	struct SystemCleaning_TYP SystemCleaning;
	struct EncPar_TYP EncoderPar[5];
	struct MachineModule_TYP MachineModule;
	struct PrintMarkPar_TYP PrintMarkPar;
	struct Tank_TYP TankType;
	struct MotorisedAdjustment_TYP MotorisedAdjustment;
	struct PressureControl_TYP PressureControl;
	struct StitchmodeDistance_TYP StitchmodeDistance;
	struct TempCtrlConfigPar_TYP TempCtrlConfig;
	unsigned long nScreensaverTime;
	signed char nBonDevIndex;
	signed char nTriggerSenIndex;
	signed char nEncoderIndex;
	signed char nProductDetectIndex;
	signed char nPrintMarkSensorIndex;
	float rEnVelocity;
	float rOpenTime;
	unsigned char nNrOfRounds;
	unsigned long nHopperFillingPeriod;
	unsigned long nHopperCleaningPeriod;
	unsigned long nCollectorFillingPeriod;
	unsigned long nCollectorCleaningPeriod;
} MachinePar_TYP;

typedef struct BondingPar_Typ
{	float rPause[16];
	float rStitchLen[16];
} BondingPar_Typ;

typedef struct BondingIdx_TYP
{	struct BondingPar_Typ BondingParIdx[4];
} BondingIdx_TYP;

typedef struct ProductDet_TYP
{	signed char nIdxProductDetect;
	plcbit bProductDetExist;
	unsigned long nOffset;
} ProductDet_TYP;

typedef struct ProdParMotAdjustAlignment_TYP
{	float rParkPosition;
	float rWorkPosition;
} ProdParMotAdjustAlignment_TYP;

typedef struct AxesPosition_TYP
{	struct ProdParMotAdjustAlignment_TYP Alignment[2];
} AxesPosition_TYP;

typedef struct BonDevPar_TYP
{	struct BondingIdx_TYP Switches;
	struct ProductDet_TYP ProductDetection;
	signed char nSwitchIndexMax[4];
	float rProductLen;
	float rProductLenAdapted;
	float rSenOffset;
	float rCylinderPerimeter;
	float rPmProductOffset;
	signed long eIdxSenType;
	signed char nIdxSenSource;
	signed char nIdxPrintMarkSensorSource;
	signed char nIdxEncoder;
	signed char nIdxPrintMarkEncoder;
	float rDistancePrintMarkTriggerSensor;
	float rDistancePrintMark_VelocityJump;
	plcbit bStitchmode;
	signed long nStitchSize;
	float rStitchCloseDistance;
	plcbit bBonDevEn;
	struct AxesPosition_TYP AxesPosition;
	float rReduceProductLenPercent;
	float rValveCompensationOpenTime;
	float rValveCompensationCloseTime;
} BonDevPar_TYP;

typedef struct TriggerSenPar_TYP
{	plcbit bPosNegDetection[12];
} TriggerSenPar_TYP;

typedef struct PmSenPar_TYP
{	plcbit bPosNegDetection[12];
} PmSenPar_TYP;

typedef struct TempCtrlTankPar_TYP
{	float rSetTemp;
	float rTempStandby;
} TempCtrlTankPar_TYP;

typedef struct TempCtrlBonDevPar_TYP
{	plcbit bEnable;
	float rSetTemp[3];
} TempCtrlBonDevPar_TYP;

typedef struct TempCtrlModulePar_TYP
{	struct TempCtrlTankPar_TYP Tank[2];
	struct TempCtrlBonDevPar_TYP BonDev[12];
} TempCtrlModulePar_TYP;

typedef struct ProductPar_TYP
{	struct BonDevPar_TYP BonDevPar[12];
	struct TriggerSenPar_TYP TriggerSenPar;
	struct PmSenPar_TYP PmSenPar;
	struct TempCtrlModulePar_TYP TempCtrlPar;
} ProductPar_TYP;

typedef struct Par_TYP
{	struct MachinePar_TYP MachinePar;
	struct ProductPar_TYP ProductPar;
} Par_TYP;

typedef struct DcsPar_TYP
{	float rPosComp;
	float rOnComp;
	float rOffComp;
	float rHysteresis;
	plcbit bDisNegDir;
} DcsPar_TYP;

typedef struct ParPermanentTempCtrlTank_TYP
{	plcbit bHeatingOn;
} ParPermanentTempCtrlTank_TYP;

typedef struct ParPermanent_TYP
{	unsigned short nVisLanguage;
	unsigned long nOperatingSecondsTotal;
	unsigned long nOperatingSecondsService;
	plcstring sCurrentProductName[260];
	unsigned long nImgProcessingErrCnt;
	float rSimulatedEncoderSpeed;
} ParPermanent_TYP;

typedef struct PosToBond_TYP
{	signed long nPosToBond;
	signed long nDiffPos;
	plcbit bEditProdukt;
	plcbit bProductDetected;
} PosToBond_TYP;

typedef struct PmPos_TYP
{	signed long nStartPosFromTs;
	signed char nPmIdx;
	plcbit bEditProdukt;
	plcbit bProductDetected;
	plcbit bProductPositionSetByTrigger;
	signed long nPositionProductBegins;
} PmPos_TYP;

typedef struct SwitchesPar_TYP
{	float rFirstOnPos[16];
	float rLastOnPos[16];
} SwitchesPar_TYP;

typedef struct SwitchesIdx_TYP
{	struct SwitchesPar_TYP SwitchesIdx[4];
} SwitchesIdx_TYP;

typedef struct NegativeSensorOffset_TYP
{	signed long nNegativeSensorOffset;
	signed long nProductsBetweenBonDev_Sensor;
	signed long nProductRestBetweenBonDev_Sensor;
	signed long nProductRest;
	signed long nNegativeOffsetToBonDev;
	signed long nNegativePrintMarkPositionOffset;
} NegativeSensorOffset_TYP;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Global/Global.typ\\\" scope \\\"global\\\"\\n\"");

/* Zusätzliche IEC Abhängigkeiten */
__asm__(".ascii \"iecdep \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecdep \\\"Logical/Libraries/BrbLib/BrbLib.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_1_ */

