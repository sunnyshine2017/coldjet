/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273701_2_
#define _BUR_1500273701_2_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
 #define nIDX_TEMP_CTRL_TANK_MAX 1U
 #define nIDX_TEMP_CTRL_ZONE_MAX 2U
 #define nIDX_PRINT_MARK_MAX 3U
 #define nIDX_ENCODER_MAX 4U
 #define nIDX_SWITCHES_MAX 15U
 #define nIDX_PRODUCT_MEM 99U
 #define nDEV_INDEX_MAX 11U
 #define nFACTOR_VELOCITY 1000000
 #define nMAX_SENSOR_DISTANCE 600000U
 #define nCLEANING_VELOCITY 1.0f
 #define rMAX_PRODUCT_LEN 6000.0f
 #define rMIN_PRODUCT_LEN 20.0f
 #define rFACTOR_MM_TO_AXIS_UNITS 100.0f
#else
 _GLOBAL_CONST unsigned char nIDX_TEMP_CTRL_TANK_MAX;
 _GLOBAL_CONST unsigned char nIDX_TEMP_CTRL_ZONE_MAX;
 _GLOBAL_CONST unsigned char nIDX_PRINT_MARK_MAX;
 _GLOBAL_CONST unsigned char nIDX_ENCODER_MAX;
 _GLOBAL_CONST unsigned char nIDX_SWITCHES_MAX;
 _GLOBAL_CONST unsigned char nIDX_PRODUCT_MEM;
 _GLOBAL_CONST unsigned char nDEV_INDEX_MAX;
 _GLOBAL_CONST signed long nFACTOR_VELOCITY;
 _GLOBAL_CONST unsigned long nMAX_SENSOR_DISTANCE;
 _GLOBAL_CONST float nCLEANING_VELOCITY;
 _GLOBAL_CONST float rMAX_PRODUCT_LEN;
 _GLOBAL_CONST float rMIN_PRODUCT_LEN;
 _GLOBAL_CONST float rFACTOR_MM_TO_AXIS_UNITS;
#endif


/* Variablen */
_GLOBAL signed short gAxis08_MotorStep0;
_GLOBAL struct SdcDiDoIf_typ gAxis08_DiDoIf;
_GLOBAL struct SdcEncIf16_typ gAxis08_EncIf1;
_GLOBAL struct SdcDrvIf16_typ gAxis08_DrvIf;
_GLOBAL struct SdcHwCfg_typ gAxis08_HW;
_GLOBAL struct ACP10AXIS_typ gAxis08;
_GLOBAL signed short gAxis07_MotorStep0;
_GLOBAL struct SdcDiDoIf_typ gAxis07_DiDoIf;
_GLOBAL struct SdcEncIf16_typ gAxis07_EncIf1;
_GLOBAL struct SdcDrvIf16_typ gAxis07_DrvIf;
_GLOBAL struct SdcHwCfg_typ gAxis07_HW;
_GLOBAL struct ACP10AXIS_typ gAxis07;
_GLOBAL signed short gAxis06_MotorStep0;
_GLOBAL struct SdcDiDoIf_typ gAxis06_DiDoIf;
_GLOBAL struct SdcEncIf16_typ gAxis06_EncIf1;
_GLOBAL struct SdcDrvIf16_typ gAxis06_DrvIf;
_GLOBAL struct SdcHwCfg_typ gAxis06_HW;
_GLOBAL struct ACP10AXIS_typ gAxis06;
_GLOBAL signed short gAxis05_MotorStep0;
_GLOBAL struct SdcDiDoIf_typ gAxis05_DiDoIf;
_GLOBAL struct SdcEncIf16_typ gAxis05_EncIf1;
_GLOBAL struct SdcDrvIf16_typ gAxis05_DrvIf;
_GLOBAL struct SdcHwCfg_typ gAxis05_HW;
_GLOBAL struct ACP10AXIS_typ gAxis05;
_GLOBAL signed short gAxis04_MotorStep0;
_GLOBAL struct SdcDiDoIf_typ gAxis04_DiDoIf;
_GLOBAL struct SdcEncIf16_typ gAxis04_EncIf1;
_GLOBAL struct SdcDrvIf16_typ gAxis04_DrvIf;
_GLOBAL struct SdcHwCfg_typ gAxis04_HW;
_GLOBAL struct ACP10AXIS_typ gAxis04;
_GLOBAL signed short gAxis03_MotorStep0;
_GLOBAL struct SdcDiDoIf_typ gAxis03_DiDoIf;
_GLOBAL struct SdcEncIf16_typ gAxis03_EncIf1;
_GLOBAL struct SdcDrvIf16_typ gAxis03_DrvIf;
_GLOBAL struct SdcHwCfg_typ gAxis03_HW;
_GLOBAL struct ACP10AXIS_typ gAxis03;
_GLOBAL signed short gAxis02_MotorStep0;
_GLOBAL struct SdcDiDoIf_typ gAxis02_DiDoIf;
_GLOBAL struct SdcEncIf16_typ gAxis02_EncIf1;
_GLOBAL struct SdcDrvIf16_typ gAxis02_DrvIf;
_GLOBAL struct SdcHwCfg_typ gAxis02_HW;
_GLOBAL struct ACP10AXIS_typ gAxis02;
_GLOBAL signed short gAxis01_MotorStep0;
_GLOBAL struct SdcDiDoIf_typ gAxis01_DiDoIf;
_GLOBAL struct SdcEncIf16_typ gAxis01_EncIf1;
_GLOBAL struct SdcDrvIf16_typ gAxis01_DrvIf;
_GLOBAL struct SdcHwCfg_typ gAxis01_HW;
_GLOBAL struct ACP10AXIS_typ gAxis01;
_GLOBAL_RETAIN struct ParPermanent_TYP gParPermanent;
_GLOBAL struct Par_TYP gPar;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Global/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273701_2_ */

