/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */
/* BrbLib 2.03.0 */

#ifndef _BRBLIB_
#define _BRBLIB_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _BrbLib_VERSION
#define _BrbLib_VERSION 2.03.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG4
		#include "AsBrStr.h"
		#include "standard.h"
		#include "sys_lib.h"
		#include "visapi.h"
		#include "astime.h"
		#include "AsUSB.h"
		#include "DataObj.h"
		#include "FileIO.h"
#endif
#ifdef _SG3
		#include "AsBrStr.h"
		#include "standard.h"
		#include "sys_lib.h"
		#include "visapi.h"
		#include "astime.h"
		#include "AsUSB.h"
		#include "DataObj.h"
		#include "FileIO.h"
#endif
#ifdef _SGC
		#include "AsBrStr.h"
		#include "standard.h"
		#include "sys_lib.h"
		#include "visapi.h"
		#include "astime.h"
		#include "AsUSB.h"
		#include "DataObj.h"
		#include "FileIO.h"
#endif

/* Constants */
#ifdef _REPLACE_CONST
 #define rBRB_FACTOR_DEG_TO_RAD 0.0174533f
 #define rBRB_FACTOR_RAD_TO_DEG 57.2958f
 #define nBRB_TIMERSWITCH_POINT_INDEX_MAX 15U
 #define nBRB_TIME_TEXT_CHAR_MAX 24U
 #define nBRB_TRENDLINK_TREND_INDEX_MAX 3U
 #define nBRB_TREND_CURVE_INDEX_MAX 3U
 #define nBRB_TREND_SCALE_Y_INDEX_MAX 1U
 #define nBRB_DRAW_TEXT_CHAR_MAX 100U
 #define nBRB_URL_CHAR_MAX 300U
 #define nBRB_TAB_PAGE_INDEX_MAX 8U
 #define nBRB_STRING_INPUT_CHAR_MAX 300U
 #define nBRB_PASSWORD_INDEX_MAX 30U
 #define nBRB_PASSWORD_CHAR_MAX 20U
 #define nBRB_PIECHART_VALUE_INDEX_MAX 10U
 #define nBRB_LISTBOX_OPTION_MAX 100U
 #define nBRB_DROPDOWN_OPTION_MAX 16U
 #define nBRB_EDIT_CTRL_CMD_CHAR_MAX 200U
 #define nBRB_PAGE_LAST_INDEX_MAX 10U
 #define nBRB_VIS_NAME_CHAR_MAX 10U
 #define nBRB_FILTER_SINGLE_INDEX_MAX 10U
 #define nBRB_FILTER_SINGLE_CHAR_MAX 20U
 #define nBRB_FILTER_ALL_CHAR_MAX 200U
 #define nBRB_USB_DEVICE_LIST_INDEX_MAX 5U
 #define nBRB_FILE_NAME_CHAR_MAX 259U
 #define nBRB_DEVICE_NAME_CHAR_MAX 32U
 #define nBRB_PVNAME_CHAR_MAX 520U
 #define nBRB_FILE_LINES_CHAR_MAX 50000U
 #define nBRB_STEPLOG_STEPS_MAX 20U
 #define nBRB_STEP_TEXT_CHAR_MAX 50U
 #define nBRB_IP_ADR_CHAR_MAX 16U
 _WEAK const unsigned char sBRB_QM[2] = {34U,0U};
 _WEAK const unsigned char sBRB_CRLF[3] = {13U,10U,0U};
 _WEAK const unsigned char sBRB_CR[2] = {13U,0U};
 _WEAK const unsigned char sBRB_LFCR[3] = {10U,13U,0U};
 _WEAK const unsigned char sBRB_LF[2] = {10U,0U};
 _WEAK const unsigned char sBRB_TAB[2] = {9U,0U};
 _WEAK const unsigned char sBRB_ETX[2] = {3U,0U};
 _WEAK const unsigned char sBRB_STX[2] = {2U,0U};
#else
 #ifndef _GLOBAL_CONST
   #define _GLOBAL_CONST _WEAK const
 #endif
 _GLOBAL_CONST float rBRB_FACTOR_DEG_TO_RAD;
 _GLOBAL_CONST float rBRB_FACTOR_RAD_TO_DEG;
 _GLOBAL_CONST unsigned short nBRB_TIMERSWITCH_POINT_INDEX_MAX;
 _GLOBAL_CONST unsigned char nBRB_TIME_TEXT_CHAR_MAX;
 _GLOBAL_CONST unsigned short nBRB_TRENDLINK_TREND_INDEX_MAX;
 _GLOBAL_CONST unsigned short nBRB_TREND_CURVE_INDEX_MAX;
 _GLOBAL_CONST unsigned short nBRB_TREND_SCALE_Y_INDEX_MAX;
 _GLOBAL_CONST unsigned short nBRB_DRAW_TEXT_CHAR_MAX;
 _GLOBAL_CONST unsigned short nBRB_URL_CHAR_MAX;
 _GLOBAL_CONST unsigned short nBRB_TAB_PAGE_INDEX_MAX;
 _GLOBAL_CONST unsigned short nBRB_STRING_INPUT_CHAR_MAX;
 _GLOBAL_CONST unsigned short nBRB_PASSWORD_INDEX_MAX;
 _GLOBAL_CONST unsigned short nBRB_PASSWORD_CHAR_MAX;
 _GLOBAL_CONST unsigned short nBRB_PIECHART_VALUE_INDEX_MAX;
 _GLOBAL_CONST unsigned short nBRB_LISTBOX_OPTION_MAX;
 _GLOBAL_CONST unsigned short nBRB_DROPDOWN_OPTION_MAX;
 _GLOBAL_CONST unsigned short nBRB_EDIT_CTRL_CMD_CHAR_MAX;
 _GLOBAL_CONST unsigned char nBRB_PAGE_LAST_INDEX_MAX;
 _GLOBAL_CONST unsigned char nBRB_VIS_NAME_CHAR_MAX;
 _GLOBAL_CONST unsigned long nBRB_FILTER_SINGLE_INDEX_MAX;
 _GLOBAL_CONST unsigned long nBRB_FILTER_SINGLE_CHAR_MAX;
 _GLOBAL_CONST unsigned long nBRB_FILTER_ALL_CHAR_MAX;
 _GLOBAL_CONST unsigned short nBRB_USB_DEVICE_LIST_INDEX_MAX;
 _GLOBAL_CONST unsigned long nBRB_FILE_NAME_CHAR_MAX;
 _GLOBAL_CONST unsigned long nBRB_DEVICE_NAME_CHAR_MAX;
 _GLOBAL_CONST unsigned short nBRB_PVNAME_CHAR_MAX;
 _GLOBAL_CONST unsigned short nBRB_FILE_LINES_CHAR_MAX;
 _GLOBAL_CONST unsigned short nBRB_STEPLOG_STEPS_MAX;
 _GLOBAL_CONST unsigned short nBRB_STEP_TEXT_CHAR_MAX;
 _GLOBAL_CONST unsigned short nBRB_IP_ADR_CHAR_MAX;
 _GLOBAL_CONST unsigned char sBRB_QM[2];
 _GLOBAL_CONST unsigned char sBRB_CRLF[3];
 _GLOBAL_CONST unsigned char sBRB_CR[2];
 _GLOBAL_CONST unsigned char sBRB_LFCR[3];
 _GLOBAL_CONST unsigned char sBRB_LF[2];
 _GLOBAL_CONST unsigned char sBRB_TAB[2];
 _GLOBAL_CONST unsigned char sBRB_ETX[2];
 _GLOBAL_CONST unsigned char sBRB_STX[2];
#endif




/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum BrbCallerStates_ENUM
{	eBRB_CALLER_STATE_NOT_READY = -1,
	eBRB_CALLER_STATE_OK = 0,
	eBRB_CALLER_STATE_BUSY = 1
} BrbCallerStates_ENUM;

typedef enum BrbDirInfoFilter_ENUM
{	eBRB_DIR_INFO_ONLY_FILES,
	eBRB_DIR_INFO_ONLY_DIRS,
	eBRB_DIR_INFO_FILES_AND_DIRS
} BrbDirInfoFilter_ENUM;

typedef enum BrbFileSorting_ENUM
{	eBRB_FILE_SORTING_NONE,
	eBRB_FILE_SORTING_ALPH_UP,
	eBRB_FILE_SORTING_ALPH_DOWN,
	eBRB_FILE_SORTING_OLDEST,
	eBRB_FILE_SORTING_YOUNGEST,
	eBRB_FILE_SORTING_BIGGEST,
	eBRB_FILE_SORTING_SMALLEST
} BrbFileSorting_ENUM;

typedef enum BrbVc4TouchStates_ENUM
{	eBRB_TOUCH_STATE_UNPUSHED = 0,
	eBRB_TOUCH_STATE_UNPUSHED_EDGE = 1,
	eBRB_TOUCH_STATE_PUSHED_EDGE = 2,
	eBRB_TOUCH_STATE_PUSHED = 3,
	eBRB_TOUCH_STATE_DOUBLECLICK = 4
} BrbVc4TouchStates_ENUM;

typedef enum BrbVc4Colors_ENUM
{	eBRB_COLOR_ENABLED = 59,
	eBRB_COLOR_DISABLED = 63547,
	eBRB_COLOR_TRANSPARENT = 255
} BrbVc4Colors_ENUM;

typedef enum BrbVc4DropdownOptions_ENUM
{	eBRB_DD_OPTION_NORMAL = 0,
	eBRB_DD_OPTION_DISABLED = 1,
	eBRB_DD_OPTION_INVISIBLE = 2
} BrbVc4DropdownOptions_ENUM;

typedef enum BrbVc4HwPosSwitchPositions_ENUM
{	eBRB_HPS_POS_0_EDGE,
	eBRB_HPS_POS_0,
	eBRB_HPS_POS_1_EDGE,
	eBRB_HPS_POS_1,
	eBRB_HPS_POS_2_EDGE,
	eBRB_HPS_POS_2,
	eBRB_HPS_POS_UNDEF
} BrbVc4HwPosSwitchPositions_ENUM;

typedef enum BrbVc4HwSafteyButtonStates_ENUM
{	eBRB_HSB_STATE_UNPUSHED,
	eBRB_HSB_STATE_UNPUSHED_EDGE,
	eBRB_HSB_STATE_PUSHED_EDGE,
	eBRB_HSB_STATE_PUSHED
} BrbVc4HwSafteyButtonStates_ENUM;

typedef enum BrbVc4IncButtonStates_ENUM
{	eBRB_INC_BTN_STATE_UNPUSHED,
	eBRB_INC_BTN_STATE_UNPUSHED_EDGE,
	eBRB_INC_BTN_STATE_PUSHED_EDGE,
	eBRB_INC_BTN_STATE_PUSHED_DELAY,
	eBRB_INC_BTN_STATE_PUSHED_REPEAT,
	eBRB_INC_BTN_STATE_PUSHED_INC
} BrbVc4IncButtonStates_ENUM;

typedef enum BrbVc4JogButtonStates_ENUM
{	eBRB_JOG_BTN_STATE_UNPUSHED,
	eBRB_JOG_BTN_STATE_UNPUSHED_EDGE,
	eBRB_JOG_BTN_STATE_PUSHED_EDGE,
	eBRB_JOG_BTN_STATE_PUSHED
} BrbVc4JogButtonStates_ENUM;

typedef enum BrbVc4ToggleButtonStates_ENUM
{	eBRB_TOG_BTN_STATE_UNPUSHED,
	eBRB_TOG_BTN_STATE_UNPUSHED_EDGE,
	eBRB_TOG_BTN_STATE_PUSHED_EDGE,
	eBRB_TOG_BTN_STATE_PUSHED
} BrbVc4ToggleButtonStates_ENUM;

typedef enum BrbVc4Figures_ENUM
{	eBRB_FIGURE_LINE,
	eBRB_FIGURE_RECTANGLE,
	eBRB_FIGURE_ELLIPSE,
	eBRB_FIGURE_ARC,
	eBRB_FIGURE_TEXT
} BrbVc4Figures_ENUM;

typedef enum BrbVc4TrendScaleYIndex_ENUM
{	eBRB_TREND_SCALE_Y_INDEX_LEFT,
	eBRB_TREND_SCALE_Y_INDEX_RIGHT
} BrbVc4TrendScaleYIndex_ENUM;

typedef enum BrbVc4TrendCurveMode_ENUM
{	eBRB_TREND_CURVE_MODE_LINED,
	eBRB_TREND_CURVE_MODE_STEPPED
} BrbVc4TrendCurveMode_ENUM;

typedef enum BrbVc4TrendSource_ENUM
{	eBRB_TREND_SOURCE_SINGLE_ARRAY,
	eBRB_TREND_SOURCE_STRUCT_ARRAY
} BrbVc4TrendSource_ENUM;

typedef enum BrbVc4TrendValueDatatype_ENUM
{	eBRB_TREND_VALUE_DATATYPE_REAL,
	eBRB_TREND_VALUE_DATATYPE_DINT,
	eBRB_TREND_VALUE_DATATYPE_INT
} BrbVc4TrendValueDatatype_ENUM;

typedef enum BrbVc4TrendTouchAction_ENUM
{	eBRB_TREND_TOUCHACTION_NONE,
	eBRB_TREND_TOUCHACTION_CURS_SET,
	eBRB_TREND_TOUCHACTION_SCROLL,
	eBRB_TREND_TOUCHACTION_ZOOM_DRAG,
	eBRB_TREND_TOUCHACTION_ZOOM_SET
} BrbVc4TrendTouchAction_ENUM;

typedef enum BrbVc4PlotSource_ENUM
{	eBRB_PLOT_SOURCE_SINGLE_ARRAY,
	eBRB_PLOT_SOURCE_STRUCT_ARRAY
} BrbVc4PlotSource_ENUM;

typedef enum BrbVc4PlotTouchAction_ENUM
{	eBRB_PLOT_TOUCHACTION_NONE,
	eBRB_PLOT_TOUCHACTION_CURS_SET,
	eBRB_PLOT_TOUCHACTION_SCROLL,
	eBRB_PLOT_TOUCHACTION_ZOOM_DRAG,
	eBRB_PLOT_TOUCHACTION_ZOOM_SET
} BrbVc4PlotTouchAction_ENUM;

typedef enum BrbVc4DrawAxisCaptionOrder_ENUM
{	eBRB_DRAW_AXIS_NAME_POS_VEL,
	eBRB_DRAW_AXIS_NAME_VEL_POS,
	eBRB_DRAW_AXIS_POS_NAME_VEL,
	eBRB_DRAW_AXIS_POS_VEL_NAME,
	eBRB_DRAW_AXIS_VEL_NAME_POS,
	eBRB_DRAW_AXIS_VEL_POS_NAME
} BrbVc4DrawAxisCaptionOrder_ENUM;

typedef enum BrbTimeAndDateCompare_ENUM
{	eBRB_TAD_COMPARE_YOUNGER,
	eBRB_TAD_COMPARE_YOUNGEREQUAL,
	eBRB_TAD_COMPARE_EQUAL,
	eBRB_TAD_COMPARE_OLDEREQUAL,
	eBRB_TAD_COMPARE_OLDER
} BrbTimeAndDateCompare_ENUM;

typedef enum BrbTimerSwitchType_ENUM
{	eBRB_TIMERSWITCH_TYPE_OFF,
	eBRB_TIMERSWITCH_TYPE_ON,
	eBRB_TIMERSWITCH_TYPE_TOGGLE,
	eBRB_TIMERSWITCH_TYPE_IMPULSE
} BrbTimerSwitchType_ENUM;

typedef enum BrbTimerSwitchMode_ENUM
{	eBRB_TIMERSWITCH_MODE_MONTH,
	eBRB_TIMERSWITCH_MODE_WEEK,
	eBRB_TIMERSWITCH_MODE_DAY,
	eBRB_TIMERSWITCH_MODE_HOUR,
	eBRB_TIMERSWITCH_MODE_MINUTE,
	eBRB_TIMERSWITCH_MODE_SECOND
} BrbTimerSwitchMode_ENUM;

typedef enum BrbTimerSwitchWeekdays_ENUM
{	eBRB_TIMERSWITCH_WD_SUNDAY,
	eBRB_TIMERSWITCH_WD_MONDAY,
	eBRB_TIMERSWITCH_WD_TUESDAY,
	eBRB_TIMERSWITCH_WD_WEDNESDAY,
	eBRB_TIMERSWITCH_WD_THURSDAY,
	eBRB_TIMERSWITCH_WD_FRIDAY,
	eBRB_TIMERSWITCH_WD_SATURDAY
} BrbTimerSwitchWeekdays_ENUM;

typedef enum BrbError_ENUM
{	eBRB_ERR_OK = 0,
	eBRB_ERR_NULL_POINTER = 50000,
	eBRB_ERR_INVALID_PARAMETER = 50001,
	eBRB_ERR_WRONG_VERSION = 50700,
	eBRB_ERR_TOO_LESS_FIGURES = 50701,
	eBRB_ERR_TOO_LESS_TRIANGLES = 50702,
	eBRB_ERR_TOO_LESS_TEXTS = 50703,
	eBRB_ERR_BUSY = 65535
} BrbError_ENUM;

typedef enum BrbRound_ENUM
{	eBRB_ROUND_APPROP,
	eBRB_ROUND_UP,
	eBRB_ROUND_DOWN
} BrbRound_ENUM;

typedef struct BrbStepHandlingCurrent_TYP
{	signed long nStepNr;
	plcstring sStepText[51];
	plcbit bTimeoutElapsed;
	signed long nTimeoutContinueStep;
} BrbStepHandlingCurrent_TYP;

typedef struct BrbStepHandlingStep_TYP
{	signed long nStepNr;
	plcstring sStepText[51];
	unsigned long nCycleCount;
} BrbStepHandlingStep_TYP;

typedef struct BrbStepHandlingLog_TYP
{	plcbit bClear;
	plcbit bStop;
	struct BrbStepHandlingStep_TYP Steps[21];
} BrbStepHandlingLog_TYP;

typedef struct BrbStepHandlingIntern_TYP
{	signed long nStepNrOld;
	unsigned long nCycleCount;
	plcbit bLogOnNextCycle;
	struct TON fbTimeout;
} BrbStepHandlingIntern_TYP;

typedef struct BrbStepHandling_TYP
{	struct BrbStepHandlingCurrent_TYP Current;
	struct BrbStepHandlingLog_TYP Log;
	struct BrbStepHandlingIntern_TYP Intern;
} BrbStepHandling_TYP;

typedef struct BrbStopWatch_TYP
{	plctime tStartTime;
	plctime tStopTime;
	unsigned long nTimeDiff;
	plcstring sTimeDiff[25];
} BrbStopWatch_TYP;

typedef struct BrbCaller_TYP
{	signed long nCallerId;
	plcbit bLock;
} BrbCaller_TYP;

typedef struct BrbPvInfo_TYP
{	plcstring sName[521];
	unsigned long nAdr;
	unsigned long nDataType;
	unsigned long nLen;
	unsigned short nDimension;
	unsigned short nItemIndex;
} BrbPvInfo_TYP;

typedef struct BrbUsbDeviceListEntry_TYP
{	plcstring sInterfaceName[33];
	plcstring sDeviceName[33];
	unsigned long nNode;
	unsigned long nHandle;
} BrbUsbDeviceListEntry_TYP;

typedef struct BrbReadDirListEntry_TYP
{	plcstring sName[260];
	plcdt dtDate;
	unsigned long nSize;
} BrbReadDirListEntry_TYP;

typedef struct BrbVc4GeneralTouch_TYP
{	unsigned long nX;
	unsigned long nY;
	enum BrbVc4TouchStates_ENUM eState;
	enum BrbVc4TouchStates_ENUM eStateSync;
	struct TON fbDoubleClickDelay;
	struct TouchAction TouchAction;
	struct TouchAction TouchActionOld;
} BrbVc4GeneralTouch_TYP;

typedef struct BrbVc4General_TYP
{	plcstring sVisName[11];
	plctime tDoubleclickDelay;
	unsigned long nDoubleclickDrift;
	unsigned short nRedrawCycles;
	plcbit bDisableRedraw;
	unsigned short nLanguageCurrent;
	unsigned short nLanguageChange;
	unsigned long nLifeSign;
	unsigned long nVcHandle;
	struct BrbVc4GeneralTouch_TYP Touch;
	unsigned short nRedrawCounter;
} BrbVc4General_TYP;

typedef struct BrbVc4Hotspot_TYP
{	plcbit bClicked;
	unsigned short nStatus;
} BrbVc4Hotspot_TYP;

typedef struct BrbVc4ScreenSaver_TYP
{	plcbit bEnable;
	struct BrbVc4Hotspot_TYP hsScreen;
	unsigned short nScreenSaverPage;
	struct TON fbScreenSaver;
	struct BrbVc4GeneralTouch_TYP TouchOld;
	signed long nPageBeforeScreenSaver;
} BrbVc4ScreenSaver_TYP;

typedef struct BrbMemListManagement_Typ
{	unsigned long pList;
	unsigned long nEntryLength;
	unsigned short nIndexMax;
	unsigned short nEntryCount;
} BrbMemListManagement_Typ;

typedef struct BrbVc4PageHandling_TYP
{	signed long nPageDefault;
	plcbit bChangePageDirect;
	signed long nPageChange;
	signed long nPageCurrent;
	signed long nPageNext;
	struct BrbMemListManagement_Typ PagesPreviousLifo;
	signed long nPagesPrevious[11];
	plcbit bPageInit;
	plcbit bPageExit;
	plcbit bPageChangeInProgress;
} BrbVc4PageHandling_TYP;

typedef struct BrbVc4Animation_TYP
{	plcbit bEnable;
	unsigned short nIndexMin;
	unsigned short nIndexMax;
	unsigned short nIndexStanding;
	unsigned short nIndex;
	unsigned short nStatus;
	struct TON fbTimer;
} BrbVc4Animation_TYP;

typedef struct BrbVc4Bargraph_TYP
{	float rValue;
	signed long nValue;
	unsigned short nColor;
	unsigned short nStatus;
} BrbVc4Bargraph_TYP;

typedef struct BrbVc4Bitmap_TYP
{	unsigned short nIndex;
	unsigned short nColor;
	unsigned short nFillColor1;
	unsigned short nFillColor2;
	unsigned short nStatus;
} BrbVc4Bitmap_TYP;

typedef struct BrbVc4Button_TYP
{	unsigned short nBmpIndex;
	unsigned short nTextIndex;
	unsigned short nColor;
	plcbit bClicked;
	unsigned short nStatus;
} BrbVc4Button_TYP;

typedef struct BrbVc4Checkbox_TYP
{	plcbit bClicked;
	unsigned short nBmpIndex;
	unsigned short nTextColor;
	unsigned short nStatus;
	plcbit bChecked;
} BrbVc4Checkbox_TYP;

typedef struct BrbVc4DateTime_TYP
{	plcdt dtValue;
	unsigned short nColor;
	unsigned short nStatus;
} BrbVc4DateTime_TYP;

typedef struct BrbVc4Drawbox_TYP
{	unsigned long nLeft;
	unsigned long nTop;
	unsigned long nWidth;
	unsigned long nHeight;
	plcstring sFullName[260];
	unsigned short nBackgroundColor;
	unsigned short nStatus;
} BrbVc4Drawbox_TYP;

typedef struct BrbVc4Dropdown_TYP
{	unsigned short nIndex;
	unsigned short nIndexMin;
	unsigned short nIndexMax;
	unsigned char nOptions[17];
	plcbit bInputCompleted;
	unsigned short nStatus;
} BrbVc4Dropdown_TYP;

typedef struct BrbVc4EditCtrl_TYP
{	unsigned short nStatus;
	unsigned short nBusy;
	plcstring sUrl[301];
	plcstring sCmdRequest[201];
	plcstring sCmdResponse[201];
	unsigned short nCmdStatus;
	unsigned short nCompletion;
	unsigned long nCursorLine;
	unsigned long nCursorColumn;
	unsigned char nInsertMode;
	unsigned char nModified;
	unsigned char nSelectionMode;
	unsigned long nLineCount;
	unsigned long nColumsMax;
	plcstring sLastCmdRequest[201];
} BrbVc4EditCtrl_TYP;

typedef struct BrbVc4Gauge_TYP
{	signed long nValue;
	signed long nValueMin;
	signed long nValueMax;
	unsigned short nColor;
	unsigned short nStatus;
} BrbVc4Gauge_TYP;

typedef struct BrbVc4Html_TYP
{	plcstring sCurrentUrl[301];
	plcstring sChangeUrl[301];
	plcstring sCurrentTitle[301];
	plcbit bBusy;
	unsigned long nHttpErrorCode;
	unsigned short nStatus;
	plcbit bBusyOld;
} BrbVc4Html_TYP;

typedef struct BrbVc4HwPosSwitch2_TYP
{	plcbit bPos0;
	plcbit bPos2;
	unsigned short nStatus;
	unsigned short nSwitchStatus0;
	unsigned short nSwitchStatus1;
	unsigned short nSwitchStatus2;
	enum BrbVc4HwPosSwitchPositions_ENUM ePosOld;
	enum BrbVc4HwPosSwitchPositions_ENUM ePos;
} BrbVc4HwPosSwitch2_TYP;

typedef struct BrbVc4HwSafetyButton_TYP
{	plcbit bNormallyOpened;
	plcbit bNormallyClosed;
	unsigned short nColor;
	unsigned short nStatus;
	plcbit bPushedOld;
	plcbit bPushed;
	enum BrbVc4HwSafteyButtonStates_ENUM eState;
} BrbVc4HwSafetyButton_TYP;

typedef struct BrbVc4IncButton_TYP
{	plcbit bEnabled;
	plcbit bSuppressDelay;
	plcbit bSuppressRepeat;
	unsigned short nBmpIndex;
	unsigned short nTextIndex;
	unsigned short nColor;
	plcbit bPushed;
	unsigned short nStatus;
	enum BrbVc4IncButtonStates_ENUM eIncState;
	plcbit bPushedOld;
	plcbit bEnabledOld;
	struct TON fbDelay;
	struct TON fbRepeat;
} BrbVc4IncButton_TYP;

typedef struct BrbVc4JogButton_TYP
{	plcbit bEnabled;
	plcbit bSuppressTimeout;
	unsigned short nBmpIndex;
	unsigned short nTextIndex;
	unsigned short nColor;
	plcbit bPushed;
	unsigned short nStatus;
	enum BrbVc4JogButtonStates_ENUM eJogState;
	plcbit bPushedOld;
	plcbit bEnabledOld;
	struct TON fbUnpush;
} BrbVc4JogButton_TYP;

typedef struct BrbVc4Layer_TYP
{	unsigned short nStatus;
} BrbVc4Layer_TYP;

typedef struct BrbVc4Listbox_TYP
{	unsigned short nIndex;
	unsigned short nIndexMin;
	unsigned short nIndexMax;
	unsigned short nControlColor;
	unsigned short nDisabledColor;
	unsigned short nSelectedColor;
	unsigned short nSliderColor;
	unsigned char nOptions[101];
	plcbit bInputCompleted;
	unsigned short nStatus;
} BrbVc4Listbox_TYP;

typedef struct BrbVc4Numeric_TYP
{	signed long nValue;
	signed long nMin;
	signed long nMax;
	float rValue;
	float rMin;
	float rMax;
	unsigned short nColor;
	plcbit bInputCompleted;
	unsigned short nStatus;
} BrbVc4Numeric_TYP;

typedef struct BrbVc4Optionbox_TYP
{	plcbit bClicked;
	unsigned short nBmpIndex;
	unsigned short nTextColor;
	unsigned short nStatus;
	plcbit bChecked;
} BrbVc4Optionbox_TYP;

typedef struct BrbVc4Password_TYP
{	plcstring sPasswords[31][21];
	unsigned short nLevel;
	unsigned short nColor;
	plcbit bInputCompleted;
	unsigned short nStatus;
} BrbVc4Password_TYP;

typedef struct BrbVc4PieChart_TYP
{	signed long nValue[11];
	unsigned short nColor;
	unsigned short nStatus;
} BrbVc4PieChart_TYP;

typedef struct BrbVc4Scale_TYP
{	signed long nValue;
	signed long nValueMin;
	signed long nValueMax;
	plcbit bInputCompleted;
	unsigned short nControlColor;
	unsigned short nRangeColor;
	unsigned short nStatus;
} BrbVc4Scale_TYP;

typedef struct BrbVc4ScrollbarHor_TYP
{	plcbit bDisabled;
	signed long nTotalIndexMin;
	signed long nTotalIndexMax;
	unsigned long nCountShow;
	unsigned long nEntryCountTotal;
	unsigned long nScrollTotal;
	struct BrbVc4Button_TYP btnLeft;
	struct BrbVc4IncButton_TYP btnPageLeft;
	struct BrbVc4IncButton_TYP btnLineLeft;
	struct BrbVc4IncButton_TYP btnLineRight;
	struct BrbVc4IncButton_TYP btnPageRight;
	struct BrbVc4Button_TYP btnRight;
	plcbit bScrollDone;
} BrbVc4ScrollbarHor_TYP;

typedef struct BrbVc4ScrollbarVer_TYP
{	plcbit bDisabled;
	signed long nTotalIndexMin;
	signed long nTotalIndexMax;
	unsigned long nCountShow;
	unsigned long nEntryCountTotal;
	unsigned long nScrollTotal;
	struct BrbVc4Button_TYP btnTop;
	struct BrbVc4IncButton_TYP btnPageUp;
	struct BrbVc4IncButton_TYP btnLineUp;
	struct BrbVc4IncButton_TYP btnLineDown;
	struct BrbVc4IncButton_TYP btnPageDown;
	struct BrbVc4Button_TYP btnBottom;
	plcbit bScrollDone;
} BrbVc4ScrollbarVer_TYP;

typedef struct BrbVc4Scrollbar_TYP
{	struct BrbVc4ScrollbarHor_TYP Horizontal;
	struct BrbVc4ScrollbarVer_TYP Vertical;
} BrbVc4Scrollbar_TYP;

typedef struct BrbVc4ScrollList_TYP
{	plcbit bGetList;
	signed long nScrollOffset;
	signed long nSelectedIndex;
} BrbVc4ScrollList_TYP;

typedef struct BrbVc4Shape_TYP
{	unsigned short nColor;
	unsigned short nStatus;
} BrbVc4Shape_TYP;

typedef struct BrbVc4Slider_TYP
{	signed long nValueShow;
	signed long nValueInput;
	signed long nValueMin;
	signed long nValueMax;
	plcbit bInputCompleted;
	unsigned short nColor;
	unsigned short nStatus;
} BrbVc4Slider_TYP;

typedef struct BrbVc4String_TYP
{	plcstring sValue[301];
	unsigned short nColor;
	plcbit bInputCompleted;
	unsigned short nStatus;
} BrbVc4String_TYP;

typedef struct BrbVc4TabPage_TYP
{	struct BrbVc4Button_TYP btnPage;
	unsigned short nStatus;
} BrbVc4TabPage_TYP;

typedef struct BrbVc4TabCtrl_TYP
{	unsigned short nSelectedTabPageIndex;
	struct BrbVc4TabPage_TYP TabPage[9];
} BrbVc4TabCtrl_TYP;

typedef struct BrbVc4Text_TYP
{	unsigned short nIndex;
	unsigned short nColor;
	unsigned short nStatus;
} BrbVc4Text_TYP;

typedef struct BrbVc4ToggleButton_TYP
{	unsigned short nBmpIndex;
	unsigned short nTextIndex;
	unsigned short nColor;
	plcbit bPushed;
	unsigned short nStatus;
	enum BrbVc4ToggleButtonStates_ENUM eToggleState;
	plcbit bPushedOld;
} BrbVc4ToggleButton_TYP;

typedef struct BrbVc4ToggleButtonExt_TYP
{	unsigned short nBmpIndex;
	unsigned short nTextIndex;
	unsigned short nColor;
	plcbit bVisPushed;
	plcbit bPushed;
	unsigned short nStatus;
	enum BrbVc4ToggleButtonStates_ENUM eToggleState;
	plcbit bVisPushedOld;
	plcbit bPushedOld;
} BrbVc4ToggleButtonExt_TYP;

typedef struct BrbVc4Line_TYP
{	signed long nLeft;
	signed long nTop;
	signed long nRight;
	signed long nBottom;
	unsigned short nColor;
	unsigned short nDashWidth;
} BrbVc4Line_TYP;

typedef struct BrbVc4Rectangle_TYP
{	signed long nLeft;
	signed long nTop;
	signed long nWidth;
	signed long nHeight;
	unsigned short nFillColor;
	unsigned short nBorderColor;
	unsigned short nDashWidth;
} BrbVc4Rectangle_TYP;

typedef struct BrbVc4Ellipse_TYP
{	signed long nLeft;
	signed long nTop;
	signed long nWidth;
	signed long nHeight;
	unsigned short nFillColor;
	unsigned short nBorderColor;
	unsigned short nDashWidth;
} BrbVc4Ellipse_TYP;

typedef struct BrbVc4Arc_TYP
{	signed long nLeft;
	signed long nTop;
	signed long nWidth;
	signed long nHeight;
	float rStartAngle;
	float rEndAngle;
	unsigned short nFillColor;
	unsigned short nBorderColor;
	unsigned short nDashWidth;
} BrbVc4Arc_TYP;

typedef struct BrbVc4DrawText_TYP
{	signed long nLeft;
	signed long nTop;
	unsigned short nFontIndex;
	unsigned short nTextColor;
	unsigned short nBackgroundColor;
	plcstring sText[101];
} BrbVc4DrawText_TYP;

typedef struct BrbVc4Touchgrid_TYP
{	plcbit bClickEnabled;
	plcbit bUseSyncTouchState;
	plcbit bDrawGrid;
	plcbit bDrawMarker;
	signed long nGridLeft;
	signed long nGridTop;
	unsigned short nCellWidth;
	unsigned short nIndexMaxX;
	unsigned short nCellHeight;
	unsigned short nIndexMaxY;
	unsigned short nGridColor;
	enum BrbVc4Figures_ENUM eMarkerFigure;
	struct BrbVc4Line_TYP MarkerLine;
	struct BrbVc4Rectangle_TYP MarkerRectangle;
	struct BrbVc4Ellipse_TYP MarkerEllipse;
	struct BrbVc4Arc_TYP MarkerArc;
	struct BrbVc4DrawText_TYP MarkerText;
	unsigned short nSelectedIndexX;
	unsigned short nSelectedIndexY;
	unsigned short nSelectedIndex;
	unsigned long nSelectedIndexMax;
	enum BrbVc4TouchStates_ENUM eState;
} BrbVc4Touchgrid_TYP;

typedef struct BrbVc4Point_TYP
{	signed long nX;
	signed long nY;
} BrbVc4Point_TYP;

typedef struct BrbVc4Font_TYP
{	unsigned short nIndex;
	float rCharWidth;
	float rCharHeight;
} BrbVc4Font_TYP;

typedef struct BrbVc4DrawPadding_TYP
{	signed long nTop;
	signed long nBottom;
	signed long nLeft;
	signed long nRight;
} BrbVc4DrawPadding_TYP;

typedef struct BrbVc4DrawTouchBorderCorr_TYP
{	signed char nX;
	signed char nY;
} BrbVc4DrawTouchBorderCorr_TYP;

typedef struct BrbVc4DrawTrendCfgGrid_TYP
{	plcbit bShow;
	unsigned short nColor;
} BrbVc4DrawTrendCfgGrid_TYP;

typedef struct BrbVc4DrawTrendCfgScaleY_TYP
{	plcbit bShow;
	unsigned short nColor;
	unsigned short nLinesCount;
	signed long nLineLength;
	plcstring sUnit[101];
	struct BrbVc4DrawTrendCfgGrid_TYP Grid;
	float rMin;
	float rMax;
	unsigned short nFractionDigits;
} BrbVc4DrawTrendCfgScaleY_TYP;

typedef struct BrbVc4DrawTrendCfgScaleX_TYP
{	plcbit bShow;
	signed long nColor;
	unsigned short nLinesCount;
	signed long nLineLength;
	struct BrbVc4DrawTrendCfgGrid_TYP Grid;
	plcstring sFormat[25];
	struct DTStructure dtsStartTime;
	unsigned long nInterval;
	unsigned long nZoomValueCount;
	signed long nScrollOffset;
	plcbit bLimitScrollOffset;
} BrbVc4DrawTrendCfgScaleX_TYP;

typedef struct BrbVc4DrawTrendCfgTouchAct_TYP
{	struct BrbVc4DrawTouchBorderCorr_TYP BorderCorrection;
	plcbit bSetCursor0;
	plcbit bSetCursor1;
	plcbit bZoomX;
	plcbit bZoomY;
	plcbit bScrollX;
	plcbit bScrollY;
} BrbVc4DrawTrendCfgTouchAct_TYP;

typedef struct BrbVc4DrawTrendCfgCurve_TYP
{	plcbit bShow;
	signed long nColor;
	enum BrbVc4TrendScaleYIndex_ENUM eScaleY;
	enum BrbVc4TrendCurveMode_ENUM eMode;
	enum BrbVc4TrendSource_ENUM eValueSource;
	enum BrbVc4TrendValueDatatype_ENUM eValueDatatype;
	unsigned long pValueSource;
	unsigned long nStructSize;
	unsigned long nStructMemberOffset;
	float rConversionFactor;
	plcbit bCalculateStatistics;
} BrbVc4DrawTrendCfgCurve_TYP;

typedef struct BrbVc4DrawTrendCfgCursor_TYP
{	plcbit bShow;
	signed long nColor;
	signed long nSampleIndex;
} BrbVc4DrawTrendCfgCursor_TYP;

typedef struct BrbVc4DrawTrendCfgCallbacks_TYP
{	unsigned long pCallbackAfterClear;
	unsigned long pCallbackAfterCurveArea;
	unsigned long pCallbackAfterScaleLineY;
	unsigned long pCallbackAfterScaleY;
	unsigned long pCallbackAfterScaleLineX;
	unsigned long pCallbackAfterScaleX;
	unsigned long pCallbackAfterCurve;
	unsigned long pCallbackAfterCursor;
	unsigned long pCallbackAfterZoomWindow;
} BrbVc4DrawTrendCfgCallbacks_TYP;

typedef struct BrbVc4DrawTrendCfg_TYP
{	struct BrbVc4Drawbox_TYP Drawbox;
	struct BrbVc4DrawPadding_TYP Padding;
	unsigned short nCurveAreaColor;
	struct BrbVc4Font_TYP ScaleFont;
	struct BrbVc4DrawTrendCfgScaleY_TYP ScaleY[2];
	signed long nSourceArrayIndexMax;
	struct BrbVc4DrawTrendCfgScaleX_TYP ScaleX;
	struct BrbVc4DrawTrendCfgTouchAct_TYP TouchAction;
	struct BrbVc4DrawTrendCfgCurve_TYP Curve[4];
	struct BrbVc4DrawTrendCfgCursor_TYP Cursor[2];
	struct BrbVc4DrawTrendCfgCallbacks_TYP Callbacks;
	unsigned long pTag;
} BrbVc4DrawTrendCfg_TYP;

typedef struct BrbVc4DrawTrendStateCurveCur_TYP
{	float rValue;
	struct DTStructure dtsTimeStamp;
} BrbVc4DrawTrendStateCurveCur_TYP;

typedef struct BrbVc4DrawTrendStateCurve_TYP
{	float rValueMax;
	float rValueMin;
	float rValueAverage;
	struct BrbVc4DrawTrendStateCurveCur_TYP Cursor[2];
} BrbVc4DrawTrendStateCurve_TYP;

typedef struct BrbVc4DrawTrendState_TYP
{	enum BrbVc4TrendTouchAction_ENUM eTouchAction;
	struct BrbVc4DrawTrendStateCurve_TYP Curve[4];
} BrbVc4DrawTrendState_TYP;

typedef struct BrbVc4DrawTrendInternScaleY_TYP
{	float rValueScaleFactor;
	float rScaleDistance;
	struct BrbVc4Line_TYP ScaleLine;
} BrbVc4DrawTrendInternScaleY_TYP;

typedef struct BrbVc4DrawTrendInternTouchAc_TYP
{	struct BrbVc4Point_TYP TouchPointDrawbox;
	struct BrbVc4Point_TYP TouchPointCurveArea;
	struct BrbVc4Point_TYP LastScrollPosition;
	struct BrbVc4Point_TYP LastScrollDifference;
	struct TON fbZoomDragDelay;
} BrbVc4DrawTrendInternTouchAc_TYP;

typedef struct BrbVc4DrawTrendInternScaleX_TYP
{	float rZoomScaleFactor;
	float rScaleDistance;
	struct BrbVc4Line_TYP ScaleLine;
	signed long nScrollOffsetMax;
} BrbVc4DrawTrendInternScaleX_TYP;

typedef struct BrbVc4DrawTrendInternCurve_TYP
{	float rValueSum;
} BrbVc4DrawTrendInternCurve_TYP;

typedef struct BrbVc4DrawTrendInternCursor_TYP
{	struct BrbVc4Line_TYP CursorLine;
} BrbVc4DrawTrendInternCursor_TYP;

typedef struct BrbVc4DrawTrendInternZoomWin_TYP
{	plcbit bShow;
	struct BrbVc4Rectangle_TYP RectangleTouch;
	struct BrbVc4Rectangle_TYP RectangleDraw;
} BrbVc4DrawTrendInternZoomWin_TYP;

typedef struct BrbVc4DrawTrendIntern_TYP
{	unsigned short nAccessStatus;
	unsigned short nAttachStatus;
	struct BrbVc4Rectangle_TYP CurveArea;
	struct BrbVc4DrawTrendInternScaleY_TYP ScaleY[2];
	struct BrbVc4DrawTrendInternTouchAc_TYP TouchAction;
	struct BrbVc4DrawTrendInternScaleX_TYP ScaleX;
	struct BrbVc4DrawTrendInternCurve_TYP Curve[4];
	struct BrbVc4DrawTrendInternCursor_TYP Cursor[2];
	struct BrbVc4DrawTrendInternZoomWin_TYP ZoomWindow;
} BrbVc4DrawTrendIntern_TYP;

typedef struct BrbVc4DrawTrend_TYP
{	plcbit bEnable;
	unsigned short nRedrawCounterMatch;
	struct BrbVc4DrawTrendCfg_TYP Cfg;
	struct BrbVc4DrawTrendState_TYP State;
	struct BrbVc4DrawTrendIntern_TYP Intern;
} BrbVc4DrawTrend_TYP;

typedef struct BrbVc4DrawTrendLinkCfgTrend_TYP
{	struct BrbVc4DrawTrend_TYP* pTrend;
} BrbVc4DrawTrendLinkCfgTrend_TYP;

typedef struct BrbVc4DrawTrendLinkCfg_TYP
{	struct BrbVc4DrawTrendLinkCfgTrend_TYP Trend[4];
	plcbit bLinkSetCursor0;
	plcbit bLinkSetCursor1;
	plcbit bLinkZoomX;
	plcbit bLinkZoomY;
	plcbit bLinkScrollX;
	plcbit bLinkScrollY;
} BrbVc4DrawTrendLinkCfg_TYP;

typedef struct BrbVc4LinkTrends_TYP
{	plcbit bEnable;
	struct BrbVc4DrawTrendLinkCfg_TYP Cfg;
} BrbVc4LinkTrends_TYP;

typedef struct BrbVc4DrawPlotCfgScaleY_TYP
{	plcbit bShow;
	unsigned short nColor;
	unsigned short nLinesCount;
	signed long nLineLength;
	struct BrbVc4DrawTrendCfgGrid_TYP Grid;
	float rMin;
	float rMax;
	unsigned short nFractionDigits;
} BrbVc4DrawPlotCfgScaleY_TYP;

typedef struct BrbVc4DrawPlotCfgScaleX_TYP
{	plcbit bShow;
	unsigned short nColor;
	unsigned short nLinesCount;
	signed long nLineLength;
	struct BrbVc4DrawTrendCfgGrid_TYP Grid;
	float rMin;
	float rMax;
	unsigned short nFractionDigits;
} BrbVc4DrawPlotCfgScaleX_TYP;

typedef struct BrbVc4DrawPlotCfgSource_TYP
{	unsigned long nSourceArrayIndexMax;
	enum BrbVc4PlotSource_ENUM eValueSource;
	unsigned long pArrayX;
	unsigned long pArrayY;
	unsigned long pArrayStruct;
	unsigned long nStructSize;
	unsigned long nStructMemberOffsetX;
	unsigned long nStructMemberOffsetY;
} BrbVc4DrawPlotCfgSource_TYP;

typedef struct BrbVc4DrawPlotCfgTouchAct_TYP
{	struct BrbVc4DrawTouchBorderCorr_TYP BorderCorrection;
	plcbit bSetCursor;
	plcbit bZoomX;
	plcbit bZoomY;
	plcbit bScrollX;
	plcbit bScrollY;
} BrbVc4DrawPlotCfgTouchAct_TYP;

typedef struct BrbVc4DrawPlotCfgCursor_TYP
{	unsigned char bShow;
	unsigned short nColor;
	float rCursorX;
	float rCursorY;
} BrbVc4DrawPlotCfgCursor_TYP;

typedef struct BrbVc4DrawPlotCfgCallbacks_TYP
{	unsigned long pCallbackAfterClear;
	unsigned long pCallbackAfterCurveArea;
	unsigned long pCallbackAfterZeroLines;
	unsigned long pCallbackAfterScaleLineY;
	unsigned long pCallbackAfterScaleY;
	unsigned long pCallbackAfterScaleLineX;
	unsigned long pCallbackAfterScaleX;
	unsigned long pCallbackAfterCurve;
	unsigned long pCallbackAfterCursor;
	unsigned long pCallbackAfterZoomWindow;
} BrbVc4DrawPlotCfgCallbacks_TYP;

typedef struct BrbVc4DrawPlotCfg_TYP
{	struct BrbVc4Drawbox_TYP Drawbox;
	struct BrbVc4DrawPadding_TYP Padding;
	unsigned short nCurveAreaColor;
	plcbit bShowZeroLines;
	unsigned short nZeroLinesColor;
	struct BrbVc4Font_TYP ScaleFont;
	struct BrbVc4DrawPlotCfgScaleY_TYP ScaleY;
	struct BrbVc4DrawPlotCfgScaleX_TYP ScaleX;
	struct BrbVc4DrawPlotCfgSource_TYP Source;
	unsigned short nCurveColor;
	struct BrbVc4DrawPlotCfgTouchAct_TYP TouchAction;
	struct BrbVc4DrawPlotCfgCursor_TYP Cursor;
	struct BrbVc4DrawPlotCfgCallbacks_TYP Callbacks;
	unsigned long pTag;
} BrbVc4DrawPlotCfg_TYP;

typedef struct BrbVc4DrawPlotStateStatistic_TYP
{	float rMinX;
	float rMaxX;
	float rMinY;
	float rMaxY;
} BrbVc4DrawPlotStateStatistic_TYP;

typedef struct BrbVc4DrawPlotState_TYP
{	enum BrbVc4PlotTouchAction_ENUM eTouchAction;
	signed long nCursorSampleIndex;
	struct BrbVc4DrawPlotStateStatistic_TYP Statistic;
} BrbVc4DrawPlotState_TYP;

typedef struct BrbVc4DrawPlotInternScaleY_TYP
{	float rValueScaleFactor;
	float rScaleDistance;
	struct BrbVc4Line_TYP ScaleLine;
} BrbVc4DrawPlotInternScaleY_TYP;

typedef struct BrbVc4DrawPlotInternTouchAc_TYP
{	struct BrbVc4Point_TYP TouchPointDrawbox;
	struct BrbVc4Point_TYP TouchPointCurveArea;
	struct BrbVc4Point_TYP LastScrollPosition;
	struct BrbVc4Point_TYP LastScrollDifference;
	struct TON fbZoomDragDelay;
} BrbVc4DrawPlotInternTouchAc_TYP;

typedef struct BrbVc4DrawPlotInternScaleX_TYP
{	float rValueScaleFactor;
	float rScaleDistance;
	struct BrbVc4Line_TYP ScaleLine;
} BrbVc4DrawPlotInternScaleX_TYP;

typedef struct BrbVc4DrawPlotInternCursor_TYP
{	struct BrbVc4Line_TYP CursorLineY;
	struct BrbVc4Line_TYP CursorLineX;
} BrbVc4DrawPlotInternCursor_TYP;

typedef struct BrbVc4DrawPlotInternZoomWin_TYP
{	plcbit bShow;
	struct BrbVc4Rectangle_TYP RectangleTouch;
	struct BrbVc4Rectangle_TYP RectangleDraw;
} BrbVc4DrawPlotInternZoomWin_TYP;

typedef struct BrbVc4DrawPlotIntern_TYP
{	unsigned short nAccessStatus;
	unsigned short nAttachStatus;
	struct BrbVc4Rectangle_TYP CurveArea;
	signed long nCurveAreaRight;
	signed long nCurveAreaBottom;
	struct BrbVc4Line_TYP ZeroLineY;
	struct BrbVc4Line_TYP ZeroLineX;
	struct BrbVc4DrawPlotInternScaleY_TYP ScaleY;
	struct BrbVc4DrawPlotInternScaleX_TYP ScaleX;
	struct BrbVc4DrawPlotInternTouchAc_TYP TouchAction;
	struct BrbVc4DrawPlotInternCursor_TYP Cursor;
	struct BrbVc4DrawPlotInternZoomWin_TYP ZoomWindow;
} BrbVc4DrawPlotIntern_TYP;

typedef struct BrbVc4DrawPlot_TYP
{	plcbit bEnable;
	unsigned short nRedrawCounterMatch;
	struct BrbVc4DrawPlotCfg_TYP Cfg;
	struct BrbVc4DrawPlotState_TYP State;
	struct BrbVc4DrawPlotIntern_TYP Intern;
} BrbVc4DrawPlot_TYP;

typedef struct BrbVc4DrawAxisLinear_TYP
{	plcbit bVertical;
	plcbit bShowDrawArea;
	unsigned long nDrawAreaLeft;
	unsigned long nDrawAreaTop;
	unsigned long nDrawAreaWidth;
	unsigned long nDrawAreaHeight;
	unsigned short nDrawAreaColor;
	unsigned long nDrawIndent;
	signed long nAxisLimitMin;
	signed long nAxisLimitMax;
	plcbit bShowAxisScale;
	unsigned short nAxisScaleCount;
	unsigned short nAxisScaleColor;
	struct BrbVc4Font_TYP AxisScaleFont;
	plcbit bHighlightActPosition;
	unsigned short nAxisScaleHighlightColor;
	plcbit bInverted;
	plcbit bClip;
	unsigned long nAxisClipRange;
	plcbit bShowAxisPosLine;
	plcbit bShowAxisBorder;
	unsigned short nAxisColor;
	enum BrbVc4DrawAxisCaptionOrder_ENUM eAxisCaptionOrder;
	plcbit bShowAxisName;
	plcstring sAxisName[101];
	unsigned short nAxisNameColor;
	struct BrbVc4Font_TYP AxisNameFont;
	plcbit bShowAxisActPosition;
	float rAxisActPosition;
	unsigned short nAxisActPositionColor;
	plcbit bShowAxisActVelocity;
	float rAxisActVelocity;
	unsigned short nAxisActVelocityColor;
	struct BrbVc4Font_TYP AxisValueFont;
	plcbit bShowAxisSetPosition;
	float rAxisSetPosition;
	unsigned short nAxisSetPositionColor;
} BrbVc4DrawAxisLinear_TYP;

typedef struct BrbVc4DrawAxisRadial_TYP
{	plcbit bShowDrawArea;
	unsigned long nDrawAreaLeft;
	unsigned long nDrawAreaTop;
	unsigned long nDrawAreaWidth;
	unsigned long nDrawAreaHeight;
	unsigned short nDrawAreaColor;
	unsigned long nRadius;
	signed long nAxisLimitMin;
	signed long nAxisLimitMax;
	plcbit bShowAxisScale;
	unsigned short nAxisScaleCount;
	unsigned short nAxisScaleColor;
	struct BrbVc4Font_TYP AxisScaleFont;
	plcbit bHighlightActPosition;
	unsigned short nAxisScaleHighlightColor;
	plcbit bInverted;
	unsigned short nOffset;
	plcbit bClip;
	unsigned long nAxisClipRange;
	plcbit bShowAxisPosLine;
	unsigned short nAxisColor;
	enum BrbVc4DrawAxisCaptionOrder_ENUM eAxisCaptionOrder;
	plcbit bShowAxisName;
	plcstring sAxisName[101];
	unsigned short nAxisNameColor;
	struct BrbVc4Font_TYP AxisNameFont;
	plcbit bShowAxisActPosition;
	float rAxisActPosition;
	unsigned short nAxisActPositionColor;
	plcbit bShowAxisActVelocity;
	float rAxisActVelocity;
	unsigned short nAxisActVelocityColor;
	struct BrbVc4Font_TYP AxisValueFont;
	plcbit bShowAxisSetPosition;
	float rAxisSetPosition;
	unsigned short nAxisSetPositionColor;
} BrbVc4DrawAxisRadial_TYP;

typedef struct BrbTimerSwitchParTimePoint_TYP
{	plcbit bActive;
	struct DTStructure dtsTimePoint;
	enum BrbTimerSwitchType_ENUM eSwitchType;
} BrbTimerSwitchParTimePoint_TYP;

typedef struct BrbTimerSwitchPar_TYP
{	enum BrbTimerSwitchMode_ENUM eMode;
	struct BrbTimerSwitchParTimePoint_TYP TimePoint[16];
} BrbTimerSwitchPar_TYP;

typedef struct BrbSaveVarAscii
{
	/* VAR_INPUT (analog) */
	plcstring (*pDevice);
	plcstring (*pFile);
	plcstring (*pVarName);
	unsigned short nLinesToWriteAtOneStep;
	/* VAR_OUTPUT (analog) */
	unsigned long nCharCountMaxPerWrite;
	unsigned short nStatus;
	/* VAR (analog) */
	signed long eStep;
	unsigned short nStatusIntern;
	struct FileDelete fbFileDelete;
	struct FileCreate fbFileCreate;
	plcstring sHelp[521];
	unsigned long nOffset;
	unsigned long nLineCountWrite;
	struct BrbPvInfo_TYP PvInfo[17];
	unsigned short nPvLevel;
	plcstring sLines[50001];
	struct FileWrite fbFileWrite;
	struct FileClose fbFileClose;
	/* VAR (digital) */
	plcbit bVarFinished;
} BrbSaveVarAscii_typ;

typedef struct BrbLoadVarAscii
{
	/* VAR_INPUT (analog) */
	plcstring (*pDevice);
	plcstring (*pFile);
	unsigned short nLinesToReadAtOneStep;
	/* VAR_OUTPUT (analog) */
	unsigned short nStatus;
	/* VAR (analog) */
	signed long eStep;
	unsigned short nStatusIntern;
	struct DatObjInfo fbDatObjInfo;
	struct FileOpen fbFileOpen;
	struct DatObjCreate fbDatObjCreate;
	plcstring (*pTextStart);
	plcstring (*pTextEnd);
	plcstring (*pText);
	unsigned long nLineCountRead;
	struct FileRead fbFileRead;
	struct FileClose fbFileClose;
	struct DatObjDelete fbDatObjDelete;
} BrbLoadVarAscii_typ;

typedef struct BrbSaveVarBin
{
	/* VAR_INPUT (analog) */
	plcstring (*pDevice);
	plcstring (*pFile);
	plcstring (*pVarName);
	/* VAR_OUTPUT (analog) */
	unsigned short nStatus;
	/* VAR (analog) */
	signed long eStep;
	unsigned short nStatusIntern;
	struct BrbPvInfo_TYP PvInfo;
	struct FileDelete fbFileDelete;
	struct FileCreate fbFileCreate;
	struct FileWrite fbFileWrite;
	struct FileClose fbFileClose;
} BrbSaveVarBin_typ;

typedef struct BrbLoadVarBin
{
	/* VAR_INPUT (analog) */
	plcstring (*pDevice);
	plcstring (*pFile);
	plcstring (*pVarName);
	/* VAR_OUTPUT (analog) */
	unsigned short nStatus;
	/* VAR (analog) */
	signed long eStep;
	unsigned short nStatusIntern;
	struct FileOpen fbFileOpen;
	struct BrbPvInfo_TYP PvInfo;
	struct FileRead fbFileRead;
	struct FileClose fbFileClose;
	/* VAR_INPUT (digital) */
	plcbit bAllowBiggerVar;
} BrbLoadVarBin_typ;

typedef struct BrbCheckUsbSticks
{
	/* VAR_OUTPUT (analog) */
	unsigned short nUsbDeviceCount;
	struct BrbUsbDeviceListEntry_TYP UsbDeviceList[6];
	unsigned short nStatus;
	/* VAR (analog) */
	signed long eStep;
	struct BrbMemListManagement_Typ DeviceListMan;
	unsigned short nUsbDeviceIndex;
	struct BrbUsbDeviceListEntry_TYP UsbDeviceListEntry;
	struct UsbNodeListGet fbUsbNodeListGet;
	unsigned short nUsbNodeIndex;
	unsigned long UsbNodeList[6];
	unsigned long nAttachDetachCountOld;
	struct DevUnlink fbDevUnlink;
	struct UsbNodeGet fbUsbNodeGet;
	struct usbNode_typ UsbNodeInfo;
	plcstring sLinkParameter[101];
	struct DevLink fbDevLink;
	/* VAR_INPUT (digital) */
	plcbit bAutoLink;
	/* VAR_OUTPUT (digital) */
	plcbit bUsbDeviceCountChanged;
} BrbCheckUsbSticks_typ;

typedef struct BrbReadDir
{
	/* VAR_INPUT (analog) */
	plcstring (*pDevice);
	plcstring (*pPath);
	enum BrbDirInfoFilter_ENUM eFilter;
	plcstring (*pFileFilter);
	enum BrbFileSorting_ENUM eSorting;
	unsigned long pList;
	unsigned long nListIndexMax;
	/* VAR_OUTPUT (analog) */
	unsigned long nDirCount;
	unsigned long nFileCount;
	unsigned long nTotalCount;
	unsigned short nStatus;
	/* VAR (analog) */
	signed long eStep;
	struct BrbMemListManagement_Typ FilterListMan;
	plcstring FilterList[11][21];
	struct DirRead fbDirRead;
	struct fiDIR_READ_DATA FileInfo;
	struct BrbReadDirListEntry_TYP ReadListEntry;
	struct BrbMemListManagement_Typ FileListMan;
	/* VAR_INPUT (digital) */
	plcbit bWithParentDir;
} BrbReadDir_typ;

typedef struct BrbDeleteFiles
{
	/* VAR_INPUT (analog) */
	plcstring (*pDevice);
	plcstring (*pPath);
	plcstring (*pFileFilter);
	plcdt dtDateStart;
	plcdt dtDateEnd;
	/* VAR_OUTPUT (analog) */
	unsigned long nDeletedFileCount;
	unsigned long nKeptFileCount;
	unsigned short nStatus;
	/* VAR (analog) */
	signed long eStep;
	struct BrbMemListManagement_Typ FilterListMan;
	plcstring FilterList[11][21];
	struct DirRead fbDirRead;
	struct fiDIR_READ_DATA FileInfo;
	plcstring sFileWithPath[260];
	struct FileDelete fbFileDelete;
} BrbDeleteFiles_typ;

typedef struct BrbLoadFileDataObj
{
	/* VAR_INPUT (analog) */
	plcstring (*pDevice);
	plcstring (*pFile);
	plcstring (*pDataObjName);
	unsigned char nDataObjMemType;
	unsigned long nDataObjOption;
	/* VAR_OUTPUT (analog) */
	unsigned short nStatus;
	unsigned long nDataObjIdent;
	unsigned long pDataObjMem;
	unsigned long nDataObjLen;
	/* VAR (analog) */
	signed long eStep;
	unsigned short nStatusIntern;
	struct DatObjInfo fbDatObjInfo;
	struct DatObjDelete fbDatObjDelete;
	struct FileOpen fbFileOpen;
	struct DatObjCreate fbDatObjCreate;
	struct FileRead fbFileRead;
	struct FileClose fbFileClose;
} BrbLoadFileDataObj_typ;

typedef struct BrbSaveFileDataObj
{
	/* VAR_INPUT (analog) */
	plcstring (*pDevice);
	plcstring (*pFile);
	plcstring (*pDataObjName);
	/* VAR_OUTPUT (analog) */
	unsigned short nStatus;
	/* VAR (analog) */
	signed long eStep;
	unsigned short nStatusIntern;
	struct DatObjInfo fbDatObjInfo;
	struct FileInfo fbFileInfo;
	struct fiFILE_INFO FileInfo;
	struct FileDelete fbFileDelete;
	struct FileCreate fbFileCreate;
	struct FileWrite fbFileWrite;
	struct FileClose fbFileClose;
} BrbSaveFileDataObj_typ;

typedef struct BrbTimerSwitch
{
	/* VAR_INPUT (analog) */
	struct DTStructure* pUserTime;
	struct BrbTimerSwitchPar_TYP Parameter;
	/* VAR_OUTPUT (analog) */
	struct DTStructure dtsUsedTime;
	unsigned long nSwitchCount;
	/* VAR (analog) */
	struct DTStructureGetTime fbDTStructureGetTime;
	struct TON fbLock;
	/* VAR_INPUT (digital) */
	plcbit bEnable;
	plcbit bCmdSwitchOff;
	plcbit bCmdSwitchOn;
	plcbit bCmdToggle;
	/* VAR_OUTPUT (digital) */
	plcbit bOut;
	/* VAR (digital) */
	plcbit bLocked;
	plcbit bClearOutOnNextCycle;
} BrbTimerSwitch_typ;

typedef struct BrbDebounceInput
{
	/* VAR_INPUT (analog) */
	unsigned long nDebounceTime;
	/* VAR (analog) */
	struct TON fbDebounce;
	/* VAR_INPUT (digital) */
	plcbit bInput;
	/* VAR_OUTPUT (digital) */
	plcbit bOutput;
} BrbDebounceInput_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void BrbSaveVarAscii(struct BrbSaveVarAscii* inst);
_BUR_PUBLIC void BrbLoadVarAscii(struct BrbLoadVarAscii* inst);
_BUR_PUBLIC void BrbSaveVarBin(struct BrbSaveVarBin* inst);
_BUR_PUBLIC void BrbLoadVarBin(struct BrbLoadVarBin* inst);
_BUR_PUBLIC void BrbCheckUsbSticks(struct BrbCheckUsbSticks* inst);
_BUR_PUBLIC void BrbReadDir(struct BrbReadDir* inst);
_BUR_PUBLIC void BrbDeleteFiles(struct BrbDeleteFiles* inst);
_BUR_PUBLIC void BrbLoadFileDataObj(struct BrbLoadFileDataObj* inst);
_BUR_PUBLIC void BrbSaveFileDataObj(struct BrbSaveFileDataObj* inst);
_BUR_PUBLIC void BrbTimerSwitch(struct BrbTimerSwitch* inst);
_BUR_PUBLIC void BrbDebounceInput(struct BrbDebounceInput* inst);
_BUR_PUBLIC signed long BrbStepHandler(struct BrbStepHandling_TYP* pStepHandling);
_BUR_PUBLIC unsigned short BrbStartStepTimeout(struct BrbStepHandling_TYP* pStepHandling, unsigned long nTimeout, signed long nContinueStep);
_BUR_PUBLIC unsigned short BrbStopStepTimeout(struct BrbStepHandling_TYP* pStepHandling);
_BUR_PUBLIC plcbit BrbStartStopWatch(struct BrbStopWatch_TYP* pStopWatch);
_BUR_PUBLIC unsigned long BrbStopStopWatch(struct BrbStopWatch_TYP* pStopWatch);
_BUR_PUBLIC BrbCallerStates_ENUM BrbSetCaller(struct BrbCaller_TYP* pCaller, signed long nCallerId);
_BUR_PUBLIC unsigned short BrbClearDirectBox(unsigned long pDirectBox, unsigned long nSize);
_BUR_PUBLIC unsigned short BrbClearCallerBox(unsigned long pCallerBox, unsigned long nSize);
_BUR_PUBLIC unsigned short BrbCheckFileName(plcstring* pFileName);
_BUR_PUBLIC plcbit BrbCheckFileEnding(plcstring* pFileName, plcstring* pEnding);
_BUR_PUBLIC unsigned short BrbCombinePath(plcstring* pPath, plcstring* pFilename, plcstring* pFilenameWithPath);
_BUR_PUBLIC plcbit BrbVc4HandleGeneral(struct BrbVc4General_TYP* pVisGeneral);
_BUR_PUBLIC unsigned short BrbVc4HandleChangePage(struct BrbVc4PageHandling_TYP* pPageHandling);
_BUR_PUBLIC plcbit BrbVc4ChangePage(struct BrbVc4PageHandling_TYP* pPageHandling, signed long nIndex, plcbit bStoreLastPage);
_BUR_PUBLIC plcbit BrbVc4ChangePageBack(struct BrbVc4PageHandling_TYP* pPageHandling);
_BUR_PUBLIC plcbit BrbVc4HandleScreenSaver(struct BrbVc4ScreenSaver_TYP* pScreenSaver, struct BrbVc4General_TYP* pGeneral, struct BrbVc4PageHandling_TYP* pPageHandling);
_BUR_PUBLIC plcbit BrbVc4SetControlEnability(unsigned short* pStatus, plcbit bEnable);
_BUR_PUBLIC plcbit BrbVc4IsControlEnabled(unsigned short nStatus);
_BUR_PUBLIC plcbit BrbVc4SetControlVisibility(unsigned short* pStatus, plcbit bVisible);
_BUR_PUBLIC plcbit BrbVc4IsControlVisible(unsigned short nStatus);
_BUR_PUBLIC plcbit BrbVc4SetControlFocus(unsigned short* pStatus, plcbit bFocus);
_BUR_PUBLIC plcbit BrbVc4HasControlFocus(unsigned short nStatus);
_BUR_PUBLIC plcbit BrbVc4IsControlInputActive(unsigned short nStatus);
_BUR_PUBLIC plcbit BrbVc4OpenTouchpad(unsigned short* pStatus);
_BUR_PUBLIC plcbit BrbVc4CloseTouchpad(unsigned short* pStatus);
_BUR_PUBLIC plcbit BrbVc4IsTouchpadOpen(unsigned short nStatus);
_BUR_PUBLIC unsigned short BrbVc4SetControlColor(unsigned short* pColorDatapoint, plcbit bCondition, unsigned short nColorTrue, unsigned short nColorFalse);
_BUR_PUBLIC unsigned short BrbVc4HandleAnimation(struct BrbVc4Animation_TYP* pAnimation);
_BUR_PUBLIC plcbit BrbVc4HandleButton(struct BrbVc4Button_TYP* pButton);
_BUR_PUBLIC plcbit BrbVc4HandleCheckbox(struct BrbVc4Checkbox_TYP* pCheckbox);
_BUR_PUBLIC plcbit BrbVc4HandleDropdown(struct BrbVc4Dropdown_TYP* pDropdown);
_BUR_PUBLIC BrbVc4HwPosSwitchPositions_ENUM BrbVc4HandleHwPosSwitch2(struct BrbVc4HwPosSwitch2_TYP* pPosSwitch);
_BUR_PUBLIC plcbit BrbVc4HandleHwSafetyButton(struct BrbVc4HwSafetyButton_TYP* pSafetyButton);
_BUR_PUBLIC plcbit BrbVc4HandleIncButton(struct BrbVc4IncButton_TYP* pButton);
_BUR_PUBLIC plcbit BrbVc4HandleJogButton(struct BrbVc4JogButton_TYP* pButton);
_BUR_PUBLIC plcbit BrbVc4HandleOptionbox(struct BrbVc4Optionbox_TYP* pOptionbox);
_BUR_PUBLIC plcbit BrbVc4HandleScrollbarHorizontal(struct BrbVc4ScrollbarHor_TYP* pScrollbar, signed long* pScrollOffset);
_BUR_PUBLIC plcbit BrbVc4HandleScrollbarVertical(struct BrbVc4ScrollbarVer_TYP* pScrollbar, signed long* pScrollOffset);
_BUR_PUBLIC plcbit BrbVc4HandleTabCtrl(struct BrbVc4TabCtrl_TYP* pTabCtrl);
_BUR_PUBLIC plcbit BrbVc4HandleToggleButton(struct BrbVc4ToggleButton_TYP* pButton);
_BUR_PUBLIC plcbit BrbVc4HandleToggleButtonExt(struct BrbVc4ToggleButtonExt_TYP* pButton);
_BUR_PUBLIC unsigned short BrbVc4HandleTouchGrid(struct BrbVc4Touchgrid_TYP* pTouchgrid, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawLine(struct BrbVc4Line_TYP* pLine, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawLineCorr(struct BrbVc4Line_TYP* pLine, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawLineClip(struct BrbVc4Line_TYP* pLine, struct BrbVc4Rectangle_TYP* pClip, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawRectangle(struct BrbVc4Rectangle_TYP* pRectangle, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawRectangleCorr(struct BrbVc4Rectangle_TYP* pRectangle, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawRectangleClip(struct BrbVc4Rectangle_TYP* pRectangle, struct BrbVc4Rectangle_TYP* pClip, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawEllipse(struct BrbVc4Ellipse_TYP* pEllipse, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawArc(struct BrbVc4Arc_TYP* pArc, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawText(struct BrbVc4DrawText_TYP* pText, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawTrend(struct BrbVc4DrawTrend_TYP* pTrend, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC signed long BrbVc4GetTrendDisplayCoordinateY(struct BrbVc4DrawTrend_TYP* pTrend, enum BrbVc4TrendScaleYIndex_ENUM eScaleY, float rValue);
_BUR_PUBLIC signed long BrbVc4GetTrendDisplayCoordinateX(struct BrbVc4DrawTrend_TYP* pTrend, signed long nSampleIndex);
_BUR_PUBLIC unsigned short BrbVc4TrendCallbackAfterClear(struct BrbVc4DrawTrend_TYP* pTrend);
_BUR_PUBLIC unsigned short BrbVc4TrendCallbackAfterCrveArea(struct BrbVc4DrawTrend_TYP* pTrend);
_BUR_PUBLIC unsigned short BrbVc4TrendCallbackAfterScaleY(struct BrbVc4DrawTrend_TYP* pTrend, enum BrbVc4TrendScaleYIndex_ENUM eScaleYIndex);
_BUR_PUBLIC unsigned short BrbVc4TrendCallbackAfterScaleX(struct BrbVc4DrawTrend_TYP* pTrend);
_BUR_PUBLIC unsigned short BrbVc4TrendCallbackAfterCurve(struct BrbVc4DrawTrend_TYP* pTrend, unsigned short nCurveIndex);
_BUR_PUBLIC unsigned short BrbVc4TrendCallbackAfterCursor(struct BrbVc4DrawTrend_TYP* pTrend, unsigned short nCursorIndex);
_BUR_PUBLIC unsigned short BrbVc4TrendCallbackAfterZoomWind(struct BrbVc4DrawTrend_TYP* pTrend);
_BUR_PUBLIC unsigned short BrbVc4LinkTrends(struct BrbVc4LinkTrends_TYP* pLinkTrends, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4DrawPlot(struct BrbVc4DrawPlot_TYP* pPlot, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC signed long BrbVc4GetPlotDisplayCoordinateY(struct BrbVc4DrawPlot_TYP* pPlot, float rValue);
_BUR_PUBLIC signed long BrbVc4GetPlotDisplayCoordinateX(struct BrbVc4DrawPlot_TYP* pPlot, float rValue);
_BUR_PUBLIC float BrbVc4DrawAxisLinear(struct BrbVc4DrawAxisLinear_TYP* pAxis, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC float BrbVc4DrawAxisRadial(struct BrbVc4DrawAxisRadial_TYP* pAxis, struct BrbVc4General_TYP* pGeneral);
_BUR_PUBLIC unsigned short BrbVc4CorrectLine(struct BrbVc4Line_TYP* pLine);
_BUR_PUBLIC unsigned short BrbVc4ClipLine(struct BrbVc4Line_TYP* pLine, struct BrbVc4Rectangle_TYP* pClip);
_BUR_PUBLIC unsigned short BrbVc4CorrectRectangle(struct BrbVc4Rectangle_TYP* pRectangle);
_BUR_PUBLIC unsigned short BrbVc4ClipRectangle(struct BrbVc4Rectangle_TYP* pRectangle, struct BrbVc4Rectangle_TYP* pClip);
_BUR_PUBLIC plcbit BrbVc4IsPointWithinRectangle(signed long nPointX, signed long nPointY, struct BrbVc4Rectangle_TYP* pRectangle);
_BUR_PUBLIC unsigned short BrbGetTimeText(struct RTCtime_typ* pTime, plcstring* pText, unsigned long nTextSize, plcstring* pFormat);
_BUR_PUBLIC unsigned short BrbGetCurrentTimeText(plcstring* pText, unsigned long nTextSize, plcstring* pFormat);
_BUR_PUBLIC unsigned short BrbGetTimeTextDtStruct(struct DTStructure* pTime, plcstring* pText, unsigned long nTextSize, plcstring* pFormat);
_BUR_PUBLIC unsigned short BrbGetTimeTextDt(plcdt dtTime, plcstring* pText, unsigned long nTextSize, plcstring* pFormat);
_BUR_PUBLIC plcdt BrbGetDtFromTimeText(plcstring* pTimeText, plcstring* pFormat);
_BUR_PUBLIC unsigned short BrbRtcTimeToDtStruct(struct RTCtime_typ* pRtcTime, struct DTStructure* pDtStruct);
_BUR_PUBLIC plcbit BrbDtStructCompare(struct DTStructure* pDtStruct1, enum BrbTimeAndDateCompare_ENUM eCompare, struct DTStructure* pDtStruct2);
_BUR_PUBLIC plcdt BrbDtStructAddSeconds(struct DTStructure* pDtStruct, signed long nSeconds);
_BUR_PUBLIC plcdt BrbDtStructAddMilliseconds(struct DTStructure* pDtStruct, signed long nMilliseconds);
_BUR_PUBLIC unsigned short BrbUdintToAscii(unsigned long nValue, plcstring* pText);
_BUR_PUBLIC unsigned long BrbAsciiToUdint(plcstring* pText);
_BUR_PUBLIC unsigned short BrbUdintToHex(unsigned long nValue, plcstring* pHex, unsigned long nHexSize, plcbit bWithPraefix);
_BUR_PUBLIC unsigned long BrbHexToUdint(plcstring* pHex);
_BUR_PUBLIC plcbit BrbAsciiFieldToString(unsigned char* pAsciiField, unsigned long nAsciiFieldLen, unsigned long nFinalAsciiCharCount, plcstring* pText, unsigned long nTextSize);
_BUR_PUBLIC signed long BrbStringGetIndexOf(plcstring* pText, plcstring* pFind, unsigned long nTextLen);
_BUR_PUBLIC signed long BrbStringGetLastIndexOf(plcstring* pText, plcstring* pFind, unsigned long nTextLen);
_BUR_PUBLIC plcstring* BrbStringGetAdrOf(plcstring* pText, plcstring* pFind, unsigned long nTextLen);
_BUR_PUBLIC plcstring* BrbStringGetLastAdrOf(plcstring* pText, plcstring* pFind, unsigned long nTextLen);
_BUR_PUBLIC plcbit BrbStringStartsWith(plcstring* pText, plcstring* pFind);
_BUR_PUBLIC plcbit BrbStringEndsWith(plcstring* pText, plcstring* pFind);
_BUR_PUBLIC plcstring* BrbStringGetSubText(plcstring* pText, unsigned long nIndex, unsigned long nLen, plcstring* pSubText);
_BUR_PUBLIC plcstring* BrbStringGetSubTextByLen(plcstring* pStart, unsigned long nLen, plcstring* pSubText);
_BUR_PUBLIC plcstring* BrbStringGetSubTextByAdr(plcstring* pStart, plcstring* pEnd, plcstring* pSubText);
_BUR_PUBLIC plcstring* BrbStringCut(plcstring* pText, unsigned long nCutIndex, unsigned long nCutLen, plcstring* pCut);
_BUR_PUBLIC unsigned long BrbStringCutFromLastSeparator(plcstring* pText, plcstring* pSeparator, plcstring* pCut);
_BUR_PUBLIC plcstring* BrbStringInsert(plcstring* pText, unsigned long nInsertIndex, plcstring* pInsert);
_BUR_PUBLIC unsigned long BrbStringReplace(plcstring* pText, plcstring* pFind, plcstring* pReplace);
_BUR_PUBLIC plcbit BrbStringPadLeft(plcstring* pText, plcstring* pFillChar, unsigned long nLen);
_BUR_PUBLIC plcbit BrbStringPadRight(plcstring* pText, plcstring* pFillChar, unsigned long nLen);
_BUR_PUBLIC plcbit BrbStringTrimLeft(plcstring* pText, plcstring* pTrim);
_BUR_PUBLIC plcbit BrbStringTrimRight(plcstring* pText, plcstring* pTrim);
_BUR_PUBLIC unsigned long BrbStringSplit(plcstring* pText, plcstring* pSep, unsigned long pSplitArray, unsigned long nArrayIndexMax, unsigned long nEntrySize);
_BUR_PUBLIC plcbit BrbStringConvertRealFromExp(plcstring* pValue, plcstring* pResult, unsigned long nResultSize);
_BUR_PUBLIC plcbit BrbStringConvertRealToExp(plcstring* pValue, plcstring* pResult, unsigned long nResultSize);
_BUR_PUBLIC plcbit BrbStringFormatFractionDigits(plcstring* pValue, unsigned long nValueSize, unsigned short nFractionsDigits);
_BUR_PUBLIC plcbit BrbStringSwap(plcstring* pText, plcstring* pSwapped, unsigned long nSwappedSize);
_BUR_PUBLIC plcbit BrbStringToUpper(plcstring* pText);
_BUR_PUBLIC plcbit BrbStringToLower(plcstring* pText);
_BUR_PUBLIC plcbit BrbStringIsNumerical(plcstring* pText);
_BUR_PUBLIC unsigned long BrbXmlGetTagText(plcstring* pStartTag, plcstring* pEndTag, unsigned long pStart, unsigned long pEnd, plcstring* pText, unsigned long nTextSize);
_BUR_PUBLIC unsigned long BrbXmlGetNextTag(plcstring* pTag, unsigned long pStart, unsigned long pEnd);
_BUR_PUBLIC signed long BrbMemListClear(struct BrbMemListManagement_Typ* pListManagement);
_BUR_PUBLIC signed long BrbMemListIn(struct BrbMemListManagement_Typ* pListManagement, unsigned long nIndex, unsigned long pNewEntry);
_BUR_PUBLIC signed long BrbMemListOut(struct BrbMemListManagement_Typ* pListManagement, unsigned long nIndex, unsigned long pListEntry);
_BUR_PUBLIC signed long BrbMemListGetEntry(struct BrbMemListManagement_Typ* pListManagement, unsigned long nIndex, unsigned long pListEntry);
_BUR_PUBLIC signed long BrbFifoIn(struct BrbMemListManagement_Typ* pListManagement, unsigned long pNewEntry);
_BUR_PUBLIC signed long BrbFifoOut(struct BrbMemListManagement_Typ* pListManagement, unsigned long pListEntry);
_BUR_PUBLIC signed long BrbLifoIn(struct BrbMemListManagement_Typ* pListManagement, unsigned long pNewEntry);
_BUR_PUBLIC signed long BrbLifoOut(struct BrbMemListManagement_Typ* pListManagement, unsigned long pListEntry);
_BUR_PUBLIC plcbit BrbGetBitUdint(unsigned long nValue, unsigned short nBitNumber);
_BUR_PUBLIC plcbit BrbSetBitUdint(unsigned long* pValue, unsigned short nBitNumber, plcbit bBit);
_BUR_PUBLIC plcbit BrbGetBitMaskUdint(unsigned long nValue, unsigned long nBitMask);
_BUR_PUBLIC plcbit BrbSetBitMaskUdint(unsigned long* pValue, unsigned long nBitMask, plcbit bSet);
_BUR_PUBLIC plcbit BrbGetBitUint(unsigned short nValue, unsigned short nBitNumber);
_BUR_PUBLIC plcbit BrbSetBitUint(unsigned short* pValue, unsigned short nBitNumber, plcbit bBit);
_BUR_PUBLIC plcbit BrbGetBitMaskUint(unsigned short nValue, unsigned short nBitMask);
_BUR_PUBLIC plcbit BrbSetBitMaskUint(unsigned short* pValue, unsigned short nBitMask, plcbit bSet);
_BUR_PUBLIC plcbit BrbGetBitUsint(unsigned char nValue, unsigned short nBitNumber);
_BUR_PUBLIC plcbit BrbSetBitUsint(unsigned char* pValue, unsigned short nBitNumber, plcbit bBit);
_BUR_PUBLIC plcbit BrbGetBitMaskUsint(unsigned char nValue, unsigned char nBitMask);
_BUR_PUBLIC plcbit BrbSetBitMaskUsint(unsigned char* pValue, unsigned char nBitMask, plcbit bSet);
_BUR_PUBLIC float BrbGetRandomPercent(void);
_BUR_PUBLIC plcbit BrbGetRandomBool(void);
_BUR_PUBLIC unsigned long BrbGetRandomUdint(unsigned long nMin, unsigned long nMax);
_BUR_PUBLIC signed long BrbGetRandomDint(signed long nMin, signed long nMax);
_BUR_PUBLIC float BrbGetAngleRad(float rAngleDeg);
_BUR_PUBLIC float BrbGetAngleDeg(float rAngleRad);
_BUR_PUBLIC float BrbNormalizeAngleRad(float rAngleRad);
_BUR_PUBLIC float BrbNormalizeAngleDeg(float rAngleDeg, unsigned char bKeep360);
_BUR_PUBLIC plcbit BrbCheckIpAddress(plcstring* pIpAddress);
_BUR_PUBLIC signed long BrbRoundDint(signed long nValue, enum BrbRound_ENUM eRound, unsigned char nDigits);
_BUR_PUBLIC unsigned short BrbGetStructMemberOffset(plcstring* pStructName, plcstring* pMemberName, unsigned long* pOffset);


#ifdef __cplusplus
};
#endif
#endif /* _BRBLIB_ */

