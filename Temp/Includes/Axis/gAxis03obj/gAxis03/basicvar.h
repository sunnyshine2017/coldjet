/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273702_78_
#define _BUR_1500273702_78_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variablen */
_BUR_LOCAL struct ACP10AXIS_typ *pAxisStrukt;
_BUR_LOCAL float SwLimitValue;
_BUR_LOCAL float ActPosition;
_BUR_LOCAL float Distance;
_BUR_LOCAL plcbit SwLimitAvailable;
_BUR_LOCAL plcstring sErrTxt[81];
_BUR_LOCAL unsigned char nAxisStep;
_BUR_LOCAL unsigned long nAxisAdr;
_BUR_LOCAL struct MC_Halt MC_Halt_0;
_BUR_LOCAL struct MC_BR_InitAxisSubjectPar MC_BR_InitAxisSubjectPar_0;
_BUR_LOCAL struct MC_BR_ReadDriveStatus MC_BR_ReadDriveStatus_0;
_BUR_LOCAL struct MC_ReadActualVelocity MC_ReadActualVelocity_0;
_BUR_LOCAL struct MC_ReadActualPosition MC_ReadActualPosition_0;
_BUR_LOCAL struct MC_ReadStatus MC_ReadStatus_0;
_BUR_LOCAL struct MC_Reset MC_Reset_0;
_BUR_LOCAL struct MC_ReadAxisError MC_ReadAxisError_0;
_BUR_LOCAL struct MC_MoveAdditive MC_MoveAdditive_0;
_BUR_LOCAL struct MC_MoveAbsolute MC_MoveAbsolute_0;
_BUR_LOCAL struct MC_MoveVelocity MC_MoveVelocity_0;
_BUR_LOCAL struct MC_Stop MC_Stop_0;
_BUR_LOCAL struct MC_Home MC_Home_0;
_BUR_LOCAL struct MC_Power MC_Power_0;
_BUR_LOCAL struct basic_typ BasicControl;
_BUR_LOCAL unsigned short StatusGetSwLimit;





__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Axis/BasicAxisTask/Basic/basic.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/Acp10_MC/acp10_mc.fun\\\" scope \\\"global\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273702_78_ */

