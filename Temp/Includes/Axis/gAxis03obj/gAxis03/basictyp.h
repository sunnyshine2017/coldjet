/* Durch Automation Studio generierte Headerdatei*/
/* Nicht bearbeiten! */

#ifndef _BUR_1500273702_77_
#define _BUR_1500273702_77_

#include <bur/plctypes.h>

/* Datentypen und Datentypen von Funktionsblöcken */
typedef enum Step_ENUM
{	STATE_WAIT,
	STATE_POWER_ON,
	STATE_READY = 100,
	STATE_HOME,
	STATE_HALT,
	STATE_STOP,
	STATE_JOG_POSITIVE,
	STATE_JOG_NEGATIVE,
	STATE_MOVE_ABSOLUTE,
	STATE_MOVE_ADDITIVE,
	STATE_MOVE_VELOCITY,
	STATE_ERROR_AXIS = 200,
	STATE_ERROR,
	STATE_ERROR_RESET
} Step_ENUM;

typedef struct basic_typ
{	struct Basic_command_TYP* Command;
	struct Basic_parameter_TYP* Parameter;
	struct Basic_status_TYP* Status;
	struct Basic_axisState_TYP* AxisState;
} basic_typ;






__asm__(".section \".plc\"");

/* Verwendete IEC Dateien */
__asm__(".ascii \"iecfile \\\"Logical/Axis/BasicAxisTask/Basic/basic.typ\\\" scope \\\"local\\\"\\n\"");

/* Exportierte Bibliotheksfunktionen und Funktionsblöcke */

__asm__(".previous");


#endif /* _BUR_1500273702_77_ */

