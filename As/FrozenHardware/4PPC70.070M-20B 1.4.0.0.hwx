<?xml version="1.0" encoding="UTF-8"?>
<Module xmlns="http://br-automation.co.at/AS/HardwareModule"
        Version="1.4.0.0"
        Description-de="C70 TFT WVGA 7.0in P/B, PLK, X2X"
        Description-en="C70 TFT WVGA 7.0in P/B, PLK, X2X"
        Width="140"
        Height="198"
        Terminal="true"
        Custom="false">
  <Aprol>
      <Config ID="Supported" FromVersion="APROL R 3.3-70" Recommended="true"/>
   </Aprol>
  <Channels Target="SG4">
      <Channel ID="SerialNumber"
               HwdResource="IO"
               Description-de="Seriennummer"
               Description-en="Serial number">
         <Parameter ID="Ext" Value="/HW" Type="STRING"/>
         <Parameter ID="Address" Value="0" Type="UDINT"/>
         <Parameter ID="Direction" Value="IN" Type="STRING"/>
         <Parameter ID="Type" Value="UDINT" Type="STRING"/>
         <Parameter ID="Sig_St_Udint" Value="1" Type="STRING"/>
         <Parameter ID="ChannelType" Value="SerialNumber" Type="STRING"/>
      </Channel>
      <Channel ID="ModuleID"
               HwdResource="IO"
               Description-de="Modulkennung"
               Description-en="Module ID">
         <Parameter ID="Ext" Value="/HW" Type="STRING"/>
         <Parameter ID="Address" Value="1" Type="UDINT"/>
         <Parameter ID="Direction" Value="IN" Type="STRING"/>
         <Parameter ID="Type" Value="UINT" Type="STRING"/>
         <Parameter ID="Sig_St_Uint" Value="1" Type="STRING"/>
         <Parameter ID="ChannelType" Value="ModuleID" Type="STRING"/>
      </Channel>
      <Channel ID="HardwareVariant"
               HwdResource="IO"
               FromVersion="A4.23"
               Description-de="Hardware Variante"
               Description-en="Hardware variant">
         <Parameter ID="Ext" Value="/HW"/>
         <Parameter ID="Address" Value="15" Type="UDINT"/>
         <Parameter ID="Direction" Value="IN"/>
         <Parameter ID="Type" Value="UINT"/>
         <Parameter ID="ChannelType" Value="HardwareVariant" Type="STRING"/>
      </Channel>
      <Channel ID="FirmwareVersion"
               HwdResource="IO"
               FromVersion="A4.23"
               Description-de="Firmware Version"
               Description-en="Frimware version">
         <Parameter ID="Ext" Value="/HW"/>
         <Parameter ID="Address" Value="16" Type="UDINT"/>
         <Parameter ID="Direction" Value="IN"/>
         <Parameter ID="Type" Value="UINT"/>
         <Parameter ID="ChannelType" Value="FirmwareVersion" Type="STRING"/>
      </Channel>
      <Separator/>
      <Channel ID="TemperatureCPU"
               HwdResource="IO"
               Description-de="Temperatur CPU [0,1°C]"
               Description-en="Temperature CPU [0.1°C]">
         <Parameter ID="Ext" Value="/HW" Type="STRING"/>
         <Parameter ID="Address" Value="4" Type="UDINT"/>
         <Parameter ID="Direction" Value="IN" Type="STRING"/>
         <Parameter ID="Type" Value="UINT" Type="STRING"/>
         <Parameter ID="Sig_Temp_CpuTemp" Value="1" Type="STRING"/>
         <Parameter ID="ChannelType" Value="Temp" Type="STRING"/>
         <Parameter ID="Ordinal" Value="2" Type="UDINT"/>
      </Channel>
      <Channel ID="TemperatureENV"
               HwdResource="IO"
               Description-de="Temperatur Kühlblech [0,1°C]"
               Description-en="Temperature cooling plate [0.1°C]">
         <Parameter ID="Ext" Value="/HW" Type="STRING"/>
         <Parameter ID="Address" Value="5" Type="UDINT"/>
         <Parameter ID="Direction" Value="IN" Type="STRING"/>
         <Parameter ID="Type" Value="UINT" Type="STRING"/>
         <Parameter ID="Sig_Temp_CpuTemp" Value="1" Type="STRING"/>
         <Parameter ID="ChannelType" Value="Temp" Type="STRING"/>
         <Parameter ID="Ordinal" Value="1" Type="UDINT"/>
      </Channel>
      <Separator/>
      <Channel ID="SystemTime"
               HwdResource="IO"
               Description-de="Systemzeit beim Start des aktuellen Taskklassenzyklus [µs]"
               Description-en="System time at the start of the current task cycle [µs]">
         <Parameter ID="Ext" Value="/HW" Type="STRING"/>
         <Parameter ID="Address" Value="9" Type="UDINT"/>
         <Parameter ID="Direction" Value="IN" Type="STRING"/>
         <Parameter ID="Type" Value="DINT" Type="STRING"/>
         <Parameter ID="Sig_St_Dint" Value="1" Type="STRING"/>
         <Parameter ID="ChannelType" Value="SystemTime" Type="STRING"/>
      </Channel>
      <Separator/>
   </Channels>
  <Classification>
      <CpuType Value="PPC7x"/>
      <DisplaySize Value="WVGA7"/>
      <DisplayType Value="Graphic"/>
      <DisplayType Value="Keys"/>
      <DisplayType Value="Touch"/>
      <HardwareModuleFamily Value="PowerPanel"/>
      <HardwareModuleType Value="OperatorInterface"/>
      <HardwareModuleType Value="CPU"/>
      <HardwareModuleType Value="Display"/>
      <NetworkType Value="Ethernet"/>
      <NetworkType Value="Powerlink"/>
      <NetworkType Value="X2X"/>
      <WizardType Value="NewConfiguration"/>
   </Classification>
  <Connectors>
      <Connector Name="IF1"
                 BuiltIn="true"
                 DaisyChain="true"
                 Label="PLK"
                 LastNumber="253"
                 Type="Cable">
         <Channels Target="SG4">
            <Channel ID="LinkOk"
                     HwdResource="IF1"
                     Description-de="Link-Status"
                     Description-en="Link state">
               <Parameter ID="Ext" Value="/INFO" Type="STRING"/>
               <Parameter ID="Address" Value="0x8000001A" Type="UDINT"/>
               <Parameter ID="BitOffset" Value="0" Type="UDINT"/>
               <Parameter ID="Direction" Value="IN" Type="STRING"/>
               <Parameter ID="Type" Value="BOOL" Type="STRING"/>
               <Parameter ID="Sig_St_Bool" Value="1" Type="STRING"/>
            </Channel>
            <Channel ID="OutputDMAMargin"
                     HwdResource="IF1"
                     Description-de="Abstand zur Ouput Zyklus Deadline in Mikrosekunden"
                     Description-en="Margin to output cycle deadline in microseconds">
               <Filter Value="(NOT (Operation='EthOnly'))"/>
               <Parameter ID="Ext" Value="/INFO" Type="STRING"/>
               <Parameter ID="Address" Value="0x8000000C" Type="UDINT"/>
               <Parameter ID="Direction" Value="IN" Type="STRING"/>
               <Parameter ID="Type" Value="DINT" Type="STRING"/>
               <Parameter ID="Sig_St_Dint" Value="1" Type="STRING"/>
            </Channel>
            <Include File="ChannelsPwl-1" HwdResource="IF3"/>
         </Channels>
         <Connections>
            <Connection Name="Ethernet"/>
            <Connection Name="Powerlink"/>
         </Connections>
         <Editors>
            <Editor Name="Configuration" Target="SG4"/>
            <Editor Name="IoMapping" Target="SG4"/>
         </Editors>
         <Properties Target="SG4">
            <Include File="ArCfgEplV2ForceNodeType4noHWswitch"/>
         </Properties>
      </Connector>
      <Connector Name="IF2"
                 BuiltIn="true"
                 DaisyChain="true"
                 Label="ETH"
                 LastNumber="254"
                 Male="true"
                 Type="Cable"
                 NodeSwitch="true">
         <Connections>
            <Connection Name="Ethernet"/>
         </Connections>
         <Editors>
            <Editor Name="Configuration" Target="SG4"/>
            <Editor Name="IoMapping" Target="SG4"/>
         </Editors>
         <Properties Target="SG4">
            <Include File="ArCfgEthTceiInaNode"/>
            <Parameter ID="DeviceParam"
                       Value="/EDDN=tcei"
                       Visible="false"
                       SysConfPath="DEVICES:%NW_ADDRESS%:DeviceParam"
                       ArConfigSkip="true"/>
         </Properties>
      </Connector>
      <Connector Name="IF3"
                 BuiltIn="true"
                 Label="USB"
                 LastNumber="1"
                 Male="true"
                 MaxConnections="1"
                 Type="Cable">
         <Connections>
            <Connection Name="USB"/>
         </Connections>
      </Connector>
      <Connector Name="IF4"
                 BuiltIn="true"
                 Label="USB"
                 LastNumber="1"
                 Male="true"
                 MaxConnections="1"
                 Type="Cable">
         <Connections>
            <Connection Name="USB"/>
         </Connections>
      </Connector>
      <Connector Name="IF5"
                 BuiltIn="true"
                 DaisyChain="true"
                 Label="X2X"
                 LastNumber="253"
                 Type="Cable">
         <Channels>
            <Include File="ChannelsX2X"/>
         </Channels>
         <Connections>
            <Connection Name="X2X"/>
         </Connections>
         <Editors>
            <Editor Name="Configuration" Target="SG4"/>
            <Editor Name="IoMapping" Target="SG4"/>
         </Editors>
         <Properties Target="SG4">
            <Include File="ArCfgX2XPPC70x"/>
         </Properties>
      </Connector>
   </Connectors>
  <Display ResolutionX="800" ResolutionY="480" RotationAngle="90">
      <Abilities Bitmaps="true"
                 Circles="true"
                 Lines="true"
                 Pixels="true"
                 Fonts="true"
                 Transparent="true"/>
      <Touch Format="Analog"
             PositionX="72.3"
             PositionY="67"
             FirstTouchKey="0"
             Columns="8"
             Rows="6"/>
   </Display>
  <EcadData>
      <ArticleData TypeNr="4PPC70.070M-20B"
                   ShortDescription-de="C70 TFT WVGA 7.0in P/B, PLK, X2X"
                   ShortDescription-en="C70 TFT WVGA 7.0in P/B, PLK, X2X"
                   Description-de="Power Panel C70 WVGA 7.0in, analog resistiver Touch, Intel ATOM 333 MHz komp., 256 MB DDRAM, 32 kB FRAM, 2 GB on Board Flash-Drive , 1 X2X Link Schnittstelle, 1 POWERLINK-Schnittstelle, 1 Ethernet-Schnittstelle 10/100 Base-T, 2 USB 2.0 Schnittstellen, Basisgerät ohne Option Board, Hochformat, Anthrazit Pinstripe"
                   Description-en="Power Panel C70 WVGA 7.0in, analog resistive touch, Intel ATOM 333 MHz komp., 256 MB DDRAM, 32 kB FRAM, 2 GB on board flash-drive, 1 X2X Link interface, 1 POWERLINK interface, 1 Ethernet interface 10/100 Base-T, 2 USB 2.0 interfaces, base device without interface option board, portrait format, anthracite pinstripe"
                   Height="198"
                   Width="140"
                   Depth="46"
                   Weight="0"
                   DepthSpacingRear="80"
                   DepthSpacingFront="200"
                   HeightSpacingAbove="20"
                   HeightSpacingBelow="20"
                   WidthSpacingLeft="20"
                   WidthSpacingRight="20"
                   SnapHeightMiddleOffset="0"
                   PackagingQuantity="1"/>
      <PinSpecification>
         <Pin Number="0" Name="PE" Terminal="X0"/>
         <Pin Number="1" Name="VDC" Terminal="X1" IoType="Versorgung"/>
         <Pin Number="2" Name="GND" Terminal="X1" IoType="Versorgung"/>
         <Pin Number="1" Name="PLK" Terminal="IF1"/>
         <Pin Number="1" Name="ETH" Terminal="IF2"/>
         <Pin Number="1" Name="USB1" Terminal="IF3"/>
         <Pin Number="1" Name="USB2" Terminal="IF4"/>
         <Pin Number="1" Name="X2X" Terminal="IF5"/>
         <Pin Number="2" Name="X2XGND" Terminal="IF5"/>
         <Pin Number="3" Name="X2X\" Terminal="IF5"/>
         <Pin Number="4" Name="SHLD" Terminal="IF5"/>
      </PinSpecification>
   </EcadData>
  <Editors>
      <Editor Name="IoMapping" Target="SG4"/>
      <Editor Name="Configuration" Target="SG4"/>
      <Editor Name="Software" Target="SG4"/>
      <Editor Name="Permanent" Target="SG4"/>
      <Editor Name="PvPvMapping" Target="SG4"/>
   </Editors>
  <Firmware Folder="4PPC70.070M-20B">
      <Target FromVersion="K4.08" Name="SG4" SubFolder="1"/>
   </Firmware>
  <HWD Target="SG4">
      <Parameter ID="CompatibleCpuCode" Value="4PPC70.070M-20B"/>
      <Parameter ID="CompatibleCpuCodeMatchUseConfigured" Value="1" Type="UDINT"/>
      <Parameter ID="Transparent" Value="1" Type="UDINT"/>
      <Parameter ID="HwcCpuSlot" Value="0" Type="UDINT"/>
      <Parameter ID="HwcCpuStation" Value="0" Type="UDINT"/>
      <Parameter ID="HwcShortName" Value="PPC7x"/>
      <Parameter ID="ArModno" Value="58546" Type="UDINT"/>
      <Resource ID="IO">
         <Parameter ID="Transparent" Value="1" Type="UDINT"/>
         <Parameter ID="Classcode" Value="0x20000022" Type="UDINT"/>
         <Parameter ID="ARIOMaster" Value="1" Type="UDINT"/>
      </Resource>
      <Resource ID="PCI">
         <Parameter ID="Transparent" Value="1" Type="UDINT"/>
         <Parameter ID="Classcode" Value="0x20000000" Type="UDINT"/>
         <Parameter ID="OfferedBus" Value="PCI"/>
         <Parameter ID="PciDmaFastEnable" Value="1" Type="UDINT"/>
         <Resource ID="BUS-2">
            <Parameter ID="Transparent" Type="UDINT" Value="1"/>
            <Parameter ID="BusAddress" Type="UDINT" Value="0x0"/>
            <Parameter ID="PCIFuncNo" Type="UDINT" Value="0"/>
            <Parameter ID="VendorId" Type="UDINT" Value="0x8086"/>
            <Parameter ID="DeviceId" Type="UDINT" Value="0x8801"/>
            <Resource ID="IF2">
               <Parameter ID="BusAddress" Value="0" Type="UDINT"/>
               <Parameter ID="PCIFuncNo" Value="1" Type="UDINT"/>
               <Parameter ID="VendorId" Value="0x8086" Type="UDINT"/>
               <Parameter ID="DeviceId" Value="0x8802" Type="UDINT"/>
               <Parameter ID="MemEnable" Value="0" Type="UDINT"/>
               <Parameter ID="OfferedBus" Value="ETHERNET"/>
               <Parameter ID="PCIePath" Value="23,0/0,0"/>
            </Resource>
         </Resource>
         <Resource ID="FPGA">
            <Parameter ID="Transparent" Value="1" Type="UDINT"/>
            <Parameter ID="BusAddress" Value="0x0" Type="UDINT"/>
            <Parameter ID="VendorId" Value="0x1677" Type="UDINT"/>
            <Parameter ID="DeviceId" Value="0xe4b2" Type="UDINT"/>
            <Parameter ID="FwLoadMethod" Value="19" Type="UDINT"/>
            <Resource ID="BASEDEV">
               <Parameter ID="BusAddress" Value="0x0" Type="UDINT"/>
               <Parameter ID="PCIFuncNo" Value="0" Type="UDINT"/>
               <Parameter ID="VendorId" Value="0x1677" Type="UDINT"/>
               <Parameter ID="DeviceId" Value="0xe4b2" Type="UDINT"/>
               <Parameter ID="Transparent" Value="1" Type="UDINT"/>
            </Resource>
            <Resource ID="IF1">
               <Parameter ID="VirtFunc" Value="BASEDEV"/>
               <Parameter ID="OfferedBus" Value="P2IF"/>
               <Parameter ID="InterfaceType" Value="POWERLINK"/>
               <Parameter ID="MemEnable" Value="0" Type="UDINT"/>
               <Parameter ID="MemBAR" Value="3" Type="UDINT"/>
               <Parameter ID="MemEnableIfArio" Value="1" Type="UDINT"/>
               <Parameter ID="MemOffset" Value="0x80000" Type="UDINT"/>
               <Parameter ID="MMIOOffset" Value="0" Type="UDINT"/>
               <Parameter ID="IOSuffix" Value=":PLK.BUS"/>
               <Parameter ID="Classcode" Value="0x18800200" Type="UDINT"/>
               <Parameter ID="FirmwareFile" Value="58741a0.epl"/>
               <Resource ID="ETH">
                  <Parameter ID="Transparent" Value="1" Type="UDINT"/>
                  <Parameter ID="Classcode" Value="0x00000200" Type="UDINT"/>
                  <Parameter ID="AutoPlug" Value="1" Type="UDINT"/>
                  <Parameter ID="OfferedBus" Value="ETHERNET"/>
               </Resource>
               <Channel ID="LinkOk">
                  <Parameter ID="Ext" Value="/INFO"/>
                  <Parameter ID="Address" Value="0x8000001A" Type="UDINT"/>
                  <Parameter ID="BitOffset" Value="0" Type="UDINT"/>
                  <Parameter ID="Direction" Value="IN"/>
                  <Parameter ID="Type" Value="BOOL"/>
               </Channel>
            </Resource>
            <Resource ID="IF5">
               <Parameter ID="VirtFunc" Value="BASEDEV"/>
               <Parameter ID="OfferedBus" Value="X2XIF"/>
               <Parameter ID="InterfaceType" Value="X2X"/>
               <Parameter ID="MemEnable" Value="0" Type="UDINT"/>
               <Parameter ID="MemEnableIfArio" Value="1" Type="UDINT"/>
               <Parameter ID="MemLen0" Value="0xC000" Type="UDINT"/>
               <Parameter ID="MemBAR" Value="2" Type="UDINT"/>
               <Parameter ID="MemOffset" Value="0x8000" Type="UDINT"/>
               <Parameter ID="MMIOOffset" Value="0" Type="UDINT"/>
               <Parameter ID="MemLen2" Value="0xd000" Type="UDINT"/>
               <Parameter ID="IOSuffix" Value="X2X.BUS"/>
               <Parameter ID="Classcode" Value="0x09800200" Type="UDINT"/>
            </Resource>
            <Resource ID="IF10">
               <Parameter ID="VirtFunc" Value="BASEDEV"/>
               <Parameter ID="BusAddress" Value="0x0" Type="UDINT"/>
               <Parameter ID="PCIFuncNo" Value="0" Type="UDINT"/>
               <Parameter ID="MemBAR" Value="0" Type="UDINT"/>
               <Parameter ID="MMIOOffset" Value="0x2100" Type="UDINT"/>
               <Parameter ID="Classcode" Value="0x200000fe" Type="UDINT"/>
               <Parameter ID="DDriverName" Value="DdVxSfTouchPci"/>
               <Parameter ID="Key0" Value="-585,3223,538,1030,3,127"/>
               <Parameter ID="Transparent" Value="1" Type="UDINT"/>
            </Resource>
            <Resource ID="SRAM">
               <Parameter ID="Classcode" Value="0x00000500" Type="UDINT"/>
               <Parameter ID="MemLen0" Value="32768" Type="UDINT"/>
            </Resource>
         </Resource>
      </Resource>
   </HWD>
  <Images>
      <Image Type="FrontFoto" Name="4PPC70.070M-20B.front.png"/>
      <Image Type="Icon" Name="hw_dis.ico"/>
   </Images>
  <Keys>
      <Key Number="1" KeyBufferPos="127">
        <Layout PositionX="95.9"
                 PositionY="176"
                 SizeX="24.7"
                 SizeY="10.7"
                 Form="Rectangle"/>
      </Key>
      <Key Number="100" KeyBufferPos="126">
	  </Key>
   </Keys>
  
  <Memory Default="UserROM" Size="31488">
      <DRAM Transfer="true"/>
      <RemMem Transfer="false" DeviceParam="/DIRECT=0" DriverName="DdxxNvSRAM2"/>
      <SystemROM Transfer="true"/>
      <UserRAM Transfer="true" DeviceParam="/DIRECT=0" DriverName="DdxxNvSRAM2"/>
      <UserROM Transfer="true"/>
   </Memory>
  <Properties Target="SG4">
      <Include File="ArCfgCpuPPC70x"/>
      <Include File="VcCfgVcMappingKeys"/>
   </Properties>
  <Properties Target="SG4" Terminal="true">
      <Selector ID="OS"
                Name-en="Operating system"
                Name-de="Betriebssystem"
                Value="AR"
                ArConfigSkip="true">
         <Selection ID="AR" Name-en="AR Embedded" Name-de="AR Embedded">
            <Parameter ID="InaNodeNumber"
                       Name-en="INA node number"
                       Name-de="INA Knotennummer"
                       Type="UDINT"
                       Value="1"
                       Range="0-255"
                       SysConfPath="INADEVICES:%NW_ADDRESS%:InaNode"
                       MultiSelection="false"
                       Validated="true"/>
            <Include File="VcCfgEthSlaveVcMappingKeys"/>
         </Selection>
      </Selector>
   </Properties>
  <Runtime Target="SG4"
            FromVersion="K4.08"
            BuildError="true"
            AutomationRuntime="PPC7x">
      <DeviceDrivers>
         <Driver ID="File" Name="DdVxFiLowio"/>
         <Driver ID="serx2x" Name="DdVxSfX67"/>
      </DeviceDrivers>
      <InaDrivers>
         <Driver ID="Ethernet" Name="FbEthUdp"/>
      </InaDrivers>
   </Runtime>
  <Upload ModuleNr="58741" ModuleType="CPU"/>
  <VcDrivers VcTarget="VC4">
      <VcDriver Type="Display" DriverModule="vcpdi815.br" ClassID="0x211"/>
      <VcDriver Type="Touch" DriverModule="vcptelo.br" ClassID="0x20A">
         <Parameter Name="MirrorX" Value="false"/>
         <Parameter Name="MirrorY" Value="false"/>
         <Parameter Name="Interface" Value="PCI.FPGA.IF10"/>
      </VcDriver>
      <VcDriver Type="Key" DriverModule="vcpk.br" ClassID="0x208">
         <Parameter Name="RefreshRate" Value="10"/>
      </VcDriver>
   </VcDrivers>
</Module>
